({
	doInit : function(component, event, helper) {
        console.log('auth**' );
       
        var action = component.get("c.initialize");
          
        action.setCallback(this, function(a) {
            
                 var state = a.getState();
               if (state === "SUCCESS") {
                   
                        var auth = a.getReturnValue().isAuthenticate;
            			var CRUpdate = a.getReturnValue().isCommunityUpdate;
            			var vote = a.getReturnValue().isVote;
                        console.log('auth**'+auth);
                        console.log('CRUpdate**'+CRUpdate);
                   		console.log('vote**'+vote);
                        var Campaign = a.getReturnValue();  
                        
                   
                   		if(auth == true){ 
                             
                            component.set("v.isAuthenticate",true);
                        }
                        else if(CRUpdate == true){
                          	                            
                            	component.set("v.Campaign",Campaign); 
								component.set("v.isCommunityUpdate",true);                            
                        }
                        else if(vote == true){
                                                           
                            	 component.set("v.Campaign",Campaign);
                                 component.set("v.isVote",true);
                        }
                        else{
                            component.set("v.isCommunityUpdate",false);
                            component.set("v.isVote",false);
                            component.set("v.isAuthenticate",false);
                        }
                  
                }
                else{

                    alert('Failed');
                }
            
        });   
               
        $A.enqueueAction(action);
         
    },
    
    authenticate: function(component, event, helper) {
        
        var usertoken = component.get("v.UserToken");
        var firstname = component.get("v.FirstName"); 
        console.log('**IN_F**'+firstname);
        console.log('**IN_U**'+usertoken);
     	var action = component.get("c.authorizeuser");
 		 action.setParams({ 
            "usertoken": usertoken,
            "firstname": firstname
    	});
        
         
        action.setCallback(this, function(a) {
           
           var state = a.getState();
        if (state === "SUCCESS") {
                var auth = a.getReturnValue().isAuthenticate;
            	var CRUpdate = a.getReturnValue().isCommunityUpdate;
            	var vote = a.getReturnValue().isVote;
            
              
                        
                        var auth = a.getReturnValue().isAuthenticate;
            			var CRUpdate = a.getReturnValue().isCommunityUpdate;
            			var vote = a.getReturnValue().isVote;
                        console.log('auth**'+auth);
                        
                        console.log('vote**'+vote);
                        console.log('CRUpdate**'+CRUpdate);

                        var Campaign = a.getReturnValue();
                   		if(auth == true && CRUpdate == true){                            
                                 alert("You have already voted");
                        }
                        else if(auth == true){
                            alert("Please enter a valid Login and Password. If you are having trouble logging in please contact Jodi Goldberg at sajbd2@ctjc.co.za");
                        }
                        else if(CRUpdate == true){
                          	                               
                            	component.set("v.Campaign",Campaign); 
                                component.set("v.isCommunityUpdate",true); 
                                component.set("v.isAuthenticate",false);                           
                        }
                        else if(vote == true){
                                 
                                component.set("v.isAuthenticate",false);                          
                            	 component.set("v.Campaign",Campaign);
                                 component.set("v.isVote",true);
                                 
                        }
                        else{
                            
                        }
                 
            }
         else
         {
              alert("Failed");
         }
        });
        
    $A.enqueueAction(action);
    },
    
    UpdateCommunity: function(component, event, helper) {
        console.log('**IN**');
        var Campaign = component.get("v.Campaign"); 
 	    var action = component.get("c.UpdateCommunityReg");
        
 		 action.setParams({ 
        	"Campaign": Campaign
    	});
     	
        action.setCallback(this, function(a) {
           
            var state = a.getState();
            Campaign = a.getReturnValue();
           if (state === "SUCCESS") {
                Campaign = component.set("v.Campaign",Campaign);
               component.set("v.isVote",true);
               component.set("v.isCommunityUpdate",false);
          }
          else
          {
               alert("Failed");
          }
         });
        
        $A.enqueueAction(action);
    },
    
    
    SubmitVote: function(component, event, helper) {
        console.log('**IN**');
        var Campaign = component.get("v.Campaign"); 
        var candidates = component.get("v.Campaign.lst_Candidates");

        var count=0;
        
            
         
            for(var key in candidates){
                
                if(candidates[key].Selected == true){
                    count++;
                    
                }
            }    
       

        if(count == 0){
            alert('You must select at least one candiate.');
        }    
        else{
                var action = component.get("c.SubmitVoteSave");
                action.setParams({ 
                    "Campaign": Campaign
                });
            
                
                action.setCallback(this, function(a) {
                
                    var state = a.getState();
                if (state === "SUCCESS") {
                        console.log('**Thanks you **');     
                    component.set("v.ThankYouVote", true);
                }
                else
                {
                    alert("Failed");
                }
                });
                
                $A.enqueueAction(action);
        }        
    },

    openModel: function(component, event, helper) {
        
        var idx = event.target.id;
        
        var candidates = component.get("v.Campaign.lst_Candidates");
        var viewprofile = component.get("v.Campaign.ViewCandidates");
        
        console.log('**Profile	**'+viewprofile.First_Name__c); 
        
        for(var key in candidates){
             console.log('**Can	**'+idx); 
            if(candidates[key].CRid ==idx ){
                console.log('**Motivat**'+candidates[key].Motivation__c);
                
                 viewprofile.First_Name__c = candidates[key].Candidates.First_Name__c;
                 viewprofile.Last_Name__c = candidates[key].Candidates.Last_Name__c;
                 viewprofile.Motivation__c = candidates[key].Candidates.Motivation__c;
                viewprofile.Photo__c = candidates[key].Candidates.Photo__c;
                
            }    
        }
        
        component.set("v.Campaign.ViewCandidates",viewprofile);      
        component.set("v.isOpenProfile", true);
    },
    closeModel: function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.isOpenProfile", false);
    },
    updateSelected: function(component, event, helper) {
        var Camp = component.get("v.Campaign.lst_Candidates"); 

        var idx = event.target.id;
        var SelectedValue =event.target.checked;
        var count=0;
        console.log('**SelectedValue**'+SelectedValue);
        
            console.log('**One*');
            for(var key in Camp){
                
                
                if(Camp[key].CRid ==idx){
                    Camp[key].Selected = SelectedValue;
                }
                if(Camp[key].Selected == true){
                    console.log('**Plus**');
                    count++;
                }
                
                if(count>component.get("v.Campaign.MaxNumberOfVotes")){
                    console.log('**too many candidates selected**');
                    alert('You have chosen too many nominees');
                    for(var key in Camp){
                        if(Camp[key].CRid ==idx ){
                            Camp[key].Selected = false;
                            
                            break;
                        }   
                     
                    }    

                    break;
                }
                console.log('**Two*'+Camp[key].Selected);
            }
        
        
        component.set("v.Campaign.lst_Candidates",Camp); 

    },
	 openThankYou: function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.isOpenProfile", true);
    },
    closeThankYou: function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
                
        window.location.assign("https://www.capesajbd.org/");
        /*
                var urlEvent = $A.get("e.force:navigateToURL");
 					 urlEvent.setParams({
                  	   
    					'url': 'https://www.capesajbd.org/' 
  					});
                      urlEvent.fire();
         */             
    },
    
    opentermsconditions: function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.TermsConditions", true);
    },
    closetermsconditions: function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.TermsConditions", false);
    },


    
})