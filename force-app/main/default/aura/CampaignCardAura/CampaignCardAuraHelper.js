({
    fetchData: function (cmp) {
        console.log('CampaignCardAura: fetching data for ' + cmp.get('v.crid'));
        var action = cmp.get("c.crCampaignCardData");
        action.setParams({ crId : cmp.get('v.crid'), userListSelected : cmp.get('v.showUserList') });
        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('CampaignCardAura.crCampaignCardData: STATE ' + state);
            if (state === "SUCCESS") {
                // Alert the user with the value returned 
                // from the server
                
                cmp.set('v.com', response.getReturnValue());
                cmp.set('v.loaded', true);
                cmp.set('v.pledgeId', cmp.get('v.com.mostRecentPledge.Id'))
                // cmp.set('v.com.mostRecentPledgeRemarks', cmp.get('v.com.mostRecentPledgeRemarks'))
                // cmp.set('v.loaded', true);
                console.log('CampaignCardAura.crCampaignCardData: return ' + response.getReturnValue());
                console.log('CampaignCardAura.crCampaignCardData: pledgeId ' + cmp.get('v.pledgeId'));
                // You would typically fire a event here to trigger 
                // client-side notification that the server-side 
                // action is complete
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.error(JSON.stringify(errors));
            }
        });
        $A.enqueueAction(action);
    }, 

    toast : function(cmp, message, title, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message, 
            "type": type
        });
        toastEvent.fire();
    }
    
});