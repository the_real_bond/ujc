({
    init: function (cmp, event, helper) {
        console.log('CampaignCardAura: Initializing');
        // var fetchData = {
        //     opportunityName: "company.companyName",
        //     accountName : "name.findName",
        //     closeDate : "date.future",
        //     amount : "finance.amount",
        //     contact: "internet.email",
        //     phone : "phone.phoneNumber",
        //     website : "internet.url",
        //     status : {type : "helpers.randomize", values : [ 'Pending', 'Approved', 'Complete', 'Closed' ] },
        //     actionLabel : {type : "helpers.randomize", values : [ 'Approve', 'Complete', 'Close', 'Closed' ]},
        //     confidenceDeltaIcon : {type : "helpers.randomize", values : [ 'utility:up', 'utility:down' ]}
        // };


        // cmp.set('v.columns', [
            
        //     {label: 'Name', fieldName: 'Name', type: 'text'}
            
        // ]);

        helper.fetchData(cmp);
    },

    onCheck: function(component, event, helper) {
        var status = component.find("status").get("v.value");
        console.log('current status ' + status);
        var rbtValue = event.getSource().getLocalId();
        if (rbtValue == 'checkboxPledged') {
            component.find("status").set("v.value", "Active");
            component.find("checkboxRefused").set("v.value", "false");
            component.find("checkboxDeferred").set("v.value", "false");
        } else if (rbtValue == 'checkboxRefused') {
            component.find("status").set("v.value", "Refused");
            component.find("checkboxPledged").set("v.value", "false");
            component.find("checkboxDeferred").set("v.value", "false");
        } else {
            component.find("status").set("v.value", "Pending");
            component.find("checkboxPledged").set("v.value", "false");
            component.find("checkboxRefused").set("v.value", "false");
        }
    },

    onButtonPressed: function(component, event, helper) {
        var navigate = component.get('v.navigateFlow');
    	var selectedRow = component.get('v.selectedRow');
        var whichFlowButtonHasBeenPressed = event.getSource().getLocalId();
        console.log('button pressed ' + whichFlowButtonHasBeenPressed)
        if (whichFlowButtonHasBeenPressed == 'NEXT') {
            // if (selectedRow != undefined && selectedRow != '' && selectedRow.length > 0) {
                
            // } else {
            //     helper.toast(component, 'Please select a Community Register from Your List or the General List to continue.',  'Selection error', 'error');
            // }
            component.find("editForm").submit();
            navigate('NEXT');
        } else if (whichFlowButtonHasBeenPressed == 'BACK') {
            navigate('BACK');
        } else if (whichFlowButtonHasBeenPressed == 'PAUSE') {
            navigate('PAUSE');
        } 
    },

    update : function(component,event,helper) {
        if (component.find("checkboxRefused").get("v.value") || component.find("checkboxPledged").get("v.value") || component.find("checkboxDeferred").get("v.value")) {
            component.find("editForm").submit();
            
        } else {
            helper.toast(component,'Please select a call outcome checkbox (Outcome 1: Donor has Pledged, Outcome 2: Donor has Refused, Outcome 3: Donor has asked to call later/deferred)', 'Cannot update', 'error')
        }
        
    },

    formSuccess : function(component,event,helper) {
        component.set('v.canNext',true);
        helper.toast(component, 'The update was successful', 'Update record', 'success');
    },


    handleNewRemark: function(component, event, helper) {
        var remark = component.find('remarks').get('v.value');
        var date = component.find('date').get('v.value');
        var dateToday = new Date().toISOString().slice(0, 10);
        //var action = cmp.get("c.addRemarkToPledge");
        var concat = sprintf("%s{20}%s{11}%s{50}%s{11}", component.get("v.com.loggedInUser"), dateToday, remark, date);
        console.log('new remark ' + concat);
        // action.setParams({ 
        //     recordId : cmp.get('v.com.mostRecentPledge.Id'), 
        //     remark 
        // });
        // // Create a callback that is executed after 
        // // the server-side action returns
        // action.setCallback(this, function(response) {
        //     var state = response.getState();
        //     console.log('CampaignCardAura.crCampaignCardData: STATE ' + state);
        //     if (state === "SUCCESS") {
        //         // Alert the user with the value returned 
        //         // from the server
                
        //         cmp.set('v.com', response.getReturnValue());
        //         // cmp.set('v.com.mostRecentPledgeRemarks', cmp.get('v.com.mostRecentPledgeRemarks'))
        //         // cmp.set('v.loaded', true);
        //         console.log('CampaignCardAura.crCampaignCardData: return ' + response.getReturnValue());
        //         // You would typically fire a event here to trigger 
        //         // client-side notification that the server-side 
        //         // action is complete
        //     }
        //     else if (state === "INCOMPLETE") {
        //         // do something
        //     }
        //     else if (state === "ERROR") {
        //         var errors = response.getError();
        //         console.error(JSON.stringify(errors));
        //     }
        // });
        // $A.enqueueAction(action);
    }

    // updateSelected: function (cmp, event) {
    //     var selectedRows = event.getParam('selectedRows');
    //     console.log(selectedRows);
    //     cmp.set('v.selectedRow', selectedRows);
    // },

    // toggleList: function (cmp, event, helper) {
    //     cmp.set('v.showUserList', cmp.get('v.showUserList') == true ? false : true);
    //     helper.sucessToast(cmp, cmp.get('v.showUserList') == true ? 'Now showing Your List' : 'Now showing the General List', 'List change');
    // },

    
});