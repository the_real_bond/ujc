({
    fetchData: function (cmp) {
        var action = cmp.get("c.getLoggedInUserList");
        console.log('DigitalTelephonCallListController: fetching data');
        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('DigitalTelephonCallListController.getLoggedInUserList: STATE ' + state);
            if (state === "SUCCESS") {
                // Alert the user with the value returned 
                // from the server
                
                cmp.set('v.data', response.getReturnValue());
                
                console.log('DigitalTelephonCallListController.getLoggedInUserList: return ' + JSON.stringify(response.getReturnValue()));
                // You would typically fire a event here to trigger 
                // client-side notification that the server-side 
                // action is complete
                cmp.set('v.loaded', true);

                this.chartjs(cmp);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                this.toast(cmp, errors[0].message, 'Error', 'error');
                console.error(JSON.stringify(errors));
                // throw errors[0].message;
            }
        });
        $A.enqueueAction(action);
    },

    chartjs: function (cmp) {
        console.log('DigitalTelephonCallListController.chartjs: Start ');
        var action = cmp.get("c.getChartData");
        console.log('DigitalTelephonCallListController.getChartData: fetching chart data');
        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('DigitalTelephonCallListController.getChartData: STATE ' + state);
            if (state === "SUCCESS") {
                console.log('DigitalTelephonCallListController.getChartData: return ' + JSON.stringify(response.getReturnValue()));
                var el = cmp.find('bar-chart').getElement();
                var ctx = el.getContext('2d');
                
                new Chart(ctx, {
                    maintainAspectRatio: false,
                    animationEnabled: true,
                    responsiveAnimationDuration: 2,
                    responsive: true,
                    theme: 'light2',
                    type: 'bar',
                    data: {
                    labels: response.getReturnValue().labels,
                    datasets: [
                        {
                        label: "Pledge Total (thousands)",
                        data: response.getReturnValue().data
                        }
                    ]
                    },
                    options: {
                    legend: { display: false },
                    title: {
                        display: true,
                        text: 'Pledge Totals By Canvasser'
                    }
                    }
                });
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                this.toast(cmp, errors[0].message, 'Error', 'error');
                console.error(JSON.stringify(errors));
                // throw errors[0].message;
            }
        });
        $A.enqueueAction(action);
        
    },

    toast : function(cmp, message, title, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message, 
            "type": type
        });
        toastEvent.fire();
    }
});