({
    init: function (cmp, event, helper) {
        console.log('DigitalTelephonCallListController: Initializing');
        // var fetchData = {
        //     opportunityName: "company.companyName",
        //     accountName : "name.findName",
        //     closeDate : "date.future",
        //     amount : "finance.amount",
        //     contact: "internet.email",
        //     phone : "phone.phoneNumber",
        //     website : "internet.url",
        //     status : {type : "helpers.randomize", values : [ 'Pending', 'Approved', 'Complete', 'Closed' ] },
        //     actionLabel : {type : "helpers.randomize", values : [ 'Approve', 'Complete', 'Close', 'Closed' ]},
        //     confidenceDeltaIcon : {type : "helpers.randomize", values : [ 'utility:up', 'utility:down' ]}
        // };
        

        cmp.set('v.columns', [
            {label: 'Name', fieldName: 'Name', type: 'text'}, 
            {label: 'Last called', fieldName: 'latestPledgeLastModifiedDate', type: 'text'},
            {label: 'When to follow-up', fieldName: 'latestPledgeFollowUpDate', type: 'text'}
        ]);

        helper.fetchData(cmp);

    },

    updateSelected: function (cmp, event) {
        var selectedRows = event.getParam('selectedRows');
        // console.log(selectedRows);
        cmp.set('v.selectedRow', event.getParam('selectedRows'));
        // cmp.set('v.selectedCR', event.getParam('selectedRows')[0]);
        cmp.set('v.crid', event.getParam('selectedRows')[0].Id);
        console.log(JSON.stringify((cmp.get('v.crid'))))
    },

    toggleList: function (cmp, event, helper) {
        cmp.set('v.showUserList', cmp.get('v.showUserList') == true ? false : true);
        helper.toast(cmp, cmp.get('v.showUserList') == true ? 'Now showing Your List' : 'Now showing the General List', 'List change', 'success');
    },
    onButtonPressed: function(component, event, helper) {
        var navigate = component.get('v.navigateFlow');
    	var selectedRow = component.get('v.selectedRow');
        var whichFlowButtonHasBeenPressed = event.getSource().getLocalId();
        console.log('button pressed ' + whichFlowButtonHasBeenPressed)
        if (whichFlowButtonHasBeenPressed == 'NEXT') {
            if (selectedRow != undefined && selectedRow != '' && selectedRow.length > 0) {
                navigate('NEXT');
            } else {
                helper.toast(component, 'Please select a Community Register from Your List or the General List to continue.',  'Selection error', 'error');
            }
           	
        } else if (whichFlowButtonHasBeenPressed == 'BACK') {
            navigate('BACK');
        } else if (whichFlowButtonHasBeenPressed == 'PAUSE') {
            navigate('PAUSE');
        } 
    },

    
});