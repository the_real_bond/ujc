/**
 * @description       : Handles sending of emails for Pledges, Statements and Receipts
 * @author            : Sean Parker
 * @group             : 
 * @last modified on  : 01-12-2020
 * @last modified by  : Lian Bond
 * Modifications Log 
 * Ver   Date         Author      Modification
 * 1.0   ????-2015    Sean Parker   Initial Version
 * 1.1   01/12/2020   Lian Bond     Updated the Receiptthankyouemail method to be able to handle multiple receipts to the same account
**/
Public Class PledgeStatementEmailUtility{

Public static Boolean Individual;            

public static void pledgethankyouemail(List<Recurring_Donation__c> Lst_RecDon,Boolean send) {
        List<Account> lst_Acc;
        
         Integer CurrentYear = system.Today().year();          
                
        Boolean SendEmail = True;
        Set<String> set_Accid = new Set<String>();
        Map<String,Decimal> map_Amount = new Map<String,Decimal>();
        Map<String,String> map_Date = new Map<String,String>();
        
        Date todayDate = system.Today();
        
        for(Recurring_Donation__c r:Lst_RecDon){
                if(r.Thank_you_Sent__c == false){
                     map_Amount.put(r.Donor__c,r.Pledge_Total__c);
                     map_Date.put(r.Donor__c,r.Today_Date__c);
                    set_Accid.Add(r.Donor__c);
                }          
        }
        
        lst_Acc = new List<Account>(); 
        lst_Acc = [SELECT PersonEmail,Email_Thank_you_letter__c,Mail_Merge_Email__c,Email_Personal__c,CreatedById,ownerid,Communal_Reference_Number__c,SALUTATION_INITIALS_LASTNAME__c,MAIL_MERGE_ADDRESS__c,MAIL_MERGE_SUBURB__c,MAIL_MERGE_POSTAL_CODE__c,FIRSTNAME__c,X2015_PLEDGE_TOTAL__c 
                        FROM Account where   id in:set_Accid AND Mail_Merge_Email__c !='' AND Category__c excludes ('Friends of UJC','Lion of Judah','Top Donor','Trust & Foundations')
                         AND DatabaseStatus__c =:'Active' ]; //AND  Mail_Preference__c !=:'X - No Mail'
        system.debug('***EMAIL2***'+lst_Acc);

 if(send == true){
    for(Account acc:lst_Acc){     
         acc.Email_Thank_you_letter__c=true;
    }
    update lst_Acc; 
 }
 else{       
     Task addEmail;
        
     for(Account acc:lst_Acc){
     
      if(acc.Email_Thank_you_letter__c==true){
        
        addEmail = new Task();
        
        if(acc.Mail_Merge_Email__c == null){           
            system.debug('***Null Email***');
            SendEmail=false;           
            addEmail.subject = 'Missing Email Address';
            addEmail.Description = 'Pledge Thank You Email was not sent because there is no email address No: '+ acc.name;        
        }
        
        else{
            acc.Email_Thank_you_letter__c=false;
            system.debug('***Not Null Email***');
            addEmail.OwnerId = acc.ownerid;
            addEmail.subject = 'Thank you Email';
            addEmail.status = 'closed';
            addEmail.Priority = 'Normal';
            addEmail.ActivityDate = System.Today();
            addEmail.Description = 'Pledge Thank You Email Sent';
            
             
            String addresses,donor,subject,body;
            
            EmailTemplate templateId; 
            
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            
            
            if(acc.Mail_Merge_Email__c != null){
                
                donor = acc.name;
                addresses = acc.Mail_Merge_Email__c;
                addEmail.WhatId = acc.id;
                
                
                
                subject = 'Thank you | '+CurrentYear+ ' UJC Pledge';
                body = '<html><body style=" font-family:Calibri";><div style="padding:70px;">';
                  
               
                body =body + '<br> </p><p>Dear '+ acc.FIRSTNAME__c;
                body =body + ', <br><br><b>UJC Pledge '+ CurrentYear+'</b><br></p><table width ="70%"><tr><td>I would like to say a heartfelt thank you for your generous pledge in the amount of R'; 
                body =body + map_Amount.get(acc.id) + ' to our '+ CurrentYear +' Campaign.<br><br> </tr></td><tr><td align="justify">It is difficult to convey the appreciation and gratitude for your ongoing involvement in ensuring the '; 
                body =body +'Cape Town Jewish community is able to thrive and provide the excellent services and infrastructure that are needed to sustain our vibrant community.<br><br></tr></td>';
                body =body +'<tr><td align="justify">We are always striving to improve our offerings and interactions with you, in order to ensure a meaningful platform to contribute philanthropically to the Jewish community and its members. Please let us know if you have any suggestions that can help us to ensure your sustained involvement.</tr></td>';
                body =body +'<tr><td align="justify"><br>Thank you for supporting and strengthening our community! <br><br>Warm wishes<br><br><img   src="https://c.cs86.content.force.com/servlet/servlet.FileDownload?file=015D0000004ERim" /></tr></td>';
                body =body +'<tr><td align="justify">Barry Levitt <br>Executive Director <br>United Jewish Campaign <br>0214646700 <br>blevitt@ctjc.co.za <br></tr></td>';
                
                body =body +'<tr><td align="justify"><br>Account Name: United Jewish Campaign <br>Bank: Standard Bank <br>Account No: 070708010 <br>Brank Code: 020909 <br>Your reference: '+acc.Communal_Reference_Number__c+' <br></tr></td>';
                body =body +'</div></body></html>';
               
                 email.setHtmlBody(body );
                 email.setSubject( subject );
                      
            }  
        //Attachment get the data
        
              

        // Create the email attachment and the email
            
            system.debug('**addresses**'+addresses);
            String[] toAddresses = addresses.split(':', 0);
          
            email.setSaveAsActivity(true);
            email.setReplyTo('amaneveld@ctjc.co.za');
            email.setToAddresses( toAddresses);
           
             OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'blevitt@ctjc.co.za'];  
            // Sets the paramaters of the email
            
            if ( owea.size() > 0 ) {
                email.setOrgWideEmailAddressId(owea.get(0).Id);
            }
             
            // Sends the email    
            if (!Test.isRunningTest()) {    
                Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});   
            }
            Insert addEmail;
           }      
       }
      } 
      update lst_Acc;
      
      for(Recurring_Donation__c r:Lst_RecDon){
          r.Thank_you_Sent__c = true;
      }
      
      update Lst_RecDon;    
         
    }        
}    

public static void Receiptthankyouemail(List<Receipt__c> Lst_RecDon,Boolean send) {
        List<Account> lst_Acc;
        
         Integer CurrentYear = system.Today().year();          
                
        Boolean SendEmail = True;
        Set<String> set_Accid = new Set<String>();
        Map<String,Decimal> map_Amount = new Map<String,Decimal>();
        Map<String,String> map_Date = new Map<String,String>();
        
        Date todayDate = system.Today();
        
        system.debug('***EMAIL2***'+Lst_RecDon);
        for(Receipt__c r:Lst_RecDon){
                if(r.Thank_you_Sent__c == false){
                    //LB - there may be multiple receipts linked to this account for which thank yous have to be sent, 
                    //so we need to make sure that we do not overwrite existing values in the map
                    decimal amount = map_Amount.get(r.Donor__c) == null ? map_Amount.put(r.Donor__c,r.Total_Receipt_Amount__c) : map_Amount.put(r.Donor__c,(r.Total_Receipt_Amount__c + map_Amount.get(r.Donor__c)));
                    //map_Amount.put(r.Donor__c,r.Total_Receipt_Amount__c);
                     map_Date.put(r.Donor__c,r.Receipt_Date_Email__c);
                    set_Accid.Add(r.Donor__c);
                }          
        }
        
        lst_Acc = new List<Account>(); 
        lst_Acc = [SELECT Id,PersonEmail,Preferred_Name__c,Email_Receipt_Thank_you_letter__c,Mail_Merge_Email__c,Email_Personal__c,ownerid,CreatedById,Communal_Reference_Number__c,SALUTATION_INITIALS_LASTNAME__c,MAIL_MERGE_ADDRESS__c,MAIL_MERGE_SUBURB__c,MAIL_MERGE_POSTAL_CODE__c,FIRSTNAME__c,X2015_PLEDGE_TOTAL__c 
                        FROM Account where   id in:set_Accid AND Mail_Merge_Email__c !='' AND Category__c excludes ('Friends of UJC')
                         AND DatabaseStatus__c =:'Active' ]; 
    
    	/*lst_Acc = [SELECT PersonEmail,Preferred_Name__c,Email_Receipt_Thank_you_letter__c,Mail_Merge_Email__c,Email_Personal__c,ownerid,CreatedById,Communal_Reference_Number__c,SALUTATION_INITIALS_LASTNAME__c,MAIL_MERGE_ADDRESS__c,MAIL_MERGE_SUBURB__c,MAIL_MERGE_POSTAL_CODE__c,FIRSTNAME__c,X2015_PLEDGE_TOTAL__c 
                        FROM Account where   id in:set_Accid AND Mail_Merge_Email__c !='' AND Category__c excludes ('Friends of UJC','Lion of Judah','Top Donor','Trust & Foundations')
                         AND DatabaseStatus__c =:'Active' ]; */
        system.debug('***EMAIL2***'+lst_Acc);

 if(send == true){
    for(Account acc:lst_Acc){     
         acc.Email_Receipt_Thank_you_letter__c=true;
    }
    update lst_Acc; 
 }
 else{       
     Task addEmail;
        
     for(Account acc:lst_Acc){
     
      if(acc.Email_Receipt_Thank_you_letter__c==true){
        
        addEmail = new Task();
        
        if(acc.Mail_Merge_Email__c == null){           
            system.debug('***Null Email***');
            SendEmail=false;           
            addEmail.subject = 'Missing Email Address';
            addEmail.Description = 'Payment thank you was not sent because there is no email address';        
        }
        
        else{
            acc.Email_Receipt_Thank_you_letter__c=false;
            system.debug('***Not Null Email***');
            addEmail.OwnerId = acc.ownerid;
            addEmail.subject = 'Thank you Email';
            addEmail.status = 'closed';
            addEmail.Priority = 'Normal';
            addEmail.ActivityDate = System.Today();
            addEmail.Description = 'Payment(s) Thank You Email Sent for total amount of ' + map_Amount.get(acc.Id);
            
             
            String addresses,donor,subject,body,FirstName;
            
            EmailTemplate templateId; 
            
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            
            
            if(acc.Mail_Merge_Email__c != null){
                
                donor = acc.name;
                addresses = acc.Mail_Merge_Email__c;
                addEmail.WhatId = acc.id;
                
                if(acc.Preferred_Name__c != null){
                    FirstName = acc.Preferred_Name__c;
                }
                else{
                    FirstName = acc.FIRSTNAME__c;
                }
                
                subject = 'Thank you | '+CurrentYear+ ' UJC Receipt';
                body = '<html><body style=" font-family:Calibri";><div style="padding:70px;">';
                  
               
                body =body + '<br> </p><p>Dear '+ FirstName;
                body =body + '</p><table width ="70%" style=" font-family:Calibri";><tr><td>I would like to say a heartfelt thank you for your payment received on ' +map_Date.get(acc.id) + ' in the amount of R'; 
                body =body + map_Amount.get(acc.id) + '.<br><br> </tr></td><tr><td align="justify">It is difficult to convey the appreciation and gratitude for your ongoing involvement in ensuring the '; 
                body =body +'Cape Town Jewish community is able to thrive and provide the excellent services and infrastructure that are needed to sustain our vibrant community.<br><br></tr></td>';
                body =body +'<tr><td align="justify">We are always striving to improve our offerings and interactions with you, in order to ensure a meaningful platform to contribute philanthropically to the Jewish community and its members. Please let us know if you have any suggestions that can help us to ensure your sustained involvement.</tr></td>';
                body =body +'<tr><td align="justify"><br>Thank you for supporting and strengthening our community! <br><br>Warm wishes<br><br><img width="700"  src="https://c.um2.content.force.com/servlet/servlet.ImageServer?id=0154G000006TfPh&oid=00DD0000000lDkL&lastMod=1568362548000" /></tr></td>';
                /*
                body =body +'<tr><td align="justify">Barry Levitt <br>Executive Director <br>United Jewish Campaign <br>0214646700 <br>blevitt@ctjc.co.za <br></tr></td>';
                
                body =body +'<tr><td align="justify"><br>Account Name: United Jewish Campaign <br>Bank: Standard Bank <br>Account No: 070708010 <br>Brank Code: 020909 <br>Your reference: '+acc.Communal_Reference_Number__c+' <br></tr></td>';
                */
                
                body =body +'</div></body></html>';
               
                 email.setHtmlBody(body );
                 email.setSubject( subject );
                      
            }  
        //Attachment get the data
        
              

        // Create the email attachment and the email
            
            system.debug('**addresses**'+addresses);
            String[] toAddresses = addresses.split(':', 0);
          
            email.setSaveAsActivity(true);
            email.setReplyTo('amaneveld@ctjc.co.za');
            email.setToAddresses( toAddresses);
           
            OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'blevitt@ctjc.co.za'];  
            // Sets the paramaters of the email
            
            if ( owea.size() > 0 ) {
                email.setOrgWideEmailAddressId(owea.get(0).Id);
            }
             
            // Sends the email    
            if (!Test.isRunningTest()) {    
                Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});   
            }
          
            Insert addEmail;
            System.debug('PledgeStatementEmailUtility: creating list of tasks: ' + addEmail);
           }      
       }
      } 
      update lst_Acc;
      
      for(Receipt__c r:Lst_RecDon){
          r.Thank_you_Sent__c = true;
      }
      
      update Lst_RecDon;    
         
    }        
}

Public PageReference IndividualEmailStatement(){
    
    List<Recurring_Donation__c> Lst_RecDon;
    
    
        Integer CurrentYearNo = system.Today().year();
        String CurrentY, YLessone,YLesstwo,YLessthree,YLessfour;
        
        CurrentY = String.Valueof(CurrentYearNo);
        YLessone = String.Valueof(CurrentYearNo-1);
        YLesstwo = String.Valueof(CurrentYearNo-2);
        YLessthree = String.Valueof(CurrentYearNo-3);
        YLessfour = String.Valueof(CurrentYearNo-4);
                
        Lst_RecDon = new List<Recurring_Donation__c>([SELECT id FROM Recurring_Donation__c WHERE Status__c ='Active' AND Approved__c =:True AND Donor__c =: apexpages.currentpage().getparameters().get('id') 
                    AND (Method_of_Payment__c =:'OP' OR (Method_of_Payment__c =:'CC' AND Frequency__c =:'Yearly' ) ) AND Campaign__c =:'JC' AND (Campaign_Year__c =:CurrentY OR Campaign_Year__c =:YLessone OR Campaign_Year__c =:YLesstwo  OR Campaign_Year__c =:YLessthree OR Campaign_Year__c =:YLessfour)]);
        
        Individual=True;
        pledgestatement(Lst_RecDon,true);
        return new PageReference('/'+apexpages.currentpage().getparameters().get('id')); 

}


public static void pledgestatement(List<Recurring_Donation__c> Lst_RecDon, Boolean send) {
        List<Account> lst_Acc;
        
        Integer CurrentYear = system.Today().year();          
                
        Boolean SendEmail = True;
        Set<String> set_Accid = new Set<String>();
        Set<String> set_Pledge = new Set<String>();
        Map<String,Decimal> map_Amount = new Map<String,Decimal>();
        
        Map<String,Decimal> map_BalCurrent = new Map<String,Decimal>();
        Map<String,Decimal> map_BalFirst = new Map<String,Decimal>();
        Map<String,Decimal> map_BalSecond = new Map<String,Decimal>();
        Map<String,Decimal> map_BalThird = new Map<String,Decimal>();
        Map<String,Decimal> map_BalFourth = new Map<String,Decimal>();
        
        Map<String,Decimal> map_PledgeCurrent = new Map<String,Decimal>();
        Map<String,Decimal> map_PledgeFirst = new Map<String,Decimal>();
        Map<String,Decimal> map_PledgeSecond = new Map<String,Decimal>();
        Map<String,Decimal> map_PledgeThird = new Map<String,Decimal>();
        Map<String,Decimal> map_PledgeFourth = new Map<String,Decimal>();
        Map<String,Decimal> map_Total = new Map<String,Decimal>();
      
        Map<String,String> map_Date = new Map<String,String>();
               
        for(Recurring_Donation__c r:Lst_RecDon){
             set_Pledge.add(r.id);           
        } 
         
                
        //Aggregate function to Dynamically get the last 5 years total pledge and outstanding
        
        AggregateResult[] groupedResults = [SELECT Sum(Pledge_Total__c)Pledge, Sum(Total_Outstanding__c)Balance, Donor__c,Campaign_Year__c  FROM Recurring_Donation__c WHERE id in:set_Pledge Group By Donor__c,Campaign_Year__c  ]; 
         system.debug('***groupedResults***'+groupedResults);
         
        
        for (AggregateResult ar : groupedResults)  {
            map_PledgeCurrent.put((String) ar.get('Donor__c'),0);
            map_PledgeFirst.put((String) ar.get('Donor__c'),0);
            map_PledgeSecond.put((String) ar.get('Donor__c'),0);
            map_PledgeThird.put((String) ar.get('Donor__c'),0);
            map_PledgeFourth.put((String) ar.get('Donor__c'),0);
            
            map_BalCurrent.put((String) ar.get('Donor__c'),0);
            map_BalFirst.put((String) ar.get('Donor__c'),0);
            map_BalSecond.put((String) ar.get('Donor__c'),0);
            map_BalThird.put((String) ar.get('Donor__c'),0);
            map_BalFourth.put((String) ar.get('Donor__c'),0); 
        }
        
        for (AggregateResult ar : groupedResults)  {                   
            
           if (CurrentYear - integer.valueof(ar.get('Campaign_Year__c')) == 0){
               map_PledgeCurrent.put((String) ar.get('Donor__c'),(Decimal) ar.get('Pledge'));
               map_BalCurrent.put((String) ar.get('Donor__c'),(Decimal) ar.get('Balance'));                       
               set_Accid.add((String) ar.get('Donor__c'));
           }
           if (CurrentYear - integer.valueof(ar.get('Campaign_Year__c')) == 1){
               map_PledgeFirst.put((String) ar.get('Donor__c'),(Decimal) ar.get('Pledge'));
               map_BalFirst.put((String) ar.get('Donor__c'),(Decimal) ar.get('Balance'));
              
               set_Accid.add((String) ar.get('Donor__c'));
           }
           if (CurrentYear - integer.valueof(ar.get('Campaign_Year__c')) == 2){
               map_PledgeSecond.put((String) ar.get('Donor__c'),(Decimal) ar.get('Pledge'));              
               map_BalSecond.put((String) ar.get('Donor__c'),(Decimal) ar.get('Balance'));
               
               set_Accid.add((String) ar.get('Donor__c')); 
           }
           if (CurrentYear - integer.valueof(ar.get('Campaign_Year__c')) == 3){
               map_PledgeThird.put((String) ar.get('Donor__c'),(Decimal) ar.get('Pledge'));
               map_BalThird.put((String) ar.get('Donor__c'),(Decimal) ar.get('Balance'));

               set_Accid.add((String) ar.get('Donor__c'));
           }
           if (CurrentYear - integer.valueof(ar.get('Campaign_Year__c')) == 4){
               map_PledgeFourth.put((String) ar.get('Donor__c'),(Decimal) ar.get('Pledge'));
               map_BalFourth.put((String) ar.get('Donor__c'),(Decimal) ar.get('Balance'));

               set_Accid.add((String) ar.get('Donor__c'));
           }
            
       } 
       
       
          
     lst_Acc = new List<Account>(); 
        lst_Acc = [SELECT Name,PersonEmail,Mail_Merge_Email__c,Email_Personal__c,Total_Billable__c,ownerid,CreatedById,Communal_Reference_Number__c,SALUTATION_INITIALS_LASTNAME__c,MAIL_MERGE_ADDRESS__c,MAIL_MERGE_SUBURB__c,MAIL_MERGE_POSTAL_CODE__c,FIRSTNAME__c,X2015_PLEDGE_TOTAL__c FROM Account 
                                    where id in:set_Accid AND Mail_Merge_Email__c !='' AND Receive_Statement__c !=:'N'  AND  (DatabaseStatus__c !=:'Deceased' OR DatabaseStatus__c !=:'Emigrated' OR DatabaseStatus__c !=:'Inactive' OR DatabaseStatus__c !=:'Other Centre') 
                                        AND Category__c excludes ('Friends of UJC','Lion of Judah','Top Donor','Trust & Foundations')  ]; //AND Mail_Preference__c !='X - No Mail'
    
    
        system.debug('***lst_Acc***'+lst_Acc);
        set_Accid.clear();
    for(Account acc:lst_Acc){    
        
            
        map_Total.put(acc.id,map_BalCurrent.get(acc.id)+map_BalFirst.get(acc.id)+map_BalSecond.get(acc.id)+map_BalThird.get(acc.id)+map_BalFourth.get(acc.id));         
        system.debug('***SET***'+map_Total.get(acc.id)+'*****'+map_Total); 
        if(map_Total.get(acc.id) > 0){
            set_Accid.Add(acc.id);        
        }
        
            
    }
       
    lst_Acc = new List<Account>(); 
    lst_Acc = [SELECT PersonEmail,TodayDate__c,Email_Monthly_Statements__c,Mail_Merge_Email__c,Total_Billable__c,ownerid,CreatedById,Communal_Reference_Number__c,SALUTATION_INITIALS_LASTNAME__c,MAIL_MERGE_ADDRESS__c,MAIL_MERGE_SUBURB__c,MAIL_MERGE_POSTAL_CODE__c,FIRSTNAME__c,X2015_PLEDGE_TOTAL__c FROM Account where id in:set_Accid Order by LastName ASC];
        
          
 if(send == false){
 
    for(Account acc:lst_Acc){     
         system.debug('***EMAIL2***'+lst_Acc);
         acc.Email_Monthly_Statements__c=true;
    }
    update lst_Acc; 
 }
 else{       
     
     Task addEmail;
        
     for(Account acc:lst_Acc){ 
        system.debug('***In Mail***');
        addEmail = new Task();
        
      if(acc.Email_Monthly_Statements__c==true || Individual==True){ 
        
        if(acc.Mail_Merge_Email__c == null){           
            system.debug('***Null Email***');
            SendEmail=false;           
            addEmail.subject = 'Missing Email Address';
            addEmail.Description = 'Statement Email was not sent because there is no email address No: '+ acc.name;        
        }
        if(SendEmail==false){
        
        }
        else{
            acc.Email_Monthly_Statements__c=false;
            system.debug('***Not Null Email***');
            addEmail.OwnerId = acc.ownerid;
            addEmail.subject = 'Statement Email';
            addEmail.status = 'closed';
            addEmail.Priority = 'Normal';
            addEmail.ActivityDate = System.Today();
            addEmail.Description = 'Statement Email Sent';
            
             
            String addresses,donor,subject,body,emailbody,FirstName;
            
            EmailTemplate templateId; 
            
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            
            
            if(acc.Mail_Merge_Email__c != null){
                
                donor = acc.name;
                addresses = acc.Mail_Merge_Email__c;
                addEmail.WhatId = acc.id;
                
                if(acc.Preferred_Name__c != null){
                    FirstName = acc.Preferred_Name__c;
                }
                else{
                    FirstName = acc.FIRSTNAME__c;
                }              
                        
                subject = 'Statement | UJC Contribution';
              body='<html><body style=" font-family:Calibri,Arial; font-size:9px";><table width ="70%"><tr><td align="right"><img width="110" height="80" src="https://c.um2.content.force.com/servlet/servlet.ImageServer?id=015D0000004D5XB&oid=00DD0000000lDkL&lastMod=1496069622000" /></td></tr></table>';

              body=body+  acc.SALUTATION_INITIALS_LASTNAME__c + '<br>';
              if(acc.MAIL_MERGE_ADDRESS__c != null){
                  body=body+  acc.MAIL_MERGE_ADDRESS__c + '<br>';
              }
              else{
                  body=body+  '<br>';
              }
              if( acc.MAIL_MERGE_SUBURB__c != null){  
                  body=body+  acc.MAIL_MERGE_SUBURB__c  + '<br>';
              }
              else{
                  body=body+ '<br>';
              }  
              if(acc.MAIL_MERGE_POSTAL_CODE__c !=null){
                  body=body+  acc.MAIL_MERGE_POSTAL_CODE__c+ '<br>';
              }
              else{
                  body=body+ '<br>';
              }  
              body=body+  '<br>'+acc.TodayDate__c +'<br> Ref:'+acc.COMMUNAL_REFERENCE_NUMBER__c+ '<br><br><table width ="100%" border="1" ><tr><td>';
              body=body+  ' <b><u><p align="center">UNITED JEWISH CAMPAIGN <br>FINANCIAL STATEMENT</p></u></b><br>';
              body=body+  ' Our records indicate your pledge totals and balances as follows<br>';
              body=body+   ' <br>TOTAL DUE: R '+ map_Total.get(acc.id) +'0<br>';
              body=body+  '<table border="0">';
              body=body+    '<tr><td height ="10px"><b>'+CurrentYear +' Total Pledged: </b>R ' +map_PledgeCurrent.get(acc.id); if(map_PledgeCurrent.get(acc.id)>0){body=body+'0';}else{} body=body+ '</td><td><b>'+CurrentYear+ ' Balance:</b> R ' +map_BalCurrent.get(acc.id);if(map_BalCurrent.get(acc.id)>0){body=body+'0';}else{} body=body+ '</td></tr>';                                                                                            
              body=body+    '<tr><td height ="10px"><b>'+String.valueof(CurrentYear-1) +' Total Pledged:</b> R ' +map_PledgeFirst.get(acc.id);if(map_PledgeFirst.get(acc.id)>0){body=body+'0';}else{} body=body+ '</td><td><b>'+String.valueof(CurrentYear-1) + ' Balance: </b>R ' +map_BalFirst.get(acc.id);if(map_BalFirst.get(acc.id)>0){body=body+'0';}else{} body=body+ '</td></tr>';
              body=body+   ' <tr><td><b>'+String.valueof(CurrentYear-2) +' Total Pledged:</b> R ' +map_PledgeSecond.get(acc.id);if(map_PledgeSecond.get(acc.id)>0){body=body+'0';}else{} body=body+ '</td><td><b>'+String.valueof(CurrentYear-2)+ ' Balance:</b>R ' +map_BalSecond.get(acc.id); if(map_BalSecond.get(acc.id)>0){body=body+'0';}else{} body=body+ '</td></tr>';
              body=body+   ' <tr><td><b>'+String.valueof(CurrentYear-3) +' Total Pledged:</b> R ' +map_PledgeThird.get(acc.id);if(map_PledgeThird.get(acc.id)>0){body=body+'0';}else{} body=body+ '</td><td><b>'+String.valueof(CurrentYear-3)+ ' Balance: </b>R '+ map_BalThird.get(acc.id);if(map_BalThird.get(acc.id)>0){body=body+'0';}else{} body=body+ '</td></tr>';
              body=body+   ' <tr><td><b>'+String.valueof(CurrentYear-4)+' Total Pledged:</b> R ' +map_PledgeFourth.get(acc.id);if(map_PledgeFourth.get(acc.id)>0){body=body+'0';}else{} body=body+ '</td><td><b>'+String.valueof(CurrentYear-4)+ ' Balance: </b>R '+ map_BalFourth.get(acc.id);if( map_BalFourth.get(acc.id)>0){body=body+'0';}else{} body=body+ '</td></tr>';
              body=body+   ' </table>';
              
              body=body+    'The above represents a summary of all pledges made within a particular year. Should you require a breakdown, kindly notify us.<br>';
              body=body+    'IF YOU HAVE MADE PAYMENT PLEASE DISREGARD THIS STATEMENT.';
              body=body+   ' </tr></td></table><br><b><u>Debit Orders:</u></b><br>Can be set up via our office. Please speak to Arlene at 021-464-6740.<br><br>';
              body=body+   '<b><u>Cheques:</u></b><br>Can be made out to the United Jewish Campaign and mailed to P.O.Box 4176, Cape Town, 8000.<br>';
              body=body+    '<table width="100%"><tr><td><b><u>Online payment facilities:</u></b><br>Payment can be made at <a href = "https://ujc.org.za/donate/">https://ujc.org.za/donate/</a><br><br>';
              body=body+   '<b><u>EFT: </u></b><br>Bank: Standard Bank <br>A/C No: 070 708 010 <br>Branch Code: 020 909  <br>Ref:'+ acc.COMMUNAL_REFERENCE_NUMBER__c +'</td>';
              body=body+   '<td align ="right" valign="top"><img height="90" width="90" src="https://c.um2.content.force.com/servlet/servlet.ImageServer?id=0154G000006Rmxj&oid=00DD0000000lDkL&lastMod=1564734048000"/></td></tr></table>';
              
               body=body+   'United Jewish Campaign: P.O.Box 4176 Cape Town 8000. Tel: 021-464-6700. Fax: 021-461-5805 blevitt@ctjc.co.za </body></html>';

               
                emailbody = '<html><body style="font-family:Calibri";>'; 
                 
                emailbody =emailbody + ' <p>Dear '+ FirstName;
                emailbody =emailbody +'<table style=" font-family:Calibri,Arial";><tr><td>Thank you for supporting and strengthening our community! Your ongoing commitment and generosity to the Cape Town Jewish community is incredible - especially during this very challenging and trying time. </tr></td>';
                emailbody =emailbody +'<tr><td><br>Please find the attached statement for the balance due from your Campaign pledges by year.<br></tr></td>';
                emailbody =emailbody +'<tr><td><br>Please note that pledges committed via debit order are not included in this statement.<br></tr></td>';
                emailbody =emailbody +'<tr><td><br>Warm wishes<br><br></tr></td>';
                emailbody =emailbody +'<tr><td><img   src="https://c.um2.content.force.com/servlet/servlet.ImageServer?id=015D0000004ERim&oid=00DD0000000lDkL&lastMod=1468576956000" /><br></tr></td>';
                emailbody =emailbody +'<tr><td><br>Barry Levitt <br>Executive Director <br>United Jewish Campaign <br>0214646700 <br>blevitt@ctjc.co.za     <br></td></tr></table></body></html>';
               
               
                
                 email.setHtmlBody(emailbody );
                //email.setPlainTextBody( body );
                 email.setSubject( subject );
                      
            }  
        //Attachment get the data
        
          

        // Create the email attachment and the email
            
            system.debug('**addresses**'+addresses);
            String[] toAddresses = addresses.split(':', 0);
           OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'blevitt@ctjc.co.za'];  
            // Sets the paramaters of the email
            
            if ( owea.size() > 0 ) {
                email.setOrgWideEmailAddressId(owea.get(0).Id);
            }
           
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
              efa.setFileName('UJCStatement.pdf');
           // efa.setBody(b);  
           if(Test.isRunningTest()){
               body='test blob';
           }
            system.debug('***body***'+body);     
            efa.Body = Blob.toPDF(body);
            email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
            email.setSaveAsActivity(true);
            email.setReplyTo('amaneveld@ctjc.co.za');
            email.setToAddresses( toAddresses);
           

            // Sends the email    
            if (!Test.isRunningTest()) {
                Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
            }   
          
            Insert addEmail;
          }      
         }
       }
       update lst_Acc;   
    }        
}    




}