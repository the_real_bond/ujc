/**
 * @description       : 
 * @author            : Lian Bond
 * @group             : 
 * @last modified on  : 03-03-2021
 * @last modified by  : Lian Bond
 * Modifications Log 
 * Ver   Date         Author      Modification
 * 1.0   27-01-2021   Lian Bond   Initial Version
**/
public without sharing class DigitalTelephonController {
    
    private static List<Account> lstAccount = new List<Account>();
    private static List<Recurring_Donation__c> lstPledges = new List<Recurring_Donation__c>();
    private static List<Recurring_Donation__c> lstPastThreeYearsPledgesAll = new List<Recurring_Donation__c>(); //all pledges for the past three years, not just annual pledges
    private static Account cr = null;
    private static User loggedInUser = null;
    private static List<accountWrapper> lstAccountLoggedInUser = new List<accountWrapper>();
    private static List<accountWrapper> lstAccountGeneral = new List<accountWrapper>();
    private static Set<Id> donorIds = new Set<Id>();
    private static Map<Id, Decimal> crCurrentYearAnnualCampaignDonationsTotal = new Map<Id, Decimal>();
    private static Map<Id, Decimal> crPrevYearAnnualCampaignDonationsTotal = new Map<Id, Decimal>();
    private static String currentYearDate;
    private static String previousYearDate;
    private static List<User> users = new List<User>();

    @AuraEnabled
    public static wrapLists getLoggedInUserList() {
    System.debug('DigitalTelephonCallListController.getLoggedInUserList: Start');

    //logged in user deets
    Id loggedInUserId = UserInfo.getUserId();
    users = [SELECT Name, AccountId, Id, IsActive
    FROM User WHERE Profile.Name = 'UJC Telethoner'];
    for (User u : users) {
        if (u.Id == loggedInUserId) {
            loggedInUser = u;
        }
    }
    Account loggedInCommunityRegister = [SELECT Name, Past_Volunteer_Activities__c
    FROM Account WHERE Id =: loggedInUser.AccountId];
    
    System.debug('DigitalTelephonCallListController.getLoggedInUserList: Step 1: Logged in user is ' + loggedInCommunityRegister.Name);
    if (loggedInCommunityRegister.Past_Volunteer_Activities__c == null) {
        throw new AuraHandledException('Only Community Registers with Telethoner in their Volunteer Activities may partake in the Digital Telethon');
        
    } else if (!loggedInCommunityRegister.Past_Volunteer_Activities__c.contains('Telethoner')) {
        throw new AuraHandledException('Only Community Registers with Telethoner in their Volunteer Activities may partake in the Digital Telethon');
    }

    //get the dates
    currentYearDate = ''+Date.today().year();    
    previousYearDate = ''+Date.today().addYears(-1).year();

    System.debug('DigitalTelephonCallListController.getLoggedInUserList: Step 2: years -> curYear = ' +  currentYearDate + ' prev year = ' + previousYearDate);
    ///individual type = 012D00000006jWeIAI'
    //SELECT Last_JC_Canvasser_s__c, RecordTypeId 
    //FROM Account WHERE RecordTypeId = '012D00000006jWeIAI' and Last_JC_Canvasser_s__c != null
    
    lstPledges = [SELECT Donor__c, Canvasser__c, Pledge_Type__c, Pledge_Total__c, Status__c, Campaign_Year__c, LastModifiedDate, Canvasser_Last_Call_Date__c, Follow_Up_Date__c
                
    FROM Recurring_Donation__c 
                WHERE 
                    (Campaign_Year__c =: currentYearDate 
                    OR Campaign_Year__c =: previousYearDate) 
                    AND Pledge_Type__c = 'Annual Campaign' 
                    //AND Canvasser__c =: loggedInUser.AccountId
                ORDER BY CreatedDate DESC];
    
    if (lstPledges.size() == 0) {
        System.debug('DigitalTelephonCallListController.getLoggedInUserList: !!!!!!!!!!WARNING!!!!!!! No pledges found' );
    } else {
        System.debug('DigitalTelephonCallListController.getLoggedInUserList: Pledges found. Number: ' + lstPledges.size() + ' List: ' +  lstPledges);
    }

    for (Recurring_Donation__c rd : lstPledges) {
        if (rd.Campaign_Year__c == currentYearDate) {
            Decimal runningTotal = crCurrentYearAnnualCampaignDonationsTotal.get(rd.Donor__c) == null ? 0 : crCurrentYearAnnualCampaignDonationsTotal.get(rd.Donor__c);
            crCurrentYearAnnualCampaignDonationsTotal.put(rd.Donor__c, runningTotal + rd.Pledge_Total__c);
        } else if (rd.Campaign_Year__c == previousYearDate && rd.Status__c != 'Refused') {
            Decimal runningTotal = crPrevYearAnnualCampaignDonationsTotal.get(rd.Donor__c) == null ? 0 : crPrevYearAnnualCampaignDonationsTotal.get(rd.Donor__c);
            crPrevYearAnnualCampaignDonationsTotal.put(rd.Donor__c, runningTotal + rd.Pledge_Total__c);
        }
    }


    lstAccount = new List<Account>([SELECT 
                                    ID, Salutation, Name, Force_Include_In_General_Call_List__c, Past_Volunteer_Activities__c,Category__c
                                    FROM Account 
                                    WHERE 
                                        DatabaseStatus__c =:'Active' //recordType.Name=:'Individual'
                                        AND RecordType.Name=:'Individual']);

    if (lstAccount.size() == 0) {
        throw new AuraHandledException('No community registers found.');
    }


    for (Account a : lstAccount) {
        //STEP 0: exclude top donors and T&F's
        if (!a.Category__c.contains('Top Donor') && !a.Category__c.contains('Trust & Foundations')) {
            Recurring_Donation__c mostRecentPledge = null;
            if (a.Force_Include_In_General_Call_List__c) {
                a.Force_Include_In_General_Call_List__c = false;
                mostRecentPledge = findMostRecentPledge(a);
                lstAccountGeneral.add(new accountWrapper(a.Name, mostRecentPledge.Canvasser_Last_Call_Date__c, a.Id, mostRecentPledge.Follow_Up_Date__c));
                System.debug('DigitalTelephonCallListController.getLoggedInUserList: ' +  a.Name + ' forced into general call list');
            } else {
                //STEP 1: make sure that CR has ANNUAL pledge last year (with total > 0), but no ANNUAL (with total > 0) pledge this year
                if (checkPledgeTotalsByYear(a)) {
                    //STEP 2: if there is an ANNUAL pledge this year, status must be pending, for example status must not be active or refused
                    if (checkAnnualPledgesForThisYear(a)) {
                        // STEP 3: WE KNOW THAT: step 1 and step 2 have passed meaning, that we can now look for the most recent pledge to 
                        //determine the canvasser and ultimately to which list this account must go, My list or the General list
                        //i.e we use the most recent pledge to determine the most recent canvasser
                        mostRecentPledge = findMostRecentPledge(a);
                        if (mostRecentPledge != null && mostRecentPledge.Canvasser__c == loggedInUser.AccountId) {
                            lstAccountLoggedInUser.add(new accountWrapper(a.Name, mostRecentPledge.Canvasser_Last_Call_Date__c, a.Id, mostRecentPledge.Follow_Up_Date__c));
                        } else if (mostRecentPledge.Canvasser__c == null || !checkIfMostRecentCanvasserIsStillActive(mostRecentPledge.Canvasser__c)) {
                            System.debug('DigitalTelephonCallListController.getLoggedInUserList: ' +  a.Name + ' put into general call list');
                            lstAccountGeneral.add(new accountWrapper(a.Name, mostRecentPledge.Canvasser_Last_Call_Date__c, a.Id, mostRecentPledge.Follow_Up_Date__c));
                        }
                    }
                }
            }
        } else {
            System.debug('DigitalTelephonCallListController.getLoggedInUserList: ' +  a.Name + ' excluded because of category containing Top Donor or Trust & Foundations');
        }
    }
    wrapLists wrapReturn = new wrapLists(lstAccountLoggedInUser, lstAccountGeneral);
    System.debug('DigitalTelephonCallListController.getLoggedInUserList: lstAccountGeneral ' +  wrapReturn.lstAccountGeneral);
    return wrapReturn;

}

public static Boolean checkPledgeTotalsByYear(Account acc) {
    Boolean pass = ((crCurrentYearAnnualCampaignDonationsTotal.get(acc.Id) == 0 || crCurrentYearAnnualCampaignDonationsTotal.get(acc.Id) == null) && crPrevYearAnnualCampaignDonationsTotal.get(acc.Id) > 0);
    if (pass) {
        System.debug(
            'DigitalTelephonCallListController.getLoggedInUserList: ' +  acc.Name + ' passes step 1\n' + 
            'Prev year donation = ' +  crPrevYearAnnualCampaignDonationsTotal.get(acc.Id) + '\n' +
            'this year donation = ' + crCurrentYearAnnualCampaignDonationsTotal.get(acc.Id)
        );
    } else {
        System.debug(
            'DigitalTelephonCallListController.getLoggedInUserList: ' +  acc.Name + ' fails step 1\n' + 
            'Prev year donation (should be >0) = ' +  crPrevYearAnnualCampaignDonationsTotal.get(acc.Id) + '\n' +
            'this year annual pledge donation (should be 0) = ' + crCurrentYearAnnualCampaignDonationsTotal.get(acc.Id)
        );
    }
    return pass;
}

public static Boolean checkIfMostRecentCanvasserIsStillActive(Id accId) {
    try {
        Account telethonerAcc = null;
        for (Account a : lstAccount) {
            telethonerAcc = a.Id == accId ? a : null;
            if (telethonerAcc != null) {
                break;
            }
        }
        User telethonerUser = null;
        for (User u : users) {
            telethonerUser = u.AccountId == accId ? u : null;
            if (telethonerUser != null) {
                break;
            }
        }
        if (telethonerAcc.Past_Volunteer_Activities__c.contains('Telethoner')) {
            if (telethonerUser.IsActive == true) {
                return true;
            } else {
                System.debug('DigitalTelephonCallListController.getLoggedInUserList: User ' + telethonerUser.Name + ' is not active');
                return false;
            }
            
        } else {
            System.debug('DigitalTelephonCallListController.getLoggedInUserList: Cr ' + telethonerAcc.Name + ' does not have telethoner in Volunteer activities ');
            return false;
        }
    } catch (Exception e) {
        System.debug('DigitalTelephonCallListController.getLoggedInUserList: ' + e.getMessage());
        return false;
    }
}


//there should be no annual pledges for this year or if there are, they should be open, ie have no values
public static Boolean checkAnnualPledgesForThisYear(Account acc) {
    for (Recurring_Donation__c rd : lstPledges) {
        if (rd.Donor__c == acc.Id) {
            if (rd.Campaign_Year__c == currentYearDate && rd.Status__c != 'Pending') {
                System.debug('DigitalTelephonCallListController.getLoggedInUserList: ' +  acc.Name + ' fails step 2 with ' + rd);
                return false; //if return equals exlude then we dont want to include this user in any list
            } else {
                System.debug('DigitalTelephonCallListController.getLoggedInUserList: ' +  acc.Name + ' passes step 2 with ' + rd);
            }
        }
        
    }
    return true;
}

//we use the most recent pledge to determine the most recent canvasser
public static Recurring_Donation__c findMostRecentPledge(Account acc) {
    for (Recurring_Donation__c rd : lstPledges) {
        if (rd.Donor__c == acc.Id) {
            if (!(rd.Campaign_Year__c == currentYearDate && (rd.Status__c == 'Refused' || rd.Status__c == 'File per Campaign'))) {
                System.debug('DigitalTelephonCallListController.getLoggedInUserList: ' +  acc.Name + ' passes step 3');
                return rd;
                // rd.Id
            }
        }
    }
    System.debug('DigitalTelephonCallListController.getLoggedInUserList: WARNING NO PLEDGE FOUND ' +  acc.Name);
    return null;
}

public static Recurring_Donation__c findMostRecentPledgeThisYear(Account acc) {
    for (Recurring_Donation__c rd : lstPledges) {
        
            if ((rd.Campaign_Year__c == currentYearDate && rd.Status__c == 'Pending')) {
                System.debug('DigitalTelephonCallListController.findMostRecentPledgeThisYear ' + acc.Name + ' most recent pledge ' + rd);
                return rd;
                // rd.Id
            }
        
    }
    System.debug('DigitalTelephonCallListController.getLoggedInUserList: WARNING NO PLEDGE FOUND ' +  acc.Name);
    return null;
}


@AuraEnabled
public static chartDataWrapper getChartData() {
    List<User> telethonersUsers = [SELECT AccountId FROM User WHERE Profile.Name = 'UJC Telethoner'];
    Map<Id, User> mapUsersAndAccIds = new Map<Id, User>();
    for (User u : telethonersUsers) {
        mapUsersAndAccIds.put(u.AccountId, u);
    }
    List<Account> telethonersAccs = [SELECT Id, Name, Past_Volunteer_Activities__c
    FROM Account WHERE Id IN :mapUsersAndAccIds.keySet()];
    Map<Id, Decimal> mapAccountIdAndPledgeTotal = new Map<Id, Decimal>();
    for (Account a : telethonersAccs) {
        mapAccountIdAndPledgeTotal.put(a.Id, 0.0);
    }
    // Map<Id, String> mapAccountIdAndAccountName = new Map<Id, String>();
    // for (Account a : telethonersAccs) {
    //     mapAccountIdAndAccountName.put(a.Id, a.Name);
    // }

    List<Recurring_Donation__c> pledges = [SELECT Pledge_Total__c, Canvasser__c FROM Recurring_Donation__c WHERE Canvasser__c IN :mapAccountIdAndPledgeTotal.keySet()];

    for (Recurring_Donation__c rc : pledges) {
        mapAccountIdAndPledgeTotal.put(rc.Canvasser__c, mapAccountIdAndPledgeTotal.get(rc.Canvasser__c)+rc.Pledge_Total__c);
    }

    List<String> labels = new List<String>();
    List<Decimal> data = new List<Decimal>();

    for (Account a : telethonersAccs) {
        labels.add(a.Name);
        data.add(mapAccountIdAndPledgeTotal.get(a.Id));
    }

    return new chartDataWrapper(labels, data);
}


// This is our wrapper/container class. A container class is a class, a data structure, or an abstract data type whose instances are collections of other objects. In this example a wrapper class contains both the standard salesforce object Account and a Boolean value
public class chartDataWrapper {
    @AuraEnabled public List<String> labels {get; set;}
    @AuraEnabled public List<Decimal> data {get; set;} 
    
    public chartDataWrapper(List<String> labels, List<Decimal> data) {
           
        this.labels = labels;
        this.data = data;
    }
}




/**
* @description All code below was borrowed and modified FROM Sean Parkers original code for the controller for the campaign card visualforce page.
* @author Lian Bond | 17-02-2021 
* @param Id crId 
* @param Boolean userListSelected ---> This boolean indicates whether the person selected was selected from My List or The General list
* @return communityRegister 
**/
@AuraEnabled
public static communityRegister crCampaignCardData(Id crId, Boolean userListSelected) {
     //logged in user deets
     Id loggedInUserId = UserInfo.getUserId();
     loggedInUser = [SELECT AccountId 
     FROM User WHERE Id =: loggedInUserId];
     Account loggedInCommunityRegister = [SELECT Name 
     FROM Account WHERE Id =: loggedInUser.AccountId];

    communityRegister com = null;

    Account a = ([SELECT 
            Account__c,Salutation,Name, MAIL_MERGE_ADDRESS__c,MAIL_MERGE_SUBURB__c,MAIL_MERGE_PROVINCE__c,MAIL_MERGE_COUNTRY__c,MAIL_MERGE_POSTAL_CODE__c,Canvass_Info__c,
            Description,Spouse_Name__r.name,Household_Main_Member__c,Date_of_Birth__c,Business_Name__c,Residential_Postal_Address_RP__c,
            Suburb_RP__c, Postal_Code_RP__c,Business_Address_BS__c, Suburb_BS__c, Postal_Code_BS__c, Business_Postal_Address_BP__c,
            Suburb_BP__c,Notes__c,Spouse_Name__c, Residential_Telephone__c, Residential_Fax__c, Cellphone__c,  Email_Personal__c, Email_Business__c, Category__c, Occupation__c, Qualification__c, 
            Special_Attention__c,phone,Individual_Type__c,NonJC_Pledge_History__c,BANK_NAME__c, Account_Number__c, Credit_Card_CVV__c, BANK_CARD_EXPIRY__c, id,Children_at_Herzlia__c, Retired__c,  Age__c, DatabaseStatus__c,Total_Pledge_History__c,
            X2019_Percentage__c,X2019_BuTruSp_Total__c,
            X2019_JC_Pledge_Total__c,X2018_JC_Pledge_Total__c,X2016_JC_Pledge_Total__c,X2017_JC_Pledge_Total__c , X2015_JC_Pledge_Total__c
            
            FROM Account WHERE DatabaseStatus__c =:'Active' AND id=: crid][0]);
    
    //get the dates
    currentYearDate = ''+Date.today().year();    
    previousYearDate = ''+Date.today().addYears(-1).year();
    String twoYearsAgoYearDate = ''+Date.today().addYears(-2).year();

    lstPastThreeYearsPledgesAll = new List<Recurring_Donation__c>([SELECT id, Specified_Amount__c,  Campaign_Year__c,Pledge_Type__c,Canvas_Method__c,Start_Date__c,Canvassers_All__c,
                                                        Total_Outstanding__c,Total_Cancelled__c,Donor__c,Method_of_Payment__c, Installment_Total__c,Welfare_Total__c,
                                                        UCF_Education_Total__c,UCF_Communal_Total__c,UCF_Total__c,Pledge_Total__c,IUA_Total__c,Campaign_Info__c,Status__c 
                                                        FROM Recurring_Donation__c WHERE 
                                                        (Campaign_Year__c =: currentYearDate 
                                                            OR Campaign_Year__c =: previousYearDate OR Campaign_Year__c =: twoYearsAgoYearDate) 
                                                        AND  Donor__c  = :a.Id ORDER BY Campaign_Year__c DESC]); 

    
    lstPledges = [SELECT Donor__c, Canvasser__c, Pledge_Type__c, Pledge_Total__c, Status__c, Campaign_Year__c, Annual_Pledge_Remarks__c
                
    FROM Recurring_Donation__c 
                WHERE 
                    (Campaign_Year__c =: currentYearDate) 
                    AND Pledge_Type__c = 'Annual Campaign' AND Donor__c  = :a.Id AND Status__c = 'Pending'
                    //AND Canvasser__c =: loggedInUser.AccountId
                ORDER BY CreatedDate DESC];
    
    
    com = new communityRegister();
    
    //this years date
    com.currentYearDate = currentYearDate;

    //current years annual pledge
    com.mostRecentPledge = findMostRecentPledgeThisYear(a);
    if (com.mostRecentPledge == null) {
        com.mostRecentPledge = createAnnualPledgeForThisYear(crId, loggedInUser.AccountId, userListSelected);
    }
    com.loggedInUser = loggedInCommunityRegister.Name;
    com.Communal_Reference_Number =a.Account__c;
    com.Salutation =a.Salutation;
    com.Name =a.Name; 
    com.MAIL_MERGE_ADDRESS =a.MAIL_MERGE_ADDRESS__c;
    com.MAIL_MERGE_SUBURB =a.MAIL_MERGE_SUBURB__c;
    com.MAIL_MERGE_PROVINCE =a.MAIL_MERGE_PROVINCE__c;
    com.MAIL_MERGE_COUNTRY =a.MAIL_MERGE_COUNTRY__c;
    com.MAIL_MERGE_POSTAL_CODE =a.MAIL_MERGE_POSTAL_CODE__c;
    com.Suburb_RP =a.Suburb_RP__c; 
    com.Postal_Code_RP =a.Postal_Code_RP__c;
    com.Business_Address_BS =a.Business_Address_BS__c; 
    com.Suburb_BS =a.Suburb_BS__c; 
    com.Postal_Code_BS =a.Postal_Code_BS__c; 
    com.Business_Postal_Address_BP =a.Business_Postal_Address_BP__c;
    com.Suburb_BP =a.Suburb_BP__c;
    com.Notes2014 =a.Notes__c;
    com.Description = a.Canvass_Info__c;
    com.Spouse_Name =a.Spouse_Name__r.name; 
    com.Residential_Telephone =a.Residential_Telephone__c; 
    com.Residential_Fax =a.Residential_Fax__c; 
    com.Cellphone =a.Cellphone__c;  
    com.Email_Personal =a.Email_Personal__c; 
    com.Email_Business =a.Email_Business__c; 
    com.Category =a.Category__c; 
    com.Occupation =a.Occupation__c; 
    com.Qualification =a.Qualification__c; 
    com.Special_Attention =a.Special_Attention__c;  
    com.IndividualType =a.Individual_Type__c; 
    com.Children_at_Herzlia =string.valueof(a.Children_at_Herzlia__c); 
    com.Retired =a.Retired__c;  
    com.Age =string.valueof(a.Age__c); 
    com.DatabaseStatus =a.DatabaseStatus__c;
    com.Total_NONJC_History = a.NonJC_Pledge_History__c;  
    com.Total_Pledge_History = a.Total_Pledge_History__c;
    
    com.PrevYear_PercentageIncrease = a.X2019_Percentage__c ;
    com.PrevYear_BuTruSp_Total =a.X2019_BuTruSp_Total__c;
    
    com.PrevY1_Pledge_Total =a.X2019_JC_Pledge_Total__c;
    com.PrevY2_Pledge_Total =a.X2018_JC_Pledge_Total__c ;
    com.PrevY3_Pledge_Total =a.X2017_JC_Pledge_Total__c ; 
    com.PrevY4_Pledge_Total =a.X2016_JC_Pledge_Total__c;
    com.PrevY5_Pledge_Total =a.X2015_JC_Pledge_Total__c;

    com.BANK_NAME =a.BANK_NAME__c; 
    com.Account_Number =a.Account_Number__c; 
    com.Credit_Card_CVV =a.Credit_Card_CVV__c; 
    com.BANK_CARD_EXPIRY =a.BANK_CARD_EXPIRY__c;

    com.Business_Name = a.Business_Name__c;
    com.Residential_Postal_Address_RP = a.Residential_Postal_Address_RP__c; 
    
    com.Date_of_Birth = string.valueof(a.Date_of_Birth__c);
    com.Household_Main_Member = string.valueof(a.Household_Main_Member__c);
    com.BusinessPhone = string.valueof(a.phone); 

    List<Pledge> lstPledgeWrapper = new List<Pledge>();
    for(Recurring_Donation__c r:lstPastThreeYearsPledgesAll ){             
        if(r.Donor__c == a.id){
        Pledge pledge = new Pledge();             
        pledge.Campaign_Info =r.Campaign_Info__c;
        pledge.Status=r.Status__c;
        pledge.Pledge_Total=r.Pledge_Total__c;
        pledge.IUA_Total=r.IUA_Total__c;
        pledge.UCF_Communal_Total=r.UCF_Communal_Total__c;
        pledge.UCF_Education_Total =r.UCF_Education_Total__c;
        pledge.Welfare_Total=r.Welfare_Total__c;
        pledge.Installment_Total=r.Installment_Total__c;
        pledge.Method_of_Payment=r.Method_of_Payment__c;
        pledge.Total_Cancelled=r.Total_Cancelled__c;
        pledge.Total_Outstanding =r.Total_Outstanding__c;
        pledge.Specified_Amount =r.Specified_Amount__c;
        pledge.Canvassers_All =r.Canvassers_All__c;
        pledge.CamapaignYear  = string.valueof(r.Start_Date__c);
        pledge.Canvassmethod = r.Canvas_Method__c; 
        pledge.PledgeType = r.Pledge_Type__c;  
        lstPledgeWrapper.Add(pledge); 
        }
    }
    com.lstPledge = lstPledgeWrapper;     
    
    System.debug('DigitalTelephonCallListController.crCampaignCardData: returning ' + com);
    return com;
}

public static Recurring_Donation__c createAnnualPledgeForThisYear(Id crId, Id loggedInCanvasser, Boolean userListSelected) {
    Recurring_Donation__c rd = new Recurring_Donation__c();
    rd.Donor__c = crId;
    rd.Method_of_Payment__c = 'OP';
    rd.Campaign_Year__c = '2021';
    rd.Campaign__c = 'JC';
    rd.Start_Date__c = Date.today();
    rd.Number_of_Installments__c = 1;
    rd.Status__c = 'Pending';
    rd.Pledge_Type__c = 'Annual Campaign';
    rd.Canvas_Method__c = 'Telethon - Volunteers'; 
    rd.Do_Not_Ack__c = true;
    rd.Do_Not_Send_Email__c = true;
    //rd.Canvasser__c = loggedInCanvasser;
    rd.Canvasser__c = userListSelected == true ? loggedInCanvasser : null; //if user was selected from My List then we want to set canvasser to me,
    //if user was selected from the general list we set canvasser to null because this person does not belong to us until we've actually closed and completed the call
    insert rd;
    return rd;
}

// This is our wrapper/container class. A container class is a class, a data structure, or an abstract data type whose instances are collections of other objects. In this example a wrapper class contains both the standard salesforce object Account and a Boolean value
public class wrapLists {
    @AuraEnabled public List<accountWrapper> lstAccountLoggedInUser {get; set;}
    @AuraEnabled public List<accountWrapper> lstAccountGeneral {get; set;}

    //This is the contructor method. When we create a new wrapAccount object we pass a Account that is set to the acc property. We also set the SELECTed value to false
    public wrapLists(List<accountWrapper> lstLoggedInUser, List<accountWrapper> lstGeneral) {
        lstAccountLoggedInUser = lstLoggedInUser;
        lstAccountGeneral = lstGeneral;
    }
}

// This is our wrapper/container class. A container class is a class, a data structure, or an abstract data type whose instances are collections of other objects. In this example a wrapper class contains both the standard salesforce object Account and a Boolean value
public class accountWrapper {
    @AuraEnabled public String Name {get; set;}
    @AuraEnabled public String latestPledgeLastModifiedDate {get; set;} 
    @AuraEnabled public String latestPledgeFollowUpDate {get; set;} 
    @AuraEnabled public Id Id {get; set;} 
    
    public accountWrapper(String Name, Datetime latestPledgeLastModifiedDate, Id Id, Date latestPledgeFollowUpDate) {
           
        this.Name = Name;
        this.latestPledgeLastModifiedDate = latestPledgeLastModifiedDate == null ? 'Not called yet in ' + currentYearDate : latestPledgeLastModifiedDate.format();
        this.Id = Id;
        this.latestPledgeFollowUpDate = latestPledgeFollowUpDate == null ? 'No follow up date set for ' + currentYearDate : latestPledgeFollowUpDate.format();
    }
}


public class Pledge{

    @AuraEnabled public String Campaign_Info {get;set;}
    @AuraEnabled public String Status {get;set;}
    @AuraEnabled public Decimal Pledge_Total {get;set;}
    @AuraEnabled public Decimal IUA_Total {get;set;}
    @AuraEnabled public Decimal UCF_Communal_Total {get;set;}
    @AuraEnabled public Decimal UCF_Education_Total {get;set;}
    @AuraEnabled public Decimal Welfare_Total {get;set;}
    @AuraEnabled public Decimal Installment_Total {get;set;}
    @AuraEnabled public String Method_of_Payment {get;set;}
    @AuraEnabled public Decimal Total_Cancelled {get;set;}
    @AuraEnabled public Decimal Total_Outstanding {get;set;}
    @AuraEnabled public Decimal Specified_Amount  {get;set;}
    @AuraEnabled public String Canvassers_All {get;set;}
    @AuraEnabled public String CamapaignYear {get;set;}
    @AuraEnabled public String Canvassmethod {get;set;}
    @AuraEnabled public String PledgeType {get;set;}
    
    
}


public class communityRegister{

    @AuraEnabled public String s {get;set;}
    @AuraEnabled public String  Communal_Reference_Number {get;set;}
    @AuraEnabled public String  Salutation {get;set;}
    @AuraEnabled public String  Name {get;set;} 
    @AuraEnabled public String  MAIL_MERGE_ADDRESS {get;set;}
    @AuraEnabled public String  MAIL_MERGE_SUBURB {get;set;}
    @AuraEnabled public String MAIL_MERGE_PROVINCE {get;set;}
    @AuraEnabled public String MAIL_MERGE_COUNTRY {get;set;}
    @AuraEnabled public String  MAIL_MERGE_POSTAL_CODE {get;set;}
    @AuraEnabled public String Suburb_RP {get;set;} 
    @AuraEnabled public String Postal_Code_RP {get;set;}
    @AuraEnabled public String Business_Address_BS {get;set;} 
    @AuraEnabled public String Suburb_BS {get;set;} 
    @AuraEnabled public String Postal_Code_BS {get;set;} 
    @AuraEnabled public String Business_Postal_Address_BP {get;set;}
    @AuraEnabled public String Suburb_BP {get;set;}
    @AuraEnabled public String Notes2014 {get;set;} 
    
    @AuraEnabled public String Description {get;set;}
    @AuraEnabled public String Spouse_Name {get;set;} 
    @AuraEnabled public String Residential_Telephone {get;set;}
    @AuraEnabled public String Residential_Fax {get;set;} 
    @AuraEnabled public String Cellphone {get;set;}  
    @AuraEnabled public String Email_Personal {get;set;} 
    @AuraEnabled public String Email_Business {get;set;} 
    @AuraEnabled public String Category {get;set;}
    @AuraEnabled public String Occupation {get;set;} 
    @AuraEnabled public String Qualification {get;set;}
    @AuraEnabled public String Special_Attention {get;set;}  
    @AuraEnabled public String IndividualType {get;set;} 
    @AuraEnabled public String Children_at_Herzlia {get;set;} 
    @AuraEnabled public String Retired {get;set;}  
    @AuraEnabled public String Age {get;set;} 
    @AuraEnabled public String DatabaseStatus {get;set;}
    @AuraEnabled public Decimal Total_Pledge_History{get;set;} 
    
    @AuraEnabled public Decimal Total_NONJC_History{get;set;}
    
    @AuraEnabled public Decimal PrevYear_PercentageIncrease {get;set;} 
    @AuraEnabled public Decimal PrevYear_BuTruSp_Total {get;set;}
    
    @AuraEnabled public Decimal PrevY1_Pledge_Total {get;set;} 
    @AuraEnabled public Decimal PrevY2_Pledge_Total {get;set;}
    @AuraEnabled public Decimal PrevY3_Pledge_Total {get;set;}
    @AuraEnabled public Decimal PrevY4_Pledge_Total {get;set;}
    @AuraEnabled public Decimal PrevY5_Pledge_Total {get;set;}
    
     
     
    
    @AuraEnabled public String BusinessPhone {get;set;}
    
    
    @AuraEnabled public String BANK_NAME {get;set;}
    @AuraEnabled public String Account_Number {get;set;} 
    @AuraEnabled public String Credit_Card_CVV {get;set;} 
    @AuraEnabled public String BANK_CARD_EXPIRY {get;set;}
    @AuraEnabled public String Residential_Postal_Address_RP {get;set;}
    
    @AuraEnabled public String Business_Name {get;set;}
    @AuraEnabled public String Date_of_Birth {get;set;}
    @AuraEnabled public String Household_Main_Member {get;set;}
    
    //most recent annual pledge
    @AuraEnabled public Recurring_Donation__c mostRecentPledge {get;set;}
    @AuraEnabled public String mostRecentPledgeRemarks {get;set;}
    

    @AuraEnabled public String loggedInUser {get;set;}
    
    @AuraEnabled public List<Pledge> lstPledge {get;set;}

    @AuraEnabled public String currentYearDate {get;set;}
    
}   

@AuraEnabled
public static void addRemarkToPledge(Id recordId, String remark){
    try {
        Recurring_Donation__c rd = [SELECT Id, Annual_Pledge_Remarks__c FROM Recurring_Donation__c WHERE Id =: recordId][0];
        rd.Annual_Pledge_Remarks__c = remark;
        update rd;
    } catch (Exception e) {
        throw new AuraHandledException(e.getMessage());
    }
}
    

    // public static void getOpenPoolList() {
    //     lstAccount = new List<Account>([SELECT 
    //         Account__c,Salutation,Name, MAIL_MERGE_ADDRESS__c,MAIL_MERGE_SUBURB__c,MAIL_MERGE_PROVINCE__c,MAIL_MERGE_COUNTRY__c,MAIL_MERGE_POSTAL_CODE__c,
    //         Description,Spouse_Name__r.name,Household_Main_Member__c,Date_of_Birth__c,Business_Name__c,Residential_Postal_Address_RP__c,
    //         Suburb_RP__c, Postal_Code_RP__c,Business_Address_BS__c, Suburb_BS__c, Postal_Code_BS__c, Business_Postal_Address_BP__c,
    //         Suburb_BP__c,Notes__c,Spouse_Name__c, Residential_Telephone__c, Residential_Fax__c, Cellphone__c,  Email_Personal__c, Email_Business__c, Category__c, Occupation__c, Qualification__c, 
    //         Special_Attention__c,phone,Individual_Type__c,NonJC_Pledge_History__c,BANK_NAME__c, Account_Number__c, Credit_Card_CVV__c, BANK_CARD_EXPIRY__c, id,Children_at_Herzlia__c, Retired__c,  Age__c, DatabaseStatus__c,Total_Pledge_History__c,
            
    //         X2019_Percentage__c,X2019_BuTruSp_Total__c,
    //         X2019_JC_Pledge_Total__c,X2018_JC_Pledge_Total__c,X2016_JC_Pledge_Total__c,X2017_JC_Pledge_Total__c , X2015_JC_Pledge_Total__c
            
            
    //         
    // /FROM Account WHERE DatabaseStatus__c =:'Active' AND id=: crid AND X2020_JC_Pledge_Total__c=:0 ]);
    // }


}