/**
 * @description       : This class is the 2nd part of receipt thank yous. First Batch_RequestReceiptthankyous runs then this class.
 *                      The difference between the two is that Batch_RequestReceiptthankyous sets a checkbox on the account
 *                      related to a receipt. Then when Batch_SubmiReceiptthankyous runs it only takes receipts where their account
 *                      has this checkbox set into consideration
 * @author            : Sean Parker
 * @group             : CloudSmiths
 * @last modified on  : 04-11-2020
 * @last modified by  : Lian Bond
 * Modifications Log 
 * Ver   Date         Author      Modification
 * 1.0   2015         Sean Parker Initial Version
 * 1.1   2020-04-11   Lian Bond   Fixed a bug that caused receipt thank yous not to be sent. The issue was that the
 *                                Batch Submit class   
**/
global class Batch_SubmitReceiptthankyou implements Database.Batchable<sObject>,Schedulable, Database.AllowsCallouts, Database.Stateful{
   
    public boolean  isTestRunning=false;
    public static boolean DoStop = false;
     
    global Batch_SubmitReceiptthankyou(Boolean TestRun){
        this.isTestRunning = TestRun;
    }
    global Batch_SubmitReceiptthankyou(){
        this.isTestRunning = false;
    }
   
    global Database.QueryLocator start(Database.BatchableContext BC){ 
        Date todayDate = system.Today();       
        return Database.getQUeryLocator( [SELECT id,Email_Thank_you_letter__c FROM Account where Email_Receipt_Thank_you_letter__c =: true ] );
   }

    global void execute(Database.BatchableContext BC, List<sObject> scope){
       Date todayDate = system.Today();
       Set<String> set_Receipt = new Set<String>();

       for(sobject var : scope){
           set_Receipt.add(var.id);
       }
     //  List<Receipt__c> Lst_RecDon = new List<Receipt__c>([SELECT id,Donor__c,Today_Date__c,Pledge_Total__c,Date_Approved__c,Approved__c FROM Receipt__c Where Donor__c in: set_Receipt AND Date_Approved__c >=: todayDate-7 AND Approved__c =: True AND  Pledge_Total__c > 0 AND Pledge_Total__c < 18000 AND Do_Not_Ack__c =:false  ]);
     //                                                                                                                                                                                                 where Receipt_Date__c >=: todayDate-7 AND Payment_Status__c = 'Receipted' AND  Total_Receipt_Amount__c > 0 AND Pledge__r.Method_of_Payment__c = 'OP' AND Pledge__r.Campaign__c = 'JC' AND Thank_you_Sent__c =: false                                             
       List<Receipt__c> Lst_RecDon = new List<Receipt__c>([SELECT id,Donor__c,TodayDate__c,Total_Receipt_Amount__c,Thank_you_Sent__c,Receipt_Date_Email__c FROM Receipt__c Where Donor__c in: set_Receipt AND Receipt_Date__c >=: todayDate-7 AND Payment_Status__c = 'Receipted' AND  Total_Receipt_Amount__c > 0 AND Pledge__r.Method_of_Payment__c ='OP' AND Pledge__r.Campaign__c = 'JC' AND Thank_you_Sent__c = false]);
       System.debug('number of RecDon ' + Lst_RecDon.size());
        PledgeStatementEmailUtility.Receiptthankyouemail(Lst_RecDon,false);
        
    }

    global void finish(Database.BatchableContext BC){
         
    }
    
    global void execute(SchedulableContext sc)
    {
        Batch_SubmitReceiptthankyou PostBatch = new Batch_SubmitReceiptthankyou();
        database.executebatch(PostBatch ,10 );
    }
}