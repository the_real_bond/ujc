/**
 * @description       : This class is the 1st part of receipt thank yous. This class runs first and sets the records to be used by
 *                      Batch_SubmitReceiptthankyous
 *                      The difference between the two is that Batch_RequestReceiptthankyous sets a checkbox on the account
 *                      related to a receipt. Then when Batch_SubmiReceiptthankyous runs it only takes receipts where their account
 *                      has this checkbox set into consideration
 * @author            : Sean Parker
 * @group             : CloudSmiths
 * @last modified on  : 04-11-2020
 * @last modified by  : Lian Bond
 * Modifications Log 
 * Ver   Date         Author      Modification
 * 1.0   2015         Sean Parker Initial Version
 * 1.1   2020-04-11   Lian Bond   Fixed a bug that caused receipt thank yous not to be sent. 
 *                                The issue was that this class exluded receipts where payment method was OP and DO, but it
 *                                should actually include receipts where the pledge's payment method = DO
**/

global class Batch_RequestReceiptthankyou implements Database.Batchable<sObject>,Schedulable, Database.AllowsCallouts, Database.Stateful{
   
    public boolean  isTestRunning=false;
    public static boolean DoStop = false; 
    
    
    global Database.QueryLocator start(Database.BatchableContext BC){ 
        Date todayDate = system.Today();
        
        //return Database.getQUeryLocator( [SELECT id,Donor__c,TodayDate__c,Total_Receipt_Amount__c FROM Receipt__c where Receipt_Date__c >=: todayDate-7 AND Payment_Status__c = 'Receipted' AND  Total_Receipt_Amount__c > 0 AND Method_of_Payment__c !='OP' AND Method_of_Payment__c !='DO' AND Thank_you_Sent__c =: false ] );
   		return Database.getQUeryLocator( [SELECT id,Donor__c,TodayDate__c,Total_Receipt_Amount__c FROM Receipt__c where Receipt_Date__c >=: todayDate-7 AND Payment_Status__c = 'Receipted' AND  Total_Receipt_Amount__c > 0 AND Pledge__r.Method_of_Payment__c = 'OP' AND Pledge__r.Campaign__c = 'JC' AND Thank_you_Sent__c = false ] );
   
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){
       
       Set<String> set_Don = new Set<String>();

       for(sobject var : scope){
           set_Don.add(var.id);
       }
       
        List<Receipt__c> Lst_RecDon = new List<Receipt__c>([SELECT id,Donor__c,TodayDate__c,Total_Receipt_Amount__c,Thank_you_Sent__c,Receipt_Date_Email__c    FROM Receipt__c Where id in: set_Don ]);
        system.debug('**Batch_RequestReceiptthankyou.Execute: Number of Receipts for which we will set the checkbox on their accounts: '+Lst_RecDon.size());
        PledgeStatementEmailUtility.Receiptthankyouemail(Lst_RecDon,True);
        
    }

    global void finish(Database.BatchableContext BC){
         
    }
    
    global void execute(SchedulableContext sc)
    {
        Batch_RequestReceiptthankyou PostBatch = new Batch_RequestReceiptthankyou();
        database.executebatch(PostBatch ,10 );
    }
}