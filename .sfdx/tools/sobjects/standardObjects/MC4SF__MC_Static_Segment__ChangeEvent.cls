// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class MC4SF__MC_Static_Segment__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global MC4SF__MC_List__c MC4SF__MC_List__c;
    global String MC4SF__Created_Date__c;
    global String MC4SF__Last_Reset__c;
    global String MC4SF__Last_Update__c;
    global Double MC4SF__MailChimp_ID__c;
    global Double MC4SF__Member_Count__c;

    global MC4SF__MC_Static_Segment__ChangeEvent () 
    {
    }
}