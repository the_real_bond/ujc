// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__CnP_Auto_Responder_Settings_Options__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global String CnP_PaaS__Amount_Value__c;
    global String CnP_PaaS__Answer__c;
    global String CnP_PaaS__Campaign_Value__c;
    global String CnP_PaaS__Campaign__c;
    global String CnP_PaaS__Checkout_page_WID__c;
    global CnP_PaaS__CnP_Auto_Responder__c CnP_PaaS__CnP_Auto_Responder_Settings__c;
    global String CnP_PaaS__Option_Type__c;
    global String CnP_PaaS__Payment_Amount__c;
    global String CnP_PaaS__Question_Options__c;
    global String CnP_PaaS__Question__c;
    global String CnP_PaaS__SKU_Value__c;
    global String CnP_PaaS__SKU__c;
    global String CnP_PaaS__WID_value__c;

    global CnP_PaaS__CnP_Auto_Responder_Settings_Options__ChangeEvent () 
    {
    }
}