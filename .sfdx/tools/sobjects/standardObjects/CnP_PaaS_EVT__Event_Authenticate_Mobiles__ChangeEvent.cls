// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS_EVT__Event_Authenticate_Mobiles__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global String CnP_PaaS_EVT__Account_Name__c;
    global String CnP_PaaS_EVT__Account_Number__c;
    global Datetime CnP_PaaS_EVT__End_Date__c;
    global String CnP_PaaS_EVT__Mobile_Status__c;
    global Datetime CnP_PaaS_EVT__Start_Date__c;
    global String CnP_PaaS_EVT__User_id__c;
    global String CnP_PaaS_EVT__Verification_With__c;
    global CnP_PaaS_EVT__Event__c CnP_PaaS_EVT__c_p_Event__c;

    global CnP_PaaS_EVT__Event_Authenticate_Mobiles__ChangeEvent () 
    {
    }
}