// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CloudingoAgent__CldIn__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global String CloudingoAgent__P1__c;
    global String CloudingoAgent__P2__c;
    global String CloudingoAgent__P3__c;
    global String CloudingoAgent__P4__c;
    global String CloudingoAgent__P5__c;
    global String CloudingoAgent__R1__c;
    global String CloudingoAgent__T1__c;
    global Double CloudingoAgent__stat__c;
    global Double CloudingoAgent__wsm__c;

    global CloudingoAgent__CldIn__ChangeEvent () 
    {
    }
}