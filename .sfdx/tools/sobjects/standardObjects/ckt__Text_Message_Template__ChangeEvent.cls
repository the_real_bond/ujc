// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class ckt__Text_Message_Template__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Boolean ckt__Available_For_Use__c;
    global String ckt__Description__c;
    global ckt__Text_Message_Template_Folder__c ckt__Folder__c;
    global String ckt__Template_Unique_Name__c;
    global String ckt__Text_Message_Body__c;

    global ckt__Text_Message_Template__ChangeEvent () 
    {
    }
}