// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__CnP_Auto_Responder_Sending_Details__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global String CnP_PaaS__Application_Name__c;
    global CnP_PaaS__CnP_Auto_Responder__c CnP_PaaS__CnP_Auto_Responder_Settings__c;
    global CnP_PaaS__CnP_Designer__c CnP_PaaS__CnP_Designer__c;
    global CnP_PaaS__CnP_Transaction__c CnP_PaaS__CnP_Transaction__c;
    global Contact CnP_PaaS__Contact__c;
    global String CnP_PaaS__No_Of_Times__c;
    global Datetime CnP_PaaS__Sended_Date__c;
    global String CnP_PaaS__Status__c;

    global CnP_PaaS__CnP_Auto_Responder_Sending_Details__ChangeEvent () 
    {
    }
}