// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class MC4SF__MC_Query_Filter__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global MC4SF__MC_Query__c MC4SF__MC_Query__c;
    global Double MC4SF__Display_Order__c;
    global String MC4SF__Error_Message__c;
    global String MC4SF__Field_Name__c;
    global String MC4SF__Field_Type__c;
    global String MC4SF__Object_Name__c;
    global String MC4SF__Operator__c;
    global String MC4SF__Value__c;

    global MC4SF__MC_Query_Filter__ChangeEvent () 
    {
    }
}