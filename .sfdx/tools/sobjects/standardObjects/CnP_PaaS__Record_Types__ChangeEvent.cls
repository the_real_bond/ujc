// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__Record_Types__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global String CnP_PaaS__Account_Record_type__c;
    global String CnP_PaaS__Account_WID_Condition__c;
    global String CnP_PaaS__Account_WID__c;
    global Boolean CnP_PaaS__Account__c;
    global String CnP_PaaS__Assign_Task__c;
    global String CnP_PaaS__Campaign_Condition__c;
    global String CnP_PaaS__Campaign_Sku__c;
    global String CnP_PaaS__Campaign__c;
    global Campaign CnP_PaaS__Campaign_lp__c;
    global Boolean CnP_PaaS__Campaigns__c;
    global String CnP_PaaS__Condition__c;
    global Boolean CnP_PaaS__Connect_Setting__c;
    global Contact CnP_PaaS__ContactId_Task__c;
    global String CnP_PaaS__Contact_Id__c;
    global String CnP_PaaS__Contact_Name_Task__c;
    global Boolean CnP_PaaS__Contact_Role__c;
    global String CnP_PaaS__Contact_role_condition__c;
    global String CnP_PaaS__Contact_role_name__c;
    global Boolean CnP_PaaS__Contacts__c;
    global String CnP_PaaS__Custom_Answer__c;
    global Boolean CnP_PaaS__Custom_Questions__c;
    global String CnP_PaaS__Custom_Text__c;
    global String CnP_PaaS__Default_Items_Edit_Option_XML__c;
    global String CnP_PaaS__Donation_Campaign__c;
    global Boolean CnP_PaaS__Donation_Check__c;
    global String CnP_PaaS__Donation_Pay__c;
    global String CnP_PaaS__Donation_Quantity__c;
    global String CnP_PaaS__Donation_SKU__c;
    global String CnP_PaaS__Email_Task__c;
    global String CnP_PaaS__First_Name_Separator__c;
    global String CnP_PaaS__Firstname_Prefix__c;
    global String CnP_PaaS__Ledger_Account_Name__c;
    global Campaign CnP_PaaS__Ledger_Campaign__c;
    global String CnP_PaaS__Ledger_Condition__c;
    global Boolean CnP_PaaS__Ledgers__c;
    global Boolean CnP_PaaS__Mandatory_question__c;
    global String CnP_PaaS__Message_Task__c;
    global String CnP_PaaS__Notifier_Task__c;
    global Boolean CnP_PaaS__Opportunities__c;
    global String CnP_PaaS__Opportunity_Conditions__c;
    global Boolean CnP_PaaS__Opportunity_Name__c;
    global String CnP_PaaS__Opportunity_Record_type__c;
    global Boolean CnP_PaaS__Primary_Contact_Role__c;
    global String CnP_PaaS__Priority__c;
    global String CnP_PaaS__Product_Condition__c;
    global String CnP_PaaS__Product_Record_Type__c;
    global String CnP_PaaS__Product_Sku__c;
    global Boolean CnP_PaaS__Products__c;
    global String CnP_PaaS__Question__c;
    global String CnP_PaaS__Record_Type__c;
    global Boolean CnP_PaaS__Remainder_Task__c;
    global String CnP_PaaS__Role_SKU__c;
    global String CnP_PaaS__SKU_Condition_Connect__c;
    global String CnP_PaaS__SKU_Text__c;
    global String CnP_PaaS__SKU_value__c;
    global String CnP_PaaS__SecondName_Prefix__c;
    global String CnP_PaaS__SecondName_Separator__c;
    global String CnP_PaaS__Sku__c;
    global String CnP_PaaS__Status_Task__c;
    global Boolean CnP_PaaS__Task__c;
    global Double CnP_PaaS__Tax_Deductible__c;
    global String CnP_PaaS__ThirdName_Prefix__c;
    global String CnP_PaaS__ThirdName_Separator__c;
    global Double CnP_PaaS__Unit_Discount__c;
    global Double CnP_PaaS__Unit_Item__c;
    global Double CnP_PaaS__Unit_Shipping__c;
    global Double CnP_PaaS__Unit_Tax__c;
    global User CnP_PaaS__UserID_Task__c;
    global String CnP_PaaS__User_name_Task__c;
    global String CnP_PaaS__WID_Condition__c;
    global String CnP_PaaS__WID__c;
    global String CnP_PaaS__ledger_sku_text__c;
    global String CnP_PaaS__remainder_days_task__c;

    global CnP_PaaS__Record_Types__ChangeEvent () 
    {
    }
}