// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class APXT_BPM__Scheduled_Conductor_History__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global APXT_BPM__Conductor__c APXT_BPM__Conga_Conductor__c;
    global Datetime APXT_BPM__Date__c;
    global String APXT_BPM__Description__c;
    global Double APXT_BPM__Number_of_Failures__c;
    global Double APXT_BPM__Number_of_Service_Events__c;
    global Double APXT_BPM__Number_of_Successes__c;
    global String APXT_BPM__Output_File_Link__c;
    global User APXT_BPM__Ran_as__c;
    global Double APXT_BPM__Total_Number_of_Records__c;

    global APXT_BPM__Scheduled_Conductor_History__ChangeEvent () 
    {
    }
}