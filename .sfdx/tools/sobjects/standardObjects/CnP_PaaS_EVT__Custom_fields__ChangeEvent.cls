// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS_EVT__Custom_fields__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global String CnP_PaaS_EVT__Create_new_group__c;
    global String CnP_PaaS_EVT__Default_value__c;
    global CnP_PaaS_EVT__Event__c CnP_PaaS_EVT__Event__c;
    global String CnP_PaaS_EVT__Field_Options__c;
    global String CnP_PaaS_EVT__Field_name__c;
    global String CnP_PaaS_EVT__Field_type__c;
    global String CnP_PaaS_EVT__Field_values__c;
    global Double CnP_PaaS_EVT__Int_order_Number__c;
    global String CnP_PaaS_EVT__Order_number__c;
    global CnP_PaaS_EVT__Questionsection__c CnP_PaaS_EVT__Question_Section__c;
    global String CnP_PaaS_EVT__Question__c;
    global CnP_PaaS_EVT__Registration_level__c CnP_PaaS_EVT__Registration_level__c;
    global Boolean CnP_PaaS_EVT__Required__c;
    global String CnP_PaaS_EVT__Section_order__c;
    global String CnP_PaaS_EVT__Select_group__c;
    global Boolean CnP_PaaS_EVT__Visible__c;

    global CnP_PaaS_EVT__Custom_fields__ChangeEvent () 
    {
    }
}