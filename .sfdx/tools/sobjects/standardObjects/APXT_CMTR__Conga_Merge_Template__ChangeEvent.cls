// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class APXT_CMTR__Conga_Merge_Template__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global String APXT_CMTR__Description__c;
    /* For MassMerge Label Templates: This setting indicates to use ReportData (or QueryData) as the data source for Labels
    */
    global Boolean APXT_CMTR__Label_Template_Use_Detail_Data__c;
    /* Enter the API field name, an equals sign, and the value to set (with spaces replaced with plus signs). 

Examples: 
Status__c=Completed 
Status_Date__c=Today 
Stage__c=In+Progress
    */
    global String APXT_CMTR__Master_Field_to_Set_1__c;
    global String APXT_CMTR__Master_Field_to_Set_2__c;
    global String APXT_CMTR__Master_Field_to_Set_3__c;
    global String APXT_CMTR__Name__c;
    /* Identify the group name to which this template belongs.  Used in conjunction with the "&TemplateGroup=" parameter in PointMerge.
    */
    global String APXT_CMTR__Template_Group__c;
    global String APXT_CMTR__Template_Type__c;

    global APXT_CMTR__Conga_Merge_Template__ChangeEvent () 
    {
    }
}