// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__XML_Setting__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global String CnP_PaaS__Account_Number__c;
    global String CnP_PaaS__Account_Record_Type__c;
    global String CnP_PaaS__Account_Type__c;
    global Account CnP_PaaS__Account_lp__c;
    global Boolean CnP_PaaS__Accounts__c;
    global Boolean CnP_PaaS__Activate_Swiper1__c;
    global Boolean CnP_PaaS__Add_Questions__c;
    global String CnP_PaaS__Amount__c;
    global Boolean CnP_PaaS__Authorized_Opportunity__c;
    global String CnP_PaaS__Campaign__c;
    global Campaign CnP_PaaS__Campaign_lp__c;
    global String CnP_PaaS__Contact_Option__c;
    global Boolean CnP_PaaS__Contact_Role__c;
    global String CnP_PaaS__Contact_Rolename__c;
    global Boolean CnP_PaaS__Contacts__c;
    global String CnP_PaaS__Convenience_Amount__c;
    global String CnP_PaaS__Convenience_Fee__c;
    global Boolean CnP_PaaS__Convenience_Opportunity__c;
    global String CnP_PaaS__Country_Format__c;
    global String CnP_PaaS__Custom_Payment_Options__c;
    global Boolean CnP_PaaS__Custom_Payment_Type_Opportunity__c;
    global String CnP_PaaS__Custom_parameters__c;
    global Boolean CnP_PaaS__Customize_Name__c;
    global Boolean CnP_PaaS__Declined_Opportunity__c;
    global String CnP_PaaS__Deductible_Charge__c;
    global Boolean CnP_PaaS__Disableupdate__c;
    global Boolean CnP_PaaS__Discount_Opportunity__c;
    global String CnP_PaaS__Discount__c;
    global Double CnP_PaaS__Donor_s_Donations_Rank_Factor__c;
    global Double CnP_PaaS__Donor_s_Raised_Donations_Rank_Factor__c;
    global Double CnP_PaaS__Donors_Perso_Donations_Count_Rank_Factor__c;
    global Double CnP_PaaS__Donors_Raised_Donations_Count_factor__c;
    global Boolean CnP_PaaS__Email__c;
    global String CnP_PaaS__Internal_Notifications__c;
    global Boolean CnP_PaaS__Invoice_Opportunity__c;
    global String CnP_PaaS__Lead_source__c;
    global String CnP_PaaS__Manual_Opportunities__c;
    global Boolean CnP_PaaS__No_Account__c;
    global String CnP_PaaS__Number_Of_Installments__c;
    global String CnP_PaaS__Oppor_Stage_Name__c;
    global Boolean CnP_PaaS__Opportunities__c;
    global String CnP_PaaS__Organization_Information__c;
    global Boolean CnP_PaaS__Payment_Credit_Card__c;
    global Boolean CnP_PaaS__Payment_Custom_Payment_Type__c;
    global String CnP_PaaS__Payment_For__c;
    global Boolean CnP_PaaS__Payment_Invoice__c;
    global Boolean CnP_PaaS__Payment_Purchase_Order__c;
    global Boolean CnP_PaaS__Payment_eCheck__c;
    global String CnP_PaaS__Periodicity__c;
    global Boolean CnP_PaaS__Person_Accounts__c;
    global Double CnP_PaaS__Personal_Donations_Count_Factor__c;
    global Double CnP_PaaS__Personal_Donations_Rank_Factor__c;
    global Boolean CnP_PaaS__Products__c;
    global Boolean CnP_PaaS__Purchase_Order_Opportunity__c;
    global Double CnP_PaaS__Raised_Donations_Count_factor__c;
    global Double CnP_PaaS__Raised_Donations_Rank_Factor__c;
    global String CnP_PaaS__Recurring_Type__c;
    global Boolean CnP_PaaS__Recurring__c;
    global String CnP_PaaS__SKU__c;
    global Boolean CnP_PaaS__Send_Receipt__c;
    global Boolean CnP_PaaS__Shipping_Opportunity__c;
    global String CnP_PaaS__Shipping__c;
    global String CnP_PaaS__Stage_Custom_Payment__c;
    global String CnP_PaaS__Stage_Declined__c;
    global String CnP_PaaS__Stage_Free__c;
    global String CnP_PaaS__Stage_Future__c;
    global String CnP_PaaS__Stage_Invoice__c;
    global String CnP_PaaS__Stage_PO__c;
    global String CnP_PaaS__State_Format__c;
    global Boolean CnP_PaaS__Tax_Opportunity__c;
    global String CnP_PaaS__Tax__c;
    global String CnP_PaaS__Terms_Conditions__c;
    global String CnP_PaaS__Thank_you__c;
    global String CnP_PaaS__Tracker__c;
    global Boolean CnP_PaaS__Update_Contacts__c;
    global String CnP_PaaS__VT_Customize_XML__c;
    global Boolean CnP_PaaS__insert_contacts__c;

    global CnP_PaaS__XML_Setting__ChangeEvent () 
    {
    }
}