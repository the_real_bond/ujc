// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CampaignMemberChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global Campaign Campaign;
    global Id CampaignId;
    global Lead Lead;
    global Id LeadId;
    global Contact Contact;
    global Id ContactId;
    global String Status;
    global Boolean HasResponded;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Date FirstRespondedDate;
    global String Payment_Status__c;
    global String Name_Corporate_Booking__c;
    global Account Booked_with__c;
    global Double Quantity__c;
    global String Timeslot__c;
    global String CampaignMTool__Address_Of_Member__c;
    global String CampaignMTool__City_Of_Member__c;
    global String CampaignMTool__Company_Of_Member__c;
    global String CampaignMTool__Country_Of_Member__c;
    global String CampaignMTool__Do_Not_Call_Member__c;
    global String CampaignMTool__Email_Of_Member__c;
    global String CampaignMTool__Fax_Of_Member__c;
    global String CampaignMTool__MobilePhone_Of_Member__c;
    global String CampaignMTool__Name_Of_Member__c;
    global String CampaignMTool__Phone_Of_Member__c;
    global String CampaignMTool__PostalCode_Of_Member__c;
    global User CampaignMTool__Related_To__c;
    global String CampaignMTool__Salutation_Of_Member__c;
    global String CampaignMTool__Title_Of_Member__c;
    global String Status_Reason__c;
    global String Database_Status__c;
    global Boolean Pledge_Card_Created__c;
    global String Hometown__c;
    global String Location__c;

    global CampaignMemberChangeEvent () 
    {
    }
}