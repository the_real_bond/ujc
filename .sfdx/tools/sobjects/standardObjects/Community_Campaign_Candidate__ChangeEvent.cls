// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Community_Campaign_Candidate__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Community_Campaign__c Community_Campaign__c;
    global Boolean Active__c;
    global String Biography__c;
    global Account Community_Register__c;
    global String First_Name__c;
    global String Last_Name__c;
    global String Motivation__c;
    global String Photo__c;

    global Community_Campaign_Candidate__ChangeEvent () 
    {
    }
}