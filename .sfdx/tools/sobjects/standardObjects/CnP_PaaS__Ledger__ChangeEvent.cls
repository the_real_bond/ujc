// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__Ledger__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global String CnP_PaaS__Account_Name__c;
    global String CnP_PaaS__Account_Number__c;
    global String CnP_PaaS__Account_Type__c;
    global String CnP_PaaS__Description__c;
    global String CnP_PaaS__Notes__c;
    global String CnP_PaaS__Sub_Account__c;

    global CnP_PaaS__Ledger__ChangeEvent () 
    {
    }
}