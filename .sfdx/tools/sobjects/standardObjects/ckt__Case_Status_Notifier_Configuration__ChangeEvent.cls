// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class ckt__Case_Status_Notifier_Configuration__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Boolean ckt__Include_case_contact__c;
    global Boolean ckt__Include_case_owner__c;
    global String ckt__Message__c;
    global String ckt__Recipients__c;
    global String ckt__Status__c;
    global String ckt__Template_Folder_Name__c;
    global String ckt__Template_Name__c;

    global ckt__Case_Status_Notifier_Configuration__ChangeEvent () 
    {
    }
}