// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Relationship__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global String Description__c;
    global String Individual_Communal_Reference_Number__c;
    global String Related_Communal_Reference_Number__c;
    global String Status__c;
    global String Type__c;
    global Account Individual__c;
    global Account Related_Individual__c;
    global Relationship__c Reciprocal_Relationship__c;
    global String Individual_Gender__c;
    global String Related_Individual_Gender__c;
    global String DB_Status__c;
    global Date DoD__c;

    global Relationship__ChangeEvent () 
    {
    }
}