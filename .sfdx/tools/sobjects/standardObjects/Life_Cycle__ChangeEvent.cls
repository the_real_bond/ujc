// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Life_Cycle__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global String Event_Type__c;
    global String Location__c;
    global String Relationship__c;
    global Date Event_Date__c;
    /* How did we become aware of the event?
    */
    global String Source__c;
    global Account Person__c;
    global String Relation__c;
    global String Event_Details__c;
    global String Life_Cycle_Year__c;
    global String Life_Cycle_Week__c;
    global String Event__c;
    global String Preferred_Name__c;
    global String Mail_Merge_Email__c;
    global String Formal_Salutation__c;
    global String Hebrew_Date__c;
    global String Today_s_Date__c;
    global String Relation_Last__c;
    global Boolean Letter_Sent__c;
    global String Event_Category__c;
    global String Conga_Template_Id__c;
    global String Gender_Pronoun__c;
    global String Not_Sent_Because__c;
    global String Salutation_First_Names__c;

    global Life_Cycle__ChangeEvent () 
    {
    }
}