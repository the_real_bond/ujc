// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__C_P_Data1__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global String CnP_PaaS__CnP_Data_ID__c;
    global Contact CnP_PaaS__Contact__c;
    global String CnP_PaaS__DataXML__c;
    global Boolean CnP_PaaS__Email__c;
    global String CnP_PaaS__Message__c;
    global String CnP_PaaS__Name__c;
    global String CnP_PaaS__Order_Number__c;
    global Double CnP_PaaS__Status_ID__c;
    global Decimal CnP_PaaS__Total_Charged__c;
    global String CnP_PaaS__Transaction_Result__c;

    global CnP_PaaS__C_P_Data1__ChangeEvent () 
    {
    }
}