// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS_EVT__Event_registrant_session__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global String CnP_PaaS_EVT__Account_Type__c;
    global Decimal CnP_PaaS_EVT__Additional_Donation__c;
    global String CnP_PaaS_EVT__Card_Type__c;
    global Contact CnP_PaaS_EVT__ContactId__c;
    global String CnP_PaaS_EVT__Contact_Data__c;
    global String CnP_PaaS_EVT__Coupon_code__c;
    global CnP_PaaS_EVT__Event__c CnP_PaaS_EVT__EventId__c;
    global Double CnP_PaaS_EVT__Increment_Number__c;
    global String CnP_PaaS_EVT__Payment_Type__c;
    global String CnP_PaaS_EVT__Registration_type__c;
    global String CnP_PaaS_EVT__Response_Detail__c;
    global String CnP_PaaS_EVT__Status__c;
    global String CnP_PaaS_EVT__Ticket_Number__c;
    global Decimal CnP_PaaS_EVT__Total_Amount__c;
    global Decimal CnP_PaaS_EVT__Total_Discount__c;
    global String CnP_PaaS_EVT__Transaction_order_number__c;

    global CnP_PaaS_EVT__Event_registrant_session__ChangeEvent () 
    {
    }
}