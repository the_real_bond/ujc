// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class dlrs__LookupChild__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Double dlrs__Amount__c;
    global dlrs__LookupParent__c dlrs__LookupParent__c;
    global String dlrs__Color__c;
    global String dlrs__Description2__c;
    global String dlrs__Description__c;
    global dlrs__LookupParent__c dlrs__LookupParent2__c;

    global dlrs__LookupChild__ChangeEvent () 
    {
    }
}