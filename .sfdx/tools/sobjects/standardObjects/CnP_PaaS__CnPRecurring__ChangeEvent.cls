// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__CnPRecurring__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Account CnP_PaaS__Account__c;
    global Boolean CnP_PaaS__Canceled__c;
    global Date CnP_PaaS__Cancellation_Date__c;
    global Contact CnP_PaaS__Contact__c;
    global Date CnP_PaaS__Date_Established__c;
    global Date CnP_PaaS__FirstChargeDate__c;
    global String CnP_PaaS__Full_Name__c;
    global Decimal CnP_PaaS__Installment_Amount__c;
    global Double CnP_PaaS__InstallmentsMade__c;
    global Double CnP_PaaS__Installments__c;
    global Date CnP_PaaS__NextInstallment_Date__c;
    global String CnP_PaaS__OrderNumber__c;
    global String CnP_PaaS__Periodicity__c;
    global String CnP_PaaS__RecurringMethod__c;
    global String CnP_PaaS__Recurring_Transaction_Id__c;
    global Decimal CnP_PaaS__Total_Made__c;
    global Decimal CnP_PaaS__Total__c;
    global String CnP_PaaS__Transaction_Result__c;
    global Decimal CnP_PaaS__Upcoming_Receipts__c;

    global CnP_PaaS__CnPRecurring__ChangeEvent () 
    {
    }
}