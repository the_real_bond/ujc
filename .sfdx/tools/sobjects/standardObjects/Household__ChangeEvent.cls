// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Household__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global String Communal_Reference_Number__c;
    global String Old_Register_Area_R__c;
    global Double Household_Counter__c;
    global String City_RP__c;
    global String City_RS__c;
    global String Country_RP__c;
    global String Country_RS__c;
    global String Email_Residential__c;
    global String Postal_Code_RP__c;
    global String Postal_Code_RS__c;
    global String Province_RP__c;
    global String Province_RS__c;
    global String Residential_Address_RS__c;
    global String Residential_Postal_Address_RP__c;
    global String Residential_Telephone__c;
    global String Suburb_RP__c;
    global String Suburb_RS__c;

    global Household__ChangeEvent () 
    {
    }
}