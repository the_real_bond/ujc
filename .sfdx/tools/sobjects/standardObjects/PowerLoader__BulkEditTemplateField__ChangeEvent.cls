// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class PowerLoader__BulkEditTemplateField__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global PowerLoader__BulkEditTemplate__c PowerLoader__BulkEditTemplate__c;
    global Double PowerLoader__FieldOrder__c;
    global Boolean PowerLoader__Group__c;
    global Boolean PowerLoader__SplitAt__c;
    global String PowerLoader__SummaryType__c;
    global Double PowerLoader__Width__c;

    global PowerLoader__BulkEditTemplateField__ChangeEvent () 
    {
    }
}