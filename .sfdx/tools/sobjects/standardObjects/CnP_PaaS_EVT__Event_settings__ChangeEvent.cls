// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS_EVT__Event_settings__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Boolean CnP_PaaS_EVT__Acknowledgement_Mandatory__c;
    global Boolean CnP_PaaS_EVT__American_Express__c;
    global String CnP_PaaS_EVT__Attendee_Ticket_Email_Body__c;
    global Boolean CnP_PaaS_EVT__Box_Office_American_Express__c;
    global Boolean CnP_PaaS_EVT__Box_Office_Credit_Card__c;
    global Boolean CnP_PaaS_EVT__Box_Office_Custom_Payment_Check__c;
    global String CnP_PaaS_EVT__Box_Office_Custom_Payment_Name__c;
    global Boolean CnP_PaaS_EVT__Box_Office_Discover__c;
    global String CnP_PaaS_EVT__Box_Office_Free_Payment__c;
    global Boolean CnP_PaaS_EVT__Box_Office_Invoice__c;
    global Boolean CnP_PaaS_EVT__Box_Office_JCB__c;
    global Boolean CnP_PaaS_EVT__Box_Office_Master_Card__c;
    global Boolean CnP_PaaS_EVT__Box_Office_Purchase_Order__c;
    global Boolean CnP_PaaS_EVT__Box_Office_Visa__c;
    global Boolean CnP_PaaS_EVT__Box_Office_eCheck__c;
    global Boolean CnP_PaaS_EVT__Credit_Card__c;
    global Boolean CnP_PaaS_EVT__Custom_Payment_Check__c;
    global String CnP_PaaS_EVT__Custom_Payment_Name__c;
    global String CnP_PaaS_EVT__Default_Account_Number__c;
    global String CnP_PaaS_EVT__Default_Public_Site_Url__c;
    global Boolean CnP_PaaS_EVT__Discover__c;
    global String CnP_PaaS_EVT__Email_Fromname__c;
    global String CnP_PaaS_EVT__Email_ReplyTo__c;
    global String CnP_PaaS_EVT__Email_Subject__c;
    global String CnP_PaaS_EVT__Event_Ended__c;
    global String CnP_PaaS_EVT__Event_categories__c;
    global String CnP_PaaS_EVT__Free_Payment__c;
    global String CnP_PaaS_EVT__Internal_Notification__c;
    global Boolean CnP_PaaS_EVT__Invoice__c;
    global Boolean CnP_PaaS_EVT__JCB__c;
    global String CnP_PaaS_EVT__Level_Sold_Out_Message__c;
    global Boolean CnP_PaaS_EVT__Master_Card__c;
    global String CnP_PaaS_EVT__Organization_Information__c;
    global Boolean CnP_PaaS_EVT__Purchase_Order__c;
    global String CnP_PaaS_EVT__Receipt_Terms_Condition__c;
    global String CnP_PaaS_EVT__Registered_Status_options__c;
    global String CnP_PaaS_EVT__Registrant_Ticket_Email_Body__c;
    global String CnP_PaaS_EVT__Registration_Declined_Message__c;
    global Boolean CnP_PaaS_EVT__Send_Receipt__c;
    global Boolean CnP_PaaS_EVT__Show_Terms_Conditions__c;
    global String CnP_PaaS_EVT__Site_Custom_Payment_Name__c;
    global String CnP_PaaS_EVT__Sold_Out__c;
    global String CnP_PaaS_EVT__Terms_Conditions__c;
    global String CnP_PaaS_EVT__Thank_You_Message_Receipt__c;
    global String CnP_PaaS_EVT__Thank_You_Message__c;
    global Boolean CnP_PaaS_EVT__Visa__c;
    global Boolean CnP_PaaS_EVT__eCheck__c;

    global CnP_PaaS_EVT__Event_settings__ChangeEvent () 
    {
    }
}