// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class PowerLoader__Query_Parameter_Server_Filter__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global PowerLoader__Query_Parameter__c PowerLoader__Query_Parameter__c;
    global String PowerLoader__Criteria_Value__c;
    global String PowerLoader__Criteria__c;
    global String PowerLoader__Filter_Operation__c;
    global String PowerLoader__Filter_Type__c;
    global String PowerLoader__Filter_Value__c;
    global Boolean PowerLoader__Negate_Criteria__c;

    global PowerLoader__Query_Parameter_Server_Filter__ChangeEvent () 
    {
    }
}