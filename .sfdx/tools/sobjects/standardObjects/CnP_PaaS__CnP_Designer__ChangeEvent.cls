// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__CnP_Designer__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global String CnP_PaaS__Design_Selection__c;
    global String CnP_PaaS__Library__c;
    global String CnP_PaaS__Pdf__c;
    global Boolean CnP_PaaS__Pdf_include__c;
    global String CnP_PaaS__SalesForce_Public_Site_URL__c;
    global String CnP_PaaS__Select_Layout__c;
    global String CnP_PaaS__Select_Template_Cat__c;
    global String CnP_PaaS__Tags__c;
    global String CnP_PaaS__Template_Layout_Name__c;
    global String CnP_PaaS_EVT__Event_Custom_Css__c;
    global String CnP_PaaS_EVT__Event_Designer_Category__c;

    global CnP_PaaS__CnP_Designer__ChangeEvent () 
    {
    }
}