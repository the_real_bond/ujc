// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__Broadcaster_Campaign__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global CnP_PaaS__BroadCaster_Service__c CnP_PaaS__BroadCaster_Service__c;
    global String CnP_PaaS__Analytics__c;
    global String CnP_PaaS__Analytics_tag__c;
    global Boolean CnP_PaaS__Authenticate__c;
    global CnP_PaaS__Broadcaster_List__c CnP_PaaS__Broadcaster_List__c;
    global String CnP_PaaS__Camp_Id__c;
    global String CnP_PaaS__Content_Type__c;
    global String CnP_PaaS__Create_Time__c;
    global Double CnP_PaaS__Email_Sent__c;
    global Double CnP_PaaS__Folder_Id__c;
    global String CnP_PaaS__From_email__c;
    global String CnP_PaaS__From_name__c;
    global String CnP_PaaS__List_Id__c;
    global String CnP_PaaS__Parent_Id__c;
    global String CnP_PaaS__Send_Time__c;
    global String CnP_PaaS__Status__c;
    global String CnP_PaaS__Subject__c;
    global Double CnP_PaaS__Template_Id__c;
    global Boolean CnP_PaaS__Timewarp__c;
    global String CnP_PaaS__Timewarp_schedule__c;
    global String CnP_PaaS__To_Name__c;
    global String CnP_PaaS__Type__c;
    global Double CnP_PaaS__Web_Id__c;
    global String CnP_PaaS__archieve_URL__c;
    global String CnP_PaaS__auto_fb_post__c;
    global Boolean CnP_PaaS__auto_footer__c;
    global Boolean CnP_PaaS__auto_tweet__c;
    global Boolean CnP_PaaS__ecomm360__c;
    global Boolean CnP_PaaS__html_clicks__c;
    global Boolean CnP_PaaS__inline_CSS__c;
    global Boolean CnP_PaaS__opens__c;
    global String CnP_PaaS__segment_text__c;
    global Boolean CnP_PaaS__text_clicks__c;

    global CnP_PaaS__Broadcaster_Campaign__ChangeEvent () 
    {
    }
}