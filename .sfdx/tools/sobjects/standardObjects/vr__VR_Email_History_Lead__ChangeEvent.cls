// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class vr__VR_Email_History_Lead__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Boolean vr__Bounced__c;
    global String vr__Campaign_Hash__c;
    global String vr__Clicked_Links__c;
    global Boolean vr__Clicked__c;
    global String vr__Company_Hash__c;
    global String vr__Email_ID__c;
    global String vr__Email_Type__c;
    global Lead vr__Lead__c;
    global String vr__List_Type__c;
    global Datetime vr__Mail_Date__c;
    global Boolean vr__Opened__c;
    global Boolean vr__Sent__c;
    global Boolean vr__Unsubscribed__c;

    global vr__VR_Email_History_Lead__ChangeEvent () 
    {
    }
}