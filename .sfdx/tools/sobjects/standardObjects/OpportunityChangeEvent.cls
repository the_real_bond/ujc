// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class OpportunityChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global Account Account;
    global Id AccountId;
    global RecordType RecordType;
    global Id RecordTypeId;
    global Boolean IsPrivate;
    global String Name;
    global String Description;
    global String StageName;
    global Decimal Amount;
    global Decimal ExpectedRevenue;
    global Double TotalOpportunityQuantity;
    global Date CloseDate;
    global String Type;
    global String NextStep;
    global String LeadSource;
    global Boolean IsClosed;
    global Boolean IsWon;
    global String ForecastCategory;
    global String ForecastCategoryName;
    global Campaign Campaign;
    global Id CampaignId;
    global Boolean HasOpportunityLineItem;
    global Pricebook2 Pricebook2;
    global Id Pricebook2Id;
    global User Owner;
    global Id OwnerId;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Contact Contact;
    global Id ContactId;
    global Double Current_Installment_Number__c;
    global Household__c Household__c;
    global Decimal IUA_Installment__c;
    global Decimal Installment_Total__c;
    global Double Number_of_Installments__c;
    global Recurring_Donation__c Recurring_Donation__c;
    global Decimal UCF_Communal_Installment__c;
    global Decimal UCF_Education_Installment__c;
    global Decimal Welfare_Installment__c;
    global String Campaign_Year__c;
    global String Campaign__c;
    global String Cancellation_Reason__c;
    global String Communal_Reference_Number__c;
    global String Method_of_Payment__c;
    global String Old_Pledge_Reference_Number__c;
    global String Pledge_Reference_Auto_Number__c;
    global String Pledge_Number__c;
    global Date Receipt_Date__c;
    global Double IUA__c;
    global Double UCFC__c;
    global Double UCFE__c;
    global Double Welfare__c;
    global Date TodaysDate__c;
    global Double TotalAmount__c;
    global String Trans_Key__c;
    global String Trans_Type__c;
    global String CnP_PaaS__C_P_Recurring_Reference_Number__c;
    global CnP_PaaS__CnPRecurring__c CnP_PaaS__C_P_Recurring__c;
    global CnP_PaaS__Class__c CnP_PaaS__Class__c;
    global Contact CnP_PaaS__CnP_Fundraiser_Contact__c;
    global String CnP_PaaS__CnP_Opportunity_Type__c;
    global CnP_PaaS__CnP_Transaction__c CnP_PaaS__CnP_OrderNumber__c;
    global String CnP_PaaS__CnP_Payment_Type__c;
    global Contact CnP_PaaS__Contact__c;
    global Decimal CnP_PaaS__Discount__c;
    global CnP_PaaS__Invoice__c CnP_PaaS__Invoice__c;
    global String Campaign_Info__c;
    global CnP_PaaS__Ledger__c CnP_PaaS__Ledger__c;
    global String CnP_PaaS__SKU__c;
    global CnP_PaaS__Sub_Class__c CnP_PaaS__Sub_Class__c;
    global Decimal CnP_PaaS__Tax__c;
    global Decimal CnP_PaaS__Tax_deductible__c;

    global OpportunityChangeEvent () 
    {
    }
}