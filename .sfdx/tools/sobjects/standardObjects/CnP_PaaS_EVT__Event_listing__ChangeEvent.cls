// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS_EVT__Event_listing__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global CnP_PaaS__CnP_Designer__c CnP_PaaS_EVT__C_P_Designer__c;
    global String CnP_PaaS_EVT__Font_family__c;
    global String CnP_PaaS_EVT__Font_size__c;
    global String CnP_PaaS_EVT__Footer_Text__c;
    global String CnP_PaaS_EVT__Footer_background__c;
    global String CnP_PaaS_EVT__Footer_information__c;
    global String CnP_PaaS_EVT__Iframe_2v__c;
    global String CnP_PaaS_EVT__Logo_width__c;
    global String CnP_PaaS_EVT__Page_background__c;
    global String CnP_PaaS_EVT__Page_header__c;
    global String CnP_PaaS_EVT__Public_Site_Url__c;
    global String CnP_PaaS_EVT__Public_site__c;
    global String CnP_PaaS_EVT__Section_Headers_background__c;
    global String CnP_PaaS_EVT__Section_description__c;
    global String CnP_PaaS_EVT__Section_header_title__c;
    global String CnP_PaaS_EVT__Section_headers_text__c;
    global String CnP_PaaS_EVT__Section_titleheader_background__c;
    global String CnP_PaaS_EVT__Title_and_information__c;
    global String CnP_PaaS_EVT__Upload_background_image__c;
    global String CnP_PaaS_EVT__Upload_banner__c;
    global String CnP_PaaS_EVT__iFrame_Code__c;

    global CnP_PaaS_EVT__Event_listing__ChangeEvent () 
    {
    }
}