// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Community_Campaign_Vote__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Community_Campaign__c Community_Campaign__c;
    global String Communal_Reference_Number__c;
    global Account Community_Register__c;
    global String Email__c;
    global String First_Name__c;
    global String Last_Name__c;
    global Boolean Send_Email__c;
    global String CellPhone__c;
    global String Mail_Merge_Addres__c;
    global String Postal_Code__c;
    global String Prefered_Name__c;
    global String Residential_Address__c;
    global String Suburb__c;
    global Double No_of_Candidate_Votes__c;

    global Community_Campaign_Vote__ChangeEvent () 
    {
    }
}