// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS_EVT__Inventory_Process__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global CnP_PaaS_EVT__Registration_level__c CnP_PaaS_EVT__C_P_Event_Registration_Level__c;
    global CnP_PaaS_EVT__Event__c CnP_PaaS_EVT__C_P_Event__c;
    global Double CnP_PaaS_EVT__Cookie_Gen_Num__c;
    global Double CnP_PaaS_EVT__Quantity__c;
    global Datetime CnP_PaaS_EVT__UserTimeset__c;

    global CnP_PaaS_EVT__Inventory_Process__ChangeEvent () 
    {
    }
}