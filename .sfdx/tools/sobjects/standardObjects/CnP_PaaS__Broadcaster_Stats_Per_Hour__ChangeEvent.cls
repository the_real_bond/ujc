// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__Broadcaster_Stats_Per_Hour__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global CnP_PaaS__Broadcaster_Campaign__c CnP_PaaS__Broadcaster_Campaign__c;
    global CnP_PaaS__Broadcaster_Stats__c CnP_PaaS__Broadcaster_Stats__c;
    global Double CnP_PaaS__Emails_Sent__c;
    global Double CnP_PaaS__Recipients_Click__c;
    global String CnP_PaaS__Timestamp__c;
    global Double CnP_PaaS__Unique_Opens__c;

    global CnP_PaaS__Broadcaster_Stats_Per_Hour__ChangeEvent () 
    {
    }
}