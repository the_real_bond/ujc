// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Campaign {
    global Id Id;
    global Boolean IsDeleted;
    global String Name;
    global Campaign Parent;
    global Id ParentId;
    global String Type;
    global String Status;
    global Date StartDate;
    global Date EndDate;
    global Decimal ExpectedRevenue;
    global Decimal BudgetedCost;
    global Decimal ActualCost;
    global Double ExpectedResponse;
    global Double NumberSent;
    global Boolean IsActive;
    global String Description;
    global Integer NumberOfLeads;
    global Integer NumberOfConvertedLeads;
    global Integer NumberOfContacts;
    global Integer NumberOfResponses;
    global Integer NumberOfOpportunities;
    global Integer NumberOfWonOpportunities;
    global Decimal AmountAllOpportunities;
    global Decimal AmountWonOpportunities;
    global User Owner;
    global Id OwnerId;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global Date LastActivityDate;
    global Datetime LastViewedDate;
    global Datetime LastReferencedDate;
    global RecordType CampaignMemberRecordType;
    global Id CampaignMemberRecordTypeId;
    global List<ActivityHistory> ActivityHistories;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<Attachment> Attachments;
    global List<Campaign> ChildCampaigns;
    global List<CampaignFeed> Feeds;
    global List<CampaignHistory> Histories;
    global List<CampaignMember> CampaignMembers;
    global List<CampaignMemberStatus> CampaignMemberStatuses;
    global List<CampaignShare> Shares;
    global List<CnP_PaaS_EVT__Event__c> CnP_PaaS_EVT__C_P_Events1__r;
    global List<CnP_PaaS_EVT__Event__c> CnP_PaaS_EVT__C_P_Events__r;
    global List<CnP_PaaS_EVT__Registration_level__c> CnP_PaaS_EVT__Registration_levels__r;
    global List<CnP_PaaS__CnP_Transaction__c> CnP_PaaS__C_P_Transactions__r;
    global List<CnP_PaaS__Payment_Policy__c> CnP_PaaS__Payment_Policies__r;
    global List<CnP_PaaS__Pledge__c> CnP_PaaS__C_P_Pledge__r;
    global List<CnP_PaaS__Record_Types__c> CnP_PaaS__Record_Types__r;
    global List<CnP_PaaS__Record_Types__c> CnP_PaaS__C_P_Record_Types__r;
    global List<CnP_PaaS__Sub_Recordtypes__c> CnP_PaaS__Sub_Recordtypes__r;
    global List<CnP_PaaS__XML_Setting__c> CnP_PaaS__XML_Settings__r;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<EmailMessage> Emails;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<Event> Events;
    global List<ListEmail> ListEmails;
    global List<ListEmailRecipientSource> ListEmailRecipientSources;
    global List<NetworkActivityAudit> ParentEntities;
    global List<OpenActivity> OpenActivities;
    global List<Opportunity> Opportunities;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordAction> RecordActions;
    global List<RecordActionHistory> RecordActionHistories;
    global List<Recurring_Donation__c> Pledges__r;
    global List<Task> Tasks;
    global List<TopicAssignment> TopicAssignments;
    global List<CampaignChangeEvent> Parent;
    global List<CampaignMemberChangeEvent> Campaign;
    global List<ContentVersion> FirstPublishLocation;
    global List<EventChangeEvent> What;
    global List<EventRelationChangeEvent> Relation;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;
    global List<ListEmailChangeEvent> Campaign;
    global List<TaskChangeEvent> What;

    global Campaign () 
    {
    }
}