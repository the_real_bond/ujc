// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Field_Trip__Field_Analysis__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Field_Trip__Object_Analysis__c Field_Trip__Object_Analysis__c;
    global Date Field_Trip__Field_Created_Date__c;
    global String Field_Trip__Label__c;
    global Double Field_Trip__Populated_On_Percent__c;
    global Double Field_Trip__Populated_On__c;
    global Double Field_Trip__Total_Tally__c;
    global String Field_Trip__Type__c;
    global Boolean Field_Trip__isCustom__c;
    global Boolean Field_Trip__isRequired__c;

    global Field_Trip__Field_Analysis__ChangeEvent () 
    {
    }
}