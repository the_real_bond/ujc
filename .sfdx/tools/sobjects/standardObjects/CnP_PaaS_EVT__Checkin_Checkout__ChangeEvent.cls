// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS_EVT__Checkin_Checkout__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime CnP_PaaS_EVT__CheckIn_Checkout_Time__c;
    global String CnP_PaaS_EVT__Check_Status__c;
    global Contact CnP_PaaS_EVT__Contact__c;
    global String CnP_PaaS_EVT__Device_Address__c;
    global CnP_PaaS_EVT__Event_Authenticate_Mobiles__c CnP_PaaS_EVT__Device__c;
    global String CnP_PaaS_EVT__User_ID__c;
    global CnP_PaaS_EVT__Event_attendee_session__c CnP_PaaS_EVT__attendee_name__c;

    global CnP_PaaS_EVT__Checkin_Checkout__ChangeEvent () 
    {
    }
}