// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__Broadcaster_Recurring_Schedule__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global CnP_PaaS__Broadcaster_Schedule_Process__c CnP_PaaS__Broadcaster_Schedule_Process__c;
    global String CnP_PaaS__End_on__c;
    global String CnP_PaaS__Repeats_Every__c;
    global Date CnP_PaaS__Scheduledate__c;

    global CnP_PaaS__Broadcaster_Recurring_Schedule__ChangeEvent () 
    {
    }
}