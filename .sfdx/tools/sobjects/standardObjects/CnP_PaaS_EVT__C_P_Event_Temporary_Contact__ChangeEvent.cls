// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS_EVT__C_P_Event_Temporary_Contact__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global CnP_PaaS_EVT__Event_attendee_session__c CnP_PaaS_EVT__AttendeeID__c;
    global CnP_PaaS__CnPData__c CnP_PaaS_EVT__C_P_Data__c;
    global CnP_PaaS_EVT__Event_registrant_session__c CnP_PaaS_EVT__C_P_Event_Registrant__c;
    global String CnP_PaaS_EVT__Contact_Data__c;
    global String CnP_PaaS_EVT__Email__c;
    global CnP_PaaS_EVT__Event__c CnP_PaaS_EVT__Event_name__c;
    global String CnP_PaaS_EVT__First_name__c;
    global String CnP_PaaS_EVT__Last_name__c;
    global Contact CnP_PaaS_EVT__Map_Contact__c;
    global String CnP_PaaS_EVT__Order_Number__c;
    global String CnP_PaaS_EVT__XML_Data__c;

    global CnP_PaaS_EVT__C_P_Event_Temporary_Contact__ChangeEvent () 
    {
    }
}