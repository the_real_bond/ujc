// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS_EVT__Discount_plan__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Double CnP_PaaS_EVT__Available_Inventory__c;
    global CnP_PaaS_EVT__Discount_plan__c CnP_PaaS_EVT__C_P_Discount_Plan__c;
    global String CnP_PaaS_EVT__Coupon_Code_value__c;
    global String CnP_PaaS_EVT__Coupon_code__c;
    global String CnP_PaaS_EVT__End_of_plan__c;
    global CnP_PaaS_EVT__Event__c CnP_PaaS_EVT__Event_name__c;
    global Double CnP_PaaS_EVT__Inventory_Sold__c;
    global Decimal CnP_PaaS_EVT__Max_Amount__c;
    global String CnP_PaaS_EVT__Max_number__c;
    global Decimal CnP_PaaS_EVT__Min_Amount__c;
    global String CnP_PaaS_EVT__Min_number__c;
    global CnP_PaaS_EVT__Registration_level__c CnP_PaaS_EVT__Registration_level__c;
    global Datetime CnP_PaaS_EVT__Start_date__c;
    global Double CnP_PaaS_EVT__Total_Inventory__c;
    global Double CnP_PaaS_EVT__Total_discount__c;
    global String CnP_PaaS_EVT__discount__c;
    global Datetime CnP_PaaS_EVT__expire_date__c;
    global String CnP_PaaS_EVT__fixed_discount__c;
    global String CnP_PaaS_EVT__inventory__c;

    global CnP_PaaS_EVT__Discount_plan__ChangeEvent () 
    {
    }
}