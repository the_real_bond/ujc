// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__Connect_Statistics__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Contact CnP_PaaS__Contact__c;
    global Double CnP_PaaS__Donor_PD_Amount_Rank_Factor__c;
    global Double CnP_PaaS__Donor_PD_Count_Rank_Factor__c;
    global Double CnP_PaaS__Donor_RD_Amount_Rank_Factor__c;
    global Double CnP_PaaS__Donor_RD_Count_Rank_Factor__c;
    global Double CnP_PaaS__Donor_Rank_Factor__c;
    global Double CnP_PaaS__Global_Rank_Factor__c;
    global Double CnP_PaaS__Global_Rank__c;
    global Double CnP_PaaS__My_Rank_Factor__c;
    global Double CnP_PaaS__PD_Amount_Rank_Factor__c;
    global Double CnP_PaaS__PD_Count_Rank_Factor__c;
    global Decimal CnP_PaaS__Personal_Donations_Amount_Donors__c;
    global Double CnP_PaaS__Personal_Donations_Amount_Rank_Donors__c;
    global Double CnP_PaaS__Personal_Donations_Amount_Rank__c;
    global Decimal CnP_PaaS__Personal_Donations_Amount__c;
    global Double CnP_PaaS__Personal_Donations_Count_Donors__c;
    global Double CnP_PaaS__Personal_Donations_Count_Rank_Donors__c;
    global Double CnP_PaaS__Personal_Donations_Count_Rank__c;
    global Double CnP_PaaS__Personal_Donations_Count__c;
    global Double CnP_PaaS__RD_Amount_Rank_Factor__c;
    global Double CnP_PaaS__RD_Count_Rank_Factor__c;
    global Decimal CnP_PaaS__Raised_Donations_Amount_Donors__c;
    global Double CnP_PaaS__Raised_Donations_Amount_Rank_Donors__c;
    global Double CnP_PaaS__Raised_Donations_Amount_Rank__c;
    global Decimal CnP_PaaS__Raised_Donations_Amount__c;
    global Double CnP_PaaS__Raised_Donations_Count_Donors__c;
    global Double CnP_PaaS__Raised_Donations_Count_Rank_Donors__c;
    global Double CnP_PaaS__Raised_Donations_Count_Rank__c;
    global Double CnP_PaaS__Raised_Donations_Count__c;
    global Decimal CnP_PaaS__Total_Intrinsic_Value__c;
    global Double CnP_PaaS__Total_My_Donors_Rank__c;
    global Decimal CnP_PaaS__Total_extrinsic_value__c;
    global Double CnP_PaaS__Total_rank__c;

    global CnP_PaaS__Connect_Statistics__ChangeEvent () 
    {
    }
}