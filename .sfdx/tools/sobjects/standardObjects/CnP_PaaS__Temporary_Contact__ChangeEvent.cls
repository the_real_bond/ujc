// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__Temporary_Contact__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Decimal CnP_PaaS__Amount__c;
    global CnP_PaaS__CnPData__c CnP_PaaS__C_P_Data__c;
    global String CnP_PaaS__Email__c;
    global String CnP_PaaS__First_Name__c;
    global Double CnP_PaaS__Installment__c;
    global String CnP_PaaS__Last_Name__c;
    global Contact CnP_PaaS__Map_Contact__c;
    global String CnP_PaaS__Master_Transaction_Number__c;
    global String CnP_PaaS__OrderNumber__c;
    global String CnP_PaaS__Recurring__c;
    global String CnP_PaaS__Xmldata__c;
    global CnP_PaaS_EVT__Event__c CnP_PaaS_EVT__Event_ID__c;

    global CnP_PaaS__Temporary_Contact__ChangeEvent () 
    {
    }
}