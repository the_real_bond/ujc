// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class APXT_BPM__Conductor__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    /* The Id of a Content Workspace to which the output from a QMode=1 operation should be attached.  (Blank means to attach to the running-user's Personal Workspace.)

This setting does not affect the Content settings that apply to an individual merge event.
    */
    global String APXT_BPM__Content_Workspace_Id__c;
    global String APXT_BPM__Description__c;
    global Double APXT_BPM__Has_Query_Id__c;
    global Double APXT_BPM__Has_Record_Id__c;
    global Double APXT_BPM__Has_Report_Id__c;
    /* Use the Schedule button to change this value
    */
    global Datetime APXT_BPM__Next_Run_Date_Display__c;
    /* Determined by "Schedule" button.  Do not edit this field manually.
    */
    global Datetime APXT_BPM__Next_Run_Date__c;
    /* The Id of a Conga Query that contains the list of master object ids. Optionally, the query may include other columns.
    */
    global String APXT_BPM__Query_Id__c;
    /* An individual Salesforce record to use as the master object id.
    */
    global String APXT_BPM__Record_Id__c;
    /* The Id of a Report that contains the list of master object ids. Optionally, the report may include other columns.
    */
    global String APXT_BPM__Report_Id__c;
    /* Use the Schedule button to change this value
    */
    global String APXT_BPM__Schedule_Description_Display__c;
    /* Determined by "Schedule" button.  Do not edit this field manually.
    */
    global String APXT_BPM__Schedule_Description__c;
    global String APXT_BPM__Title__c;
    /* API Name of the field on the master object that contains the Composer URL value (parameters, etc).
    */
    global String APXT_BPM__URL_Field_Name__c;

    global APXT_BPM__Conductor__ChangeEvent () 
    {
    }
}