// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__CnP_Transaction__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global RecordType RecordType;
    global Id RecordTypeId;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global String CnP_PaaS__Account_Type__c;
    global Account CnP_PaaS__Account__c;
    global Decimal CnP_PaaS__Amount__c;
    global String CnP_PaaS__Application_Name__c;
    global String CnP_PaaS__AutorizationCode__c;
    global CnP_PaaS__CnP_Currency__c CnP_PaaS__C_P_Currency__c;
    global CnP_PaaS__CnPRecurring__c CnP_PaaS__C_P_Recurring__c;
    global String CnP_PaaS__Campaign__c;
    global String CnP_PaaS__CardExpiration__c;
    global Decimal CnP_PaaS__Charge_Amount__c;
    global Datetime CnP_PaaS__Charge_Date__c;
    global String CnP_PaaS__CheckOutPageId__c;
    global String CnP_PaaS__CheckOutPage__c;
    global String CnP_PaaS__CheckType__c;
    global String CnP_PaaS__Check_Number__c;
    global String CnP_PaaS__Check_Type__c;
    global Double CnP_PaaS__CnPAccountID__c;
    global String CnP_PaaS__CnP_Account_ID__c;
    global Contact CnP_PaaS__CnP_Fundraiser_Contact__c;
    global Contact CnP_PaaS__Contact__c;
    global String CnP_PaaS__CouponCode__c;
    global String CnP_PaaS__CreditCard4x4__c;
    global String CnP_PaaS__Credit_Card_Name__c;
    global Double CnP_PaaS__CurrencyCode__c;
    global String CnP_PaaS__Currency__c;
    global String CnP_PaaS__CustomQuestions__c;
    global String CnP_PaaS__Custom_Payment_Type_Name__c;
    global Decimal CnP_PaaS__Deductible_Charge__c;
    global Decimal CnP_PaaS__Deductible_Due__c;
    global Decimal CnP_PaaS__Discount_Charge__c;
    global Decimal CnP_PaaS__Discount_Due__c;
    global String CnP_PaaS__Donation_Name__c;
    global String CnP_PaaS__Email__c;
    global String CnP_PaaS__GatewayTransactionNumber__c;
    global String CnP_PaaS__IdNumber__c;
    global String CnP_PaaS__IdState__c;
    global String CnP_PaaS__IdType__c;
    global String CnP_PaaS__Invoice_Check_Number__c;
    global String CnP_PaaS__Name_On_card__c;
    global String CnP_PaaS__Organization_Name__c;
    global String CnP_PaaS__PaymentType__c;
    global String CnP_PaaS__Payment_For__c;
    global String CnP_PaaS__Purchase_Order_Number__c;
    global Double CnP_PaaS__Quantity__c;
    global String CnP_PaaS__Record_Type__c;
    global String CnP_PaaS__Routing_Number__c;
    global String CnP_PaaS__ShippingMethod__c;
    global Decimal CnP_PaaS__Shipping_Cost_Charge__c;
    global Decimal CnP_PaaS__Shipping_Cost_Due__c;
    global Decimal CnP_PaaS__SurCharge__c;
    global Decimal CnP_PaaS__TaxAmount_Charge__c;
    global Decimal CnP_PaaS__TaxAmount_Due__c;
    global Decimal CnP_PaaS__TotalCharged__c;
    global Decimal CnP_PaaS__TotalDue__c;
    global String CnP_PaaS__Tracker__c;
    global Datetime CnP_PaaS__TransactionDate__c;
    global Decimal CnP_PaaS__TransactionDiscountCharge__c;
    global Decimal CnP_PaaS__TransactionDiscountDue__c;
    global Datetime CnP_PaaS__TransactionTimeZone__c;
    global String CnP_PaaS__Transaction_Result__c;
    global String CnP_PaaS__Transaction_Type__c;
    global String CnP_PaaS__UrlReferrer__c;
    global String CnP_PaaS__VaultGUID__c;
    global Double CnP_PaaS__bSource__c;
    global String CnP_PaaS__remarks__c;
    global Campaign CnP_PaaS__sf_Campaign__c;

    global CnP_PaaS__CnP_Transaction__ChangeEvent () 
    {
    }
}