// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS_EVT__Event_Account_Info__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Boolean CnP_PaaS_EVT__American_Express__c;
    global CnP_PaaS__CnP_API_Settings__c CnP_PaaS_EVT__C_P_API_Settings__c;
    global CnP_PaaS_EVT__Event__c CnP_PaaS_EVT__C_P_Event__c;
    global Boolean CnP_PaaS_EVT__Credit_Card__c;
    global String CnP_PaaS_EVT__Currency__c;
    global String CnP_PaaS_EVT__Currency_label__c;
    global Boolean CnP_PaaS_EVT__Custom_Payment_Check__c;
    global String CnP_PaaS_EVT__Custom_Payment_Name__c;
    global Boolean CnP_PaaS_EVT__Discover__c;
    global String CnP_PaaS_EVT__Free_Payment__c;
    global Boolean CnP_PaaS_EVT__Invoice__c;
    global Boolean CnP_PaaS_EVT__JCB__c;
    global Boolean CnP_PaaS_EVT__Master_Card__c;
    global Boolean CnP_PaaS_EVT__Purchase_Order__c;
    global Boolean CnP_PaaS_EVT__Visa__c;
    global Boolean CnP_PaaS_EVT__eCheck__c;

    global CnP_PaaS_EVT__Event_Account_Info__ChangeEvent () 
    {
    }
}