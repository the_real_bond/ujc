// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class APXTConga4__Conga_Email_Staging__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global String APXTConga4__HTMLBody__c;
    global String APXTConga4__Subject__c;
    global String APXTConga4__TextBody__c;
    global String APXTConga4__WhatId__c;
    global String APXTConga4__WhoId__c;

    global APXTConga4__Conga_Email_Staging__ChangeEvent () 
    {
    }
}