// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Salesforce_Check_List__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global String Issue_Group__c;
    global String Status__c;
    global String Assigned_Consultant__c;
    global Double Minimum_Estimate_Hours__c;
    global Double Maximum_Estimate_Hours__c;
    global Double Actual_Hours__c;
    global String Project_Description__c;
    global String Project_Detail__c;
    global String Priority__c;

    global Salesforce_Check_List__ChangeEvent () 
    {
    }
}