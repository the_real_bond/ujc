// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__Invoice_Items__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Decimal CnP_PaaS__Discount__c;
    global String CnP_PaaS__Item_Campaign__c;
    global Decimal CnP_PaaS__Item_Price__c;
    global Double CnP_PaaS__Quantity__c;
    global String CnP_PaaS__SKU__c;
    global Decimal CnP_PaaS__Tax__c;
    global CnP_PaaS__Invoice__c CnP_PaaS__Temp_Invoice__c;
    global Decimal CnP_PaaS__tax_deductible__c;

    global CnP_PaaS__Invoice_Items__ChangeEvent () 
    {
    }
}