// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class MC4SF__MC_Subscriber_Activity__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global MC4SF__MC_Subscriber__c MC4SF__MC_Subscriber__c;
    global String MC4SF__Action__c;
    global String MC4SF__Bounce_Type__c;
    global MC4SF__MC_Campaign__c MC4SF__MC_Campaign__c;
    global MC4SF__MC_List__c MC4SF__MC_List__c;
    global String MC4SF__MailChimp_Campaign_ID__c;
    global String MC4SF__Text_URL__c;
    global Datetime MC4SF__Timestamp__c;
    global String MC4SF__Type__c;
    global String MC4SF__URL__c;

    global MC4SF__MC_Subscriber_Activity__ChangeEvent () 
    {
    }
}