// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Donation_Receipts__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Tax_Receipt__c Tax_Receipt__c;
    global Receipt__c Receipt__c;
    global String Amount_in_Words__c;
    global Date Date__c;
    global Decimal Donation_Amount__c;
    global String Donation_Type__c;
    global String Type__c;
    global String dateclosed__c;
    global String Donor__c;

    global Donation_Receipts__ChangeEvent () 
    {
    }
}