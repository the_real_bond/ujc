// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class PowerLoader__Query_Parameter__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global PowerLoader__Query_Panel__c PowerLoader__Query_Panel__c;
    /* Field containing Ids from which distinct values will be retrieved to be used as a filter against the main query panel object
    */
    global String PowerLoader__Base_Master_Field_API_Name__c;
    /* API Name of Object that filters will be run against
    */
    global String PowerLoader__Base_Object_API_Name__c;
    global String PowerLoader__Default_Value_Type__c;
    global String PowerLoader__Default_Value__c;
    global Double PowerLoader__Display_Sequence__c;
    global String PowerLoader__Display_Type__c;
    global Boolean PowerLoader__IsDisabled__c;
    global Boolean PowerLoader__IsHidden__c;
    global Boolean PowerLoader__IsRequired__c;
    global Boolean PowerLoader__Is_Label_HTML__c;
    global String PowerLoader__Label__c;
    global String PowerLoader__List_Label_Field_API_Name__c;
    global String PowerLoader__List_Source_Type__c;
    global String PowerLoader__List_Source__c;
    global String PowerLoader__List_Value_Field_API_Name__c;
    global String PowerLoader__Object_API_Name__c;
    global String PowerLoader__Tag__c;

    global PowerLoader__Query_Parameter__ChangeEvent () 
    {
    }
}