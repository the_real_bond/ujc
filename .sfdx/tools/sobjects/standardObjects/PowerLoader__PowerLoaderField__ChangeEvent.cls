// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class PowerLoader__PowerLoaderField__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global PowerLoader__PowerLoaderMapping__c PowerLoader__PowerLoaderMapping__c;
    global String PowerLoader__CSVField__c;
    global String PowerLoader__ObjField__c;
    global String PowerLoader__RelField__c;

    global PowerLoader__PowerLoaderField__ChangeEvent () 
    {
    }
}