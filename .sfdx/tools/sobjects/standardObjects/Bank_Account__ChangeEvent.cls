// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Bank_Account__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global String Name;
    global RecordType RecordType;
    global Id RecordTypeId;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global String xxxBank_Name__c;
    global String xxxBranch_Code__c;
    global String Account_Type__c;
    global String Account_Number__c;
    global Account Account_Holder__c;
    global String Status__c;
    global String Account_Type_Number__c;
    global String Credit_Card_CVV__c;
    global String Credit_Card_Expiry_Month__c;
    global String Credit_Card_Expiry_Year__c;
    global String Credit_Card_Type__c;
    global String Credit_Card_Number__c;
    global Bank__c Bank__c;
    global Branch__c Branch__c;
    global String Bank_Name_Display__c;

    global Bank_Account__ChangeEvent () 
    {
    }
}