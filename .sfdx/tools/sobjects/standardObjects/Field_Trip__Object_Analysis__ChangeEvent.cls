// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Field_Trip__Object_Analysis__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global String Field_Trip__Filter__c;
    global Datetime Field_Trip__Last_Analyzed__c;
    global String Field_Trip__Last_Batch_Id__c;
    global String Field_Trip__Object_Label__c;
    global String Field_Trip__Object_Name__c;
    global Double Field_Trip__Record_Types__c;
    global Double Field_Trip__Records__c;
    global Double Field_Trip__Tally__c;
    global Boolean Field_Trip__isCustom__c;
    global Double Field_Trip__Fields__c;

    global Field_Trip__Object_Analysis__ChangeEvent () 
    {
    }
}