// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__CnPRecurringTransaction__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global CnP_PaaS__CnPRecurring__c CnP_PaaS__Recurring_Transaction__c;
    global CnP_PaaS__CnP_Transaction__c CnP_PaaS__TransactionId__c;
    global Double CnP_PaaS__InstallmentNumber__c;

    global CnP_PaaS__CnPRecurringTransaction__ChangeEvent () 
    {
    }
}