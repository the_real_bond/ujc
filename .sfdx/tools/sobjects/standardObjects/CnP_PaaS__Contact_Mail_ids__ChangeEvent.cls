// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__Contact_Mail_ids__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Contact CnP_PaaS__Contact__c;
    global String CnP_PaaS__Alias_Contact_Data__c;
    global String CnP_PaaS__Billing_City__c;
    global String CnP_PaaS__Billing_Country__c;
    global String CnP_PaaS__Billing_Phone__c;
    global String CnP_PaaS__Billing_Postal_Code__c;
    global String CnP_PaaS__Billing_State__c;
    global String CnP_PaaS__Billing_Street__c;
    global String CnP_PaaS__Contact_Mail_id__c;
    global String CnP_PaaS__First_Name__c;
    global String CnP_PaaS__Last_Name__c;

    global CnP_PaaS__Contact_Mail_ids__ChangeEvent () 
    {
    }
}