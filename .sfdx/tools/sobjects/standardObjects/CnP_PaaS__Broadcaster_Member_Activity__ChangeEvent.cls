// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__Broadcaster_Member_Activity__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global CnP_PaaS__Broadcaster_Campaign__c CnP_PaaS__Broadcaster_Campaign__c;
    global Double CnP_PaaS__Abuses__c;
    global Double CnP_PaaS__Bounces__c;
    global CnP_PaaS__Broadcaster_List__c CnP_PaaS__Broadcaster_List__c;
    global Double CnP_PaaS__Clicks__c;
    global Contact CnP_PaaS__Contact__c;
    global Double CnP_PaaS__Opens__c;
    global Double CnP_PaaS__Queued__c;
    global Double CnP_PaaS__Sents__c;
    global Double CnP_PaaS__Unsubscribes__c;
    global Double CnP_PaaS__ecomm__c;

    global CnP_PaaS__Broadcaster_Member_Activity__ChangeEvent () 
    {
    }
}