// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Specified_Pledges__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global String SP_Organisation__c;
    global Decimal SP_Amount__c;
    global String SP_Donor__c;
    global Recurring_Donation__c ParentPledge__c;
    global Date SP_Date__c;
    global String SP_Division__c;
    global String SP_Camp_Year__c;
    global Date Date_Paid_Across__c;
    global Boolean SP_Direct__c;
    global String Payment_Source__c;
    global Double Total_Division_Outstanding__c;
    global Double Total_Division_Pledge__c;
    global Boolean SP_Transfered__c;
    global String SP_Campaign__c;
    global String Donor_Email__c;
    global Account SP_Beneficiary__c;
    global String SP_Beneficiary_Campaign__c;
    /* This field is only used for Direct Pledges.
    */
    global String SP_Pledge_Type__c;

    global Specified_Pledges__ChangeEvent () 
    {
    }
}