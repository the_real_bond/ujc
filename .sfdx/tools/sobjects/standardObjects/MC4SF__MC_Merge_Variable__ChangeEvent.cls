// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class MC4SF__MC_Merge_Variable__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global MC4SF__MC_List__c MC4SF__MC_List__c;
    /* For radio and dropdown field types, an array of the options available
    */
    global String MC4SF__Choices__c;
    global String MC4SF__Contact_Field_Mapping__c;
    /* The default value the list owner has set for this field
    */
    global String MC4SF__Default_Value__c;
    /* The "data type" of this merge var.
    */
    global String MC4SF__Field_Type__c;
    global String MC4SF__Lead_Field_Mapping__c;
    global Double MC4SF__MailChimp_ID__c;
    /* The order the list owner has set this field to display in
    */
    global String MC4SF__Order__c;
    /* Whether or not this field is visible to list subscribers
    */
    global Boolean MC4SF__Public__c;
    global Boolean MC4SF__Required__c;
    global String MC4SF__SFDC_Data_Type__c;
    /* Whether the list owner has this field displayed on their list dashboard
    */
    global Boolean MC4SF__Show__c;
    /* The width of the field to be used
    */
    global String MC4SF__Size__c;
    /* The merge tag that's used for forms and MailChimp API calls.
    */
    global String MC4SF__Tag__c;

    global MC4SF__MC_Merge_Variable__ChangeEvent () 
    {
    }
}