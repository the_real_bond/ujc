// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class APXTConga4__Conga_Email_Template__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global String APXTConga4__Description__c;
    global String APXTConga4__HTMLBody__c;
    /* When enabled, Conga Composer stores the body of the HTML template as an attachment, rather than in the Rich Text field.
    */
    global Boolean APXTConga4__Is_Body_Attachment__c;
    global String APXTConga4__Name__c;
    global String APXTConga4__Subject__c;
    global String APXTConga4__Template_Group__c;
    global String APXTConga4__TextBody__c;
    global String APXTConga4__Key__c;

    global APXTConga4__Conga_Email_Template__ChangeEvent () 
    {
    }
}