// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__Sub_Recordtypes__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global CnP_PaaS__Record_Types__c CnP_PaaS__C_P_Record_Type__c;
    global Campaign CnP_PaaS__Campaign__c;
    global CnP_PaaS__Class__c CnP_PaaS__Class__c;
    global CnP_PaaS__Ledger__c CnP_PaaS__GL_Account__c;
    global String CnP_PaaS__Ledger_Name__c;
    global String CnP_PaaS__Ledger__c;
    global Boolean CnP_PaaS__Opportunity_Allocation__c;
    global Boolean CnP_PaaS__Opportunity_Product_Allocation__c;
    global String CnP_PaaS__Opportunity_SKU_Condition__c;
    global String CnP_PaaS__Percentage_amount__c;
    global String CnP_PaaS__SKU_value__c;
    global CnP_PaaS__Sub_Class__c CnP_PaaS__Sub_Class__c;

    global CnP_PaaS__Sub_Recordtypes__ChangeEvent () 
    {
    }
}