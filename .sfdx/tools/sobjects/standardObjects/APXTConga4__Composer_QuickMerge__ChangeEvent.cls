// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class APXTConga4__Composer_QuickMerge__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global APXTConga4__Conga_Solution__c APXTConga4__Conga_Solution__c;
    global String APXTConga4__Description__c;
    /* Use this button to launch the Composer QuickMerge Solution. Only available when the Button ID field is populated.
    */
    global String APXTConga4__Launch_CM8__c;
    global String APXTConga4__Weblink_ID_Formula__c;
    global String APXTConga4__Weblink_ID__c;
    global String APXTConga4__Weblink_Name_Formula__c;
    global String APXTConga4__Weblink_Name__c;

    global APXTConga4__Composer_QuickMerge__ChangeEvent () 
    {
    }
}