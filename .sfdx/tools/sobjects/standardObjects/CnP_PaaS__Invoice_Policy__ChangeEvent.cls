// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__Invoice_Policy__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Boolean CnP_PaaS__Acknowledgment__c;
    global Boolean CnP_PaaS__Additional_Payment__c;
    global Boolean CnP_PaaS__American_Express__c;
    global String CnP_PaaS__C_P_Replay_To_Address__c;
    global String CnP_PaaS__ClickandPledge_SMTP__c;
    global CnP_PaaS__CnP_API_Settings__c CnP_PaaS__CnP_API_Settings__c;
    global String CnP_PaaS__CnP_Alert_Subject__c;
    global String CnP_PaaS__CnP_Mail_From_Name__c;
    global String CnP_PaaS__Custom_Payment_Options__c;
    global Boolean CnP_PaaS__Custom_Payment_Type__c;
    global String CnP_PaaS__Decline__c;
    global String CnP_PaaS__Declined_Invoice_Stage__c;
    global Boolean CnP_PaaS__Discover__c;
    global String CnP_PaaS__DueDate__c;
    global String CnP_PaaS__Email_Alerts__c;
    global CnP_PaaS__CnP_Designer__c CnP_PaaS__Email_Template__c;
    global Boolean CnP_PaaS__Internal_Alerts__c;
    global String CnP_PaaS__Invoice_Subject__c;
    global String CnP_PaaS__Issued__c;
    global Boolean CnP_PaaS__JCB__c;
    global String CnP_PaaS__Mail_From_Address__c;
    global Boolean CnP_PaaS__Master_Card__c;
    global String CnP_PaaS__Net__c;
    global String CnP_PaaS__Paid_Invoice__c;
    global Boolean CnP_PaaS__Partial_Payments__c;
    global String CnP_PaaS__Past_Due_Invoice__c;
    global String CnP_PaaS__Policy_Status__c;
    global Boolean CnP_PaaS__Purchase_Order__c;
    global CnP_PaaS__SMTP_settings__c CnP_PaaS__SMTP_settings__c;
    global String CnP_PaaS__Send_Email__c;
    global String CnP_PaaS__Send_To__c;
    global String CnP_PaaS__Terms_Conditions__c;
    global String CnP_PaaS__Thank_you__c;
    global Date CnP_PaaS__Transcation_date__c;
    global Boolean CnP_PaaS__Visa__c;
    global CnP_PaaS__CnP_Designer__c CnP_PaaS__Web_Template__c;
    global Boolean CnP_PaaS__echeck__c;

    global CnP_PaaS__Invoice_Policy__ChangeEvent () 
    {
    }
}