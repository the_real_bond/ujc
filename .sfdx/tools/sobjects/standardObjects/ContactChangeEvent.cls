// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class ContactChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global Account Account;
    global Id AccountId;
    global Boolean IsPersonAccount;
    global String LastName;
    global String FirstName;
    global String Salutation;
    global String Name;
    global RecordType RecordType;
    global Id RecordTypeId;
    global String OtherStreet;
    global String OtherCity;
    global String OtherState;
    global String OtherPostalCode;
    global String OtherCountry;
    global Double OtherLatitude;
    global Double OtherLongitude;
    global String OtherGeocodeAccuracy;
    global Address OtherAddress;
    global String MailingStreet;
    global String MailingCity;
    global String MailingState;
    global String MailingPostalCode;
    global String MailingCountry;
    global Double MailingLatitude;
    global Double MailingLongitude;
    global String MailingGeocodeAccuracy;
    global Address MailingAddress;
    global String Phone;
    global String Fax;
    global String MobilePhone;
    global String HomePhone;
    global String OtherPhone;
    global String AssistantPhone;
    global Contact ReportsTo;
    global Id ReportsToId;
    global String Email;
    global String Title;
    global String Department;
    global String AssistantName;
    global String LeadSource;
    global Date Birthdate;
    global String Description;
    global User Owner;
    global Id OwnerId;
    global Boolean HasOptedOutOfEmail;
    global Boolean DoNotCall;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastCURequestDate;
    global Datetime LastCUUpdateDate;
    global String EmailBouncedReason;
    global Datetime EmailBouncedDate;
    global String Jigsaw;
    global String JigsawContactId;
    global Individual Individual;
    global Id IndividualId;
    global Boolean ckt__Opt_Out__c;
    global Date tecnics__Next_Birth_Day__c;
    global MC4SF__MC_Subscriber__c MC4SF__MC_Subscriber__c;
    global String CnP_PaaS__Alias_Contact_Data__c;
    global String CnP_PaaS__CnP_Connect_Alias_Index__c;
    global Double CnP_PaaS__CnP_Global_Rank__c;
    global Decimal CnP_PaaS__CnP_Total_Intrinsic__c;
    global Decimal CnP_PaaS__CnP_Total_extrinsic__c;
    global String CnP_PaaS__Connect_Link__c;

    global ContactChangeEvent () 
    {
    }
}