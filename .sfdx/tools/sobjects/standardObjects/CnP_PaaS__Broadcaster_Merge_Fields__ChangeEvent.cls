// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__Broadcaster_Merge_Fields__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global CnP_PaaS__Broadcaster_List__c CnP_PaaS__Broadcaster_List__c;
    global Double CnP_PaaS__Field_Id__c;
    global Boolean CnP_PaaS__Req__c;
    global String CnP_PaaS__Tag__c;
    global String CnP_PaaS__helptext__c;
    global String CnP_PaaS__order__c;
    global Boolean CnP_PaaS__show__c;
    global String CnP_PaaS__size__c;

    global CnP_PaaS__Broadcaster_Merge_Fields__ChangeEvent () 
    {
    }
}