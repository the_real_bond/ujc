// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class APXTConga4__Conga_Collection_Solution__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global APXTConga4__Conga_Collection__c APXTConga4__Conga_Collection__c;
    global APXTConga4__Conga_Solution__c APXTConga4__Conga_Solution__c;
    global String APXTConga4__Description__c;
    global Double APXTConga4__Sort_Order__c;

    global APXTConga4__Conga_Collection_Solution__ChangeEvent () 
    {
    }
}