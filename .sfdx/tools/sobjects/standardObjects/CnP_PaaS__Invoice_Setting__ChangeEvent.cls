// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__Invoice_Setting__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global String CnP_PaaS__Counter__c;
    global String CnP_PaaS__Counter_hidden__c;
    global String CnP_PaaS__DateValue1__c;
    global String CnP_PaaS__DateValue__c;
    global String CnP_PaaS__Date_Format__c;
    global String CnP_PaaS__Incrementer__c;
    global String CnP_PaaS__Prefix__c;
    global String CnP_PaaS__Seperator1__c;
    global String CnP_PaaS__Seperator__c;
    global String CnP_PaaS__SiteUrl__c;

    global CnP_PaaS__Invoice_Setting__ChangeEvent () 
    {
    }
}