// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class MC4SF__MC_Campaign_Hourly_Stats__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global MC4SF__MC_Campaign__c MC4SF__MC_Campaign__c;
    /* The total emails sent during the hour
    */
    global Double MC4SF__Emails_Sent__c;
    /* Unique clicks seen during the hour
    */
    global Double MC4SF__Recipients_Click__c;
    global Datetime MC4SF__Statistics_Hour__c;
    /* Unique opens seen during the hour
    */
    global Double MC4SF__Unique_Opens__c;

    global MC4SF__MC_Campaign_Hourly_Stats__ChangeEvent () 
    {
    }
}