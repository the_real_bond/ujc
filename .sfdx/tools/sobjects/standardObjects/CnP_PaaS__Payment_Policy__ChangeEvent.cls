// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__Payment_Policy__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Boolean CnP_PaaS__Additional_Payment__c;
    global Campaign CnP_PaaS__Campaign__c;
    global String CnP_PaaS__CnP_Reminder_Subject__c;
    global Decimal CnP_PaaS__Early_Amount__c;
    global String CnP_PaaS__Early_Days__c;
    global Decimal CnP_PaaS__Early_Discount__c;
    global Boolean CnP_PaaS__Early_Payment__c;
    global String CnP_PaaS__Invoice_Due__c;
    global CnP_PaaS__Invoice_Policy__c CnP_PaaS__Invoice_Polici__c;
    global Decimal CnP_PaaS__Late_Amount__c;
    global String CnP_PaaS__Late_Days__c;
    global Decimal CnP_PaaS__Late_Fee__c;
    global Boolean CnP_PaaS__Late_Payment__c;
    global String CnP_PaaS__Late_SKU__c;
    global String CnP_PaaS__Payment_Name__c;
    global Boolean CnP_PaaS__Remaindersent__c;
    global Boolean CnP_PaaS__Reminder__c;
    global String CnP_PaaS__SKU__c;
    global String CnP_PaaS__Send__c;
    global Double CnP_PaaS__Tax_Deductible__c;
    global Double CnP_PaaS__Tax__c;
    global String CnP_PaaS__Template__c;
    global Date CnP_PaaS__remainderdate__c;

    global CnP_PaaS__Payment_Policy__ChangeEvent () 
    {
    }
}