// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class APXT_CMQM__QuickMerge_Link__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global RecordType RecordType;
    global Id RecordTypeId;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    /* Enter a subject for the Task
    */
    global String APXT_CMQM__Activity_Subject__c;
    /* With this enabled, Conga Merge will automatically log an activity (based on the Log Activity Options) when you click to download Letters/Documents.
    */
    global Boolean APXT_CMQM__Automatic_Logging__c;
    /* Enable this field to bypass the Mass Merge wizard and proceed directly to the Download page
    */
    global Boolean APXT_CMQM__Bypass_Wizard__c;
    /* Paste a 15-character Campaign Id here to use its members as the data source
    */
    global String APXT_CMQM__Campaign_Id__c;
    /* Enable this field to set the default output type to PDF
    */
    global Boolean APXT_CMQM__Default_to_PDF__c;
    global String APXT_CMQM__Description__c;
    /* Enter a 15-character id of the record containing the document template.
    */
    global String APXT_CMQM__Document_Template_Id__c;
    /* Enter a 15-character ID of the record containing the envelope template.
    */
    global String APXT_CMQM__Envelope_Template_Id__c;
    /* Enter a value to indicate the number of days in the future to schedule a follow-up task
    */
    global Double APXT_CMQM__Follow_up_Interval__c;
    /* Enable this field to force the output type to PDF
    */
    global Boolean APXT_CMQM__Force_Output_as_PDF__c;
    /* Enter a 15-character ID of the record containing the label template.
    */
    global String APXT_CMQM__Label_Template_Id__c;
    global String APXT_CMQM__Launch__c;
    global String APXT_CMQM__Link_Type__c;
    /* Enable this field to store the merged body of the document in the Task record
    */
    global Boolean APXT_CMQM__LogWordBody__c;
    /* Enter other PointMerge parameters, using static values. You MAY NOT use Salesforce fields such as "{!Account.Name}". Use ampersands (&) between parameters.
For example:
&DefaultPDF=1&DS4=1
    */
    global String APXT_CMQM__Other_Parameters__c;
    /* Enter the Query Record ID (plus optional parameters). In a Mass Merge operation, this Query would represent the Detail data.
    */
    global String APXT_CMQM__Query_Id_1__c;
    /* Enter the Query Record ID (plus optional parameters). In a Mass Merge operation, this Query would represent the Master data.
    */
    global String APXT_CMQM__Query_Id__c;
    /* Enter 15-character report ids separated by commas. You may use "User.Id" (no quotes) as a value to pass. NOTE: MassMerge supports the use of only one report id.
    */
    global String APXT_CMQM__Report_Id__c;
    global String APXT_CMQM__Title__c;

    global APXT_CMQM__QuickMerge_Link__ChangeEvent () 
    {
    }
}