// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS_EVT__Event_Layout__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Boolean CnP_PaaS_EVT__Acknowledgement_mandatory__c;
    global String CnP_PaaS_EVT__Action_button_background__c;
    global String CnP_PaaS_EVT__Action_button_text__c;
    global String CnP_PaaS_EVT__Button_Label__c;
    global String CnP_PaaS_EVT__Button_font_family__c;
    global String CnP_PaaS_EVT__Button_font_size__c;
    global String CnP_PaaS_EVT__Custom_Page_Html__c;
    global Boolean CnP_PaaS_EVT__Display_Banner__c;
    global Boolean CnP_PaaS_EVT__Display_Description__c;
    global Boolean CnP_PaaS_EVT__Display_Footer__c;
    global Boolean CnP_PaaS_EVT__Display_Title_Information__c;
    global Boolean CnP_PaaS_EVT__Display_Title__c;
    global Datetime CnP_PaaS_EVT__End_date__c;
    global String CnP_PaaS_EVT__Engine_CSS__c;
    global CnP_PaaS_EVT__Event__c CnP_PaaS_EVT__Event_name__c;
    global String CnP_PaaS_EVT__Font_family__c;
    global String CnP_PaaS_EVT__Font_size__c;
    global String CnP_PaaS_EVT__Footer_Border_Color__c;
    global String CnP_PaaS_EVT__Footer_Text__c;
    global String CnP_PaaS_EVT__Footer_background__c;
    global String CnP_PaaS_EVT__Footer_information__c;
    global String CnP_PaaS_EVT__Logo_width__c;
    global String CnP_PaaS_EVT__Main_Section_Background_Color__c;
    global String CnP_PaaS_EVT__Main_Section_Border_Color__c;
    global String CnP_PaaS_EVT__Page_Section_Border_Color__c;
    global String CnP_PaaS_EVT__Page__c;
    global String CnP_PaaS_EVT__Page_background__c;
    global String CnP_PaaS_EVT__Page_header__c;
    global String CnP_PaaS_EVT__Page_name__c;
    global String CnP_PaaS_EVT__Section_Description__c;
    global String CnP_PaaS_EVT__Section_Headers_background__c;
    global String CnP_PaaS_EVT__Section_Title__c;
    global String CnP_PaaS_EVT__Section_font_family__c;
    global String CnP_PaaS_EVT__Section_font_size__c;
    global String CnP_PaaS_EVT__Section_header_title__c;
    global String CnP_PaaS_EVT__Section_headers_text__c;
    global String CnP_PaaS_EVT__Section_titleheader_background__c;
    global Boolean CnP_PaaS_EVT__Show_Page_Image__c;
    global Boolean CnP_PaaS_EVT__Show_ajenda__c;
    global Boolean CnP_PaaS_EVT__Show_terms_conditions__c;
    global Datetime CnP_PaaS_EVT__Start_date__c;
    global String CnP_PaaS_EVT__Term_Background_color__c;
    global String CnP_PaaS_EVT__Terms_conditions__c;
    global String CnP_PaaS_EVT__Theme_Selection__c;
    global String CnP_PaaS_EVT__Title_Section_Border_Color__c;
    global String CnP_PaaS_EVT__Title_and_information__c;
    global String CnP_PaaS_EVT__Upload_background_image__c;
    global String CnP_PaaS_EVT__Upload_banner__c;
    global String CnP_PaaS_EVT__Upload_logo__c;

    global CnP_PaaS_EVT__Event_Layout__ChangeEvent () 
    {
    }
}