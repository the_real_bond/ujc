// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Receipt__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Decimal IUA__c;
    global Date Receipt_Date__c;
    global Decimal UCF_Communal__c;
    global Decimal UCF_Education__c;
    global Decimal Welfare__c;
    global Decimal Total_Receipt_Amount__c;
    global Recurring_Donation__c Pledge__c;
    global String Trans_Key__c;
    global Account Donor__c;
    global String Receipt_Number__c;
    global String Payment_Status__c;
    global String Method_of_Payment__c;
    global String ReceiptEmail__c;
    global Boolean Do_Not_Send_Receipt__c;
    global Boolean Thank_you_Sent__c;
    global String TodayDate__c;
    global Double ReceiptCountUCFED__c;
    global Double ReceiptCount__c;
    global String Receipt_Date_Email__c;
    global Date Last_Receipt_Date__c;
    global Date Next_Receipt_Date__c;
    global Date Date_Signed_Receipts__c;
    global Datetime Create_Datetime__c;
    global String RecName__c;
    global Decimal Payment_Commission__c;
    global String Key_Formula__c;
    global String Key__c;
    global Double CC_Batch__c;
    /* Please insert the total value of the receipt.
    */
    global Decimal Receipt_Total_Checker__c;
    global Boolean Receipt_Query__c;
    global String Query_Reason__c;
    global Decimal Welfare_Outstanding__c;
    global Decimal IUA_Outstanding__c;
    global Decimal UCF_Communal_Outstanding__c;
    global Decimal UCF_Education_Outstanding__c;
    global String Query_Status__c;
    global String Bank_Code_Unpaid__c;
    global String Query_Resolution__c;
    global String Query_Notes__c;
    global String Reference_Details__c;
    global Decimal IUA_PriorValue__c;
    global String Prior_Campaign_Value__c;
    global Decimal Receipt_Total__c;
    global Decimal Welfare_Portion__c;
    global Decimal IUA_Portion__c;
    global Decimal UCF_Ed_Portion__c;
    global Decimal UCF_Comm_Portion__c;
    global Decimal Welfare_PriorValue__c;
    global Decimal UCF_Ed_PriorValue__c;
    global Decimal UCF_Comm_PriorValue__c;
    global Double Pledge_Prior_Value__c;
    global Date Modified_Date__c;
    global Date Created_Date__c;
    global Boolean Receipt_Adjustment__c;
    global String Receipt_Type__c;

    global Receipt__ChangeEvent () 
    {
    }
}