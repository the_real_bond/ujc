// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS_EVT__Event__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Boolean CnP_PaaS_EVT__Acknowledgement_Mandatory__c;
    global Boolean CnP_PaaS_EVT__Additional_Donation__c;
    global String CnP_PaaS_EVT__Additional_Fee__c;
    global Campaign CnP_PaaS_EVT__Additonal_Donation_Campaign__c;
    global String CnP_PaaS_EVT__Address_1__c;
    global String CnP_PaaS_EVT__Address_2__c;
    global String CnP_PaaS_EVT__Agenda_Display_Name__c;
    global Boolean CnP_PaaS_EVT__American_express__c;
    global Boolean CnP_PaaS_EVT__Anonymous__c;
    global String CnP_PaaS_EVT__Attendee_NameBadge_Email_Body__c;
    global Double CnP_PaaS_EVT__Available_Inventory__c;
    global String CnP_PaaS_EVT__C_P_Account_Id__c;
    global Campaign CnP_PaaS_EVT__Campaign__c;
    global Boolean CnP_PaaS_EVT__Check__c;
    global String CnP_PaaS_EVT__City__c;
    global String CnP_PaaS_EVT__Confirmation_message__c;
    global String CnP_PaaS_EVT__Country__c;
    global Boolean CnP_PaaS_EVT__Credit_card__c;
    global Boolean CnP_PaaS_EVT__Custom_Payment_Check__c;
    global String CnP_PaaS_EVT__Custom_Payment_Name__c;
    global String CnP_PaaS_EVT__Declined__c;
    global String CnP_PaaS_EVT__Discount_Type__c;
    global Boolean CnP_PaaS_EVT__Discover__c;
    global String CnP_PaaS_EVT__Display_Address__c;
    global Double CnP_PaaS_EVT__Donation_tax_deductible__c;
    global Datetime CnP_PaaS_EVT__End_date_and_time__c;
    global String CnP_PaaS_EVT__Event_Ended__c;
    global String CnP_PaaS_EVT__Event_Site_2v__c;
    global String CnP_PaaS_EVT__Event_description__c;
    global CnP_PaaS_EVT__Event_listing__c CnP_PaaS_EVT__Event_listing__c;
    global String CnP_PaaS_EVT__Event_title__c;
    global Boolean CnP_PaaS_EVT__Hide_First_Page_of_Registration__c;
    global Boolean CnP_PaaS_EVT__Hide_timer__c;
    global String CnP_PaaS_EVT__Internal_Notification__c;
    global Double CnP_PaaS_EVT__Inventory_sold__c;
    global Boolean CnP_PaaS_EVT__Invoice_P_O__c;
    global String CnP_PaaS_EVT__Label_for_additional_donation__c;
    global String CnP_PaaS_EVT__Maximum_capacity__c;
    global String CnP_PaaS_EVT__NameBadge_Email_Body__c;
    global String CnP_PaaS_EVT__Name_Badge_Email_FromName__c;
    global String CnP_PaaS_EVT__Name_Badge_Email_ReplyTo__c;
    global String CnP_PaaS_EVT__Organization_Information__c;
    global String CnP_PaaS_EVT__Organization__c;
    global String CnP_PaaS_EVT__Pdf_Name__c;
    global String CnP_PaaS_EVT__Price__c;
    global String CnP_PaaS_EVT__Public_site__c;
    global Datetime CnP_PaaS_EVT__Publish_date__c;
    global Boolean CnP_PaaS_EVT__Purchase_order__c;
    global String CnP_PaaS_EVT__Quantity__c;
    global String CnP_PaaS_EVT__Receipt_Header__c;
    global String CnP_PaaS_EVT__Receipt_Terms_Condition__c;
    global String CnP_PaaS_EVT__Registration_Timeout__c;
    global String CnP_PaaS_EVT__Registration_Type__c;
    global String CnP_PaaS_EVT__Registration_conformation_message__c;
    global String CnP_PaaS_EVT__Registrations_Included__c;
    global String CnP_PaaS_EVT__Report_Color__c;
    global String CnP_PaaS_EVT__SKU_for_additional_donation__c;
    global CnP_PaaS__CnP_Designer__c CnP_PaaS_EVT__Select_Template__c;
    global Boolean CnP_PaaS_EVT__Send_Agenda__c;
    global Boolean CnP_PaaS_EVT__Send_Receipt__c;
    global Boolean CnP_PaaS_EVT__Show_Terms_Conditions__c;
    global String CnP_PaaS_EVT__Site_Url__c;
    global String CnP_PaaS_EVT__Sold_Out__c;
    global Datetime CnP_PaaS_EVT__Start_date_and_time__c;
    global String CnP_PaaS_EVT__State__c;
    global String CnP_PaaS_EVT__Terms_Conditions__c;
    global String CnP_PaaS_EVT__Thank_You_Message_Receipt__c;
    global String CnP_PaaS_EVT__Thank_You__c;
    global Double CnP_PaaS_EVT__Ticket_Count_Number__c;
    global String CnP_PaaS_EVT__Ticket_Email_Body__c;
    global String CnP_PaaS_EVT__Ticket_Email_Subject__c;
    global Double CnP_PaaS_EVT__Ticket_Increment_Number__c;
    global Double CnP_PaaS_EVT__Ticket_Start_Number__c;
    global String CnP_PaaS_EVT__Ticket_email_ReplyTo__c;
    global String CnP_PaaS_EVT__Ticket_email_fromname__c;
    global String CnP_PaaS_EVT__Tickets_widget__c;
    global Double CnP_PaaS_EVT__Total_Inventory__c;
    global String CnP_PaaS_EVT__Total__c;
    global String CnP_PaaS_EVT__Venue_Name__c;
    global Boolean CnP_PaaS_EVT__Visa__c;
    global String CnP_PaaS_EVT__Zip__c;
    global CnP_PaaS_EVT__Event_category__c CnP_PaaS_EVT__category__c;
    global String CnP_PaaS_EVT__eTicket_Email_Body__c;
    global Boolean CnP_PaaS_EVT__e_check__c;
    global Contact CnP_PaaS_EVT__event_coordinator__c;
    global String CnP_PaaS_EVT__iFrame_Code_2__c;
    global String CnP_PaaS_EVT__iFrame_Code__c;
    global Boolean CnP_PaaS_EVT__master_card__c;
    global String CnP_PaaS_EVT__name_Badge_Email_Subject__c;

    global CnP_PaaS_EVT__Event__ChangeEvent () 
    {
    }
}