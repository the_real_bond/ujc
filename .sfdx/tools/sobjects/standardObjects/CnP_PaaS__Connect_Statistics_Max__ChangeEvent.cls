// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__Connect_Statistics_Max__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Double CnP_PaaS__Donor_Personal_Donation_Amount_Rank_Max__c;
    global Double CnP_PaaS__Donor_Personal_Donation_Count_Rank_Max__c;
    global Double CnP_PaaS__Donor_Raised_Donation_Amount_Rank_Max__c;
    global Double CnP_PaaS__Donor_Raised_Donation_Count_Rank_Max__c;
    global Double CnP_PaaS__Donors_Rank_Factor_Max__c;
    global String CnP_PaaS__Job_Status__c;
    global Decimal CnP_PaaS__My_Donor_Personal_Donation_Amount_Max__c;
    global Double CnP_PaaS__My_Donor_Personal_Donation_Count_Max__c;
    global Decimal CnP_PaaS__My_Donor_Raised_Donation_Amount_Max__c;
    global Double CnP_PaaS__My_Donor_Raised_Donation_Count_Max__c;
    global Double CnP_PaaS__My_Rank_Factor_Max__c;
    global Decimal CnP_PaaS__Personal_Donation_Amount_Max__c;
    global Double CnP_PaaS__Personal_Donation_Amount_Rank_Max__c;
    global Double CnP_PaaS__Personal_Donation_Count_Max__c;
    global Double CnP_PaaS__Personal_Donation_Count_Rank_Max__c;
    global Decimal CnP_PaaS__Raised_Donation_Amount_Max__c;
    global Double CnP_PaaS__Raised_Donation_Amount_Rank_Max__c;
    global Double CnP_PaaS__Raised_Donation_Count_Max__c;
    global Double CnP_PaaS__Raised_Donation_Count_Rank_Max__c;
    global Decimal CnP_PaaS__Total_Intrinsic_Value_Max__c;
    global Double CnP_PaaS__Total_Records__c;
    global Decimal CnP_PaaS__Total__c;
    global Decimal CnP_PaaS__Total_extrinsic_value_Max__c;

    global CnP_PaaS__Connect_Statistics_Max__ChangeEvent () 
    {
    }
}