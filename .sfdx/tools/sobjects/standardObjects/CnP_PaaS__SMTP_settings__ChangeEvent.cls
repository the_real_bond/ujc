// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__SMTP_settings__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global String CnP_PaaS__API_User__c;
    global String CnP_PaaS__API_key__c;
    global Boolean CnP_PaaS__Enable_SSL__c;
    global String CnP_PaaS__Outgoing_Mail_Server_SMTP__c;
    global Boolean CnP_PaaS__Send_grid__c;
    global String CnP_PaaS__Sender_Email__c;
    global String CnP_PaaS__Sender_Name__c;
    global String CnP_PaaS__Server_Port_Number_SMTP__c;

    global CnP_PaaS__SMTP_settings__ChangeEvent () 
    {
    }
}