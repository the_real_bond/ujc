// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class APXTConga4__Conga_Solution__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global String APXTConga4__Button_Link_API_Name__c;
    global String APXTConga4__Button_body_field__c;
    global String APXTConga4__Composer_Parameters__c;
    global String APXTConga4__Custom_Object_Id__c;
    global String APXTConga4__Formula_Field_API_Name__c;
    global String APXTConga4__Formula_body_field__c;
    global String APXTConga4__Is_Quick_Start__c;
    global String APXTConga4__Launch_C8_Formula_Button__c;
    global String APXTConga4__Master_Object_Type_Validator__c;
    global String APXTConga4__Master_Object_Type__c;
    global String APXTConga4__Sample_Record_Id__c;
    global String APXTConga4__Sample_Record_Name__c;
    global String APXTConga4__Solution_Description__c;
    global String APXTConga4__Solution_Weblink_Syntax__c;
    global String APXTConga4__Weblink_Id__c;
    global Double APXTConga4__CongaEmailTemplateCount__c;

    global APXTConga4__Conga_Solution__ChangeEvent () 
    {
    }
}