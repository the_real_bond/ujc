// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__Broadcaster_Stats__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global CnP_PaaS__Broadcaster_Campaign__c CnP_PaaS__Broadcaster_Campaign__c;
    global Double CnP_PaaS__Abuse_Reports__c;
    global Double CnP_PaaS__Clicks__c;
    global Double CnP_PaaS__Emails_Sent__c;
    global Double CnP_PaaS__Facebook_Likes__c;
    global Double CnP_PaaS__Forwards_Opens__c;
    global Double CnP_PaaS__Forwards__c;
    global Double CnP_PaaS__Hard_Bounces__c;
    global String CnP_PaaS__Last_Click__c;
    global String CnP_PaaS__Last_Open__c;
    global Double CnP_PaaS__Opens__c;
    global Double CnP_PaaS__Recipient_Likes__c;
    global Double CnP_PaaS__Soft_Bounces__c;
    global Double CnP_PaaS__Syntax_Errors__c;
    global Double CnP_PaaS__Unique_Clicks__c;
    global Double CnP_PaaS__Unique_Likes__c;
    global Double CnP_PaaS__Unique_Opens__c;
    global Double CnP_PaaS__Unsubscribes__c;
    global Double CnP_PaaS__Users_Who_Cclicked__c;

    global CnP_PaaS__Broadcaster_Stats__ChangeEvent () 
    {
    }
}