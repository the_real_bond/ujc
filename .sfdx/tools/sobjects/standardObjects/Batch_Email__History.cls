// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Batch_Email__History {
    global Id Id;
    global Boolean IsDeleted;
    global Batch_Email__c Parent;
    global Id ParentId;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global String Field;
    global Object OldValue;
    global Object NewValue;

    global Batch_Email__History () 
    {
    }
}