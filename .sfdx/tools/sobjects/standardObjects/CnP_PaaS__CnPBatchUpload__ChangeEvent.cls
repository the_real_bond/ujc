// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__CnPBatchUpload__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Double CnP_PaaS__AuthorizeTransactions__c;
    global String CnP_PaaS__CnPAID__c;
    global Double CnP_PaaS__DeclineTransactions__c;
    global Double CnP_PaaS__Errors__c;
    global String CnP_PaaS__FileContent__c;
    global String CnP_PaaS__FileHeader__c;
    global String CnP_PaaS__FileName__c;
    global Datetime CnP_PaaS__ProcessingDate__c;
    global String CnP_PaaS__Upload_Status__c;
    global String CnP_PaaS__Username__c;

    global CnP_PaaS__CnPBatchUpload__ChangeEvent () 
    {
    }
}