// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Tax_Receipt__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global String AccountDate__c;
    global String AccountEmail__c;
    global String Account_Donor_Id__c;
    global String Account_Name__c;
    global Account Account__c;
    global String Amount_in_Words__c;
    global String Attention__c;
    global String Certificate_Number__c;
    global String CloseDatemonthandYear__c;
    global Contact Contact__c;
    global Date Date_closed__c;
    global Boolean Email_Check__c;
    global Date Financial_Year_End__c;
    global Date Financial_Year_Start__c;
    global Boolean IsClosed__c;
    global String ReceiptNumber__c;
    global Date Receipt_Date__c;
    global String Tax_Certificate_Issued_to__c;
    global String TodayDate__c;
    global String Type__c;
    global String YearEnd__c;
    global String Year_Start_Words__c;
    global String Year_end_Words__c;
    global String YearendYear__c;
    global Decimal Total_Receipt_Amount__c;
    global String Donor_Status__c;

    global Tax_Receipt__ChangeEvent () 
    {
    }
}