// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class APXTConga4__Conga_Solution_Query__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global APXTConga4__Conga_Solution__c APXTConga4__Conga_Solution__c;
    global String APXTConga4__Alias__c;
    global String APXTConga4__Comments__c;
    global String APXTConga4__Conga_Query_Name__c;
    global APXTConga4__Conga_Merge_Query__c APXTConga4__Conga_Query__c;
    /* Automatically populated according to the field selected. This value will be dynamically passed to the first filter of the query. Using a plus sign (+) specifies that a blank value should be used.
    */
    global String APXTConga4__pv0__c;
    /* Automatically populated according to the field selected. This value will be dynamically passed to the second filter of the query. Using a plus sign (+) specifies that a blank value should be used.
    */
    global String APXTConga4__pv1__c;
    /* Automatically populated according to the field selected. This value will be dynamically passed to the third filter of the query. Using a plus sign (+) specifies that a blank value should be used.
    */
    global String APXTConga4__pv2__c;

    global APXTConga4__Conga_Solution_Query__ChangeEvent () 
    {
    }
}