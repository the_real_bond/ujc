// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__Soft_Credit__ChangeEvent {
    global Id Id;
    global String ReplayId;
    global Object ChangeEventHeader;
    global SObject Owner;
    global Id OwnerId;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global CnP_PaaS__CnP_Transaction__c CnP_PaaS__C_P_Transaction__c;
    global Contact CnP_PaaS__Contact__c;
    global String CnP_PaaS__Object_record__c;
    global Opportunity CnP_PaaS__Opportunity__c;
    global String CnP_PaaS__Reciprocal_Relationship__c;
    global Account CnP_PaaS__Related_Account__c;
    global Contact CnP_PaaS__Related_Contact__c;
    global String CnP_PaaS__Relationship__c;
    global String CnP_PaaS__SKU_Filter__c;
    global String CnP_PaaS__SKU__c;
    global String CnP_PaaS__Status__c;
    global Decimal CnP_PaaS__Total_Soft_Credit__c;

    global CnP_PaaS__Soft_Credit__ChangeEvent () 
    {
    }
}