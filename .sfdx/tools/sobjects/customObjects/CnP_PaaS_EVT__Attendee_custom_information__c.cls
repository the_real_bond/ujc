// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS_EVT__Attendee_custom_information__c {
    global Id Id;
    global SObject Owner;
    global Id OwnerId;
    global Boolean IsDeleted;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global String CnP_PaaS_EVT__Answer__c;
    global CnP_PaaS_EVT__Custom_fields__c CnP_PaaS_EVT__C_P_Custom_Field__r;
    global Id CnP_PaaS_EVT__C_P_Custom_Field__c;
    global CnP_PaaS_EVT__Event_registrant_session__c CnP_PaaS_EVT__C_P_Event_Registrant__r;
    global Id CnP_PaaS_EVT__C_P_Event_Registrant__c;
    global Contact CnP_PaaS_EVT__Contact__r;
    global Id CnP_PaaS_EVT__Contact__c;
    global CnP_PaaS_EVT__Event__c CnP_PaaS_EVT__Event_name__r;
    global Id CnP_PaaS_EVT__Event_name__c;
    global String CnP_PaaS_EVT__Field_name__c;
    global String CnP_PaaS_EVT__Field_type__c;
    global String CnP_PaaS_EVT__Field_value__c;
    global CnP_PaaS_EVT__Registration_level__c CnP_PaaS_EVT__Registration_level__r;
    global Id CnP_PaaS_EVT__Registration_level__c;
    global String CnP_PaaS_EVT__Section_name__c;
    global CnP_PaaS_EVT__Event_attendee_session__c CnP_PaaS_EVT__attendee_session_Id__r;
    global Id CnP_PaaS_EVT__attendee_session_Id__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<Attachment> Attachments;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<NetworkActivityAudit> ParentEntities;
    global List<NetworkUserHistoryRecent> NetworkUserHistoryRecentToRecord;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordAction> RecordActions;
    global List<RecordActionHistory> RecordActionHistories;
    global List<TopicAssignment> TopicAssignments;
    global List<ContentVersion> FirstPublishLocation;
    global List<EventChangeEvent> What;
    global List<EventRelationChangeEvent> Relation;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;
    global List<TaskChangeEvent> What;

    global CnP_PaaS_EVT__Attendee_custom_information__c () 
    {
    }
}