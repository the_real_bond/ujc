// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__CnP_Custom_Question_Settings__c {
    global Id Id;
    global SObject Owner;
    global Id OwnerId;
    global Boolean IsDeleted;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global Date LastActivityDate;
    global String CnP_PaaS__Answer_Condition__c;
    global Account CnP_PaaS__Assigned_Account__r;
    global Id CnP_PaaS__Assigned_Account__c;
    global Contact CnP_PaaS__Assigned_Contact__r;
    global Id CnP_PaaS__Assigned_Contact__c;
    global String CnP_PaaS__Condition__c;
    global String CnP_PaaS__Contact_Role__c;
    global Contact CnP_PaaS__Contact__r;
    global Id CnP_PaaS__Contact__c;
    global Double CnP_PaaS__Date_Now_Minus_Number__c;
    global Double CnP_PaaS__Date_Now_Plus_Number__c;
    global String CnP_PaaS__Days__c;
    global String CnP_PaaS__Field_Map__c;
    global String CnP_PaaS__Object_Map__c;
    global String CnP_PaaS__Object_Parent_Id__c;
    global String CnP_PaaS__Object_Record__c;
    global String CnP_PaaS__Original_Answer__c;
    global String CnP_PaaS__Original_Relation__c;
    global String CnP_PaaS__Question_Condition__c;
    global String CnP_PaaS__Reciprocal_Relationship__c;
    global String CnP_PaaS__Replaced_Answer__c;
    global String CnP_PaaS__SKU_Condition__c;
    global String CnP_PaaS__SKUvalue__c;
    global String CnP_PaaS__Select_Date__c;
    global String CnP_PaaS__Sku__c;
    global String CnP_PaaS__Soft_Credit_Setting__c;
    global Date CnP_PaaS__Status_Change_Date__c;
    global String CnP_PaaS__Status__c;
    global String CnP_PaaS__Type__c;
    global String CnP_PaaS__date_entered__c;
    global Double CnP_PaaS__soft_credit_percent__c;
    global Boolean CnP_PaaS__softcredit__c;
    global List<ActivityHistory> ActivityHistories;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<Attachment> Attachments;
    global List<CnP_PaaS__CnP_Custom_Question_Settings__History> Histories;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EmailMessage> Emails;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<Event> Events;
    global List<NetworkActivityAudit> ParentEntities;
    global List<NetworkUserHistoryRecent> NetworkUserHistoryRecentToRecord;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordAction> RecordActions;
    global List<RecordActionHistory> RecordActionHistories;
    global List<Task> Tasks;
    global List<TopicAssignment> TopicAssignments;
    global List<ContentVersion> FirstPublishLocation;
    global List<EventChangeEvent> What;
    global List<EventRelationChangeEvent> Relation;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;
    global List<TaskChangeEvent> What;

    global CnP_PaaS__CnP_Custom_Question_Settings__c () 
    {
    }
}