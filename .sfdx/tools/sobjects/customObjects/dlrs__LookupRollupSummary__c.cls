// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class dlrs__LookupRollupSummary__c {
    global Id Id;
    global SObject Owner;
    global Id OwnerId;
    global Boolean IsDeleted;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global Datetime LastViewedDate;
    global Datetime LastReferencedDate;
    /* For Realtime rollups can only be set when the Child Apex Trigger has been deployed.
    */
    global Boolean dlrs__Active__c;
    /* Rollup operation.
    */
    global String dlrs__AggregateOperation__c;
    /* API name of the field that will store the result of the rollup on the Parent Object, e.g. AnnualRevenue
    */
    global String dlrs__AggregateResultField__c;
    /* This field is used by the system when using the Calculate button to track if a calculation job is already running. Clear this field if the system reports the calculate job is already running and you known this is not the case.
    */
    global String dlrs__CalculateJobId__c;
    /* Realtime mode requires an Apex Trigger to be deployed for the Child Object. Click Manage Child Trigger button to deploy.
    */
    global String dlrs__CalculationMode__c;
    /* API name of the Child Object, e.g. Opportunity
    */
    global String dlrs__ChildObject__c;
    /* API name of the field on the Child Object that contains the value to rollup, e.g. Amount
    */
    global String dlrs__FieldToAggregate__c;
    /* API name of the Parent Object, e.g. Account
    */
    global String dlrs__ParentObject__c;
    /* If you have specified a relationship criteria, you must confirm the fields referenced by it here on separate lines, for example for criteria StageName = 'Won' list StageName in this field. You do not need to specify the Field to Aggregate field however.
    */
    global String dlrs__RelationshipCriteriaFields__c;
    /* SOQL WHERE clause applied when querying Child Object records, e.g. Amount > 200
    */
    global String dlrs__RelationshipCriteria__c;
    /* API name of the Lookup field on the Child Object relating to the Parent Object, e.g. AccountId
    */
    global String dlrs__RelationshipField__c;
    /* Includes child records that have been archived by the system and/or placed in the recycle bin.
    */
    global Boolean dlrs__AggregateAllRows__c;
    /* Determines if the Sharing Rules defined on the Child Object are considered when calculating the rollup. Default is User.
    */
    global String dlrs__CalculationSharingMode__c;
    /* Enter the character or characters to delimit values in the Field to Aggregate when rolling up text values into the Aggregate Result Field, enter BR() for new line. Only applies when using Concatenate operation.
    */
    global String dlrs__ConcatenateDelimiter__c;
    global String dlrs__Description__c;
    /* Only applicable when using the Concatenate, Concatenate Distinct, Last and First aggregate operations. Supports multiple fields (comma separated) with optional ASC/DESC and/or NULLS FIRST/LAST.
    */
    global String dlrs__FieldToOrderBy__c;
    /* Limits the number of rows used in the rollup. Applies only to the Last and Concatenate operators.
    */
    global Double dlrs__RowLimit__c;
    /* Only use this option as a last resort to get the generated test or custom test code working. It is generally considered bad practice to make your test dependent on org data. Always try to create test data in the test code if possible.
    */
    global Boolean dlrs__TestCodeSeeAllData__c;
    /* This tool utilises a dynamically generated Apex Trigger and by default a generated Apex Test. Use this field if instructed by a developer to replace the generated test code. For more information see the tools Wiki on GitHub.
    */
    global String dlrs__TestCode__c;
    global String dlrs__UniqueName__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<Attachment> Attachments;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<NetworkActivityAudit> ParentEntities;
    global List<NetworkUserHistoryRecent> NetworkUserHistoryRecentToRecord;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordAction> RecordActions;
    global List<RecordActionHistory> RecordActionHistories;
    global List<TopicAssignment> TopicAssignments;
    global List<dlrs__LookupRollupSummaryScheduleItems__c> dlrs__LookupRollupSummaryScheduleQueues__r;
    global List<ContentVersion> FirstPublishLocation;
    global List<EventChangeEvent> What;
    global List<EventRelationChangeEvent> Relation;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;
    global List<TaskChangeEvent> What;

    global dlrs__LookupRollupSummary__c () 
    {
    }
}