// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__Payment_Policy__c {
    global Id Id;
    global SObject Owner;
    global Id OwnerId;
    global Boolean IsDeleted;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global Boolean CnP_PaaS__Additional_Payment__c;
    global Campaign CnP_PaaS__Campaign__r;
    global Id CnP_PaaS__Campaign__c;
    global String CnP_PaaS__CnP_Reminder_Subject__c;
    global Decimal CnP_PaaS__Early_Amount__c;
    global String CnP_PaaS__Early_Days__c;
    global Decimal CnP_PaaS__Early_Discount__c;
    global Boolean CnP_PaaS__Early_Payment__c;
    global String CnP_PaaS__Invoice_Due__c;
    global CnP_PaaS__Invoice_Policy__c CnP_PaaS__Invoice_Polici__r;
    global Id CnP_PaaS__Invoice_Polici__c;
    global Decimal CnP_PaaS__Late_Amount__c;
    global String CnP_PaaS__Late_Days__c;
    global Decimal CnP_PaaS__Late_Fee__c;
    global Boolean CnP_PaaS__Late_Payment__c;
    global String CnP_PaaS__Late_SKU__c;
    global String CnP_PaaS__Payment_Name__c;
    global Boolean CnP_PaaS__Remaindersent__c;
    global Boolean CnP_PaaS__Reminder__c;
    global String CnP_PaaS__SKU__c;
    global String CnP_PaaS__Send__c;
    global Double CnP_PaaS__Tax_Deductible__c;
    global Double CnP_PaaS__Tax__c;
    global String CnP_PaaS__Template__c;
    global Date CnP_PaaS__remainderdate__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<Attachment> Attachments;
    global List<CnP_PaaS__Invoice_Schedule__c> CnP_PaaS__C_P_Invoice_Schedule_del__r;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<NetworkActivityAudit> ParentEntities;
    global List<NetworkUserHistoryRecent> NetworkUserHistoryRecentToRecord;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordAction> RecordActions;
    global List<RecordActionHistory> RecordActionHistories;
    global List<TopicAssignment> TopicAssignments;
    global List<ContentVersion> FirstPublishLocation;
    global List<EventChangeEvent> What;
    global List<EventRelationChangeEvent> Relation;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;
    global List<TaskChangeEvent> What;

    global CnP_PaaS__Payment_Policy__c () 
    {
    }
}