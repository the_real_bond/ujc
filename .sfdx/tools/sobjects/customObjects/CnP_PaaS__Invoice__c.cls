// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__Invoice__c {
    global Id Id;
    global SObject Owner;
    global Id OwnerId;
    global Boolean IsDeleted;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global Date LastActivityDate;
    global Boolean CnP_PaaS__Addship__c;
    global Boolean CnP_PaaS__Allow_for_Editing__c;
    global String CnP_PaaS__Billing_Address2__c;
    global String CnP_PaaS__Billing_Address__c;
    global CnP_PaaS__CnP_Transaction__c CnP_PaaS__C_P_Transaction__r;
    global Id CnP_PaaS__C_P_Transaction__c;
    global String CnP_PaaS__Campaign_Text__c;
    global String CnP_PaaS__City__c;
    global Decimal CnP_PaaS__Convenience_Fee__c;
    global String CnP_PaaS__Country__c;
    global String CnP_PaaS__Currency_Code__c;
    global String CnP_PaaS__Email__c;
    global Date CnP_PaaS__Final_Date__c;
    global String CnP_PaaS__First_Name__c;
    global String CnP_PaaS__Guid__c;
    global String CnP_PaaS__Invoice_Account_GUID__c;
    global String CnP_PaaS__Invoice_Account_Id__c;
    global Boolean CnP_PaaS__Invoice_Check__c;
    global Contact CnP_PaaS__Invoice_Contact__r;
    global Id CnP_PaaS__Invoice_Contact__c;
    global String CnP_PaaS__Invoice_Number__c;
    global CnP_PaaS__Invoice_Policy__c CnP_PaaS__Invoice_Policy__r;
    global Id CnP_PaaS__Invoice_Policy__c;
    global String CnP_PaaS__Last_Name__c;
    global String CnP_PaaS__Organization_Information__c;
    global String CnP_PaaS__Phone__c;
    global Boolean CnP_PaaS__Sameship__c;
    global Boolean CnP_PaaS__Send_Receipt__c;
    global String CnP_PaaS__ShipCountry__c;
    global String CnP_PaaS__ShipName__c;
    global String CnP_PaaS__Ship_Address2__c;
    global String CnP_PaaS__Ship_Address__c;
    global String CnP_PaaS__Ship_Lastname__c;
    global String CnP_PaaS__Shipcode__c;
    global Boolean CnP_PaaS__Shipinvoice__c;
    global String CnP_PaaS__Shipphone__c;
    global String CnP_PaaS__Shipping_Email__c;
    global String CnP_PaaS__State__c;
    global String CnP_PaaS__Status__c;
    global CnP_PaaS__CnP_Designer__c CnP_PaaS__TemplateName__r;
    global Id CnP_PaaS__TemplateName__c;
    global String CnP_PaaS__Terms_Conditions__c;
    global String CnP_PaaS__Thank_you__c;
    global String CnP_PaaS__Tracker__c;
    global CnP_PaaS__CnP_Designer__c CnP_PaaS__WebTemplate__r;
    global Id CnP_PaaS__WebTemplate__c;
    global String CnP_PaaS__Zip_Code__c;
    global Date CnP_PaaS__invoicedate__c;
    global Boolean CnP_PaaS__invoicesent__c;
    global Date CnP_PaaS__sent_date__c;
    global String CnP_PaaS__sent_status__c;
    global String CnP_PaaS__shipState__c;
    global String CnP_PaaS__shipcity__c;
    global Decimal CnP_PaaS__shipping__c;
    global Date CnP_PaaS__specific_date__c;
    global Date CnP_PaaS__td_Date__c;
    global List<ActivityHistory> ActivityHistories;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<Attachment> Attachments;
    global List<CnP_PaaS__Invoice_Items__c> CnP_PaaS__Invoice_Items__r;
    global List<CnP_PaaS__Invoice_Schedule__c> CnP_PaaS__C_P_Invoice_Schedule_del__r;
    global List<CnP_PaaS__Invoice__History> Histories;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EmailMessage> Emails;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<Event> Events;
    global List<NetworkActivityAudit> ParentEntities;
    global List<NetworkUserHistoryRecent> NetworkUserHistoryRecentToRecord;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global List<Opportunity> CnP_PaaS__Opportunities__r;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordAction> RecordActions;
    global List<RecordActionHistory> RecordActionHistories;
    global List<Task> Tasks;
    global List<TopicAssignment> TopicAssignments;
    global List<ContentVersion> FirstPublishLocation;
    global List<EventChangeEvent> What;
    global List<EventRelationChangeEvent> Relation;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;
    global List<TaskChangeEvent> What;

    global CnP_PaaS__Invoice__c () 
    {
    }
}