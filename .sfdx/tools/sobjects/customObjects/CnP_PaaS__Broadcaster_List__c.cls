// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__Broadcaster_List__c {
    global Id Id;
    global Boolean IsDeleted;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global Date LastActivityDate;
    global CnP_PaaS__BroadCaster_Service__c CnP_PaaS__BroadCaster_Service__r;
    global Id CnP_PaaS__BroadCaster_Service__c;
    global Double CnP_PaaS__Avg_Sub_Rate__c;
    global Double CnP_PaaS__Avg_Unsub_Rate__c;
    global String CnP_PaaS__Beamer_Address__c;
    global Double CnP_PaaS__Campaign_Count__c;
    global Double CnP_PaaS__Cleaned_Count_Since_Send__c;
    global Double CnP_PaaS__Cleaned_Count__c;
    global Double CnP_PaaS__Click_Rate__c;
    global CnP_PaaS__DynamicReport__c CnP_PaaS__CnP_Dynamic_Report__r;
    global Id CnP_PaaS__CnP_Dynamic_Report__c;
    global String CnP_PaaS__Date_Created__c;
    global String CnP_PaaS__Default_From_Email__c;
    global String CnP_PaaS__Default_From_Name__c;
    global String CnP_PaaS__Default_Language__c;
    global String CnP_PaaS__Default_Subject__c;
    global Boolean CnP_PaaS__Email_Type_Option__c;
    global Double CnP_PaaS__Group_Count__c;
    global Double CnP_PaaS__Grouping_Count__c;
    global String CnP_PaaS__List_Id__c;
    global Double CnP_PaaS__List_Rating__c;
    global Double CnP_PaaS__Member_Count_Since_Send__c;
    global Double CnP_PaaS__Member_Count__c;
    global Double CnP_PaaS__Merge_Var_Count__c;
    global Double CnP_PaaS__Open_Rate__c;
    global String CnP_PaaS__Subscribe_Url_Long__c;
    global String CnP_PaaS__Subscribe_Url_Short__c;
    global Double CnP_PaaS__Target_Sub_Rate__c;
    global Double CnP_PaaS__Unsubscribe_Count_Since_Send__c;
    global Double CnP_PaaS__Unsubscribe_Count__c;
    global Boolean CnP_PaaS__Use_Awesomebar__c;
    global String CnP_PaaS__Visibility__c;
    global Double CnP_PaaS__Web_Id__c;
    global List<ActivityHistory> ActivityHistories;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<Attachment> Attachments;
    global List<CnP_PaaS__Broadcaster_Campaign__c> CnP_PaaS__Broadcaster_Campaign__r;
    global List<CnP_PaaS__Broadcaster_List__History> Histories;
    global List<CnP_PaaS__Broadcaster_Member_Activity__c> CnP_PaaS__Broadcaster_Member_Activity__r;
    global List<CnP_PaaS__Broadcaster_Merge_Fields__c> CnP_PaaS__Broadcaster_Merge_Fields__r;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EmailMessage> Emails;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<Event> Events;
    global List<NetworkActivityAudit> ParentEntities;
    global List<NetworkUserHistoryRecent> NetworkUserHistoryRecentToRecord;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordAction> RecordActions;
    global List<RecordActionHistory> RecordActionHistories;
    global List<Task> Tasks;
    global List<TopicAssignment> TopicAssignments;
    global List<ContentVersion> FirstPublishLocation;
    global List<EventChangeEvent> What;
    global List<EventRelationChangeEvent> Relation;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;
    global List<TaskChangeEvent> What;

    global CnP_PaaS__Broadcaster_List__c () 
    {
    }
}