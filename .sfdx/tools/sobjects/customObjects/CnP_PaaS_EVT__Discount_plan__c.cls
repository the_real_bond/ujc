// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS_EVT__Discount_plan__c {
    global Id Id;
    global SObject Owner;
    global Id OwnerId;
    global Boolean IsDeleted;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global Double CnP_PaaS_EVT__Available_Inventory__c;
    global CnP_PaaS_EVT__Discount_plan__c CnP_PaaS_EVT__C_P_Discount_Plan__r;
    global Id CnP_PaaS_EVT__C_P_Discount_Plan__c;
    global String CnP_PaaS_EVT__Coupon_Code_value__c;
    global String CnP_PaaS_EVT__Coupon_code__c;
    global String CnP_PaaS_EVT__End_of_plan__c;
    global CnP_PaaS_EVT__Event__c CnP_PaaS_EVT__Event_name__r;
    global Id CnP_PaaS_EVT__Event_name__c;
    global Double CnP_PaaS_EVT__Inventory_Sold__c;
    global Decimal CnP_PaaS_EVT__Max_Amount__c;
    global String CnP_PaaS_EVT__Max_number__c;
    global Decimal CnP_PaaS_EVT__Min_Amount__c;
    global String CnP_PaaS_EVT__Min_number__c;
    global CnP_PaaS_EVT__Registration_level__c CnP_PaaS_EVT__Registration_level__r;
    global Id CnP_PaaS_EVT__Registration_level__c;
    global Datetime CnP_PaaS_EVT__Start_date__c;
    global Double CnP_PaaS_EVT__Total_Inventory__c;
    global Double CnP_PaaS_EVT__Total_discount__c;
    global String CnP_PaaS_EVT__discount__c;
    global Datetime CnP_PaaS_EVT__expire_date__c;
    global String CnP_PaaS_EVT__fixed_discount__c;
    global String CnP_PaaS_EVT__inventory__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<Attachment> Attachments;
    global List<CnP_PaaS_EVT__C_P_Event_LevelGroup__c> CnP_PaaS_EVT__C_P_Event_Level_Groups__r;
    global List<CnP_PaaS_EVT__Discount_plan__c> CnP_PaaS_EVT__C_P_Discount_Plans__r;
    global List<CnP_PaaS_EVT__Event_attendee_session__c> CnP_PaaS_EVT__C_P_Registered_Attendees__r;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<NetworkActivityAudit> ParentEntities;
    global List<NetworkUserHistoryRecent> NetworkUserHistoryRecentToRecord;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordAction> RecordActions;
    global List<RecordActionHistory> RecordActionHistories;
    global List<TopicAssignment> TopicAssignments;
    global List<ContentVersion> FirstPublishLocation;
    global List<EventChangeEvent> What;
    global List<EventRelationChangeEvent> Relation;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;
    global List<TaskChangeEvent> What;

    global CnP_PaaS_EVT__Discount_plan__c () 
    {
    }
}