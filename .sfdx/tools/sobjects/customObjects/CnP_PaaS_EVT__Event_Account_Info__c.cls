// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS_EVT__Event_Account_Info__c {
    global Id Id;
    global SObject Owner;
    global Id OwnerId;
    global Boolean IsDeleted;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global Boolean CnP_PaaS_EVT__American_Express__c;
    global CnP_PaaS__CnP_API_Settings__c CnP_PaaS_EVT__C_P_API_Settings__r;
    global Id CnP_PaaS_EVT__C_P_API_Settings__c;
    global CnP_PaaS_EVT__Event__c CnP_PaaS_EVT__C_P_Event__r;
    global Id CnP_PaaS_EVT__C_P_Event__c;
    global Boolean CnP_PaaS_EVT__Credit_Card__c;
    global String CnP_PaaS_EVT__Currency__c;
    global String CnP_PaaS_EVT__Currency_label__c;
    global Boolean CnP_PaaS_EVT__Custom_Payment_Check__c;
    global String CnP_PaaS_EVT__Custom_Payment_Name__c;
    global Boolean CnP_PaaS_EVT__Discover__c;
    global String CnP_PaaS_EVT__Free_Payment__c;
    global Boolean CnP_PaaS_EVT__Invoice__c;
    global Boolean CnP_PaaS_EVT__JCB__c;
    global Boolean CnP_PaaS_EVT__Master_Card__c;
    global Boolean CnP_PaaS_EVT__Purchase_Order__c;
    global Boolean CnP_PaaS_EVT__Visa__c;
    global Boolean CnP_PaaS_EVT__eCheck__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<Attachment> Attachments;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<NetworkActivityAudit> ParentEntities;
    global List<NetworkUserHistoryRecent> NetworkUserHistoryRecentToRecord;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordAction> RecordActions;
    global List<RecordActionHistory> RecordActionHistories;
    global List<TopicAssignment> TopicAssignments;
    global List<ContentVersion> FirstPublishLocation;
    global List<EventChangeEvent> What;
    global List<EventRelationChangeEvent> Relation;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;
    global List<TaskChangeEvent> What;

    global CnP_PaaS_EVT__Event_Account_Info__c () 
    {
    }
}