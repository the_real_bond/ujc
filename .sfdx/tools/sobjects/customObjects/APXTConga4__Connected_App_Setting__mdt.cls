// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class APXTConga4__Connected_App_Setting__mdt {
    global Id Id;
    global String DeveloperName;
    global String MasterLabel;
    global String Language;
    global String NamespacePrefix;
    global String Label;
    global String QualifiedApiName;
    global String APXTConga4__Connected_App_Name_SF1__c;
    global String APXTConga4__Connected_App_Name__c;
    global String APXTConga4__Hostname__c;
    global List<APXTConga4__Composer_Setting__mdt> APXTConga4__Composer_Settings__r;

    global APXTConga4__Connected_App_Setting__mdt () 
    {
    }
}