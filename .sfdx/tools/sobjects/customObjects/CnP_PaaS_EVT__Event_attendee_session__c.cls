// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS_EVT__Event_attendee_session__c {
    global Id Id;
    global SObject Owner;
    global Id OwnerId;
    global Boolean IsDeleted;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global CnP_PaaS_EVT__Discount_plan__c CnP_PaaS_EVT__C_P_Discount_Plan__r;
    global Id CnP_PaaS_EVT__C_P_Discount_Plan__c;
    global CnP_PaaS_EVT__C_P_Event_LevelGroup__c CnP_PaaS_EVT__C_P_Event_LevelGroup__r;
    global Id CnP_PaaS_EVT__C_P_Event_LevelGroup__c;
    global String CnP_PaaS_EVT__CheckIn_Notes__c;
    global String CnP_PaaS_EVT__CheckIn_Status__c;
    global Contact CnP_PaaS_EVT__ContactId__r;
    global Id CnP_PaaS_EVT__ContactId__c;
    global String CnP_PaaS_EVT__Contact_Data__c;
    global CnP_PaaS_EVT__Event__c CnP_PaaS_EVT__EventId__r;
    global Id CnP_PaaS_EVT__EventId__c;
    global String CnP_PaaS_EVT__First_name__c;
    global Double CnP_PaaS_EVT__Increment_Number__c;
    global Datetime CnP_PaaS_EVT__Last_Status_Change__c;
    global String CnP_PaaS_EVT__Last_name__c;
    global CnP_PaaS_EVT__Event_registrant_session__c CnP_PaaS_EVT__Registrant_session_Id__r;
    global Id CnP_PaaS_EVT__Registrant_session_Id__c;
    global CnP_PaaS_EVT__Registration_level__c CnP_PaaS_EVT__Registration_level__r;
    global Id CnP_PaaS_EVT__Registration_level__c;
    global String CnP_PaaS_EVT__Status__c;
    global String CnP_PaaS_EVT__Text_Field__c;
    global String CnP_PaaS_EVT__Ticket_Number__c;
    global String CnP_PaaS_EVT__Ticket_guid__c;
    /* Price + Additional Fee
    */
    global Decimal CnP_PaaS_EVT__Total_Amount__c;
    global Decimal CnP_PaaS_EVT__Total_Discount__c;
    global Decimal CnP_PaaS_EVT__Total_Tax__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<Attachment> Attachments;
    global List<CnP_PaaS_EVT__Attendee_custom_information__c> CnP_PaaS_EVT__Attendee_custom_informations__r;
    global List<CnP_PaaS_EVT__C_P_Event_Temporary_Contact__c> CnP_PaaS_EVT__C_P_Event_Temporary_Contacts__r;
    global List<CnP_PaaS_EVT__Checkin_Checkout__c> CnP_PaaS_EVT__C_P_Checkin_and_Checkout__r;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<NetworkActivityAudit> ParentEntities;
    global List<NetworkUserHistoryRecent> NetworkUserHistoryRecentToRecord;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordAction> RecordActions;
    global List<RecordActionHistory> RecordActionHistories;
    global List<TopicAssignment> TopicAssignments;
    global List<ContentVersion> FirstPublishLocation;
    global List<EventChangeEvent> What;
    global List<EventRelationChangeEvent> Relation;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;
    global List<TaskChangeEvent> What;

    global CnP_PaaS_EVT__Event_attendee_session__c () 
    {
    }
}