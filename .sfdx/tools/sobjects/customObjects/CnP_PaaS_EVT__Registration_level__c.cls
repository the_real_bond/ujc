// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS_EVT__Registration_level__c {
    global Id Id;
    global SObject Owner;
    global Id OwnerId;
    global Boolean IsDeleted;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global Decimal CnP_PaaS_EVT__Additional_Fee__c;
    global Boolean CnP_PaaS_EVT__Attendee_Send_NameBadges__c;
    global Double CnP_PaaS_EVT__Available_Inventory__c;
    global Campaign CnP_PaaS_EVT__Campaign__r;
    global Id CnP_PaaS_EVT__Campaign__c;
    global Double CnP_PaaS_EVT__Current_inventory__c;
    global String CnP_PaaS_EVT__Description__c;
    global Datetime CnP_PaaS_EVT__Display_this_registration_from__c;
    global String CnP_PaaS_EVT__End_date_message__c;
    global CnP_PaaS_EVT__Event__c CnP_PaaS_EVT__Event_name__r;
    global Id CnP_PaaS_EVT__Event_name__c;
    global String CnP_PaaS_EVT__Fee_label__c;
    global Datetime CnP_PaaS_EVT__Hide_this_registration_from__c;
    global Double CnP_PaaS_EVT__Inventory_Sold__c;
    global Double CnP_PaaS_EVT__Limit_for_this_type__c;
    global String CnP_PaaS_EVT__Message_to_display_when_limit_is_reached__c;
    global String CnP_PaaS_EVT__Mode__c;
    global String CnP_PaaS_EVT__Name_on_reports__c;
    global String CnP_PaaS_EVT__Name_on_the_form__c;
    global Double CnP_PaaS_EVT__OrderLevel__c;
    global Decimal CnP_PaaS_EVT__Price__c;
    global String CnP_PaaS_EVT__Report_Color__c;
    global String CnP_PaaS_EVT__SKU_Code_Level__c;
    global CnP_PaaS__CnP_Designer__c CnP_PaaS_EVT__Select_NameBadge__r;
    global Id CnP_PaaS_EVT__Select_NameBadge__c;
    global CnP_PaaS__CnP_Designer__c CnP_PaaS_EVT__Select_Ticket__r;
    global Id CnP_PaaS_EVT__Select_Ticket__c;
    global CnP_PaaS__CnP_Designer__c CnP_PaaS_EVT__Select_eTicket__r;
    global Id CnP_PaaS_EVT__Select_eTicket__c;
    global Boolean CnP_PaaS_EVT__Send_NameBadges__c;
    global Boolean CnP_PaaS_EVT__Send_Tickets__c;
    global Boolean CnP_PaaS_EVT__Send_eTickets__c;
    global Decimal CnP_PaaS_EVT__Tax_Deductible__c;
    global Double CnP_PaaS_EVT__Tax_rate__c;
    global String CnP_PaaS_EVT__Ticket_Number_Prefix__c;
    global Double CnP_PaaS_EVT__Tickets_Included__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<Attachment> Attachments;
    global List<CnP_PaaS_EVT__Attendee_custom_information__c> CnP_PaaS_EVT__Attendee_custom_informations__r;
    global List<CnP_PaaS_EVT__C_P_Event_LevelGroup__c> CnP_PaaS_EVT__C_P_Event_Level_Groups__r;
    global List<CnP_PaaS_EVT__Contact_information_fields__c> CnP_PaaS_EVT__Contact_information_fields__r;
    global List<CnP_PaaS_EVT__Custom_fields__c> CnP_PaaS_EVT__Custom_registration_fields__r;
    global List<CnP_PaaS_EVT__Discount_plan__c> CnP_PaaS_EVT__Discount_plan__r;
    global List<CnP_PaaS_EVT__Event_attendee_session__c> CnP_PaaS_EVT__Event_attendee_sessions__r;
    global List<CnP_PaaS_EVT__Inventory_Process__c> CnP_PaaS_EVT__C_P_Inventory_Process__r;
    global List<CnP_PaaS_EVT__Questionsection__c> CnP_PaaS_EVT__C_P_Question_Sections__r;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<NetworkActivityAudit> ParentEntities;
    global List<NetworkUserHistoryRecent> NetworkUserHistoryRecentToRecord;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordAction> RecordActions;
    global List<RecordActionHistory> RecordActionHistories;
    global List<TopicAssignment> TopicAssignments;
    global List<ContentVersion> FirstPublishLocation;
    global List<EventChangeEvent> What;
    global List<EventRelationChangeEvent> Relation;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;
    global List<TaskChangeEvent> What;

    global CnP_PaaS_EVT__Registration_level__c () 
    {
    }
}