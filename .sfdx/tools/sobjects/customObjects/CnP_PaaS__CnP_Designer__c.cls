// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__CnP_Designer__c {
    global Id Id;
    global SObject Owner;
    global Id OwnerId;
    global Boolean IsDeleted;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global Date LastActivityDate;
    global Datetime LastViewedDate;
    global Datetime LastReferencedDate;
    global String CnP_PaaS__Design_Selection__c;
    global String CnP_PaaS__Library__c;
    global String CnP_PaaS__Pdf__c;
    global Boolean CnP_PaaS__Pdf_include__c;
    global String CnP_PaaS__SalesForce_Public_Site_URL__c;
    global String CnP_PaaS__Select_Layout__c;
    global String CnP_PaaS__Select_Template_Cat__c;
    global String CnP_PaaS__Tags__c;
    global String CnP_PaaS__Template_Layout_Name__c;
    global String CnP_PaaS_EVT__Event_Custom_Css__c;
    global String CnP_PaaS_EVT__Event_Designer_Category__c;
    global List<ActivityHistory> ActivityHistories;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<Attachment> Attachments;
    global List<CnP_PaaS_EVT__Event__c> CnP_PaaS_EVT__C_P_Events__r;
    global List<CnP_PaaS_EVT__Event_listing__c> CnP_PaaS_EVT__C_P_Event_Listings__r;
    global List<CnP_PaaS_EVT__Registration_level__c> CnP_PaaS_EVT__C_P_Event_Registration_Levels1__r;
    global List<CnP_PaaS_EVT__Registration_level__c> CnP_PaaS_EVT__C_P_Event_Registration_Levels__r;
    global List<CnP_PaaS_EVT__Registration_level__c> CnP_PaaS_EVT__C_P_Event_Registration_Levels2__r;
    global List<CnP_PaaS__C_P_Designer_Content_Data__c> CnP_PaaS__C_P_Designer_Content_Data__r;
    global List<CnP_PaaS__CnP_Auto_Responder_Queue__c> CnP_PaaS__C_P_Auto_Responder_Queue__r;
    global List<CnP_PaaS__CnP_Auto_Responder_Sending_Details__c> CnP_PaaS__C_P_Auto_Responder_Sending_Details__r;
    global List<CnP_PaaS__CnP_Auto_Responder__c> CnP_PaaS__C_P_Auto_Responder_Settings__r;
    global List<CnP_PaaS__CnP_Designer__History> Histories;
    global List<CnP_PaaS__Invoice_Policy__c> CnP_PaaS__Invoice_Policy__r;
    global List<CnP_PaaS__Invoice_Policy__c> CnP_PaaS__Invoice_Policy1__r;
    global List<CnP_PaaS__Invoice__c> CnP_PaaS__C_P_Invoice__r;
    global List<CnP_PaaS__Invoice__c> CnP_PaaS__C_P_Invoice1__r;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EmailMessage> Emails;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<Event> Events;
    global List<NetworkActivityAudit> ParentEntities;
    global List<NetworkUserHistoryRecent> NetworkUserHistoryRecentToRecord;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordAction> RecordActions;
    global List<RecordActionHistory> RecordActionHistories;
    global List<Task> Tasks;
    global List<TopicAssignment> TopicAssignments;
    global List<ContentVersion> FirstPublishLocation;
    global List<EventChangeEvent> What;
    global List<EventRelationChangeEvent> Relation;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;
    global List<TaskChangeEvent> What;

    global CnP_PaaS__CnP_Designer__c () 
    {
    }
}