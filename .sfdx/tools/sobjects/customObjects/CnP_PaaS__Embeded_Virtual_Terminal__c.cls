// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__Embeded_Virtual_Terminal__c {
    global Id Id;
    global SObject Owner;
    global Id OwnerId;
    global Boolean IsDeleted;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global Date LastActivityDate;
    global Datetime LastViewedDate;
    global Datetime LastReferencedDate;
    global Boolean CnP_PaaS__Acknowledgement__c;
    global Boolean CnP_PaaS__American_Express__c;
    global String CnP_PaaS__Application_Name__c;
    global String CnP_PaaS__Base_URL__c;
    global String CnP_PaaS__Click_Pledge_Account_Id__c;
    global String CnP_PaaS__Contact_Role__c;
    global Boolean CnP_PaaS__Discover__c;
    global String CnP_PaaS__Form_CSS__c;
    global String CnP_PaaS__Identification_Number__c;
    global Boolean CnP_PaaS__Invoice_PO__c;
    global Boolean CnP_PaaS__JCB__c;
    global Boolean CnP_PaaS__Master_Card__c;
    global String CnP_PaaS__Organization_Information__c;
    global String CnP_PaaS__Page_Name__c;
    global String CnP_PaaS__Payment_Declined__c;
    global Boolean CnP_PaaS__Purchase_Order__c;
    global String CnP_PaaS__Receipt_Header__c;
    global Boolean CnP_PaaS__Show_terms__c;
    global String CnP_PaaS__Terms_Conditions_Text__c;
    global String CnP_PaaS__Terms_Conditions__c;
    global String CnP_PaaS__Thank_you__c;
    global String CnP_PaaS__Version_Number__c;
    global Boolean CnP_PaaS__Visa__c;
    global Account CnP_PaaS__cs_at_aid__r;
    global Id CnP_PaaS__cs_at_aid__c;
    global Boolean CnP_PaaS__cs_at_ca__c;
    global Boolean CnP_PaaS__cs_at_na__c;
    global Boolean CnP_PaaS__cs_cr_cc__c;
    global Boolean CnP_PaaS__cs_cr_uc__c;
    global String CnP_PaaS__cs_os_at__c;
    global Boolean CnP_PaaS__cs_os_co__c;
    global Boolean CnP_PaaS__cs_os_cp__c;
    global Boolean CnP_PaaS__cs_os_cr__c;
    global String CnP_PaaS__cs_os_dt__c;
    global String CnP_PaaS__cs_os_fs__c;
    global String CnP_PaaS__cs_os_ins__c;
    global String CnP_PaaS__cs_os_pos__c;
    global Boolean CnP_PaaS__e_Check__c;
    global List<ActivityHistory> ActivityHistories;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<Attachment> Attachments;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EmailMessage> Emails;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<Event> Events;
    global List<NetworkActivityAudit> ParentEntities;
    global List<NetworkUserHistoryRecent> NetworkUserHistoryRecentToRecord;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordAction> RecordActions;
    global List<RecordActionHistory> RecordActionHistories;
    global List<Task> Tasks;
    global List<TopicAssignment> TopicAssignments;
    global List<ContentVersion> FirstPublishLocation;
    global List<EventChangeEvent> What;
    global List<EventRelationChangeEvent> Relation;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;
    global List<TaskChangeEvent> What;

    global CnP_PaaS__Embeded_Virtual_Terminal__c () 
    {
    }
}