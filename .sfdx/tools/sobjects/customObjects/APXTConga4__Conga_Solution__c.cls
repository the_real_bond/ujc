// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class APXTConga4__Conga_Solution__c {
    global Id Id;
    global SObject Owner;
    global Id OwnerId;
    global Boolean IsDeleted;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global Datetime LastViewedDate;
    global Datetime LastReferencedDate;
    global String APXTConga4__Button_Link_API_Name__c;
    global String APXTConga4__Button_body_field__c;
    global String APXTConga4__Composer_Parameters__c;
    global String APXTConga4__Custom_Object_Id__c;
    global String APXTConga4__Formula_Field_API_Name__c;
    global String APXTConga4__Formula_body_field__c;
    global String APXTConga4__Is_Quick_Start__c;
    global String APXTConga4__Launch_C8_Formula_Button__c;
    global String APXTConga4__Master_Object_Type_Validator__c;
    global String APXTConga4__Master_Object_Type__c;
    global String APXTConga4__Sample_Record_Id__c;
    global String APXTConga4__Sample_Record_Name__c;
    global String APXTConga4__Solution_Description__c;
    global String APXTConga4__Solution_Weblink_Syntax__c;
    global String APXTConga4__Weblink_Id__c;
    global Double APXTConga4__CongaEmailTemplateCount__c;
    global List<APXTConga4__Composer_QuickMerge__c> APXTConga4__Composer_QuickMerge_Records__r;
    global List<APXTConga4__Conga_Collection_Solution__c> APXTConga4__Conga_Collection_Solutions__r;
    global List<APXTConga4__Conga_Solution_Email_Template__c> APXTConga4__Conga_Solution_Email_Templates__r;
    global List<APXTConga4__Conga_Solution_Parameter__c> APXTConga4__Conga_Solution_Parameters__r;
    global List<APXTConga4__Conga_Solution_Query__c> APXTConga4__Conga_Solution_Queries__r;
    global List<APXTConga4__Conga_Solution_Report__c> APXTConga4__Conga_Solution_Reports__r;
    global List<APXTConga4__Conga_Solution_Template__c> APXTConga4__Conga_Solution_Templates__r;
    global List<APXTConga4__Conga_Solution__Share> Shares;
    global List<APXTConga4__Document_History__c> APXTConga4__Document_Histories__r;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<Attachment> Attachments;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<NetworkActivityAudit> ParentEntities;
    global List<NetworkUserHistoryRecent> NetworkUserHistoryRecentToRecord;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordAction> RecordActions;
    global List<RecordActionHistory> RecordActionHistories;
    global List<TopicAssignment> TopicAssignments;
    global List<ContentVersion> FirstPublishLocation;
    global List<EventChangeEvent> What;
    global List<EventRelationChangeEvent> Relation;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;
    global List<TaskChangeEvent> What;

    global APXTConga4__Conga_Solution__c () 
    {
    }
}