// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS_EVT__Custom_fields__c {
    global Id Id;
    global SObject Owner;
    global Id OwnerId;
    global Boolean IsDeleted;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global String CnP_PaaS_EVT__Create_new_group__c;
    global String CnP_PaaS_EVT__Default_value__c;
    global CnP_PaaS_EVT__Event__c CnP_PaaS_EVT__Event__r;
    global Id CnP_PaaS_EVT__Event__c;
    global String CnP_PaaS_EVT__Field_Options__c;
    global String CnP_PaaS_EVT__Field_name__c;
    global String CnP_PaaS_EVT__Field_type__c;
    global String CnP_PaaS_EVT__Field_values__c;
    global Double CnP_PaaS_EVT__Int_order_Number__c;
    global String CnP_PaaS_EVT__Order_number__c;
    global CnP_PaaS_EVT__Questionsection__c CnP_PaaS_EVT__Question_Section__r;
    global Id CnP_PaaS_EVT__Question_Section__c;
    global String CnP_PaaS_EVT__Question__c;
    global CnP_PaaS_EVT__Registration_level__c CnP_PaaS_EVT__Registration_level__r;
    global Id CnP_PaaS_EVT__Registration_level__c;
    global Boolean CnP_PaaS_EVT__Required__c;
    global String CnP_PaaS_EVT__Section_order__c;
    global String CnP_PaaS_EVT__Select_group__c;
    global Boolean CnP_PaaS_EVT__Visible__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<Attachment> Attachments;
    global List<CnP_PaaS_EVT__Attendee_custom_information__c> CnP_PaaS_EVT__C_P_Attendee_Custom_Informations__r;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<NetworkActivityAudit> ParentEntities;
    global List<NetworkUserHistoryRecent> NetworkUserHistoryRecentToRecord;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordAction> RecordActions;
    global List<RecordActionHistory> RecordActionHistories;
    global List<TopicAssignment> TopicAssignments;
    global List<ContentVersion> FirstPublishLocation;
    global List<EventChangeEvent> What;
    global List<EventRelationChangeEvent> Relation;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;
    global List<TaskChangeEvent> What;

    global CnP_PaaS_EVT__Custom_fields__c () 
    {
    }
}