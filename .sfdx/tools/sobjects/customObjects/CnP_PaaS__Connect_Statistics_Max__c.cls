// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__Connect_Statistics_Max__c {
    global Id Id;
    global SObject Owner;
    global Id OwnerId;
    global Boolean IsDeleted;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global Date LastActivityDate;
    global Double CnP_PaaS__Donor_Personal_Donation_Amount_Rank_Max__c;
    global Double CnP_PaaS__Donor_Personal_Donation_Count_Rank_Max__c;
    global Double CnP_PaaS__Donor_Raised_Donation_Amount_Rank_Max__c;
    global Double CnP_PaaS__Donor_Raised_Donation_Count_Rank_Max__c;
    global Double CnP_PaaS__Donors_Rank_Factor_Max__c;
    global String CnP_PaaS__Job_Status__c;
    global Decimal CnP_PaaS__My_Donor_Personal_Donation_Amount_Max__c;
    global Double CnP_PaaS__My_Donor_Personal_Donation_Count_Max__c;
    global Decimal CnP_PaaS__My_Donor_Raised_Donation_Amount_Max__c;
    global Double CnP_PaaS__My_Donor_Raised_Donation_Count_Max__c;
    global Double CnP_PaaS__My_Rank_Factor_Max__c;
    global Decimal CnP_PaaS__Personal_Donation_Amount_Max__c;
    global Double CnP_PaaS__Personal_Donation_Amount_Rank_Max__c;
    global Double CnP_PaaS__Personal_Donation_Count_Max__c;
    global Double CnP_PaaS__Personal_Donation_Count_Rank_Max__c;
    global Decimal CnP_PaaS__Raised_Donation_Amount_Max__c;
    global Double CnP_PaaS__Raised_Donation_Amount_Rank_Max__c;
    global Double CnP_PaaS__Raised_Donation_Count_Max__c;
    global Double CnP_PaaS__Raised_Donation_Count_Rank_Max__c;
    global Decimal CnP_PaaS__Total_Intrinsic_Value_Max__c;
    global Double CnP_PaaS__Total_Records__c;
    global Decimal CnP_PaaS__Total__c;
    global Decimal CnP_PaaS__Total_extrinsic_value_Max__c;
    global List<ActivityHistory> ActivityHistories;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<Attachment> Attachments;
    global List<CnP_PaaS__Connect_Statistics_Max__History> Histories;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EmailMessage> Emails;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<Event> Events;
    global List<NetworkActivityAudit> ParentEntities;
    global List<NetworkUserHistoryRecent> NetworkUserHistoryRecentToRecord;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordAction> RecordActions;
    global List<RecordActionHistory> RecordActionHistories;
    global List<Task> Tasks;
    global List<TopicAssignment> TopicAssignments;
    global List<ContentVersion> FirstPublishLocation;
    global List<EventChangeEvent> What;
    global List<EventRelationChangeEvent> Relation;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;
    global List<TaskChangeEvent> What;

    global CnP_PaaS__Connect_Statistics_Max__c () 
    {
    }
}