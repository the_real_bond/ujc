// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class MC4SF__MC_Query__c {
    global Id Id;
    global Boolean IsDeleted;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global MC4SF__MC_List__c MC4SF__MC_List__r;
    global Id MC4SF__MC_List__c;
    global String MC4SF__Campaign_Id__c;
    global String MC4SF__Campaign_Members_SOQL__c;
    global Boolean MC4SF__Campaign_Members__c;
    global String MC4SF__Contacts_SOQL__c;
    global Boolean MC4SF__Contacts__c;
    global String MC4SF__Error_Message__c;
    global String MC4SF__Interests__c;
    global User MC4SF__Last_Run_As__r;
    global Id MC4SF__Last_Run_As__c;
    global Datetime MC4SF__Last_Run__c;
    global String MC4SF__Leads_SOQL__c;
    global Boolean MC4SF__Leads__c;
    global String MC4SF__Run_Daily_At__c;
    global String MC4SF__Static_Segments__c;
    global String MC4SF__Status__c;
    global Double MC4SF__Subscribers_Added_last_run__c;
    global Boolean MC4SF__User_Contacts_Only__c;
    global Boolean MC4SF__User_Leads_Only__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<Attachment> Attachments;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<MC4SF__MC_Query_Filter__c> MC4SF__MC_Query_Filters__r;
    global List<NetworkActivityAudit> ParentEntities;
    global List<NetworkUserHistoryRecent> NetworkUserHistoryRecentToRecord;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordAction> RecordActions;
    global List<RecordActionHistory> RecordActionHistories;
    global List<TopicAssignment> TopicAssignments;
    global List<ContentVersion> FirstPublishLocation;
    global List<EventChangeEvent> What;
    global List<EventRelationChangeEvent> Relation;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;
    global List<TaskChangeEvent> What;

    global MC4SF__MC_Query__c () 
    {
    }
}