// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__Sub_Recordtypes__c {
    global Id Id;
    global SObject Owner;
    global Id OwnerId;
    global Boolean IsDeleted;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global CnP_PaaS__Record_Types__c CnP_PaaS__C_P_Record_Type__r;
    global Id CnP_PaaS__C_P_Record_Type__c;
    global Campaign CnP_PaaS__Campaign__r;
    global Id CnP_PaaS__Campaign__c;
    global CnP_PaaS__Class__c CnP_PaaS__Class__r;
    global Id CnP_PaaS__Class__c;
    global CnP_PaaS__Ledger__c CnP_PaaS__GL_Account__r;
    global Id CnP_PaaS__GL_Account__c;
    global String CnP_PaaS__Ledger_Name__c;
    global String CnP_PaaS__Ledger__c;
    global Boolean CnP_PaaS__Opportunity_Allocation__c;
    global Boolean CnP_PaaS__Opportunity_Product_Allocation__c;
    global String CnP_PaaS__Opportunity_SKU_Condition__c;
    global String CnP_PaaS__Percentage_amount__c;
    global String CnP_PaaS__SKU_value__c;
    global CnP_PaaS__Sub_Class__c CnP_PaaS__Sub_Class__r;
    global Id CnP_PaaS__Sub_Class__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<Attachment> Attachments;
    global List<CnP_PaaS__Sub_Recordtypes__History> Histories;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<NetworkActivityAudit> ParentEntities;
    global List<NetworkUserHistoryRecent> NetworkUserHistoryRecentToRecord;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordAction> RecordActions;
    global List<RecordActionHistory> RecordActionHistories;
    global List<TopicAssignment> TopicAssignments;
    global List<ContentVersion> FirstPublishLocation;
    global List<EventChangeEvent> What;
    global List<EventRelationChangeEvent> Relation;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;
    global List<TaskChangeEvent> What;

    global CnP_PaaS__Sub_Recordtypes__c () 
    {
    }
}