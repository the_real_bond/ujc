// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__CnPData__c {
    global Id Id;
    global SObject Owner;
    global Id OwnerId;
    global Boolean IsDeleted;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global Datetime LastViewedDate;
    global Datetime LastReferencedDate;
    global String CnP_PaaS__CnPDataID__c;
    global Contact CnP_PaaS__Contact__r;
    global Id CnP_PaaS__Contact__c;
    global String CnP_PaaS__DataXML__c;
    global Boolean CnP_PaaS__Email__c;
    global String CnP_PaaS__Installed_Apps__c;
    global String CnP_PaaS__Message__c;
    global String CnP_PaaS__Name__c;
    global String CnP_PaaS__OrderNumber__c;
    global String CnP_PaaS__Rollup_Update__c;
    global Double CnP_PaaS__StatusID__c;
    global Decimal CnP_PaaS__Total_Charged__c;
    global String CnP_PaaS__Transaction_Result__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<Attachment> Attachments;
    global List<CnP_PaaS_EVT__C_P_Event_Temporary_Contact__c> CnP_PaaS_EVT__C_P_Event_Temporary_Contacts__r;
    global List<CnP_PaaS__CnPData__History> Histories;
    global List<CnP_PaaS__Temporary_Contact__c> CnP_PaaS__Temporary_Contact__r;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<NetworkActivityAudit> ParentEntities;
    global List<NetworkUserHistoryRecent> NetworkUserHistoryRecentToRecord;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordAction> RecordActions;
    global List<RecordActionHistory> RecordActionHistories;
    global List<TopicAssignment> TopicAssignments;
    global List<ContentVersion> FirstPublishLocation;
    global List<EventChangeEvent> What;
    global List<EventRelationChangeEvent> Relation;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;
    global List<TaskChangeEvent> What;

    global CnP_PaaS__CnPData__c () 
    {
    }
}