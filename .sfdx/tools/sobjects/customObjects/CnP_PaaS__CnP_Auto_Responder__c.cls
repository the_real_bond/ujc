// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__CnP_Auto_Responder__c {
    global Id Id;
    global SObject Owner;
    global Id OwnerId;
    global Boolean IsDeleted;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global Date LastActivityDate;
    global Boolean CnP_PaaS__American_Express__c;
    global Boolean CnP_PaaS__Authorized__c;
    global String CnP_PaaS__BCC_Email_Address__c;
    global String CnP_PaaS__CC_Email_Address__c;
    global String CnP_PaaS__C_P_Account_Names__c;
    global String CnP_PaaS__Campain_Math_Condition__c;
    global String CnP_PaaS__Clickandpledge_SMTP__c;
    global CnP_PaaS__CnP_API_Settings__c CnP_PaaS__CnP_API_Settings__r;
    global Id CnP_PaaS__CnP_API_Settings__c;
    global CnP_PaaS__CnP_Designer__c CnP_PaaS__CnP_Designer__r;
    global Id CnP_PaaS__CnP_Designer__c;
    global Boolean CnP_PaaS__Credit_card__c;
    global String CnP_PaaS__Custom_Payment_Type_Name__c;
    global Boolean CnP_PaaS__Custom_Payment_Type__c;
    global Boolean CnP_PaaS__Declined__c;
    global String CnP_PaaS__Delayed_day__c;
    global Boolean CnP_PaaS__Discover__c;
    global String CnP_PaaS__Future_Transaction_Type__c;
    global Boolean CnP_PaaS__Future_Transaction__c;
    global Boolean CnP_PaaS__Invoice__c;
    global Boolean CnP_PaaS__JCB__c;
    global String CnP_PaaS__Log_Archive__c;
    global String CnP_PaaS__Mail_From_Address__c;
    global String CnP_PaaS__Mail_From_Name__c;
    global String CnP_PaaS__Mail_to__c;
    global Boolean CnP_PaaS__Master_Card__c;
    global String CnP_PaaS__Payment_Math_Condition__c;
    global Boolean CnP_PaaS__Pending__c;
    global Boolean CnP_PaaS__Purchase_Order__c;
    global String CnP_PaaS__Question_Math_Condition__c;
    global String CnP_PaaS__Recipient__c;
    global String CnP_PaaS__Recurring_Type__c;
    global Boolean CnP_PaaS__Recurring__c;
    global String CnP_PaaS__Replay_to_Address__c;
    global String CnP_PaaS__SKU_Math_Condition__c;
    global CnP_PaaS__SMTP_settings__c CnP_PaaS__SMTP_settings__r;
    global Id CnP_PaaS__SMTP_settings__c;
    global String CnP_PaaS__Send_Option__c;
    global Date CnP_PaaS__Specific_date__c;
    global Datetime CnP_PaaS__Specific_date_and_time__c;
    global String CnP_PaaS__Status__c;
    global String CnP_PaaS__Stop_dele__c;
    global String CnP_PaaS__Subject__c;
    global String CnP_PaaS__Subscription_Installment_type__c;
    global String CnP_PaaS__Tags__c;
    global Boolean CnP_PaaS__Visa__c;
    global String CnP_PaaS__WID_Math_Condition__c;
    global Boolean CnP_PaaS__eCheck__c;
    global String CnP_PaaS__hour__c;
    global String CnP_PaaS__minute__c;
    global List<ActivityHistory> ActivityHistories;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<Attachment> Attachments;
    global List<CnP_PaaS__CnP_Auto_Responder_Queue__c> CnP_PaaS__C_P_Auto_Responder_Queue__r;
    global List<CnP_PaaS__CnP_Auto_Responder_Sending_Details__c> CnP_PaaS__C_P_Auto_Responder_Sending_Details__r;
    global List<CnP_PaaS__CnP_Auto_Responder_Settings_Options__c> CnP_PaaS__C_P_Auto_Responder_Settings_Options__r;
    global List<CnP_PaaS__CnP_Auto_Responder__History> Histories;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EmailMessage> Emails;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<Event> Events;
    global List<NetworkActivityAudit> ParentEntities;
    global List<NetworkUserHistoryRecent> NetworkUserHistoryRecentToRecord;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordAction> RecordActions;
    global List<RecordActionHistory> RecordActionHistories;
    global List<Task> Tasks;
    global List<TopicAssignment> TopicAssignments;
    global List<ContentVersion> FirstPublishLocation;
    global List<EventChangeEvent> What;
    global List<EventRelationChangeEvent> Relation;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;
    global List<TaskChangeEvent> What;

    global CnP_PaaS__CnP_Auto_Responder__c () 
    {
    }
}