// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class dlrs__LookupRollupSummary2__mdt {
    global Id Id;
    global String DeveloperName;
    global String MasterLabel;
    global String Language;
    global String NamespacePrefix;
    global String Label;
    global String QualifiedApiName;
    /* For Realtime rollups can only be set when the Child Apex Trigger has been deployed.
    */
    global Boolean dlrs__Active__c;
    /* Includes child records that have been archived by the system and/or placed in the recycle bin.
    */
    global Boolean dlrs__AggregateAllRows__c;
    /* Rollup operation.
    */
    global String dlrs__AggregateOperation__c;
    /* API name of the field that will store the result of the rollup on the Parent Object, e.g. AnnualRevenue
    */
    global String dlrs__AggregateResultField__c;
    /* Realtime and Schedule modes require an Apex Trigger to be deployed for the Child Object. Click Manage Child Trigger button to deploy.
    */
    global String dlrs__CalculationMode__c;
    /* Determines if the Sharing Rules defined on the Child Object are considered when calculating the rollup. Default is User.
    */
    global String dlrs__CalculationSharingMode__c;
    /* API name of the Child Object, e.g. Opportunity
    */
    global String dlrs__ChildObject__c;
    /* Enter the character or characters to delimit values in the Field to Aggregate when rolling up text values into the Aggregate Result Field, enter BR() for new line. Only applies when using Concatenate operation.
    */
    global String dlrs__ConcatenateDelimiter__c;
    global String dlrs__Description__c;
    /* API name of the field on the Child Object that contains the value to rollup, e.g. Amount
    */
    global String dlrs__FieldToAggregate__c;
    /* Only applicable when using the Concatenate, Concatenate Distinct, Last and First aggregate operations. Supports multiple fields (comma separated) with optional ASC/DESC and/or NULLS FIRST/LAST.
    */
    global String dlrs__FieldToOrderBy__c;
    /* API name of the Parent Object, e.g. Account
    */
    global String dlrs__ParentObject__c;
    /* If you have specified a relationship criteria, you must confirm the fields referenced by it here on separate lines, for example for criteria StageName = 'Won' list StageName in this field. You do not need to specify the Field to Aggregate field however.
    */
    global String dlrs__RelationshipCriteriaFields__c;
    /* SOQL WHERE clause applied when querying Child Object records, e.g. Amount > 200
    */
    global String dlrs__RelationshipCriteria__c;
    /* API name of the Lookup field on the Child Object relating to the Parent Object, e.g. AccountId
    */
    global String dlrs__RelationshipField__c;
    /* Limits the number of rows used in the rollup. Applies only to the Last and Concatenate operators.
    */
    global Double dlrs__RowLimit__c;
    /* This tool utilises a dynamically generated Apex Trigger and by default a generated Apex Test. Use this field if instructed by a developer to replace the generated test code. See Wiki on GitHub.
    */
    global String dlrs__TestCode2__c;
    /* Only use this option as a last resort to get the generated test or custom test code working. It is generally considered bad practice to make your test dependent on org data. Always try to create test data in the test code if possible.
    */
    global Boolean dlrs__TestCodeSeeAllData__c;
    /* This tool utilises a dynamically generated Apex Trigger and by default a generated Apex Test. Use this field if instructed by a developer to replace the generated test code. See Wiki on GitHub.
    */
    global String dlrs__TestCode__c;

    global dlrs__LookupRollupSummary2__mdt () 
    {
    }
}