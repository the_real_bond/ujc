// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Tax_Receipt__c {
    global Id Id;
    global SObject Owner;
    global Id OwnerId;
    global Boolean IsDeleted;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global Date LastActivityDate;
    global Datetime LastViewedDate;
    global Datetime LastReferencedDate;
    global String AccountDate__c;
    global String AccountEmail__c;
    global String Account_Donor_Id__c;
    global String Account_Name__c;
    global Account Account__r;
    global Id Account__c;
    global String Amount_in_Words__c;
    global String Attention__c;
    global String Certificate_Number__c;
    global String CloseDatemonthandYear__c;
    global Contact Contact__r;
    global Id Contact__c;
    global Date Date_closed__c;
    global Boolean Email_Check__c;
    global Date Financial_Year_End__c;
    global Date Financial_Year_Start__c;
    global Boolean IsClosed__c;
    global String ReceiptNumber__c;
    global Date Receipt_Date__c;
    global String Tax_Certificate_Issued_to__c;
    global String TodayDate__c;
    global String Type__c;
    global String YearEnd__c;
    global String Year_Start_Words__c;
    global String Year_end_Words__c;
    global String YearendYear__c;
    global Decimal Total_Receipt_Amount__c;
    global String Donor_Status__c;
    global List<ActivityHistory> ActivityHistories;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<Attachment> Attachments;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<Donation_Receipts__c> Donation_Receipts__r;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EmailMessage> Emails;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<Event> Events;
    global List<NetworkActivityAudit> ParentEntities;
    global List<NetworkUserHistoryRecent> NetworkUserHistoryRecentToRecord;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordAction> RecordActions;
    global List<RecordActionHistory> RecordActionHistories;
    global List<Task> Tasks;
    global List<Tax_Receipt__History> Histories;
    global List<TopicAssignment> TopicAssignments;
    global List<ContentVersion> FirstPublishLocation;
    global List<EventChangeEvent> What;
    global List<EventRelationChangeEvent> Relation;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;
    global List<TaskChangeEvent> What;

    global Tax_Receipt__c () 
    {
    }
}