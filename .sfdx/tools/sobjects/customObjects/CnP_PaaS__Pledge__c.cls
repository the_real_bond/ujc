// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__Pledge__c {
    global Id Id;
    global SObject Owner;
    global Id OwnerId;
    global Boolean IsDeleted;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global Date LastActivityDate;
    global Datetime LastViewedDate;
    global Datetime LastReferencedDate;
    global Account CnP_PaaS__Account__r;
    global Id CnP_PaaS__Account__c;
    global Decimal CnP_PaaS__Benifit_Value__c;
    global Campaign CnP_PaaS__Campaign__r;
    global Id CnP_PaaS__Campaign__c;
    global Contact CnP_PaaS__Contact__r;
    global Id CnP_PaaS__Contact__c;
    global String CnP_PaaS__Description__c;
    global Date CnP_PaaS__End_Date__c;
    global Boolean CnP_PaaS__Excess_Amount__c;
    global Double CnP_PaaS__Number_of_Payments__c;
    global Decimal CnP_PaaS__Pledge_Amount__c;
    global String CnP_PaaS__Pledge_Status__c;
    global Decimal CnP_PaaS__Remaining_Balance__c;
    global String CnP_PaaS__SKU_Condition__c;
    global String CnP_PaaS__SKU_VT__c;
    global String CnP_PaaS__SKU__c;
    global Date CnP_PaaS__Start_Date__c;
    global String CnP_PaaS__Tribute__c;
    global List<ActivityHistory> ActivityHistories;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<Attachment> Attachments;
    global List<CnP_PaaS__Opportunity_Pledge__c> CnP_PaaS__Opportunity_Pledge__r;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EmailMessage> Emails;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<Event> Events;
    global List<NetworkActivityAudit> ParentEntities;
    global List<NetworkUserHistoryRecent> NetworkUserHistoryRecentToRecord;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordAction> RecordActions;
    global List<RecordActionHistory> RecordActionHistories;
    global List<Task> Tasks;
    global List<TopicAssignment> TopicAssignments;
    global List<ContentVersion> FirstPublishLocation;
    global List<EventChangeEvent> What;
    global List<EventRelationChangeEvent> Relation;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;
    global List<TaskChangeEvent> What;

    global CnP_PaaS__Pledge__c () 
    {
    }
}