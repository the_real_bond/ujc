// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__CnP_Auto_Responder_Settings_Options__c {
    global Id Id;
    global SObject Owner;
    global Id OwnerId;
    global Boolean IsDeleted;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global String CnP_PaaS__Amount_Value__c;
    global String CnP_PaaS__Answer__c;
    global String CnP_PaaS__Campaign_Value__c;
    global String CnP_PaaS__Campaign__c;
    global String CnP_PaaS__Checkout_page_WID__c;
    global CnP_PaaS__CnP_Auto_Responder__c CnP_PaaS__CnP_Auto_Responder_Settings__r;
    global Id CnP_PaaS__CnP_Auto_Responder_Settings__c;
    global String CnP_PaaS__Option_Type__c;
    global String CnP_PaaS__Payment_Amount__c;
    global String CnP_PaaS__Question_Options__c;
    global String CnP_PaaS__Question__c;
    global String CnP_PaaS__SKU_Value__c;
    global String CnP_PaaS__SKU__c;
    global String CnP_PaaS__WID_value__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<Attachment> Attachments;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<NetworkActivityAudit> ParentEntities;
    global List<NetworkUserHistoryRecent> NetworkUserHistoryRecentToRecord;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordAction> RecordActions;
    global List<RecordActionHistory> RecordActionHistories;
    global List<TopicAssignment> TopicAssignments;
    global List<ContentVersion> FirstPublishLocation;
    global List<EventChangeEvent> What;
    global List<EventRelationChangeEvent> Relation;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;
    global List<TaskChangeEvent> What;

    global CnP_PaaS__CnP_Auto_Responder_Settings_Options__c () 
    {
    }
}