// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class CnP_PaaS__CnPRecurring__c {
    global Id Id;
    global SObject Owner;
    global Id OwnerId;
    global Boolean IsDeleted;
    global String Name;
    global Datetime CreatedDate;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime LastModifiedDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime SystemModstamp;
    global Datetime LastViewedDate;
    global Datetime LastReferencedDate;
    global Account CnP_PaaS__Account__r;
    global Id CnP_PaaS__Account__c;
    global Boolean CnP_PaaS__Canceled__c;
    global Date CnP_PaaS__Cancellation_Date__c;
    global Contact CnP_PaaS__Contact__r;
    global Id CnP_PaaS__Contact__c;
    global Date CnP_PaaS__Date_Established__c;
    global Date CnP_PaaS__FirstChargeDate__c;
    global String CnP_PaaS__Full_Name__c;
    global Decimal CnP_PaaS__Installment_Amount__c;
    global Double CnP_PaaS__InstallmentsMade__c;
    global Double CnP_PaaS__Installments__c;
    global Date CnP_PaaS__NextInstallment_Date__c;
    global String CnP_PaaS__OrderNumber__c;
    global String CnP_PaaS__Periodicity__c;
    global String CnP_PaaS__RecurringMethod__c;
    global String CnP_PaaS__Recurring_Transaction_Id__c;
    global Decimal CnP_PaaS__Total_Made__c;
    global Decimal CnP_PaaS__Total__c;
    global String CnP_PaaS__Transaction_Result__c;
    global Decimal CnP_PaaS__Upcoming_Receipts__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<Attachment> Attachments;
    global List<Case> CnP_PaaS__Cases__r;
    global List<CnP_PaaS__CnPRecurringTransaction__c> CnP_PaaS__RecurringTransactions__r;
    global List<CnP_PaaS__CnP_Transaction__c> CnP_PaaS__C_P_Transactions__r;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<NetworkActivityAudit> ParentEntities;
    global List<NetworkUserHistoryRecent> NetworkUserHistoryRecentToRecord;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<Opportunity> CnP_PaaS__Opportunities__r;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordAction> RecordActions;
    global List<RecordActionHistory> RecordActionHistories;
    global List<TopicAssignment> TopicAssignments;
    global List<ContentVersion> FirstPublishLocation;
    global List<EventChangeEvent> What;
    global List<EventRelationChangeEvent> Relation;
    global List<FeedComment> Parent;
    global List<FlowRecordRelation> RelatedRecord;
    global List<TaskChangeEvent> What;

    global CnP_PaaS__CnPRecurring__c () 
    {
    }
}