declare module "@salesforce/apex/DigitalTelephonController.getLoggedInUserList" {
  export default function getLoggedInUserList(): Promise<any>;
}
declare module "@salesforce/apex/DigitalTelephonController.getChartData" {
  export default function getChartData(): Promise<any>;
}
declare module "@salesforce/apex/DigitalTelephonController.crCampaignCardData" {
  export default function crCampaignCardData(param: {crId: any, userListSelected: any}): Promise<any>;
}
declare module "@salesforce/apex/DigitalTelephonController.addRemarkToPledge" {
  export default function addRemarkToPledge(param: {recordId: any, remark: any}): Promise<any>;
}
