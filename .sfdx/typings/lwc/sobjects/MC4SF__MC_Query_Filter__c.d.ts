declare module "@salesforce/schema/MC4SF__MC_Query_Filter__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/MC4SF__MC_Query_Filter__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/MC4SF__MC_Query_Filter__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/MC4SF__MC_Query_Filter__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/MC4SF__MC_Query_Filter__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/MC4SF__MC_Query_Filter__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/MC4SF__MC_Query_Filter__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/MC4SF__MC_Query_Filter__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/MC4SF__MC_Query_Filter__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/MC4SF__MC_Query_Filter__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/MC4SF__MC_Query_Filter__c.MC4SF__MC_Query__r" {
  const MC4SF__MC_Query__r:any;
  export default MC4SF__MC_Query__r;
}
declare module "@salesforce/schema/MC4SF__MC_Query_Filter__c.MC4SF__MC_Query__c" {
  const MC4SF__MC_Query__c:any;
  export default MC4SF__MC_Query__c;
}
declare module "@salesforce/schema/MC4SF__MC_Query_Filter__c.MC4SF__Display_Order__c" {
  const MC4SF__Display_Order__c:number;
  export default MC4SF__Display_Order__c;
}
declare module "@salesforce/schema/MC4SF__MC_Query_Filter__c.MC4SF__Error_Message__c" {
  const MC4SF__Error_Message__c:string;
  export default MC4SF__Error_Message__c;
}
declare module "@salesforce/schema/MC4SF__MC_Query_Filter__c.MC4SF__Field_Name__c" {
  const MC4SF__Field_Name__c:string;
  export default MC4SF__Field_Name__c;
}
declare module "@salesforce/schema/MC4SF__MC_Query_Filter__c.MC4SF__Field_Type__c" {
  const MC4SF__Field_Type__c:string;
  export default MC4SF__Field_Type__c;
}
declare module "@salesforce/schema/MC4SF__MC_Query_Filter__c.MC4SF__Object_Name__c" {
  const MC4SF__Object_Name__c:string;
  export default MC4SF__Object_Name__c;
}
declare module "@salesforce/schema/MC4SF__MC_Query_Filter__c.MC4SF__Operator__c" {
  const MC4SF__Operator__c:string;
  export default MC4SF__Operator__c;
}
declare module "@salesforce/schema/MC4SF__MC_Query_Filter__c.MC4SF__Value__c" {
  const MC4SF__Value__c:string;
  export default MC4SF__Value__c;
}
