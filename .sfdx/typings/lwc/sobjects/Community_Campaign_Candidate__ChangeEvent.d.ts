declare module "@salesforce/schema/Community_Campaign_Candidate__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__ChangeEvent.Community_Campaign__c" {
  const Community_Campaign__c:any;
  export default Community_Campaign__c;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__ChangeEvent.Active__c" {
  const Active__c:boolean;
  export default Active__c;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__ChangeEvent.Biography__c" {
  const Biography__c:string;
  export default Biography__c;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__ChangeEvent.Community_Register__c" {
  const Community_Register__c:any;
  export default Community_Register__c;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__ChangeEvent.First_Name__c" {
  const First_Name__c:string;
  export default First_Name__c;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__ChangeEvent.Last_Name__c" {
  const Last_Name__c:string;
  export default Last_Name__c;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__ChangeEvent.Motivation__c" {
  const Motivation__c:string;
  export default Motivation__c;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__ChangeEvent.Photo__c" {
  const Photo__c:string;
  export default Photo__c;
}
