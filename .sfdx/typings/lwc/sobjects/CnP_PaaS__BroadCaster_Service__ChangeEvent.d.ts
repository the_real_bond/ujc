declare module "@salesforce/schema/CnP_PaaS__BroadCaster_Service__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__BroadCaster_Service__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__BroadCaster_Service__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__BroadCaster_Service__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__BroadCaster_Service__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__BroadCaster_Service__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__BroadCaster_Service__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__BroadCaster_Service__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__BroadCaster_Service__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__BroadCaster_Service__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__BroadCaster_Service__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__BroadCaster_Service__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__BroadCaster_Service__ChangeEvent.CnP_PaaS__API_Key__c" {
  const CnP_PaaS__API_Key__c:string;
  export default CnP_PaaS__API_Key__c;
}
declare module "@salesforce/schema/CnP_PaaS__BroadCaster_Service__ChangeEvent.CnP_PaaS__Client_Id__c" {
  const CnP_PaaS__Client_Id__c:string;
  export default CnP_PaaS__Client_Id__c;
}
declare module "@salesforce/schema/CnP_PaaS__BroadCaster_Service__ChangeEvent.CnP_PaaS__Client_Secret__c" {
  const CnP_PaaS__Client_Secret__c:string;
  export default CnP_PaaS__Client_Secret__c;
}
declare module "@salesforce/schema/CnP_PaaS__BroadCaster_Service__ChangeEvent.CnP_PaaS__Endpoint__c" {
  const CnP_PaaS__Endpoint__c:string;
  export default CnP_PaaS__Endpoint__c;
}
declare module "@salesforce/schema/CnP_PaaS__BroadCaster_Service__ChangeEvent.CnP_PaaS__IsAuthorized__c" {
  const CnP_PaaS__IsAuthorized__c:boolean;
  export default CnP_PaaS__IsAuthorized__c;
}
declare module "@salesforce/schema/CnP_PaaS__BroadCaster_Service__ChangeEvent.CnP_PaaS__Token__c" {
  const CnP_PaaS__Token__c:string;
  export default CnP_PaaS__Token__c;
}
