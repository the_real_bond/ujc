declare module "@salesforce/schema/Relationship__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Relationship__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Relationship__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Relationship__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Relationship__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Relationship__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Relationship__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Relationship__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Relationship__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Relationship__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Relationship__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Relationship__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Relationship__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Relationship__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Relationship__c.Description__c" {
  const Description__c:string;
  export default Description__c;
}
declare module "@salesforce/schema/Relationship__c.Individual_Communal_Reference_Number__c" {
  const Individual_Communal_Reference_Number__c:string;
  export default Individual_Communal_Reference_Number__c;
}
declare module "@salesforce/schema/Relationship__c.Related_Communal_Reference_Number__c" {
  const Related_Communal_Reference_Number__c:string;
  export default Related_Communal_Reference_Number__c;
}
declare module "@salesforce/schema/Relationship__c.Status__c" {
  const Status__c:string;
  export default Status__c;
}
declare module "@salesforce/schema/Relationship__c.Type__c" {
  const Type__c:string;
  export default Type__c;
}
declare module "@salesforce/schema/Relationship__c.Individual__r" {
  const Individual__r:any;
  export default Individual__r;
}
declare module "@salesforce/schema/Relationship__c.Individual__c" {
  const Individual__c:any;
  export default Individual__c;
}
declare module "@salesforce/schema/Relationship__c.Related_Individual__r" {
  const Related_Individual__r:any;
  export default Related_Individual__r;
}
declare module "@salesforce/schema/Relationship__c.Related_Individual__c" {
  const Related_Individual__c:any;
  export default Related_Individual__c;
}
declare module "@salesforce/schema/Relationship__c.Reciprocal_Relationship__r" {
  const Reciprocal_Relationship__r:any;
  export default Reciprocal_Relationship__r;
}
declare module "@salesforce/schema/Relationship__c.Reciprocal_Relationship__c" {
  const Reciprocal_Relationship__c:any;
  export default Reciprocal_Relationship__c;
}
declare module "@salesforce/schema/Relationship__c.Individual_Gender__c" {
  const Individual_Gender__c:string;
  export default Individual_Gender__c;
}
declare module "@salesforce/schema/Relationship__c.Related_Individual_Gender__c" {
  const Related_Individual_Gender__c:string;
  export default Related_Individual_Gender__c;
}
declare module "@salesforce/schema/Relationship__c.DB_Status__c" {
  const DB_Status__c:string;
  export default DB_Status__c;
}
declare module "@salesforce/schema/Relationship__c.DoD__c" {
  const DoD__c:any;
  export default DoD__c;
}
