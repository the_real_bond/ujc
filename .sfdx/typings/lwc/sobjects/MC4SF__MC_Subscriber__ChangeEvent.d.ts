declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Country_Code__c" {
  const MC4SF__Country_Code__c:string;
  export default MC4SF__Country_Code__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Daylight_Savings_GMT_Offset__c" {
  const MC4SF__Daylight_Savings_GMT_Offset__c:string;
  export default MC4SF__Daylight_Savings_GMT_Offset__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Email2__c" {
  const MC4SF__Email2__c:string;
  export default MC4SF__Email2__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Email_Address__c" {
  const MC4SF__Email_Address__c:string;
  export default MC4SF__Email_Address__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Email_Type__c" {
  const MC4SF__Email_Type__c:string;
  export default MC4SF__Email_Type__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__GMT_Offset__c" {
  const MC4SF__GMT_Offset__c:string;
  export default MC4SF__GMT_Offset__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Geographic_Location__Latitude__s" {
  const MC4SF__Geographic_Location__Latitude__s:number;
  export default MC4SF__Geographic_Location__Latitude__s;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Geographic_Location__Longitude__s" {
  const MC4SF__Geographic_Location__Longitude__s:number;
  export default MC4SF__Geographic_Location__Longitude__s;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Geographic_Location__c" {
  const MC4SF__Geographic_Location__c:any;
  export default MC4SF__Geographic_Location__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Golden_Monkey__c" {
  const MC4SF__Golden_Monkey__c:boolean;
  export default MC4SF__Golden_Monkey__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__IP_Opt__c" {
  const MC4SF__IP_Opt__c:string;
  export default MC4SF__IP_Opt__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__IP_Signup__c" {
  const MC4SF__IP_Signup__c:string;
  export default MC4SF__IP_Signup__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Info_Changed__c" {
  const MC4SF__Info_Changed__c:any;
  export default MC4SF__Info_Changed__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Interests__c" {
  const MC4SF__Interests__c:string;
  export default MC4SF__Interests__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Language__c" {
  const MC4SF__Language__c:string;
  export default MC4SF__Language__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Last_Activity_Sync_Date__c" {
  const MC4SF__Last_Activity_Sync_Date__c:any;
  export default MC4SF__Last_Activity_Sync_Date__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Last_Sync_Date__c" {
  const MC4SF__Last_Sync_Date__c:any;
  export default MC4SF__Last_Sync_Date__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Lead_Validation_Errors__c" {
  const MC4SF__Lead_Validation_Errors__c:string;
  export default MC4SF__Lead_Validation_Errors__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Lists__c" {
  const MC4SF__Lists__c:string;
  export default MC4SF__Lists__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__MC_List__c" {
  const MC4SF__MC_List__c:any;
  export default MC4SF__MC_List__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__MailChimp_Campaign_ID__c" {
  const MC4SF__MailChimp_Campaign_ID__c:string;
  export default MC4SF__MailChimp_Campaign_ID__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__MailChimp_ID__c" {
  const MC4SF__MailChimp_ID__c:string;
  export default MC4SF__MailChimp_ID__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__MailChimp_List_ID__c" {
  const MC4SF__MailChimp_List_ID__c:string;
  export default MC4SF__MailChimp_List_ID__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Member_Rating_Image__c" {
  const MC4SF__Member_Rating_Image__c:string;
  export default MC4SF__Member_Rating_Image__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Member_Rating__c" {
  const MC4SF__Member_Rating__c:number;
  export default MC4SF__Member_Rating__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Member_Status__c" {
  const MC4SF__Member_Status__c:string;
  export default MC4SF__Member_Status__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Merge0__c" {
  const MC4SF__Merge0__c:string;
  export default MC4SF__Merge0__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Merge10__c" {
  const MC4SF__Merge10__c:string;
  export default MC4SF__Merge10__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Merge11__c" {
  const MC4SF__Merge11__c:string;
  export default MC4SF__Merge11__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Merge12__c" {
  const MC4SF__Merge12__c:string;
  export default MC4SF__Merge12__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Merge13__c" {
  const MC4SF__Merge13__c:string;
  export default MC4SF__Merge13__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Merge14__c" {
  const MC4SF__Merge14__c:string;
  export default MC4SF__Merge14__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Merge15__c" {
  const MC4SF__Merge15__c:string;
  export default MC4SF__Merge15__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Merge16__c" {
  const MC4SF__Merge16__c:string;
  export default MC4SF__Merge16__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Merge17__c" {
  const MC4SF__Merge17__c:string;
  export default MC4SF__Merge17__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Merge18__c" {
  const MC4SF__Merge18__c:string;
  export default MC4SF__Merge18__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Merge19__c" {
  const MC4SF__Merge19__c:string;
  export default MC4SF__Merge19__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Merge1__c" {
  const MC4SF__Merge1__c:string;
  export default MC4SF__Merge1__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Merge20__c" {
  const MC4SF__Merge20__c:string;
  export default MC4SF__Merge20__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Merge21__c" {
  const MC4SF__Merge21__c:string;
  export default MC4SF__Merge21__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Merge22__c" {
  const MC4SF__Merge22__c:string;
  export default MC4SF__Merge22__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Merge23__c" {
  const MC4SF__Merge23__c:string;
  export default MC4SF__Merge23__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Merge24__c" {
  const MC4SF__Merge24__c:string;
  export default MC4SF__Merge24__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Merge25__c" {
  const MC4SF__Merge25__c:string;
  export default MC4SF__Merge25__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Merge26__c" {
  const MC4SF__Merge26__c:string;
  export default MC4SF__Merge26__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Merge27__c" {
  const MC4SF__Merge27__c:string;
  export default MC4SF__Merge27__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Merge28__c" {
  const MC4SF__Merge28__c:string;
  export default MC4SF__Merge28__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Merge29__c" {
  const MC4SF__Merge29__c:string;
  export default MC4SF__Merge29__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Merge2__c" {
  const MC4SF__Merge2__c:string;
  export default MC4SF__Merge2__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Merge30__c" {
  const MC4SF__Merge30__c:string;
  export default MC4SF__Merge30__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Merge3__c" {
  const MC4SF__Merge3__c:string;
  export default MC4SF__Merge3__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Merge4__c" {
  const MC4SF__Merge4__c:string;
  export default MC4SF__Merge4__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Merge5__c" {
  const MC4SF__Merge5__c:string;
  export default MC4SF__Merge5__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Merge6__c" {
  const MC4SF__Merge6__c:string;
  export default MC4SF__Merge6__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Merge7__c" {
  const MC4SF__Merge7__c:string;
  export default MC4SF__Merge7__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Merge8__c" {
  const MC4SF__Merge8__c:string;
  export default MC4SF__Merge8__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Merge9__c" {
  const MC4SF__Merge9__c:string;
  export default MC4SF__Merge9__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Notes__c" {
  const MC4SF__Notes__c:string;
  export default MC4SF__Notes__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Parent_MC_Subscriber__c" {
  const MC4SF__Parent_MC_Subscriber__c:any;
  export default MC4SF__Parent_MC_Subscriber__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Region__c" {
  const MC4SF__Region__c:string;
  export default MC4SF__Region__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Static_Segments__c" {
  const MC4SF__Static_Segments__c:string;
  export default MC4SF__Static_Segments__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Status_Reason_Text__c" {
  const MC4SF__Status_Reason_Text__c:string;
  export default MC4SF__Status_Reason_Text__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Status_Reason__c" {
  const MC4SF__Status_Reason__c:string;
  export default MC4SF__Status_Reason__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Status_Timestamp__c" {
  const MC4SF__Status_Timestamp__c:any;
  export default MC4SF__Status_Timestamp__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Subscriber_Last_Modified_Since_Sync_Date__c" {
  const MC4SF__Subscriber_Last_Modified_Since_Sync_Date__c:any;
  export default MC4SF__Subscriber_Last_Modified_Since_Sync_Date__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Time_Zone__c" {
  const MC4SF__Time_Zone__c:string;
  export default MC4SF__Time_Zone__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Timestamp_Opt__c" {
  const MC4SF__Timestamp_Opt__c:any;
  export default MC4SF__Timestamp_Opt__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Timestamp_Signup__c" {
  const MC4SF__Timestamp_Signup__c:any;
  export default MC4SF__Timestamp_Signup__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__View_Subscriber_in_MC__c" {
  const MC4SF__View_Subscriber_in_MC__c:string;
  export default MC4SF__View_Subscriber_in_MC__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Web_ID__c" {
  const MC4SF__Web_ID__c:number;
  export default MC4SF__Web_ID__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber__ChangeEvent.MC4SF__Last_Activity_Date__c" {
  const MC4SF__Last_Activity_Date__c:any;
  export default MC4SF__Last_Activity_Date__c;
}
