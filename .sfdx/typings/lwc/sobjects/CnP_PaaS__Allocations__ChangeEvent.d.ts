declare module "@salesforce/schema/CnP_PaaS__Allocations__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Allocations__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__Allocations__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__Allocations__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__Allocations__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__Allocations__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Allocations__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Allocations__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Allocations__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Allocations__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Allocations__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Allocations__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Allocations__ChangeEvent.CnP_PaaS__Amount__c" {
  const CnP_PaaS__Amount__c:number;
  export default CnP_PaaS__Amount__c;
}
declare module "@salesforce/schema/CnP_PaaS__Allocations__ChangeEvent.CnP_PaaS__Class__c" {
  const CnP_PaaS__Class__c:any;
  export default CnP_PaaS__Class__c;
}
declare module "@salesforce/schema/CnP_PaaS__Allocations__ChangeEvent.CnP_PaaS__Ledger__c" {
  const CnP_PaaS__Ledger__c:any;
  export default CnP_PaaS__Ledger__c;
}
declare module "@salesforce/schema/CnP_PaaS__Allocations__ChangeEvent.CnP_PaaS__Opportunity__c" {
  const CnP_PaaS__Opportunity__c:any;
  export default CnP_PaaS__Opportunity__c;
}
declare module "@salesforce/schema/CnP_PaaS__Allocations__ChangeEvent.CnP_PaaS__Sub_Class__c" {
  const CnP_PaaS__Sub_Class__c:any;
  export default CnP_PaaS__Sub_Class__c;
}
