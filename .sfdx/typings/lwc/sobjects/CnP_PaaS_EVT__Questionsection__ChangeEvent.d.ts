declare module "@salesforce/schema/CnP_PaaS_EVT__Questionsection__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Questionsection__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Questionsection__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Questionsection__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Questionsection__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Questionsection__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Questionsection__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Questionsection__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Questionsection__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Questionsection__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Questionsection__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Questionsection__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Questionsection__ChangeEvent.CnP_PaaS_EVT__Event_name__c" {
  const CnP_PaaS_EVT__Event_name__c:any;
  export default CnP_PaaS_EVT__Event_name__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Questionsection__ChangeEvent.CnP_PaaS_EVT__Registration_Level__c" {
  const CnP_PaaS_EVT__Registration_Level__c:any;
  export default CnP_PaaS_EVT__Registration_Level__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Questionsection__ChangeEvent.CnP_PaaS_EVT__Section_Order__c" {
  const CnP_PaaS_EVT__Section_Order__c:number;
  export default CnP_PaaS_EVT__Section_Order__c;
}
