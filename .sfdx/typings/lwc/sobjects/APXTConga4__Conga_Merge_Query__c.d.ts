declare module "@salesforce/schema/APXTConga4__Conga_Merge_Query__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/APXTConga4__Conga_Merge_Query__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/APXTConga4__Conga_Merge_Query__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/APXTConga4__Conga_Merge_Query__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/APXTConga4__Conga_Merge_Query__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/APXTConga4__Conga_Merge_Query__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/APXTConga4__Conga_Merge_Query__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/APXTConga4__Conga_Merge_Query__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/APXTConga4__Conga_Merge_Query__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/APXTConga4__Conga_Merge_Query__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/APXTConga4__Conga_Merge_Query__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/APXTConga4__Conga_Merge_Query__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/APXTConga4__Conga_Merge_Query__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/APXTConga4__Conga_Merge_Query__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/APXTConga4__Conga_Merge_Query__c.APXTConga4__Description__c" {
  const APXTConga4__Description__c:string;
  export default APXTConga4__Description__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Merge_Query__c.APXTConga4__Name__c" {
  const APXTConga4__Name__c:string;
  export default APXTConga4__Name__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Merge_Query__c.APXTConga4__Query__c" {
  const APXTConga4__Query__c:string;
  export default APXTConga4__Query__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Merge_Query__c.APXTConga4__Key__c" {
  const APXTConga4__Key__c:string;
  export default APXTConga4__Key__c;
}
