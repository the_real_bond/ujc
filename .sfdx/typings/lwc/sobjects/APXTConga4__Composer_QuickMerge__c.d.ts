declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__c.APXTConga4__Conga_Solution__r" {
  const APXTConga4__Conga_Solution__r:any;
  export default APXTConga4__Conga_Solution__r;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__c.APXTConga4__Conga_Solution__c" {
  const APXTConga4__Conga_Solution__c:any;
  export default APXTConga4__Conga_Solution__c;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__c.APXTConga4__Description__c" {
  const APXTConga4__Description__c:string;
  export default APXTConga4__Description__c;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__c.APXTConga4__Launch_CM8__c" {
  const APXTConga4__Launch_CM8__c:string;
  export default APXTConga4__Launch_CM8__c;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__c.APXTConga4__Weblink_ID_Formula__c" {
  const APXTConga4__Weblink_ID_Formula__c:string;
  export default APXTConga4__Weblink_ID_Formula__c;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__c.APXTConga4__Weblink_ID__c" {
  const APXTConga4__Weblink_ID__c:string;
  export default APXTConga4__Weblink_ID__c;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__c.APXTConga4__Weblink_Name_Formula__c" {
  const APXTConga4__Weblink_Name_Formula__c:string;
  export default APXTConga4__Weblink_Name_Formula__c;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__c.APXTConga4__Weblink_Name__c" {
  const APXTConga4__Weblink_Name__c:string;
  export default APXTConga4__Weblink_Name__c;
}
