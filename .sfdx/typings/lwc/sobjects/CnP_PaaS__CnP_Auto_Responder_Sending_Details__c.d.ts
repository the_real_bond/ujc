declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Sending_Details__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Sending_Details__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Sending_Details__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Sending_Details__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Sending_Details__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Sending_Details__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Sending_Details__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Sending_Details__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Sending_Details__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Sending_Details__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Sending_Details__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Sending_Details__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Sending_Details__c.CnP_PaaS__Application_Name__c" {
  const CnP_PaaS__Application_Name__c:string;
  export default CnP_PaaS__Application_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Sending_Details__c.CnP_PaaS__CnP_Auto_Responder_Settings__r" {
  const CnP_PaaS__CnP_Auto_Responder_Settings__r:any;
  export default CnP_PaaS__CnP_Auto_Responder_Settings__r;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Sending_Details__c.CnP_PaaS__CnP_Auto_Responder_Settings__c" {
  const CnP_PaaS__CnP_Auto_Responder_Settings__c:any;
  export default CnP_PaaS__CnP_Auto_Responder_Settings__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Sending_Details__c.CnP_PaaS__CnP_Designer__r" {
  const CnP_PaaS__CnP_Designer__r:any;
  export default CnP_PaaS__CnP_Designer__r;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Sending_Details__c.CnP_PaaS__CnP_Designer__c" {
  const CnP_PaaS__CnP_Designer__c:any;
  export default CnP_PaaS__CnP_Designer__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Sending_Details__c.CnP_PaaS__CnP_Transaction__r" {
  const CnP_PaaS__CnP_Transaction__r:any;
  export default CnP_PaaS__CnP_Transaction__r;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Sending_Details__c.CnP_PaaS__CnP_Transaction__c" {
  const CnP_PaaS__CnP_Transaction__c:any;
  export default CnP_PaaS__CnP_Transaction__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Sending_Details__c.CnP_PaaS__Contact__r" {
  const CnP_PaaS__Contact__r:any;
  export default CnP_PaaS__Contact__r;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Sending_Details__c.CnP_PaaS__Contact__c" {
  const CnP_PaaS__Contact__c:any;
  export default CnP_PaaS__Contact__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Sending_Details__c.CnP_PaaS__No_Of_Times__c" {
  const CnP_PaaS__No_Of_Times__c:string;
  export default CnP_PaaS__No_Of_Times__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Sending_Details__c.CnP_PaaS__Sended_Date__c" {
  const CnP_PaaS__Sended_Date__c:any;
  export default CnP_PaaS__Sended_Date__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Sending_Details__c.CnP_PaaS__Status__c" {
  const CnP_PaaS__Status__c:string;
  export default CnP_PaaS__Status__c;
}
