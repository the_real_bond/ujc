declare module "@salesforce/schema/PowerLoader__Page_Layout_Cache__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/PowerLoader__Page_Layout_Cache__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/PowerLoader__Page_Layout_Cache__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/PowerLoader__Page_Layout_Cache__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/PowerLoader__Page_Layout_Cache__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/PowerLoader__Page_Layout_Cache__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/PowerLoader__Page_Layout_Cache__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/PowerLoader__Page_Layout_Cache__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/PowerLoader__Page_Layout_Cache__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/PowerLoader__Page_Layout_Cache__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/PowerLoader__Page_Layout_Cache__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/PowerLoader__Page_Layout_Cache__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/PowerLoader__Page_Layout_Cache__c.PowerLoader__Object_API_Name__c" {
  const PowerLoader__Object_API_Name__c:string;
  export default PowerLoader__Object_API_Name__c;
}
declare module "@salesforce/schema/PowerLoader__Page_Layout_Cache__c.PowerLoader__Record_Type_ID__c" {
  const PowerLoader__Record_Type_ID__c:string;
  export default PowerLoader__Record_Type_ID__c;
}
