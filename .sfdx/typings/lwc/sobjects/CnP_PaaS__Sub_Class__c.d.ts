declare module "@salesforce/schema/CnP_PaaS__Sub_Class__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Class__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Class__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Class__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Class__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Class__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Class__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Class__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Class__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Class__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Class__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Class__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Class__c.CnP_PaaS__Class__r" {
  const CnP_PaaS__Class__r:any;
  export default CnP_PaaS__Class__r;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Class__c.CnP_PaaS__Class__c" {
  const CnP_PaaS__Class__c:any;
  export default CnP_PaaS__Class__c;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Class__c.CnP_PaaS__Description__c" {
  const CnP_PaaS__Description__c:string;
  export default CnP_PaaS__Description__c;
}
