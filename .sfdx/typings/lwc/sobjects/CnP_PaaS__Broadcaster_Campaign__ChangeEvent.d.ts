declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CnP_PaaS__BroadCaster_Service__c" {
  const CnP_PaaS__BroadCaster_Service__c:any;
  export default CnP_PaaS__BroadCaster_Service__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CnP_PaaS__Analytics__c" {
  const CnP_PaaS__Analytics__c:string;
  export default CnP_PaaS__Analytics__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CnP_PaaS__Analytics_tag__c" {
  const CnP_PaaS__Analytics_tag__c:string;
  export default CnP_PaaS__Analytics_tag__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CnP_PaaS__Authenticate__c" {
  const CnP_PaaS__Authenticate__c:boolean;
  export default CnP_PaaS__Authenticate__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CnP_PaaS__Broadcaster_List__c" {
  const CnP_PaaS__Broadcaster_List__c:any;
  export default CnP_PaaS__Broadcaster_List__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CnP_PaaS__Camp_Id__c" {
  const CnP_PaaS__Camp_Id__c:string;
  export default CnP_PaaS__Camp_Id__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CnP_PaaS__Content_Type__c" {
  const CnP_PaaS__Content_Type__c:string;
  export default CnP_PaaS__Content_Type__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CnP_PaaS__Create_Time__c" {
  const CnP_PaaS__Create_Time__c:string;
  export default CnP_PaaS__Create_Time__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CnP_PaaS__Email_Sent__c" {
  const CnP_PaaS__Email_Sent__c:number;
  export default CnP_PaaS__Email_Sent__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CnP_PaaS__Folder_Id__c" {
  const CnP_PaaS__Folder_Id__c:number;
  export default CnP_PaaS__Folder_Id__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CnP_PaaS__From_email__c" {
  const CnP_PaaS__From_email__c:string;
  export default CnP_PaaS__From_email__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CnP_PaaS__From_name__c" {
  const CnP_PaaS__From_name__c:string;
  export default CnP_PaaS__From_name__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CnP_PaaS__List_Id__c" {
  const CnP_PaaS__List_Id__c:string;
  export default CnP_PaaS__List_Id__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CnP_PaaS__Parent_Id__c" {
  const CnP_PaaS__Parent_Id__c:string;
  export default CnP_PaaS__Parent_Id__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CnP_PaaS__Send_Time__c" {
  const CnP_PaaS__Send_Time__c:string;
  export default CnP_PaaS__Send_Time__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CnP_PaaS__Status__c" {
  const CnP_PaaS__Status__c:string;
  export default CnP_PaaS__Status__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CnP_PaaS__Subject__c" {
  const CnP_PaaS__Subject__c:string;
  export default CnP_PaaS__Subject__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CnP_PaaS__Template_Id__c" {
  const CnP_PaaS__Template_Id__c:number;
  export default CnP_PaaS__Template_Id__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CnP_PaaS__Timewarp__c" {
  const CnP_PaaS__Timewarp__c:boolean;
  export default CnP_PaaS__Timewarp__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CnP_PaaS__Timewarp_schedule__c" {
  const CnP_PaaS__Timewarp_schedule__c:string;
  export default CnP_PaaS__Timewarp_schedule__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CnP_PaaS__To_Name__c" {
  const CnP_PaaS__To_Name__c:string;
  export default CnP_PaaS__To_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CnP_PaaS__Type__c" {
  const CnP_PaaS__Type__c:string;
  export default CnP_PaaS__Type__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CnP_PaaS__Web_Id__c" {
  const CnP_PaaS__Web_Id__c:number;
  export default CnP_PaaS__Web_Id__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CnP_PaaS__archieve_URL__c" {
  const CnP_PaaS__archieve_URL__c:string;
  export default CnP_PaaS__archieve_URL__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CnP_PaaS__auto_fb_post__c" {
  const CnP_PaaS__auto_fb_post__c:string;
  export default CnP_PaaS__auto_fb_post__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CnP_PaaS__auto_footer__c" {
  const CnP_PaaS__auto_footer__c:boolean;
  export default CnP_PaaS__auto_footer__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CnP_PaaS__auto_tweet__c" {
  const CnP_PaaS__auto_tweet__c:boolean;
  export default CnP_PaaS__auto_tweet__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CnP_PaaS__ecomm360__c" {
  const CnP_PaaS__ecomm360__c:boolean;
  export default CnP_PaaS__ecomm360__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CnP_PaaS__html_clicks__c" {
  const CnP_PaaS__html_clicks__c:boolean;
  export default CnP_PaaS__html_clicks__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CnP_PaaS__inline_CSS__c" {
  const CnP_PaaS__inline_CSS__c:boolean;
  export default CnP_PaaS__inline_CSS__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CnP_PaaS__opens__c" {
  const CnP_PaaS__opens__c:boolean;
  export default CnP_PaaS__opens__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CnP_PaaS__segment_text__c" {
  const CnP_PaaS__segment_text__c:string;
  export default CnP_PaaS__segment_text__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Campaign__ChangeEvent.CnP_PaaS__text_clicks__c" {
  const CnP_PaaS__text_clicks__c:boolean;
  export default CnP_PaaS__text_clicks__c;
}
