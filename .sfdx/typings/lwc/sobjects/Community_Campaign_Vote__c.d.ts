declare module "@salesforce/schema/Community_Campaign_Vote__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Community_Campaign_Vote__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Community_Campaign_Vote__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Community_Campaign_Vote__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Community_Campaign_Vote__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Community_Campaign_Vote__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Community_Campaign_Vote__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Community_Campaign_Vote__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Community_Campaign_Vote__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Community_Campaign_Vote__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Community_Campaign_Vote__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/Community_Campaign_Vote__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Community_Campaign_Vote__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Community_Campaign_Vote__c.Community_Campaign__r" {
  const Community_Campaign__r:any;
  export default Community_Campaign__r;
}
declare module "@salesforce/schema/Community_Campaign_Vote__c.Community_Campaign__c" {
  const Community_Campaign__c:any;
  export default Community_Campaign__c;
}
declare module "@salesforce/schema/Community_Campaign_Vote__c.Communal_Reference_Number__c" {
  const Communal_Reference_Number__c:string;
  export default Communal_Reference_Number__c;
}
declare module "@salesforce/schema/Community_Campaign_Vote__c.Community_Register__r" {
  const Community_Register__r:any;
  export default Community_Register__r;
}
declare module "@salesforce/schema/Community_Campaign_Vote__c.Community_Register__c" {
  const Community_Register__c:any;
  export default Community_Register__c;
}
declare module "@salesforce/schema/Community_Campaign_Vote__c.Email__c" {
  const Email__c:string;
  export default Email__c;
}
declare module "@salesforce/schema/Community_Campaign_Vote__c.First_Name__c" {
  const First_Name__c:string;
  export default First_Name__c;
}
declare module "@salesforce/schema/Community_Campaign_Vote__c.Last_Name__c" {
  const Last_Name__c:string;
  export default Last_Name__c;
}
declare module "@salesforce/schema/Community_Campaign_Vote__c.Send_Email__c" {
  const Send_Email__c:boolean;
  export default Send_Email__c;
}
declare module "@salesforce/schema/Community_Campaign_Vote__c.CellPhone__c" {
  const CellPhone__c:string;
  export default CellPhone__c;
}
declare module "@salesforce/schema/Community_Campaign_Vote__c.Mail_Merge_Addres__c" {
  const Mail_Merge_Addres__c:string;
  export default Mail_Merge_Addres__c;
}
declare module "@salesforce/schema/Community_Campaign_Vote__c.Postal_Code__c" {
  const Postal_Code__c:string;
  export default Postal_Code__c;
}
declare module "@salesforce/schema/Community_Campaign_Vote__c.Prefered_Name__c" {
  const Prefered_Name__c:string;
  export default Prefered_Name__c;
}
declare module "@salesforce/schema/Community_Campaign_Vote__c.Residential_Address__c" {
  const Residential_Address__c:string;
  export default Residential_Address__c;
}
declare module "@salesforce/schema/Community_Campaign_Vote__c.Suburb__c" {
  const Suburb__c:string;
  export default Suburb__c;
}
declare module "@salesforce/schema/Community_Campaign_Vote__c.No_of_Candidate_Votes__c" {
  const No_of_Candidate_Votes__c:number;
  export default No_of_Candidate_Votes__c;
}
