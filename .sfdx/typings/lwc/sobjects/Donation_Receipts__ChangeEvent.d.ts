declare module "@salesforce/schema/Donation_Receipts__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Donation_Receipts__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/Donation_Receipts__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/Donation_Receipts__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Donation_Receipts__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Donation_Receipts__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Donation_Receipts__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Donation_Receipts__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Donation_Receipts__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Donation_Receipts__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Donation_Receipts__ChangeEvent.Tax_Receipt__c" {
  const Tax_Receipt__c:any;
  export default Tax_Receipt__c;
}
declare module "@salesforce/schema/Donation_Receipts__ChangeEvent.Receipt__c" {
  const Receipt__c:any;
  export default Receipt__c;
}
declare module "@salesforce/schema/Donation_Receipts__ChangeEvent.Amount_in_Words__c" {
  const Amount_in_Words__c:string;
  export default Amount_in_Words__c;
}
declare module "@salesforce/schema/Donation_Receipts__ChangeEvent.Date__c" {
  const Date__c:any;
  export default Date__c;
}
declare module "@salesforce/schema/Donation_Receipts__ChangeEvent.Donation_Amount__c" {
  const Donation_Amount__c:number;
  export default Donation_Amount__c;
}
declare module "@salesforce/schema/Donation_Receipts__ChangeEvent.Donation_Type__c" {
  const Donation_Type__c:string;
  export default Donation_Type__c;
}
declare module "@salesforce/schema/Donation_Receipts__ChangeEvent.Type__c" {
  const Type__c:string;
  export default Type__c;
}
declare module "@salesforce/schema/Donation_Receipts__ChangeEvent.dateclosed__c" {
  const dateclosed__c:string;
  export default dateclosed__c;
}
declare module "@salesforce/schema/Donation_Receipts__ChangeEvent.Donor__c" {
  const Donor__c:string;
  export default Donor__c;
}
