declare module "@salesforce/schema/SocialPersona.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/SocialPersona.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/SocialPersona.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/SocialPersona.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/SocialPersona.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/SocialPersona.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/SocialPersona.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/SocialPersona.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/SocialPersona.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/SocialPersona.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/SocialPersona.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/SocialPersona.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/SocialPersona.Parent" {
  const Parent:any;
  export default Parent;
}
declare module "@salesforce/schema/SocialPersona.ParentId" {
  const ParentId:any;
  export default ParentId;
}
declare module "@salesforce/schema/SocialPersona.Provider" {
  const Provider:string;
  export default Provider;
}
declare module "@salesforce/schema/SocialPersona.ExternalId" {
  const ExternalId:string;
  export default ExternalId;
}
declare module "@salesforce/schema/SocialPersona.IsDefault" {
  const IsDefault:boolean;
  export default IsDefault;
}
declare module "@salesforce/schema/SocialPersona.ExternalPictureURL" {
  const ExternalPictureURL:string;
  export default ExternalPictureURL;
}
declare module "@salesforce/schema/SocialPersona.ProfileUrl" {
  const ProfileUrl:string;
  export default ProfileUrl;
}
declare module "@salesforce/schema/SocialPersona.TopicType" {
  const TopicType:string;
  export default TopicType;
}
declare module "@salesforce/schema/SocialPersona.IsBlacklisted" {
  const IsBlacklisted:boolean;
  export default IsBlacklisted;
}
declare module "@salesforce/schema/SocialPersona.RealName" {
  const RealName:string;
  export default RealName;
}
declare module "@salesforce/schema/SocialPersona.IsFollowingUs" {
  const IsFollowingUs:boolean;
  export default IsFollowingUs;
}
declare module "@salesforce/schema/SocialPersona.AreWeFollowing" {
  const AreWeFollowing:boolean;
  export default AreWeFollowing;
}
declare module "@salesforce/schema/SocialPersona.MediaType" {
  const MediaType:string;
  export default MediaType;
}
declare module "@salesforce/schema/SocialPersona.SourceApp" {
  const SourceApp:string;
  export default SourceApp;
}
declare module "@salesforce/schema/SocialPersona.AuthorLabels" {
  const AuthorLabels:string;
  export default AuthorLabels;
}
declare module "@salesforce/schema/SocialPersona.IsVerified" {
  const IsVerified:boolean;
  export default IsVerified;
}
declare module "@salesforce/schema/SocialPersona.InfluencerScore" {
  const InfluencerScore:number;
  export default InfluencerScore;
}
declare module "@salesforce/schema/SocialPersona.AvatarUrl" {
  const AvatarUrl:string;
  export default AvatarUrl;
}
