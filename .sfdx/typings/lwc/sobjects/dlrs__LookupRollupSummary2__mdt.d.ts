declare module "@salesforce/schema/dlrs__LookupRollupSummary2__mdt.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary2__mdt.DeveloperName" {
  const DeveloperName:string;
  export default DeveloperName;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary2__mdt.MasterLabel" {
  const MasterLabel:string;
  export default MasterLabel;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary2__mdt.Language" {
  const Language:string;
  export default Language;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary2__mdt.NamespacePrefix" {
  const NamespacePrefix:string;
  export default NamespacePrefix;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary2__mdt.Label" {
  const Label:string;
  export default Label;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary2__mdt.QualifiedApiName" {
  const QualifiedApiName:string;
  export default QualifiedApiName;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary2__mdt.dlrs__Active__c" {
  const dlrs__Active__c:boolean;
  export default dlrs__Active__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary2__mdt.dlrs__AggregateAllRows__c" {
  const dlrs__AggregateAllRows__c:boolean;
  export default dlrs__AggregateAllRows__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary2__mdt.dlrs__AggregateOperation__c" {
  const dlrs__AggregateOperation__c:string;
  export default dlrs__AggregateOperation__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary2__mdt.dlrs__AggregateResultField__c" {
  const dlrs__AggregateResultField__c:string;
  export default dlrs__AggregateResultField__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary2__mdt.dlrs__CalculationMode__c" {
  const dlrs__CalculationMode__c:string;
  export default dlrs__CalculationMode__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary2__mdt.dlrs__CalculationSharingMode__c" {
  const dlrs__CalculationSharingMode__c:string;
  export default dlrs__CalculationSharingMode__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary2__mdt.dlrs__ChildObject__c" {
  const dlrs__ChildObject__c:string;
  export default dlrs__ChildObject__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary2__mdt.dlrs__ConcatenateDelimiter__c" {
  const dlrs__ConcatenateDelimiter__c:string;
  export default dlrs__ConcatenateDelimiter__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary2__mdt.dlrs__Description__c" {
  const dlrs__Description__c:string;
  export default dlrs__Description__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary2__mdt.dlrs__FieldToAggregate__c" {
  const dlrs__FieldToAggregate__c:string;
  export default dlrs__FieldToAggregate__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary2__mdt.dlrs__FieldToOrderBy__c" {
  const dlrs__FieldToOrderBy__c:string;
  export default dlrs__FieldToOrderBy__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary2__mdt.dlrs__ParentObject__c" {
  const dlrs__ParentObject__c:string;
  export default dlrs__ParentObject__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary2__mdt.dlrs__RelationshipCriteriaFields__c" {
  const dlrs__RelationshipCriteriaFields__c:string;
  export default dlrs__RelationshipCriteriaFields__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary2__mdt.dlrs__RelationshipCriteria__c" {
  const dlrs__RelationshipCriteria__c:string;
  export default dlrs__RelationshipCriteria__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary2__mdt.dlrs__RelationshipField__c" {
  const dlrs__RelationshipField__c:string;
  export default dlrs__RelationshipField__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary2__mdt.dlrs__RowLimit__c" {
  const dlrs__RowLimit__c:number;
  export default dlrs__RowLimit__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary2__mdt.dlrs__TestCode2__c" {
  const dlrs__TestCode2__c:string;
  export default dlrs__TestCode2__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary2__mdt.dlrs__TestCodeSeeAllData__c" {
  const dlrs__TestCodeSeeAllData__c:boolean;
  export default dlrs__TestCodeSeeAllData__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary2__mdt.dlrs__TestCode__c" {
  const dlrs__TestCode__c:string;
  export default dlrs__TestCode__c;
}
