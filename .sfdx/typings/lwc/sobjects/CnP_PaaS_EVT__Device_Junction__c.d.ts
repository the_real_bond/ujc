declare module "@salesforce/schema/CnP_PaaS_EVT__Device_Junction__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Device_Junction__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Device_Junction__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Device_Junction__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Device_Junction__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Device_Junction__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Device_Junction__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Device_Junction__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Device_Junction__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Device_Junction__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Device_Junction__c.CnP_PaaS_EVT__Authenticate_Device__r" {
  const CnP_PaaS_EVT__Authenticate_Device__r:any;
  export default CnP_PaaS_EVT__Authenticate_Device__r;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Device_Junction__c.CnP_PaaS_EVT__Authenticate_Device__c" {
  const CnP_PaaS_EVT__Authenticate_Device__c:any;
  export default CnP_PaaS_EVT__Authenticate_Device__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Device_Junction__c.CnP_PaaS_EVT__C_P_Event__r" {
  const CnP_PaaS_EVT__C_P_Event__r:any;
  export default CnP_PaaS_EVT__C_P_Event__r;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Device_Junction__c.CnP_PaaS_EVT__C_P_Event__c" {
  const CnP_PaaS_EVT__C_P_Event__c:any;
  export default CnP_PaaS_EVT__C_P_Event__c;
}
