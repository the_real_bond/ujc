declare module "@salesforce/schema/APXTConga4__Conga_Collection__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/APXTConga4__Conga_Collection__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/APXTConga4__Conga_Collection__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/APXTConga4__Conga_Collection__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/APXTConga4__Conga_Collection__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/APXTConga4__Conga_Collection__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/APXTConga4__Conga_Collection__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/APXTConga4__Conga_Collection__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/APXTConga4__Conga_Collection__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/APXTConga4__Conga_Collection__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/APXTConga4__Conga_Collection__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/APXTConga4__Conga_Collection__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/APXTConga4__Conga_Collection__ChangeEvent.APXTConga4__Description__c" {
  const APXTConga4__Description__c:string;
  export default APXTConga4__Description__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Collection__ChangeEvent.APXTConga4__Is_SF1_Enabled__c" {
  const APXTConga4__Is_SF1_Enabled__c:boolean;
  export default APXTConga4__Is_SF1_Enabled__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Collection__ChangeEvent.APXTConga4__SF1_Binding_sObject_Type__c" {
  const APXTConga4__SF1_Binding_sObject_Type__c:string;
  export default APXTConga4__SF1_Binding_sObject_Type__c;
}
