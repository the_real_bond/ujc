declare module "@salesforce/schema/Batch_Email__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Batch_Email__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Batch_Email__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Batch_Email__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Batch_Email__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Batch_Email__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Batch_Email__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Batch_Email__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Batch_Email__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Batch_Email__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Batch_Email__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Batch_Email__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Batch_Email__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/Batch_Email__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Batch_Email__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Batch_Email__c.Body1_Welfare__c" {
  const Body1_Welfare__c:string;
  export default Body1_Welfare__c;
}
declare module "@salesforce/schema/Batch_Email__c.Body_1_UCFED__c" {
  const Body_1_UCFED__c:string;
  export default Body_1_UCFED__c;
}
declare module "@salesforce/schema/Batch_Email__c.Date__c" {
  const Date__c:string;
  export default Date__c;
}
declare module "@salesforce/schema/Batch_Email__c.Email_Body__c" {
  const Email_Body__c:string;
  export default Email_Body__c;
}
declare module "@salesforce/schema/Batch_Email__c.Email_Subject_UCFED__c" {
  const Email_Subject_UCFED__c:string;
  export default Email_Subject_UCFED__c;
}
declare module "@salesforce/schema/Batch_Email__c.Email_Subject__c" {
  const Email_Subject__c:string;
  export default Email_Subject__c;
}
