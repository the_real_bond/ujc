declare module "@salesforce/schema/CampaignMemberChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.Campaign" {
  const Campaign:any;
  export default Campaign;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.CampaignId" {
  const CampaignId:any;
  export default CampaignId;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.Lead" {
  const Lead:any;
  export default Lead;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.LeadId" {
  const LeadId:any;
  export default LeadId;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.Contact" {
  const Contact:any;
  export default Contact;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.ContactId" {
  const ContactId:any;
  export default ContactId;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.Status" {
  const Status:string;
  export default Status;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.HasResponded" {
  const HasResponded:boolean;
  export default HasResponded;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.FirstRespondedDate" {
  const FirstRespondedDate:any;
  export default FirstRespondedDate;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.Payment_Status__c" {
  const Payment_Status__c:string;
  export default Payment_Status__c;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.Name_Corporate_Booking__c" {
  const Name_Corporate_Booking__c:string;
  export default Name_Corporate_Booking__c;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.Booked_with__c" {
  const Booked_with__c:any;
  export default Booked_with__c;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.Quantity__c" {
  const Quantity__c:number;
  export default Quantity__c;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.Timeslot__c" {
  const Timeslot__c:string;
  export default Timeslot__c;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.CampaignMTool__Address_Of_Member__c" {
  const CampaignMTool__Address_Of_Member__c:string;
  export default CampaignMTool__Address_Of_Member__c;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.CampaignMTool__City_Of_Member__c" {
  const CampaignMTool__City_Of_Member__c:string;
  export default CampaignMTool__City_Of_Member__c;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.CampaignMTool__Company_Of_Member__c" {
  const CampaignMTool__Company_Of_Member__c:string;
  export default CampaignMTool__Company_Of_Member__c;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.CampaignMTool__Country_Of_Member__c" {
  const CampaignMTool__Country_Of_Member__c:string;
  export default CampaignMTool__Country_Of_Member__c;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.CampaignMTool__Do_Not_Call_Member__c" {
  const CampaignMTool__Do_Not_Call_Member__c:string;
  export default CampaignMTool__Do_Not_Call_Member__c;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.CampaignMTool__Email_Of_Member__c" {
  const CampaignMTool__Email_Of_Member__c:string;
  export default CampaignMTool__Email_Of_Member__c;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.CampaignMTool__Fax_Of_Member__c" {
  const CampaignMTool__Fax_Of_Member__c:string;
  export default CampaignMTool__Fax_Of_Member__c;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.CampaignMTool__MobilePhone_Of_Member__c" {
  const CampaignMTool__MobilePhone_Of_Member__c:string;
  export default CampaignMTool__MobilePhone_Of_Member__c;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.CampaignMTool__Name_Of_Member__c" {
  const CampaignMTool__Name_Of_Member__c:string;
  export default CampaignMTool__Name_Of_Member__c;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.CampaignMTool__Phone_Of_Member__c" {
  const CampaignMTool__Phone_Of_Member__c:string;
  export default CampaignMTool__Phone_Of_Member__c;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.CampaignMTool__PostalCode_Of_Member__c" {
  const CampaignMTool__PostalCode_Of_Member__c:string;
  export default CampaignMTool__PostalCode_Of_Member__c;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.CampaignMTool__Related_To__c" {
  const CampaignMTool__Related_To__c:any;
  export default CampaignMTool__Related_To__c;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.CampaignMTool__Salutation_Of_Member__c" {
  const CampaignMTool__Salutation_Of_Member__c:string;
  export default CampaignMTool__Salutation_Of_Member__c;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.CampaignMTool__Title_Of_Member__c" {
  const CampaignMTool__Title_Of_Member__c:string;
  export default CampaignMTool__Title_Of_Member__c;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.Status_Reason__c" {
  const Status_Reason__c:string;
  export default Status_Reason__c;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.Database_Status__c" {
  const Database_Status__c:string;
  export default Database_Status__c;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.Pledge_Card_Created__c" {
  const Pledge_Card_Created__c:boolean;
  export default Pledge_Card_Created__c;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.Hometown__c" {
  const Hometown__c:string;
  export default Hometown__c;
}
declare module "@salesforce/schema/CampaignMemberChangeEvent.Location__c" {
  const Location__c:string;
  export default Location__c;
}
