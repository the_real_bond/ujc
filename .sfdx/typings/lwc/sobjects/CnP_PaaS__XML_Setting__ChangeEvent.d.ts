declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Account_Number__c" {
  const CnP_PaaS__Account_Number__c:string;
  export default CnP_PaaS__Account_Number__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Account_Record_Type__c" {
  const CnP_PaaS__Account_Record_Type__c:string;
  export default CnP_PaaS__Account_Record_Type__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Account_Type__c" {
  const CnP_PaaS__Account_Type__c:string;
  export default CnP_PaaS__Account_Type__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Account_lp__c" {
  const CnP_PaaS__Account_lp__c:any;
  export default CnP_PaaS__Account_lp__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Accounts__c" {
  const CnP_PaaS__Accounts__c:boolean;
  export default CnP_PaaS__Accounts__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Activate_Swiper1__c" {
  const CnP_PaaS__Activate_Swiper1__c:boolean;
  export default CnP_PaaS__Activate_Swiper1__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Add_Questions__c" {
  const CnP_PaaS__Add_Questions__c:boolean;
  export default CnP_PaaS__Add_Questions__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Amount__c" {
  const CnP_PaaS__Amount__c:string;
  export default CnP_PaaS__Amount__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Authorized_Opportunity__c" {
  const CnP_PaaS__Authorized_Opportunity__c:boolean;
  export default CnP_PaaS__Authorized_Opportunity__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Campaign__c" {
  const CnP_PaaS__Campaign__c:string;
  export default CnP_PaaS__Campaign__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Campaign_lp__c" {
  const CnP_PaaS__Campaign_lp__c:any;
  export default CnP_PaaS__Campaign_lp__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Contact_Option__c" {
  const CnP_PaaS__Contact_Option__c:string;
  export default CnP_PaaS__Contact_Option__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Contact_Role__c" {
  const CnP_PaaS__Contact_Role__c:boolean;
  export default CnP_PaaS__Contact_Role__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Contact_Rolename__c" {
  const CnP_PaaS__Contact_Rolename__c:string;
  export default CnP_PaaS__Contact_Rolename__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Contacts__c" {
  const CnP_PaaS__Contacts__c:boolean;
  export default CnP_PaaS__Contacts__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Convenience_Amount__c" {
  const CnP_PaaS__Convenience_Amount__c:string;
  export default CnP_PaaS__Convenience_Amount__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Convenience_Fee__c" {
  const CnP_PaaS__Convenience_Fee__c:string;
  export default CnP_PaaS__Convenience_Fee__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Convenience_Opportunity__c" {
  const CnP_PaaS__Convenience_Opportunity__c:boolean;
  export default CnP_PaaS__Convenience_Opportunity__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Country_Format__c" {
  const CnP_PaaS__Country_Format__c:string;
  export default CnP_PaaS__Country_Format__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Custom_Payment_Options__c" {
  const CnP_PaaS__Custom_Payment_Options__c:string;
  export default CnP_PaaS__Custom_Payment_Options__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Custom_Payment_Type_Opportunity__c" {
  const CnP_PaaS__Custom_Payment_Type_Opportunity__c:boolean;
  export default CnP_PaaS__Custom_Payment_Type_Opportunity__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Custom_parameters__c" {
  const CnP_PaaS__Custom_parameters__c:string;
  export default CnP_PaaS__Custom_parameters__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Customize_Name__c" {
  const CnP_PaaS__Customize_Name__c:boolean;
  export default CnP_PaaS__Customize_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Declined_Opportunity__c" {
  const CnP_PaaS__Declined_Opportunity__c:boolean;
  export default CnP_PaaS__Declined_Opportunity__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Deductible_Charge__c" {
  const CnP_PaaS__Deductible_Charge__c:string;
  export default CnP_PaaS__Deductible_Charge__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Disableupdate__c" {
  const CnP_PaaS__Disableupdate__c:boolean;
  export default CnP_PaaS__Disableupdate__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Discount_Opportunity__c" {
  const CnP_PaaS__Discount_Opportunity__c:boolean;
  export default CnP_PaaS__Discount_Opportunity__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Discount__c" {
  const CnP_PaaS__Discount__c:string;
  export default CnP_PaaS__Discount__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Donor_s_Donations_Rank_Factor__c" {
  const CnP_PaaS__Donor_s_Donations_Rank_Factor__c:number;
  export default CnP_PaaS__Donor_s_Donations_Rank_Factor__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Donor_s_Raised_Donations_Rank_Factor__c" {
  const CnP_PaaS__Donor_s_Raised_Donations_Rank_Factor__c:number;
  export default CnP_PaaS__Donor_s_Raised_Donations_Rank_Factor__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Donors_Perso_Donations_Count_Rank_Factor__c" {
  const CnP_PaaS__Donors_Perso_Donations_Count_Rank_Factor__c:number;
  export default CnP_PaaS__Donors_Perso_Donations_Count_Rank_Factor__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Donors_Raised_Donations_Count_factor__c" {
  const CnP_PaaS__Donors_Raised_Donations_Count_factor__c:number;
  export default CnP_PaaS__Donors_Raised_Donations_Count_factor__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Email__c" {
  const CnP_PaaS__Email__c:boolean;
  export default CnP_PaaS__Email__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Internal_Notifications__c" {
  const CnP_PaaS__Internal_Notifications__c:string;
  export default CnP_PaaS__Internal_Notifications__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Invoice_Opportunity__c" {
  const CnP_PaaS__Invoice_Opportunity__c:boolean;
  export default CnP_PaaS__Invoice_Opportunity__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Lead_source__c" {
  const CnP_PaaS__Lead_source__c:string;
  export default CnP_PaaS__Lead_source__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Manual_Opportunities__c" {
  const CnP_PaaS__Manual_Opportunities__c:string;
  export default CnP_PaaS__Manual_Opportunities__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__No_Account__c" {
  const CnP_PaaS__No_Account__c:boolean;
  export default CnP_PaaS__No_Account__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Number_Of_Installments__c" {
  const CnP_PaaS__Number_Of_Installments__c:string;
  export default CnP_PaaS__Number_Of_Installments__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Oppor_Stage_Name__c" {
  const CnP_PaaS__Oppor_Stage_Name__c:string;
  export default CnP_PaaS__Oppor_Stage_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Opportunities__c" {
  const CnP_PaaS__Opportunities__c:boolean;
  export default CnP_PaaS__Opportunities__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Organization_Information__c" {
  const CnP_PaaS__Organization_Information__c:string;
  export default CnP_PaaS__Organization_Information__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Payment_Credit_Card__c" {
  const CnP_PaaS__Payment_Credit_Card__c:boolean;
  export default CnP_PaaS__Payment_Credit_Card__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Payment_Custom_Payment_Type__c" {
  const CnP_PaaS__Payment_Custom_Payment_Type__c:boolean;
  export default CnP_PaaS__Payment_Custom_Payment_Type__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Payment_For__c" {
  const CnP_PaaS__Payment_For__c:string;
  export default CnP_PaaS__Payment_For__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Payment_Invoice__c" {
  const CnP_PaaS__Payment_Invoice__c:boolean;
  export default CnP_PaaS__Payment_Invoice__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Payment_Purchase_Order__c" {
  const CnP_PaaS__Payment_Purchase_Order__c:boolean;
  export default CnP_PaaS__Payment_Purchase_Order__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Payment_eCheck__c" {
  const CnP_PaaS__Payment_eCheck__c:boolean;
  export default CnP_PaaS__Payment_eCheck__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Periodicity__c" {
  const CnP_PaaS__Periodicity__c:string;
  export default CnP_PaaS__Periodicity__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Person_Accounts__c" {
  const CnP_PaaS__Person_Accounts__c:boolean;
  export default CnP_PaaS__Person_Accounts__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Personal_Donations_Count_Factor__c" {
  const CnP_PaaS__Personal_Donations_Count_Factor__c:number;
  export default CnP_PaaS__Personal_Donations_Count_Factor__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Personal_Donations_Rank_Factor__c" {
  const CnP_PaaS__Personal_Donations_Rank_Factor__c:number;
  export default CnP_PaaS__Personal_Donations_Rank_Factor__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Products__c" {
  const CnP_PaaS__Products__c:boolean;
  export default CnP_PaaS__Products__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Purchase_Order_Opportunity__c" {
  const CnP_PaaS__Purchase_Order_Opportunity__c:boolean;
  export default CnP_PaaS__Purchase_Order_Opportunity__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Raised_Donations_Count_factor__c" {
  const CnP_PaaS__Raised_Donations_Count_factor__c:number;
  export default CnP_PaaS__Raised_Donations_Count_factor__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Raised_Donations_Rank_Factor__c" {
  const CnP_PaaS__Raised_Donations_Rank_Factor__c:number;
  export default CnP_PaaS__Raised_Donations_Rank_Factor__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Recurring_Type__c" {
  const CnP_PaaS__Recurring_Type__c:string;
  export default CnP_PaaS__Recurring_Type__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Recurring__c" {
  const CnP_PaaS__Recurring__c:boolean;
  export default CnP_PaaS__Recurring__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__SKU__c" {
  const CnP_PaaS__SKU__c:string;
  export default CnP_PaaS__SKU__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Send_Receipt__c" {
  const CnP_PaaS__Send_Receipt__c:boolean;
  export default CnP_PaaS__Send_Receipt__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Shipping_Opportunity__c" {
  const CnP_PaaS__Shipping_Opportunity__c:boolean;
  export default CnP_PaaS__Shipping_Opportunity__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Shipping__c" {
  const CnP_PaaS__Shipping__c:string;
  export default CnP_PaaS__Shipping__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Stage_Custom_Payment__c" {
  const CnP_PaaS__Stage_Custom_Payment__c:string;
  export default CnP_PaaS__Stage_Custom_Payment__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Stage_Declined__c" {
  const CnP_PaaS__Stage_Declined__c:string;
  export default CnP_PaaS__Stage_Declined__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Stage_Free__c" {
  const CnP_PaaS__Stage_Free__c:string;
  export default CnP_PaaS__Stage_Free__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Stage_Future__c" {
  const CnP_PaaS__Stage_Future__c:string;
  export default CnP_PaaS__Stage_Future__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Stage_Invoice__c" {
  const CnP_PaaS__Stage_Invoice__c:string;
  export default CnP_PaaS__Stage_Invoice__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Stage_PO__c" {
  const CnP_PaaS__Stage_PO__c:string;
  export default CnP_PaaS__Stage_PO__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__State_Format__c" {
  const CnP_PaaS__State_Format__c:string;
  export default CnP_PaaS__State_Format__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Tax_Opportunity__c" {
  const CnP_PaaS__Tax_Opportunity__c:boolean;
  export default CnP_PaaS__Tax_Opportunity__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Tax__c" {
  const CnP_PaaS__Tax__c:string;
  export default CnP_PaaS__Tax__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Terms_Conditions__c" {
  const CnP_PaaS__Terms_Conditions__c:string;
  export default CnP_PaaS__Terms_Conditions__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Thank_you__c" {
  const CnP_PaaS__Thank_you__c:string;
  export default CnP_PaaS__Thank_you__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Tracker__c" {
  const CnP_PaaS__Tracker__c:string;
  export default CnP_PaaS__Tracker__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__Update_Contacts__c" {
  const CnP_PaaS__Update_Contacts__c:boolean;
  export default CnP_PaaS__Update_Contacts__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__VT_Customize_XML__c" {
  const CnP_PaaS__VT_Customize_XML__c:string;
  export default CnP_PaaS__VT_Customize_XML__c;
}
declare module "@salesforce/schema/CnP_PaaS__XML_Setting__ChangeEvent.CnP_PaaS__insert_contacts__c" {
  const CnP_PaaS__insert_contacts__c:boolean;
  export default CnP_PaaS__insert_contacts__c;
}
