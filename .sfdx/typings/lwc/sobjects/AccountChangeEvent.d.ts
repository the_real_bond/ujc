declare module "@salesforce/schema/AccountChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/AccountChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/AccountChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/AccountChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/AccountChangeEvent.LastName" {
  const LastName:string;
  export default LastName;
}
declare module "@salesforce/schema/AccountChangeEvent.FirstName" {
  const FirstName:string;
  export default FirstName;
}
declare module "@salesforce/schema/AccountChangeEvent.Salutation" {
  const Salutation:string;
  export default Salutation;
}
declare module "@salesforce/schema/AccountChangeEvent.Type" {
  const Type:string;
  export default Type;
}
declare module "@salesforce/schema/AccountChangeEvent.RecordType" {
  const RecordType:any;
  export default RecordType;
}
declare module "@salesforce/schema/AccountChangeEvent.RecordTypeId" {
  const RecordTypeId:any;
  export default RecordTypeId;
}
declare module "@salesforce/schema/AccountChangeEvent.BillingStreet" {
  const BillingStreet:string;
  export default BillingStreet;
}
declare module "@salesforce/schema/AccountChangeEvent.BillingCity" {
  const BillingCity:string;
  export default BillingCity;
}
declare module "@salesforce/schema/AccountChangeEvent.BillingState" {
  const BillingState:string;
  export default BillingState;
}
declare module "@salesforce/schema/AccountChangeEvent.BillingPostalCode" {
  const BillingPostalCode:string;
  export default BillingPostalCode;
}
declare module "@salesforce/schema/AccountChangeEvent.BillingCountry" {
  const BillingCountry:string;
  export default BillingCountry;
}
declare module "@salesforce/schema/AccountChangeEvent.BillingLatitude" {
  const BillingLatitude:number;
  export default BillingLatitude;
}
declare module "@salesforce/schema/AccountChangeEvent.BillingLongitude" {
  const BillingLongitude:number;
  export default BillingLongitude;
}
declare module "@salesforce/schema/AccountChangeEvent.BillingGeocodeAccuracy" {
  const BillingGeocodeAccuracy:string;
  export default BillingGeocodeAccuracy;
}
declare module "@salesforce/schema/AccountChangeEvent.BillingAddress" {
  const BillingAddress:any;
  export default BillingAddress;
}
declare module "@salesforce/schema/AccountChangeEvent.ShippingStreet" {
  const ShippingStreet:string;
  export default ShippingStreet;
}
declare module "@salesforce/schema/AccountChangeEvent.ShippingCity" {
  const ShippingCity:string;
  export default ShippingCity;
}
declare module "@salesforce/schema/AccountChangeEvent.ShippingState" {
  const ShippingState:string;
  export default ShippingState;
}
declare module "@salesforce/schema/AccountChangeEvent.ShippingPostalCode" {
  const ShippingPostalCode:string;
  export default ShippingPostalCode;
}
declare module "@salesforce/schema/AccountChangeEvent.ShippingCountry" {
  const ShippingCountry:string;
  export default ShippingCountry;
}
declare module "@salesforce/schema/AccountChangeEvent.ShippingLatitude" {
  const ShippingLatitude:number;
  export default ShippingLatitude;
}
declare module "@salesforce/schema/AccountChangeEvent.ShippingLongitude" {
  const ShippingLongitude:number;
  export default ShippingLongitude;
}
declare module "@salesforce/schema/AccountChangeEvent.ShippingGeocodeAccuracy" {
  const ShippingGeocodeAccuracy:string;
  export default ShippingGeocodeAccuracy;
}
declare module "@salesforce/schema/AccountChangeEvent.ShippingAddress" {
  const ShippingAddress:any;
  export default ShippingAddress;
}
declare module "@salesforce/schema/AccountChangeEvent.Phone" {
  const Phone:string;
  export default Phone;
}
declare module "@salesforce/schema/AccountChangeEvent.Fax" {
  const Fax:string;
  export default Fax;
}
declare module "@salesforce/schema/AccountChangeEvent.AccountNumber" {
  const AccountNumber:string;
  export default AccountNumber;
}
declare module "@salesforce/schema/AccountChangeEvent.Website" {
  const Website:string;
  export default Website;
}
declare module "@salesforce/schema/AccountChangeEvent.Sic" {
  const Sic:string;
  export default Sic;
}
declare module "@salesforce/schema/AccountChangeEvent.Industry" {
  const Industry:string;
  export default Industry;
}
declare module "@salesforce/schema/AccountChangeEvent.AnnualRevenue" {
  const AnnualRevenue:number;
  export default AnnualRevenue;
}
declare module "@salesforce/schema/AccountChangeEvent.NumberOfEmployees" {
  const NumberOfEmployees:number;
  export default NumberOfEmployees;
}
declare module "@salesforce/schema/AccountChangeEvent.Ownership" {
  const Ownership:string;
  export default Ownership;
}
declare module "@salesforce/schema/AccountChangeEvent.TickerSymbol" {
  const TickerSymbol:string;
  export default TickerSymbol;
}
declare module "@salesforce/schema/AccountChangeEvent.Description" {
  const Description:string;
  export default Description;
}
declare module "@salesforce/schema/AccountChangeEvent.Rating" {
  const Rating:string;
  export default Rating;
}
declare module "@salesforce/schema/AccountChangeEvent.Site" {
  const Site:string;
  export default Site;
}
declare module "@salesforce/schema/AccountChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/AccountChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/AccountChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/AccountChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/AccountChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/AccountChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/AccountChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/AccountChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonContact" {
  const PersonContact:any;
  export default PersonContact;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonContactId" {
  const PersonContactId:any;
  export default PersonContactId;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonMailingStreet" {
  const PersonMailingStreet:string;
  export default PersonMailingStreet;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonMailingCity" {
  const PersonMailingCity:string;
  export default PersonMailingCity;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonMailingState" {
  const PersonMailingState:string;
  export default PersonMailingState;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonMailingPostalCode" {
  const PersonMailingPostalCode:string;
  export default PersonMailingPostalCode;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonMailingCountry" {
  const PersonMailingCountry:string;
  export default PersonMailingCountry;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonMailingLatitude" {
  const PersonMailingLatitude:number;
  export default PersonMailingLatitude;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonMailingLongitude" {
  const PersonMailingLongitude:number;
  export default PersonMailingLongitude;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonMailingGeocodeAccuracy" {
  const PersonMailingGeocodeAccuracy:string;
  export default PersonMailingGeocodeAccuracy;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonMailingAddress" {
  const PersonMailingAddress:any;
  export default PersonMailingAddress;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonOtherStreet" {
  const PersonOtherStreet:string;
  export default PersonOtherStreet;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonOtherCity" {
  const PersonOtherCity:string;
  export default PersonOtherCity;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonOtherState" {
  const PersonOtherState:string;
  export default PersonOtherState;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonOtherPostalCode" {
  const PersonOtherPostalCode:string;
  export default PersonOtherPostalCode;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonOtherCountry" {
  const PersonOtherCountry:string;
  export default PersonOtherCountry;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonOtherLatitude" {
  const PersonOtherLatitude:number;
  export default PersonOtherLatitude;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonOtherLongitude" {
  const PersonOtherLongitude:number;
  export default PersonOtherLongitude;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonOtherGeocodeAccuracy" {
  const PersonOtherGeocodeAccuracy:string;
  export default PersonOtherGeocodeAccuracy;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonOtherAddress" {
  const PersonOtherAddress:any;
  export default PersonOtherAddress;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonMobilePhone" {
  const PersonMobilePhone:string;
  export default PersonMobilePhone;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonHomePhone" {
  const PersonHomePhone:string;
  export default PersonHomePhone;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonOtherPhone" {
  const PersonOtherPhone:string;
  export default PersonOtherPhone;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonAssistantPhone" {
  const PersonAssistantPhone:string;
  export default PersonAssistantPhone;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonEmail" {
  const PersonEmail:string;
  export default PersonEmail;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonTitle" {
  const PersonTitle:string;
  export default PersonTitle;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonDepartment" {
  const PersonDepartment:string;
  export default PersonDepartment;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonAssistantName" {
  const PersonAssistantName:string;
  export default PersonAssistantName;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonLeadSource" {
  const PersonLeadSource:string;
  export default PersonLeadSource;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonBirthdate" {
  const PersonBirthdate:any;
  export default PersonBirthdate;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonHasOptedOutOfEmail" {
  const PersonHasOptedOutOfEmail:boolean;
  export default PersonHasOptedOutOfEmail;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonDoNotCall" {
  const PersonDoNotCall:boolean;
  export default PersonDoNotCall;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonLastCURequestDate" {
  const PersonLastCURequestDate:any;
  export default PersonLastCURequestDate;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonLastCUUpdateDate" {
  const PersonLastCUUpdateDate:any;
  export default PersonLastCUUpdateDate;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonEmailBouncedReason" {
  const PersonEmailBouncedReason:string;
  export default PersonEmailBouncedReason;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonEmailBouncedDate" {
  const PersonEmailBouncedDate:any;
  export default PersonEmailBouncedDate;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonIndividual" {
  const PersonIndividual:any;
  export default PersonIndividual;
}
declare module "@salesforce/schema/AccountChangeEvent.PersonIndividualId" {
  const PersonIndividualId:any;
  export default PersonIndividualId;
}
declare module "@salesforce/schema/AccountChangeEvent.Jigsaw" {
  const Jigsaw:string;
  export default Jigsaw;
}
declare module "@salesforce/schema/AccountChangeEvent.JigsawCompanyId" {
  const JigsawCompanyId:string;
  export default JigsawCompanyId;
}
declare module "@salesforce/schema/AccountChangeEvent.AccountSource" {
  const AccountSource:string;
  export default AccountSource;
}
declare module "@salesforce/schema/AccountChangeEvent.SicDesc" {
  const SicDesc:string;
  export default SicDesc;
}
declare module "@salesforce/schema/AccountChangeEvent.Year_of_Emigration__c" {
  const Year_of_Emigration__c:string;
  export default Year_of_Emigration__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Receive_Statement__c" {
  const Receive_Statement__c:string;
  export default Receive_Statement__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Tax_Certificate_Required__c" {
  const Tax_Certificate_Required__c:string;
  export default Tax_Certificate_Required__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Communal_Reference_Auto_Number__c" {
  const Communal_Reference_Auto_Number__c:string;
  export default Communal_Reference_Auto_Number__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Old_Register_Area_R__c" {
  const Old_Register_Area_R__c:string;
  export default Old_Register_Area_R__c;
}
declare module "@salesforce/schema/AccountChangeEvent.CanvassStatus__c" {
  const CanvassStatus__c:string;
  export default CanvassStatus__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Picture__c" {
  const Picture__c:string;
  export default Picture__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Communal_Reference_Number__c" {
  const Communal_Reference_Number__c:string;
  export default Communal_Reference_Number__c;
}
declare module "@salesforce/schema/AccountChangeEvent.LinkedIn__c" {
  const LinkedIn__c:string;
  export default LinkedIn__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Assistant_Email__c" {
  const Assistant_Email__c:string;
  export default Assistant_Email__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Residential_Fax__c" {
  const Residential_Fax__c:string;
  export default Residential_Fax__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Cape_Jewish_Seniors__c" {
  const Cape_Jewish_Seniors__c:string;
  export default Cape_Jewish_Seniors__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Last_JC_Pledge_Date__c" {
  const Last_JC_Pledge_Date__c:any;
  export default Last_JC_Pledge_Date__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Children__c" {
  const Children__c:string;
  export default Children__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Children_at_Herzlia__c" {
  const Children_at_Herzlia__c:boolean;
  export default Children_at_Herzlia__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Chronicle_Mailing__c" {
  const Chronicle_Mailing__c:string;
  export default Chronicle_Mailing__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Date_of_Death__c" {
  const Date_of_Death__c:any;
  export default Date_of_Death__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Emigrated_To__c" {
  const Emigrated_To__c:string;
  export default Emigrated_To__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Email_Alternative__c" {
  const Email_Alternative__c:string;
  export default Email_Alternative__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Email_Personal__c" {
  const Email_Personal__c:string;
  export default Email_Personal__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Email_Preference__c" {
  const Email_Preference__c:string;
  export default Email_Preference__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Gender__c" {
  const Gender__c:string;
  export default Gender__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Household__c" {
  const Household__c:any;
  export default Household__c;
}
declare module "@salesforce/schema/AccountChangeEvent.ID_No__c" {
  const ID_No__c:string;
  export default ID_No__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Initials__c" {
  const Initials__c:string;
  export default Initials__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Israeli__c" {
  const Israeli__c:string;
  export default Israeli__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Mail_Preference__c" {
  const Mail_Preference__c:string;
  export default Mail_Preference__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Occupation__c" {
  const Occupation__c:string;
  export default Occupation__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Personal_Interest__c" {
  const Personal_Interest__c:string;
  export default Personal_Interest__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Business_Link__c" {
  const Business_Link__c:any;
  export default Business_Link__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Preferred_Name__c" {
  const Preferred_Name__c:string;
  export default Preferred_Name__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Qualification__c" {
  const Qualification__c:string;
  export default Qualification__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Retired__c" {
  const Retired__c:string;
  export default Retired__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Spouse__c" {
  const Spouse__c:string;
  export default Spouse__c;
}
declare module "@salesforce/schema/AccountChangeEvent.DatabaseStatus__c" {
  const DatabaseStatus__c:string;
  export default DatabaseStatus__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Address_2_RS__c" {
  const Address_2_RS__c:string;
  export default Address_2_RS__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Individual_Type__c" {
  const Individual_Type__c:string;
  export default Individual_Type__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Voluntary_Worker__c" {
  const Voluntary_Worker__c:string;
  export default Voluntary_Worker__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Residential_Address_RS__c" {
  const Residential_Address_RS__c:string;
  export default Residential_Address_RS__c;
}
declare module "@salesforce/schema/AccountChangeEvent.City_RS__c" {
  const City_RS__c:string;
  export default City_RS__c;
}
declare module "@salesforce/schema/AccountChangeEvent.AKA__c" {
  const AKA__c:string;
  export default AKA__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Province_RS__c" {
  const Province_RS__c:string;
  export default Province_RS__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Postal_Code_RS__c" {
  const Postal_Code_RS__c:string;
  export default Postal_Code_RS__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Country_RS__c" {
  const Country_RS__c:string;
  export default Country_RS__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Residential_Postal_Address_RP__c" {
  const Residential_Postal_Address_RP__c:string;
  export default Residential_Postal_Address_RP__c;
}
declare module "@salesforce/schema/AccountChangeEvent.City_RP__c" {
  const City_RP__c:string;
  export default City_RP__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Suburb_RP__c" {
  const Suburb_RP__c:string;
  export default Suburb_RP__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Postal_Code_RP__c" {
  const Postal_Code_RP__c:string;
  export default Postal_Code_RP__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Country_RP__c" {
  const Country_RP__c:string;
  export default Country_RP__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Business_Address_BS__c" {
  const Business_Address_BS__c:string;
  export default Business_Address_BS__c;
}
declare module "@salesforce/schema/AccountChangeEvent.City_BS__c" {
  const City_BS__c:string;
  export default City_BS__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Suburb_BS__c" {
  const Suburb_BS__c:string;
  export default Suburb_BS__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Postal_Code_BS__c" {
  const Postal_Code_BS__c:string;
  export default Postal_Code_BS__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Country_BS__c" {
  const Country_BS__c:string;
  export default Country_BS__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Business_Postal_Address_BP__c" {
  const Business_Postal_Address_BP__c:string;
  export default Business_Postal_Address_BP__c;
}
declare module "@salesforce/schema/AccountChangeEvent.City_BP__c" {
  const City_BP__c:string;
  export default City_BP__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Suburb_BP__c" {
  const Suburb_BP__c:string;
  export default Suburb_BP__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Postal_Code_BP__c" {
  const Postal_Code_BP__c:string;
  export default Postal_Code_BP__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Country_BP__c" {
  const Country_BP__c:string;
  export default Country_BP__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Business_Name__c" {
  const Business_Name__c:string;
  export default Business_Name__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Province_RP__c" {
  const Province_RP__c:string;
  export default Province_RP__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Province_BS__c" {
  const Province_BS__c:string;
  export default Province_BS__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Province_BP__c" {
  const Province_BP__c:string;
  export default Province_BP__c;
}
declare module "@salesforce/schema/AccountChangeEvent.COVID_19_Total_Outstanding__c" {
  const COVID_19_Total_Outstanding__c:number;
  export default COVID_19_Total_Outstanding__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Email_Monthly_Statements__c" {
  const Email_Monthly_Statements__c:boolean;
  export default Email_Monthly_Statements__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Qualification_Other__c" {
  const Qualification_Other__c:string;
  export default Qualification_Other__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Occupation_Other__c" {
  const Occupation_Other__c:string;
  export default Occupation_Other__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Emigrated__c" {
  const Emigrated__c:boolean;
  export default Emigrated__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Date_Status_Updated__c" {
  const Date_Status_Updated__c:any;
  export default Date_Status_Updated__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Residential_Telephone__c" {
  const Residential_Telephone__c:string;
  export default Residential_Telephone__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Email_Business__c" {
  const Email_Business__c:string;
  export default Email_Business__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Cellphone__c" {
  const Cellphone__c:string;
  export default Cellphone__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Age__c" {
  const Age__c:number;
  export default Age__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Create_New_Household__c" {
  const Create_New_Household__c:string;
  export default Create_New_Household__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Date_of_Birth__c" {
  const Date_of_Birth__c:any;
  export default Date_of_Birth__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Household_Main_Member__c" {
  const Household_Main_Member__c:boolean;
  export default Household_Main_Member__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Pledge_History_Report__c" {
  const Pledge_History_Report__c:string;
  export default Pledge_History_Report__c;
}
declare module "@salesforce/schema/AccountChangeEvent.School__c" {
  const School__c:string;
  export default School__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Suburb_RS__c" {
  const Suburb_RS__c:string;
  export default Suburb_RS__c;
}
declare module "@salesforce/schema/AccountChangeEvent.View_Household_Members__c" {
  const View_Household_Members__c:string;
  export default View_Household_Members__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Email_Thank_you_letter__c" {
  const Email_Thank_you_letter__c:boolean;
  export default Email_Thank_you_letter__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Communal_Reference_Number_Old__c" {
  const Communal_Reference_Number_Old__c:string;
  export default Communal_Reference_Number_Old__c;
}
declare module "@salesforce/schema/AccountChangeEvent.CRN_Link__c" {
  const CRN_Link__c:string;
  export default CRN_Link__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Status_Comments__c" {
  const Status_Comments__c:string;
  export default Status_Comments__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Month_of_Emigration__c" {
  const Month_of_Emigration__c:string;
  export default Month_of_Emigration__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Previous_two_years_Pledge__c" {
  const Previous_two_years_Pledge__c:number;
  export default Previous_two_years_Pledge__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Special_Campaign_Opt_Out__c" {
  const Special_Campaign_Opt_Out__c:string;
  export default Special_Campaign_Opt_Out__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Last_Friends_Pledge_Amount__c" {
  const Last_Friends_Pledge_Amount__c:number;
  export default Last_Friends_Pledge_Amount__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Spouse_Name__c" {
  const Spouse_Name__c:any;
  export default Spouse_Name__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Spouse_First_Name__c" {
  const Spouse_First_Name__c:string;
  export default Spouse_First_Name__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Salutation_First_Names__c" {
  const Salutation_First_Names__c:string;
  export default Salutation_First_Names__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Salutation_Formal__c" {
  const Salutation_Formal__c:string;
  export default Salutation_Formal__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Salutation_Formal_with_Initial__c" {
  const Salutation_Formal_with_Initial__c:string;
  export default Salutation_Formal_with_Initial__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Last_JC_Pledge__c" {
  const Last_JC_Pledge__c:number;
  export default Last_JC_Pledge__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Category__c" {
  const Category__c:string;
  export default Category__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Mail_Merge_Address__c" {
  const Mail_Merge_Address__c:string;
  export default Mail_Merge_Address__c;
}
declare module "@salesforce/schema/AccountChangeEvent.TodayDate__c" {
  const TodayDate__c:string;
  export default TodayDate__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2020_JC_IUA__c" {
  const X2020_JC_IUA__c:number;
  export default X2020_JC_IUA__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Mail_Merge_Suburb__c" {
  const Mail_Merge_Suburb__c:string;
  export default Mail_Merge_Suburb__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Mail_Merge_City__c" {
  const Mail_Merge_City__c:string;
  export default Mail_Merge_City__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Mail_Merge_Province__c" {
  const Mail_Merge_Province__c:string;
  export default Mail_Merge_Province__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Mail_Merge_Postal_Code__c" {
  const Mail_Merge_Postal_Code__c:string;
  export default Mail_Merge_Postal_Code__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Mail_Merge_Country__c" {
  const Mail_Merge_Country__c:string;
  export default Mail_Merge_Country__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Mail_Merge_Email__c" {
  const Mail_Merge_Email__c:string;
  export default Mail_Merge_Email__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Foreign_Residential_Address__c" {
  const Foreign_Residential_Address__c:string;
  export default Foreign_Residential_Address__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Suburb_FR__c" {
  const Suburb_FR__c:string;
  export default Suburb_FR__c;
}
declare module "@salesforce/schema/AccountChangeEvent.City_FR__c" {
  const City_FR__c:string;
  export default City_FR__c;
}
declare module "@salesforce/schema/AccountChangeEvent.State_Province_FR__c" {
  const State_Province_FR__c:string;
  export default State_Province_FR__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Country_FR__c" {
  const Country_FR__c:string;
  export default Country_FR__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Postal_Code_FR__c" {
  const Postal_Code_FR__c:string;
  export default Postal_Code_FR__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Foreign_Business_Address__c" {
  const Foreign_Business_Address__c:string;
  export default Foreign_Business_Address__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Suburb_FB__c" {
  const Suburb_FB__c:string;
  export default Suburb_FB__c;
}
declare module "@salesforce/schema/AccountChangeEvent.City_FB__c" {
  const City_FB__c:string;
  export default City_FB__c;
}
declare module "@salesforce/schema/AccountChangeEvent.State_Province_FB__c" {
  const State_Province_FB__c:string;
  export default State_Province_FB__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Country_FB__c" {
  const Country_FB__c:string;
  export default Country_FB__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Postal_Code_FB__c" {
  const Postal_Code_FB__c:string;
  export default Postal_Code_FB__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Business_Name_FB__c" {
  const Business_Name_FB__c:string;
  export default Business_Name_FB__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Phone_Foreign_Residential__c" {
  const Phone_Foreign_Residential__c:string;
  export default Phone_Foreign_Residential__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Phone_Foreign_Business__c" {
  const Phone_Foreign_Business__c:string;
  export default Phone_Foreign_Business__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Phone_Foreign_Cell__c" {
  const Phone_Foreign_Cell__c:string;
  export default Phone_Foreign_Cell__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Spouse_Canvass_Status__c" {
  const Spouse_Canvass_Status__c:string;
  export default Spouse_Canvass_Status__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Total2011D3__c" {
  const Total2011D3__c:string;
  export default Total2011D3__c;
}
declare module "@salesforce/schema/AccountChangeEvent.University__c" {
  const University__c:string;
  export default University__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Trust_Found_Link__c" {
  const Trust_Found_Link__c:any;
  export default Trust_Found_Link__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Foreign_Res_Add_2__c" {
  const Foreign_Res_Add_2__c:string;
  export default Foreign_Res_Add_2__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Incomplete_Record__c" {
  const Incomplete_Record__c:boolean;
  export default Incomplete_Record__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Address_3_RS__c" {
  const Address_3_RS__c:string;
  export default Address_3_RS__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Herzlia_Alum__c" {
  const Herzlia_Alum__c:boolean;
  export default Herzlia_Alum__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Special_Attention__c" {
  const Special_Attention__c:string;
  export default Special_Attention__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Pledge_Amount_Category__c" {
  const Pledge_Amount_Category__c:string;
  export default Pledge_Amount_Category__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Account_Number__c" {
  const Account_Number__c:string;
  export default Account_Number__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2019_JC_IUA__c" {
  const X2019_JC_IUA__c:number;
  export default X2019_JC_IUA__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Past_Volunteer_Activities__c" {
  const Past_Volunteer_Activities__c:string;
  export default Past_Volunteer_Activities__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Total_Billable__c" {
  const Total_Billable__c:number;
  export default Total_Billable__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Next_Birthday__c" {
  const Next_Birthday__c:any;
  export default Next_Birthday__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Bank_Card_Expiry__c" {
  const Bank_Card_Expiry__c:string;
  export default Bank_Card_Expiry__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2014_Chron_Sub__c" {
  const X2014_Chron_Sub__c:number;
  export default X2014_Chron_Sub__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2014_Chron_Donation__c" {
  const X2014_Chron_Donation__c:number;
  export default X2014_Chron_Donation__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Counter__c" {
  const Counter__c:number;
  export default Counter__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Account__c" {
  const Account__c:string;
  export default Account__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Tax_Certificate_Sent__c" {
  const Tax_Certificate_Sent__c:string;
  export default Tax_Certificate_Sent__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Tax_Certificate_Name__c" {
  const Tax_Certificate_Name__c:string;
  export default Tax_Certificate_Name__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Linked_Individual__c" {
  const Linked_Individual__c:any;
  export default Linked_Individual__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Trust_Manager_First_Name__c" {
  const Trust_Manager_First_Name__c:string;
  export default Trust_Manager_First_Name__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Trust_Manager_Last_Name__c" {
  const Trust_Manager_Last_Name__c:string;
  export default Trust_Manager_Last_Name__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Trust_Manager_Telephone_Number__c" {
  const Trust_Manager_Telephone_Number__c:string;
  export default Trust_Manager_Telephone_Number__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Trust_Manager_Email__c" {
  const Trust_Manager_Email__c:string;
  export default Trust_Manager_Email__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Spouse_No__c" {
  const Spouse_No__c:string;
  export default Spouse_No__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2013_Chronicle_Sub__c" {
  const X2013_Chronicle_Sub__c:number;
  export default X2013_Chronicle_Sub__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2013_Chronicle_Donation__c" {
  const X2013_Chronicle_Donation__c:number;
  export default X2013_Chronicle_Donation__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Statement_Explanation__c" {
  const Statement_Explanation__c:string;
  export default Statement_Explanation__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2012_Chronicle_Sub__c" {
  const X2012_Chronicle_Sub__c:number;
  export default X2012_Chronicle_Sub__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2011_Chronicle_Sub__c" {
  const X2011_Chronicle_Sub__c:number;
  export default X2011_Chronicle_Sub__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Synagogue__c" {
  const Synagogue__c:string;
  export default Synagogue__c;
}
declare module "@salesforce/schema/AccountChangeEvent.DonorCategory__c" {
  const DonorCategory__c:string;
  export default DonorCategory__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Send_Birthday_Email_Check__c" {
  const Send_Birthday_Email_Check__c:boolean;
  export default Send_Birthday_Email_Check__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Verified__c" {
  const Verified__c:boolean;
  export default Verified__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2018_NCFR_Pledge_Total__c" {
  const X2018_NCFR_Pledge_Total__c:number;
  export default X2018_NCFR_Pledge_Total__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Telethon_Volunteer__c" {
  const Telethon_Volunteer__c:string;
  export default Telethon_Volunteer__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Telethon_More_Info__c" {
  const Telethon_More_Info__c:string;
  export default Telethon_More_Info__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Telethon_Requested_More__c" {
  const Telethon_Requested_More__c:boolean;
  export default Telethon_Requested_More__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Special_per__c" {
  const Special_per__c:string;
  export default Special_per__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Canvass_Info__c" {
  const Canvass_Info__c:string;
  export default Canvass_Info__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Total_Cancelled__c" {
  const Total_Cancelled__c:number;
  export default Total_Cancelled__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Notes2014__c" {
  const Notes2014__c:string;
  export default Notes2014__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Bank_Name__c" {
  const Bank_Name__c:string;
  export default Bank_Name__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Credit_Card_CVV__c" {
  const Credit_Card_CVV__c:string;
  export default Credit_Card_CVV__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Total_Pledge_History__c" {
  const Total_Pledge_History__c:number;
  export default Total_Pledge_History__c;
}
declare module "@salesforce/schema/AccountChangeEvent.BULK_SMS_Cell__c" {
  const BULK_SMS_Cell__c:string;
  export default BULK_SMS_Cell__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Year_of_Herzlia_Grad__c" {
  const Year_of_Herzlia_Grad__c:number;
  export default Year_of_Herzlia_Grad__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Online_Statement__c" {
  const Online_Statement__c:string;
  export default Online_Statement__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Bulk_SMS_Opt_Out__c" {
  const Bulk_SMS_Opt_Out__c:boolean;
  export default Bulk_SMS_Opt_Out__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Notes__c" {
  const Notes__c:string;
  export default Notes__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2015_IUA__c" {
  const X2015_IUA__c:number;
  export default X2015_IUA__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2015_UCF_Comm__c" {
  const X2015_UCF_Comm__c:number;
  export default X2015_UCF_Comm__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2015_UCF_Ed__c" {
  const X2015_UCF_Ed__c:number;
  export default X2015_UCF_Ed__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2015_Welfare__c" {
  const X2015_Welfare__c:number;
  export default X2015_Welfare__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2015_Refusal_Count__c" {
  const X2015_Refusal_Count__c:number;
  export default X2015_Refusal_Count__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2015_Cancelled__c" {
  const X2015_Cancelled__c:number;
  export default X2015_Cancelled__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2015_Pledge_Total__c" {
  const X2015_Pledge_Total__c:number;
  export default X2015_Pledge_Total__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2015_JC_Pledge_Total__c" {
  const X2015_JC_Pledge_Total__c:number;
  export default X2015_JC_Pledge_Total__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2015_Balance_Outstanding__c" {
  const X2015_Balance_Outstanding__c:number;
  export default X2015_Balance_Outstanding__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Salutation_First_Last_Names__c" {
  const Salutation_First_Last_Names__c:string;
  export default Salutation_First_Last_Names__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Mail_Merge_Workflow_Test__c" {
  const Mail_Merge_Workflow_Test__c:string;
  export default Mail_Merge_Workflow_Test__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2014_JC_Pledge_Total__c" {
  const X2014_JC_Pledge_Total__c:number;
  export default X2014_JC_Pledge_Total__c;
}
declare module "@salesforce/schema/AccountChangeEvent.NonJC_Pledge_History__c" {
  const NonJC_Pledge_History__c:number;
  export default NonJC_Pledge_History__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Salutation_Initials_LastName__c" {
  const Salutation_Initials_LastName__c:string;
  export default Salutation_Initials_LastName__c;
}
declare module "@salesforce/schema/AccountChangeEvent.FirstName__c" {
  const FirstName__c:string;
  export default FirstName__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2019_JC_UCF_Comm__c" {
  const X2019_JC_UCF_Comm__c:number;
  export default X2019_JC_UCF_Comm__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2019_JC_UCF_Ed__c" {
  const X2019_JC_UCF_Ed__c:number;
  export default X2019_JC_UCF_Ed__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2019_JC_UCF_Total__c" {
  const X2019_JC_UCF_Total__c:number;
  export default X2019_JC_UCF_Total__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2019_JC_Welfare__c" {
  const X2019_JC_Welfare__c:number;
  export default X2019_JC_Welfare__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2019_JC_Pledge_Total__c" {
  const X2019_JC_Pledge_Total__c:number;
  export default X2019_JC_Pledge_Total__c;
}
declare module "@salesforce/schema/AccountChangeEvent.District__c" {
  const District__c:string;
  export default District__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Salutation_Thank_You__c" {
  const Salutation_Thank_You__c:string;
  export default Salutation_Thank_You__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Lion_of_Judah__c" {
  const Lion_of_Judah__c:string;
  export default Lion_of_Judah__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Trust_Type__c" {
  const Trust_Type__c:string;
  export default Trust_Type__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Last_Friends_Pledge_Year__c" {
  const Last_Friends_Pledge_Year__c:string;
  export default Last_Friends_Pledge_Year__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2019_NCF_Pledge_Total__c" {
  const X2019_NCF_Pledge_Total__c:number;
  export default X2019_NCF_Pledge_Total__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Last_JC_Pledge_Amount__c" {
  const Last_JC_Pledge_Amount__c:number;
  export default Last_JC_Pledge_Amount__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2019_IUA__c" {
  const X2019_IUA__c:number;
  export default X2019_IUA__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2019_UCF_Comm__c" {
  const X2019_UCF_Comm__c:number;
  export default X2019_UCF_Comm__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Last_JC_Pledge_Year__c" {
  const Last_JC_Pledge_Year__c:string;
  export default Last_JC_Pledge_Year__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2019_UCF_Ed__c" {
  const X2019_UCF_Ed__c:number;
  export default X2019_UCF_Ed__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Highlands_House_Resident__c" {
  const Highlands_House_Resident__c:boolean;
  export default Highlands_House_Resident__c;
}
declare module "@salesforce/schema/AccountChangeEvent.New_Donor__c" {
  const New_Donor__c:boolean;
  export default New_Donor__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Number_of_JC_Pledges__c" {
  const Number_of_JC_Pledges__c:number;
  export default Number_of_JC_Pledges__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Fountain_Donor__c" {
  const Fountain_Donor__c:boolean;
  export default Fountain_Donor__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2020_JC_Welfare__c" {
  const X2020_JC_Welfare__c:number;
  export default X2020_JC_Welfare__c;
}
declare module "@salesforce/schema/AccountChangeEvent.COVID_19_Pledge__c" {
  const COVID_19_Pledge__c:number;
  export default COVID_19_Pledge__c;
}
declare module "@salesforce/schema/AccountChangeEvent.BuTruSp_COVID_19_Pledge__c" {
  const BuTruSp_COVID_19_Pledge__c:number;
  export default BuTruSp_COVID_19_Pledge__c;
}
declare module "@salesforce/schema/AccountChangeEvent.COVID_19_Tax_Cert__c" {
  const COVID_19_Tax_Cert__c:boolean;
  export default COVID_19_Tax_Cert__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Last_Friends_Pledge__c" {
  const Last_Friends_Pledge__c:number;
  export default Last_Friends_Pledge__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Debut_Pledge_Date__c" {
  const Debut_Pledge_Date__c:any;
  export default Debut_Pledge_Date__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2020_Refusal_Count__c" {
  const X2020_Refusal_Count__c:number;
  export default X2020_Refusal_Count__c;
}
declare module "@salesforce/schema/AccountChangeEvent.FOUJC__c" {
  const FOUJC__c:string;
  export default FOUJC__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2019_UCF_Total__c" {
  const X2019_UCF_Total__c:number;
  export default X2019_UCF_Total__c;
}
declare module "@salesforce/schema/AccountChangeEvent.OP_Total_Outstanding_2016__c" {
  const OP_Total_Outstanding_2016__c:number;
  export default OP_Total_Outstanding_2016__c;
}
declare module "@salesforce/schema/AccountChangeEvent.OP_Total_Outstanding_2017__c" {
  const OP_Total_Outstanding_2017__c:number;
  export default OP_Total_Outstanding_2017__c;
}
declare module "@salesforce/schema/AccountChangeEvent.OP_Total_Outstanding_2015__c" {
  const OP_Total_Outstanding_2015__c:number;
  export default OP_Total_Outstanding_2015__c;
}
declare module "@salesforce/schema/AccountChangeEvent.OP_Pledge_Total_2016__c" {
  const OP_Pledge_Total_2016__c:number;
  export default OP_Pledge_Total_2016__c;
}
declare module "@salesforce/schema/AccountChangeEvent.OP_Pledge_Total_2017__c" {
  const OP_Pledge_Total_2017__c:number;
  export default OP_Pledge_Total_2017__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2020_JC_UCF_Ed__c" {
  const X2020_JC_UCF_Ed__c:number;
  export default X2020_JC_UCF_Ed__c;
}
declare module "@salesforce/schema/AccountChangeEvent.OP_Pledge_Total_2015__c" {
  const OP_Pledge_Total_2015__c:number;
  export default OP_Pledge_Total_2015__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2016_IUA__c" {
  const X2016_IUA__c:number;
  export default X2016_IUA__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2016_UCF_Comm__c" {
  const X2016_UCF_Comm__c:number;
  export default X2016_UCF_Comm__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2016_UCF_Ed__c" {
  const X2016_UCF_Ed__c:number;
  export default X2016_UCF_Ed__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2016_Welfare__c" {
  const X2016_Welfare__c:number;
  export default X2016_Welfare__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2016_Refusal_Count__c" {
  const X2016_Refusal_Count__c:number;
  export default X2016_Refusal_Count__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2016_Cancelled__c" {
  const X2016_Cancelled__c:number;
  export default X2016_Cancelled__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2016_Pledge_Total__c" {
  const X2016_Pledge_Total__c:number;
  export default X2016_Pledge_Total__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2018_BuTruSp_Total__c" {
  const X2018_BuTruSp_Total__c:number;
  export default X2018_BuTruSp_Total__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2016_Balance_Outstanding__c" {
  const X2016_Balance_Outstanding__c:number;
  export default X2016_Balance_Outstanding__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2016_JC_Pledge_Total__c" {
  const X2016_JC_Pledge_Total__c:number;
  export default X2016_JC_Pledge_Total__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Debut_Pledge_Age__c" {
  const Debut_Pledge_Age__c:number;
  export default Debut_Pledge_Age__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2020_JC_Giving__c" {
  const X2020_JC_Giving__c:number;
  export default X2020_JC_Giving__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2020_BuTrusp_IUA__c" {
  const X2020_BuTrusp_IUA__c:number;
  export default X2020_BuTrusp_IUA__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2019_Welfare__c" {
  const X2019_Welfare__c:number;
  export default X2019_Welfare__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2019_BuTruSp_IUA__c" {
  const X2019_BuTruSp_IUA__c:number;
  export default X2019_BuTruSp_IUA__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2019_Pledge_Total__c" {
  const X2019_Pledge_Total__c:number;
  export default X2019_Pledge_Total__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2018_BuTruSp_IUA__c" {
  const X2018_BuTruSp_IUA__c:number;
  export default X2018_BuTruSp_IUA__c;
}
declare module "@salesforce/schema/AccountChangeEvent.JC2015_IUA__c" {
  const JC2015_IUA__c:number;
  export default JC2015_IUA__c;
}
declare module "@salesforce/schema/AccountChangeEvent.JC2015_UCFCom__c" {
  const JC2015_UCFCom__c:number;
  export default JC2015_UCFCom__c;
}
declare module "@salesforce/schema/AccountChangeEvent.JC2015_UCFEd__c" {
  const JC2015_UCFEd__c:number;
  export default JC2015_UCFEd__c;
}
declare module "@salesforce/schema/AccountChangeEvent.JC2015_Welf__c" {
  const JC2015_Welf__c:number;
  export default JC2015_Welf__c;
}
declare module "@salesforce/schema/AccountChangeEvent.JC2016_IUA__c" {
  const JC2016_IUA__c:number;
  export default JC2016_IUA__c;
}
declare module "@salesforce/schema/AccountChangeEvent.JC2016_UCFCom__c" {
  const JC2016_UCFCom__c:number;
  export default JC2016_UCFCom__c;
}
declare module "@salesforce/schema/AccountChangeEvent.JC2016_UCFEd__c" {
  const JC2016_UCFEd__c:number;
  export default JC2016_UCFEd__c;
}
declare module "@salesforce/schema/AccountChangeEvent.JC2016_Welf__c" {
  const JC2016_Welf__c:number;
  export default JC2016_Welf__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2019_Balance_Outstanding__c" {
  const X2019_Balance_Outstanding__c:number;
  export default X2019_Balance_Outstanding__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2019_Cancelled__c" {
  const X2019_Cancelled__c:number;
  export default X2019_Cancelled__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2019_JC_Giving__c" {
  const X2019_JC_Giving__c:number;
  export default X2019_JC_Giving__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Age_Grouping__c" {
  const Age_Grouping__c:string;
  export default Age_Grouping__c;
}
declare module "@salesforce/schema/AccountChangeEvent.JC_UCF_Total_2016__c" {
  const JC_UCF_Total_2016__c:number;
  export default JC_UCF_Total_2016__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2016_Chronicle_Sub__c" {
  const X2016_Chronicle_Sub__c:number;
  export default X2016_Chronicle_Sub__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2017_Chronicle_Sub__c" {
  const X2017_Chronicle_Sub__c:number;
  export default X2017_Chronicle_Sub__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2018_Chronicle_Sub__c" {
  const X2018_Chronicle_Sub__c:number;
  export default X2018_Chronicle_Sub__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2019_Chronicle_Sub__c" {
  const X2019_Chronicle_Sub__c:number;
  export default X2019_Chronicle_Sub__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2020_Chronicle_Sub__c" {
  const X2020_Chronicle_Sub__c:number;
  export default X2020_Chronicle_Sub__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2015_Chronicle_Sub__c" {
  const X2015_Chronicle_Sub__c:number;
  export default X2015_Chronicle_Sub__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2015_Chron_Donation__c" {
  const X2015_Chron_Donation__c:number;
  export default X2015_Chron_Donation__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2016_Chron_Donation__c" {
  const X2016_Chron_Donation__c:number;
  export default X2016_Chron_Donation__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2017_Chron_Donation__c" {
  const X2017_Chron_Donation__c:number;
  export default X2017_Chron_Donation__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2018_Chron_Donation__c" {
  const X2018_Chron_Donation__c:number;
  export default X2018_Chron_Donation__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Email_Receipt_Thank_you_letter__c" {
  const Email_Receipt_Thank_you_letter__c:boolean;
  export default Email_Receipt_Thank_you_letter__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2017_Welfare__c" {
  const X2017_Welfare__c:number;
  export default X2017_Welfare__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2nd_Most_recent_Modified_by__c" {
  const X2nd_Most_recent_Modified_by__c:string;
  export default X2nd_Most_recent_Modified_by__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X3rd_Most_recent_Modified_by__c" {
  const X3rd_Most_recent_Modified_by__c:string;
  export default X3rd_Most_recent_Modified_by__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X4th_Most_recent_Modified_by__c" {
  const X4th_Most_recent_Modified_by__c:string;
  export default X4th_Most_recent_Modified_by__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X5th_Most_recent_Modified_by__c" {
  const X5th_Most_recent_Modified_by__c:string;
  export default X5th_Most_recent_Modified_by__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Marriage_Status__c" {
  const Marriage_Status__c:string;
  export default Marriage_Status__c;
}
declare module "@salesforce/schema/AccountChangeEvent.JC2017_Welf__c" {
  const JC2017_Welf__c:number;
  export default JC2017_Welf__c;
}
declare module "@salesforce/schema/AccountChangeEvent.JC2017_UCFEd__c" {
  const JC2017_UCFEd__c:number;
  export default JC2017_UCFEd__c;
}
declare module "@salesforce/schema/AccountChangeEvent.JC2017_UCFCom__c" {
  const JC2017_UCFCom__c:number;
  export default JC2017_UCFCom__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2017_UCF_Ed__c" {
  const X2017_UCF_Ed__c:number;
  export default X2017_UCF_Ed__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2017_UCF_Comm__c" {
  const X2017_UCF_Comm__c:number;
  export default X2017_UCF_Comm__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2017_Refusal_Count__c" {
  const X2017_Refusal_Count__c:number;
  export default X2017_Refusal_Count__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2019_File_per_Campaign__c" {
  const X2019_File_per_Campaign__c:number;
  export default X2019_File_per_Campaign__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2017_Pledge_Total__c" {
  const X2017_Pledge_Total__c:number;
  export default X2017_Pledge_Total__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2018_JC_Giving__c" {
  const X2018_JC_Giving__c:number;
  export default X2018_JC_Giving__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2017_JC_Pledge_Total__c" {
  const X2017_JC_Pledge_Total__c:number;
  export default X2017_JC_Pledge_Total__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2017_IUA__c" {
  const X2017_IUA__c:number;
  export default X2017_IUA__c;
}
declare module "@salesforce/schema/AccountChangeEvent.JC2017_IUA__c" {
  const JC2017_IUA__c:number;
  export default JC2017_IUA__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2017_JC_Giving__c" {
  const X2017_JC_Giving__c:number;
  export default X2017_JC_Giving__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2019_Refusal_Count__c" {
  const X2019_Refusal_Count__c:number;
  export default X2019_Refusal_Count__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Top_Donor__c" {
  const Top_Donor__c:boolean;
  export default Top_Donor__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2017_Cancelled__c" {
  const X2017_Cancelled__c:number;
  export default X2017_Cancelled__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2017_Balance_Outstanding__c" {
  const X2017_Balance_Outstanding__c:number;
  export default X2017_Balance_Outstanding__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2019_JC_OP__c" {
  const X2019_JC_OP__c:number;
  export default X2019_JC_OP__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Age_at_Last_JC_Pledge__c" {
  const Age_at_Last_JC_Pledge__c:number;
  export default Age_at_Last_JC_Pledge__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Total_UCF_2017__c" {
  const Total_UCF_2017__c:number;
  export default Total_UCF_2017__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2017_Percentage__c" {
  const X2017_Percentage__c:number;
  export default X2017_Percentage__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Spouse_Thank_you__c" {
  const Spouse_Thank_you__c:boolean;
  export default Spouse_Thank_you__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Husband_Email__c" {
  const Husband_Email__c:boolean;
  export default Husband_Email__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Total_Giving__c" {
  const Total_Giving__c:number;
  export default Total_Giving__c;
}
declare module "@salesforce/schema/AccountChangeEvent.JC_UCF_Total_2017__c" {
  const JC_UCF_Total_2017__c:number;
  export default JC_UCF_Total_2017__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Not_Jewish__c" {
  const Not_Jewish__c:boolean;
  export default Not_Jewish__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2017_NCFR_Pledge_Total__c" {
  const X2017_NCFR_Pledge_Total__c:number;
  export default X2017_NCFR_Pledge_Total__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Maiden_Name__c" {
  const Maiden_Name__c:string;
  export default Maiden_Name__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2018_Balance_Outstanding__c" {
  const X2018_Balance_Outstanding__c:number;
  export default X2018_Balance_Outstanding__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2018_Cancelled__c" {
  const X2018_Cancelled__c:number;
  export default X2018_Cancelled__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2018_UCF_Comm__c" {
  const X2018_UCF_Comm__c:number;
  export default X2018_UCF_Comm__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2018_UCF_Ed__c" {
  const X2018_UCF_Ed__c:number;
  export default X2018_UCF_Ed__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Last_NCF_Pledge_Date__c" {
  const Last_NCF_Pledge_Date__c:any;
  export default Last_NCF_Pledge_Date__c;
}
declare module "@salesforce/schema/AccountChangeEvent.OP_Total_Outstanding_2019__c" {
  const OP_Total_Outstanding_2019__c:number;
  export default OP_Total_Outstanding_2019__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2018_JC_IUA__c" {
  const X2018_JC_IUA__c:number;
  export default X2018_JC_IUA__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2018_IUA__c" {
  const X2018_IUA__c:number;
  export default X2018_IUA__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2018_JC_Pledge_Total__c" {
  const X2018_JC_Pledge_Total__c:number;
  export default X2018_JC_Pledge_Total__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2020_JC_Pledge_Total__c" {
  const X2020_JC_Pledge_Total__c:number;
  export default X2020_JC_Pledge_Total__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2018_Pledge_Total__c" {
  const X2018_Pledge_Total__c:number;
  export default X2018_Pledge_Total__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2018_Refusal_Count__c" {
  const X2018_Refusal_Count__c:number;
  export default X2018_Refusal_Count__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2018_JC_UCF_Comm__c" {
  const X2018_JC_UCF_Comm__c:number;
  export default X2018_JC_UCF_Comm__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2018_JC_UCF_Ed__c" {
  const X2018_JC_UCF_Ed__c:number;
  export default X2018_JC_UCF_Ed__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2018_JC_Welfare__c" {
  const X2018_JC_Welfare__c:number;
  export default X2018_JC_Welfare__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2018_Welfare__c" {
  const X2018_Welfare__c:number;
  export default X2018_Welfare__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2017_BuTruSp_Total__c" {
  const X2017_BuTruSp_Total__c:number;
  export default X2017_BuTruSp_Total__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2018_Percentage_Increase__c" {
  const X2018_Percentage_Increase__c:number;
  export default X2018_Percentage_Increase__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2016_Percentage__c" {
  const X2016_Percentage__c:number;
  export default X2016_Percentage__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2018_UCF_Total__c" {
  const X2018_UCF_Total__c:number;
  export default X2018_UCF_Total__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2018_JC_UCF_Total__c" {
  const X2018_JC_UCF_Total__c:number;
  export default X2018_JC_UCF_Total__c;
}
declare module "@salesforce/schema/AccountChangeEvent.OP_Pledge_Total_2018__c" {
  const OP_Pledge_Total_2018__c:number;
  export default OP_Pledge_Total_2018__c;
}
declare module "@salesforce/schema/AccountChangeEvent.OP_Total_Outstanding_2018__c" {
  const OP_Total_Outstanding_2018__c:number;
  export default OP_Total_Outstanding_2018__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Chronicle_Notes__c" {
  const Chronicle_Notes__c:string;
  export default Chronicle_Notes__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2019_NCFR_Pledge_Total__c" {
  const X2019_NCFR_Pledge_Total__c:number;
  export default X2019_NCFR_Pledge_Total__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2019_BuTruSp_Total__c" {
  const X2019_BuTruSp_Total__c:number;
  export default X2019_BuTruSp_Total__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2016_BuTruSp_Total__c" {
  const X2016_BuTruSp_Total__c:number;
  export default X2016_BuTruSp_Total__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2017_NCF_Pledge_Total__c" {
  const X2017_NCF_Pledge_Total__c:number;
  export default X2017_NCF_Pledge_Total__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2018_NCF_Pledge_Total__c" {
  const X2018_NCF_Pledge_Total__c:number;
  export default X2018_NCF_Pledge_Total__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2016_NCFR_Pledge_Total__c" {
  const X2016_NCFR_Pledge_Total__c:number;
  export default X2016_NCFR_Pledge_Total__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Last_Life_Cycle_Event__c" {
  const Last_Life_Cycle_Event__c:any;
  export default Last_Life_Cycle_Event__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2014_Refusal_Count__c" {
  const X2014_Refusal_Count__c:number;
  export default X2014_Refusal_Count__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2013_Refusal_Count__c" {
  const X2013_Refusal_Count__c:number;
  export default X2013_Refusal_Count__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2012_Refusal_Count__c" {
  const X2012_Refusal_Count__c:number;
  export default X2012_Refusal_Count__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Total_Refusals__c" {
  const Total_Refusals__c:number;
  export default Total_Refusals__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2013_File_per_Campaign__c" {
  const X2013_File_per_Campaign__c:number;
  export default X2013_File_per_Campaign__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2014_File_per_Campaign__c" {
  const X2014_File_per_Campaign__c:number;
  export default X2014_File_per_Campaign__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2015_File_per_Campaign__c" {
  const X2015_File_per_Campaign__c:number;
  export default X2015_File_per_Campaign__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2016_File_per_Campaign__c" {
  const X2016_File_per_Campaign__c:number;
  export default X2016_File_per_Campaign__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2017_File_per_Campaign__c" {
  const X2017_File_per_Campaign__c:number;
  export default X2017_File_per_Campaign__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2018_File_per_Campaign__c" {
  const X2018_File_per_Campaign__c:number;
  export default X2018_File_per_Campaign__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2015_NCFR_Pledge_Total__c" {
  const X2015_NCFR_Pledge_Total__c:number;
  export default X2015_NCFR_Pledge_Total__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2019_Percentage__c" {
  const X2019_Percentage__c:number;
  export default X2019_Percentage__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2020_NCF_Pledge_Total__c" {
  const X2020_NCF_Pledge_Total__c:number;
  export default X2020_NCF_Pledge_Total__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2020_NCFR_Pledge_Total__c" {
  const X2020_NCFR_Pledge_Total__c:number;
  export default X2020_NCFR_Pledge_Total__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2020_BuTruSp_Total__c" {
  const X2020_BuTruSp_Total__c:number;
  export default X2020_BuTruSp_Total__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2020_Percentage__c" {
  const X2020_Percentage__c:number;
  export default X2020_Percentage__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2020_JC_OP_Total__c" {
  const X2020_JC_OP_Total__c:number;
  export default X2020_JC_OP_Total__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Last_NCFR_Pledge_Date__c" {
  const Last_NCFR_Pledge_Date__c:any;
  export default Last_NCFR_Pledge_Date__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2020_JC_UCF_Comm__c" {
  const X2020_JC_UCF_Comm__c:number;
  export default X2020_JC_UCF_Comm__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Don_Cat__c" {
  const Don_Cat__c:string;
  export default Don_Cat__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2020_File_per_Campaign__c" {
  const X2020_File_per_Campaign__c:number;
  export default X2020_File_per_Campaign__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2020_JC_OP_Total_Outstanding__c" {
  const X2020_JC_OP_Total_Outstanding__c:number;
  export default X2020_JC_OP_Total_Outstanding__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2020_Pledge_Total__c" {
  const X2020_Pledge_Total__c:number;
  export default X2020_Pledge_Total__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2017_NCFR_Giving__c" {
  const X2017_NCFR_Giving__c:number;
  export default X2017_NCFR_Giving__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2018_NCFR_Giving__c" {
  const X2018_NCFR_Giving__c:number;
  export default X2018_NCFR_Giving__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2019_NCFR_Giving__c" {
  const X2019_NCFR_Giving__c:number;
  export default X2019_NCFR_Giving__c;
}
declare module "@salesforce/schema/AccountChangeEvent.X2020_NCFR_Giving__c" {
  const X2020_NCFR_Giving__c:number;
  export default X2020_NCFR_Giving__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Last_JC_Canvass_Method__c" {
  const Last_JC_Canvass_Method__c:string;
  export default Last_JC_Canvass_Method__c;
}
declare module "@salesforce/schema/AccountChangeEvent.Last_JC_Canvasser_s__c" {
  const Last_JC_Canvasser_s__c:string;
  export default Last_JC_Canvasser_s__c;
}
declare module "@salesforce/schema/AccountChangeEvent.ckt__Opt_Out__pc" {
  const ckt__Opt_Out__pc:boolean;
  export default ckt__Opt_Out__pc;
}
declare module "@salesforce/schema/AccountChangeEvent.tecnics__Next_Birth_Day__pc" {
  const tecnics__Next_Birth_Day__pc:any;
  export default tecnics__Next_Birth_Day__pc;
}
declare module "@salesforce/schema/AccountChangeEvent.MC4SF__MC_Subscriber__pc" {
  const MC4SF__MC_Subscriber__pc:any;
  export default MC4SF__MC_Subscriber__pc;
}
declare module "@salesforce/schema/AccountChangeEvent.CnP_PaaS__Alias_Contact_Data__pc" {
  const CnP_PaaS__Alias_Contact_Data__pc:string;
  export default CnP_PaaS__Alias_Contact_Data__pc;
}
declare module "@salesforce/schema/AccountChangeEvent.CnP_PaaS__CnP_Connect_Alias_Index__pc" {
  const CnP_PaaS__CnP_Connect_Alias_Index__pc:string;
  export default CnP_PaaS__CnP_Connect_Alias_Index__pc;
}
declare module "@salesforce/schema/AccountChangeEvent.CnP_PaaS__CnP_Global_Rank__pc" {
  const CnP_PaaS__CnP_Global_Rank__pc:number;
  export default CnP_PaaS__CnP_Global_Rank__pc;
}
declare module "@salesforce/schema/AccountChangeEvent.CnP_PaaS__CnP_Total_Intrinsic__pc" {
  const CnP_PaaS__CnP_Total_Intrinsic__pc:number;
  export default CnP_PaaS__CnP_Total_Intrinsic__pc;
}
declare module "@salesforce/schema/AccountChangeEvent.CnP_PaaS__CnP_Total_extrinsic__pc" {
  const CnP_PaaS__CnP_Total_extrinsic__pc:number;
  export default CnP_PaaS__CnP_Total_extrinsic__pc;
}
declare module "@salesforce/schema/AccountChangeEvent.CnP_PaaS__Connect_Link__pc" {
  const CnP_PaaS__Connect_Link__pc:string;
  export default CnP_PaaS__Connect_Link__pc;
}
