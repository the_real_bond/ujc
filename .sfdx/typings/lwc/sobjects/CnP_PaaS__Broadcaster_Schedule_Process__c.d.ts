declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Schedule_Process__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Schedule_Process__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Schedule_Process__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Schedule_Process__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Schedule_Process__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Schedule_Process__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Schedule_Process__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Schedule_Process__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Schedule_Process__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Schedule_Process__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Schedule_Process__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Schedule_Process__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Schedule_Process__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Schedule_Process__c.CnP_PaaS__Campaign_id__c" {
  const CnP_PaaS__Campaign_id__c:string;
  export default CnP_PaaS__Campaign_id__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Schedule_Process__c.CnP_PaaS__End_Date__c" {
  const CnP_PaaS__End_Date__c:any;
  export default CnP_PaaS__End_Date__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Schedule_Process__c.CnP_PaaS__End_Occurrences__c" {
  const CnP_PaaS__End_Occurrences__c:number;
  export default CnP_PaaS__End_Occurrences__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Schedule_Process__c.CnP_PaaS__End_on__c" {
  const CnP_PaaS__End_on__c:string;
  export default CnP_PaaS__End_on__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Schedule_Process__c.CnP_PaaS__Recurring_Broadcast__c" {
  const CnP_PaaS__Recurring_Broadcast__c:boolean;
  export default CnP_PaaS__Recurring_Broadcast__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Schedule_Process__c.CnP_PaaS__Repeat_Every__c" {
  const CnP_PaaS__Repeat_Every__c:string;
  export default CnP_PaaS__Repeat_Every__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Schedule_Process__c.CnP_PaaS__Repeat_by__c" {
  const CnP_PaaS__Repeat_by__c:string;
  export default CnP_PaaS__Repeat_by__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Schedule_Process__c.CnP_PaaS__Repeat_on__c" {
  const CnP_PaaS__Repeat_on__c:string;
  export default CnP_PaaS__Repeat_on__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Schedule_Process__c.CnP_PaaS__Repeats__c" {
  const CnP_PaaS__Repeats__c:string;
  export default CnP_PaaS__Repeats__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Schedule_Process__c.CnP_PaaS__Specific_date__c" {
  const CnP_PaaS__Specific_date__c:any;
  export default CnP_PaaS__Specific_date__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Schedule_Process__c.CnP_PaaS__Starts_on__c" {
  const CnP_PaaS__Starts_on__c:any;
  export default CnP_PaaS__Starts_on__c;
}
