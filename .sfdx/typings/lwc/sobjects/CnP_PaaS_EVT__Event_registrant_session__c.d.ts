declare module "@salesforce/schema/CnP_PaaS_EVT__Event_registrant_session__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_registrant_session__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_registrant_session__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_registrant_session__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_registrant_session__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_registrant_session__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_registrant_session__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_registrant_session__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_registrant_session__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_registrant_session__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_registrant_session__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_registrant_session__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_registrant_session__c.CnP_PaaS_EVT__Account_Type__c" {
  const CnP_PaaS_EVT__Account_Type__c:string;
  export default CnP_PaaS_EVT__Account_Type__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_registrant_session__c.CnP_PaaS_EVT__Additional_Donation__c" {
  const CnP_PaaS_EVT__Additional_Donation__c:number;
  export default CnP_PaaS_EVT__Additional_Donation__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_registrant_session__c.CnP_PaaS_EVT__Card_Type__c" {
  const CnP_PaaS_EVT__Card_Type__c:string;
  export default CnP_PaaS_EVT__Card_Type__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_registrant_session__c.CnP_PaaS_EVT__ContactId__r" {
  const CnP_PaaS_EVT__ContactId__r:any;
  export default CnP_PaaS_EVT__ContactId__r;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_registrant_session__c.CnP_PaaS_EVT__ContactId__c" {
  const CnP_PaaS_EVT__ContactId__c:any;
  export default CnP_PaaS_EVT__ContactId__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_registrant_session__c.CnP_PaaS_EVT__Contact_Data__c" {
  const CnP_PaaS_EVT__Contact_Data__c:string;
  export default CnP_PaaS_EVT__Contact_Data__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_registrant_session__c.CnP_PaaS_EVT__Coupon_code__c" {
  const CnP_PaaS_EVT__Coupon_code__c:string;
  export default CnP_PaaS_EVT__Coupon_code__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_registrant_session__c.CnP_PaaS_EVT__EventId__r" {
  const CnP_PaaS_EVT__EventId__r:any;
  export default CnP_PaaS_EVT__EventId__r;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_registrant_session__c.CnP_PaaS_EVT__EventId__c" {
  const CnP_PaaS_EVT__EventId__c:any;
  export default CnP_PaaS_EVT__EventId__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_registrant_session__c.CnP_PaaS_EVT__Increment_Number__c" {
  const CnP_PaaS_EVT__Increment_Number__c:number;
  export default CnP_PaaS_EVT__Increment_Number__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_registrant_session__c.CnP_PaaS_EVT__Payment_Type__c" {
  const CnP_PaaS_EVT__Payment_Type__c:string;
  export default CnP_PaaS_EVT__Payment_Type__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_registrant_session__c.CnP_PaaS_EVT__Registration_type__c" {
  const CnP_PaaS_EVT__Registration_type__c:string;
  export default CnP_PaaS_EVT__Registration_type__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_registrant_session__c.CnP_PaaS_EVT__Response_Detail__c" {
  const CnP_PaaS_EVT__Response_Detail__c:string;
  export default CnP_PaaS_EVT__Response_Detail__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_registrant_session__c.CnP_PaaS_EVT__Status__c" {
  const CnP_PaaS_EVT__Status__c:string;
  export default CnP_PaaS_EVT__Status__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_registrant_session__c.CnP_PaaS_EVT__Ticket_Number__c" {
  const CnP_PaaS_EVT__Ticket_Number__c:string;
  export default CnP_PaaS_EVT__Ticket_Number__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_registrant_session__c.CnP_PaaS_EVT__Total_Amount__c" {
  const CnP_PaaS_EVT__Total_Amount__c:number;
  export default CnP_PaaS_EVT__Total_Amount__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_registrant_session__c.CnP_PaaS_EVT__Total_Discount__c" {
  const CnP_PaaS_EVT__Total_Discount__c:number;
  export default CnP_PaaS_EVT__Total_Discount__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_registrant_session__c.CnP_PaaS_EVT__Transaction_order_number__c" {
  const CnP_PaaS_EVT__Transaction_order_number__c:string;
  export default CnP_PaaS_EVT__Transaction_order_number__c;
}
