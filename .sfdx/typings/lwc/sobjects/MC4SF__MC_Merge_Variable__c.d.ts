declare module "@salesforce/schema/MC4SF__MC_Merge_Variable__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/MC4SF__MC_Merge_Variable__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/MC4SF__MC_Merge_Variable__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/MC4SF__MC_Merge_Variable__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/MC4SF__MC_Merge_Variable__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/MC4SF__MC_Merge_Variable__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/MC4SF__MC_Merge_Variable__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/MC4SF__MC_Merge_Variable__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/MC4SF__MC_Merge_Variable__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/MC4SF__MC_Merge_Variable__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/MC4SF__MC_Merge_Variable__c.MC4SF__MC_List__r" {
  const MC4SF__MC_List__r:any;
  export default MC4SF__MC_List__r;
}
declare module "@salesforce/schema/MC4SF__MC_Merge_Variable__c.MC4SF__MC_List__c" {
  const MC4SF__MC_List__c:any;
  export default MC4SF__MC_List__c;
}
declare module "@salesforce/schema/MC4SF__MC_Merge_Variable__c.MC4SF__Choices__c" {
  const MC4SF__Choices__c:string;
  export default MC4SF__Choices__c;
}
declare module "@salesforce/schema/MC4SF__MC_Merge_Variable__c.MC4SF__Contact_Field_Mapping__c" {
  const MC4SF__Contact_Field_Mapping__c:string;
  export default MC4SF__Contact_Field_Mapping__c;
}
declare module "@salesforce/schema/MC4SF__MC_Merge_Variable__c.MC4SF__Default_Value__c" {
  const MC4SF__Default_Value__c:string;
  export default MC4SF__Default_Value__c;
}
declare module "@salesforce/schema/MC4SF__MC_Merge_Variable__c.MC4SF__Field_Type__c" {
  const MC4SF__Field_Type__c:string;
  export default MC4SF__Field_Type__c;
}
declare module "@salesforce/schema/MC4SF__MC_Merge_Variable__c.MC4SF__Lead_Field_Mapping__c" {
  const MC4SF__Lead_Field_Mapping__c:string;
  export default MC4SF__Lead_Field_Mapping__c;
}
declare module "@salesforce/schema/MC4SF__MC_Merge_Variable__c.MC4SF__MailChimp_ID__c" {
  const MC4SF__MailChimp_ID__c:number;
  export default MC4SF__MailChimp_ID__c;
}
declare module "@salesforce/schema/MC4SF__MC_Merge_Variable__c.MC4SF__Order__c" {
  const MC4SF__Order__c:string;
  export default MC4SF__Order__c;
}
declare module "@salesforce/schema/MC4SF__MC_Merge_Variable__c.MC4SF__Public__c" {
  const MC4SF__Public__c:boolean;
  export default MC4SF__Public__c;
}
declare module "@salesforce/schema/MC4SF__MC_Merge_Variable__c.MC4SF__Required__c" {
  const MC4SF__Required__c:boolean;
  export default MC4SF__Required__c;
}
declare module "@salesforce/schema/MC4SF__MC_Merge_Variable__c.MC4SF__SFDC_Data_Type__c" {
  const MC4SF__SFDC_Data_Type__c:string;
  export default MC4SF__SFDC_Data_Type__c;
}
declare module "@salesforce/schema/MC4SF__MC_Merge_Variable__c.MC4SF__Show__c" {
  const MC4SF__Show__c:boolean;
  export default MC4SF__Show__c;
}
declare module "@salesforce/schema/MC4SF__MC_Merge_Variable__c.MC4SF__Size__c" {
  const MC4SF__Size__c:string;
  export default MC4SF__Size__c;
}
declare module "@salesforce/schema/MC4SF__MC_Merge_Variable__c.MC4SF__Tag__c" {
  const MC4SF__Tag__c:string;
  export default MC4SF__Tag__c;
}
