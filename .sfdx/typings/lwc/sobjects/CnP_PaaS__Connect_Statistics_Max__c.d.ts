declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.CnP_PaaS__Donor_Personal_Donation_Amount_Rank_Max__c" {
  const CnP_PaaS__Donor_Personal_Donation_Amount_Rank_Max__c:number;
  export default CnP_PaaS__Donor_Personal_Donation_Amount_Rank_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.CnP_PaaS__Donor_Personal_Donation_Count_Rank_Max__c" {
  const CnP_PaaS__Donor_Personal_Donation_Count_Rank_Max__c:number;
  export default CnP_PaaS__Donor_Personal_Donation_Count_Rank_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.CnP_PaaS__Donor_Raised_Donation_Amount_Rank_Max__c" {
  const CnP_PaaS__Donor_Raised_Donation_Amount_Rank_Max__c:number;
  export default CnP_PaaS__Donor_Raised_Donation_Amount_Rank_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.CnP_PaaS__Donor_Raised_Donation_Count_Rank_Max__c" {
  const CnP_PaaS__Donor_Raised_Donation_Count_Rank_Max__c:number;
  export default CnP_PaaS__Donor_Raised_Donation_Count_Rank_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.CnP_PaaS__Donors_Rank_Factor_Max__c" {
  const CnP_PaaS__Donors_Rank_Factor_Max__c:number;
  export default CnP_PaaS__Donors_Rank_Factor_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.CnP_PaaS__Job_Status__c" {
  const CnP_PaaS__Job_Status__c:string;
  export default CnP_PaaS__Job_Status__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.CnP_PaaS__My_Donor_Personal_Donation_Amount_Max__c" {
  const CnP_PaaS__My_Donor_Personal_Donation_Amount_Max__c:number;
  export default CnP_PaaS__My_Donor_Personal_Donation_Amount_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.CnP_PaaS__My_Donor_Personal_Donation_Count_Max__c" {
  const CnP_PaaS__My_Donor_Personal_Donation_Count_Max__c:number;
  export default CnP_PaaS__My_Donor_Personal_Donation_Count_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.CnP_PaaS__My_Donor_Raised_Donation_Amount_Max__c" {
  const CnP_PaaS__My_Donor_Raised_Donation_Amount_Max__c:number;
  export default CnP_PaaS__My_Donor_Raised_Donation_Amount_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.CnP_PaaS__My_Donor_Raised_Donation_Count_Max__c" {
  const CnP_PaaS__My_Donor_Raised_Donation_Count_Max__c:number;
  export default CnP_PaaS__My_Donor_Raised_Donation_Count_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.CnP_PaaS__My_Rank_Factor_Max__c" {
  const CnP_PaaS__My_Rank_Factor_Max__c:number;
  export default CnP_PaaS__My_Rank_Factor_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.CnP_PaaS__Personal_Donation_Amount_Max__c" {
  const CnP_PaaS__Personal_Donation_Amount_Max__c:number;
  export default CnP_PaaS__Personal_Donation_Amount_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.CnP_PaaS__Personal_Donation_Amount_Rank_Max__c" {
  const CnP_PaaS__Personal_Donation_Amount_Rank_Max__c:number;
  export default CnP_PaaS__Personal_Donation_Amount_Rank_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.CnP_PaaS__Personal_Donation_Count_Max__c" {
  const CnP_PaaS__Personal_Donation_Count_Max__c:number;
  export default CnP_PaaS__Personal_Donation_Count_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.CnP_PaaS__Personal_Donation_Count_Rank_Max__c" {
  const CnP_PaaS__Personal_Donation_Count_Rank_Max__c:number;
  export default CnP_PaaS__Personal_Donation_Count_Rank_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.CnP_PaaS__Raised_Donation_Amount_Max__c" {
  const CnP_PaaS__Raised_Donation_Amount_Max__c:number;
  export default CnP_PaaS__Raised_Donation_Amount_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.CnP_PaaS__Raised_Donation_Amount_Rank_Max__c" {
  const CnP_PaaS__Raised_Donation_Amount_Rank_Max__c:number;
  export default CnP_PaaS__Raised_Donation_Amount_Rank_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.CnP_PaaS__Raised_Donation_Count_Max__c" {
  const CnP_PaaS__Raised_Donation_Count_Max__c:number;
  export default CnP_PaaS__Raised_Donation_Count_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.CnP_PaaS__Raised_Donation_Count_Rank_Max__c" {
  const CnP_PaaS__Raised_Donation_Count_Rank_Max__c:number;
  export default CnP_PaaS__Raised_Donation_Count_Rank_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.CnP_PaaS__Total_Intrinsic_Value_Max__c" {
  const CnP_PaaS__Total_Intrinsic_Value_Max__c:number;
  export default CnP_PaaS__Total_Intrinsic_Value_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.CnP_PaaS__Total_Records__c" {
  const CnP_PaaS__Total_Records__c:number;
  export default CnP_PaaS__Total_Records__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.CnP_PaaS__Total__c" {
  const CnP_PaaS__Total__c:number;
  export default CnP_PaaS__Total__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__c.CnP_PaaS__Total_extrinsic_value_Max__c" {
  const CnP_PaaS__Total_extrinsic_value_Max__c:number;
  export default CnP_PaaS__Total_extrinsic_value_Max__c;
}
