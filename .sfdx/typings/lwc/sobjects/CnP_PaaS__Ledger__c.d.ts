declare module "@salesforce/schema/CnP_PaaS__Ledger__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Ledger__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__Ledger__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__Ledger__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__Ledger__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Ledger__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Ledger__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Ledger__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Ledger__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Ledger__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Ledger__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Ledger__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__Ledger__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/CnP_PaaS__Ledger__c.CnP_PaaS__Account_Name__c" {
  const CnP_PaaS__Account_Name__c:string;
  export default CnP_PaaS__Account_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__Ledger__c.CnP_PaaS__Account_Number__c" {
  const CnP_PaaS__Account_Number__c:string;
  export default CnP_PaaS__Account_Number__c;
}
declare module "@salesforce/schema/CnP_PaaS__Ledger__c.CnP_PaaS__Account_Type__c" {
  const CnP_PaaS__Account_Type__c:string;
  export default CnP_PaaS__Account_Type__c;
}
declare module "@salesforce/schema/CnP_PaaS__Ledger__c.CnP_PaaS__Description__c" {
  const CnP_PaaS__Description__c:string;
  export default CnP_PaaS__Description__c;
}
declare module "@salesforce/schema/CnP_PaaS__Ledger__c.CnP_PaaS__Notes__c" {
  const CnP_PaaS__Notes__c:string;
  export default CnP_PaaS__Notes__c;
}
declare module "@salesforce/schema/CnP_PaaS__Ledger__c.CnP_PaaS__Sub_Account__c" {
  const CnP_PaaS__Sub_Account__c:string;
  export default CnP_PaaS__Sub_Account__c;
}
