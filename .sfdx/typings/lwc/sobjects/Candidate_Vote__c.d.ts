declare module "@salesforce/schema/Candidate_Vote__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Candidate_Vote__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Candidate_Vote__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Candidate_Vote__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Candidate_Vote__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Candidate_Vote__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Candidate_Vote__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Candidate_Vote__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Candidate_Vote__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Candidate_Vote__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Candidate_Vote__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/Candidate_Vote__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Candidate_Vote__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Candidate_Vote__c.Community_Campaign_Vote__r" {
  const Community_Campaign_Vote__r:any;
  export default Community_Campaign_Vote__r;
}
declare module "@salesforce/schema/Candidate_Vote__c.Community_Campaign_Vote__c" {
  const Community_Campaign_Vote__c:any;
  export default Community_Campaign_Vote__c;
}
declare module "@salesforce/schema/Candidate_Vote__c.Community_Campaign_Candidate__r" {
  const Community_Campaign_Candidate__r:any;
  export default Community_Campaign_Candidate__r;
}
declare module "@salesforce/schema/Candidate_Vote__c.Community_Campaign_Candidate__c" {
  const Community_Campaign_Candidate__c:any;
  export default Community_Campaign_Candidate__c;
}
declare module "@salesforce/schema/Candidate_Vote__c.Candidate_Name__c" {
  const Candidate_Name__c:string;
  export default Candidate_Name__c;
}
