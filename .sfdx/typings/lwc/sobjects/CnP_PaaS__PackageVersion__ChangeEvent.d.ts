declare module "@salesforce/schema/CnP_PaaS__PackageVersion__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__PackageVersion__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__PackageVersion__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__PackageVersion__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__PackageVersion__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__PackageVersion__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__PackageVersion__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__PackageVersion__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__PackageVersion__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__PackageVersion__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__PackageVersion__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__PackageVersion__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__PackageVersion__ChangeEvent.CnP_PaaS__Appexchange_Link__c" {
  const CnP_PaaS__Appexchange_Link__c:string;
  export default CnP_PaaS__Appexchange_Link__c;
}
declare module "@salesforce/schema/CnP_PaaS__PackageVersion__ChangeEvent.CnP_PaaS__Forum__c" {
  const CnP_PaaS__Forum__c:string;
  export default CnP_PaaS__Forum__c;
}
declare module "@salesforce/schema/CnP_PaaS__PackageVersion__ChangeEvent.CnP_PaaS__Manual__c" {
  const CnP_PaaS__Manual__c:string;
  export default CnP_PaaS__Manual__c;
}
declare module "@salesforce/schema/CnP_PaaS__PackageVersion__ChangeEvent.CnP_PaaS__Namespace__c" {
  const CnP_PaaS__Namespace__c:string;
  export default CnP_PaaS__Namespace__c;
}
declare module "@salesforce/schema/CnP_PaaS__PackageVersion__ChangeEvent.CnP_PaaS__Version_Number__c" {
  const CnP_PaaS__Version_Number__c:number;
  export default CnP_PaaS__Version_Number__c;
}
