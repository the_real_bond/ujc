declare module "@salesforce/schema/Life_Cycle__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Life_Cycle__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/Life_Cycle__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/Life_Cycle__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Life_Cycle__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Life_Cycle__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Life_Cycle__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Life_Cycle__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Life_Cycle__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Life_Cycle__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Life_Cycle__ChangeEvent.Event_Type__c" {
  const Event_Type__c:string;
  export default Event_Type__c;
}
declare module "@salesforce/schema/Life_Cycle__ChangeEvent.Location__c" {
  const Location__c:string;
  export default Location__c;
}
declare module "@salesforce/schema/Life_Cycle__ChangeEvent.Relationship__c" {
  const Relationship__c:string;
  export default Relationship__c;
}
declare module "@salesforce/schema/Life_Cycle__ChangeEvent.Event_Date__c" {
  const Event_Date__c:any;
  export default Event_Date__c;
}
declare module "@salesforce/schema/Life_Cycle__ChangeEvent.Source__c" {
  const Source__c:string;
  export default Source__c;
}
declare module "@salesforce/schema/Life_Cycle__ChangeEvent.Person__c" {
  const Person__c:any;
  export default Person__c;
}
declare module "@salesforce/schema/Life_Cycle__ChangeEvent.Relation__c" {
  const Relation__c:string;
  export default Relation__c;
}
declare module "@salesforce/schema/Life_Cycle__ChangeEvent.Event_Details__c" {
  const Event_Details__c:string;
  export default Event_Details__c;
}
declare module "@salesforce/schema/Life_Cycle__ChangeEvent.Life_Cycle_Year__c" {
  const Life_Cycle_Year__c:string;
  export default Life_Cycle_Year__c;
}
declare module "@salesforce/schema/Life_Cycle__ChangeEvent.Life_Cycle_Week__c" {
  const Life_Cycle_Week__c:string;
  export default Life_Cycle_Week__c;
}
declare module "@salesforce/schema/Life_Cycle__ChangeEvent.Event__c" {
  const Event__c:string;
  export default Event__c;
}
declare module "@salesforce/schema/Life_Cycle__ChangeEvent.Preferred_Name__c" {
  const Preferred_Name__c:string;
  export default Preferred_Name__c;
}
declare module "@salesforce/schema/Life_Cycle__ChangeEvent.Mail_Merge_Email__c" {
  const Mail_Merge_Email__c:string;
  export default Mail_Merge_Email__c;
}
declare module "@salesforce/schema/Life_Cycle__ChangeEvent.Formal_Salutation__c" {
  const Formal_Salutation__c:string;
  export default Formal_Salutation__c;
}
declare module "@salesforce/schema/Life_Cycle__ChangeEvent.Hebrew_Date__c" {
  const Hebrew_Date__c:string;
  export default Hebrew_Date__c;
}
declare module "@salesforce/schema/Life_Cycle__ChangeEvent.Today_s_Date__c" {
  const Today_s_Date__c:string;
  export default Today_s_Date__c;
}
declare module "@salesforce/schema/Life_Cycle__ChangeEvent.Relation_Last__c" {
  const Relation_Last__c:string;
  export default Relation_Last__c;
}
declare module "@salesforce/schema/Life_Cycle__ChangeEvent.Letter_Sent__c" {
  const Letter_Sent__c:boolean;
  export default Letter_Sent__c;
}
declare module "@salesforce/schema/Life_Cycle__ChangeEvent.Event_Category__c" {
  const Event_Category__c:string;
  export default Event_Category__c;
}
declare module "@salesforce/schema/Life_Cycle__ChangeEvent.Conga_Template_Id__c" {
  const Conga_Template_Id__c:string;
  export default Conga_Template_Id__c;
}
declare module "@salesforce/schema/Life_Cycle__ChangeEvent.Gender_Pronoun__c" {
  const Gender_Pronoun__c:string;
  export default Gender_Pronoun__c;
}
declare module "@salesforce/schema/Life_Cycle__ChangeEvent.Not_Sent_Because__c" {
  const Not_Sent_Because__c:string;
  export default Not_Sent_Because__c;
}
declare module "@salesforce/schema/Life_Cycle__ChangeEvent.Salutation_First_Names__c" {
  const Salutation_First_Names__c:string;
  export default Salutation_First_Names__c;
}
