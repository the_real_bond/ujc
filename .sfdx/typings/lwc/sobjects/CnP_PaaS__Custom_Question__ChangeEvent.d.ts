declare module "@salesforce/schema/CnP_PaaS__Custom_Question__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Custom_Question__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__Custom_Question__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__Custom_Question__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__Custom_Question__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__Custom_Question__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Custom_Question__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Custom_Question__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Custom_Question__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Custom_Question__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Custom_Question__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Custom_Question__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Custom_Question__ChangeEvent.CnP_PaaS__Answer__c" {
  const CnP_PaaS__Answer__c:string;
  export default CnP_PaaS__Answer__c;
}
declare module "@salesforce/schema/CnP_PaaS__Custom_Question__ChangeEvent.CnP_PaaS__C_P_Transaction__c" {
  const CnP_PaaS__C_P_Transaction__c:any;
  export default CnP_PaaS__C_P_Transaction__c;
}
declare module "@salesforce/schema/CnP_PaaS__Custom_Question__ChangeEvent.CnP_PaaS__Contact__c" {
  const CnP_PaaS__Contact__c:any;
  export default CnP_PaaS__Contact__c;
}
declare module "@salesforce/schema/CnP_PaaS__Custom_Question__ChangeEvent.CnP_PaaS__Custom_Question__c" {
  const CnP_PaaS__Custom_Question__c:string;
  export default CnP_PaaS__Custom_Question__c;
}
