declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings__ChangeEvent.CnP_PaaS__BCC_Email_Address__c" {
  const CnP_PaaS__BCC_Email_Address__c:string;
  export default CnP_PaaS__BCC_Email_Address__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings__ChangeEvent.CnP_PaaS__Mail_From_Address__c" {
  const CnP_PaaS__Mail_From_Address__c:string;
  export default CnP_PaaS__Mail_From_Address__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings__ChangeEvent.CnP_PaaS__Mail_From_Name__c" {
  const CnP_PaaS__Mail_From_Name__c:string;
  export default CnP_PaaS__Mail_From_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings__ChangeEvent.CnP_PaaS__Replay_To_Address__c" {
  const CnP_PaaS__Replay_To_Address__c:string;
  export default CnP_PaaS__Replay_To_Address__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings__ChangeEvent.CnP_PaaS__Subject__c" {
  const CnP_PaaS__Subject__c:string;
  export default CnP_PaaS__Subject__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings__ChangeEvent.CnP_PaaS__TagsNew__c" {
  const CnP_PaaS__TagsNew__c:string;
  export default CnP_PaaS__TagsNew__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings__ChangeEvent.CnP_PaaS__Tags__c" {
  const CnP_PaaS__Tags__c:string;
  export default CnP_PaaS__Tags__c;
}
