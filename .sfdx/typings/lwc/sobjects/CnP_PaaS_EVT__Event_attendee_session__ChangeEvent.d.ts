declare module "@salesforce/schema/CnP_PaaS_EVT__Event_attendee_session__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_attendee_session__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_attendee_session__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_attendee_session__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_attendee_session__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_attendee_session__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_attendee_session__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_attendee_session__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_attendee_session__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_attendee_session__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_attendee_session__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_attendee_session__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_attendee_session__ChangeEvent.CnP_PaaS_EVT__C_P_Discount_Plan__c" {
  const CnP_PaaS_EVT__C_P_Discount_Plan__c:any;
  export default CnP_PaaS_EVT__C_P_Discount_Plan__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_attendee_session__ChangeEvent.CnP_PaaS_EVT__C_P_Event_LevelGroup__c" {
  const CnP_PaaS_EVT__C_P_Event_LevelGroup__c:any;
  export default CnP_PaaS_EVT__C_P_Event_LevelGroup__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_attendee_session__ChangeEvent.CnP_PaaS_EVT__CheckIn_Notes__c" {
  const CnP_PaaS_EVT__CheckIn_Notes__c:string;
  export default CnP_PaaS_EVT__CheckIn_Notes__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_attendee_session__ChangeEvent.CnP_PaaS_EVT__CheckIn_Status__c" {
  const CnP_PaaS_EVT__CheckIn_Status__c:string;
  export default CnP_PaaS_EVT__CheckIn_Status__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_attendee_session__ChangeEvent.CnP_PaaS_EVT__ContactId__c" {
  const CnP_PaaS_EVT__ContactId__c:any;
  export default CnP_PaaS_EVT__ContactId__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_attendee_session__ChangeEvent.CnP_PaaS_EVT__Contact_Data__c" {
  const CnP_PaaS_EVT__Contact_Data__c:string;
  export default CnP_PaaS_EVT__Contact_Data__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_attendee_session__ChangeEvent.CnP_PaaS_EVT__EventId__c" {
  const CnP_PaaS_EVT__EventId__c:any;
  export default CnP_PaaS_EVT__EventId__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_attendee_session__ChangeEvent.CnP_PaaS_EVT__First_name__c" {
  const CnP_PaaS_EVT__First_name__c:string;
  export default CnP_PaaS_EVT__First_name__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_attendee_session__ChangeEvent.CnP_PaaS_EVT__Increment_Number__c" {
  const CnP_PaaS_EVT__Increment_Number__c:number;
  export default CnP_PaaS_EVT__Increment_Number__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_attendee_session__ChangeEvent.CnP_PaaS_EVT__Last_Status_Change__c" {
  const CnP_PaaS_EVT__Last_Status_Change__c:any;
  export default CnP_PaaS_EVT__Last_Status_Change__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_attendee_session__ChangeEvent.CnP_PaaS_EVT__Last_name__c" {
  const CnP_PaaS_EVT__Last_name__c:string;
  export default CnP_PaaS_EVT__Last_name__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_attendee_session__ChangeEvent.CnP_PaaS_EVT__Registrant_session_Id__c" {
  const CnP_PaaS_EVT__Registrant_session_Id__c:any;
  export default CnP_PaaS_EVT__Registrant_session_Id__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_attendee_session__ChangeEvent.CnP_PaaS_EVT__Registration_level__c" {
  const CnP_PaaS_EVT__Registration_level__c:any;
  export default CnP_PaaS_EVT__Registration_level__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_attendee_session__ChangeEvent.CnP_PaaS_EVT__Status__c" {
  const CnP_PaaS_EVT__Status__c:string;
  export default CnP_PaaS_EVT__Status__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_attendee_session__ChangeEvent.CnP_PaaS_EVT__Text_Field__c" {
  const CnP_PaaS_EVT__Text_Field__c:string;
  export default CnP_PaaS_EVT__Text_Field__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_attendee_session__ChangeEvent.CnP_PaaS_EVT__Ticket_Number__c" {
  const CnP_PaaS_EVT__Ticket_Number__c:string;
  export default CnP_PaaS_EVT__Ticket_Number__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_attendee_session__ChangeEvent.CnP_PaaS_EVT__Ticket_guid__c" {
  const CnP_PaaS_EVT__Ticket_guid__c:string;
  export default CnP_PaaS_EVT__Ticket_guid__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_attendee_session__ChangeEvent.CnP_PaaS_EVT__Total_Amount__c" {
  const CnP_PaaS_EVT__Total_Amount__c:number;
  export default CnP_PaaS_EVT__Total_Amount__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_attendee_session__ChangeEvent.CnP_PaaS_EVT__Total_Discount__c" {
  const CnP_PaaS_EVT__Total_Discount__c:number;
  export default CnP_PaaS_EVT__Total_Discount__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_attendee_session__ChangeEvent.CnP_PaaS_EVT__Total_Tax__c" {
  const CnP_PaaS_EVT__Total_Tax__c:number;
  export default CnP_PaaS_EVT__Total_Tax__c;
}
