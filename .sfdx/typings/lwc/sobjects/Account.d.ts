declare module "@salesforce/schema/Account.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Account.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Account.MasterRecord" {
  const MasterRecord:any;
  export default MasterRecord;
}
declare module "@salesforce/schema/Account.MasterRecordId" {
  const MasterRecordId:any;
  export default MasterRecordId;
}
declare module "@salesforce/schema/Account.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Account.LastName" {
  const LastName:string;
  export default LastName;
}
declare module "@salesforce/schema/Account.FirstName" {
  const FirstName:string;
  export default FirstName;
}
declare module "@salesforce/schema/Account.Salutation" {
  const Salutation:string;
  export default Salutation;
}
declare module "@salesforce/schema/Account.Type" {
  const Type:string;
  export default Type;
}
declare module "@salesforce/schema/Account.RecordType" {
  const RecordType:any;
  export default RecordType;
}
declare module "@salesforce/schema/Account.RecordTypeId" {
  const RecordTypeId:any;
  export default RecordTypeId;
}
declare module "@salesforce/schema/Account.BillingStreet" {
  const BillingStreet:string;
  export default BillingStreet;
}
declare module "@salesforce/schema/Account.BillingCity" {
  const BillingCity:string;
  export default BillingCity;
}
declare module "@salesforce/schema/Account.BillingState" {
  const BillingState:string;
  export default BillingState;
}
declare module "@salesforce/schema/Account.BillingPostalCode" {
  const BillingPostalCode:string;
  export default BillingPostalCode;
}
declare module "@salesforce/schema/Account.BillingCountry" {
  const BillingCountry:string;
  export default BillingCountry;
}
declare module "@salesforce/schema/Account.BillingLatitude" {
  const BillingLatitude:number;
  export default BillingLatitude;
}
declare module "@salesforce/schema/Account.BillingLongitude" {
  const BillingLongitude:number;
  export default BillingLongitude;
}
declare module "@salesforce/schema/Account.BillingGeocodeAccuracy" {
  const BillingGeocodeAccuracy:string;
  export default BillingGeocodeAccuracy;
}
declare module "@salesforce/schema/Account.BillingAddress" {
  const BillingAddress:any;
  export default BillingAddress;
}
declare module "@salesforce/schema/Account.ShippingStreet" {
  const ShippingStreet:string;
  export default ShippingStreet;
}
declare module "@salesforce/schema/Account.ShippingCity" {
  const ShippingCity:string;
  export default ShippingCity;
}
declare module "@salesforce/schema/Account.ShippingState" {
  const ShippingState:string;
  export default ShippingState;
}
declare module "@salesforce/schema/Account.ShippingPostalCode" {
  const ShippingPostalCode:string;
  export default ShippingPostalCode;
}
declare module "@salesforce/schema/Account.ShippingCountry" {
  const ShippingCountry:string;
  export default ShippingCountry;
}
declare module "@salesforce/schema/Account.ShippingLatitude" {
  const ShippingLatitude:number;
  export default ShippingLatitude;
}
declare module "@salesforce/schema/Account.ShippingLongitude" {
  const ShippingLongitude:number;
  export default ShippingLongitude;
}
declare module "@salesforce/schema/Account.ShippingGeocodeAccuracy" {
  const ShippingGeocodeAccuracy:string;
  export default ShippingGeocodeAccuracy;
}
declare module "@salesforce/schema/Account.ShippingAddress" {
  const ShippingAddress:any;
  export default ShippingAddress;
}
declare module "@salesforce/schema/Account.Phone" {
  const Phone:string;
  export default Phone;
}
declare module "@salesforce/schema/Account.Fax" {
  const Fax:string;
  export default Fax;
}
declare module "@salesforce/schema/Account.AccountNumber" {
  const AccountNumber:string;
  export default AccountNumber;
}
declare module "@salesforce/schema/Account.Website" {
  const Website:string;
  export default Website;
}
declare module "@salesforce/schema/Account.PhotoUrl" {
  const PhotoUrl:string;
  export default PhotoUrl;
}
declare module "@salesforce/schema/Account.Sic" {
  const Sic:string;
  export default Sic;
}
declare module "@salesforce/schema/Account.Industry" {
  const Industry:string;
  export default Industry;
}
declare module "@salesforce/schema/Account.AnnualRevenue" {
  const AnnualRevenue:number;
  export default AnnualRevenue;
}
declare module "@salesforce/schema/Account.NumberOfEmployees" {
  const NumberOfEmployees:number;
  export default NumberOfEmployees;
}
declare module "@salesforce/schema/Account.Ownership" {
  const Ownership:string;
  export default Ownership;
}
declare module "@salesforce/schema/Account.TickerSymbol" {
  const TickerSymbol:string;
  export default TickerSymbol;
}
declare module "@salesforce/schema/Account.Description" {
  const Description:string;
  export default Description;
}
declare module "@salesforce/schema/Account.Rating" {
  const Rating:string;
  export default Rating;
}
declare module "@salesforce/schema/Account.Site" {
  const Site:string;
  export default Site;
}
declare module "@salesforce/schema/Account.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Account.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Account.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Account.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Account.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Account.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Account.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Account.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Account.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Account.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/Account.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Account.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Account.IsCustomerPortal" {
  const IsCustomerPortal:boolean;
  export default IsCustomerPortal;
}
declare module "@salesforce/schema/Account.PersonContact" {
  const PersonContact:any;
  export default PersonContact;
}
declare module "@salesforce/schema/Account.PersonContactId" {
  const PersonContactId:any;
  export default PersonContactId;
}
declare module "@salesforce/schema/Account.IsPersonAccount" {
  const IsPersonAccount:boolean;
  export default IsPersonAccount;
}
declare module "@salesforce/schema/Account.PersonMailingStreet" {
  const PersonMailingStreet:string;
  export default PersonMailingStreet;
}
declare module "@salesforce/schema/Account.PersonMailingCity" {
  const PersonMailingCity:string;
  export default PersonMailingCity;
}
declare module "@salesforce/schema/Account.PersonMailingState" {
  const PersonMailingState:string;
  export default PersonMailingState;
}
declare module "@salesforce/schema/Account.PersonMailingPostalCode" {
  const PersonMailingPostalCode:string;
  export default PersonMailingPostalCode;
}
declare module "@salesforce/schema/Account.PersonMailingCountry" {
  const PersonMailingCountry:string;
  export default PersonMailingCountry;
}
declare module "@salesforce/schema/Account.PersonMailingLatitude" {
  const PersonMailingLatitude:number;
  export default PersonMailingLatitude;
}
declare module "@salesforce/schema/Account.PersonMailingLongitude" {
  const PersonMailingLongitude:number;
  export default PersonMailingLongitude;
}
declare module "@salesforce/schema/Account.PersonMailingGeocodeAccuracy" {
  const PersonMailingGeocodeAccuracy:string;
  export default PersonMailingGeocodeAccuracy;
}
declare module "@salesforce/schema/Account.PersonMailingAddress" {
  const PersonMailingAddress:any;
  export default PersonMailingAddress;
}
declare module "@salesforce/schema/Account.PersonOtherStreet" {
  const PersonOtherStreet:string;
  export default PersonOtherStreet;
}
declare module "@salesforce/schema/Account.PersonOtherCity" {
  const PersonOtherCity:string;
  export default PersonOtherCity;
}
declare module "@salesforce/schema/Account.PersonOtherState" {
  const PersonOtherState:string;
  export default PersonOtherState;
}
declare module "@salesforce/schema/Account.PersonOtherPostalCode" {
  const PersonOtherPostalCode:string;
  export default PersonOtherPostalCode;
}
declare module "@salesforce/schema/Account.PersonOtherCountry" {
  const PersonOtherCountry:string;
  export default PersonOtherCountry;
}
declare module "@salesforce/schema/Account.PersonOtherLatitude" {
  const PersonOtherLatitude:number;
  export default PersonOtherLatitude;
}
declare module "@salesforce/schema/Account.PersonOtherLongitude" {
  const PersonOtherLongitude:number;
  export default PersonOtherLongitude;
}
declare module "@salesforce/schema/Account.PersonOtherGeocodeAccuracy" {
  const PersonOtherGeocodeAccuracy:string;
  export default PersonOtherGeocodeAccuracy;
}
declare module "@salesforce/schema/Account.PersonOtherAddress" {
  const PersonOtherAddress:any;
  export default PersonOtherAddress;
}
declare module "@salesforce/schema/Account.PersonMobilePhone" {
  const PersonMobilePhone:string;
  export default PersonMobilePhone;
}
declare module "@salesforce/schema/Account.PersonHomePhone" {
  const PersonHomePhone:string;
  export default PersonHomePhone;
}
declare module "@salesforce/schema/Account.PersonOtherPhone" {
  const PersonOtherPhone:string;
  export default PersonOtherPhone;
}
declare module "@salesforce/schema/Account.PersonAssistantPhone" {
  const PersonAssistantPhone:string;
  export default PersonAssistantPhone;
}
declare module "@salesforce/schema/Account.PersonEmail" {
  const PersonEmail:string;
  export default PersonEmail;
}
declare module "@salesforce/schema/Account.PersonTitle" {
  const PersonTitle:string;
  export default PersonTitle;
}
declare module "@salesforce/schema/Account.PersonDepartment" {
  const PersonDepartment:string;
  export default PersonDepartment;
}
declare module "@salesforce/schema/Account.PersonAssistantName" {
  const PersonAssistantName:string;
  export default PersonAssistantName;
}
declare module "@salesforce/schema/Account.PersonLeadSource" {
  const PersonLeadSource:string;
  export default PersonLeadSource;
}
declare module "@salesforce/schema/Account.PersonBirthdate" {
  const PersonBirthdate:any;
  export default PersonBirthdate;
}
declare module "@salesforce/schema/Account.PersonHasOptedOutOfEmail" {
  const PersonHasOptedOutOfEmail:boolean;
  export default PersonHasOptedOutOfEmail;
}
declare module "@salesforce/schema/Account.PersonDoNotCall" {
  const PersonDoNotCall:boolean;
  export default PersonDoNotCall;
}
declare module "@salesforce/schema/Account.PersonLastCURequestDate" {
  const PersonLastCURequestDate:any;
  export default PersonLastCURequestDate;
}
declare module "@salesforce/schema/Account.PersonLastCUUpdateDate" {
  const PersonLastCUUpdateDate:any;
  export default PersonLastCUUpdateDate;
}
declare module "@salesforce/schema/Account.PersonEmailBouncedReason" {
  const PersonEmailBouncedReason:string;
  export default PersonEmailBouncedReason;
}
declare module "@salesforce/schema/Account.PersonEmailBouncedDate" {
  const PersonEmailBouncedDate:any;
  export default PersonEmailBouncedDate;
}
declare module "@salesforce/schema/Account.PersonIndividual" {
  const PersonIndividual:any;
  export default PersonIndividual;
}
declare module "@salesforce/schema/Account.PersonIndividualId" {
  const PersonIndividualId:any;
  export default PersonIndividualId;
}
declare module "@salesforce/schema/Account.Jigsaw" {
  const Jigsaw:string;
  export default Jigsaw;
}
declare module "@salesforce/schema/Account.JigsawCompanyId" {
  const JigsawCompanyId:string;
  export default JigsawCompanyId;
}
declare module "@salesforce/schema/Account.AccountSource" {
  const AccountSource:string;
  export default AccountSource;
}
declare module "@salesforce/schema/Account.SicDesc" {
  const SicDesc:string;
  export default SicDesc;
}
declare module "@salesforce/schema/Account.Year_of_Emigration__c" {
  const Year_of_Emigration__c:string;
  export default Year_of_Emigration__c;
}
declare module "@salesforce/schema/Account.Receive_Statement__c" {
  const Receive_Statement__c:string;
  export default Receive_Statement__c;
}
declare module "@salesforce/schema/Account.Tax_Certificate_Required__c" {
  const Tax_Certificate_Required__c:string;
  export default Tax_Certificate_Required__c;
}
declare module "@salesforce/schema/Account.Communal_Reference_Auto_Number__c" {
  const Communal_Reference_Auto_Number__c:string;
  export default Communal_Reference_Auto_Number__c;
}
declare module "@salesforce/schema/Account.Old_Register_Area_R__c" {
  const Old_Register_Area_R__c:string;
  export default Old_Register_Area_R__c;
}
declare module "@salesforce/schema/Account.CanvassStatus__c" {
  const CanvassStatus__c:string;
  export default CanvassStatus__c;
}
declare module "@salesforce/schema/Account.Picture__c" {
  const Picture__c:string;
  export default Picture__c;
}
declare module "@salesforce/schema/Account.Communal_Reference_Number__c" {
  const Communal_Reference_Number__c:string;
  export default Communal_Reference_Number__c;
}
declare module "@salesforce/schema/Account.LinkedIn__c" {
  const LinkedIn__c:string;
  export default LinkedIn__c;
}
declare module "@salesforce/schema/Account.Assistant_Email__c" {
  const Assistant_Email__c:string;
  export default Assistant_Email__c;
}
declare module "@salesforce/schema/Account.Residential_Fax__c" {
  const Residential_Fax__c:string;
  export default Residential_Fax__c;
}
declare module "@salesforce/schema/Account.Cape_Jewish_Seniors__c" {
  const Cape_Jewish_Seniors__c:string;
  export default Cape_Jewish_Seniors__c;
}
declare module "@salesforce/schema/Account.Last_JC_Pledge_Date__c" {
  const Last_JC_Pledge_Date__c:any;
  export default Last_JC_Pledge_Date__c;
}
declare module "@salesforce/schema/Account.Children__c" {
  const Children__c:string;
  export default Children__c;
}
declare module "@salesforce/schema/Account.Children_at_Herzlia__c" {
  const Children_at_Herzlia__c:boolean;
  export default Children_at_Herzlia__c;
}
declare module "@salesforce/schema/Account.Chronicle_Mailing__c" {
  const Chronicle_Mailing__c:string;
  export default Chronicle_Mailing__c;
}
declare module "@salesforce/schema/Account.Date_of_Death__c" {
  const Date_of_Death__c:any;
  export default Date_of_Death__c;
}
declare module "@salesforce/schema/Account.Emigrated_To__c" {
  const Emigrated_To__c:string;
  export default Emigrated_To__c;
}
declare module "@salesforce/schema/Account.Email_Alternative__c" {
  const Email_Alternative__c:string;
  export default Email_Alternative__c;
}
declare module "@salesforce/schema/Account.Email_Personal__c" {
  const Email_Personal__c:string;
  export default Email_Personal__c;
}
declare module "@salesforce/schema/Account.Email_Preference__c" {
  const Email_Preference__c:string;
  export default Email_Preference__c;
}
declare module "@salesforce/schema/Account.Gender__c" {
  const Gender__c:string;
  export default Gender__c;
}
declare module "@salesforce/schema/Account.Household__r" {
  const Household__r:any;
  export default Household__r;
}
declare module "@salesforce/schema/Account.Household__c" {
  const Household__c:any;
  export default Household__c;
}
declare module "@salesforce/schema/Account.ID_No__c" {
  const ID_No__c:string;
  export default ID_No__c;
}
declare module "@salesforce/schema/Account.Initials__c" {
  const Initials__c:string;
  export default Initials__c;
}
declare module "@salesforce/schema/Account.Israeli__c" {
  const Israeli__c:string;
  export default Israeli__c;
}
declare module "@salesforce/schema/Account.Mail_Preference__c" {
  const Mail_Preference__c:string;
  export default Mail_Preference__c;
}
declare module "@salesforce/schema/Account.Occupation__c" {
  const Occupation__c:string;
  export default Occupation__c;
}
declare module "@salesforce/schema/Account.Personal_Interest__c" {
  const Personal_Interest__c:string;
  export default Personal_Interest__c;
}
declare module "@salesforce/schema/Account.Business_Link__r" {
  const Business_Link__r:any;
  export default Business_Link__r;
}
declare module "@salesforce/schema/Account.Business_Link__c" {
  const Business_Link__c:any;
  export default Business_Link__c;
}
declare module "@salesforce/schema/Account.Preferred_Name__c" {
  const Preferred_Name__c:string;
  export default Preferred_Name__c;
}
declare module "@salesforce/schema/Account.Qualification__c" {
  const Qualification__c:string;
  export default Qualification__c;
}
declare module "@salesforce/schema/Account.Retired__c" {
  const Retired__c:string;
  export default Retired__c;
}
declare module "@salesforce/schema/Account.Spouse__c" {
  const Spouse__c:string;
  export default Spouse__c;
}
declare module "@salesforce/schema/Account.DatabaseStatus__c" {
  const DatabaseStatus__c:string;
  export default DatabaseStatus__c;
}
declare module "@salesforce/schema/Account.Address_2_RS__c" {
  const Address_2_RS__c:string;
  export default Address_2_RS__c;
}
declare module "@salesforce/schema/Account.Individual_Type__c" {
  const Individual_Type__c:string;
  export default Individual_Type__c;
}
declare module "@salesforce/schema/Account.Voluntary_Worker__c" {
  const Voluntary_Worker__c:string;
  export default Voluntary_Worker__c;
}
declare module "@salesforce/schema/Account.Residential_Address_RS__c" {
  const Residential_Address_RS__c:string;
  export default Residential_Address_RS__c;
}
declare module "@salesforce/schema/Account.City_RS__c" {
  const City_RS__c:string;
  export default City_RS__c;
}
declare module "@salesforce/schema/Account.AKA__c" {
  const AKA__c:string;
  export default AKA__c;
}
declare module "@salesforce/schema/Account.Province_RS__c" {
  const Province_RS__c:string;
  export default Province_RS__c;
}
declare module "@salesforce/schema/Account.Postal_Code_RS__c" {
  const Postal_Code_RS__c:string;
  export default Postal_Code_RS__c;
}
declare module "@salesforce/schema/Account.Country_RS__c" {
  const Country_RS__c:string;
  export default Country_RS__c;
}
declare module "@salesforce/schema/Account.Residential_Postal_Address_RP__c" {
  const Residential_Postal_Address_RP__c:string;
  export default Residential_Postal_Address_RP__c;
}
declare module "@salesforce/schema/Account.City_RP__c" {
  const City_RP__c:string;
  export default City_RP__c;
}
declare module "@salesforce/schema/Account.Suburb_RP__c" {
  const Suburb_RP__c:string;
  export default Suburb_RP__c;
}
declare module "@salesforce/schema/Account.Postal_Code_RP__c" {
  const Postal_Code_RP__c:string;
  export default Postal_Code_RP__c;
}
declare module "@salesforce/schema/Account.Country_RP__c" {
  const Country_RP__c:string;
  export default Country_RP__c;
}
declare module "@salesforce/schema/Account.Business_Address_BS__c" {
  const Business_Address_BS__c:string;
  export default Business_Address_BS__c;
}
declare module "@salesforce/schema/Account.City_BS__c" {
  const City_BS__c:string;
  export default City_BS__c;
}
declare module "@salesforce/schema/Account.Suburb_BS__c" {
  const Suburb_BS__c:string;
  export default Suburb_BS__c;
}
declare module "@salesforce/schema/Account.Postal_Code_BS__c" {
  const Postal_Code_BS__c:string;
  export default Postal_Code_BS__c;
}
declare module "@salesforce/schema/Account.Country_BS__c" {
  const Country_BS__c:string;
  export default Country_BS__c;
}
declare module "@salesforce/schema/Account.Business_Postal_Address_BP__c" {
  const Business_Postal_Address_BP__c:string;
  export default Business_Postal_Address_BP__c;
}
declare module "@salesforce/schema/Account.City_BP__c" {
  const City_BP__c:string;
  export default City_BP__c;
}
declare module "@salesforce/schema/Account.Suburb_BP__c" {
  const Suburb_BP__c:string;
  export default Suburb_BP__c;
}
declare module "@salesforce/schema/Account.Postal_Code_BP__c" {
  const Postal_Code_BP__c:string;
  export default Postal_Code_BP__c;
}
declare module "@salesforce/schema/Account.Country_BP__c" {
  const Country_BP__c:string;
  export default Country_BP__c;
}
declare module "@salesforce/schema/Account.Business_Name__c" {
  const Business_Name__c:string;
  export default Business_Name__c;
}
declare module "@salesforce/schema/Account.Province_RP__c" {
  const Province_RP__c:string;
  export default Province_RP__c;
}
declare module "@salesforce/schema/Account.Province_BS__c" {
  const Province_BS__c:string;
  export default Province_BS__c;
}
declare module "@salesforce/schema/Account.Province_BP__c" {
  const Province_BP__c:string;
  export default Province_BP__c;
}
declare module "@salesforce/schema/Account.COVID_19_Total_Outstanding__c" {
  const COVID_19_Total_Outstanding__c:number;
  export default COVID_19_Total_Outstanding__c;
}
declare module "@salesforce/schema/Account.Email_Monthly_Statements__c" {
  const Email_Monthly_Statements__c:boolean;
  export default Email_Monthly_Statements__c;
}
declare module "@salesforce/schema/Account.Qualification_Other__c" {
  const Qualification_Other__c:string;
  export default Qualification_Other__c;
}
declare module "@salesforce/schema/Account.Occupation_Other__c" {
  const Occupation_Other__c:string;
  export default Occupation_Other__c;
}
declare module "@salesforce/schema/Account.Emigrated__c" {
  const Emigrated__c:boolean;
  export default Emigrated__c;
}
declare module "@salesforce/schema/Account.Date_Status_Updated__c" {
  const Date_Status_Updated__c:any;
  export default Date_Status_Updated__c;
}
declare module "@salesforce/schema/Account.Residential_Telephone__c" {
  const Residential_Telephone__c:string;
  export default Residential_Telephone__c;
}
declare module "@salesforce/schema/Account.Email_Business__c" {
  const Email_Business__c:string;
  export default Email_Business__c;
}
declare module "@salesforce/schema/Account.Cellphone__c" {
  const Cellphone__c:string;
  export default Cellphone__c;
}
declare module "@salesforce/schema/Account.Age__c" {
  const Age__c:number;
  export default Age__c;
}
declare module "@salesforce/schema/Account.Create_New_Household__c" {
  const Create_New_Household__c:string;
  export default Create_New_Household__c;
}
declare module "@salesforce/schema/Account.Date_of_Birth__c" {
  const Date_of_Birth__c:any;
  export default Date_of_Birth__c;
}
declare module "@salesforce/schema/Account.Household_Main_Member__c" {
  const Household_Main_Member__c:boolean;
  export default Household_Main_Member__c;
}
declare module "@salesforce/schema/Account.Pledge_History_Report__c" {
  const Pledge_History_Report__c:string;
  export default Pledge_History_Report__c;
}
declare module "@salesforce/schema/Account.School__c" {
  const School__c:string;
  export default School__c;
}
declare module "@salesforce/schema/Account.Suburb_RS__c" {
  const Suburb_RS__c:string;
  export default Suburb_RS__c;
}
declare module "@salesforce/schema/Account.View_Household_Members__c" {
  const View_Household_Members__c:string;
  export default View_Household_Members__c;
}
declare module "@salesforce/schema/Account.Email_Thank_you_letter__c" {
  const Email_Thank_you_letter__c:boolean;
  export default Email_Thank_you_letter__c;
}
declare module "@salesforce/schema/Account.Communal_Reference_Number_Old__c" {
  const Communal_Reference_Number_Old__c:string;
  export default Communal_Reference_Number_Old__c;
}
declare module "@salesforce/schema/Account.CRN_Link__c" {
  const CRN_Link__c:string;
  export default CRN_Link__c;
}
declare module "@salesforce/schema/Account.Status_Comments__c" {
  const Status_Comments__c:string;
  export default Status_Comments__c;
}
declare module "@salesforce/schema/Account.Month_of_Emigration__c" {
  const Month_of_Emigration__c:string;
  export default Month_of_Emigration__c;
}
declare module "@salesforce/schema/Account.Previous_two_years_Pledge__c" {
  const Previous_two_years_Pledge__c:number;
  export default Previous_two_years_Pledge__c;
}
declare module "@salesforce/schema/Account.Special_Campaign_Opt_Out__c" {
  const Special_Campaign_Opt_Out__c:string;
  export default Special_Campaign_Opt_Out__c;
}
declare module "@salesforce/schema/Account.Last_Friends_Pledge_Amount__c" {
  const Last_Friends_Pledge_Amount__c:number;
  export default Last_Friends_Pledge_Amount__c;
}
declare module "@salesforce/schema/Account.Spouse_Name__r" {
  const Spouse_Name__r:any;
  export default Spouse_Name__r;
}
declare module "@salesforce/schema/Account.Spouse_Name__c" {
  const Spouse_Name__c:any;
  export default Spouse_Name__c;
}
declare module "@salesforce/schema/Account.Spouse_First_Name__c" {
  const Spouse_First_Name__c:string;
  export default Spouse_First_Name__c;
}
declare module "@salesforce/schema/Account.Salutation_First_Names__c" {
  const Salutation_First_Names__c:string;
  export default Salutation_First_Names__c;
}
declare module "@salesforce/schema/Account.Salutation_Formal__c" {
  const Salutation_Formal__c:string;
  export default Salutation_Formal__c;
}
declare module "@salesforce/schema/Account.Salutation_Formal_with_Initial__c" {
  const Salutation_Formal_with_Initial__c:string;
  export default Salutation_Formal_with_Initial__c;
}
declare module "@salesforce/schema/Account.Last_JC_Pledge__c" {
  const Last_JC_Pledge__c:number;
  export default Last_JC_Pledge__c;
}
declare module "@salesforce/schema/Account.Category__c" {
  const Category__c:string;
  export default Category__c;
}
declare module "@salesforce/schema/Account.Mail_Merge_Address__c" {
  const Mail_Merge_Address__c:string;
  export default Mail_Merge_Address__c;
}
declare module "@salesforce/schema/Account.TodayDate__c" {
  const TodayDate__c:string;
  export default TodayDate__c;
}
declare module "@salesforce/schema/Account.X2020_JC_IUA__c" {
  const X2020_JC_IUA__c:number;
  export default X2020_JC_IUA__c;
}
declare module "@salesforce/schema/Account.Mail_Merge_Suburb__c" {
  const Mail_Merge_Suburb__c:string;
  export default Mail_Merge_Suburb__c;
}
declare module "@salesforce/schema/Account.Mail_Merge_City__c" {
  const Mail_Merge_City__c:string;
  export default Mail_Merge_City__c;
}
declare module "@salesforce/schema/Account.Mail_Merge_Province__c" {
  const Mail_Merge_Province__c:string;
  export default Mail_Merge_Province__c;
}
declare module "@salesforce/schema/Account.Mail_Merge_Postal_Code__c" {
  const Mail_Merge_Postal_Code__c:string;
  export default Mail_Merge_Postal_Code__c;
}
declare module "@salesforce/schema/Account.Mail_Merge_Country__c" {
  const Mail_Merge_Country__c:string;
  export default Mail_Merge_Country__c;
}
declare module "@salesforce/schema/Account.Mail_Merge_Email__c" {
  const Mail_Merge_Email__c:string;
  export default Mail_Merge_Email__c;
}
declare module "@salesforce/schema/Account.Foreign_Residential_Address__c" {
  const Foreign_Residential_Address__c:string;
  export default Foreign_Residential_Address__c;
}
declare module "@salesforce/schema/Account.Suburb_FR__c" {
  const Suburb_FR__c:string;
  export default Suburb_FR__c;
}
declare module "@salesforce/schema/Account.City_FR__c" {
  const City_FR__c:string;
  export default City_FR__c;
}
declare module "@salesforce/schema/Account.State_Province_FR__c" {
  const State_Province_FR__c:string;
  export default State_Province_FR__c;
}
declare module "@salesforce/schema/Account.Country_FR__c" {
  const Country_FR__c:string;
  export default Country_FR__c;
}
declare module "@salesforce/schema/Account.Postal_Code_FR__c" {
  const Postal_Code_FR__c:string;
  export default Postal_Code_FR__c;
}
declare module "@salesforce/schema/Account.Foreign_Business_Address__c" {
  const Foreign_Business_Address__c:string;
  export default Foreign_Business_Address__c;
}
declare module "@salesforce/schema/Account.Suburb_FB__c" {
  const Suburb_FB__c:string;
  export default Suburb_FB__c;
}
declare module "@salesforce/schema/Account.City_FB__c" {
  const City_FB__c:string;
  export default City_FB__c;
}
declare module "@salesforce/schema/Account.State_Province_FB__c" {
  const State_Province_FB__c:string;
  export default State_Province_FB__c;
}
declare module "@salesforce/schema/Account.Country_FB__c" {
  const Country_FB__c:string;
  export default Country_FB__c;
}
declare module "@salesforce/schema/Account.Postal_Code_FB__c" {
  const Postal_Code_FB__c:string;
  export default Postal_Code_FB__c;
}
declare module "@salesforce/schema/Account.Business_Name_FB__c" {
  const Business_Name_FB__c:string;
  export default Business_Name_FB__c;
}
declare module "@salesforce/schema/Account.Phone_Foreign_Residential__c" {
  const Phone_Foreign_Residential__c:string;
  export default Phone_Foreign_Residential__c;
}
declare module "@salesforce/schema/Account.Phone_Foreign_Business__c" {
  const Phone_Foreign_Business__c:string;
  export default Phone_Foreign_Business__c;
}
declare module "@salesforce/schema/Account.Phone_Foreign_Cell__c" {
  const Phone_Foreign_Cell__c:string;
  export default Phone_Foreign_Cell__c;
}
declare module "@salesforce/schema/Account.Spouse_Canvass_Status__c" {
  const Spouse_Canvass_Status__c:string;
  export default Spouse_Canvass_Status__c;
}
declare module "@salesforce/schema/Account.Total2011D3__c" {
  const Total2011D3__c:string;
  export default Total2011D3__c;
}
declare module "@salesforce/schema/Account.University__c" {
  const University__c:string;
  export default University__c;
}
declare module "@salesforce/schema/Account.Trust_Found_Link__r" {
  const Trust_Found_Link__r:any;
  export default Trust_Found_Link__r;
}
declare module "@salesforce/schema/Account.Trust_Found_Link__c" {
  const Trust_Found_Link__c:any;
  export default Trust_Found_Link__c;
}
declare module "@salesforce/schema/Account.Foreign_Res_Add_2__c" {
  const Foreign_Res_Add_2__c:string;
  export default Foreign_Res_Add_2__c;
}
declare module "@salesforce/schema/Account.Incomplete_Record__c" {
  const Incomplete_Record__c:boolean;
  export default Incomplete_Record__c;
}
declare module "@salesforce/schema/Account.Address_3_RS__c" {
  const Address_3_RS__c:string;
  export default Address_3_RS__c;
}
declare module "@salesforce/schema/Account.Herzlia_Alum__c" {
  const Herzlia_Alum__c:boolean;
  export default Herzlia_Alum__c;
}
declare module "@salesforce/schema/Account.Special_Attention__c" {
  const Special_Attention__c:string;
  export default Special_Attention__c;
}
declare module "@salesforce/schema/Account.Pledge_Amount_Category__c" {
  const Pledge_Amount_Category__c:string;
  export default Pledge_Amount_Category__c;
}
declare module "@salesforce/schema/Account.Account_Number__c" {
  const Account_Number__c:string;
  export default Account_Number__c;
}
declare module "@salesforce/schema/Account.X2019_JC_IUA__c" {
  const X2019_JC_IUA__c:number;
  export default X2019_JC_IUA__c;
}
declare module "@salesforce/schema/Account.Past_Volunteer_Activities__c" {
  const Past_Volunteer_Activities__c:string;
  export default Past_Volunteer_Activities__c;
}
declare module "@salesforce/schema/Account.Total_Billable__c" {
  const Total_Billable__c:number;
  export default Total_Billable__c;
}
declare module "@salesforce/schema/Account.Next_Birthday__c" {
  const Next_Birthday__c:any;
  export default Next_Birthday__c;
}
declare module "@salesforce/schema/Account.Bank_Card_Expiry__c" {
  const Bank_Card_Expiry__c:string;
  export default Bank_Card_Expiry__c;
}
declare module "@salesforce/schema/Account.X2014_Chron_Sub__c" {
  const X2014_Chron_Sub__c:number;
  export default X2014_Chron_Sub__c;
}
declare module "@salesforce/schema/Account.X2014_Chron_Donation__c" {
  const X2014_Chron_Donation__c:number;
  export default X2014_Chron_Donation__c;
}
declare module "@salesforce/schema/Account.Counter__c" {
  const Counter__c:number;
  export default Counter__c;
}
declare module "@salesforce/schema/Account.Account__c" {
  const Account__c:string;
  export default Account__c;
}
declare module "@salesforce/schema/Account.Tax_Certificate_Sent__c" {
  const Tax_Certificate_Sent__c:string;
  export default Tax_Certificate_Sent__c;
}
declare module "@salesforce/schema/Account.Tax_Certificate_Name__c" {
  const Tax_Certificate_Name__c:string;
  export default Tax_Certificate_Name__c;
}
declare module "@salesforce/schema/Account.Linked_Individual__r" {
  const Linked_Individual__r:any;
  export default Linked_Individual__r;
}
declare module "@salesforce/schema/Account.Linked_Individual__c" {
  const Linked_Individual__c:any;
  export default Linked_Individual__c;
}
declare module "@salesforce/schema/Account.Trust_Manager_First_Name__c" {
  const Trust_Manager_First_Name__c:string;
  export default Trust_Manager_First_Name__c;
}
declare module "@salesforce/schema/Account.Trust_Manager_Last_Name__c" {
  const Trust_Manager_Last_Name__c:string;
  export default Trust_Manager_Last_Name__c;
}
declare module "@salesforce/schema/Account.Trust_Manager_Telephone_Number__c" {
  const Trust_Manager_Telephone_Number__c:string;
  export default Trust_Manager_Telephone_Number__c;
}
declare module "@salesforce/schema/Account.Trust_Manager_Email__c" {
  const Trust_Manager_Email__c:string;
  export default Trust_Manager_Email__c;
}
declare module "@salesforce/schema/Account.Spouse_No__c" {
  const Spouse_No__c:string;
  export default Spouse_No__c;
}
declare module "@salesforce/schema/Account.X2013_Chronicle_Sub__c" {
  const X2013_Chronicle_Sub__c:number;
  export default X2013_Chronicle_Sub__c;
}
declare module "@salesforce/schema/Account.X2013_Chronicle_Donation__c" {
  const X2013_Chronicle_Donation__c:number;
  export default X2013_Chronicle_Donation__c;
}
declare module "@salesforce/schema/Account.Statement_Explanation__c" {
  const Statement_Explanation__c:string;
  export default Statement_Explanation__c;
}
declare module "@salesforce/schema/Account.X2012_Chronicle_Sub__c" {
  const X2012_Chronicle_Sub__c:number;
  export default X2012_Chronicle_Sub__c;
}
declare module "@salesforce/schema/Account.X2011_Chronicle_Sub__c" {
  const X2011_Chronicle_Sub__c:number;
  export default X2011_Chronicle_Sub__c;
}
declare module "@salesforce/schema/Account.Synagogue__c" {
  const Synagogue__c:string;
  export default Synagogue__c;
}
declare module "@salesforce/schema/Account.DonorCategory__c" {
  const DonorCategory__c:string;
  export default DonorCategory__c;
}
declare module "@salesforce/schema/Account.Send_Birthday_Email_Check__c" {
  const Send_Birthday_Email_Check__c:boolean;
  export default Send_Birthday_Email_Check__c;
}
declare module "@salesforce/schema/Account.Verified__c" {
  const Verified__c:boolean;
  export default Verified__c;
}
declare module "@salesforce/schema/Account.X2018_NCFR_Pledge_Total__c" {
  const X2018_NCFR_Pledge_Total__c:number;
  export default X2018_NCFR_Pledge_Total__c;
}
declare module "@salesforce/schema/Account.Telethon_Volunteer__c" {
  const Telethon_Volunteer__c:string;
  export default Telethon_Volunteer__c;
}
declare module "@salesforce/schema/Account.Telethon_More_Info__c" {
  const Telethon_More_Info__c:string;
  export default Telethon_More_Info__c;
}
declare module "@salesforce/schema/Account.Telethon_Requested_More__c" {
  const Telethon_Requested_More__c:boolean;
  export default Telethon_Requested_More__c;
}
declare module "@salesforce/schema/Account.Special_per__c" {
  const Special_per__c:string;
  export default Special_per__c;
}
declare module "@salesforce/schema/Account.Canvass_Info__c" {
  const Canvass_Info__c:string;
  export default Canvass_Info__c;
}
declare module "@salesforce/schema/Account.Total_Cancelled__c" {
  const Total_Cancelled__c:number;
  export default Total_Cancelled__c;
}
declare module "@salesforce/schema/Account.Notes2014__c" {
  const Notes2014__c:string;
  export default Notes2014__c;
}
declare module "@salesforce/schema/Account.Bank_Name__c" {
  const Bank_Name__c:string;
  export default Bank_Name__c;
}
declare module "@salesforce/schema/Account.Credit_Card_CVV__c" {
  const Credit_Card_CVV__c:string;
  export default Credit_Card_CVV__c;
}
declare module "@salesforce/schema/Account.Total_Pledge_History__c" {
  const Total_Pledge_History__c:number;
  export default Total_Pledge_History__c;
}
declare module "@salesforce/schema/Account.BULK_SMS_Cell__c" {
  const BULK_SMS_Cell__c:string;
  export default BULK_SMS_Cell__c;
}
declare module "@salesforce/schema/Account.Year_of_Herzlia_Grad__c" {
  const Year_of_Herzlia_Grad__c:number;
  export default Year_of_Herzlia_Grad__c;
}
declare module "@salesforce/schema/Account.Online_Statement__c" {
  const Online_Statement__c:string;
  export default Online_Statement__c;
}
declare module "@salesforce/schema/Account.Bulk_SMS_Opt_Out__c" {
  const Bulk_SMS_Opt_Out__c:boolean;
  export default Bulk_SMS_Opt_Out__c;
}
declare module "@salesforce/schema/Account.Notes__c" {
  const Notes__c:string;
  export default Notes__c;
}
declare module "@salesforce/schema/Account.X2015_IUA__c" {
  const X2015_IUA__c:number;
  export default X2015_IUA__c;
}
declare module "@salesforce/schema/Account.X2015_UCF_Comm__c" {
  const X2015_UCF_Comm__c:number;
  export default X2015_UCF_Comm__c;
}
declare module "@salesforce/schema/Account.X2015_UCF_Ed__c" {
  const X2015_UCF_Ed__c:number;
  export default X2015_UCF_Ed__c;
}
declare module "@salesforce/schema/Account.X2015_Welfare__c" {
  const X2015_Welfare__c:number;
  export default X2015_Welfare__c;
}
declare module "@salesforce/schema/Account.X2015_Refusal_Count__c" {
  const X2015_Refusal_Count__c:number;
  export default X2015_Refusal_Count__c;
}
declare module "@salesforce/schema/Account.X2015_Cancelled__c" {
  const X2015_Cancelled__c:number;
  export default X2015_Cancelled__c;
}
declare module "@salesforce/schema/Account.X2015_Pledge_Total__c" {
  const X2015_Pledge_Total__c:number;
  export default X2015_Pledge_Total__c;
}
declare module "@salesforce/schema/Account.X2015_JC_Pledge_Total__c" {
  const X2015_JC_Pledge_Total__c:number;
  export default X2015_JC_Pledge_Total__c;
}
declare module "@salesforce/schema/Account.X2015_Balance_Outstanding__c" {
  const X2015_Balance_Outstanding__c:number;
  export default X2015_Balance_Outstanding__c;
}
declare module "@salesforce/schema/Account.Salutation_First_Last_Names__c" {
  const Salutation_First_Last_Names__c:string;
  export default Salutation_First_Last_Names__c;
}
declare module "@salesforce/schema/Account.Mail_Merge_Workflow_Test__c" {
  const Mail_Merge_Workflow_Test__c:string;
  export default Mail_Merge_Workflow_Test__c;
}
declare module "@salesforce/schema/Account.X2014_JC_Pledge_Total__c" {
  const X2014_JC_Pledge_Total__c:number;
  export default X2014_JC_Pledge_Total__c;
}
declare module "@salesforce/schema/Account.NonJC_Pledge_History__c" {
  const NonJC_Pledge_History__c:number;
  export default NonJC_Pledge_History__c;
}
declare module "@salesforce/schema/Account.Salutation_Initials_LastName__c" {
  const Salutation_Initials_LastName__c:string;
  export default Salutation_Initials_LastName__c;
}
declare module "@salesforce/schema/Account.FirstName__c" {
  const FirstName__c:string;
  export default FirstName__c;
}
declare module "@salesforce/schema/Account.X2019_JC_UCF_Comm__c" {
  const X2019_JC_UCF_Comm__c:number;
  export default X2019_JC_UCF_Comm__c;
}
declare module "@salesforce/schema/Account.X2019_JC_UCF_Ed__c" {
  const X2019_JC_UCF_Ed__c:number;
  export default X2019_JC_UCF_Ed__c;
}
declare module "@salesforce/schema/Account.X2019_JC_UCF_Total__c" {
  const X2019_JC_UCF_Total__c:number;
  export default X2019_JC_UCF_Total__c;
}
declare module "@salesforce/schema/Account.X2019_JC_Welfare__c" {
  const X2019_JC_Welfare__c:number;
  export default X2019_JC_Welfare__c;
}
declare module "@salesforce/schema/Account.X2019_JC_Pledge_Total__c" {
  const X2019_JC_Pledge_Total__c:number;
  export default X2019_JC_Pledge_Total__c;
}
declare module "@salesforce/schema/Account.District__c" {
  const District__c:string;
  export default District__c;
}
declare module "@salesforce/schema/Account.Salutation_Thank_You__c" {
  const Salutation_Thank_You__c:string;
  export default Salutation_Thank_You__c;
}
declare module "@salesforce/schema/Account.Lion_of_Judah__c" {
  const Lion_of_Judah__c:string;
  export default Lion_of_Judah__c;
}
declare module "@salesforce/schema/Account.Trust_Type__c" {
  const Trust_Type__c:string;
  export default Trust_Type__c;
}
declare module "@salesforce/schema/Account.Last_Friends_Pledge_Year__c" {
  const Last_Friends_Pledge_Year__c:string;
  export default Last_Friends_Pledge_Year__c;
}
declare module "@salesforce/schema/Account.X2019_NCF_Pledge_Total__c" {
  const X2019_NCF_Pledge_Total__c:number;
  export default X2019_NCF_Pledge_Total__c;
}
declare module "@salesforce/schema/Account.Last_JC_Pledge_Amount__c" {
  const Last_JC_Pledge_Amount__c:number;
  export default Last_JC_Pledge_Amount__c;
}
declare module "@salesforce/schema/Account.X2019_IUA__c" {
  const X2019_IUA__c:number;
  export default X2019_IUA__c;
}
declare module "@salesforce/schema/Account.X2019_UCF_Comm__c" {
  const X2019_UCF_Comm__c:number;
  export default X2019_UCF_Comm__c;
}
declare module "@salesforce/schema/Account.Last_JC_Pledge_Year__c" {
  const Last_JC_Pledge_Year__c:string;
  export default Last_JC_Pledge_Year__c;
}
declare module "@salesforce/schema/Account.X2019_UCF_Ed__c" {
  const X2019_UCF_Ed__c:number;
  export default X2019_UCF_Ed__c;
}
declare module "@salesforce/schema/Account.Highlands_House_Resident__c" {
  const Highlands_House_Resident__c:boolean;
  export default Highlands_House_Resident__c;
}
declare module "@salesforce/schema/Account.New_Donor__c" {
  const New_Donor__c:boolean;
  export default New_Donor__c;
}
declare module "@salesforce/schema/Account.Number_of_JC_Pledges__c" {
  const Number_of_JC_Pledges__c:number;
  export default Number_of_JC_Pledges__c;
}
declare module "@salesforce/schema/Account.Fountain_Donor__c" {
  const Fountain_Donor__c:boolean;
  export default Fountain_Donor__c;
}
declare module "@salesforce/schema/Account.X2020_JC_Welfare__c" {
  const X2020_JC_Welfare__c:number;
  export default X2020_JC_Welfare__c;
}
declare module "@salesforce/schema/Account.COVID_19_Pledge__c" {
  const COVID_19_Pledge__c:number;
  export default COVID_19_Pledge__c;
}
declare module "@salesforce/schema/Account.BuTruSp_COVID_19_Pledge__c" {
  const BuTruSp_COVID_19_Pledge__c:number;
  export default BuTruSp_COVID_19_Pledge__c;
}
declare module "@salesforce/schema/Account.COVID_19_Tax_Cert__c" {
  const COVID_19_Tax_Cert__c:boolean;
  export default COVID_19_Tax_Cert__c;
}
declare module "@salesforce/schema/Account.Last_Friends_Pledge__c" {
  const Last_Friends_Pledge__c:number;
  export default Last_Friends_Pledge__c;
}
declare module "@salesforce/schema/Account.Debut_Pledge_Date__c" {
  const Debut_Pledge_Date__c:any;
  export default Debut_Pledge_Date__c;
}
declare module "@salesforce/schema/Account.X2020_Refusal_Count__c" {
  const X2020_Refusal_Count__c:number;
  export default X2020_Refusal_Count__c;
}
declare module "@salesforce/schema/Account.FOUJC__c" {
  const FOUJC__c:string;
  export default FOUJC__c;
}
declare module "@salesforce/schema/Account.X2019_UCF_Total__c" {
  const X2019_UCF_Total__c:number;
  export default X2019_UCF_Total__c;
}
declare module "@salesforce/schema/Account.OP_Total_Outstanding_2016__c" {
  const OP_Total_Outstanding_2016__c:number;
  export default OP_Total_Outstanding_2016__c;
}
declare module "@salesforce/schema/Account.OP_Total_Outstanding_2017__c" {
  const OP_Total_Outstanding_2017__c:number;
  export default OP_Total_Outstanding_2017__c;
}
declare module "@salesforce/schema/Account.OP_Total_Outstanding_2015__c" {
  const OP_Total_Outstanding_2015__c:number;
  export default OP_Total_Outstanding_2015__c;
}
declare module "@salesforce/schema/Account.OP_Pledge_Total_2016__c" {
  const OP_Pledge_Total_2016__c:number;
  export default OP_Pledge_Total_2016__c;
}
declare module "@salesforce/schema/Account.OP_Pledge_Total_2017__c" {
  const OP_Pledge_Total_2017__c:number;
  export default OP_Pledge_Total_2017__c;
}
declare module "@salesforce/schema/Account.X2020_JC_UCF_Ed__c" {
  const X2020_JC_UCF_Ed__c:number;
  export default X2020_JC_UCF_Ed__c;
}
declare module "@salesforce/schema/Account.OP_Pledge_Total_2015__c" {
  const OP_Pledge_Total_2015__c:number;
  export default OP_Pledge_Total_2015__c;
}
declare module "@salesforce/schema/Account.X2016_IUA__c" {
  const X2016_IUA__c:number;
  export default X2016_IUA__c;
}
declare module "@salesforce/schema/Account.X2016_UCF_Comm__c" {
  const X2016_UCF_Comm__c:number;
  export default X2016_UCF_Comm__c;
}
declare module "@salesforce/schema/Account.X2016_UCF_Ed__c" {
  const X2016_UCF_Ed__c:number;
  export default X2016_UCF_Ed__c;
}
declare module "@salesforce/schema/Account.X2016_Welfare__c" {
  const X2016_Welfare__c:number;
  export default X2016_Welfare__c;
}
declare module "@salesforce/schema/Account.X2016_Refusal_Count__c" {
  const X2016_Refusal_Count__c:number;
  export default X2016_Refusal_Count__c;
}
declare module "@salesforce/schema/Account.X2016_Cancelled__c" {
  const X2016_Cancelled__c:number;
  export default X2016_Cancelled__c;
}
declare module "@salesforce/schema/Account.X2016_Pledge_Total__c" {
  const X2016_Pledge_Total__c:number;
  export default X2016_Pledge_Total__c;
}
declare module "@salesforce/schema/Account.X2018_BuTruSp_Total__c" {
  const X2018_BuTruSp_Total__c:number;
  export default X2018_BuTruSp_Total__c;
}
declare module "@salesforce/schema/Account.X2016_Balance_Outstanding__c" {
  const X2016_Balance_Outstanding__c:number;
  export default X2016_Balance_Outstanding__c;
}
declare module "@salesforce/schema/Account.X2016_JC_Pledge_Total__c" {
  const X2016_JC_Pledge_Total__c:number;
  export default X2016_JC_Pledge_Total__c;
}
declare module "@salesforce/schema/Account.Debut_Pledge_Age__c" {
  const Debut_Pledge_Age__c:number;
  export default Debut_Pledge_Age__c;
}
declare module "@salesforce/schema/Account.X2020_JC_Giving__c" {
  const X2020_JC_Giving__c:number;
  export default X2020_JC_Giving__c;
}
declare module "@salesforce/schema/Account.X2020_BuTrusp_IUA__c" {
  const X2020_BuTrusp_IUA__c:number;
  export default X2020_BuTrusp_IUA__c;
}
declare module "@salesforce/schema/Account.X2019_Welfare__c" {
  const X2019_Welfare__c:number;
  export default X2019_Welfare__c;
}
declare module "@salesforce/schema/Account.X2019_BuTruSp_IUA__c" {
  const X2019_BuTruSp_IUA__c:number;
  export default X2019_BuTruSp_IUA__c;
}
declare module "@salesforce/schema/Account.X2019_Pledge_Total__c" {
  const X2019_Pledge_Total__c:number;
  export default X2019_Pledge_Total__c;
}
declare module "@salesforce/schema/Account.X2018_BuTruSp_IUA__c" {
  const X2018_BuTruSp_IUA__c:number;
  export default X2018_BuTruSp_IUA__c;
}
declare module "@salesforce/schema/Account.JC2015_IUA__c" {
  const JC2015_IUA__c:number;
  export default JC2015_IUA__c;
}
declare module "@salesforce/schema/Account.JC2015_UCFCom__c" {
  const JC2015_UCFCom__c:number;
  export default JC2015_UCFCom__c;
}
declare module "@salesforce/schema/Account.JC2015_UCFEd__c" {
  const JC2015_UCFEd__c:number;
  export default JC2015_UCFEd__c;
}
declare module "@salesforce/schema/Account.JC2015_Welf__c" {
  const JC2015_Welf__c:number;
  export default JC2015_Welf__c;
}
declare module "@salesforce/schema/Account.JC2016_IUA__c" {
  const JC2016_IUA__c:number;
  export default JC2016_IUA__c;
}
declare module "@salesforce/schema/Account.JC2016_UCFCom__c" {
  const JC2016_UCFCom__c:number;
  export default JC2016_UCFCom__c;
}
declare module "@salesforce/schema/Account.JC2016_UCFEd__c" {
  const JC2016_UCFEd__c:number;
  export default JC2016_UCFEd__c;
}
declare module "@salesforce/schema/Account.JC2016_Welf__c" {
  const JC2016_Welf__c:number;
  export default JC2016_Welf__c;
}
declare module "@salesforce/schema/Account.X2019_Balance_Outstanding__c" {
  const X2019_Balance_Outstanding__c:number;
  export default X2019_Balance_Outstanding__c;
}
declare module "@salesforce/schema/Account.X2019_Cancelled__c" {
  const X2019_Cancelled__c:number;
  export default X2019_Cancelled__c;
}
declare module "@salesforce/schema/Account.X2019_JC_Giving__c" {
  const X2019_JC_Giving__c:number;
  export default X2019_JC_Giving__c;
}
declare module "@salesforce/schema/Account.Age_Grouping__c" {
  const Age_Grouping__c:string;
  export default Age_Grouping__c;
}
declare module "@salesforce/schema/Account.JC_UCF_Total_2016__c" {
  const JC_UCF_Total_2016__c:number;
  export default JC_UCF_Total_2016__c;
}
declare module "@salesforce/schema/Account.X2016_Chronicle_Sub__c" {
  const X2016_Chronicle_Sub__c:number;
  export default X2016_Chronicle_Sub__c;
}
declare module "@salesforce/schema/Account.X2017_Chronicle_Sub__c" {
  const X2017_Chronicle_Sub__c:number;
  export default X2017_Chronicle_Sub__c;
}
declare module "@salesforce/schema/Account.X2018_Chronicle_Sub__c" {
  const X2018_Chronicle_Sub__c:number;
  export default X2018_Chronicle_Sub__c;
}
declare module "@salesforce/schema/Account.X2019_Chronicle_Sub__c" {
  const X2019_Chronicle_Sub__c:number;
  export default X2019_Chronicle_Sub__c;
}
declare module "@salesforce/schema/Account.X2020_Chronicle_Sub__c" {
  const X2020_Chronicle_Sub__c:number;
  export default X2020_Chronicle_Sub__c;
}
declare module "@salesforce/schema/Account.X2015_Chronicle_Sub__c" {
  const X2015_Chronicle_Sub__c:number;
  export default X2015_Chronicle_Sub__c;
}
declare module "@salesforce/schema/Account.X2015_Chron_Donation__c" {
  const X2015_Chron_Donation__c:number;
  export default X2015_Chron_Donation__c;
}
declare module "@salesforce/schema/Account.X2016_Chron_Donation__c" {
  const X2016_Chron_Donation__c:number;
  export default X2016_Chron_Donation__c;
}
declare module "@salesforce/schema/Account.X2017_Chron_Donation__c" {
  const X2017_Chron_Donation__c:number;
  export default X2017_Chron_Donation__c;
}
declare module "@salesforce/schema/Account.X2018_Chron_Donation__c" {
  const X2018_Chron_Donation__c:number;
  export default X2018_Chron_Donation__c;
}
declare module "@salesforce/schema/Account.Email_Receipt_Thank_you_letter__c" {
  const Email_Receipt_Thank_you_letter__c:boolean;
  export default Email_Receipt_Thank_you_letter__c;
}
declare module "@salesforce/schema/Account.X2017_Welfare__c" {
  const X2017_Welfare__c:number;
  export default X2017_Welfare__c;
}
declare module "@salesforce/schema/Account.X2nd_Most_recent_Modified_by__c" {
  const X2nd_Most_recent_Modified_by__c:string;
  export default X2nd_Most_recent_Modified_by__c;
}
declare module "@salesforce/schema/Account.X3rd_Most_recent_Modified_by__c" {
  const X3rd_Most_recent_Modified_by__c:string;
  export default X3rd_Most_recent_Modified_by__c;
}
declare module "@salesforce/schema/Account.X4th_Most_recent_Modified_by__c" {
  const X4th_Most_recent_Modified_by__c:string;
  export default X4th_Most_recent_Modified_by__c;
}
declare module "@salesforce/schema/Account.X5th_Most_recent_Modified_by__c" {
  const X5th_Most_recent_Modified_by__c:string;
  export default X5th_Most_recent_Modified_by__c;
}
declare module "@salesforce/schema/Account.Marriage_Status__c" {
  const Marriage_Status__c:string;
  export default Marriage_Status__c;
}
declare module "@salesforce/schema/Account.JC2017_Welf__c" {
  const JC2017_Welf__c:number;
  export default JC2017_Welf__c;
}
declare module "@salesforce/schema/Account.JC2017_UCFEd__c" {
  const JC2017_UCFEd__c:number;
  export default JC2017_UCFEd__c;
}
declare module "@salesforce/schema/Account.JC2017_UCFCom__c" {
  const JC2017_UCFCom__c:number;
  export default JC2017_UCFCom__c;
}
declare module "@salesforce/schema/Account.X2017_UCF_Ed__c" {
  const X2017_UCF_Ed__c:number;
  export default X2017_UCF_Ed__c;
}
declare module "@salesforce/schema/Account.X2017_UCF_Comm__c" {
  const X2017_UCF_Comm__c:number;
  export default X2017_UCF_Comm__c;
}
declare module "@salesforce/schema/Account.X2017_Refusal_Count__c" {
  const X2017_Refusal_Count__c:number;
  export default X2017_Refusal_Count__c;
}
declare module "@salesforce/schema/Account.X2019_File_per_Campaign__c" {
  const X2019_File_per_Campaign__c:number;
  export default X2019_File_per_Campaign__c;
}
declare module "@salesforce/schema/Account.X2017_Pledge_Total__c" {
  const X2017_Pledge_Total__c:number;
  export default X2017_Pledge_Total__c;
}
declare module "@salesforce/schema/Account.X2018_JC_Giving__c" {
  const X2018_JC_Giving__c:number;
  export default X2018_JC_Giving__c;
}
declare module "@salesforce/schema/Account.X2017_JC_Pledge_Total__c" {
  const X2017_JC_Pledge_Total__c:number;
  export default X2017_JC_Pledge_Total__c;
}
declare module "@salesforce/schema/Account.X2017_IUA__c" {
  const X2017_IUA__c:number;
  export default X2017_IUA__c;
}
declare module "@salesforce/schema/Account.JC2017_IUA__c" {
  const JC2017_IUA__c:number;
  export default JC2017_IUA__c;
}
declare module "@salesforce/schema/Account.X2017_JC_Giving__c" {
  const X2017_JC_Giving__c:number;
  export default X2017_JC_Giving__c;
}
declare module "@salesforce/schema/Account.X2019_Refusal_Count__c" {
  const X2019_Refusal_Count__c:number;
  export default X2019_Refusal_Count__c;
}
declare module "@salesforce/schema/Account.Top_Donor__c" {
  const Top_Donor__c:boolean;
  export default Top_Donor__c;
}
declare module "@salesforce/schema/Account.X2017_Cancelled__c" {
  const X2017_Cancelled__c:number;
  export default X2017_Cancelled__c;
}
declare module "@salesforce/schema/Account.X2017_Balance_Outstanding__c" {
  const X2017_Balance_Outstanding__c:number;
  export default X2017_Balance_Outstanding__c;
}
declare module "@salesforce/schema/Account.X2019_JC_OP__c" {
  const X2019_JC_OP__c:number;
  export default X2019_JC_OP__c;
}
declare module "@salesforce/schema/Account.Age_at_Last_JC_Pledge__c" {
  const Age_at_Last_JC_Pledge__c:number;
  export default Age_at_Last_JC_Pledge__c;
}
declare module "@salesforce/schema/Account.Total_UCF_2017__c" {
  const Total_UCF_2017__c:number;
  export default Total_UCF_2017__c;
}
declare module "@salesforce/schema/Account.X2017_Percentage__c" {
  const X2017_Percentage__c:number;
  export default X2017_Percentage__c;
}
declare module "@salesforce/schema/Account.Spouse_Thank_you__c" {
  const Spouse_Thank_you__c:boolean;
  export default Spouse_Thank_you__c;
}
declare module "@salesforce/schema/Account.Husband_Email__c" {
  const Husband_Email__c:boolean;
  export default Husband_Email__c;
}
declare module "@salesforce/schema/Account.Total_Giving__c" {
  const Total_Giving__c:number;
  export default Total_Giving__c;
}
declare module "@salesforce/schema/Account.JC_UCF_Total_2017__c" {
  const JC_UCF_Total_2017__c:number;
  export default JC_UCF_Total_2017__c;
}
declare module "@salesforce/schema/Account.Not_Jewish__c" {
  const Not_Jewish__c:boolean;
  export default Not_Jewish__c;
}
declare module "@salesforce/schema/Account.X2017_NCFR_Pledge_Total__c" {
  const X2017_NCFR_Pledge_Total__c:number;
  export default X2017_NCFR_Pledge_Total__c;
}
declare module "@salesforce/schema/Account.Maiden_Name__c" {
  const Maiden_Name__c:string;
  export default Maiden_Name__c;
}
declare module "@salesforce/schema/Account.X2018_Balance_Outstanding__c" {
  const X2018_Balance_Outstanding__c:number;
  export default X2018_Balance_Outstanding__c;
}
declare module "@salesforce/schema/Account.X2018_Cancelled__c" {
  const X2018_Cancelled__c:number;
  export default X2018_Cancelled__c;
}
declare module "@salesforce/schema/Account.X2018_UCF_Comm__c" {
  const X2018_UCF_Comm__c:number;
  export default X2018_UCF_Comm__c;
}
declare module "@salesforce/schema/Account.X2018_UCF_Ed__c" {
  const X2018_UCF_Ed__c:number;
  export default X2018_UCF_Ed__c;
}
declare module "@salesforce/schema/Account.Last_NCF_Pledge_Date__c" {
  const Last_NCF_Pledge_Date__c:any;
  export default Last_NCF_Pledge_Date__c;
}
declare module "@salesforce/schema/Account.OP_Total_Outstanding_2019__c" {
  const OP_Total_Outstanding_2019__c:number;
  export default OP_Total_Outstanding_2019__c;
}
declare module "@salesforce/schema/Account.X2018_JC_IUA__c" {
  const X2018_JC_IUA__c:number;
  export default X2018_JC_IUA__c;
}
declare module "@salesforce/schema/Account.X2018_IUA__c" {
  const X2018_IUA__c:number;
  export default X2018_IUA__c;
}
declare module "@salesforce/schema/Account.X2018_JC_Pledge_Total__c" {
  const X2018_JC_Pledge_Total__c:number;
  export default X2018_JC_Pledge_Total__c;
}
declare module "@salesforce/schema/Account.X2020_JC_Pledge_Total__c" {
  const X2020_JC_Pledge_Total__c:number;
  export default X2020_JC_Pledge_Total__c;
}
declare module "@salesforce/schema/Account.X2018_Pledge_Total__c" {
  const X2018_Pledge_Total__c:number;
  export default X2018_Pledge_Total__c;
}
declare module "@salesforce/schema/Account.X2018_Refusal_Count__c" {
  const X2018_Refusal_Count__c:number;
  export default X2018_Refusal_Count__c;
}
declare module "@salesforce/schema/Account.X2018_JC_UCF_Comm__c" {
  const X2018_JC_UCF_Comm__c:number;
  export default X2018_JC_UCF_Comm__c;
}
declare module "@salesforce/schema/Account.X2018_JC_UCF_Ed__c" {
  const X2018_JC_UCF_Ed__c:number;
  export default X2018_JC_UCF_Ed__c;
}
declare module "@salesforce/schema/Account.X2018_JC_Welfare__c" {
  const X2018_JC_Welfare__c:number;
  export default X2018_JC_Welfare__c;
}
declare module "@salesforce/schema/Account.X2018_Welfare__c" {
  const X2018_Welfare__c:number;
  export default X2018_Welfare__c;
}
declare module "@salesforce/schema/Account.X2017_BuTruSp_Total__c" {
  const X2017_BuTruSp_Total__c:number;
  export default X2017_BuTruSp_Total__c;
}
declare module "@salesforce/schema/Account.X2018_Percentage_Increase__c" {
  const X2018_Percentage_Increase__c:number;
  export default X2018_Percentage_Increase__c;
}
declare module "@salesforce/schema/Account.X2016_Percentage__c" {
  const X2016_Percentage__c:number;
  export default X2016_Percentage__c;
}
declare module "@salesforce/schema/Account.X2018_UCF_Total__c" {
  const X2018_UCF_Total__c:number;
  export default X2018_UCF_Total__c;
}
declare module "@salesforce/schema/Account.X2018_JC_UCF_Total__c" {
  const X2018_JC_UCF_Total__c:number;
  export default X2018_JC_UCF_Total__c;
}
declare module "@salesforce/schema/Account.OP_Pledge_Total_2018__c" {
  const OP_Pledge_Total_2018__c:number;
  export default OP_Pledge_Total_2018__c;
}
declare module "@salesforce/schema/Account.OP_Total_Outstanding_2018__c" {
  const OP_Total_Outstanding_2018__c:number;
  export default OP_Total_Outstanding_2018__c;
}
declare module "@salesforce/schema/Account.Chronicle_Notes__c" {
  const Chronicle_Notes__c:string;
  export default Chronicle_Notes__c;
}
declare module "@salesforce/schema/Account.X2019_NCFR_Pledge_Total__c" {
  const X2019_NCFR_Pledge_Total__c:number;
  export default X2019_NCFR_Pledge_Total__c;
}
declare module "@salesforce/schema/Account.X2019_BuTruSp_Total__c" {
  const X2019_BuTruSp_Total__c:number;
  export default X2019_BuTruSp_Total__c;
}
declare module "@salesforce/schema/Account.X2016_BuTruSp_Total__c" {
  const X2016_BuTruSp_Total__c:number;
  export default X2016_BuTruSp_Total__c;
}
declare module "@salesforce/schema/Account.X2017_NCF_Pledge_Total__c" {
  const X2017_NCF_Pledge_Total__c:number;
  export default X2017_NCF_Pledge_Total__c;
}
declare module "@salesforce/schema/Account.X2018_NCF_Pledge_Total__c" {
  const X2018_NCF_Pledge_Total__c:number;
  export default X2018_NCF_Pledge_Total__c;
}
declare module "@salesforce/schema/Account.X2016_NCFR_Pledge_Total__c" {
  const X2016_NCFR_Pledge_Total__c:number;
  export default X2016_NCFR_Pledge_Total__c;
}
declare module "@salesforce/schema/Account.Last_Life_Cycle_Event__c" {
  const Last_Life_Cycle_Event__c:any;
  export default Last_Life_Cycle_Event__c;
}
declare module "@salesforce/schema/Account.X2014_Refusal_Count__c" {
  const X2014_Refusal_Count__c:number;
  export default X2014_Refusal_Count__c;
}
declare module "@salesforce/schema/Account.X2013_Refusal_Count__c" {
  const X2013_Refusal_Count__c:number;
  export default X2013_Refusal_Count__c;
}
declare module "@salesforce/schema/Account.X2012_Refusal_Count__c" {
  const X2012_Refusal_Count__c:number;
  export default X2012_Refusal_Count__c;
}
declare module "@salesforce/schema/Account.Total_Refusals__c" {
  const Total_Refusals__c:number;
  export default Total_Refusals__c;
}
declare module "@salesforce/schema/Account.X2013_File_per_Campaign__c" {
  const X2013_File_per_Campaign__c:number;
  export default X2013_File_per_Campaign__c;
}
declare module "@salesforce/schema/Account.X2014_File_per_Campaign__c" {
  const X2014_File_per_Campaign__c:number;
  export default X2014_File_per_Campaign__c;
}
declare module "@salesforce/schema/Account.X2015_File_per_Campaign__c" {
  const X2015_File_per_Campaign__c:number;
  export default X2015_File_per_Campaign__c;
}
declare module "@salesforce/schema/Account.X2016_File_per_Campaign__c" {
  const X2016_File_per_Campaign__c:number;
  export default X2016_File_per_Campaign__c;
}
declare module "@salesforce/schema/Account.X2017_File_per_Campaign__c" {
  const X2017_File_per_Campaign__c:number;
  export default X2017_File_per_Campaign__c;
}
declare module "@salesforce/schema/Account.X2018_File_per_Campaign__c" {
  const X2018_File_per_Campaign__c:number;
  export default X2018_File_per_Campaign__c;
}
declare module "@salesforce/schema/Account.X2015_NCFR_Pledge_Total__c" {
  const X2015_NCFR_Pledge_Total__c:number;
  export default X2015_NCFR_Pledge_Total__c;
}
declare module "@salesforce/schema/Account.X2019_Percentage__c" {
  const X2019_Percentage__c:number;
  export default X2019_Percentage__c;
}
declare module "@salesforce/schema/Account.X2020_NCF_Pledge_Total__c" {
  const X2020_NCF_Pledge_Total__c:number;
  export default X2020_NCF_Pledge_Total__c;
}
declare module "@salesforce/schema/Account.X2020_NCFR_Pledge_Total__c" {
  const X2020_NCFR_Pledge_Total__c:number;
  export default X2020_NCFR_Pledge_Total__c;
}
declare module "@salesforce/schema/Account.X2020_BuTruSp_Total__c" {
  const X2020_BuTruSp_Total__c:number;
  export default X2020_BuTruSp_Total__c;
}
declare module "@salesforce/schema/Account.X2020_Percentage__c" {
  const X2020_Percentage__c:number;
  export default X2020_Percentage__c;
}
declare module "@salesforce/schema/Account.X2020_JC_OP_Total__c" {
  const X2020_JC_OP_Total__c:number;
  export default X2020_JC_OP_Total__c;
}
declare module "@salesforce/schema/Account.Last_NCFR_Pledge_Date__c" {
  const Last_NCFR_Pledge_Date__c:any;
  export default Last_NCFR_Pledge_Date__c;
}
declare module "@salesforce/schema/Account.X2020_JC_UCF_Comm__c" {
  const X2020_JC_UCF_Comm__c:number;
  export default X2020_JC_UCF_Comm__c;
}
declare module "@salesforce/schema/Account.Don_Cat__c" {
  const Don_Cat__c:string;
  export default Don_Cat__c;
}
declare module "@salesforce/schema/Account.X2020_File_per_Campaign__c" {
  const X2020_File_per_Campaign__c:number;
  export default X2020_File_per_Campaign__c;
}
declare module "@salesforce/schema/Account.X2020_JC_OP_Total_Outstanding__c" {
  const X2020_JC_OP_Total_Outstanding__c:number;
  export default X2020_JC_OP_Total_Outstanding__c;
}
declare module "@salesforce/schema/Account.X2020_Pledge_Total__c" {
  const X2020_Pledge_Total__c:number;
  export default X2020_Pledge_Total__c;
}
declare module "@salesforce/schema/Account.X2017_NCFR_Giving__c" {
  const X2017_NCFR_Giving__c:number;
  export default X2017_NCFR_Giving__c;
}
declare module "@salesforce/schema/Account.X2018_NCFR_Giving__c" {
  const X2018_NCFR_Giving__c:number;
  export default X2018_NCFR_Giving__c;
}
declare module "@salesforce/schema/Account.X2019_NCFR_Giving__c" {
  const X2019_NCFR_Giving__c:number;
  export default X2019_NCFR_Giving__c;
}
declare module "@salesforce/schema/Account.X2020_NCFR_Giving__c" {
  const X2020_NCFR_Giving__c:number;
  export default X2020_NCFR_Giving__c;
}
declare module "@salesforce/schema/Account.Last_JC_Canvass_Method__c" {
  const Last_JC_Canvass_Method__c:string;
  export default Last_JC_Canvass_Method__c;
}
declare module "@salesforce/schema/Account.Last_JC_Canvasser_s__c" {
  const Last_JC_Canvasser_s__c:string;
  export default Last_JC_Canvasser_s__c;
}
declare module "@salesforce/schema/Account.ckt__Opt_Out__pc" {
  const ckt__Opt_Out__pc:boolean;
  export default ckt__Opt_Out__pc;
}
declare module "@salesforce/schema/Account.tecnics__Next_Birth_Day__pc" {
  const tecnics__Next_Birth_Day__pc:any;
  export default tecnics__Next_Birth_Day__pc;
}
declare module "@salesforce/schema/Account.MC4SF__MC_Subscriber__pr" {
  const MC4SF__MC_Subscriber__pr:any;
  export default MC4SF__MC_Subscriber__pr;
}
declare module "@salesforce/schema/Account.MC4SF__MC_Subscriber__pc" {
  const MC4SF__MC_Subscriber__pc:any;
  export default MC4SF__MC_Subscriber__pc;
}
declare module "@salesforce/schema/Account.CnP_PaaS__Alias_Contact_Data__pc" {
  const CnP_PaaS__Alias_Contact_Data__pc:string;
  export default CnP_PaaS__Alias_Contact_Data__pc;
}
declare module "@salesforce/schema/Account.CnP_PaaS__CnP_Connect_Alias_Index__pc" {
  const CnP_PaaS__CnP_Connect_Alias_Index__pc:string;
  export default CnP_PaaS__CnP_Connect_Alias_Index__pc;
}
declare module "@salesforce/schema/Account.CnP_PaaS__CnP_Global_Rank__pc" {
  const CnP_PaaS__CnP_Global_Rank__pc:number;
  export default CnP_PaaS__CnP_Global_Rank__pc;
}
declare module "@salesforce/schema/Account.CnP_PaaS__CnP_Total_Intrinsic__pc" {
  const CnP_PaaS__CnP_Total_Intrinsic__pc:number;
  export default CnP_PaaS__CnP_Total_Intrinsic__pc;
}
declare module "@salesforce/schema/Account.CnP_PaaS__CnP_Total_extrinsic__pc" {
  const CnP_PaaS__CnP_Total_extrinsic__pc:number;
  export default CnP_PaaS__CnP_Total_extrinsic__pc;
}
declare module "@salesforce/schema/Account.CnP_PaaS__Connect_Link__pc" {
  const CnP_PaaS__Connect_Link__pc:string;
  export default CnP_PaaS__Connect_Link__pc;
}
