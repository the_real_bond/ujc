declare module "@salesforce/schema/APXTConga4__Conga_Solution_Export_Environment__mdt.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Export_Environment__mdt.DeveloperName" {
  const DeveloperName:string;
  export default DeveloperName;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Export_Environment__mdt.MasterLabel" {
  const MasterLabel:string;
  export default MasterLabel;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Export_Environment__mdt.Language" {
  const Language:string;
  export default Language;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Export_Environment__mdt.NamespacePrefix" {
  const NamespacePrefix:string;
  export default NamespacePrefix;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Export_Environment__mdt.Label" {
  const Label:string;
  export default Label;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Export_Environment__mdt.QualifiedApiName" {
  const QualifiedApiName:string;
  export default QualifiedApiName;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Export_Environment__mdt.APXTConga4__Export_Endpoint__c" {
  const APXTConga4__Export_Endpoint__c:string;
  export default APXTConga4__Export_Endpoint__c;
}
