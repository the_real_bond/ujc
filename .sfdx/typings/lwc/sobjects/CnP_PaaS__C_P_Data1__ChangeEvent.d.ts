declare module "@salesforce/schema/CnP_PaaS__C_P_Data1__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Data1__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Data1__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Data1__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Data1__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Data1__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Data1__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Data1__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Data1__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Data1__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Data1__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Data1__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Data1__ChangeEvent.CnP_PaaS__CnP_Data_ID__c" {
  const CnP_PaaS__CnP_Data_ID__c:string;
  export default CnP_PaaS__CnP_Data_ID__c;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Data1__ChangeEvent.CnP_PaaS__Contact__c" {
  const CnP_PaaS__Contact__c:any;
  export default CnP_PaaS__Contact__c;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Data1__ChangeEvent.CnP_PaaS__DataXML__c" {
  const CnP_PaaS__DataXML__c:string;
  export default CnP_PaaS__DataXML__c;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Data1__ChangeEvent.CnP_PaaS__Email__c" {
  const CnP_PaaS__Email__c:boolean;
  export default CnP_PaaS__Email__c;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Data1__ChangeEvent.CnP_PaaS__Message__c" {
  const CnP_PaaS__Message__c:string;
  export default CnP_PaaS__Message__c;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Data1__ChangeEvent.CnP_PaaS__Name__c" {
  const CnP_PaaS__Name__c:string;
  export default CnP_PaaS__Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Data1__ChangeEvent.CnP_PaaS__Order_Number__c" {
  const CnP_PaaS__Order_Number__c:string;
  export default CnP_PaaS__Order_Number__c;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Data1__ChangeEvent.CnP_PaaS__Status_ID__c" {
  const CnP_PaaS__Status_ID__c:number;
  export default CnP_PaaS__Status_ID__c;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Data1__ChangeEvent.CnP_PaaS__Total_Charged__c" {
  const CnP_PaaS__Total_Charged__c:number;
  export default CnP_PaaS__Total_Charged__c;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Data1__ChangeEvent.CnP_PaaS__Transaction_Result__c" {
  const CnP_PaaS__Transaction_Result__c:string;
  export default CnP_PaaS__Transaction_Result__c;
}
