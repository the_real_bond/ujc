declare module "@salesforce/schema/PowerLoader__QueryPanelTemplateField__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplateField__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplateField__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplateField__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplateField__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplateField__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplateField__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplateField__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplateField__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplateField__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplateField__ChangeEvent.PowerLoader__QueryPanelTemplate__c" {
  const PowerLoader__QueryPanelTemplate__c:any;
  export default PowerLoader__QueryPanelTemplate__c;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplateField__ChangeEvent.PowerLoader__Value__c" {
  const PowerLoader__Value__c:string;
  export default PowerLoader__Value__c;
}
