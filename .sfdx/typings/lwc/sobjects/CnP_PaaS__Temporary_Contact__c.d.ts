declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__c.CnP_PaaS__Amount__c" {
  const CnP_PaaS__Amount__c:number;
  export default CnP_PaaS__Amount__c;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__c.CnP_PaaS__C_P_Data__r" {
  const CnP_PaaS__C_P_Data__r:any;
  export default CnP_PaaS__C_P_Data__r;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__c.CnP_PaaS__C_P_Data__c" {
  const CnP_PaaS__C_P_Data__c:any;
  export default CnP_PaaS__C_P_Data__c;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__c.CnP_PaaS__Email__c" {
  const CnP_PaaS__Email__c:string;
  export default CnP_PaaS__Email__c;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__c.CnP_PaaS__First_Name__c" {
  const CnP_PaaS__First_Name__c:string;
  export default CnP_PaaS__First_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__c.CnP_PaaS__Installment__c" {
  const CnP_PaaS__Installment__c:number;
  export default CnP_PaaS__Installment__c;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__c.CnP_PaaS__Last_Name__c" {
  const CnP_PaaS__Last_Name__c:string;
  export default CnP_PaaS__Last_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__c.CnP_PaaS__Map_Contact__r" {
  const CnP_PaaS__Map_Contact__r:any;
  export default CnP_PaaS__Map_Contact__r;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__c.CnP_PaaS__Map_Contact__c" {
  const CnP_PaaS__Map_Contact__c:any;
  export default CnP_PaaS__Map_Contact__c;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__c.CnP_PaaS__Master_Transaction_Number__c" {
  const CnP_PaaS__Master_Transaction_Number__c:string;
  export default CnP_PaaS__Master_Transaction_Number__c;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__c.CnP_PaaS__OrderNumber__c" {
  const CnP_PaaS__OrderNumber__c:string;
  export default CnP_PaaS__OrderNumber__c;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__c.CnP_PaaS__Recurring__c" {
  const CnP_PaaS__Recurring__c:string;
  export default CnP_PaaS__Recurring__c;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__c.CnP_PaaS__Xmldata__c" {
  const CnP_PaaS__Xmldata__c:string;
  export default CnP_PaaS__Xmldata__c;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__c.CnP_PaaS_EVT__Event_ID__r" {
  const CnP_PaaS_EVT__Event_ID__r:any;
  export default CnP_PaaS_EVT__Event_ID__r;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__c.CnP_PaaS_EVT__Event_ID__c" {
  const CnP_PaaS_EVT__Event_ID__c:any;
  export default CnP_PaaS_EVT__Event_ID__c;
}
