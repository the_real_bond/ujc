declare module "@salesforce/schema/vr__VR_Email_History_Lead__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/vr__VR_Email_History_Lead__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/vr__VR_Email_History_Lead__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/vr__VR_Email_History_Lead__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/vr__VR_Email_History_Lead__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/vr__VR_Email_History_Lead__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/vr__VR_Email_History_Lead__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/vr__VR_Email_History_Lead__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/vr__VR_Email_History_Lead__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/vr__VR_Email_History_Lead__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/vr__VR_Email_History_Lead__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/vr__VR_Email_History_Lead__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/vr__VR_Email_History_Lead__ChangeEvent.vr__Bounced__c" {
  const vr__Bounced__c:boolean;
  export default vr__Bounced__c;
}
declare module "@salesforce/schema/vr__VR_Email_History_Lead__ChangeEvent.vr__Campaign_Hash__c" {
  const vr__Campaign_Hash__c:string;
  export default vr__Campaign_Hash__c;
}
declare module "@salesforce/schema/vr__VR_Email_History_Lead__ChangeEvent.vr__Clicked_Links__c" {
  const vr__Clicked_Links__c:string;
  export default vr__Clicked_Links__c;
}
declare module "@salesforce/schema/vr__VR_Email_History_Lead__ChangeEvent.vr__Clicked__c" {
  const vr__Clicked__c:boolean;
  export default vr__Clicked__c;
}
declare module "@salesforce/schema/vr__VR_Email_History_Lead__ChangeEvent.vr__Company_Hash__c" {
  const vr__Company_Hash__c:string;
  export default vr__Company_Hash__c;
}
declare module "@salesforce/schema/vr__VR_Email_History_Lead__ChangeEvent.vr__Email_ID__c" {
  const vr__Email_ID__c:string;
  export default vr__Email_ID__c;
}
declare module "@salesforce/schema/vr__VR_Email_History_Lead__ChangeEvent.vr__Email_Type__c" {
  const vr__Email_Type__c:string;
  export default vr__Email_Type__c;
}
declare module "@salesforce/schema/vr__VR_Email_History_Lead__ChangeEvent.vr__Lead__c" {
  const vr__Lead__c:any;
  export default vr__Lead__c;
}
declare module "@salesforce/schema/vr__VR_Email_History_Lead__ChangeEvent.vr__List_Type__c" {
  const vr__List_Type__c:string;
  export default vr__List_Type__c;
}
declare module "@salesforce/schema/vr__VR_Email_History_Lead__ChangeEvent.vr__Mail_Date__c" {
  const vr__Mail_Date__c:any;
  export default vr__Mail_Date__c;
}
declare module "@salesforce/schema/vr__VR_Email_History_Lead__ChangeEvent.vr__Opened__c" {
  const vr__Opened__c:boolean;
  export default vr__Opened__c;
}
declare module "@salesforce/schema/vr__VR_Email_History_Lead__ChangeEvent.vr__Sent__c" {
  const vr__Sent__c:boolean;
  export default vr__Sent__c;
}
declare module "@salesforce/schema/vr__VR_Email_History_Lead__ChangeEvent.vr__Unsubscribed__c" {
  const vr__Unsubscribed__c:boolean;
  export default vr__Unsubscribed__c;
}
