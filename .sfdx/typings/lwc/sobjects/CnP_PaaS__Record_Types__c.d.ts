declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Account_Record_type__c" {
  const CnP_PaaS__Account_Record_type__c:string;
  export default CnP_PaaS__Account_Record_type__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Account_WID_Condition__c" {
  const CnP_PaaS__Account_WID_Condition__c:string;
  export default CnP_PaaS__Account_WID_Condition__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Account_WID__c" {
  const CnP_PaaS__Account_WID__c:string;
  export default CnP_PaaS__Account_WID__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Account__c" {
  const CnP_PaaS__Account__c:boolean;
  export default CnP_PaaS__Account__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Assign_Task__c" {
  const CnP_PaaS__Assign_Task__c:string;
  export default CnP_PaaS__Assign_Task__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Campaign_Condition__c" {
  const CnP_PaaS__Campaign_Condition__c:string;
  export default CnP_PaaS__Campaign_Condition__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Campaign_Sku__c" {
  const CnP_PaaS__Campaign_Sku__c:string;
  export default CnP_PaaS__Campaign_Sku__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Campaign__c" {
  const CnP_PaaS__Campaign__c:string;
  export default CnP_PaaS__Campaign__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Campaign_lp__r" {
  const CnP_PaaS__Campaign_lp__r:any;
  export default CnP_PaaS__Campaign_lp__r;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Campaign_lp__c" {
  const CnP_PaaS__Campaign_lp__c:any;
  export default CnP_PaaS__Campaign_lp__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Campaigns__c" {
  const CnP_PaaS__Campaigns__c:boolean;
  export default CnP_PaaS__Campaigns__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Condition__c" {
  const CnP_PaaS__Condition__c:string;
  export default CnP_PaaS__Condition__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Connect_Setting__c" {
  const CnP_PaaS__Connect_Setting__c:boolean;
  export default CnP_PaaS__Connect_Setting__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__ContactId_Task__r" {
  const CnP_PaaS__ContactId_Task__r:any;
  export default CnP_PaaS__ContactId_Task__r;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__ContactId_Task__c" {
  const CnP_PaaS__ContactId_Task__c:any;
  export default CnP_PaaS__ContactId_Task__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Contact_Id__c" {
  const CnP_PaaS__Contact_Id__c:string;
  export default CnP_PaaS__Contact_Id__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Contact_Name_Task__c" {
  const CnP_PaaS__Contact_Name_Task__c:string;
  export default CnP_PaaS__Contact_Name_Task__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Contact_Role__c" {
  const CnP_PaaS__Contact_Role__c:boolean;
  export default CnP_PaaS__Contact_Role__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Contact_role_condition__c" {
  const CnP_PaaS__Contact_role_condition__c:string;
  export default CnP_PaaS__Contact_role_condition__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Contact_role_name__c" {
  const CnP_PaaS__Contact_role_name__c:string;
  export default CnP_PaaS__Contact_role_name__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Contacts__c" {
  const CnP_PaaS__Contacts__c:boolean;
  export default CnP_PaaS__Contacts__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Custom_Answer__c" {
  const CnP_PaaS__Custom_Answer__c:string;
  export default CnP_PaaS__Custom_Answer__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Custom_Questions__c" {
  const CnP_PaaS__Custom_Questions__c:boolean;
  export default CnP_PaaS__Custom_Questions__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Custom_Text__c" {
  const CnP_PaaS__Custom_Text__c:string;
  export default CnP_PaaS__Custom_Text__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Default_Items_Edit_Option_XML__c" {
  const CnP_PaaS__Default_Items_Edit_Option_XML__c:string;
  export default CnP_PaaS__Default_Items_Edit_Option_XML__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Donation_Campaign__c" {
  const CnP_PaaS__Donation_Campaign__c:string;
  export default CnP_PaaS__Donation_Campaign__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Donation_Check__c" {
  const CnP_PaaS__Donation_Check__c:boolean;
  export default CnP_PaaS__Donation_Check__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Donation_Pay__c" {
  const CnP_PaaS__Donation_Pay__c:string;
  export default CnP_PaaS__Donation_Pay__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Donation_Quantity__c" {
  const CnP_PaaS__Donation_Quantity__c:string;
  export default CnP_PaaS__Donation_Quantity__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Donation_SKU__c" {
  const CnP_PaaS__Donation_SKU__c:string;
  export default CnP_PaaS__Donation_SKU__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Email_Task__c" {
  const CnP_PaaS__Email_Task__c:string;
  export default CnP_PaaS__Email_Task__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__First_Name_Separator__c" {
  const CnP_PaaS__First_Name_Separator__c:string;
  export default CnP_PaaS__First_Name_Separator__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Firstname_Prefix__c" {
  const CnP_PaaS__Firstname_Prefix__c:string;
  export default CnP_PaaS__Firstname_Prefix__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Ledger_Account_Name__c" {
  const CnP_PaaS__Ledger_Account_Name__c:string;
  export default CnP_PaaS__Ledger_Account_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Ledger_Campaign__r" {
  const CnP_PaaS__Ledger_Campaign__r:any;
  export default CnP_PaaS__Ledger_Campaign__r;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Ledger_Campaign__c" {
  const CnP_PaaS__Ledger_Campaign__c:any;
  export default CnP_PaaS__Ledger_Campaign__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Ledger_Condition__c" {
  const CnP_PaaS__Ledger_Condition__c:string;
  export default CnP_PaaS__Ledger_Condition__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Ledgers__c" {
  const CnP_PaaS__Ledgers__c:boolean;
  export default CnP_PaaS__Ledgers__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Mandatory_question__c" {
  const CnP_PaaS__Mandatory_question__c:boolean;
  export default CnP_PaaS__Mandatory_question__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Message_Task__c" {
  const CnP_PaaS__Message_Task__c:string;
  export default CnP_PaaS__Message_Task__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Notifier_Task__c" {
  const CnP_PaaS__Notifier_Task__c:string;
  export default CnP_PaaS__Notifier_Task__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Opportunities__c" {
  const CnP_PaaS__Opportunities__c:boolean;
  export default CnP_PaaS__Opportunities__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Opportunity_Conditions__c" {
  const CnP_PaaS__Opportunity_Conditions__c:string;
  export default CnP_PaaS__Opportunity_Conditions__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Opportunity_Name__c" {
  const CnP_PaaS__Opportunity_Name__c:boolean;
  export default CnP_PaaS__Opportunity_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Opportunity_Record_type__c" {
  const CnP_PaaS__Opportunity_Record_type__c:string;
  export default CnP_PaaS__Opportunity_Record_type__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Primary_Contact_Role__c" {
  const CnP_PaaS__Primary_Contact_Role__c:boolean;
  export default CnP_PaaS__Primary_Contact_Role__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Priority__c" {
  const CnP_PaaS__Priority__c:string;
  export default CnP_PaaS__Priority__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Product_Condition__c" {
  const CnP_PaaS__Product_Condition__c:string;
  export default CnP_PaaS__Product_Condition__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Product_Record_Type__c" {
  const CnP_PaaS__Product_Record_Type__c:string;
  export default CnP_PaaS__Product_Record_Type__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Product_Sku__c" {
  const CnP_PaaS__Product_Sku__c:string;
  export default CnP_PaaS__Product_Sku__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Products__c" {
  const CnP_PaaS__Products__c:boolean;
  export default CnP_PaaS__Products__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Question__c" {
  const CnP_PaaS__Question__c:string;
  export default CnP_PaaS__Question__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Record_Type__c" {
  const CnP_PaaS__Record_Type__c:string;
  export default CnP_PaaS__Record_Type__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Remainder_Task__c" {
  const CnP_PaaS__Remainder_Task__c:boolean;
  export default CnP_PaaS__Remainder_Task__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Role_SKU__c" {
  const CnP_PaaS__Role_SKU__c:string;
  export default CnP_PaaS__Role_SKU__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__SKU_Condition_Connect__c" {
  const CnP_PaaS__SKU_Condition_Connect__c:string;
  export default CnP_PaaS__SKU_Condition_Connect__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__SKU_Text__c" {
  const CnP_PaaS__SKU_Text__c:string;
  export default CnP_PaaS__SKU_Text__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__SKU_value__c" {
  const CnP_PaaS__SKU_value__c:string;
  export default CnP_PaaS__SKU_value__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__SecondName_Prefix__c" {
  const CnP_PaaS__SecondName_Prefix__c:string;
  export default CnP_PaaS__SecondName_Prefix__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__SecondName_Separator__c" {
  const CnP_PaaS__SecondName_Separator__c:string;
  export default CnP_PaaS__SecondName_Separator__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Sku__c" {
  const CnP_PaaS__Sku__c:string;
  export default CnP_PaaS__Sku__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Status_Task__c" {
  const CnP_PaaS__Status_Task__c:string;
  export default CnP_PaaS__Status_Task__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Task__c" {
  const CnP_PaaS__Task__c:boolean;
  export default CnP_PaaS__Task__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Tax_Deductible__c" {
  const CnP_PaaS__Tax_Deductible__c:number;
  export default CnP_PaaS__Tax_Deductible__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__ThirdName_Prefix__c" {
  const CnP_PaaS__ThirdName_Prefix__c:string;
  export default CnP_PaaS__ThirdName_Prefix__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__ThirdName_Separator__c" {
  const CnP_PaaS__ThirdName_Separator__c:string;
  export default CnP_PaaS__ThirdName_Separator__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Unit_Discount__c" {
  const CnP_PaaS__Unit_Discount__c:number;
  export default CnP_PaaS__Unit_Discount__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Unit_Item__c" {
  const CnP_PaaS__Unit_Item__c:number;
  export default CnP_PaaS__Unit_Item__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Unit_Shipping__c" {
  const CnP_PaaS__Unit_Shipping__c:number;
  export default CnP_PaaS__Unit_Shipping__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__Unit_Tax__c" {
  const CnP_PaaS__Unit_Tax__c:number;
  export default CnP_PaaS__Unit_Tax__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__UserID_Task__r" {
  const CnP_PaaS__UserID_Task__r:any;
  export default CnP_PaaS__UserID_Task__r;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__UserID_Task__c" {
  const CnP_PaaS__UserID_Task__c:any;
  export default CnP_PaaS__UserID_Task__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__User_name_Task__c" {
  const CnP_PaaS__User_name_Task__c:string;
  export default CnP_PaaS__User_name_Task__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__WID_Condition__c" {
  const CnP_PaaS__WID_Condition__c:string;
  export default CnP_PaaS__WID_Condition__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__WID__c" {
  const CnP_PaaS__WID__c:string;
  export default CnP_PaaS__WID__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__ledger_sku_text__c" {
  const CnP_PaaS__ledger_sku_text__c:string;
  export default CnP_PaaS__ledger_sku_text__c;
}
declare module "@salesforce/schema/CnP_PaaS__Record_Types__c.CnP_PaaS__remainder_days_task__c" {
  const CnP_PaaS__remainder_days_task__c:string;
  export default CnP_PaaS__remainder_days_task__c;
}
