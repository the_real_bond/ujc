declare module "@salesforce/schema/APXTConga4__Conga_Solution_Template__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Template__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Template__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Template__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Template__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Template__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Template__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Template__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Template__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Template__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Template__c.APXTConga4__Conga_Solution__r" {
  const APXTConga4__Conga_Solution__r:any;
  export default APXTConga4__Conga_Solution__r;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Template__c.APXTConga4__Conga_Solution__c" {
  const APXTConga4__Conga_Solution__c:any;
  export default APXTConga4__Conga_Solution__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Template__c.APXTConga4__Comments__c" {
  const APXTConga4__Comments__c:string;
  export default APXTConga4__Comments__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Template__c.APXTConga4__Conga_Template__r" {
  const APXTConga4__Conga_Template__r:any;
  export default APXTConga4__Conga_Template__r;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Template__c.APXTConga4__Conga_Template__c" {
  const APXTConga4__Conga_Template__c:any;
  export default APXTConga4__Conga_Template__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Template__c.APXTConga4__Output_File_Name__c" {
  const APXTConga4__Output_File_Name__c:string;
  export default APXTConga4__Output_File_Name__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Template__c.APXTConga4__Sort_Order__c" {
  const APXTConga4__Sort_Order__c:number;
  export default APXTConga4__Sort_Order__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Template__c.APXTConga4__Template_Extension__c" {
  const APXTConga4__Template_Extension__c:string;
  export default APXTConga4__Template_Extension__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Template__c.APXTConga4__Template_Group__c" {
  const APXTConga4__Template_Group__c:string;
  export default APXTConga4__Template_Group__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Template__c.APXTConga4__Template_Name__c" {
  const APXTConga4__Template_Name__c:string;
  export default APXTConga4__Template_Name__c;
}
