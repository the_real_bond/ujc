declare module "@salesforce/schema/APXT_CMTR__Conga_Merge_Template__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/APXT_CMTR__Conga_Merge_Template__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/APXT_CMTR__Conga_Merge_Template__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/APXT_CMTR__Conga_Merge_Template__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/APXT_CMTR__Conga_Merge_Template__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/APXT_CMTR__Conga_Merge_Template__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/APXT_CMTR__Conga_Merge_Template__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/APXT_CMTR__Conga_Merge_Template__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/APXT_CMTR__Conga_Merge_Template__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/APXT_CMTR__Conga_Merge_Template__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/APXT_CMTR__Conga_Merge_Template__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/APXT_CMTR__Conga_Merge_Template__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/APXT_CMTR__Conga_Merge_Template__ChangeEvent.APXT_CMTR__Description__c" {
  const APXT_CMTR__Description__c:string;
  export default APXT_CMTR__Description__c;
}
declare module "@salesforce/schema/APXT_CMTR__Conga_Merge_Template__ChangeEvent.APXT_CMTR__Label_Template_Use_Detail_Data__c" {
  const APXT_CMTR__Label_Template_Use_Detail_Data__c:boolean;
  export default APXT_CMTR__Label_Template_Use_Detail_Data__c;
}
declare module "@salesforce/schema/APXT_CMTR__Conga_Merge_Template__ChangeEvent.APXT_CMTR__Master_Field_to_Set_1__c" {
  const APXT_CMTR__Master_Field_to_Set_1__c:string;
  export default APXT_CMTR__Master_Field_to_Set_1__c;
}
declare module "@salesforce/schema/APXT_CMTR__Conga_Merge_Template__ChangeEvent.APXT_CMTR__Master_Field_to_Set_2__c" {
  const APXT_CMTR__Master_Field_to_Set_2__c:string;
  export default APXT_CMTR__Master_Field_to_Set_2__c;
}
declare module "@salesforce/schema/APXT_CMTR__Conga_Merge_Template__ChangeEvent.APXT_CMTR__Master_Field_to_Set_3__c" {
  const APXT_CMTR__Master_Field_to_Set_3__c:string;
  export default APXT_CMTR__Master_Field_to_Set_3__c;
}
declare module "@salesforce/schema/APXT_CMTR__Conga_Merge_Template__ChangeEvent.APXT_CMTR__Name__c" {
  const APXT_CMTR__Name__c:string;
  export default APXT_CMTR__Name__c;
}
declare module "@salesforce/schema/APXT_CMTR__Conga_Merge_Template__ChangeEvent.APXT_CMTR__Template_Group__c" {
  const APXT_CMTR__Template_Group__c:string;
  export default APXT_CMTR__Template_Group__c;
}
declare module "@salesforce/schema/APXT_CMTR__Conga_Merge_Template__ChangeEvent.APXT_CMTR__Template_Type__c" {
  const APXT_CMTR__Template_Type__c:string;
  export default APXT_CMTR__Template_Type__c;
}
