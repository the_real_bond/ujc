declare module "@salesforce/schema/Household__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Household__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Household__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Household__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Household__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Household__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Household__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Household__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Household__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Household__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Household__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Household__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Household__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/Household__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Household__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Household__c.Communal_Reference_Number__c" {
  const Communal_Reference_Number__c:string;
  export default Communal_Reference_Number__c;
}
declare module "@salesforce/schema/Household__c.Old_Register_Area_R__c" {
  const Old_Register_Area_R__c:string;
  export default Old_Register_Area_R__c;
}
declare module "@salesforce/schema/Household__c.Household_Counter__c" {
  const Household_Counter__c:number;
  export default Household_Counter__c;
}
declare module "@salesforce/schema/Household__c.City_RP__c" {
  const City_RP__c:string;
  export default City_RP__c;
}
declare module "@salesforce/schema/Household__c.City_RS__c" {
  const City_RS__c:string;
  export default City_RS__c;
}
declare module "@salesforce/schema/Household__c.Country_RP__c" {
  const Country_RP__c:string;
  export default Country_RP__c;
}
declare module "@salesforce/schema/Household__c.Country_RS__c" {
  const Country_RS__c:string;
  export default Country_RS__c;
}
declare module "@salesforce/schema/Household__c.Email_Residential__c" {
  const Email_Residential__c:string;
  export default Email_Residential__c;
}
declare module "@salesforce/schema/Household__c.Postal_Code_RP__c" {
  const Postal_Code_RP__c:string;
  export default Postal_Code_RP__c;
}
declare module "@salesforce/schema/Household__c.Postal_Code_RS__c" {
  const Postal_Code_RS__c:string;
  export default Postal_Code_RS__c;
}
declare module "@salesforce/schema/Household__c.Province_RP__c" {
  const Province_RP__c:string;
  export default Province_RP__c;
}
declare module "@salesforce/schema/Household__c.Province_RS__c" {
  const Province_RS__c:string;
  export default Province_RS__c;
}
declare module "@salesforce/schema/Household__c.Residential_Address_RS__c" {
  const Residential_Address_RS__c:string;
  export default Residential_Address_RS__c;
}
declare module "@salesforce/schema/Household__c.Residential_Postal_Address_RP__c" {
  const Residential_Postal_Address_RP__c:string;
  export default Residential_Postal_Address_RP__c;
}
declare module "@salesforce/schema/Household__c.Residential_Telephone__c" {
  const Residential_Telephone__c:string;
  export default Residential_Telephone__c;
}
declare module "@salesforce/schema/Household__c.Suburb_RP__c" {
  const Suburb_RP__c:string;
  export default Suburb_RP__c;
}
declare module "@salesforce/schema/Household__c.Suburb_RS__c" {
  const Suburb_RS__c:string;
  export default Suburb_RS__c;
}
