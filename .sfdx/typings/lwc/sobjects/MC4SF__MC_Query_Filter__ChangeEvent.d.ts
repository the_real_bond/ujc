declare module "@salesforce/schema/MC4SF__MC_Query_Filter__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/MC4SF__MC_Query_Filter__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/MC4SF__MC_Query_Filter__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/MC4SF__MC_Query_Filter__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/MC4SF__MC_Query_Filter__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/MC4SF__MC_Query_Filter__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/MC4SF__MC_Query_Filter__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/MC4SF__MC_Query_Filter__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/MC4SF__MC_Query_Filter__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/MC4SF__MC_Query_Filter__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/MC4SF__MC_Query_Filter__ChangeEvent.MC4SF__MC_Query__c" {
  const MC4SF__MC_Query__c:any;
  export default MC4SF__MC_Query__c;
}
declare module "@salesforce/schema/MC4SF__MC_Query_Filter__ChangeEvent.MC4SF__Display_Order__c" {
  const MC4SF__Display_Order__c:number;
  export default MC4SF__Display_Order__c;
}
declare module "@salesforce/schema/MC4SF__MC_Query_Filter__ChangeEvent.MC4SF__Error_Message__c" {
  const MC4SF__Error_Message__c:string;
  export default MC4SF__Error_Message__c;
}
declare module "@salesforce/schema/MC4SF__MC_Query_Filter__ChangeEvent.MC4SF__Field_Name__c" {
  const MC4SF__Field_Name__c:string;
  export default MC4SF__Field_Name__c;
}
declare module "@salesforce/schema/MC4SF__MC_Query_Filter__ChangeEvent.MC4SF__Field_Type__c" {
  const MC4SF__Field_Type__c:string;
  export default MC4SF__Field_Type__c;
}
declare module "@salesforce/schema/MC4SF__MC_Query_Filter__ChangeEvent.MC4SF__Object_Name__c" {
  const MC4SF__Object_Name__c:string;
  export default MC4SF__Object_Name__c;
}
declare module "@salesforce/schema/MC4SF__MC_Query_Filter__ChangeEvent.MC4SF__Operator__c" {
  const MC4SF__Operator__c:string;
  export default MC4SF__Operator__c;
}
declare module "@salesforce/schema/MC4SF__MC_Query_Filter__ChangeEvent.MC4SF__Value__c" {
  const MC4SF__Value__c:string;
  export default MC4SF__Value__c;
}
