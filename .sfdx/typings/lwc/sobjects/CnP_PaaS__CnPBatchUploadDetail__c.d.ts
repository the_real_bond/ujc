declare module "@salesforce/schema/CnP_PaaS__CnPBatchUploadDetail__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUploadDetail__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUploadDetail__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUploadDetail__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUploadDetail__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUploadDetail__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUploadDetail__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUploadDetail__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUploadDetail__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUploadDetail__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUploadDetail__c.CnP_PaaS__CnPBatchUploadID__r" {
  const CnP_PaaS__CnPBatchUploadID__r:any;
  export default CnP_PaaS__CnPBatchUploadID__r;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUploadDetail__c.CnP_PaaS__CnPBatchUploadID__c" {
  const CnP_PaaS__CnPBatchUploadID__c:any;
  export default CnP_PaaS__CnPBatchUploadID__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUploadDetail__c.CnP_PaaS__CnPRecordContent__c" {
  const CnP_PaaS__CnPRecordContent__c:string;
  export default CnP_PaaS__CnPRecordContent__c;
}
