declare module "@salesforce/schema/CnP_PaaS_EVT__Questionsection__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Questionsection__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Questionsection__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Questionsection__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Questionsection__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Questionsection__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Questionsection__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Questionsection__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Questionsection__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Questionsection__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Questionsection__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Questionsection__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Questionsection__c.CnP_PaaS_EVT__Event_name__r" {
  const CnP_PaaS_EVT__Event_name__r:any;
  export default CnP_PaaS_EVT__Event_name__r;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Questionsection__c.CnP_PaaS_EVT__Event_name__c" {
  const CnP_PaaS_EVT__Event_name__c:any;
  export default CnP_PaaS_EVT__Event_name__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Questionsection__c.CnP_PaaS_EVT__Registration_Level__r" {
  const CnP_PaaS_EVT__Registration_Level__r:any;
  export default CnP_PaaS_EVT__Registration_Level__r;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Questionsection__c.CnP_PaaS_EVT__Registration_Level__c" {
  const CnP_PaaS_EVT__Registration_Level__c:any;
  export default CnP_PaaS_EVT__Registration_Level__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Questionsection__c.CnP_PaaS_EVT__Section_Order__c" {
  const CnP_PaaS_EVT__Section_Order__c:number;
  export default CnP_PaaS_EVT__Section_Order__c;
}
