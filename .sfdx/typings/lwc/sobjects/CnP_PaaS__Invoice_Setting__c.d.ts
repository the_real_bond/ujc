declare module "@salesforce/schema/CnP_PaaS__Invoice_Setting__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Setting__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Setting__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Setting__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Setting__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Setting__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Setting__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Setting__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Setting__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Setting__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Setting__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Setting__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Setting__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Setting__c.CnP_PaaS__Counter__c" {
  const CnP_PaaS__Counter__c:string;
  export default CnP_PaaS__Counter__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Setting__c.CnP_PaaS__Counter_hidden__c" {
  const CnP_PaaS__Counter_hidden__c:string;
  export default CnP_PaaS__Counter_hidden__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Setting__c.CnP_PaaS__DateValue1__c" {
  const CnP_PaaS__DateValue1__c:string;
  export default CnP_PaaS__DateValue1__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Setting__c.CnP_PaaS__DateValue__c" {
  const CnP_PaaS__DateValue__c:string;
  export default CnP_PaaS__DateValue__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Setting__c.CnP_PaaS__Date_Format__c" {
  const CnP_PaaS__Date_Format__c:string;
  export default CnP_PaaS__Date_Format__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Setting__c.CnP_PaaS__Incrementer__c" {
  const CnP_PaaS__Incrementer__c:string;
  export default CnP_PaaS__Incrementer__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Setting__c.CnP_PaaS__Prefix__c" {
  const CnP_PaaS__Prefix__c:string;
  export default CnP_PaaS__Prefix__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Setting__c.CnP_PaaS__Seperator1__c" {
  const CnP_PaaS__Seperator1__c:string;
  export default CnP_PaaS__Seperator1__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Setting__c.CnP_PaaS__Seperator__c" {
  const CnP_PaaS__Seperator__c:string;
  export default CnP_PaaS__Seperator__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Setting__c.CnP_PaaS__SiteUrl__c" {
  const CnP_PaaS__SiteUrl__c:string;
  export default CnP_PaaS__SiteUrl__c;
}
