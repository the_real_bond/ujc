declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__History.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__History.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__History.Parent" {
  const Parent:any;
  export default Parent;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__History.ParentId" {
  const ParentId:any;
  export default ParentId;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__History.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__History.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__History.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__History.Field" {
  const Field:string;
  export default Field;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__History.OldValue" {
  const OldValue:any;
  export default OldValue;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__History.NewValue" {
  const NewValue:any;
  export default NewValue;
}
