declare module "@salesforce/schema/CnP_PaaS_EVT__Checkin_Checkout__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Checkin_Checkout__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Checkin_Checkout__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Checkin_Checkout__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Checkin_Checkout__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Checkin_Checkout__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Checkin_Checkout__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Checkin_Checkout__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Checkin_Checkout__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Checkin_Checkout__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Checkin_Checkout__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Checkin_Checkout__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Checkin_Checkout__ChangeEvent.CnP_PaaS_EVT__CheckIn_Checkout_Time__c" {
  const CnP_PaaS_EVT__CheckIn_Checkout_Time__c:any;
  export default CnP_PaaS_EVT__CheckIn_Checkout_Time__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Checkin_Checkout__ChangeEvent.CnP_PaaS_EVT__Check_Status__c" {
  const CnP_PaaS_EVT__Check_Status__c:string;
  export default CnP_PaaS_EVT__Check_Status__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Checkin_Checkout__ChangeEvent.CnP_PaaS_EVT__Contact__c" {
  const CnP_PaaS_EVT__Contact__c:any;
  export default CnP_PaaS_EVT__Contact__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Checkin_Checkout__ChangeEvent.CnP_PaaS_EVT__Device_Address__c" {
  const CnP_PaaS_EVT__Device_Address__c:string;
  export default CnP_PaaS_EVT__Device_Address__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Checkin_Checkout__ChangeEvent.CnP_PaaS_EVT__Device__c" {
  const CnP_PaaS_EVT__Device__c:any;
  export default CnP_PaaS_EVT__Device__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Checkin_Checkout__ChangeEvent.CnP_PaaS_EVT__User_ID__c" {
  const CnP_PaaS_EVT__User_ID__c:string;
  export default CnP_PaaS_EVT__User_ID__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Checkin_Checkout__ChangeEvent.CnP_PaaS_EVT__attendee_name__c" {
  const CnP_PaaS_EVT__attendee_name__c:any;
  export default CnP_PaaS_EVT__attendee_name__c;
}
