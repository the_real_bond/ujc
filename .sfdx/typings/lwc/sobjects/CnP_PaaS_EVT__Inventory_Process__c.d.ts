declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__c.CnP_PaaS_EVT__C_P_Event_Registration_Level__r" {
  const CnP_PaaS_EVT__C_P_Event_Registration_Level__r:any;
  export default CnP_PaaS_EVT__C_P_Event_Registration_Level__r;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__c.CnP_PaaS_EVT__C_P_Event_Registration_Level__c" {
  const CnP_PaaS_EVT__C_P_Event_Registration_Level__c:any;
  export default CnP_PaaS_EVT__C_P_Event_Registration_Level__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__c.CnP_PaaS_EVT__C_P_Event__r" {
  const CnP_PaaS_EVT__C_P_Event__r:any;
  export default CnP_PaaS_EVT__C_P_Event__r;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__c.CnP_PaaS_EVT__C_P_Event__c" {
  const CnP_PaaS_EVT__C_P_Event__c:any;
  export default CnP_PaaS_EVT__C_P_Event__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__c.CnP_PaaS_EVT__Cookie_Gen_Num__c" {
  const CnP_PaaS_EVT__Cookie_Gen_Num__c:number;
  export default CnP_PaaS_EVT__Cookie_Gen_Num__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__c.CnP_PaaS_EVT__Quantity__c" {
  const CnP_PaaS_EVT__Quantity__c:number;
  export default CnP_PaaS_EVT__Quantity__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__c.CnP_PaaS_EVT__UserTimeset__c" {
  const CnP_PaaS_EVT__UserTimeset__c:any;
  export default CnP_PaaS_EVT__UserTimeset__c;
}
