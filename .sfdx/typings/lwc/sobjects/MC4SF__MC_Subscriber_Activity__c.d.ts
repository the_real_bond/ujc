declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__c.MC4SF__MC_Subscriber__r" {
  const MC4SF__MC_Subscriber__r:any;
  export default MC4SF__MC_Subscriber__r;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__c.MC4SF__MC_Subscriber__c" {
  const MC4SF__MC_Subscriber__c:any;
  export default MC4SF__MC_Subscriber__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__c.MC4SF__Action__c" {
  const MC4SF__Action__c:string;
  export default MC4SF__Action__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__c.MC4SF__Bounce_Type__c" {
  const MC4SF__Bounce_Type__c:string;
  export default MC4SF__Bounce_Type__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__c.MC4SF__MC_Campaign__r" {
  const MC4SF__MC_Campaign__r:any;
  export default MC4SF__MC_Campaign__r;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__c.MC4SF__MC_Campaign__c" {
  const MC4SF__MC_Campaign__c:any;
  export default MC4SF__MC_Campaign__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__c.MC4SF__MC_List__r" {
  const MC4SF__MC_List__r:any;
  export default MC4SF__MC_List__r;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__c.MC4SF__MC_List__c" {
  const MC4SF__MC_List__c:any;
  export default MC4SF__MC_List__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__c.MC4SF__MailChimp_Campaign_ID__c" {
  const MC4SF__MailChimp_Campaign_ID__c:string;
  export default MC4SF__MailChimp_Campaign_ID__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__c.MC4SF__Text_URL__c" {
  const MC4SF__Text_URL__c:string;
  export default MC4SF__Text_URL__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__c.MC4SF__Timestamp__c" {
  const MC4SF__Timestamp__c:any;
  export default MC4SF__Timestamp__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__c.MC4SF__Type__c" {
  const MC4SF__Type__c:string;
  export default MC4SF__Type__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__c.MC4SF__URL__c" {
  const MC4SF__URL__c:string;
  export default MC4SF__URL__c;
}
