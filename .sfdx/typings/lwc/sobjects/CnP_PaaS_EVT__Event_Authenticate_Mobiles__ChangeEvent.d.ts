declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Authenticate_Mobiles__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Authenticate_Mobiles__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Authenticate_Mobiles__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Authenticate_Mobiles__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Authenticate_Mobiles__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Authenticate_Mobiles__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Authenticate_Mobiles__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Authenticate_Mobiles__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Authenticate_Mobiles__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Authenticate_Mobiles__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Authenticate_Mobiles__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Authenticate_Mobiles__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Authenticate_Mobiles__ChangeEvent.CnP_PaaS_EVT__Account_Name__c" {
  const CnP_PaaS_EVT__Account_Name__c:string;
  export default CnP_PaaS_EVT__Account_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Authenticate_Mobiles__ChangeEvent.CnP_PaaS_EVT__Account_Number__c" {
  const CnP_PaaS_EVT__Account_Number__c:string;
  export default CnP_PaaS_EVT__Account_Number__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Authenticate_Mobiles__ChangeEvent.CnP_PaaS_EVT__End_Date__c" {
  const CnP_PaaS_EVT__End_Date__c:any;
  export default CnP_PaaS_EVT__End_Date__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Authenticate_Mobiles__ChangeEvent.CnP_PaaS_EVT__Mobile_Status__c" {
  const CnP_PaaS_EVT__Mobile_Status__c:string;
  export default CnP_PaaS_EVT__Mobile_Status__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Authenticate_Mobiles__ChangeEvent.CnP_PaaS_EVT__Start_Date__c" {
  const CnP_PaaS_EVT__Start_Date__c:any;
  export default CnP_PaaS_EVT__Start_Date__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Authenticate_Mobiles__ChangeEvent.CnP_PaaS_EVT__User_id__c" {
  const CnP_PaaS_EVT__User_id__c:string;
  export default CnP_PaaS_EVT__User_id__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Authenticate_Mobiles__ChangeEvent.CnP_PaaS_EVT__Verification_With__c" {
  const CnP_PaaS_EVT__Verification_With__c:string;
  export default CnP_PaaS_EVT__Verification_With__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Authenticate_Mobiles__ChangeEvent.CnP_PaaS_EVT__c_p_Event__c" {
  const CnP_PaaS_EVT__c_p_Event__c:any;
  export default CnP_PaaS_EVT__c_p_Event__c;
}
