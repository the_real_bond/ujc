declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__ChangeEvent.CnP_PaaS_EVT__C_P_Event_Discount_Plan__c" {
  const CnP_PaaS_EVT__C_P_Event_Discount_Plan__c:any;
  export default CnP_PaaS_EVT__C_P_Event_Discount_Plan__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__ChangeEvent.CnP_PaaS_EVT__C_P_Event_Registrant__c" {
  const CnP_PaaS_EVT__C_P_Event_Registrant__c:any;
  export default CnP_PaaS_EVT__C_P_Event_Registrant__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__ChangeEvent.CnP_PaaS_EVT__C_P_Event_Registration_Level__c" {
  const CnP_PaaS_EVT__C_P_Event_Registration_Level__c:any;
  export default CnP_PaaS_EVT__C_P_Event_Registration_Level__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__ChangeEvent.CnP_PaaS_EVT__C_P_Event__c" {
  const CnP_PaaS_EVT__C_P_Event__c:any;
  export default CnP_PaaS_EVT__C_P_Event__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__ChangeEvent.CnP_PaaS_EVT__Status__c" {
  const CnP_PaaS_EVT__Status__c:string;
  export default CnP_PaaS_EVT__Status__c;
}
