declare module "@salesforce/schema/PowerLoader__PowerLoaderField__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderField__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderField__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderField__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderField__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderField__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderField__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderField__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderField__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderField__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderField__ChangeEvent.PowerLoader__PowerLoaderMapping__c" {
  const PowerLoader__PowerLoaderMapping__c:any;
  export default PowerLoader__PowerLoaderMapping__c;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderField__ChangeEvent.PowerLoader__CSVField__c" {
  const PowerLoader__CSVField__c:string;
  export default PowerLoader__CSVField__c;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderField__ChangeEvent.PowerLoader__ObjField__c" {
  const PowerLoader__ObjField__c:string;
  export default PowerLoader__ObjField__c;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderField__ChangeEvent.PowerLoader__RelField__c" {
  const PowerLoader__RelField__c:string;
  export default PowerLoader__RelField__c;
}
