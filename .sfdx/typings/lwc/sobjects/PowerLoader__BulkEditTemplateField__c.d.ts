declare module "@salesforce/schema/PowerLoader__BulkEditTemplateField__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplateField__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplateField__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplateField__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplateField__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplateField__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplateField__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplateField__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplateField__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplateField__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplateField__c.PowerLoader__BulkEditTemplate__r" {
  const PowerLoader__BulkEditTemplate__r:any;
  export default PowerLoader__BulkEditTemplate__r;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplateField__c.PowerLoader__BulkEditTemplate__c" {
  const PowerLoader__BulkEditTemplate__c:any;
  export default PowerLoader__BulkEditTemplate__c;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplateField__c.PowerLoader__FieldOrder__c" {
  const PowerLoader__FieldOrder__c:number;
  export default PowerLoader__FieldOrder__c;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplateField__c.PowerLoader__Group__c" {
  const PowerLoader__Group__c:boolean;
  export default PowerLoader__Group__c;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplateField__c.PowerLoader__SplitAt__c" {
  const PowerLoader__SplitAt__c:boolean;
  export default PowerLoader__SplitAt__c;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplateField__c.PowerLoader__SummaryType__c" {
  const PowerLoader__SummaryType__c:string;
  export default PowerLoader__SummaryType__c;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplateField__c.PowerLoader__Width__c" {
  const PowerLoader__Width__c:number;
  export default PowerLoader__Width__c;
}
