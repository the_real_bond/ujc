declare module "@salesforce/schema/CnP_PaaS__CnP_API_Settings__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_API_Settings__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_API_Settings__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_API_Settings__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_API_Settings__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_API_Settings__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_API_Settings__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_API_Settings__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_API_Settings__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_API_Settings__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_API_Settings__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_API_Settings__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_API_Settings__ChangeEvent.CnP_PaaS__CnP_Account_GUID__c" {
  const CnP_PaaS__CnP_Account_GUID__c:string;
  export default CnP_PaaS__CnP_Account_GUID__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_API_Settings__ChangeEvent.CnP_PaaS__CnP_Account_Number__c" {
  const CnP_PaaS__CnP_Account_Number__c:string;
  export default CnP_PaaS__CnP_Account_Number__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_API_Settings__ChangeEvent.CnP_PaaS__CnP_Account_Status__c" {
  const CnP_PaaS__CnP_Account_Status__c:string;
  export default CnP_PaaS__CnP_Account_Status__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_API_Settings__ChangeEvent.CnP_PaaS__Currency_Code__c" {
  const CnP_PaaS__Currency_Code__c:string;
  export default CnP_PaaS__Currency_Code__c;
}
