declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__c.CnP_PaaS_EVT__C_P_Event_Discount_Plan__r" {
  const CnP_PaaS_EVT__C_P_Event_Discount_Plan__r:any;
  export default CnP_PaaS_EVT__C_P_Event_Discount_Plan__r;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__c.CnP_PaaS_EVT__C_P_Event_Discount_Plan__c" {
  const CnP_PaaS_EVT__C_P_Event_Discount_Plan__c:any;
  export default CnP_PaaS_EVT__C_P_Event_Discount_Plan__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__c.CnP_PaaS_EVT__C_P_Event_Registrant__r" {
  const CnP_PaaS_EVT__C_P_Event_Registrant__r:any;
  export default CnP_PaaS_EVT__C_P_Event_Registrant__r;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__c.CnP_PaaS_EVT__C_P_Event_Registrant__c" {
  const CnP_PaaS_EVT__C_P_Event_Registrant__c:any;
  export default CnP_PaaS_EVT__C_P_Event_Registrant__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__c.CnP_PaaS_EVT__C_P_Event_Registration_Level__r" {
  const CnP_PaaS_EVT__C_P_Event_Registration_Level__r:any;
  export default CnP_PaaS_EVT__C_P_Event_Registration_Level__r;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__c.CnP_PaaS_EVT__C_P_Event_Registration_Level__c" {
  const CnP_PaaS_EVT__C_P_Event_Registration_Level__c:any;
  export default CnP_PaaS_EVT__C_P_Event_Registration_Level__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__c.CnP_PaaS_EVT__C_P_Event__r" {
  const CnP_PaaS_EVT__C_P_Event__r:any;
  export default CnP_PaaS_EVT__C_P_Event__r;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__c.CnP_PaaS_EVT__C_P_Event__c" {
  const CnP_PaaS_EVT__C_P_Event__c:any;
  export default CnP_PaaS_EVT__C_P_Event__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_LevelGroup__c.CnP_PaaS_EVT__Status__c" {
  const CnP_PaaS_EVT__Status__c:string;
  export default CnP_PaaS_EVT__Status__c;
}
