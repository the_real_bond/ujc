declare module "@salesforce/schema/Tax_Receipt__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Tax_Receipt__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Tax_Receipt__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Tax_Receipt__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Tax_Receipt__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Tax_Receipt__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Tax_Receipt__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Tax_Receipt__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Tax_Receipt__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Tax_Receipt__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Tax_Receipt__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Tax_Receipt__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Tax_Receipt__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/Tax_Receipt__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Tax_Receipt__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Tax_Receipt__c.AccountDate__c" {
  const AccountDate__c:string;
  export default AccountDate__c;
}
declare module "@salesforce/schema/Tax_Receipt__c.AccountEmail__c" {
  const AccountEmail__c:string;
  export default AccountEmail__c;
}
declare module "@salesforce/schema/Tax_Receipt__c.Account_Donor_Id__c" {
  const Account_Donor_Id__c:string;
  export default Account_Donor_Id__c;
}
declare module "@salesforce/schema/Tax_Receipt__c.Account_Name__c" {
  const Account_Name__c:string;
  export default Account_Name__c;
}
declare module "@salesforce/schema/Tax_Receipt__c.Account__r" {
  const Account__r:any;
  export default Account__r;
}
declare module "@salesforce/schema/Tax_Receipt__c.Account__c" {
  const Account__c:any;
  export default Account__c;
}
declare module "@salesforce/schema/Tax_Receipt__c.Amount_in_Words__c" {
  const Amount_in_Words__c:string;
  export default Amount_in_Words__c;
}
declare module "@salesforce/schema/Tax_Receipt__c.Attention__c" {
  const Attention__c:string;
  export default Attention__c;
}
declare module "@salesforce/schema/Tax_Receipt__c.Certificate_Number__c" {
  const Certificate_Number__c:string;
  export default Certificate_Number__c;
}
declare module "@salesforce/schema/Tax_Receipt__c.CloseDatemonthandYear__c" {
  const CloseDatemonthandYear__c:string;
  export default CloseDatemonthandYear__c;
}
declare module "@salesforce/schema/Tax_Receipt__c.Contact__r" {
  const Contact__r:any;
  export default Contact__r;
}
declare module "@salesforce/schema/Tax_Receipt__c.Contact__c" {
  const Contact__c:any;
  export default Contact__c;
}
declare module "@salesforce/schema/Tax_Receipt__c.Date_closed__c" {
  const Date_closed__c:any;
  export default Date_closed__c;
}
declare module "@salesforce/schema/Tax_Receipt__c.Email_Check__c" {
  const Email_Check__c:boolean;
  export default Email_Check__c;
}
declare module "@salesforce/schema/Tax_Receipt__c.Financial_Year_End__c" {
  const Financial_Year_End__c:any;
  export default Financial_Year_End__c;
}
declare module "@salesforce/schema/Tax_Receipt__c.Financial_Year_Start__c" {
  const Financial_Year_Start__c:any;
  export default Financial_Year_Start__c;
}
declare module "@salesforce/schema/Tax_Receipt__c.IsClosed__c" {
  const IsClosed__c:boolean;
  export default IsClosed__c;
}
declare module "@salesforce/schema/Tax_Receipt__c.ReceiptNumber__c" {
  const ReceiptNumber__c:string;
  export default ReceiptNumber__c;
}
declare module "@salesforce/schema/Tax_Receipt__c.Receipt_Date__c" {
  const Receipt_Date__c:any;
  export default Receipt_Date__c;
}
declare module "@salesforce/schema/Tax_Receipt__c.Tax_Certificate_Issued_to__c" {
  const Tax_Certificate_Issued_to__c:string;
  export default Tax_Certificate_Issued_to__c;
}
declare module "@salesforce/schema/Tax_Receipt__c.TodayDate__c" {
  const TodayDate__c:string;
  export default TodayDate__c;
}
declare module "@salesforce/schema/Tax_Receipt__c.Type__c" {
  const Type__c:string;
  export default Type__c;
}
declare module "@salesforce/schema/Tax_Receipt__c.YearEnd__c" {
  const YearEnd__c:string;
  export default YearEnd__c;
}
declare module "@salesforce/schema/Tax_Receipt__c.Year_Start_Words__c" {
  const Year_Start_Words__c:string;
  export default Year_Start_Words__c;
}
declare module "@salesforce/schema/Tax_Receipt__c.Year_end_Words__c" {
  const Year_end_Words__c:string;
  export default Year_end_Words__c;
}
declare module "@salesforce/schema/Tax_Receipt__c.YearendYear__c" {
  const YearendYear__c:string;
  export default YearendYear__c;
}
declare module "@salesforce/schema/Tax_Receipt__c.Total_Receipt_Amount__c" {
  const Total_Receipt_Amount__c:number;
  export default Total_Receipt_Amount__c;
}
declare module "@salesforce/schema/Tax_Receipt__c.Donor_Status__c" {
  const Donor_Status__c:string;
  export default Donor_Status__c;
}
