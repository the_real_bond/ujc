declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__ChangeEvent.ckt__Include_case_contact__c" {
  const ckt__Include_case_contact__c:boolean;
  export default ckt__Include_case_contact__c;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__ChangeEvent.ckt__Include_case_owner__c" {
  const ckt__Include_case_owner__c:boolean;
  export default ckt__Include_case_owner__c;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__ChangeEvent.ckt__Message__c" {
  const ckt__Message__c:string;
  export default ckt__Message__c;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__ChangeEvent.ckt__Recipients__c" {
  const ckt__Recipients__c:string;
  export default ckt__Recipients__c;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__ChangeEvent.ckt__Status__c" {
  const ckt__Status__c:string;
  export default ckt__Status__c;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__ChangeEvent.ckt__Template_Folder_Name__c" {
  const ckt__Template_Folder_Name__c:string;
  export default ckt__Template_Folder_Name__c;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__ChangeEvent.ckt__Template_Name__c" {
  const ckt__Template_Name__c:string;
  export default ckt__Template_Name__c;
}
