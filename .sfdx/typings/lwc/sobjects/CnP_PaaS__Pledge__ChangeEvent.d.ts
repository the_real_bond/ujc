declare module "@salesforce/schema/CnP_PaaS__Pledge__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__ChangeEvent.CnP_PaaS__Account__c" {
  const CnP_PaaS__Account__c:any;
  export default CnP_PaaS__Account__c;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__ChangeEvent.CnP_PaaS__Benifit_Value__c" {
  const CnP_PaaS__Benifit_Value__c:number;
  export default CnP_PaaS__Benifit_Value__c;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__ChangeEvent.CnP_PaaS__Campaign__c" {
  const CnP_PaaS__Campaign__c:any;
  export default CnP_PaaS__Campaign__c;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__ChangeEvent.CnP_PaaS__Contact__c" {
  const CnP_PaaS__Contact__c:any;
  export default CnP_PaaS__Contact__c;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__ChangeEvent.CnP_PaaS__Description__c" {
  const CnP_PaaS__Description__c:string;
  export default CnP_PaaS__Description__c;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__ChangeEvent.CnP_PaaS__End_Date__c" {
  const CnP_PaaS__End_Date__c:any;
  export default CnP_PaaS__End_Date__c;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__ChangeEvent.CnP_PaaS__Excess_Amount__c" {
  const CnP_PaaS__Excess_Amount__c:boolean;
  export default CnP_PaaS__Excess_Amount__c;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__ChangeEvent.CnP_PaaS__Number_of_Payments__c" {
  const CnP_PaaS__Number_of_Payments__c:number;
  export default CnP_PaaS__Number_of_Payments__c;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__ChangeEvent.CnP_PaaS__Pledge_Amount__c" {
  const CnP_PaaS__Pledge_Amount__c:number;
  export default CnP_PaaS__Pledge_Amount__c;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__ChangeEvent.CnP_PaaS__Pledge_Status__c" {
  const CnP_PaaS__Pledge_Status__c:string;
  export default CnP_PaaS__Pledge_Status__c;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__ChangeEvent.CnP_PaaS__Remaining_Balance__c" {
  const CnP_PaaS__Remaining_Balance__c:number;
  export default CnP_PaaS__Remaining_Balance__c;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__ChangeEvent.CnP_PaaS__SKU_Condition__c" {
  const CnP_PaaS__SKU_Condition__c:string;
  export default CnP_PaaS__SKU_Condition__c;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__ChangeEvent.CnP_PaaS__SKU_VT__c" {
  const CnP_PaaS__SKU_VT__c:string;
  export default CnP_PaaS__SKU_VT__c;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__ChangeEvent.CnP_PaaS__SKU__c" {
  const CnP_PaaS__SKU__c:string;
  export default CnP_PaaS__SKU__c;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__ChangeEvent.CnP_PaaS__Start_Date__c" {
  const CnP_PaaS__Start_Date__c:any;
  export default CnP_PaaS__Start_Date__c;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__ChangeEvent.CnP_PaaS__Tribute__c" {
  const CnP_PaaS__Tribute__c:string;
  export default CnP_PaaS__Tribute__c;
}
