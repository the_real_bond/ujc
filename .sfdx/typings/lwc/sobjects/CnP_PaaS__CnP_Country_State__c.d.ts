declare module "@salesforce/schema/CnP_PaaS__CnP_Country_State__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Country_State__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Country_State__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Country_State__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Country_State__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Country_State__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Country_State__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Country_State__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Country_State__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Country_State__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Country_State__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Country_State__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Country_State__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Country_State__c.CnP_PaaS__CnP_Countries__r" {
  const CnP_PaaS__CnP_Countries__r:any;
  export default CnP_PaaS__CnP_Countries__r;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Country_State__c.CnP_PaaS__CnP_Countries__c" {
  const CnP_PaaS__CnP_Countries__c:any;
  export default CnP_PaaS__CnP_Countries__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Country_State__c.CnP_PaaS__Country_Code__c" {
  const CnP_PaaS__Country_Code__c:string;
  export default CnP_PaaS__Country_Code__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Country_State__c.CnP_PaaS__State_Abbrev__c" {
  const CnP_PaaS__State_Abbrev__c:string;
  export default CnP_PaaS__State_Abbrev__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Country_State__c.CnP_PaaS__State_Name__c" {
  const CnP_PaaS__State_Name__c:string;
  export default CnP_PaaS__State_Name__c;
}
