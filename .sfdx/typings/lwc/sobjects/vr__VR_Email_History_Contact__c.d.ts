declare module "@salesforce/schema/vr__VR_Email_History_Contact__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/vr__VR_Email_History_Contact__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/vr__VR_Email_History_Contact__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/vr__VR_Email_History_Contact__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/vr__VR_Email_History_Contact__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/vr__VR_Email_History_Contact__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/vr__VR_Email_History_Contact__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/vr__VR_Email_History_Contact__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/vr__VR_Email_History_Contact__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/vr__VR_Email_History_Contact__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/vr__VR_Email_History_Contact__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/vr__VR_Email_History_Contact__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/vr__VR_Email_History_Contact__c.vr__Bounced__c" {
  const vr__Bounced__c:boolean;
  export default vr__Bounced__c;
}
declare module "@salesforce/schema/vr__VR_Email_History_Contact__c.vr__Campaign_Hash__c" {
  const vr__Campaign_Hash__c:string;
  export default vr__Campaign_Hash__c;
}
declare module "@salesforce/schema/vr__VR_Email_History_Contact__c.vr__Clicked_Links__c" {
  const vr__Clicked_Links__c:string;
  export default vr__Clicked_Links__c;
}
declare module "@salesforce/schema/vr__VR_Email_History_Contact__c.vr__Clicked__c" {
  const vr__Clicked__c:boolean;
  export default vr__Clicked__c;
}
declare module "@salesforce/schema/vr__VR_Email_History_Contact__c.vr__Company_Hash__c" {
  const vr__Company_Hash__c:string;
  export default vr__Company_Hash__c;
}
declare module "@salesforce/schema/vr__VR_Email_History_Contact__c.vr__Contact__r" {
  const vr__Contact__r:any;
  export default vr__Contact__r;
}
declare module "@salesforce/schema/vr__VR_Email_History_Contact__c.vr__Contact__c" {
  const vr__Contact__c:any;
  export default vr__Contact__c;
}
declare module "@salesforce/schema/vr__VR_Email_History_Contact__c.vr__Email_ID__c" {
  const vr__Email_ID__c:string;
  export default vr__Email_ID__c;
}
declare module "@salesforce/schema/vr__VR_Email_History_Contact__c.vr__Email_Type__c" {
  const vr__Email_Type__c:string;
  export default vr__Email_Type__c;
}
declare module "@salesforce/schema/vr__VR_Email_History_Contact__c.vr__List_Type__c" {
  const vr__List_Type__c:string;
  export default vr__List_Type__c;
}
declare module "@salesforce/schema/vr__VR_Email_History_Contact__c.vr__Mail_Date__c" {
  const vr__Mail_Date__c:any;
  export default vr__Mail_Date__c;
}
declare module "@salesforce/schema/vr__VR_Email_History_Contact__c.vr__Opened__c" {
  const vr__Opened__c:boolean;
  export default vr__Opened__c;
}
declare module "@salesforce/schema/vr__VR_Email_History_Contact__c.vr__Sent__c" {
  const vr__Sent__c:boolean;
  export default vr__Sent__c;
}
declare module "@salesforce/schema/vr__VR_Email_History_Contact__c.vr__Unsubscribed__c" {
  const vr__Unsubscribed__c:boolean;
  export default vr__Unsubscribed__c;
}
