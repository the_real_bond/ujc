declare module "@salesforce/schema/CnP_PaaS__Pledge__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__c.CnP_PaaS__Account__r" {
  const CnP_PaaS__Account__r:any;
  export default CnP_PaaS__Account__r;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__c.CnP_PaaS__Account__c" {
  const CnP_PaaS__Account__c:any;
  export default CnP_PaaS__Account__c;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__c.CnP_PaaS__Benifit_Value__c" {
  const CnP_PaaS__Benifit_Value__c:number;
  export default CnP_PaaS__Benifit_Value__c;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__c.CnP_PaaS__Campaign__r" {
  const CnP_PaaS__Campaign__r:any;
  export default CnP_PaaS__Campaign__r;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__c.CnP_PaaS__Campaign__c" {
  const CnP_PaaS__Campaign__c:any;
  export default CnP_PaaS__Campaign__c;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__c.CnP_PaaS__Contact__r" {
  const CnP_PaaS__Contact__r:any;
  export default CnP_PaaS__Contact__r;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__c.CnP_PaaS__Contact__c" {
  const CnP_PaaS__Contact__c:any;
  export default CnP_PaaS__Contact__c;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__c.CnP_PaaS__Description__c" {
  const CnP_PaaS__Description__c:string;
  export default CnP_PaaS__Description__c;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__c.CnP_PaaS__End_Date__c" {
  const CnP_PaaS__End_Date__c:any;
  export default CnP_PaaS__End_Date__c;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__c.CnP_PaaS__Excess_Amount__c" {
  const CnP_PaaS__Excess_Amount__c:boolean;
  export default CnP_PaaS__Excess_Amount__c;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__c.CnP_PaaS__Number_of_Payments__c" {
  const CnP_PaaS__Number_of_Payments__c:number;
  export default CnP_PaaS__Number_of_Payments__c;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__c.CnP_PaaS__Pledge_Amount__c" {
  const CnP_PaaS__Pledge_Amount__c:number;
  export default CnP_PaaS__Pledge_Amount__c;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__c.CnP_PaaS__Pledge_Status__c" {
  const CnP_PaaS__Pledge_Status__c:string;
  export default CnP_PaaS__Pledge_Status__c;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__c.CnP_PaaS__Remaining_Balance__c" {
  const CnP_PaaS__Remaining_Balance__c:number;
  export default CnP_PaaS__Remaining_Balance__c;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__c.CnP_PaaS__SKU_Condition__c" {
  const CnP_PaaS__SKU_Condition__c:string;
  export default CnP_PaaS__SKU_Condition__c;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__c.CnP_PaaS__SKU_VT__c" {
  const CnP_PaaS__SKU_VT__c:string;
  export default CnP_PaaS__SKU_VT__c;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__c.CnP_PaaS__SKU__c" {
  const CnP_PaaS__SKU__c:string;
  export default CnP_PaaS__SKU__c;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__c.CnP_PaaS__Start_Date__c" {
  const CnP_PaaS__Start_Date__c:any;
  export default CnP_PaaS__Start_Date__c;
}
declare module "@salesforce/schema/CnP_PaaS__Pledge__c.CnP_PaaS__Tribute__c" {
  const CnP_PaaS__Tribute__c:string;
  export default CnP_PaaS__Tribute__c;
}
