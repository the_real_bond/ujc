declare module "@salesforce/schema/ckt__Text_Message_Template__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__c.ckt__Available_For_Use__c" {
  const ckt__Available_For_Use__c:boolean;
  export default ckt__Available_For_Use__c;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__c.ckt__Description__c" {
  const ckt__Description__c:string;
  export default ckt__Description__c;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__c.ckt__Folder__r" {
  const ckt__Folder__r:any;
  export default ckt__Folder__r;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__c.ckt__Folder__c" {
  const ckt__Folder__c:any;
  export default ckt__Folder__c;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__c.ckt__Template_Unique_Name__c" {
  const ckt__Template_Unique_Name__c:string;
  export default ckt__Template_Unique_Name__c;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__c.ckt__Text_Message_Body__c" {
  const ckt__Text_Message_Body__c:string;
  export default ckt__Text_Message_Body__c;
}
