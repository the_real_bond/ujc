declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__c.CnP_PaaS_EVT__AttendeeID__r" {
  const CnP_PaaS_EVT__AttendeeID__r:any;
  export default CnP_PaaS_EVT__AttendeeID__r;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__c.CnP_PaaS_EVT__AttendeeID__c" {
  const CnP_PaaS_EVT__AttendeeID__c:any;
  export default CnP_PaaS_EVT__AttendeeID__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__c.CnP_PaaS_EVT__C_P_Data__r" {
  const CnP_PaaS_EVT__C_P_Data__r:any;
  export default CnP_PaaS_EVT__C_P_Data__r;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__c.CnP_PaaS_EVT__C_P_Data__c" {
  const CnP_PaaS_EVT__C_P_Data__c:any;
  export default CnP_PaaS_EVT__C_P_Data__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__c.CnP_PaaS_EVT__C_P_Event_Registrant__r" {
  const CnP_PaaS_EVT__C_P_Event_Registrant__r:any;
  export default CnP_PaaS_EVT__C_P_Event_Registrant__r;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__c.CnP_PaaS_EVT__C_P_Event_Registrant__c" {
  const CnP_PaaS_EVT__C_P_Event_Registrant__c:any;
  export default CnP_PaaS_EVT__C_P_Event_Registrant__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__c.CnP_PaaS_EVT__Contact_Data__c" {
  const CnP_PaaS_EVT__Contact_Data__c:string;
  export default CnP_PaaS_EVT__Contact_Data__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__c.CnP_PaaS_EVT__Email__c" {
  const CnP_PaaS_EVT__Email__c:string;
  export default CnP_PaaS_EVT__Email__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__c.CnP_PaaS_EVT__Event_name__r" {
  const CnP_PaaS_EVT__Event_name__r:any;
  export default CnP_PaaS_EVT__Event_name__r;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__c.CnP_PaaS_EVT__Event_name__c" {
  const CnP_PaaS_EVT__Event_name__c:any;
  export default CnP_PaaS_EVT__Event_name__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__c.CnP_PaaS_EVT__First_name__c" {
  const CnP_PaaS_EVT__First_name__c:string;
  export default CnP_PaaS_EVT__First_name__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__c.CnP_PaaS_EVT__Last_name__c" {
  const CnP_PaaS_EVT__Last_name__c:string;
  export default CnP_PaaS_EVT__Last_name__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__c.CnP_PaaS_EVT__Map_Contact__r" {
  const CnP_PaaS_EVT__Map_Contact__r:any;
  export default CnP_PaaS_EVT__Map_Contact__r;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__c.CnP_PaaS_EVT__Map_Contact__c" {
  const CnP_PaaS_EVT__Map_Contact__c:any;
  export default CnP_PaaS_EVT__Map_Contact__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__c.CnP_PaaS_EVT__Order_Number__c" {
  const CnP_PaaS_EVT__Order_Number__c:string;
  export default CnP_PaaS_EVT__Order_Number__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__c.CnP_PaaS_EVT__XML_Data__c" {
  const CnP_PaaS_EVT__XML_Data__c:string;
  export default CnP_PaaS_EVT__XML_Data__c;
}
