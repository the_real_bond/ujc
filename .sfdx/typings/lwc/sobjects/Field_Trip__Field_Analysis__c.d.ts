declare module "@salesforce/schema/Field_Trip__Field_Analysis__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Field_Trip__Field_Analysis__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Field_Trip__Field_Analysis__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Field_Trip__Field_Analysis__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Field_Trip__Field_Analysis__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Field_Trip__Field_Analysis__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Field_Trip__Field_Analysis__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Field_Trip__Field_Analysis__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Field_Trip__Field_Analysis__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Field_Trip__Field_Analysis__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Field_Trip__Field_Analysis__c.Field_Trip__Object_Analysis__r" {
  const Field_Trip__Object_Analysis__r:any;
  export default Field_Trip__Object_Analysis__r;
}
declare module "@salesforce/schema/Field_Trip__Field_Analysis__c.Field_Trip__Object_Analysis__c" {
  const Field_Trip__Object_Analysis__c:any;
  export default Field_Trip__Object_Analysis__c;
}
declare module "@salesforce/schema/Field_Trip__Field_Analysis__c.Field_Trip__Field_Created_Date__c" {
  const Field_Trip__Field_Created_Date__c:any;
  export default Field_Trip__Field_Created_Date__c;
}
declare module "@salesforce/schema/Field_Trip__Field_Analysis__c.Field_Trip__Label__c" {
  const Field_Trip__Label__c:string;
  export default Field_Trip__Label__c;
}
declare module "@salesforce/schema/Field_Trip__Field_Analysis__c.Field_Trip__Populated_On_Percent__c" {
  const Field_Trip__Populated_On_Percent__c:number;
  export default Field_Trip__Populated_On_Percent__c;
}
declare module "@salesforce/schema/Field_Trip__Field_Analysis__c.Field_Trip__Populated_On__c" {
  const Field_Trip__Populated_On__c:number;
  export default Field_Trip__Populated_On__c;
}
declare module "@salesforce/schema/Field_Trip__Field_Analysis__c.Field_Trip__Total_Tally__c" {
  const Field_Trip__Total_Tally__c:number;
  export default Field_Trip__Total_Tally__c;
}
declare module "@salesforce/schema/Field_Trip__Field_Analysis__c.Field_Trip__Type__c" {
  const Field_Trip__Type__c:string;
  export default Field_Trip__Type__c;
}
declare module "@salesforce/schema/Field_Trip__Field_Analysis__c.Field_Trip__isCustom__c" {
  const Field_Trip__isCustom__c:boolean;
  export default Field_Trip__isCustom__c;
}
declare module "@salesforce/schema/Field_Trip__Field_Analysis__c.Field_Trip__isRequired__c" {
  const Field_Trip__isRequired__c:boolean;
  export default Field_Trip__isRequired__c;
}
