declare module "@salesforce/schema/APXTConga4__Conga_Solution__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution__ChangeEvent.APXTConga4__Button_Link_API_Name__c" {
  const APXTConga4__Button_Link_API_Name__c:string;
  export default APXTConga4__Button_Link_API_Name__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution__ChangeEvent.APXTConga4__Button_body_field__c" {
  const APXTConga4__Button_body_field__c:string;
  export default APXTConga4__Button_body_field__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution__ChangeEvent.APXTConga4__Composer_Parameters__c" {
  const APXTConga4__Composer_Parameters__c:string;
  export default APXTConga4__Composer_Parameters__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution__ChangeEvent.APXTConga4__Custom_Object_Id__c" {
  const APXTConga4__Custom_Object_Id__c:string;
  export default APXTConga4__Custom_Object_Id__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution__ChangeEvent.APXTConga4__Formula_Field_API_Name__c" {
  const APXTConga4__Formula_Field_API_Name__c:string;
  export default APXTConga4__Formula_Field_API_Name__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution__ChangeEvent.APXTConga4__Formula_body_field__c" {
  const APXTConga4__Formula_body_field__c:string;
  export default APXTConga4__Formula_body_field__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution__ChangeEvent.APXTConga4__Is_Quick_Start__c" {
  const APXTConga4__Is_Quick_Start__c:string;
  export default APXTConga4__Is_Quick_Start__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution__ChangeEvent.APXTConga4__Launch_C8_Formula_Button__c" {
  const APXTConga4__Launch_C8_Formula_Button__c:string;
  export default APXTConga4__Launch_C8_Formula_Button__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution__ChangeEvent.APXTConga4__Master_Object_Type_Validator__c" {
  const APXTConga4__Master_Object_Type_Validator__c:string;
  export default APXTConga4__Master_Object_Type_Validator__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution__ChangeEvent.APXTConga4__Master_Object_Type__c" {
  const APXTConga4__Master_Object_Type__c:string;
  export default APXTConga4__Master_Object_Type__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution__ChangeEvent.APXTConga4__Sample_Record_Id__c" {
  const APXTConga4__Sample_Record_Id__c:string;
  export default APXTConga4__Sample_Record_Id__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution__ChangeEvent.APXTConga4__Sample_Record_Name__c" {
  const APXTConga4__Sample_Record_Name__c:string;
  export default APXTConga4__Sample_Record_Name__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution__ChangeEvent.APXTConga4__Solution_Description__c" {
  const APXTConga4__Solution_Description__c:string;
  export default APXTConga4__Solution_Description__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution__ChangeEvent.APXTConga4__Solution_Weblink_Syntax__c" {
  const APXTConga4__Solution_Weblink_Syntax__c:string;
  export default APXTConga4__Solution_Weblink_Syntax__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution__ChangeEvent.APXTConga4__Weblink_Id__c" {
  const APXTConga4__Weblink_Id__c:string;
  export default APXTConga4__Weblink_Id__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution__ChangeEvent.APXTConga4__CongaEmailTemplateCount__c" {
  const APXTConga4__CongaEmailTemplateCount__c:number;
  export default APXTConga4__CongaEmailTemplateCount__c;
}
