declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__c.CnP_PaaS__Discount__c" {
  const CnP_PaaS__Discount__c:number;
  export default CnP_PaaS__Discount__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__c.CnP_PaaS__Item_Campaign__c" {
  const CnP_PaaS__Item_Campaign__c:string;
  export default CnP_PaaS__Item_Campaign__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__c.CnP_PaaS__Item_Price__c" {
  const CnP_PaaS__Item_Price__c:number;
  export default CnP_PaaS__Item_Price__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__c.CnP_PaaS__Quantity__c" {
  const CnP_PaaS__Quantity__c:number;
  export default CnP_PaaS__Quantity__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__c.CnP_PaaS__SKU__c" {
  const CnP_PaaS__SKU__c:string;
  export default CnP_PaaS__SKU__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__c.CnP_PaaS__Tax__c" {
  const CnP_PaaS__Tax__c:number;
  export default CnP_PaaS__Tax__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__c.CnP_PaaS__Temp_Invoice__r" {
  const CnP_PaaS__Temp_Invoice__r:any;
  export default CnP_PaaS__Temp_Invoice__r;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__c.CnP_PaaS__Temp_Invoice__c" {
  const CnP_PaaS__Temp_Invoice__c:any;
  export default CnP_PaaS__Temp_Invoice__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__c.CnP_PaaS__tax_deductible__c" {
  const CnP_PaaS__tax_deductible__c:number;
  export default CnP_PaaS__tax_deductible__c;
}
