declare module "@salesforce/schema/CnP_PaaS__Opportunity_Pledge__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Opportunity_Pledge__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__Opportunity_Pledge__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__Opportunity_Pledge__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__Opportunity_Pledge__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Opportunity_Pledge__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Opportunity_Pledge__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Opportunity_Pledge__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Opportunity_Pledge__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Opportunity_Pledge__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Opportunity_Pledge__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Opportunity_Pledge__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__Opportunity_Pledge__c.CnP_PaaS__Amount__c" {
  const CnP_PaaS__Amount__c:number;
  export default CnP_PaaS__Amount__c;
}
declare module "@salesforce/schema/CnP_PaaS__Opportunity_Pledge__c.CnP_PaaS__Opportunity__r" {
  const CnP_PaaS__Opportunity__r:any;
  export default CnP_PaaS__Opportunity__r;
}
declare module "@salesforce/schema/CnP_PaaS__Opportunity_Pledge__c.CnP_PaaS__Opportunity__c" {
  const CnP_PaaS__Opportunity__c:any;
  export default CnP_PaaS__Opportunity__c;
}
declare module "@salesforce/schema/CnP_PaaS__Opportunity_Pledge__c.CnP_PaaS__Pledge__r" {
  const CnP_PaaS__Pledge__r:any;
  export default CnP_PaaS__Pledge__r;
}
declare module "@salesforce/schema/CnP_PaaS__Opportunity_Pledge__c.CnP_PaaS__Pledge__c" {
  const CnP_PaaS__Pledge__c:any;
  export default CnP_PaaS__Pledge__c;
}
