declare module "@salesforce/schema/APXT_BPM__Conductor__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__ChangeEvent.APXT_BPM__Content_Workspace_Id__c" {
  const APXT_BPM__Content_Workspace_Id__c:string;
  export default APXT_BPM__Content_Workspace_Id__c;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__ChangeEvent.APXT_BPM__Description__c" {
  const APXT_BPM__Description__c:string;
  export default APXT_BPM__Description__c;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__ChangeEvent.APXT_BPM__Has_Query_Id__c" {
  const APXT_BPM__Has_Query_Id__c:number;
  export default APXT_BPM__Has_Query_Id__c;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__ChangeEvent.APXT_BPM__Has_Record_Id__c" {
  const APXT_BPM__Has_Record_Id__c:number;
  export default APXT_BPM__Has_Record_Id__c;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__ChangeEvent.APXT_BPM__Has_Report_Id__c" {
  const APXT_BPM__Has_Report_Id__c:number;
  export default APXT_BPM__Has_Report_Id__c;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__ChangeEvent.APXT_BPM__Next_Run_Date_Display__c" {
  const APXT_BPM__Next_Run_Date_Display__c:any;
  export default APXT_BPM__Next_Run_Date_Display__c;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__ChangeEvent.APXT_BPM__Next_Run_Date__c" {
  const APXT_BPM__Next_Run_Date__c:any;
  export default APXT_BPM__Next_Run_Date__c;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__ChangeEvent.APXT_BPM__Query_Id__c" {
  const APXT_BPM__Query_Id__c:string;
  export default APXT_BPM__Query_Id__c;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__ChangeEvent.APXT_BPM__Record_Id__c" {
  const APXT_BPM__Record_Id__c:string;
  export default APXT_BPM__Record_Id__c;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__ChangeEvent.APXT_BPM__Report_Id__c" {
  const APXT_BPM__Report_Id__c:string;
  export default APXT_BPM__Report_Id__c;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__ChangeEvent.APXT_BPM__Schedule_Description_Display__c" {
  const APXT_BPM__Schedule_Description_Display__c:string;
  export default APXT_BPM__Schedule_Description_Display__c;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__ChangeEvent.APXT_BPM__Schedule_Description__c" {
  const APXT_BPM__Schedule_Description__c:string;
  export default APXT_BPM__Schedule_Description__c;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__ChangeEvent.APXT_BPM__Title__c" {
  const APXT_BPM__Title__c:string;
  export default APXT_BPM__Title__c;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__ChangeEvent.APXT_BPM__URL_Field_Name__c" {
  const APXT_BPM__URL_Field_Name__c:string;
  export default APXT_BPM__URL_Field_Name__c;
}
