declare module "@salesforce/schema/PowerLoader__ObjectTemplate__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/PowerLoader__ObjectTemplate__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/PowerLoader__ObjectTemplate__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/PowerLoader__ObjectTemplate__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/PowerLoader__ObjectTemplate__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/PowerLoader__ObjectTemplate__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/PowerLoader__ObjectTemplate__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/PowerLoader__ObjectTemplate__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/PowerLoader__ObjectTemplate__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/PowerLoader__ObjectTemplate__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/PowerLoader__ObjectTemplate__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/PowerLoader__ObjectTemplate__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/PowerLoader__ObjectTemplate__c.PowerLoader__DateTemplate__c" {
  const PowerLoader__DateTemplate__c:any;
  export default PowerLoader__DateTemplate__c;
}
declare module "@salesforce/schema/PowerLoader__ObjectTemplate__c.PowerLoader__NumberTemplate__c" {
  const PowerLoader__NumberTemplate__c:number;
  export default PowerLoader__NumberTemplate__c;
}
