declare module "@salesforce/schema/APXTConga4__Conga_Email_Staging__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Staging__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Staging__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Staging__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Staging__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Staging__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Staging__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Staging__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Staging__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Staging__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Staging__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Staging__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Staging__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Staging__c.APXTConga4__HTMLBody__c" {
  const APXTConga4__HTMLBody__c:string;
  export default APXTConga4__HTMLBody__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Staging__c.APXTConga4__Subject__c" {
  const APXTConga4__Subject__c:string;
  export default APXTConga4__Subject__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Staging__c.APXTConga4__TextBody__c" {
  const APXTConga4__TextBody__c:string;
  export default APXTConga4__TextBody__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Staging__c.APXTConga4__WhatId__c" {
  const APXTConga4__WhatId__c:string;
  export default APXTConga4__WhatId__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Staging__c.APXTConga4__WhoId__c" {
  const APXTConga4__WhoId__c:string;
  export default APXTConga4__WhoId__c;
}
