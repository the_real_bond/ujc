declare module "@salesforce/schema/MC4SF__MC_Interest_Grouping__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Grouping__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Grouping__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Grouping__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Grouping__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Grouping__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Grouping__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Grouping__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Grouping__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Grouping__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Grouping__c.MC4SF__MC_List__r" {
  const MC4SF__MC_List__r:any;
  export default MC4SF__MC_List__r;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Grouping__c.MC4SF__MC_List__c" {
  const MC4SF__MC_List__c:any;
  export default MC4SF__MC_List__c;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Grouping__c.MC4SF__Form_Field__c" {
  const MC4SF__Form_Field__c:string;
  export default MC4SF__Form_Field__c;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Grouping__c.MC4SF__MailChimp_ID__c" {
  const MC4SF__MailChimp_ID__c:number;
  export default MC4SF__MailChimp_ID__c;
}
