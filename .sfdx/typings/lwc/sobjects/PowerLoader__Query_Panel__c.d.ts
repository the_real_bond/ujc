declare module "@salesforce/schema/PowerLoader__Query_Panel__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/PowerLoader__Query_Panel__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/PowerLoader__Query_Panel__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/PowerLoader__Query_Panel__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/PowerLoader__Query_Panel__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/PowerLoader__Query_Panel__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/PowerLoader__Query_Panel__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/PowerLoader__Query_Panel__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/PowerLoader__Query_Panel__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/PowerLoader__Query_Panel__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/PowerLoader__Query_Panel__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/PowerLoader__Query_Panel__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/PowerLoader__Query_Panel__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/PowerLoader__Query_Panel__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/PowerLoader__Query_Panel__c.PowerLoader__Custom_Layout__c" {
  const PowerLoader__Custom_Layout__c:string;
  export default PowerLoader__Custom_Layout__c;
}
