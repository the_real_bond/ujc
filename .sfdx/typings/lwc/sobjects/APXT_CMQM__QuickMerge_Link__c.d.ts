declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.RecordType" {
  const RecordType:any;
  export default RecordType;
}
declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.RecordTypeId" {
  const RecordTypeId:any;
  export default RecordTypeId;
}
declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.APXT_CMQM__Activity_Subject__c" {
  const APXT_CMQM__Activity_Subject__c:string;
  export default APXT_CMQM__Activity_Subject__c;
}
declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.APXT_CMQM__Automatic_Logging__c" {
  const APXT_CMQM__Automatic_Logging__c:boolean;
  export default APXT_CMQM__Automatic_Logging__c;
}
declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.APXT_CMQM__Bypass_Wizard__c" {
  const APXT_CMQM__Bypass_Wizard__c:boolean;
  export default APXT_CMQM__Bypass_Wizard__c;
}
declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.APXT_CMQM__Campaign_Id__c" {
  const APXT_CMQM__Campaign_Id__c:string;
  export default APXT_CMQM__Campaign_Id__c;
}
declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.APXT_CMQM__Default_to_PDF__c" {
  const APXT_CMQM__Default_to_PDF__c:boolean;
  export default APXT_CMQM__Default_to_PDF__c;
}
declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.APXT_CMQM__Description__c" {
  const APXT_CMQM__Description__c:string;
  export default APXT_CMQM__Description__c;
}
declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.APXT_CMQM__Document_Template_Id__c" {
  const APXT_CMQM__Document_Template_Id__c:string;
  export default APXT_CMQM__Document_Template_Id__c;
}
declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.APXT_CMQM__Envelope_Template_Id__c" {
  const APXT_CMQM__Envelope_Template_Id__c:string;
  export default APXT_CMQM__Envelope_Template_Id__c;
}
declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.APXT_CMQM__Follow_up_Interval__c" {
  const APXT_CMQM__Follow_up_Interval__c:number;
  export default APXT_CMQM__Follow_up_Interval__c;
}
declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.APXT_CMQM__Force_Output_as_PDF__c" {
  const APXT_CMQM__Force_Output_as_PDF__c:boolean;
  export default APXT_CMQM__Force_Output_as_PDF__c;
}
declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.APXT_CMQM__Label_Template_Id__c" {
  const APXT_CMQM__Label_Template_Id__c:string;
  export default APXT_CMQM__Label_Template_Id__c;
}
declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.APXT_CMQM__Launch__c" {
  const APXT_CMQM__Launch__c:string;
  export default APXT_CMQM__Launch__c;
}
declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.APXT_CMQM__Link_Type__c" {
  const APXT_CMQM__Link_Type__c:string;
  export default APXT_CMQM__Link_Type__c;
}
declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.APXT_CMQM__LogWordBody__c" {
  const APXT_CMQM__LogWordBody__c:boolean;
  export default APXT_CMQM__LogWordBody__c;
}
declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.APXT_CMQM__Other_Parameters__c" {
  const APXT_CMQM__Other_Parameters__c:string;
  export default APXT_CMQM__Other_Parameters__c;
}
declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.APXT_CMQM__Query_Id_1__c" {
  const APXT_CMQM__Query_Id_1__c:string;
  export default APXT_CMQM__Query_Id_1__c;
}
declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.APXT_CMQM__Query_Id__c" {
  const APXT_CMQM__Query_Id__c:string;
  export default APXT_CMQM__Query_Id__c;
}
declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.APXT_CMQM__Report_Id__c" {
  const APXT_CMQM__Report_Id__c:string;
  export default APXT_CMQM__Report_Id__c;
}
declare module "@salesforce/schema/APXT_CMQM__QuickMerge_Link__c.APXT_CMQM__Title__c" {
  const APXT_CMQM__Title__c:string;
  export default APXT_CMQM__Title__c;
}
