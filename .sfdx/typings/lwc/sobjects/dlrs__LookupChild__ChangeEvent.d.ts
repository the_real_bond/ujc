declare module "@salesforce/schema/dlrs__LookupChild__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/dlrs__LookupChild__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/dlrs__LookupChild__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/dlrs__LookupChild__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/dlrs__LookupChild__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/dlrs__LookupChild__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/dlrs__LookupChild__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/dlrs__LookupChild__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/dlrs__LookupChild__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/dlrs__LookupChild__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/dlrs__LookupChild__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/dlrs__LookupChild__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/dlrs__LookupChild__ChangeEvent.dlrs__Amount__c" {
  const dlrs__Amount__c:number;
  export default dlrs__Amount__c;
}
declare module "@salesforce/schema/dlrs__LookupChild__ChangeEvent.dlrs__LookupParent__c" {
  const dlrs__LookupParent__c:any;
  export default dlrs__LookupParent__c;
}
declare module "@salesforce/schema/dlrs__LookupChild__ChangeEvent.dlrs__Color__c" {
  const dlrs__Color__c:string;
  export default dlrs__Color__c;
}
declare module "@salesforce/schema/dlrs__LookupChild__ChangeEvent.dlrs__Description2__c" {
  const dlrs__Description2__c:string;
  export default dlrs__Description2__c;
}
declare module "@salesforce/schema/dlrs__LookupChild__ChangeEvent.dlrs__Description__c" {
  const dlrs__Description__c:string;
  export default dlrs__Description__c;
}
declare module "@salesforce/schema/dlrs__LookupChild__ChangeEvent.dlrs__LookupParent2__c" {
  const dlrs__LookupParent2__c:any;
  export default dlrs__LookupParent2__c;
}
