declare module "@salesforce/schema/Receipt__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Receipt__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Receipt__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Receipt__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Receipt__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Receipt__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Receipt__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Receipt__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Receipt__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Receipt__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Receipt__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Receipt__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Receipt__c.IUA__c" {
  const IUA__c:number;
  export default IUA__c;
}
declare module "@salesforce/schema/Receipt__c.Receipt_Date__c" {
  const Receipt_Date__c:any;
  export default Receipt_Date__c;
}
declare module "@salesforce/schema/Receipt__c.UCF_Communal__c" {
  const UCF_Communal__c:number;
  export default UCF_Communal__c;
}
declare module "@salesforce/schema/Receipt__c.UCF_Education__c" {
  const UCF_Education__c:number;
  export default UCF_Education__c;
}
declare module "@salesforce/schema/Receipt__c.Welfare__c" {
  const Welfare__c:number;
  export default Welfare__c;
}
declare module "@salesforce/schema/Receipt__c.Total_Receipt_Amount__c" {
  const Total_Receipt_Amount__c:number;
  export default Total_Receipt_Amount__c;
}
declare module "@salesforce/schema/Receipt__c.Pledge__r" {
  const Pledge__r:any;
  export default Pledge__r;
}
declare module "@salesforce/schema/Receipt__c.Pledge__c" {
  const Pledge__c:any;
  export default Pledge__c;
}
declare module "@salesforce/schema/Receipt__c.Trans_Key__c" {
  const Trans_Key__c:string;
  export default Trans_Key__c;
}
declare module "@salesforce/schema/Receipt__c.Donor__r" {
  const Donor__r:any;
  export default Donor__r;
}
declare module "@salesforce/schema/Receipt__c.Donor__c" {
  const Donor__c:any;
  export default Donor__c;
}
declare module "@salesforce/schema/Receipt__c.Receipt_Number__c" {
  const Receipt_Number__c:string;
  export default Receipt_Number__c;
}
declare module "@salesforce/schema/Receipt__c.Payment_Status__c" {
  const Payment_Status__c:string;
  export default Payment_Status__c;
}
declare module "@salesforce/schema/Receipt__c.Method_of_Payment__c" {
  const Method_of_Payment__c:string;
  export default Method_of_Payment__c;
}
declare module "@salesforce/schema/Receipt__c.ReceiptEmail__c" {
  const ReceiptEmail__c:string;
  export default ReceiptEmail__c;
}
declare module "@salesforce/schema/Receipt__c.Do_Not_Send_Receipt__c" {
  const Do_Not_Send_Receipt__c:boolean;
  export default Do_Not_Send_Receipt__c;
}
declare module "@salesforce/schema/Receipt__c.Thank_you_Sent__c" {
  const Thank_you_Sent__c:boolean;
  export default Thank_you_Sent__c;
}
declare module "@salesforce/schema/Receipt__c.TodayDate__c" {
  const TodayDate__c:string;
  export default TodayDate__c;
}
declare module "@salesforce/schema/Receipt__c.ReceiptCountUCFED__c" {
  const ReceiptCountUCFED__c:number;
  export default ReceiptCountUCFED__c;
}
declare module "@salesforce/schema/Receipt__c.ReceiptCount__c" {
  const ReceiptCount__c:number;
  export default ReceiptCount__c;
}
declare module "@salesforce/schema/Receipt__c.Receipt_Date_Email__c" {
  const Receipt_Date_Email__c:string;
  export default Receipt_Date_Email__c;
}
declare module "@salesforce/schema/Receipt__c.Last_Receipt_Date__c" {
  const Last_Receipt_Date__c:any;
  export default Last_Receipt_Date__c;
}
declare module "@salesforce/schema/Receipt__c.Next_Receipt_Date__c" {
  const Next_Receipt_Date__c:any;
  export default Next_Receipt_Date__c;
}
declare module "@salesforce/schema/Receipt__c.Date_Signed_Receipts__c" {
  const Date_Signed_Receipts__c:any;
  export default Date_Signed_Receipts__c;
}
declare module "@salesforce/schema/Receipt__c.Create_Datetime__c" {
  const Create_Datetime__c:any;
  export default Create_Datetime__c;
}
declare module "@salesforce/schema/Receipt__c.RecName__c" {
  const RecName__c:string;
  export default RecName__c;
}
declare module "@salesforce/schema/Receipt__c.Payment_Commission__c" {
  const Payment_Commission__c:number;
  export default Payment_Commission__c;
}
declare module "@salesforce/schema/Receipt__c.Key_Formula__c" {
  const Key_Formula__c:string;
  export default Key_Formula__c;
}
declare module "@salesforce/schema/Receipt__c.Key__c" {
  const Key__c:string;
  export default Key__c;
}
declare module "@salesforce/schema/Receipt__c.CC_Batch__c" {
  const CC_Batch__c:number;
  export default CC_Batch__c;
}
declare module "@salesforce/schema/Receipt__c.Receipt_Total_Checker__c" {
  const Receipt_Total_Checker__c:number;
  export default Receipt_Total_Checker__c;
}
declare module "@salesforce/schema/Receipt__c.Receipt_Query__c" {
  const Receipt_Query__c:boolean;
  export default Receipt_Query__c;
}
declare module "@salesforce/schema/Receipt__c.Query_Reason__c" {
  const Query_Reason__c:string;
  export default Query_Reason__c;
}
declare module "@salesforce/schema/Receipt__c.Welfare_Outstanding__c" {
  const Welfare_Outstanding__c:number;
  export default Welfare_Outstanding__c;
}
declare module "@salesforce/schema/Receipt__c.IUA_Outstanding__c" {
  const IUA_Outstanding__c:number;
  export default IUA_Outstanding__c;
}
declare module "@salesforce/schema/Receipt__c.UCF_Communal_Outstanding__c" {
  const UCF_Communal_Outstanding__c:number;
  export default UCF_Communal_Outstanding__c;
}
declare module "@salesforce/schema/Receipt__c.UCF_Education_Outstanding__c" {
  const UCF_Education_Outstanding__c:number;
  export default UCF_Education_Outstanding__c;
}
declare module "@salesforce/schema/Receipt__c.Query_Status__c" {
  const Query_Status__c:string;
  export default Query_Status__c;
}
declare module "@salesforce/schema/Receipt__c.Bank_Code_Unpaid__c" {
  const Bank_Code_Unpaid__c:string;
  export default Bank_Code_Unpaid__c;
}
declare module "@salesforce/schema/Receipt__c.Query_Resolution__c" {
  const Query_Resolution__c:string;
  export default Query_Resolution__c;
}
declare module "@salesforce/schema/Receipt__c.Query_Notes__c" {
  const Query_Notes__c:string;
  export default Query_Notes__c;
}
declare module "@salesforce/schema/Receipt__c.Reference_Details__c" {
  const Reference_Details__c:string;
  export default Reference_Details__c;
}
declare module "@salesforce/schema/Receipt__c.IUA_PriorValue__c" {
  const IUA_PriorValue__c:number;
  export default IUA_PriorValue__c;
}
declare module "@salesforce/schema/Receipt__c.Prior_Campaign_Value__c" {
  const Prior_Campaign_Value__c:string;
  export default Prior_Campaign_Value__c;
}
declare module "@salesforce/schema/Receipt__c.Receipt_Total__c" {
  const Receipt_Total__c:number;
  export default Receipt_Total__c;
}
declare module "@salesforce/schema/Receipt__c.Welfare_Portion__c" {
  const Welfare_Portion__c:number;
  export default Welfare_Portion__c;
}
declare module "@salesforce/schema/Receipt__c.IUA_Portion__c" {
  const IUA_Portion__c:number;
  export default IUA_Portion__c;
}
declare module "@salesforce/schema/Receipt__c.UCF_Ed_Portion__c" {
  const UCF_Ed_Portion__c:number;
  export default UCF_Ed_Portion__c;
}
declare module "@salesforce/schema/Receipt__c.UCF_Comm_Portion__c" {
  const UCF_Comm_Portion__c:number;
  export default UCF_Comm_Portion__c;
}
declare module "@salesforce/schema/Receipt__c.Welfare_PriorValue__c" {
  const Welfare_PriorValue__c:number;
  export default Welfare_PriorValue__c;
}
declare module "@salesforce/schema/Receipt__c.UCF_Ed_PriorValue__c" {
  const UCF_Ed_PriorValue__c:number;
  export default UCF_Ed_PriorValue__c;
}
declare module "@salesforce/schema/Receipt__c.UCF_Comm_PriorValue__c" {
  const UCF_Comm_PriorValue__c:number;
  export default UCF_Comm_PriorValue__c;
}
declare module "@salesforce/schema/Receipt__c.Pledge_Prior_Value__c" {
  const Pledge_Prior_Value__c:number;
  export default Pledge_Prior_Value__c;
}
declare module "@salesforce/schema/Receipt__c.Modified_Date__c" {
  const Modified_Date__c:any;
  export default Modified_Date__c;
}
declare module "@salesforce/schema/Receipt__c.Created_Date__c" {
  const Created_Date__c:any;
  export default Created_Date__c;
}
declare module "@salesforce/schema/Receipt__c.Receipt_Adjustment__c" {
  const Receipt_Adjustment__c:boolean;
  export default Receipt_Adjustment__c;
}
declare module "@salesforce/schema/Receipt__c.Receipt_Type__c" {
  const Receipt_Type__c:string;
  export default Receipt_Type__c;
}
