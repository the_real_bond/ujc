declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__c.CnP_PaaS__API_User__c" {
  const CnP_PaaS__API_User__c:string;
  export default CnP_PaaS__API_User__c;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__c.CnP_PaaS__API_key__c" {
  const CnP_PaaS__API_key__c:string;
  export default CnP_PaaS__API_key__c;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__c.CnP_PaaS__Enable_SSL__c" {
  const CnP_PaaS__Enable_SSL__c:boolean;
  export default CnP_PaaS__Enable_SSL__c;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__c.CnP_PaaS__Outgoing_Mail_Server_SMTP__c" {
  const CnP_PaaS__Outgoing_Mail_Server_SMTP__c:string;
  export default CnP_PaaS__Outgoing_Mail_Server_SMTP__c;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__c.CnP_PaaS__Send_grid__c" {
  const CnP_PaaS__Send_grid__c:boolean;
  export default CnP_PaaS__Send_grid__c;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__c.CnP_PaaS__Sender_Email__c" {
  const CnP_PaaS__Sender_Email__c:string;
  export default CnP_PaaS__Sender_Email__c;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__c.CnP_PaaS__Sender_Name__c" {
  const CnP_PaaS__Sender_Name__c:string;
  export default CnP_PaaS__Sender_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__c.CnP_PaaS__Server_Port_Number_SMTP__c" {
  const CnP_PaaS__Server_Port_Number_SMTP__c:string;
  export default CnP_PaaS__Server_Port_Number_SMTP__c;
}
