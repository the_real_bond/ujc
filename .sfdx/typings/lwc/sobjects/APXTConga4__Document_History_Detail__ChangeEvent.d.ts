declare module "@salesforce/schema/APXTConga4__Document_History_Detail__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/APXTConga4__Document_History_Detail__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/APXTConga4__Document_History_Detail__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/APXTConga4__Document_History_Detail__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/APXTConga4__Document_History_Detail__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/APXTConga4__Document_History_Detail__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/APXTConga4__Document_History_Detail__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/APXTConga4__Document_History_Detail__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/APXTConga4__Document_History_Detail__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/APXTConga4__Document_History_Detail__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/APXTConga4__Document_History_Detail__ChangeEvent.APXTConga4__Document_History__c" {
  const APXTConga4__Document_History__c:any;
  export default APXTConga4__Document_History__c;
}
declare module "@salesforce/schema/APXTConga4__Document_History_Detail__ChangeEvent.APXTConga4__Document_Status__c" {
  const APXTConga4__Document_Status__c:string;
  export default APXTConga4__Document_Status__c;
}
declare module "@salesforce/schema/APXTConga4__Document_History_Detail__ChangeEvent.APXTConga4__Email_Address__c" {
  const APXTConga4__Email_Address__c:string;
  export default APXTConga4__Email_Address__c;
}
declare module "@salesforce/schema/APXTConga4__Document_History_Detail__ChangeEvent.APXTConga4__Last_Viewed__c" {
  const APXTConga4__Last_Viewed__c:any;
  export default APXTConga4__Last_Viewed__c;
}
declare module "@salesforce/schema/APXTConga4__Document_History_Detail__ChangeEvent.APXTConga4__Number_of_Views__c" {
  const APXTConga4__Number_of_Views__c:number;
  export default APXTConga4__Number_of_Views__c;
}
declare module "@salesforce/schema/APXTConga4__Document_History_Detail__ChangeEvent.APXTConga4__SMS_Number__c" {
  const APXTConga4__SMS_Number__c:string;
  export default APXTConga4__SMS_Number__c;
}
