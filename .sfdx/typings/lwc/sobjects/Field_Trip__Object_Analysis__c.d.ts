declare module "@salesforce/schema/Field_Trip__Object_Analysis__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__c.Field_Trip__Filter__c" {
  const Field_Trip__Filter__c:string;
  export default Field_Trip__Filter__c;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__c.Field_Trip__Last_Analyzed__c" {
  const Field_Trip__Last_Analyzed__c:any;
  export default Field_Trip__Last_Analyzed__c;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__c.Field_Trip__Last_Batch_Id__c" {
  const Field_Trip__Last_Batch_Id__c:string;
  export default Field_Trip__Last_Batch_Id__c;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__c.Field_Trip__Object_Label__c" {
  const Field_Trip__Object_Label__c:string;
  export default Field_Trip__Object_Label__c;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__c.Field_Trip__Object_Name__c" {
  const Field_Trip__Object_Name__c:string;
  export default Field_Trip__Object_Name__c;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__c.Field_Trip__Record_Types__c" {
  const Field_Trip__Record_Types__c:number;
  export default Field_Trip__Record_Types__c;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__c.Field_Trip__Records__c" {
  const Field_Trip__Records__c:number;
  export default Field_Trip__Records__c;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__c.Field_Trip__Tally__c" {
  const Field_Trip__Tally__c:number;
  export default Field_Trip__Tally__c;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__c.Field_Trip__isCustom__c" {
  const Field_Trip__isCustom__c:boolean;
  export default Field_Trip__isCustom__c;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__c.Field_Trip__Fields__c" {
  const Field_Trip__Fields__c:number;
  export default Field_Trip__Fields__c;
}
