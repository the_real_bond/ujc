declare module "@salesforce/schema/CnP_PaaS_EVT__Custom_fields__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Custom_fields__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Custom_fields__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Custom_fields__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Custom_fields__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Custom_fields__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Custom_fields__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Custom_fields__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Custom_fields__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Custom_fields__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Custom_fields__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Custom_fields__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Custom_fields__c.CnP_PaaS_EVT__Create_new_group__c" {
  const CnP_PaaS_EVT__Create_new_group__c:string;
  export default CnP_PaaS_EVT__Create_new_group__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Custom_fields__c.CnP_PaaS_EVT__Default_value__c" {
  const CnP_PaaS_EVT__Default_value__c:string;
  export default CnP_PaaS_EVT__Default_value__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Custom_fields__c.CnP_PaaS_EVT__Event__r" {
  const CnP_PaaS_EVT__Event__r:any;
  export default CnP_PaaS_EVT__Event__r;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Custom_fields__c.CnP_PaaS_EVT__Event__c" {
  const CnP_PaaS_EVT__Event__c:any;
  export default CnP_PaaS_EVT__Event__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Custom_fields__c.CnP_PaaS_EVT__Field_Options__c" {
  const CnP_PaaS_EVT__Field_Options__c:string;
  export default CnP_PaaS_EVT__Field_Options__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Custom_fields__c.CnP_PaaS_EVT__Field_name__c" {
  const CnP_PaaS_EVT__Field_name__c:string;
  export default CnP_PaaS_EVT__Field_name__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Custom_fields__c.CnP_PaaS_EVT__Field_type__c" {
  const CnP_PaaS_EVT__Field_type__c:string;
  export default CnP_PaaS_EVT__Field_type__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Custom_fields__c.CnP_PaaS_EVT__Field_values__c" {
  const CnP_PaaS_EVT__Field_values__c:string;
  export default CnP_PaaS_EVT__Field_values__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Custom_fields__c.CnP_PaaS_EVT__Int_order_Number__c" {
  const CnP_PaaS_EVT__Int_order_Number__c:number;
  export default CnP_PaaS_EVT__Int_order_Number__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Custom_fields__c.CnP_PaaS_EVT__Order_number__c" {
  const CnP_PaaS_EVT__Order_number__c:string;
  export default CnP_PaaS_EVT__Order_number__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Custom_fields__c.CnP_PaaS_EVT__Question_Section__r" {
  const CnP_PaaS_EVT__Question_Section__r:any;
  export default CnP_PaaS_EVT__Question_Section__r;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Custom_fields__c.CnP_PaaS_EVT__Question_Section__c" {
  const CnP_PaaS_EVT__Question_Section__c:any;
  export default CnP_PaaS_EVT__Question_Section__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Custom_fields__c.CnP_PaaS_EVT__Question__c" {
  const CnP_PaaS_EVT__Question__c:string;
  export default CnP_PaaS_EVT__Question__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Custom_fields__c.CnP_PaaS_EVT__Registration_level__r" {
  const CnP_PaaS_EVT__Registration_level__r:any;
  export default CnP_PaaS_EVT__Registration_level__r;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Custom_fields__c.CnP_PaaS_EVT__Registration_level__c" {
  const CnP_PaaS_EVT__Registration_level__c:any;
  export default CnP_PaaS_EVT__Registration_level__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Custom_fields__c.CnP_PaaS_EVT__Required__c" {
  const CnP_PaaS_EVT__Required__c:boolean;
  export default CnP_PaaS_EVT__Required__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Custom_fields__c.CnP_PaaS_EVT__Section_order__c" {
  const CnP_PaaS_EVT__Section_order__c:string;
  export default CnP_PaaS_EVT__Section_order__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Custom_fields__c.CnP_PaaS_EVT__Select_group__c" {
  const CnP_PaaS_EVT__Select_group__c:string;
  export default CnP_PaaS_EVT__Select_group__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Custom_fields__c.CnP_PaaS_EVT__Visible__c" {
  const CnP_PaaS_EVT__Visible__c:boolean;
  export default CnP_PaaS_EVT__Visible__c;
}
