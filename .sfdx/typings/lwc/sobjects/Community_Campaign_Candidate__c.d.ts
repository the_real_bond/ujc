declare module "@salesforce/schema/Community_Campaign_Candidate__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__c.Community_Campaign__r" {
  const Community_Campaign__r:any;
  export default Community_Campaign__r;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__c.Community_Campaign__c" {
  const Community_Campaign__c:any;
  export default Community_Campaign__c;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__c.Active__c" {
  const Active__c:boolean;
  export default Active__c;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__c.Biography__c" {
  const Biography__c:string;
  export default Biography__c;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__c.Community_Register__r" {
  const Community_Register__r:any;
  export default Community_Register__r;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__c.Community_Register__c" {
  const Community_Register__c:any;
  export default Community_Register__c;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__c.First_Name__c" {
  const First_Name__c:string;
  export default First_Name__c;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__c.Last_Name__c" {
  const Last_Name__c:string;
  export default Last_Name__c;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__c.Motivation__c" {
  const Motivation__c:string;
  export default Motivation__c;
}
declare module "@salesforce/schema/Community_Campaign_Candidate__c.Photo__c" {
  const Photo__c:string;
  export default Photo__c;
}
