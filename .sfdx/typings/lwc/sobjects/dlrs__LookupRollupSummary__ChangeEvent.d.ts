declare module "@salesforce/schema/dlrs__LookupRollupSummary__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__ChangeEvent.dlrs__Active__c" {
  const dlrs__Active__c:boolean;
  export default dlrs__Active__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__ChangeEvent.dlrs__AggregateOperation__c" {
  const dlrs__AggregateOperation__c:string;
  export default dlrs__AggregateOperation__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__ChangeEvent.dlrs__AggregateResultField__c" {
  const dlrs__AggregateResultField__c:string;
  export default dlrs__AggregateResultField__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__ChangeEvent.dlrs__CalculateJobId__c" {
  const dlrs__CalculateJobId__c:string;
  export default dlrs__CalculateJobId__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__ChangeEvent.dlrs__CalculationMode__c" {
  const dlrs__CalculationMode__c:string;
  export default dlrs__CalculationMode__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__ChangeEvent.dlrs__ChildObject__c" {
  const dlrs__ChildObject__c:string;
  export default dlrs__ChildObject__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__ChangeEvent.dlrs__FieldToAggregate__c" {
  const dlrs__FieldToAggregate__c:string;
  export default dlrs__FieldToAggregate__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__ChangeEvent.dlrs__ParentObject__c" {
  const dlrs__ParentObject__c:string;
  export default dlrs__ParentObject__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__ChangeEvent.dlrs__RelationshipCriteriaFields__c" {
  const dlrs__RelationshipCriteriaFields__c:string;
  export default dlrs__RelationshipCriteriaFields__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__ChangeEvent.dlrs__RelationshipCriteria__c" {
  const dlrs__RelationshipCriteria__c:string;
  export default dlrs__RelationshipCriteria__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__ChangeEvent.dlrs__RelationshipField__c" {
  const dlrs__RelationshipField__c:string;
  export default dlrs__RelationshipField__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__ChangeEvent.dlrs__AggregateAllRows__c" {
  const dlrs__AggregateAllRows__c:boolean;
  export default dlrs__AggregateAllRows__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__ChangeEvent.dlrs__CalculationSharingMode__c" {
  const dlrs__CalculationSharingMode__c:string;
  export default dlrs__CalculationSharingMode__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__ChangeEvent.dlrs__ConcatenateDelimiter__c" {
  const dlrs__ConcatenateDelimiter__c:string;
  export default dlrs__ConcatenateDelimiter__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__ChangeEvent.dlrs__Description__c" {
  const dlrs__Description__c:string;
  export default dlrs__Description__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__ChangeEvent.dlrs__FieldToOrderBy__c" {
  const dlrs__FieldToOrderBy__c:string;
  export default dlrs__FieldToOrderBy__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__ChangeEvent.dlrs__RowLimit__c" {
  const dlrs__RowLimit__c:number;
  export default dlrs__RowLimit__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__ChangeEvent.dlrs__TestCodeSeeAllData__c" {
  const dlrs__TestCodeSeeAllData__c:boolean;
  export default dlrs__TestCodeSeeAllData__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__ChangeEvent.dlrs__TestCode__c" {
  const dlrs__TestCode__c:string;
  export default dlrs__TestCode__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__ChangeEvent.dlrs__UniqueName__c" {
  const dlrs__UniqueName__c:string;
  export default dlrs__UniqueName__c;
}
