declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.CnP_PaaS_EVT__Additional_Fee__c" {
  const CnP_PaaS_EVT__Additional_Fee__c:number;
  export default CnP_PaaS_EVT__Additional_Fee__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.CnP_PaaS_EVT__Attendee_Send_NameBadges__c" {
  const CnP_PaaS_EVT__Attendee_Send_NameBadges__c:boolean;
  export default CnP_PaaS_EVT__Attendee_Send_NameBadges__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.CnP_PaaS_EVT__Available_Inventory__c" {
  const CnP_PaaS_EVT__Available_Inventory__c:number;
  export default CnP_PaaS_EVT__Available_Inventory__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.CnP_PaaS_EVT__Campaign__c" {
  const CnP_PaaS_EVT__Campaign__c:any;
  export default CnP_PaaS_EVT__Campaign__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.CnP_PaaS_EVT__Current_inventory__c" {
  const CnP_PaaS_EVT__Current_inventory__c:number;
  export default CnP_PaaS_EVT__Current_inventory__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.CnP_PaaS_EVT__Description__c" {
  const CnP_PaaS_EVT__Description__c:string;
  export default CnP_PaaS_EVT__Description__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.CnP_PaaS_EVT__Display_this_registration_from__c" {
  const CnP_PaaS_EVT__Display_this_registration_from__c:any;
  export default CnP_PaaS_EVT__Display_this_registration_from__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.CnP_PaaS_EVT__End_date_message__c" {
  const CnP_PaaS_EVT__End_date_message__c:string;
  export default CnP_PaaS_EVT__End_date_message__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.CnP_PaaS_EVT__Event_name__c" {
  const CnP_PaaS_EVT__Event_name__c:any;
  export default CnP_PaaS_EVT__Event_name__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.CnP_PaaS_EVT__Fee_label__c" {
  const CnP_PaaS_EVT__Fee_label__c:string;
  export default CnP_PaaS_EVT__Fee_label__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.CnP_PaaS_EVT__Hide_this_registration_from__c" {
  const CnP_PaaS_EVT__Hide_this_registration_from__c:any;
  export default CnP_PaaS_EVT__Hide_this_registration_from__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.CnP_PaaS_EVT__Inventory_Sold__c" {
  const CnP_PaaS_EVT__Inventory_Sold__c:number;
  export default CnP_PaaS_EVT__Inventory_Sold__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.CnP_PaaS_EVT__Limit_for_this_type__c" {
  const CnP_PaaS_EVT__Limit_for_this_type__c:number;
  export default CnP_PaaS_EVT__Limit_for_this_type__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.CnP_PaaS_EVT__Message_to_display_when_limit_is_reached__c" {
  const CnP_PaaS_EVT__Message_to_display_when_limit_is_reached__c:string;
  export default CnP_PaaS_EVT__Message_to_display_when_limit_is_reached__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.CnP_PaaS_EVT__Mode__c" {
  const CnP_PaaS_EVT__Mode__c:string;
  export default CnP_PaaS_EVT__Mode__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.CnP_PaaS_EVT__Name_on_reports__c" {
  const CnP_PaaS_EVT__Name_on_reports__c:string;
  export default CnP_PaaS_EVT__Name_on_reports__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.CnP_PaaS_EVT__Name_on_the_form__c" {
  const CnP_PaaS_EVT__Name_on_the_form__c:string;
  export default CnP_PaaS_EVT__Name_on_the_form__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.CnP_PaaS_EVT__OrderLevel__c" {
  const CnP_PaaS_EVT__OrderLevel__c:number;
  export default CnP_PaaS_EVT__OrderLevel__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.CnP_PaaS_EVT__Price__c" {
  const CnP_PaaS_EVT__Price__c:number;
  export default CnP_PaaS_EVT__Price__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.CnP_PaaS_EVT__Report_Color__c" {
  const CnP_PaaS_EVT__Report_Color__c:string;
  export default CnP_PaaS_EVT__Report_Color__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.CnP_PaaS_EVT__SKU_Code_Level__c" {
  const CnP_PaaS_EVT__SKU_Code_Level__c:string;
  export default CnP_PaaS_EVT__SKU_Code_Level__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.CnP_PaaS_EVT__Select_NameBadge__c" {
  const CnP_PaaS_EVT__Select_NameBadge__c:any;
  export default CnP_PaaS_EVT__Select_NameBadge__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.CnP_PaaS_EVT__Select_Ticket__c" {
  const CnP_PaaS_EVT__Select_Ticket__c:any;
  export default CnP_PaaS_EVT__Select_Ticket__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.CnP_PaaS_EVT__Select_eTicket__c" {
  const CnP_PaaS_EVT__Select_eTicket__c:any;
  export default CnP_PaaS_EVT__Select_eTicket__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.CnP_PaaS_EVT__Send_NameBadges__c" {
  const CnP_PaaS_EVT__Send_NameBadges__c:boolean;
  export default CnP_PaaS_EVT__Send_NameBadges__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.CnP_PaaS_EVT__Send_Tickets__c" {
  const CnP_PaaS_EVT__Send_Tickets__c:boolean;
  export default CnP_PaaS_EVT__Send_Tickets__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.CnP_PaaS_EVT__Send_eTickets__c" {
  const CnP_PaaS_EVT__Send_eTickets__c:boolean;
  export default CnP_PaaS_EVT__Send_eTickets__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.CnP_PaaS_EVT__Tax_Deductible__c" {
  const CnP_PaaS_EVT__Tax_Deductible__c:number;
  export default CnP_PaaS_EVT__Tax_Deductible__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.CnP_PaaS_EVT__Tax_rate__c" {
  const CnP_PaaS_EVT__Tax_rate__c:number;
  export default CnP_PaaS_EVT__Tax_rate__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.CnP_PaaS_EVT__Ticket_Number_Prefix__c" {
  const CnP_PaaS_EVT__Ticket_Number_Prefix__c:string;
  export default CnP_PaaS_EVT__Ticket_Number_Prefix__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Registration_level__ChangeEvent.CnP_PaaS_EVT__Tickets_Included__c" {
  const CnP_PaaS_EVT__Tickets_Included__c:number;
  export default CnP_PaaS_EVT__Tickets_Included__c;
}
