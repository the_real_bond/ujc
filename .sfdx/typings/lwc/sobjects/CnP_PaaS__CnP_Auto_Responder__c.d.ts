declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__American_Express__c" {
  const CnP_PaaS__American_Express__c:boolean;
  export default CnP_PaaS__American_Express__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__Authorized__c" {
  const CnP_PaaS__Authorized__c:boolean;
  export default CnP_PaaS__Authorized__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__BCC_Email_Address__c" {
  const CnP_PaaS__BCC_Email_Address__c:string;
  export default CnP_PaaS__BCC_Email_Address__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__CC_Email_Address__c" {
  const CnP_PaaS__CC_Email_Address__c:string;
  export default CnP_PaaS__CC_Email_Address__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__C_P_Account_Names__c" {
  const CnP_PaaS__C_P_Account_Names__c:string;
  export default CnP_PaaS__C_P_Account_Names__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__Campain_Math_Condition__c" {
  const CnP_PaaS__Campain_Math_Condition__c:string;
  export default CnP_PaaS__Campain_Math_Condition__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__Clickandpledge_SMTP__c" {
  const CnP_PaaS__Clickandpledge_SMTP__c:string;
  export default CnP_PaaS__Clickandpledge_SMTP__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__CnP_API_Settings__r" {
  const CnP_PaaS__CnP_API_Settings__r:any;
  export default CnP_PaaS__CnP_API_Settings__r;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__CnP_API_Settings__c" {
  const CnP_PaaS__CnP_API_Settings__c:any;
  export default CnP_PaaS__CnP_API_Settings__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__CnP_Designer__r" {
  const CnP_PaaS__CnP_Designer__r:any;
  export default CnP_PaaS__CnP_Designer__r;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__CnP_Designer__c" {
  const CnP_PaaS__CnP_Designer__c:any;
  export default CnP_PaaS__CnP_Designer__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__Credit_card__c" {
  const CnP_PaaS__Credit_card__c:boolean;
  export default CnP_PaaS__Credit_card__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__Custom_Payment_Type_Name__c" {
  const CnP_PaaS__Custom_Payment_Type_Name__c:string;
  export default CnP_PaaS__Custom_Payment_Type_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__Custom_Payment_Type__c" {
  const CnP_PaaS__Custom_Payment_Type__c:boolean;
  export default CnP_PaaS__Custom_Payment_Type__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__Declined__c" {
  const CnP_PaaS__Declined__c:boolean;
  export default CnP_PaaS__Declined__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__Delayed_day__c" {
  const CnP_PaaS__Delayed_day__c:string;
  export default CnP_PaaS__Delayed_day__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__Discover__c" {
  const CnP_PaaS__Discover__c:boolean;
  export default CnP_PaaS__Discover__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__Future_Transaction_Type__c" {
  const CnP_PaaS__Future_Transaction_Type__c:string;
  export default CnP_PaaS__Future_Transaction_Type__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__Future_Transaction__c" {
  const CnP_PaaS__Future_Transaction__c:boolean;
  export default CnP_PaaS__Future_Transaction__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__Invoice__c" {
  const CnP_PaaS__Invoice__c:boolean;
  export default CnP_PaaS__Invoice__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__JCB__c" {
  const CnP_PaaS__JCB__c:boolean;
  export default CnP_PaaS__JCB__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__Log_Archive__c" {
  const CnP_PaaS__Log_Archive__c:string;
  export default CnP_PaaS__Log_Archive__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__Mail_From_Address__c" {
  const CnP_PaaS__Mail_From_Address__c:string;
  export default CnP_PaaS__Mail_From_Address__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__Mail_From_Name__c" {
  const CnP_PaaS__Mail_From_Name__c:string;
  export default CnP_PaaS__Mail_From_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__Mail_to__c" {
  const CnP_PaaS__Mail_to__c:string;
  export default CnP_PaaS__Mail_to__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__Master_Card__c" {
  const CnP_PaaS__Master_Card__c:boolean;
  export default CnP_PaaS__Master_Card__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__Payment_Math_Condition__c" {
  const CnP_PaaS__Payment_Math_Condition__c:string;
  export default CnP_PaaS__Payment_Math_Condition__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__Pending__c" {
  const CnP_PaaS__Pending__c:boolean;
  export default CnP_PaaS__Pending__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__Purchase_Order__c" {
  const CnP_PaaS__Purchase_Order__c:boolean;
  export default CnP_PaaS__Purchase_Order__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__Question_Math_Condition__c" {
  const CnP_PaaS__Question_Math_Condition__c:string;
  export default CnP_PaaS__Question_Math_Condition__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__Recipient__c" {
  const CnP_PaaS__Recipient__c:string;
  export default CnP_PaaS__Recipient__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__Recurring_Type__c" {
  const CnP_PaaS__Recurring_Type__c:string;
  export default CnP_PaaS__Recurring_Type__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__Recurring__c" {
  const CnP_PaaS__Recurring__c:boolean;
  export default CnP_PaaS__Recurring__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__Replay_to_Address__c" {
  const CnP_PaaS__Replay_to_Address__c:string;
  export default CnP_PaaS__Replay_to_Address__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__SKU_Math_Condition__c" {
  const CnP_PaaS__SKU_Math_Condition__c:string;
  export default CnP_PaaS__SKU_Math_Condition__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__SMTP_settings__r" {
  const CnP_PaaS__SMTP_settings__r:any;
  export default CnP_PaaS__SMTP_settings__r;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__SMTP_settings__c" {
  const CnP_PaaS__SMTP_settings__c:any;
  export default CnP_PaaS__SMTP_settings__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__Send_Option__c" {
  const CnP_PaaS__Send_Option__c:string;
  export default CnP_PaaS__Send_Option__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__Specific_date__c" {
  const CnP_PaaS__Specific_date__c:any;
  export default CnP_PaaS__Specific_date__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__Specific_date_and_time__c" {
  const CnP_PaaS__Specific_date_and_time__c:any;
  export default CnP_PaaS__Specific_date_and_time__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__Status__c" {
  const CnP_PaaS__Status__c:string;
  export default CnP_PaaS__Status__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__Stop_dele__c" {
  const CnP_PaaS__Stop_dele__c:string;
  export default CnP_PaaS__Stop_dele__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__Subject__c" {
  const CnP_PaaS__Subject__c:string;
  export default CnP_PaaS__Subject__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__Subscription_Installment_type__c" {
  const CnP_PaaS__Subscription_Installment_type__c:string;
  export default CnP_PaaS__Subscription_Installment_type__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__Tags__c" {
  const CnP_PaaS__Tags__c:string;
  export default CnP_PaaS__Tags__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__Visa__c" {
  const CnP_PaaS__Visa__c:boolean;
  export default CnP_PaaS__Visa__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__WID_Math_Condition__c" {
  const CnP_PaaS__WID_Math_Condition__c:string;
  export default CnP_PaaS__WID_Math_Condition__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__eCheck__c" {
  const CnP_PaaS__eCheck__c:boolean;
  export default CnP_PaaS__eCheck__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__hour__c" {
  const CnP_PaaS__hour__c:string;
  export default CnP_PaaS__hour__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder__c.CnP_PaaS__minute__c" {
  const CnP_PaaS__minute__c:string;
  export default CnP_PaaS__minute__c;
}
