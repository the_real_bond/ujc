declare module "@salesforce/schema/APXT_CMQR__Conga_Merge_Query__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/APXT_CMQR__Conga_Merge_Query__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/APXT_CMQR__Conga_Merge_Query__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/APXT_CMQR__Conga_Merge_Query__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/APXT_CMQR__Conga_Merge_Query__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/APXT_CMQR__Conga_Merge_Query__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/APXT_CMQR__Conga_Merge_Query__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/APXT_CMQR__Conga_Merge_Query__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/APXT_CMQR__Conga_Merge_Query__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/APXT_CMQR__Conga_Merge_Query__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/APXT_CMQR__Conga_Merge_Query__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/APXT_CMQR__Conga_Merge_Query__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/APXT_CMQR__Conga_Merge_Query__ChangeEvent.APXT_CMQR__Description__c" {
  const APXT_CMQR__Description__c:string;
  export default APXT_CMQR__Description__c;
}
declare module "@salesforce/schema/APXT_CMQR__Conga_Merge_Query__ChangeEvent.APXT_CMQR__Name__c" {
  const APXT_CMQR__Name__c:string;
  export default APXT_CMQR__Name__c;
}
declare module "@salesforce/schema/APXT_CMQR__Conga_Merge_Query__ChangeEvent.APXT_CMQR__Query__c" {
  const APXT_CMQR__Query__c:string;
  export default APXT_CMQR__Query__c;
}
