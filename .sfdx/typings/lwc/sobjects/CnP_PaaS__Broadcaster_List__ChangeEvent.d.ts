declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.CnP_PaaS__BroadCaster_Service__c" {
  const CnP_PaaS__BroadCaster_Service__c:any;
  export default CnP_PaaS__BroadCaster_Service__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.CnP_PaaS__Avg_Sub_Rate__c" {
  const CnP_PaaS__Avg_Sub_Rate__c:number;
  export default CnP_PaaS__Avg_Sub_Rate__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.CnP_PaaS__Avg_Unsub_Rate__c" {
  const CnP_PaaS__Avg_Unsub_Rate__c:number;
  export default CnP_PaaS__Avg_Unsub_Rate__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.CnP_PaaS__Beamer_Address__c" {
  const CnP_PaaS__Beamer_Address__c:string;
  export default CnP_PaaS__Beamer_Address__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.CnP_PaaS__Campaign_Count__c" {
  const CnP_PaaS__Campaign_Count__c:number;
  export default CnP_PaaS__Campaign_Count__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.CnP_PaaS__Cleaned_Count_Since_Send__c" {
  const CnP_PaaS__Cleaned_Count_Since_Send__c:number;
  export default CnP_PaaS__Cleaned_Count_Since_Send__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.CnP_PaaS__Cleaned_Count__c" {
  const CnP_PaaS__Cleaned_Count__c:number;
  export default CnP_PaaS__Cleaned_Count__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.CnP_PaaS__Click_Rate__c" {
  const CnP_PaaS__Click_Rate__c:number;
  export default CnP_PaaS__Click_Rate__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.CnP_PaaS__CnP_Dynamic_Report__c" {
  const CnP_PaaS__CnP_Dynamic_Report__c:any;
  export default CnP_PaaS__CnP_Dynamic_Report__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.CnP_PaaS__Date_Created__c" {
  const CnP_PaaS__Date_Created__c:string;
  export default CnP_PaaS__Date_Created__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.CnP_PaaS__Default_From_Email__c" {
  const CnP_PaaS__Default_From_Email__c:string;
  export default CnP_PaaS__Default_From_Email__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.CnP_PaaS__Default_From_Name__c" {
  const CnP_PaaS__Default_From_Name__c:string;
  export default CnP_PaaS__Default_From_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.CnP_PaaS__Default_Language__c" {
  const CnP_PaaS__Default_Language__c:string;
  export default CnP_PaaS__Default_Language__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.CnP_PaaS__Default_Subject__c" {
  const CnP_PaaS__Default_Subject__c:string;
  export default CnP_PaaS__Default_Subject__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.CnP_PaaS__Email_Type_Option__c" {
  const CnP_PaaS__Email_Type_Option__c:boolean;
  export default CnP_PaaS__Email_Type_Option__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.CnP_PaaS__Group_Count__c" {
  const CnP_PaaS__Group_Count__c:number;
  export default CnP_PaaS__Group_Count__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.CnP_PaaS__Grouping_Count__c" {
  const CnP_PaaS__Grouping_Count__c:number;
  export default CnP_PaaS__Grouping_Count__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.CnP_PaaS__List_Id__c" {
  const CnP_PaaS__List_Id__c:string;
  export default CnP_PaaS__List_Id__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.CnP_PaaS__List_Rating__c" {
  const CnP_PaaS__List_Rating__c:number;
  export default CnP_PaaS__List_Rating__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.CnP_PaaS__Member_Count_Since_Send__c" {
  const CnP_PaaS__Member_Count_Since_Send__c:number;
  export default CnP_PaaS__Member_Count_Since_Send__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.CnP_PaaS__Member_Count__c" {
  const CnP_PaaS__Member_Count__c:number;
  export default CnP_PaaS__Member_Count__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.CnP_PaaS__Merge_Var_Count__c" {
  const CnP_PaaS__Merge_Var_Count__c:number;
  export default CnP_PaaS__Merge_Var_Count__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.CnP_PaaS__Open_Rate__c" {
  const CnP_PaaS__Open_Rate__c:number;
  export default CnP_PaaS__Open_Rate__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.CnP_PaaS__Subscribe_Url_Long__c" {
  const CnP_PaaS__Subscribe_Url_Long__c:string;
  export default CnP_PaaS__Subscribe_Url_Long__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.CnP_PaaS__Subscribe_Url_Short__c" {
  const CnP_PaaS__Subscribe_Url_Short__c:string;
  export default CnP_PaaS__Subscribe_Url_Short__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.CnP_PaaS__Target_Sub_Rate__c" {
  const CnP_PaaS__Target_Sub_Rate__c:number;
  export default CnP_PaaS__Target_Sub_Rate__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.CnP_PaaS__Unsubscribe_Count_Since_Send__c" {
  const CnP_PaaS__Unsubscribe_Count_Since_Send__c:number;
  export default CnP_PaaS__Unsubscribe_Count_Since_Send__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.CnP_PaaS__Unsubscribe_Count__c" {
  const CnP_PaaS__Unsubscribe_Count__c:number;
  export default CnP_PaaS__Unsubscribe_Count__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.CnP_PaaS__Use_Awesomebar__c" {
  const CnP_PaaS__Use_Awesomebar__c:boolean;
  export default CnP_PaaS__Use_Awesomebar__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.CnP_PaaS__Visibility__c" {
  const CnP_PaaS__Visibility__c:string;
  export default CnP_PaaS__Visibility__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_List__ChangeEvent.CnP_PaaS__Web_Id__c" {
  const CnP_PaaS__Web_Id__c:number;
  export default CnP_PaaS__Web_Id__c;
}
