declare module "@salesforce/schema/PowerLoader__PowerLoaderField__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderField__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderField__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderField__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderField__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderField__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderField__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderField__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderField__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderField__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderField__c.PowerLoader__PowerLoaderMapping__r" {
  const PowerLoader__PowerLoaderMapping__r:any;
  export default PowerLoader__PowerLoaderMapping__r;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderField__c.PowerLoader__PowerLoaderMapping__c" {
  const PowerLoader__PowerLoaderMapping__c:any;
  export default PowerLoader__PowerLoaderMapping__c;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderField__c.PowerLoader__CSVField__c" {
  const PowerLoader__CSVField__c:string;
  export default PowerLoader__CSVField__c;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderField__c.PowerLoader__ObjField__c" {
  const PowerLoader__ObjField__c:string;
  export default PowerLoader__ObjField__c;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderField__c.PowerLoader__RelField__c" {
  const PowerLoader__RelField__c:string;
  export default PowerLoader__RelField__c;
}
