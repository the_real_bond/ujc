declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__ChangeEvent.CnP_PaaS__API_User__c" {
  const CnP_PaaS__API_User__c:string;
  export default CnP_PaaS__API_User__c;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__ChangeEvent.CnP_PaaS__API_key__c" {
  const CnP_PaaS__API_key__c:string;
  export default CnP_PaaS__API_key__c;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__ChangeEvent.CnP_PaaS__Enable_SSL__c" {
  const CnP_PaaS__Enable_SSL__c:boolean;
  export default CnP_PaaS__Enable_SSL__c;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__ChangeEvent.CnP_PaaS__Outgoing_Mail_Server_SMTP__c" {
  const CnP_PaaS__Outgoing_Mail_Server_SMTP__c:string;
  export default CnP_PaaS__Outgoing_Mail_Server_SMTP__c;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__ChangeEvent.CnP_PaaS__Send_grid__c" {
  const CnP_PaaS__Send_grid__c:boolean;
  export default CnP_PaaS__Send_grid__c;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__ChangeEvent.CnP_PaaS__Sender_Email__c" {
  const CnP_PaaS__Sender_Email__c:string;
  export default CnP_PaaS__Sender_Email__c;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__ChangeEvent.CnP_PaaS__Sender_Name__c" {
  const CnP_PaaS__Sender_Name__c:string;
  export default CnP_PaaS__Sender_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__SMTP_settings__ChangeEvent.CnP_PaaS__Server_Port_Number_SMTP__c" {
  const CnP_PaaS__Server_Port_Number_SMTP__c:string;
  export default CnP_PaaS__Server_Port_Number_SMTP__c;
}
