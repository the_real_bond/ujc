declare module "@salesforce/schema/CnP_PaaS_EVT__Discount_plan__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Discount_plan__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Discount_plan__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Discount_plan__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Discount_plan__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Discount_plan__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Discount_plan__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Discount_plan__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Discount_plan__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Discount_plan__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Discount_plan__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Discount_plan__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Discount_plan__ChangeEvent.CnP_PaaS_EVT__Available_Inventory__c" {
  const CnP_PaaS_EVT__Available_Inventory__c:number;
  export default CnP_PaaS_EVT__Available_Inventory__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Discount_plan__ChangeEvent.CnP_PaaS_EVT__C_P_Discount_Plan__c" {
  const CnP_PaaS_EVT__C_P_Discount_Plan__c:any;
  export default CnP_PaaS_EVT__C_P_Discount_Plan__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Discount_plan__ChangeEvent.CnP_PaaS_EVT__Coupon_Code_value__c" {
  const CnP_PaaS_EVT__Coupon_Code_value__c:string;
  export default CnP_PaaS_EVT__Coupon_Code_value__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Discount_plan__ChangeEvent.CnP_PaaS_EVT__Coupon_code__c" {
  const CnP_PaaS_EVT__Coupon_code__c:string;
  export default CnP_PaaS_EVT__Coupon_code__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Discount_plan__ChangeEvent.CnP_PaaS_EVT__End_of_plan__c" {
  const CnP_PaaS_EVT__End_of_plan__c:string;
  export default CnP_PaaS_EVT__End_of_plan__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Discount_plan__ChangeEvent.CnP_PaaS_EVT__Event_name__c" {
  const CnP_PaaS_EVT__Event_name__c:any;
  export default CnP_PaaS_EVT__Event_name__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Discount_plan__ChangeEvent.CnP_PaaS_EVT__Inventory_Sold__c" {
  const CnP_PaaS_EVT__Inventory_Sold__c:number;
  export default CnP_PaaS_EVT__Inventory_Sold__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Discount_plan__ChangeEvent.CnP_PaaS_EVT__Max_Amount__c" {
  const CnP_PaaS_EVT__Max_Amount__c:number;
  export default CnP_PaaS_EVT__Max_Amount__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Discount_plan__ChangeEvent.CnP_PaaS_EVT__Max_number__c" {
  const CnP_PaaS_EVT__Max_number__c:string;
  export default CnP_PaaS_EVT__Max_number__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Discount_plan__ChangeEvent.CnP_PaaS_EVT__Min_Amount__c" {
  const CnP_PaaS_EVT__Min_Amount__c:number;
  export default CnP_PaaS_EVT__Min_Amount__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Discount_plan__ChangeEvent.CnP_PaaS_EVT__Min_number__c" {
  const CnP_PaaS_EVT__Min_number__c:string;
  export default CnP_PaaS_EVT__Min_number__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Discount_plan__ChangeEvent.CnP_PaaS_EVT__Registration_level__c" {
  const CnP_PaaS_EVT__Registration_level__c:any;
  export default CnP_PaaS_EVT__Registration_level__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Discount_plan__ChangeEvent.CnP_PaaS_EVT__Start_date__c" {
  const CnP_PaaS_EVT__Start_date__c:any;
  export default CnP_PaaS_EVT__Start_date__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Discount_plan__ChangeEvent.CnP_PaaS_EVT__Total_Inventory__c" {
  const CnP_PaaS_EVT__Total_Inventory__c:number;
  export default CnP_PaaS_EVT__Total_Inventory__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Discount_plan__ChangeEvent.CnP_PaaS_EVT__Total_discount__c" {
  const CnP_PaaS_EVT__Total_discount__c:number;
  export default CnP_PaaS_EVT__Total_discount__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Discount_plan__ChangeEvent.CnP_PaaS_EVT__discount__c" {
  const CnP_PaaS_EVT__discount__c:string;
  export default CnP_PaaS_EVT__discount__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Discount_plan__ChangeEvent.CnP_PaaS_EVT__expire_date__c" {
  const CnP_PaaS_EVT__expire_date__c:any;
  export default CnP_PaaS_EVT__expire_date__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Discount_plan__ChangeEvent.CnP_PaaS_EVT__fixed_discount__c" {
  const CnP_PaaS_EVT__fixed_discount__c:string;
  export default CnP_PaaS_EVT__fixed_discount__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Discount_plan__ChangeEvent.CnP_PaaS_EVT__inventory__c" {
  const CnP_PaaS_EVT__inventory__c:string;
  export default CnP_PaaS_EVT__inventory__c;
}
