declare module "@salesforce/schema/CloudingoAgent__Log__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CloudingoAgent__Log__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CloudingoAgent__Log__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CloudingoAgent__Log__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CloudingoAgent__Log__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CloudingoAgent__Log__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CloudingoAgent__Log__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CloudingoAgent__Log__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CloudingoAgent__Log__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CloudingoAgent__Log__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CloudingoAgent__Log__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CloudingoAgent__Log__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CloudingoAgent__Log__c.CloudingoAgent__CreatedDate__c" {
  const CloudingoAgent__CreatedDate__c:any;
  export default CloudingoAgent__CreatedDate__c;
}
declare module "@salesforce/schema/CloudingoAgent__Log__c.CloudingoAgent__Message__c" {
  const CloudingoAgent__Message__c:string;
  export default CloudingoAgent__Message__c;
}
declare module "@salesforce/schema/CloudingoAgent__Log__c.CloudingoAgent__Priority__c" {
  const CloudingoAgent__Priority__c:string;
  export default CloudingoAgent__Priority__c;
}
