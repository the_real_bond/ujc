declare module "@salesforce/schema/CnP_PaaS__C_P_Designer_Content_Data__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Designer_Content_Data__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Designer_Content_Data__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Designer_Content_Data__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Designer_Content_Data__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Designer_Content_Data__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Designer_Content_Data__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Designer_Content_Data__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Designer_Content_Data__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Designer_Content_Data__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Designer_Content_Data__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Designer_Content_Data__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Designer_Content_Data__ChangeEvent.CnP_PaaS__C_P_Mail_Content_Data__c" {
  const CnP_PaaS__C_P_Mail_Content_Data__c:string;
  export default CnP_PaaS__C_P_Mail_Content_Data__c;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Designer_Content_Data__ChangeEvent.CnP_PaaS__CnP_Designer__c" {
  const CnP_PaaS__CnP_Designer__c:any;
  export default CnP_PaaS__CnP_Designer__c;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Designer_Content_Data__ChangeEvent.CnP_PaaS__CnP_Mail_Content_Data__c" {
  const CnP_PaaS__CnP_Mail_Content_Data__c:string;
  export default CnP_PaaS__CnP_Mail_Content_Data__c;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Designer_Content_Data__ChangeEvent.CnP_PaaS__Order_info__c" {
  const CnP_PaaS__Order_info__c:number;
  export default CnP_PaaS__Order_info__c;
}
