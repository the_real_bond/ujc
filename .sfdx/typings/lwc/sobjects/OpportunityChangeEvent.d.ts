declare module "@salesforce/schema/OpportunityChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/OpportunityChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/OpportunityChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/OpportunityChangeEvent.Account" {
  const Account:any;
  export default Account;
}
declare module "@salesforce/schema/OpportunityChangeEvent.AccountId" {
  const AccountId:any;
  export default AccountId;
}
declare module "@salesforce/schema/OpportunityChangeEvent.RecordType" {
  const RecordType:any;
  export default RecordType;
}
declare module "@salesforce/schema/OpportunityChangeEvent.RecordTypeId" {
  const RecordTypeId:any;
  export default RecordTypeId;
}
declare module "@salesforce/schema/OpportunityChangeEvent.IsPrivate" {
  const IsPrivate:boolean;
  export default IsPrivate;
}
declare module "@salesforce/schema/OpportunityChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/OpportunityChangeEvent.Description" {
  const Description:string;
  export default Description;
}
declare module "@salesforce/schema/OpportunityChangeEvent.StageName" {
  const StageName:string;
  export default StageName;
}
declare module "@salesforce/schema/OpportunityChangeEvent.Amount" {
  const Amount:number;
  export default Amount;
}
declare module "@salesforce/schema/OpportunityChangeEvent.ExpectedRevenue" {
  const ExpectedRevenue:number;
  export default ExpectedRevenue;
}
declare module "@salesforce/schema/OpportunityChangeEvent.TotalOpportunityQuantity" {
  const TotalOpportunityQuantity:number;
  export default TotalOpportunityQuantity;
}
declare module "@salesforce/schema/OpportunityChangeEvent.CloseDate" {
  const CloseDate:any;
  export default CloseDate;
}
declare module "@salesforce/schema/OpportunityChangeEvent.Type" {
  const Type:string;
  export default Type;
}
declare module "@salesforce/schema/OpportunityChangeEvent.NextStep" {
  const NextStep:string;
  export default NextStep;
}
declare module "@salesforce/schema/OpportunityChangeEvent.LeadSource" {
  const LeadSource:string;
  export default LeadSource;
}
declare module "@salesforce/schema/OpportunityChangeEvent.IsClosed" {
  const IsClosed:boolean;
  export default IsClosed;
}
declare module "@salesforce/schema/OpportunityChangeEvent.IsWon" {
  const IsWon:boolean;
  export default IsWon;
}
declare module "@salesforce/schema/OpportunityChangeEvent.ForecastCategory" {
  const ForecastCategory:string;
  export default ForecastCategory;
}
declare module "@salesforce/schema/OpportunityChangeEvent.ForecastCategoryName" {
  const ForecastCategoryName:string;
  export default ForecastCategoryName;
}
declare module "@salesforce/schema/OpportunityChangeEvent.Campaign" {
  const Campaign:any;
  export default Campaign;
}
declare module "@salesforce/schema/OpportunityChangeEvent.CampaignId" {
  const CampaignId:any;
  export default CampaignId;
}
declare module "@salesforce/schema/OpportunityChangeEvent.HasOpportunityLineItem" {
  const HasOpportunityLineItem:boolean;
  export default HasOpportunityLineItem;
}
declare module "@salesforce/schema/OpportunityChangeEvent.Pricebook2" {
  const Pricebook2:any;
  export default Pricebook2;
}
declare module "@salesforce/schema/OpportunityChangeEvent.Pricebook2Id" {
  const Pricebook2Id:any;
  export default Pricebook2Id;
}
declare module "@salesforce/schema/OpportunityChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/OpportunityChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/OpportunityChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/OpportunityChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/OpportunityChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/OpportunityChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/OpportunityChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/OpportunityChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/OpportunityChangeEvent.Contact" {
  const Contact:any;
  export default Contact;
}
declare module "@salesforce/schema/OpportunityChangeEvent.ContactId" {
  const ContactId:any;
  export default ContactId;
}
declare module "@salesforce/schema/OpportunityChangeEvent.Current_Installment_Number__c" {
  const Current_Installment_Number__c:number;
  export default Current_Installment_Number__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.Household__c" {
  const Household__c:any;
  export default Household__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.IUA_Installment__c" {
  const IUA_Installment__c:number;
  export default IUA_Installment__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.Installment_Total__c" {
  const Installment_Total__c:number;
  export default Installment_Total__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.Number_of_Installments__c" {
  const Number_of_Installments__c:number;
  export default Number_of_Installments__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.Recurring_Donation__c" {
  const Recurring_Donation__c:any;
  export default Recurring_Donation__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.UCF_Communal_Installment__c" {
  const UCF_Communal_Installment__c:number;
  export default UCF_Communal_Installment__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.UCF_Education_Installment__c" {
  const UCF_Education_Installment__c:number;
  export default UCF_Education_Installment__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.Welfare_Installment__c" {
  const Welfare_Installment__c:number;
  export default Welfare_Installment__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.Campaign_Year__c" {
  const Campaign_Year__c:string;
  export default Campaign_Year__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.Campaign__c" {
  const Campaign__c:string;
  export default Campaign__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.Cancellation_Reason__c" {
  const Cancellation_Reason__c:string;
  export default Cancellation_Reason__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.Communal_Reference_Number__c" {
  const Communal_Reference_Number__c:string;
  export default Communal_Reference_Number__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.Method_of_Payment__c" {
  const Method_of_Payment__c:string;
  export default Method_of_Payment__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.Old_Pledge_Reference_Number__c" {
  const Old_Pledge_Reference_Number__c:string;
  export default Old_Pledge_Reference_Number__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.Pledge_Reference_Auto_Number__c" {
  const Pledge_Reference_Auto_Number__c:string;
  export default Pledge_Reference_Auto_Number__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.Pledge_Number__c" {
  const Pledge_Number__c:string;
  export default Pledge_Number__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.Receipt_Date__c" {
  const Receipt_Date__c:any;
  export default Receipt_Date__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.IUA__c" {
  const IUA__c:number;
  export default IUA__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.UCFC__c" {
  const UCFC__c:number;
  export default UCFC__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.UCFE__c" {
  const UCFE__c:number;
  export default UCFE__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.Welfare__c" {
  const Welfare__c:number;
  export default Welfare__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.TodaysDate__c" {
  const TodaysDate__c:any;
  export default TodaysDate__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.TotalAmount__c" {
  const TotalAmount__c:number;
  export default TotalAmount__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.Trans_Key__c" {
  const Trans_Key__c:string;
  export default Trans_Key__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.Trans_Type__c" {
  const Trans_Type__c:string;
  export default Trans_Type__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.CnP_PaaS__C_P_Recurring_Reference_Number__c" {
  const CnP_PaaS__C_P_Recurring_Reference_Number__c:string;
  export default CnP_PaaS__C_P_Recurring_Reference_Number__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.CnP_PaaS__C_P_Recurring__c" {
  const CnP_PaaS__C_P_Recurring__c:any;
  export default CnP_PaaS__C_P_Recurring__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.CnP_PaaS__Class__c" {
  const CnP_PaaS__Class__c:any;
  export default CnP_PaaS__Class__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.CnP_PaaS__CnP_Fundraiser_Contact__c" {
  const CnP_PaaS__CnP_Fundraiser_Contact__c:any;
  export default CnP_PaaS__CnP_Fundraiser_Contact__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.CnP_PaaS__CnP_Opportunity_Type__c" {
  const CnP_PaaS__CnP_Opportunity_Type__c:string;
  export default CnP_PaaS__CnP_Opportunity_Type__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.CnP_PaaS__CnP_OrderNumber__c" {
  const CnP_PaaS__CnP_OrderNumber__c:any;
  export default CnP_PaaS__CnP_OrderNumber__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.CnP_PaaS__CnP_Payment_Type__c" {
  const CnP_PaaS__CnP_Payment_Type__c:string;
  export default CnP_PaaS__CnP_Payment_Type__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.CnP_PaaS__Contact__c" {
  const CnP_PaaS__Contact__c:any;
  export default CnP_PaaS__Contact__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.CnP_PaaS__Discount__c" {
  const CnP_PaaS__Discount__c:number;
  export default CnP_PaaS__Discount__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.CnP_PaaS__Invoice__c" {
  const CnP_PaaS__Invoice__c:any;
  export default CnP_PaaS__Invoice__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.Campaign_Info__c" {
  const Campaign_Info__c:string;
  export default Campaign_Info__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.CnP_PaaS__Ledger__c" {
  const CnP_PaaS__Ledger__c:any;
  export default CnP_PaaS__Ledger__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.CnP_PaaS__SKU__c" {
  const CnP_PaaS__SKU__c:string;
  export default CnP_PaaS__SKU__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.CnP_PaaS__Sub_Class__c" {
  const CnP_PaaS__Sub_Class__c:any;
  export default CnP_PaaS__Sub_Class__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.CnP_PaaS__Tax__c" {
  const CnP_PaaS__Tax__c:number;
  export default CnP_PaaS__Tax__c;
}
declare module "@salesforce/schema/OpportunityChangeEvent.CnP_PaaS__Tax_deductible__c" {
  const CnP_PaaS__Tax_deductible__c:number;
  export default CnP_PaaS__Tax_deductible__c;
}
