declare module "@salesforce/schema/CnP_PaaS__Report_Fields__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Report_Fields__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__Report_Fields__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__Report_Fields__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__Report_Fields__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Report_Fields__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Report_Fields__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Report_Fields__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Report_Fields__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Report_Fields__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Report_Fields__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Report_Fields__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__Report_Fields__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/CnP_PaaS__Report_Fields__c.CnP_PaaS__Condition__c" {
  const CnP_PaaS__Condition__c:string;
  export default CnP_PaaS__Condition__c;
}
declare module "@salesforce/schema/CnP_PaaS__Report_Fields__c.CnP_PaaS__Page_Name__c" {
  const CnP_PaaS__Page_Name__c:string;
  export default CnP_PaaS__Page_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__Report_Fields__c.CnP_PaaS__fieldtype__c" {
  const CnP_PaaS__fieldtype__c:string;
  export default CnP_PaaS__fieldtype__c;
}
declare module "@salesforce/schema/CnP_PaaS__Report_Fields__c.CnP_PaaS__reportid__r" {
  const CnP_PaaS__reportid__r:any;
  export default CnP_PaaS__reportid__r;
}
declare module "@salesforce/schema/CnP_PaaS__Report_Fields__c.CnP_PaaS__reportid__c" {
  const CnP_PaaS__reportid__c:any;
  export default CnP_PaaS__reportid__c;
}
declare module "@salesforce/schema/CnP_PaaS__Report_Fields__c.CnP_PaaS__value__c" {
  const CnP_PaaS__value__c:string;
  export default CnP_PaaS__value__c;
}
