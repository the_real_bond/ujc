declare module "@salesforce/schema/CampaignMember.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CampaignMember.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CampaignMember.Campaign" {
  const Campaign:any;
  export default Campaign;
}
declare module "@salesforce/schema/CampaignMember.CampaignId" {
  const CampaignId:any;
  export default CampaignId;
}
declare module "@salesforce/schema/CampaignMember.Lead" {
  const Lead:any;
  export default Lead;
}
declare module "@salesforce/schema/CampaignMember.LeadId" {
  const LeadId:any;
  export default LeadId;
}
declare module "@salesforce/schema/CampaignMember.Contact" {
  const Contact:any;
  export default Contact;
}
declare module "@salesforce/schema/CampaignMember.ContactId" {
  const ContactId:any;
  export default ContactId;
}
declare module "@salesforce/schema/CampaignMember.Status" {
  const Status:string;
  export default Status;
}
declare module "@salesforce/schema/CampaignMember.HasResponded" {
  const HasResponded:boolean;
  export default HasResponded;
}
declare module "@salesforce/schema/CampaignMember.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CampaignMember.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CampaignMember.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CampaignMember.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CampaignMember.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CampaignMember.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CampaignMember.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CampaignMember.FirstRespondedDate" {
  const FirstRespondedDate:any;
  export default FirstRespondedDate;
}
declare module "@salesforce/schema/CampaignMember.Salutation" {
  const Salutation:string;
  export default Salutation;
}
declare module "@salesforce/schema/CampaignMember.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CampaignMember.FirstName" {
  const FirstName:string;
  export default FirstName;
}
declare module "@salesforce/schema/CampaignMember.LastName" {
  const LastName:string;
  export default LastName;
}
declare module "@salesforce/schema/CampaignMember.Title" {
  const Title:string;
  export default Title;
}
declare module "@salesforce/schema/CampaignMember.Street" {
  const Street:string;
  export default Street;
}
declare module "@salesforce/schema/CampaignMember.City" {
  const City:string;
  export default City;
}
declare module "@salesforce/schema/CampaignMember.State" {
  const State:string;
  export default State;
}
declare module "@salesforce/schema/CampaignMember.PostalCode" {
  const PostalCode:string;
  export default PostalCode;
}
declare module "@salesforce/schema/CampaignMember.Country" {
  const Country:string;
  export default Country;
}
declare module "@salesforce/schema/CampaignMember.Email" {
  const Email:string;
  export default Email;
}
declare module "@salesforce/schema/CampaignMember.Phone" {
  const Phone:string;
  export default Phone;
}
declare module "@salesforce/schema/CampaignMember.Fax" {
  const Fax:string;
  export default Fax;
}
declare module "@salesforce/schema/CampaignMember.MobilePhone" {
  const MobilePhone:string;
  export default MobilePhone;
}
declare module "@salesforce/schema/CampaignMember.Description" {
  const Description:string;
  export default Description;
}
declare module "@salesforce/schema/CampaignMember.DoNotCall" {
  const DoNotCall:boolean;
  export default DoNotCall;
}
declare module "@salesforce/schema/CampaignMember.HasOptedOutOfEmail" {
  const HasOptedOutOfEmail:boolean;
  export default HasOptedOutOfEmail;
}
declare module "@salesforce/schema/CampaignMember.HasOptedOutOfFax" {
  const HasOptedOutOfFax:boolean;
  export default HasOptedOutOfFax;
}
declare module "@salesforce/schema/CampaignMember.LeadSource" {
  const LeadSource:string;
  export default LeadSource;
}
declare module "@salesforce/schema/CampaignMember.CompanyOrAccount" {
  const CompanyOrAccount:string;
  export default CompanyOrAccount;
}
declare module "@salesforce/schema/CampaignMember.Type" {
  const Type:string;
  export default Type;
}
declare module "@salesforce/schema/CampaignMember.LeadOrContact" {
  const LeadOrContact:any;
  export default LeadOrContact;
}
declare module "@salesforce/schema/CampaignMember.LeadOrContactId" {
  const LeadOrContactId:any;
  export default LeadOrContactId;
}
declare module "@salesforce/schema/CampaignMember.LeadOrContactOwner" {
  const LeadOrContactOwner:any;
  export default LeadOrContactOwner;
}
declare module "@salesforce/schema/CampaignMember.LeadOrContactOwnerId" {
  const LeadOrContactOwnerId:any;
  export default LeadOrContactOwnerId;
}
declare module "@salesforce/schema/CampaignMember.Payment_Status__c" {
  const Payment_Status__c:string;
  export default Payment_Status__c;
}
declare module "@salesforce/schema/CampaignMember.Name_Corporate_Booking__c" {
  const Name_Corporate_Booking__c:string;
  export default Name_Corporate_Booking__c;
}
declare module "@salesforce/schema/CampaignMember.Booked_with__r" {
  const Booked_with__r:any;
  export default Booked_with__r;
}
declare module "@salesforce/schema/CampaignMember.Booked_with__c" {
  const Booked_with__c:any;
  export default Booked_with__c;
}
declare module "@salesforce/schema/CampaignMember.Quantity__c" {
  const Quantity__c:number;
  export default Quantity__c;
}
declare module "@salesforce/schema/CampaignMember.Timeslot__c" {
  const Timeslot__c:string;
  export default Timeslot__c;
}
declare module "@salesforce/schema/CampaignMember.CampaignMTool__Address_Of_Member__c" {
  const CampaignMTool__Address_Of_Member__c:string;
  export default CampaignMTool__Address_Of_Member__c;
}
declare module "@salesforce/schema/CampaignMember.CampaignMTool__City_Of_Member__c" {
  const CampaignMTool__City_Of_Member__c:string;
  export default CampaignMTool__City_Of_Member__c;
}
declare module "@salesforce/schema/CampaignMember.CampaignMTool__Company_Of_Member__c" {
  const CampaignMTool__Company_Of_Member__c:string;
  export default CampaignMTool__Company_Of_Member__c;
}
declare module "@salesforce/schema/CampaignMember.CampaignMTool__Country_Of_Member__c" {
  const CampaignMTool__Country_Of_Member__c:string;
  export default CampaignMTool__Country_Of_Member__c;
}
declare module "@salesforce/schema/CampaignMember.CampaignMTool__Do_Not_Call_Member__c" {
  const CampaignMTool__Do_Not_Call_Member__c:string;
  export default CampaignMTool__Do_Not_Call_Member__c;
}
declare module "@salesforce/schema/CampaignMember.CampaignMTool__Email_Of_Member__c" {
  const CampaignMTool__Email_Of_Member__c:string;
  export default CampaignMTool__Email_Of_Member__c;
}
declare module "@salesforce/schema/CampaignMember.CampaignMTool__Fax_Of_Member__c" {
  const CampaignMTool__Fax_Of_Member__c:string;
  export default CampaignMTool__Fax_Of_Member__c;
}
declare module "@salesforce/schema/CampaignMember.CampaignMTool__MobilePhone_Of_Member__c" {
  const CampaignMTool__MobilePhone_Of_Member__c:string;
  export default CampaignMTool__MobilePhone_Of_Member__c;
}
declare module "@salesforce/schema/CampaignMember.CampaignMTool__Name_Of_Member__c" {
  const CampaignMTool__Name_Of_Member__c:string;
  export default CampaignMTool__Name_Of_Member__c;
}
declare module "@salesforce/schema/CampaignMember.CampaignMTool__Phone_Of_Member__c" {
  const CampaignMTool__Phone_Of_Member__c:string;
  export default CampaignMTool__Phone_Of_Member__c;
}
declare module "@salesforce/schema/CampaignMember.CampaignMTool__PostalCode_Of_Member__c" {
  const CampaignMTool__PostalCode_Of_Member__c:string;
  export default CampaignMTool__PostalCode_Of_Member__c;
}
declare module "@salesforce/schema/CampaignMember.CampaignMTool__Related_To__r" {
  const CampaignMTool__Related_To__r:any;
  export default CampaignMTool__Related_To__r;
}
declare module "@salesforce/schema/CampaignMember.CampaignMTool__Related_To__c" {
  const CampaignMTool__Related_To__c:any;
  export default CampaignMTool__Related_To__c;
}
declare module "@salesforce/schema/CampaignMember.CampaignMTool__Salutation_Of_Member__c" {
  const CampaignMTool__Salutation_Of_Member__c:string;
  export default CampaignMTool__Salutation_Of_Member__c;
}
declare module "@salesforce/schema/CampaignMember.CampaignMTool__Title_Of_Member__c" {
  const CampaignMTool__Title_Of_Member__c:string;
  export default CampaignMTool__Title_Of_Member__c;
}
declare module "@salesforce/schema/CampaignMember.Status_Reason__c" {
  const Status_Reason__c:string;
  export default Status_Reason__c;
}
declare module "@salesforce/schema/CampaignMember.Database_Status__c" {
  const Database_Status__c:string;
  export default Database_Status__c;
}
declare module "@salesforce/schema/CampaignMember.Pledge_Card_Created__c" {
  const Pledge_Card_Created__c:boolean;
  export default Pledge_Card_Created__c;
}
declare module "@salesforce/schema/CampaignMember.Hometown__c" {
  const Hometown__c:string;
  export default Hometown__c;
}
declare module "@salesforce/schema/CampaignMember.Location__c" {
  const Location__c:string;
  export default Location__c;
}
