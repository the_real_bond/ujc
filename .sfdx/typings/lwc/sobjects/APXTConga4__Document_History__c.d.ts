declare module "@salesforce/schema/APXTConga4__Document_History__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/APXTConga4__Document_History__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/APXTConga4__Document_History__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/APXTConga4__Document_History__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/APXTConga4__Document_History__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/APXTConga4__Document_History__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/APXTConga4__Document_History__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/APXTConga4__Document_History__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/APXTConga4__Document_History__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/APXTConga4__Document_History__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/APXTConga4__Document_History__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/APXTConga4__Document_History__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/APXTConga4__Document_History__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/APXTConga4__Document_History__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/APXTConga4__Document_History__c.APXTConga4__Conga_Solution__r" {
  const APXTConga4__Conga_Solution__r:any;
  export default APXTConga4__Conga_Solution__r;
}
declare module "@salesforce/schema/APXTConga4__Document_History__c.APXTConga4__Conga_Solution__c" {
  const APXTConga4__Conga_Solution__c:any;
  export default APXTConga4__Conga_Solution__c;
}
declare module "@salesforce/schema/APXTConga4__Document_History__c.APXTConga4__Link_Text__c" {
  const APXTConga4__Link_Text__c:string;
  export default APXTConga4__Link_Text__c;
}
declare module "@salesforce/schema/APXTConga4__Document_History__c.APXTConga4__Master_Object_ID__c" {
  const APXTConga4__Master_Object_ID__c:string;
  export default APXTConga4__Master_Object_ID__c;
}
declare module "@salesforce/schema/APXTConga4__Document_History__c.APXTConga4__URL__c" {
  const APXTConga4__URL__c:string;
  export default APXTConga4__URL__c;
}
declare module "@salesforce/schema/APXTConga4__Document_History__c.APXTConga4__Last_Viewed__c" {
  const APXTConga4__Last_Viewed__c:any;
  export default APXTConga4__Last_Viewed__c;
}
declare module "@salesforce/schema/APXTConga4__Document_History__c.APXTConga4__Number_of_Views__c" {
  const APXTConga4__Number_of_Views__c:number;
  export default APXTConga4__Number_of_Views__c;
}
