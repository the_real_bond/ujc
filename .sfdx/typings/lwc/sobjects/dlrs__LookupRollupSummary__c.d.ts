declare module "@salesforce/schema/dlrs__LookupRollupSummary__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__c.dlrs__Active__c" {
  const dlrs__Active__c:boolean;
  export default dlrs__Active__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__c.dlrs__AggregateOperation__c" {
  const dlrs__AggregateOperation__c:string;
  export default dlrs__AggregateOperation__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__c.dlrs__AggregateResultField__c" {
  const dlrs__AggregateResultField__c:string;
  export default dlrs__AggregateResultField__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__c.dlrs__CalculateJobId__c" {
  const dlrs__CalculateJobId__c:string;
  export default dlrs__CalculateJobId__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__c.dlrs__CalculationMode__c" {
  const dlrs__CalculationMode__c:string;
  export default dlrs__CalculationMode__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__c.dlrs__ChildObject__c" {
  const dlrs__ChildObject__c:string;
  export default dlrs__ChildObject__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__c.dlrs__FieldToAggregate__c" {
  const dlrs__FieldToAggregate__c:string;
  export default dlrs__FieldToAggregate__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__c.dlrs__ParentObject__c" {
  const dlrs__ParentObject__c:string;
  export default dlrs__ParentObject__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__c.dlrs__RelationshipCriteriaFields__c" {
  const dlrs__RelationshipCriteriaFields__c:string;
  export default dlrs__RelationshipCriteriaFields__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__c.dlrs__RelationshipCriteria__c" {
  const dlrs__RelationshipCriteria__c:string;
  export default dlrs__RelationshipCriteria__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__c.dlrs__RelationshipField__c" {
  const dlrs__RelationshipField__c:string;
  export default dlrs__RelationshipField__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__c.dlrs__AggregateAllRows__c" {
  const dlrs__AggregateAllRows__c:boolean;
  export default dlrs__AggregateAllRows__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__c.dlrs__CalculationSharingMode__c" {
  const dlrs__CalculationSharingMode__c:string;
  export default dlrs__CalculationSharingMode__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__c.dlrs__ConcatenateDelimiter__c" {
  const dlrs__ConcatenateDelimiter__c:string;
  export default dlrs__ConcatenateDelimiter__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__c.dlrs__Description__c" {
  const dlrs__Description__c:string;
  export default dlrs__Description__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__c.dlrs__FieldToOrderBy__c" {
  const dlrs__FieldToOrderBy__c:string;
  export default dlrs__FieldToOrderBy__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__c.dlrs__RowLimit__c" {
  const dlrs__RowLimit__c:number;
  export default dlrs__RowLimit__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__c.dlrs__TestCodeSeeAllData__c" {
  const dlrs__TestCodeSeeAllData__c:boolean;
  export default dlrs__TestCodeSeeAllData__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__c.dlrs__TestCode__c" {
  const dlrs__TestCode__c:string;
  export default dlrs__TestCode__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummary__c.dlrs__UniqueName__c" {
  const dlrs__UniqueName__c:string;
  export default dlrs__UniqueName__c;
}
