declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__c.CnP_PaaS__Amount_Value__c" {
  const CnP_PaaS__Amount_Value__c:string;
  export default CnP_PaaS__Amount_Value__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__c.CnP_PaaS__Answer__c" {
  const CnP_PaaS__Answer__c:string;
  export default CnP_PaaS__Answer__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__c.CnP_PaaS__Campaign_Value__c" {
  const CnP_PaaS__Campaign_Value__c:string;
  export default CnP_PaaS__Campaign_Value__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__c.CnP_PaaS__Campaign__c" {
  const CnP_PaaS__Campaign__c:string;
  export default CnP_PaaS__Campaign__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__c.CnP_PaaS__Checkout_page_WID__c" {
  const CnP_PaaS__Checkout_page_WID__c:string;
  export default CnP_PaaS__Checkout_page_WID__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__c.CnP_PaaS__CnP_Auto_Responder_Settings__r" {
  const CnP_PaaS__CnP_Auto_Responder_Settings__r:any;
  export default CnP_PaaS__CnP_Auto_Responder_Settings__r;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__c.CnP_PaaS__CnP_Auto_Responder_Settings__c" {
  const CnP_PaaS__CnP_Auto_Responder_Settings__c:any;
  export default CnP_PaaS__CnP_Auto_Responder_Settings__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__c.CnP_PaaS__Option_Type__c" {
  const CnP_PaaS__Option_Type__c:string;
  export default CnP_PaaS__Option_Type__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__c.CnP_PaaS__Payment_Amount__c" {
  const CnP_PaaS__Payment_Amount__c:string;
  export default CnP_PaaS__Payment_Amount__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__c.CnP_PaaS__Question_Options__c" {
  const CnP_PaaS__Question_Options__c:string;
  export default CnP_PaaS__Question_Options__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__c.CnP_PaaS__Question__c" {
  const CnP_PaaS__Question__c:string;
  export default CnP_PaaS__Question__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__c.CnP_PaaS__SKU_Value__c" {
  const CnP_PaaS__SKU_Value__c:string;
  export default CnP_PaaS__SKU_Value__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__c.CnP_PaaS__SKU__c" {
  const CnP_PaaS__SKU__c:string;
  export default CnP_PaaS__SKU__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__c.CnP_PaaS__WID_value__c" {
  const CnP_PaaS__WID_value__c:string;
  export default CnP_PaaS__WID_value__c;
}
