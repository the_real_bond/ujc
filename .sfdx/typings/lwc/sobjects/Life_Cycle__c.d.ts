declare module "@salesforce/schema/Life_Cycle__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Life_Cycle__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Life_Cycle__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Life_Cycle__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Life_Cycle__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Life_Cycle__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Life_Cycle__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Life_Cycle__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Life_Cycle__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Life_Cycle__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Life_Cycle__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/Life_Cycle__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Life_Cycle__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Life_Cycle__c.Event_Type__c" {
  const Event_Type__c:string;
  export default Event_Type__c;
}
declare module "@salesforce/schema/Life_Cycle__c.Location__c" {
  const Location__c:string;
  export default Location__c;
}
declare module "@salesforce/schema/Life_Cycle__c.Relationship__c" {
  const Relationship__c:string;
  export default Relationship__c;
}
declare module "@salesforce/schema/Life_Cycle__c.Event_Date__c" {
  const Event_Date__c:any;
  export default Event_Date__c;
}
declare module "@salesforce/schema/Life_Cycle__c.Source__c" {
  const Source__c:string;
  export default Source__c;
}
declare module "@salesforce/schema/Life_Cycle__c.Person__r" {
  const Person__r:any;
  export default Person__r;
}
declare module "@salesforce/schema/Life_Cycle__c.Person__c" {
  const Person__c:any;
  export default Person__c;
}
declare module "@salesforce/schema/Life_Cycle__c.Relation__c" {
  const Relation__c:string;
  export default Relation__c;
}
declare module "@salesforce/schema/Life_Cycle__c.Event_Details__c" {
  const Event_Details__c:string;
  export default Event_Details__c;
}
declare module "@salesforce/schema/Life_Cycle__c.Life_Cycle_Year__c" {
  const Life_Cycle_Year__c:string;
  export default Life_Cycle_Year__c;
}
declare module "@salesforce/schema/Life_Cycle__c.Life_Cycle_Week__c" {
  const Life_Cycle_Week__c:string;
  export default Life_Cycle_Week__c;
}
declare module "@salesforce/schema/Life_Cycle__c.Event__c" {
  const Event__c:string;
  export default Event__c;
}
declare module "@salesforce/schema/Life_Cycle__c.Preferred_Name__c" {
  const Preferred_Name__c:string;
  export default Preferred_Name__c;
}
declare module "@salesforce/schema/Life_Cycle__c.Mail_Merge_Email__c" {
  const Mail_Merge_Email__c:string;
  export default Mail_Merge_Email__c;
}
declare module "@salesforce/schema/Life_Cycle__c.Formal_Salutation__c" {
  const Formal_Salutation__c:string;
  export default Formal_Salutation__c;
}
declare module "@salesforce/schema/Life_Cycle__c.Hebrew_Date__c" {
  const Hebrew_Date__c:string;
  export default Hebrew_Date__c;
}
declare module "@salesforce/schema/Life_Cycle__c.Today_s_Date__c" {
  const Today_s_Date__c:string;
  export default Today_s_Date__c;
}
declare module "@salesforce/schema/Life_Cycle__c.Relation_Last__c" {
  const Relation_Last__c:string;
  export default Relation_Last__c;
}
declare module "@salesforce/schema/Life_Cycle__c.Letter_Sent__c" {
  const Letter_Sent__c:boolean;
  export default Letter_Sent__c;
}
declare module "@salesforce/schema/Life_Cycle__c.Event_Category__c" {
  const Event_Category__c:string;
  export default Event_Category__c;
}
declare module "@salesforce/schema/Life_Cycle__c.Conga_Template_Id__c" {
  const Conga_Template_Id__c:string;
  export default Conga_Template_Id__c;
}
declare module "@salesforce/schema/Life_Cycle__c.Gender_Pronoun__c" {
  const Gender_Pronoun__c:string;
  export default Gender_Pronoun__c;
}
declare module "@salesforce/schema/Life_Cycle__c.Not_Sent_Because__c" {
  const Not_Sent_Because__c:string;
  export default Not_Sent_Because__c;
}
declare module "@salesforce/schema/Life_Cycle__c.Salutation_First_Names__c" {
  const Salutation_First_Names__c:string;
  export default Salutation_First_Names__c;
}
