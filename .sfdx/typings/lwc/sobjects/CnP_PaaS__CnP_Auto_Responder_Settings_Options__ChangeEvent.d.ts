declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__ChangeEvent.CnP_PaaS__Amount_Value__c" {
  const CnP_PaaS__Amount_Value__c:string;
  export default CnP_PaaS__Amount_Value__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__ChangeEvent.CnP_PaaS__Answer__c" {
  const CnP_PaaS__Answer__c:string;
  export default CnP_PaaS__Answer__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__ChangeEvent.CnP_PaaS__Campaign_Value__c" {
  const CnP_PaaS__Campaign_Value__c:string;
  export default CnP_PaaS__Campaign_Value__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__ChangeEvent.CnP_PaaS__Campaign__c" {
  const CnP_PaaS__Campaign__c:string;
  export default CnP_PaaS__Campaign__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__ChangeEvent.CnP_PaaS__Checkout_page_WID__c" {
  const CnP_PaaS__Checkout_page_WID__c:string;
  export default CnP_PaaS__Checkout_page_WID__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__ChangeEvent.CnP_PaaS__CnP_Auto_Responder_Settings__c" {
  const CnP_PaaS__CnP_Auto_Responder_Settings__c:any;
  export default CnP_PaaS__CnP_Auto_Responder_Settings__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__ChangeEvent.CnP_PaaS__Option_Type__c" {
  const CnP_PaaS__Option_Type__c:string;
  export default CnP_PaaS__Option_Type__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__ChangeEvent.CnP_PaaS__Payment_Amount__c" {
  const CnP_PaaS__Payment_Amount__c:string;
  export default CnP_PaaS__Payment_Amount__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__ChangeEvent.CnP_PaaS__Question_Options__c" {
  const CnP_PaaS__Question_Options__c:string;
  export default CnP_PaaS__Question_Options__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__ChangeEvent.CnP_PaaS__Question__c" {
  const CnP_PaaS__Question__c:string;
  export default CnP_PaaS__Question__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__ChangeEvent.CnP_PaaS__SKU_Value__c" {
  const CnP_PaaS__SKU_Value__c:string;
  export default CnP_PaaS__SKU_Value__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__ChangeEvent.CnP_PaaS__SKU__c" {
  const CnP_PaaS__SKU__c:string;
  export default CnP_PaaS__SKU__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Settings_Options__ChangeEvent.CnP_PaaS__WID_value__c" {
  const CnP_PaaS__WID_value__c:string;
  export default CnP_PaaS__WID_value__c;
}
