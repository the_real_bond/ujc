declare module "@salesforce/schema/PowerLoader__QueryPanelTemplate__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplate__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplate__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplate__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplate__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplate__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplate__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplate__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplate__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplate__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplate__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplate__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplate__c.PowerLoader__Private__c" {
  const PowerLoader__Private__c:boolean;
  export default PowerLoader__Private__c;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplate__c.PowerLoader__Query_Panel_Name__c" {
  const PowerLoader__Query_Panel_Name__c:string;
  export default PowerLoader__Query_Panel_Name__c;
}
