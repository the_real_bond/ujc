declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Recurring_Schedule__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Recurring_Schedule__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Recurring_Schedule__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Recurring_Schedule__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Recurring_Schedule__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Recurring_Schedule__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Recurring_Schedule__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Recurring_Schedule__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Recurring_Schedule__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Recurring_Schedule__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Recurring_Schedule__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Recurring_Schedule__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Recurring_Schedule__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Recurring_Schedule__c.CnP_PaaS__Broadcaster_Schedule_Process__r" {
  const CnP_PaaS__Broadcaster_Schedule_Process__r:any;
  export default CnP_PaaS__Broadcaster_Schedule_Process__r;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Recurring_Schedule__c.CnP_PaaS__Broadcaster_Schedule_Process__c" {
  const CnP_PaaS__Broadcaster_Schedule_Process__c:any;
  export default CnP_PaaS__Broadcaster_Schedule_Process__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Recurring_Schedule__c.CnP_PaaS__End_on__c" {
  const CnP_PaaS__End_on__c:string;
  export default CnP_PaaS__End_on__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Recurring_Schedule__c.CnP_PaaS__Repeats_Every__c" {
  const CnP_PaaS__Repeats_Every__c:string;
  export default CnP_PaaS__Repeats_Every__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Recurring_Schedule__c.CnP_PaaS__Scheduledate__c" {
  const CnP_PaaS__Scheduledate__c:any;
  export default CnP_PaaS__Scheduledate__c;
}
