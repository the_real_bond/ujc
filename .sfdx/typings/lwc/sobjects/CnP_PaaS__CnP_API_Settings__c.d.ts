declare module "@salesforce/schema/CnP_PaaS__CnP_API_Settings__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_API_Settings__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_API_Settings__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_API_Settings__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_API_Settings__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_API_Settings__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_API_Settings__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_API_Settings__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_API_Settings__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_API_Settings__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_API_Settings__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_API_Settings__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_API_Settings__c.CnP_PaaS__CnP_Account_GUID__c" {
  const CnP_PaaS__CnP_Account_GUID__c:string;
  export default CnP_PaaS__CnP_Account_GUID__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_API_Settings__c.CnP_PaaS__CnP_Account_Number__c" {
  const CnP_PaaS__CnP_Account_Number__c:string;
  export default CnP_PaaS__CnP_Account_Number__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_API_Settings__c.CnP_PaaS__CnP_Account_Status__c" {
  const CnP_PaaS__CnP_Account_Status__c:string;
  export default CnP_PaaS__CnP_Account_Status__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_API_Settings__c.CnP_PaaS__Currency_Code__c" {
  const CnP_PaaS__Currency_Code__c:string;
  export default CnP_PaaS__Currency_Code__c;
}
