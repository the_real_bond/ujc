declare module "@salesforce/schema/dlrs__LookupRollupSummaryScheduleItems__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryScheduleItems__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryScheduleItems__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryScheduleItems__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryScheduleItems__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryScheduleItems__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryScheduleItems__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryScheduleItems__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryScheduleItems__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryScheduleItems__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryScheduleItems__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryScheduleItems__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryScheduleItems__c.dlrs__LookupRollupSummary__r" {
  const dlrs__LookupRollupSummary__r:any;
  export default dlrs__LookupRollupSummary__r;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryScheduleItems__c.dlrs__LookupRollupSummary__c" {
  const dlrs__LookupRollupSummary__c:any;
  export default dlrs__LookupRollupSummary__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryScheduleItems__c.dlrs__ParentId__c" {
  const dlrs__ParentId__c:string;
  export default dlrs__ParentId__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryScheduleItems__c.dlrs__ParentRecord__c" {
  const dlrs__ParentRecord__c:string;
  export default dlrs__ParentRecord__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryScheduleItems__c.dlrs__QualifiedParentID__c" {
  const dlrs__QualifiedParentID__c:string;
  export default dlrs__QualifiedParentID__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryScheduleItems__c.dlrs__LookupRollupSummary2__c" {
  const dlrs__LookupRollupSummary2__c:string;
  export default dlrs__LookupRollupSummary2__c;
}
