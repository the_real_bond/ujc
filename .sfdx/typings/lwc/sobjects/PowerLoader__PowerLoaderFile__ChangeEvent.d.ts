declare module "@salesforce/schema/PowerLoader__PowerLoaderFile__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderFile__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderFile__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderFile__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderFile__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderFile__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderFile__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderFile__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderFile__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderFile__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderFile__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderFile__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/PowerLoader__PowerLoaderFile__ChangeEvent.PowerLoader__Records__c" {
  const PowerLoader__Records__c:number;
  export default PowerLoader__Records__c;
}
