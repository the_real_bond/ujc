declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__ChangeEvent.CnP_PaaS_EVT__C_P_Event_Registration_Level__c" {
  const CnP_PaaS_EVT__C_P_Event_Registration_Level__c:any;
  export default CnP_PaaS_EVT__C_P_Event_Registration_Level__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__ChangeEvent.CnP_PaaS_EVT__C_P_Event__c" {
  const CnP_PaaS_EVT__C_P_Event__c:any;
  export default CnP_PaaS_EVT__C_P_Event__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__ChangeEvent.CnP_PaaS_EVT__Cookie_Gen_Num__c" {
  const CnP_PaaS_EVT__Cookie_Gen_Num__c:number;
  export default CnP_PaaS_EVT__Cookie_Gen_Num__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__ChangeEvent.CnP_PaaS_EVT__Quantity__c" {
  const CnP_PaaS_EVT__Quantity__c:number;
  export default CnP_PaaS_EVT__Quantity__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Inventory_Process__ChangeEvent.CnP_PaaS_EVT__UserTimeset__c" {
  const CnP_PaaS_EVT__UserTimeset__c:any;
  export default CnP_PaaS_EVT__UserTimeset__c;
}
