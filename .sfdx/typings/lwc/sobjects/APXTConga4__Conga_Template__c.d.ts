declare module "@salesforce/schema/APXTConga4__Conga_Template__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/APXTConga4__Conga_Template__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/APXTConga4__Conga_Template__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/APXTConga4__Conga_Template__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/APXTConga4__Conga_Template__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/APXTConga4__Conga_Template__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/APXTConga4__Conga_Template__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/APXTConga4__Conga_Template__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/APXTConga4__Conga_Template__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/APXTConga4__Conga_Template__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/APXTConga4__Conga_Template__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/APXTConga4__Conga_Template__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/APXTConga4__Conga_Template__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/APXTConga4__Conga_Template__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/APXTConga4__Conga_Template__c.APXTConga4__Description__c" {
  const APXTConga4__Description__c:string;
  export default APXTConga4__Description__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Template__c.APXTConga4__Label_Template_Use_Detail_Data__c" {
  const APXTConga4__Label_Template_Use_Detail_Data__c:boolean;
  export default APXTConga4__Label_Template_Use_Detail_Data__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Template__c.APXTConga4__Master_Field_to_Set_1__c" {
  const APXTConga4__Master_Field_to_Set_1__c:string;
  export default APXTConga4__Master_Field_to_Set_1__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Template__c.APXTConga4__Master_Field_to_Set_2__c" {
  const APXTConga4__Master_Field_to_Set_2__c:string;
  export default APXTConga4__Master_Field_to_Set_2__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Template__c.APXTConga4__Master_Field_to_Set_3__c" {
  const APXTConga4__Master_Field_to_Set_3__c:string;
  export default APXTConga4__Master_Field_to_Set_3__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Template__c.APXTConga4__Name__c" {
  const APXTConga4__Name__c:string;
  export default APXTConga4__Name__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Template__c.APXTConga4__Template_Group__c" {
  const APXTConga4__Template_Group__c:string;
  export default APXTConga4__Template_Group__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Template__c.APXTConga4__Template_Type__c" {
  const APXTConga4__Template_Type__c:string;
  export default APXTConga4__Template_Type__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Template__c.APXTConga4__Key__c" {
  const APXTConga4__Key__c:string;
  export default APXTConga4__Key__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Template__c.APXTConga4__Template_Extension__c" {
  const APXTConga4__Template_Extension__c:string;
  export default APXTConga4__Template_Extension__c;
}
