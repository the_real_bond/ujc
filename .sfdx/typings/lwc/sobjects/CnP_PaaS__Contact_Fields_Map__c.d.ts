declare module "@salesforce/schema/CnP_PaaS__Contact_Fields_Map__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Fields_Map__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Fields_Map__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Fields_Map__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Fields_Map__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Fields_Map__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Fields_Map__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Fields_Map__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Fields_Map__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Fields_Map__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Fields_Map__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Fields_Map__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Fields_Map__c.CnP_PaaS__Account_Field_Type__c" {
  const CnP_PaaS__Account_Field_Type__c:string;
  export default CnP_PaaS__Account_Field_Type__c;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Fields_Map__c.CnP_PaaS__Field_Map_Values__c" {
  const CnP_PaaS__Field_Map_Values__c:string;
  export default CnP_PaaS__Field_Map_Values__c;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Fields_Map__c.CnP_PaaS__Field_Type__c" {
  const CnP_PaaS__Field_Type__c:string;
  export default CnP_PaaS__Field_Type__c;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Fields_Map__c.CnP_PaaS__Process_Type__c" {
  const CnP_PaaS__Process_Type__c:string;
  export default CnP_PaaS__Process_Type__c;
}
