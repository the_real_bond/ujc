declare module "@salesforce/schema/CnP_PaaS__CnPData__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__CnPData__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__CnPData__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__CnPData__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__CnPData__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__CnPData__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__CnPData__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnPData__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnPData__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnPData__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnPData__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnPData__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnPData__ChangeEvent.CnP_PaaS__CnPDataID__c" {
  const CnP_PaaS__CnPDataID__c:string;
  export default CnP_PaaS__CnPDataID__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPData__ChangeEvent.CnP_PaaS__Contact__c" {
  const CnP_PaaS__Contact__c:any;
  export default CnP_PaaS__Contact__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPData__ChangeEvent.CnP_PaaS__DataXML__c" {
  const CnP_PaaS__DataXML__c:string;
  export default CnP_PaaS__DataXML__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPData__ChangeEvent.CnP_PaaS__Email__c" {
  const CnP_PaaS__Email__c:boolean;
  export default CnP_PaaS__Email__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPData__ChangeEvent.CnP_PaaS__Installed_Apps__c" {
  const CnP_PaaS__Installed_Apps__c:string;
  export default CnP_PaaS__Installed_Apps__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPData__ChangeEvent.CnP_PaaS__Message__c" {
  const CnP_PaaS__Message__c:string;
  export default CnP_PaaS__Message__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPData__ChangeEvent.CnP_PaaS__Name__c" {
  const CnP_PaaS__Name__c:string;
  export default CnP_PaaS__Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPData__ChangeEvent.CnP_PaaS__OrderNumber__c" {
  const CnP_PaaS__OrderNumber__c:string;
  export default CnP_PaaS__OrderNumber__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPData__ChangeEvent.CnP_PaaS__Rollup_Update__c" {
  const CnP_PaaS__Rollup_Update__c:string;
  export default CnP_PaaS__Rollup_Update__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPData__ChangeEvent.CnP_PaaS__StatusID__c" {
  const CnP_PaaS__StatusID__c:number;
  export default CnP_PaaS__StatusID__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPData__ChangeEvent.CnP_PaaS__Total_Charged__c" {
  const CnP_PaaS__Total_Charged__c:number;
  export default CnP_PaaS__Total_Charged__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPData__ChangeEvent.CnP_PaaS__Transaction_Result__c" {
  const CnP_PaaS__Transaction_Result__c:string;
  export default CnP_PaaS__Transaction_Result__c;
}
