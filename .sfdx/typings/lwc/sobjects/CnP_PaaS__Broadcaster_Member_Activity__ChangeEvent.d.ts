declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__ChangeEvent.CnP_PaaS__Broadcaster_Campaign__c" {
  const CnP_PaaS__Broadcaster_Campaign__c:any;
  export default CnP_PaaS__Broadcaster_Campaign__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__ChangeEvent.CnP_PaaS__Abuses__c" {
  const CnP_PaaS__Abuses__c:number;
  export default CnP_PaaS__Abuses__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__ChangeEvent.CnP_PaaS__Bounces__c" {
  const CnP_PaaS__Bounces__c:number;
  export default CnP_PaaS__Bounces__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__ChangeEvent.CnP_PaaS__Broadcaster_List__c" {
  const CnP_PaaS__Broadcaster_List__c:any;
  export default CnP_PaaS__Broadcaster_List__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__ChangeEvent.CnP_PaaS__Clicks__c" {
  const CnP_PaaS__Clicks__c:number;
  export default CnP_PaaS__Clicks__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__ChangeEvent.CnP_PaaS__Contact__c" {
  const CnP_PaaS__Contact__c:any;
  export default CnP_PaaS__Contact__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__ChangeEvent.CnP_PaaS__Opens__c" {
  const CnP_PaaS__Opens__c:number;
  export default CnP_PaaS__Opens__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__ChangeEvent.CnP_PaaS__Queued__c" {
  const CnP_PaaS__Queued__c:number;
  export default CnP_PaaS__Queued__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__ChangeEvent.CnP_PaaS__Sents__c" {
  const CnP_PaaS__Sents__c:number;
  export default CnP_PaaS__Sents__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__ChangeEvent.CnP_PaaS__Unsubscribes__c" {
  const CnP_PaaS__Unsubscribes__c:number;
  export default CnP_PaaS__Unsubscribes__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__ChangeEvent.CnP_PaaS__ecomm__c" {
  const CnP_PaaS__ecomm__c:number;
  export default CnP_PaaS__ecomm__c;
}
