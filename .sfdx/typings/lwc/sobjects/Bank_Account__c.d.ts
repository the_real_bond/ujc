declare module "@salesforce/schema/Bank_Account__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Bank_Account__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Bank_Account__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Bank_Account__c.RecordType" {
  const RecordType:any;
  export default RecordType;
}
declare module "@salesforce/schema/Bank_Account__c.RecordTypeId" {
  const RecordTypeId:any;
  export default RecordTypeId;
}
declare module "@salesforce/schema/Bank_Account__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Bank_Account__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Bank_Account__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Bank_Account__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Bank_Account__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Bank_Account__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Bank_Account__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Bank_Account__c.xxxBank_Name__c" {
  const xxxBank_Name__c:string;
  export default xxxBank_Name__c;
}
declare module "@salesforce/schema/Bank_Account__c.xxxBranch_Code__c" {
  const xxxBranch_Code__c:string;
  export default xxxBranch_Code__c;
}
declare module "@salesforce/schema/Bank_Account__c.Account_Type__c" {
  const Account_Type__c:string;
  export default Account_Type__c;
}
declare module "@salesforce/schema/Bank_Account__c.Account_Number__c" {
  const Account_Number__c:string;
  export default Account_Number__c;
}
declare module "@salesforce/schema/Bank_Account__c.Account_Holder__r" {
  const Account_Holder__r:any;
  export default Account_Holder__r;
}
declare module "@salesforce/schema/Bank_Account__c.Account_Holder__c" {
  const Account_Holder__c:any;
  export default Account_Holder__c;
}
declare module "@salesforce/schema/Bank_Account__c.Status__c" {
  const Status__c:string;
  export default Status__c;
}
declare module "@salesforce/schema/Bank_Account__c.Account_Type_Number__c" {
  const Account_Type_Number__c:string;
  export default Account_Type_Number__c;
}
declare module "@salesforce/schema/Bank_Account__c.Credit_Card_CVV__c" {
  const Credit_Card_CVV__c:string;
  export default Credit_Card_CVV__c;
}
declare module "@salesforce/schema/Bank_Account__c.Credit_Card_Expiry_Month__c" {
  const Credit_Card_Expiry_Month__c:string;
  export default Credit_Card_Expiry_Month__c;
}
declare module "@salesforce/schema/Bank_Account__c.Credit_Card_Expiry_Year__c" {
  const Credit_Card_Expiry_Year__c:string;
  export default Credit_Card_Expiry_Year__c;
}
declare module "@salesforce/schema/Bank_Account__c.Credit_Card_Type__c" {
  const Credit_Card_Type__c:string;
  export default Credit_Card_Type__c;
}
declare module "@salesforce/schema/Bank_Account__c.Credit_Card_Number__c" {
  const Credit_Card_Number__c:string;
  export default Credit_Card_Number__c;
}
declare module "@salesforce/schema/Bank_Account__c.Bank__r" {
  const Bank__r:any;
  export default Bank__r;
}
declare module "@salesforce/schema/Bank_Account__c.Bank__c" {
  const Bank__c:any;
  export default Bank__c;
}
declare module "@salesforce/schema/Bank_Account__c.Branch__r" {
  const Branch__r:any;
  export default Branch__r;
}
declare module "@salesforce/schema/Bank_Account__c.Branch__c" {
  const Branch__c:any;
  export default Branch__c;
}
declare module "@salesforce/schema/Bank_Account__c.Bank_Name_Display__c" {
  const Bank_Name_Display__c:string;
  export default Bank_Name_Display__c;
}
