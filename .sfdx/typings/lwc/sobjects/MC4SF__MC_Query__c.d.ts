declare module "@salesforce/schema/MC4SF__MC_Query__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/MC4SF__MC_Query__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/MC4SF__MC_Query__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/MC4SF__MC_Query__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/MC4SF__MC_Query__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/MC4SF__MC_Query__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/MC4SF__MC_Query__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/MC4SF__MC_Query__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/MC4SF__MC_Query__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/MC4SF__MC_Query__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/MC4SF__MC_Query__c.MC4SF__MC_List__r" {
  const MC4SF__MC_List__r:any;
  export default MC4SF__MC_List__r;
}
declare module "@salesforce/schema/MC4SF__MC_Query__c.MC4SF__MC_List__c" {
  const MC4SF__MC_List__c:any;
  export default MC4SF__MC_List__c;
}
declare module "@salesforce/schema/MC4SF__MC_Query__c.MC4SF__Campaign_Id__c" {
  const MC4SF__Campaign_Id__c:string;
  export default MC4SF__Campaign_Id__c;
}
declare module "@salesforce/schema/MC4SF__MC_Query__c.MC4SF__Campaign_Members_SOQL__c" {
  const MC4SF__Campaign_Members_SOQL__c:string;
  export default MC4SF__Campaign_Members_SOQL__c;
}
declare module "@salesforce/schema/MC4SF__MC_Query__c.MC4SF__Campaign_Members__c" {
  const MC4SF__Campaign_Members__c:boolean;
  export default MC4SF__Campaign_Members__c;
}
declare module "@salesforce/schema/MC4SF__MC_Query__c.MC4SF__Contacts_SOQL__c" {
  const MC4SF__Contacts_SOQL__c:string;
  export default MC4SF__Contacts_SOQL__c;
}
declare module "@salesforce/schema/MC4SF__MC_Query__c.MC4SF__Contacts__c" {
  const MC4SF__Contacts__c:boolean;
  export default MC4SF__Contacts__c;
}
declare module "@salesforce/schema/MC4SF__MC_Query__c.MC4SF__Error_Message__c" {
  const MC4SF__Error_Message__c:string;
  export default MC4SF__Error_Message__c;
}
declare module "@salesforce/schema/MC4SF__MC_Query__c.MC4SF__Interests__c" {
  const MC4SF__Interests__c:string;
  export default MC4SF__Interests__c;
}
declare module "@salesforce/schema/MC4SF__MC_Query__c.MC4SF__Last_Run_As__r" {
  const MC4SF__Last_Run_As__r:any;
  export default MC4SF__Last_Run_As__r;
}
declare module "@salesforce/schema/MC4SF__MC_Query__c.MC4SF__Last_Run_As__c" {
  const MC4SF__Last_Run_As__c:any;
  export default MC4SF__Last_Run_As__c;
}
declare module "@salesforce/schema/MC4SF__MC_Query__c.MC4SF__Last_Run__c" {
  const MC4SF__Last_Run__c:any;
  export default MC4SF__Last_Run__c;
}
declare module "@salesforce/schema/MC4SF__MC_Query__c.MC4SF__Leads_SOQL__c" {
  const MC4SF__Leads_SOQL__c:string;
  export default MC4SF__Leads_SOQL__c;
}
declare module "@salesforce/schema/MC4SF__MC_Query__c.MC4SF__Leads__c" {
  const MC4SF__Leads__c:boolean;
  export default MC4SF__Leads__c;
}
declare module "@salesforce/schema/MC4SF__MC_Query__c.MC4SF__Run_Daily_At__c" {
  const MC4SF__Run_Daily_At__c:string;
  export default MC4SF__Run_Daily_At__c;
}
declare module "@salesforce/schema/MC4SF__MC_Query__c.MC4SF__Static_Segments__c" {
  const MC4SF__Static_Segments__c:string;
  export default MC4SF__Static_Segments__c;
}
declare module "@salesforce/schema/MC4SF__MC_Query__c.MC4SF__Status__c" {
  const MC4SF__Status__c:string;
  export default MC4SF__Status__c;
}
declare module "@salesforce/schema/MC4SF__MC_Query__c.MC4SF__Subscribers_Added_last_run__c" {
  const MC4SF__Subscribers_Added_last_run__c:number;
  export default MC4SF__Subscribers_Added_last_run__c;
}
declare module "@salesforce/schema/MC4SF__MC_Query__c.MC4SF__User_Contacts_Only__c" {
  const MC4SF__User_Contacts_Only__c:boolean;
  export default MC4SF__User_Contacts_Only__c;
}
declare module "@salesforce/schema/MC4SF__MC_Query__c.MC4SF__User_Leads_Only__c" {
  const MC4SF__User_Leads_Only__c:boolean;
  export default MC4SF__User_Leads_Only__c;
}
