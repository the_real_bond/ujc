declare module "@salesforce/schema/MC4SF__MC_Campaign_Hourly_Stats__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign_Hourly_Stats__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign_Hourly_Stats__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign_Hourly_Stats__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign_Hourly_Stats__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign_Hourly_Stats__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign_Hourly_Stats__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign_Hourly_Stats__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign_Hourly_Stats__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign_Hourly_Stats__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign_Hourly_Stats__ChangeEvent.MC4SF__MC_Campaign__c" {
  const MC4SF__MC_Campaign__c:any;
  export default MC4SF__MC_Campaign__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign_Hourly_Stats__ChangeEvent.MC4SF__Emails_Sent__c" {
  const MC4SF__Emails_Sent__c:number;
  export default MC4SF__Emails_Sent__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign_Hourly_Stats__ChangeEvent.MC4SF__Recipients_Click__c" {
  const MC4SF__Recipients_Click__c:number;
  export default MC4SF__Recipients_Click__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign_Hourly_Stats__ChangeEvent.MC4SF__Statistics_Hour__c" {
  const MC4SF__Statistics_Hour__c:any;
  export default MC4SF__Statistics_Hour__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign_Hourly_Stats__ChangeEvent.MC4SF__Unique_Opens__c" {
  const MC4SF__Unique_Opens__c:number;
  export default MC4SF__Unique_Opens__c;
}
