declare module "@salesforce/schema/Salesforce_Check_List__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Salesforce_Check_List__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Salesforce_Check_List__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Salesforce_Check_List__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Salesforce_Check_List__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Salesforce_Check_List__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Salesforce_Check_List__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Salesforce_Check_List__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Salesforce_Check_List__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Salesforce_Check_List__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Salesforce_Check_List__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Salesforce_Check_List__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Salesforce_Check_List__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/Salesforce_Check_List__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Salesforce_Check_List__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Salesforce_Check_List__c.Issue_Group__c" {
  const Issue_Group__c:string;
  export default Issue_Group__c;
}
declare module "@salesforce/schema/Salesforce_Check_List__c.Status__c" {
  const Status__c:string;
  export default Status__c;
}
declare module "@salesforce/schema/Salesforce_Check_List__c.Assigned_Consultant__c" {
  const Assigned_Consultant__c:string;
  export default Assigned_Consultant__c;
}
declare module "@salesforce/schema/Salesforce_Check_List__c.Minimum_Estimate_Hours__c" {
  const Minimum_Estimate_Hours__c:number;
  export default Minimum_Estimate_Hours__c;
}
declare module "@salesforce/schema/Salesforce_Check_List__c.Maximum_Estimate_Hours__c" {
  const Maximum_Estimate_Hours__c:number;
  export default Maximum_Estimate_Hours__c;
}
declare module "@salesforce/schema/Salesforce_Check_List__c.Actual_Hours__c" {
  const Actual_Hours__c:number;
  export default Actual_Hours__c;
}
declare module "@salesforce/schema/Salesforce_Check_List__c.Project_Description__c" {
  const Project_Description__c:string;
  export default Project_Description__c;
}
declare module "@salesforce/schema/Salesforce_Check_List__c.Project_Detail__c" {
  const Project_Detail__c:string;
  export default Project_Detail__c;
}
declare module "@salesforce/schema/Salesforce_Check_List__c.Priority__c" {
  const Priority__c:string;
  export default Priority__c;
}
