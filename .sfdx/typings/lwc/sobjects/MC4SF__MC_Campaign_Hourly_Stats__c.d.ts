declare module "@salesforce/schema/MC4SF__MC_Campaign_Hourly_Stats__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign_Hourly_Stats__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign_Hourly_Stats__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign_Hourly_Stats__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign_Hourly_Stats__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign_Hourly_Stats__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign_Hourly_Stats__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign_Hourly_Stats__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign_Hourly_Stats__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign_Hourly_Stats__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign_Hourly_Stats__c.MC4SF__MC_Campaign__r" {
  const MC4SF__MC_Campaign__r:any;
  export default MC4SF__MC_Campaign__r;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign_Hourly_Stats__c.MC4SF__MC_Campaign__c" {
  const MC4SF__MC_Campaign__c:any;
  export default MC4SF__MC_Campaign__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign_Hourly_Stats__c.MC4SF__Emails_Sent__c" {
  const MC4SF__Emails_Sent__c:number;
  export default MC4SF__Emails_Sent__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign_Hourly_Stats__c.MC4SF__Recipients_Click__c" {
  const MC4SF__Recipients_Click__c:number;
  export default MC4SF__Recipients_Click__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign_Hourly_Stats__c.MC4SF__Statistics_Hour__c" {
  const MC4SF__Statistics_Hour__c:any;
  export default MC4SF__Statistics_Hour__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign_Hourly_Stats__c.MC4SF__Unique_Opens__c" {
  const MC4SF__Unique_Opens__c:number;
  export default MC4SF__Unique_Opens__c;
}
