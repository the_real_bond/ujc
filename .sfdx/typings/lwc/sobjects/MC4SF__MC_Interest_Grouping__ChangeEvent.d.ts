declare module "@salesforce/schema/MC4SF__MC_Interest_Grouping__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Grouping__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Grouping__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Grouping__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Grouping__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Grouping__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Grouping__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Grouping__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Grouping__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Grouping__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Grouping__ChangeEvent.MC4SF__MC_List__c" {
  const MC4SF__MC_List__c:any;
  export default MC4SF__MC_List__c;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Grouping__ChangeEvent.MC4SF__Form_Field__c" {
  const MC4SF__Form_Field__c:string;
  export default MC4SF__Form_Field__c;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Grouping__ChangeEvent.MC4SF__MailChimp_ID__c" {
  const MC4SF__MailChimp_ID__c:number;
  export default MC4SF__MailChimp_ID__c;
}
