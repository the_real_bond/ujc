declare module "@salesforce/schema/CnP_PaaS__DynamicReport__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__DynamicReport__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__DynamicReport__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__DynamicReport__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__DynamicReport__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__DynamicReport__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__DynamicReport__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__DynamicReport__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__DynamicReport__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__DynamicReport__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__DynamicReport__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__DynamicReport__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__DynamicReport__ChangeEvent.CnP_PaaS__PageName__c" {
  const CnP_PaaS__PageName__c:string;
  export default CnP_PaaS__PageName__c;
}
declare module "@salesforce/schema/CnP_PaaS__DynamicReport__ChangeEvent.CnP_PaaS__Report_Name__c" {
  const CnP_PaaS__Report_Name__c:string;
  export default CnP_PaaS__Report_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__DynamicReport__ChangeEvent.CnP_PaaS__application_name__c" {
  const CnP_PaaS__application_name__c:string;
  export default CnP_PaaS__application_name__c;
}
declare module "@salesforce/schema/CnP_PaaS__DynamicReport__ChangeEvent.CnP_PaaS__query__c" {
  const CnP_PaaS__query__c:string;
  export default CnP_PaaS__query__c;
}
