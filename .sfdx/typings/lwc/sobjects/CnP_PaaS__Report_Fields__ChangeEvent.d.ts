declare module "@salesforce/schema/CnP_PaaS__Report_Fields__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Report_Fields__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__Report_Fields__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__Report_Fields__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__Report_Fields__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__Report_Fields__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Report_Fields__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Report_Fields__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Report_Fields__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Report_Fields__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Report_Fields__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Report_Fields__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Report_Fields__ChangeEvent.CnP_PaaS__Condition__c" {
  const CnP_PaaS__Condition__c:string;
  export default CnP_PaaS__Condition__c;
}
declare module "@salesforce/schema/CnP_PaaS__Report_Fields__ChangeEvent.CnP_PaaS__Page_Name__c" {
  const CnP_PaaS__Page_Name__c:string;
  export default CnP_PaaS__Page_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__Report_Fields__ChangeEvent.CnP_PaaS__fieldtype__c" {
  const CnP_PaaS__fieldtype__c:string;
  export default CnP_PaaS__fieldtype__c;
}
declare module "@salesforce/schema/CnP_PaaS__Report_Fields__ChangeEvent.CnP_PaaS__reportid__c" {
  const CnP_PaaS__reportid__c:any;
  export default CnP_PaaS__reportid__c;
}
declare module "@salesforce/schema/CnP_PaaS__Report_Fields__ChangeEvent.CnP_PaaS__value__c" {
  const CnP_PaaS__value__c:string;
  export default CnP_PaaS__value__c;
}
