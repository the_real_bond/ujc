declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.CnP_PaaS__Answer_Condition__c" {
  const CnP_PaaS__Answer_Condition__c:string;
  export default CnP_PaaS__Answer_Condition__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.CnP_PaaS__Assigned_Account__r" {
  const CnP_PaaS__Assigned_Account__r:any;
  export default CnP_PaaS__Assigned_Account__r;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.CnP_PaaS__Assigned_Account__c" {
  const CnP_PaaS__Assigned_Account__c:any;
  export default CnP_PaaS__Assigned_Account__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.CnP_PaaS__Assigned_Contact__r" {
  const CnP_PaaS__Assigned_Contact__r:any;
  export default CnP_PaaS__Assigned_Contact__r;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.CnP_PaaS__Assigned_Contact__c" {
  const CnP_PaaS__Assigned_Contact__c:any;
  export default CnP_PaaS__Assigned_Contact__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.CnP_PaaS__Condition__c" {
  const CnP_PaaS__Condition__c:string;
  export default CnP_PaaS__Condition__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.CnP_PaaS__Contact_Role__c" {
  const CnP_PaaS__Contact_Role__c:string;
  export default CnP_PaaS__Contact_Role__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.CnP_PaaS__Contact__r" {
  const CnP_PaaS__Contact__r:any;
  export default CnP_PaaS__Contact__r;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.CnP_PaaS__Contact__c" {
  const CnP_PaaS__Contact__c:any;
  export default CnP_PaaS__Contact__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.CnP_PaaS__Date_Now_Minus_Number__c" {
  const CnP_PaaS__Date_Now_Minus_Number__c:number;
  export default CnP_PaaS__Date_Now_Minus_Number__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.CnP_PaaS__Date_Now_Plus_Number__c" {
  const CnP_PaaS__Date_Now_Plus_Number__c:number;
  export default CnP_PaaS__Date_Now_Plus_Number__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.CnP_PaaS__Days__c" {
  const CnP_PaaS__Days__c:string;
  export default CnP_PaaS__Days__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.CnP_PaaS__Field_Map__c" {
  const CnP_PaaS__Field_Map__c:string;
  export default CnP_PaaS__Field_Map__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.CnP_PaaS__Object_Map__c" {
  const CnP_PaaS__Object_Map__c:string;
  export default CnP_PaaS__Object_Map__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.CnP_PaaS__Object_Parent_Id__c" {
  const CnP_PaaS__Object_Parent_Id__c:string;
  export default CnP_PaaS__Object_Parent_Id__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.CnP_PaaS__Object_Record__c" {
  const CnP_PaaS__Object_Record__c:string;
  export default CnP_PaaS__Object_Record__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.CnP_PaaS__Original_Answer__c" {
  const CnP_PaaS__Original_Answer__c:string;
  export default CnP_PaaS__Original_Answer__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.CnP_PaaS__Original_Relation__c" {
  const CnP_PaaS__Original_Relation__c:string;
  export default CnP_PaaS__Original_Relation__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.CnP_PaaS__Question_Condition__c" {
  const CnP_PaaS__Question_Condition__c:string;
  export default CnP_PaaS__Question_Condition__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.CnP_PaaS__Reciprocal_Relationship__c" {
  const CnP_PaaS__Reciprocal_Relationship__c:string;
  export default CnP_PaaS__Reciprocal_Relationship__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.CnP_PaaS__Replaced_Answer__c" {
  const CnP_PaaS__Replaced_Answer__c:string;
  export default CnP_PaaS__Replaced_Answer__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.CnP_PaaS__SKU_Condition__c" {
  const CnP_PaaS__SKU_Condition__c:string;
  export default CnP_PaaS__SKU_Condition__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.CnP_PaaS__SKUvalue__c" {
  const CnP_PaaS__SKUvalue__c:string;
  export default CnP_PaaS__SKUvalue__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.CnP_PaaS__Select_Date__c" {
  const CnP_PaaS__Select_Date__c:string;
  export default CnP_PaaS__Select_Date__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.CnP_PaaS__Sku__c" {
  const CnP_PaaS__Sku__c:string;
  export default CnP_PaaS__Sku__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.CnP_PaaS__Soft_Credit_Setting__c" {
  const CnP_PaaS__Soft_Credit_Setting__c:string;
  export default CnP_PaaS__Soft_Credit_Setting__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.CnP_PaaS__Status_Change_Date__c" {
  const CnP_PaaS__Status_Change_Date__c:any;
  export default CnP_PaaS__Status_Change_Date__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.CnP_PaaS__Status__c" {
  const CnP_PaaS__Status__c:string;
  export default CnP_PaaS__Status__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.CnP_PaaS__Type__c" {
  const CnP_PaaS__Type__c:string;
  export default CnP_PaaS__Type__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.CnP_PaaS__date_entered__c" {
  const CnP_PaaS__date_entered__c:string;
  export default CnP_PaaS__date_entered__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.CnP_PaaS__soft_credit_percent__c" {
  const CnP_PaaS__soft_credit_percent__c:number;
  export default CnP_PaaS__soft_credit_percent__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Custom_Question_Settings__c.CnP_PaaS__softcredit__c" {
  const CnP_PaaS__softcredit__c:boolean;
  export default CnP_PaaS__softcredit__c;
}
