declare module "@salesforce/schema/PowerLoader__BulkEditTemplateField__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplateField__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplateField__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplateField__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplateField__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplateField__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplateField__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplateField__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplateField__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplateField__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplateField__ChangeEvent.PowerLoader__BulkEditTemplate__c" {
  const PowerLoader__BulkEditTemplate__c:any;
  export default PowerLoader__BulkEditTemplate__c;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplateField__ChangeEvent.PowerLoader__FieldOrder__c" {
  const PowerLoader__FieldOrder__c:number;
  export default PowerLoader__FieldOrder__c;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplateField__ChangeEvent.PowerLoader__Group__c" {
  const PowerLoader__Group__c:boolean;
  export default PowerLoader__Group__c;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplateField__ChangeEvent.PowerLoader__SplitAt__c" {
  const PowerLoader__SplitAt__c:boolean;
  export default PowerLoader__SplitAt__c;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplateField__ChangeEvent.PowerLoader__SummaryType__c" {
  const PowerLoader__SummaryType__c:string;
  export default PowerLoader__SummaryType__c;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplateField__ChangeEvent.PowerLoader__Width__c" {
  const PowerLoader__Width__c:number;
  export default PowerLoader__Width__c;
}
