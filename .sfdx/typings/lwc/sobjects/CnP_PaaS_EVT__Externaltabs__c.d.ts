declare module "@salesforce/schema/CnP_PaaS_EVT__Externaltabs__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Externaltabs__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Externaltabs__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Externaltabs__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Externaltabs__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Externaltabs__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Externaltabs__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Externaltabs__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Externaltabs__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Externaltabs__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Externaltabs__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Externaltabs__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Externaltabs__c.CnP_PaaS_EVT__C_P_Event__r" {
  const CnP_PaaS_EVT__C_P_Event__r:any;
  export default CnP_PaaS_EVT__C_P_Event__r;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Externaltabs__c.CnP_PaaS_EVT__C_P_Event__c" {
  const CnP_PaaS_EVT__C_P_Event__c:any;
  export default CnP_PaaS_EVT__C_P_Event__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Externaltabs__c.CnP_PaaS_EVT__C_P_Widget__r" {
  const CnP_PaaS_EVT__C_P_Widget__r:any;
  export default CnP_PaaS_EVT__C_P_Widget__r;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Externaltabs__c.CnP_PaaS_EVT__C_P_Widget__c" {
  const CnP_PaaS_EVT__C_P_Widget__c:any;
  export default CnP_PaaS_EVT__C_P_Widget__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Externaltabs__c.CnP_PaaS_EVT__Tab_order_number__c" {
  const CnP_PaaS_EVT__Tab_order_number__c:number;
  export default CnP_PaaS_EVT__Tab_order_number__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Externaltabs__c.CnP_PaaS_EVT__content_tab1__c" {
  const CnP_PaaS_EVT__content_tab1__c:string;
  export default CnP_PaaS_EVT__content_tab1__c;
}
