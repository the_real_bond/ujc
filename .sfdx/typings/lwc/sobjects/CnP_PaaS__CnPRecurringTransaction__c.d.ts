declare module "@salesforce/schema/CnP_PaaS__CnPRecurringTransaction__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurringTransaction__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurringTransaction__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurringTransaction__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurringTransaction__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurringTransaction__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurringTransaction__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurringTransaction__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurringTransaction__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurringTransaction__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurringTransaction__c.CnP_PaaS__Recurring_Transaction__r" {
  const CnP_PaaS__Recurring_Transaction__r:any;
  export default CnP_PaaS__Recurring_Transaction__r;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurringTransaction__c.CnP_PaaS__Recurring_Transaction__c" {
  const CnP_PaaS__Recurring_Transaction__c:any;
  export default CnP_PaaS__Recurring_Transaction__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurringTransaction__c.CnP_PaaS__TransactionId__r" {
  const CnP_PaaS__TransactionId__r:any;
  export default CnP_PaaS__TransactionId__r;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurringTransaction__c.CnP_PaaS__TransactionId__c" {
  const CnP_PaaS__TransactionId__c:any;
  export default CnP_PaaS__TransactionId__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurringTransaction__c.CnP_PaaS__InstallmentNumber__c" {
  const CnP_PaaS__InstallmentNumber__c:number;
  export default CnP_PaaS__InstallmentNumber__c;
}
