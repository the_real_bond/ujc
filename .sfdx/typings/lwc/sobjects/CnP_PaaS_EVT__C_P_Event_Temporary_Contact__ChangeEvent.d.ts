declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__ChangeEvent.CnP_PaaS_EVT__AttendeeID__c" {
  const CnP_PaaS_EVT__AttendeeID__c:any;
  export default CnP_PaaS_EVT__AttendeeID__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__ChangeEvent.CnP_PaaS_EVT__C_P_Data__c" {
  const CnP_PaaS_EVT__C_P_Data__c:any;
  export default CnP_PaaS_EVT__C_P_Data__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__ChangeEvent.CnP_PaaS_EVT__C_P_Event_Registrant__c" {
  const CnP_PaaS_EVT__C_P_Event_Registrant__c:any;
  export default CnP_PaaS_EVT__C_P_Event_Registrant__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__ChangeEvent.CnP_PaaS_EVT__Contact_Data__c" {
  const CnP_PaaS_EVT__Contact_Data__c:string;
  export default CnP_PaaS_EVT__Contact_Data__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__ChangeEvent.CnP_PaaS_EVT__Email__c" {
  const CnP_PaaS_EVT__Email__c:string;
  export default CnP_PaaS_EVT__Email__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__ChangeEvent.CnP_PaaS_EVT__Event_name__c" {
  const CnP_PaaS_EVT__Event_name__c:any;
  export default CnP_PaaS_EVT__Event_name__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__ChangeEvent.CnP_PaaS_EVT__First_name__c" {
  const CnP_PaaS_EVT__First_name__c:string;
  export default CnP_PaaS_EVT__First_name__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__ChangeEvent.CnP_PaaS_EVT__Last_name__c" {
  const CnP_PaaS_EVT__Last_name__c:string;
  export default CnP_PaaS_EVT__Last_name__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__ChangeEvent.CnP_PaaS_EVT__Map_Contact__c" {
  const CnP_PaaS_EVT__Map_Contact__c:any;
  export default CnP_PaaS_EVT__Map_Contact__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__ChangeEvent.CnP_PaaS_EVT__Order_Number__c" {
  const CnP_PaaS_EVT__Order_Number__c:string;
  export default CnP_PaaS_EVT__Order_Number__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__C_P_Event_Temporary_Contact__ChangeEvent.CnP_PaaS_EVT__XML_Data__c" {
  const CnP_PaaS_EVT__XML_Data__c:string;
  export default CnP_PaaS_EVT__XML_Data__c;
}
