declare module "@salesforce/schema/Field_Trip__Object_Analysis__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__ChangeEvent.Field_Trip__Filter__c" {
  const Field_Trip__Filter__c:string;
  export default Field_Trip__Filter__c;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__ChangeEvent.Field_Trip__Last_Analyzed__c" {
  const Field_Trip__Last_Analyzed__c:any;
  export default Field_Trip__Last_Analyzed__c;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__ChangeEvent.Field_Trip__Last_Batch_Id__c" {
  const Field_Trip__Last_Batch_Id__c:string;
  export default Field_Trip__Last_Batch_Id__c;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__ChangeEvent.Field_Trip__Object_Label__c" {
  const Field_Trip__Object_Label__c:string;
  export default Field_Trip__Object_Label__c;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__ChangeEvent.Field_Trip__Object_Name__c" {
  const Field_Trip__Object_Name__c:string;
  export default Field_Trip__Object_Name__c;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__ChangeEvent.Field_Trip__Record_Types__c" {
  const Field_Trip__Record_Types__c:number;
  export default Field_Trip__Record_Types__c;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__ChangeEvent.Field_Trip__Records__c" {
  const Field_Trip__Records__c:number;
  export default Field_Trip__Records__c;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__ChangeEvent.Field_Trip__Tally__c" {
  const Field_Trip__Tally__c:number;
  export default Field_Trip__Tally__c;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__ChangeEvent.Field_Trip__isCustom__c" {
  const Field_Trip__isCustom__c:boolean;
  export default Field_Trip__isCustom__c;
}
declare module "@salesforce/schema/Field_Trip__Object_Analysis__ChangeEvent.Field_Trip__Fields__c" {
  const Field_Trip__Fields__c:number;
  export default Field_Trip__Fields__c;
}
