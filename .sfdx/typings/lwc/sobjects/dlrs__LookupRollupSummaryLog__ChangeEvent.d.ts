declare module "@salesforce/schema/dlrs__LookupRollupSummaryLog__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryLog__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryLog__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryLog__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryLog__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryLog__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryLog__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryLog__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryLog__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryLog__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryLog__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryLog__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryLog__ChangeEvent.dlrs__ErrorMessage__c" {
  const dlrs__ErrorMessage__c:string;
  export default dlrs__ErrorMessage__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryLog__ChangeEvent.dlrs__ParentId__c" {
  const dlrs__ParentId__c:string;
  export default dlrs__ParentId__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryLog__ChangeEvent.dlrs__ParentObject__c" {
  const dlrs__ParentObject__c:string;
  export default dlrs__ParentObject__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryLog__ChangeEvent.dlrs__ParentRecord__c" {
  const dlrs__ParentRecord__c:string;
  export default dlrs__ParentRecord__c;
}
