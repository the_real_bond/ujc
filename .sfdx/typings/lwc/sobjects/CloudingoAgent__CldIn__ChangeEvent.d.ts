declare module "@salesforce/schema/CloudingoAgent__CldIn__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__ChangeEvent.CloudingoAgent__P1__c" {
  const CloudingoAgent__P1__c:string;
  export default CloudingoAgent__P1__c;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__ChangeEvent.CloudingoAgent__P2__c" {
  const CloudingoAgent__P2__c:string;
  export default CloudingoAgent__P2__c;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__ChangeEvent.CloudingoAgent__P3__c" {
  const CloudingoAgent__P3__c:string;
  export default CloudingoAgent__P3__c;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__ChangeEvent.CloudingoAgent__P4__c" {
  const CloudingoAgent__P4__c:string;
  export default CloudingoAgent__P4__c;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__ChangeEvent.CloudingoAgent__P5__c" {
  const CloudingoAgent__P5__c:string;
  export default CloudingoAgent__P5__c;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__ChangeEvent.CloudingoAgent__R1__c" {
  const CloudingoAgent__R1__c:string;
  export default CloudingoAgent__R1__c;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__ChangeEvent.CloudingoAgent__T1__c" {
  const CloudingoAgent__T1__c:string;
  export default CloudingoAgent__T1__c;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__ChangeEvent.CloudingoAgent__stat__c" {
  const CloudingoAgent__stat__c:number;
  export default CloudingoAgent__stat__c;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__ChangeEvent.CloudingoAgent__wsm__c" {
  const CloudingoAgent__wsm__c:number;
  export default CloudingoAgent__wsm__c;
}
