declare module "@salesforce/schema/ckt__Text_Message_Template__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__ChangeEvent.ckt__Available_For_Use__c" {
  const ckt__Available_For_Use__c:boolean;
  export default ckt__Available_For_Use__c;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__ChangeEvent.ckt__Description__c" {
  const ckt__Description__c:string;
  export default ckt__Description__c;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__ChangeEvent.ckt__Folder__c" {
  const ckt__Folder__c:any;
  export default ckt__Folder__c;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__ChangeEvent.ckt__Template_Unique_Name__c" {
  const ckt__Template_Unique_Name__c:string;
  export default ckt__Template_Unique_Name__c;
}
declare module "@salesforce/schema/ckt__Text_Message_Template__ChangeEvent.ckt__Text_Message_Body__c" {
  const ckt__Text_Message_Body__c:string;
  export default ckt__Text_Message_Body__c;
}
