declare module "@salesforce/schema/Issue_Item__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Issue_Item__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/Issue_Item__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/Issue_Item__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Issue_Item__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Issue_Item__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Issue_Item__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Issue_Item__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Issue_Item__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Issue_Item__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Issue_Item__ChangeEvent.Issue__c" {
  const Issue__c:string;
  export default Issue__c;
}
declare module "@salesforce/schema/Issue_Item__ChangeEvent.Status__c" {
  const Status__c:string;
  export default Status__c;
}
declare module "@salesforce/schema/Issue_Item__ChangeEvent.Salesforce_Check_List__c" {
  const Salesforce_Check_List__c:any;
  export default Salesforce_Check_List__c;
}
declare module "@salesforce/schema/Issue_Item__ChangeEvent.Description__c" {
  const Description__c:string;
  export default Description__c;
}
declare module "@salesforce/schema/Issue_Item__ChangeEvent.Project_Detail__c" {
  const Project_Detail__c:string;
  export default Project_Detail__c;
}
