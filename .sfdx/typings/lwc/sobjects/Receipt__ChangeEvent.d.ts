declare module "@salesforce/schema/Receipt__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.IUA__c" {
  const IUA__c:number;
  export default IUA__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Receipt_Date__c" {
  const Receipt_Date__c:any;
  export default Receipt_Date__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.UCF_Communal__c" {
  const UCF_Communal__c:number;
  export default UCF_Communal__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.UCF_Education__c" {
  const UCF_Education__c:number;
  export default UCF_Education__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Welfare__c" {
  const Welfare__c:number;
  export default Welfare__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Total_Receipt_Amount__c" {
  const Total_Receipt_Amount__c:number;
  export default Total_Receipt_Amount__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Pledge__c" {
  const Pledge__c:any;
  export default Pledge__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Trans_Key__c" {
  const Trans_Key__c:string;
  export default Trans_Key__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Donor__c" {
  const Donor__c:any;
  export default Donor__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Receipt_Number__c" {
  const Receipt_Number__c:string;
  export default Receipt_Number__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Payment_Status__c" {
  const Payment_Status__c:string;
  export default Payment_Status__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Method_of_Payment__c" {
  const Method_of_Payment__c:string;
  export default Method_of_Payment__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.ReceiptEmail__c" {
  const ReceiptEmail__c:string;
  export default ReceiptEmail__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Do_Not_Send_Receipt__c" {
  const Do_Not_Send_Receipt__c:boolean;
  export default Do_Not_Send_Receipt__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Thank_you_Sent__c" {
  const Thank_you_Sent__c:boolean;
  export default Thank_you_Sent__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.TodayDate__c" {
  const TodayDate__c:string;
  export default TodayDate__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.ReceiptCountUCFED__c" {
  const ReceiptCountUCFED__c:number;
  export default ReceiptCountUCFED__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.ReceiptCount__c" {
  const ReceiptCount__c:number;
  export default ReceiptCount__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Receipt_Date_Email__c" {
  const Receipt_Date_Email__c:string;
  export default Receipt_Date_Email__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Last_Receipt_Date__c" {
  const Last_Receipt_Date__c:any;
  export default Last_Receipt_Date__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Next_Receipt_Date__c" {
  const Next_Receipt_Date__c:any;
  export default Next_Receipt_Date__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Date_Signed_Receipts__c" {
  const Date_Signed_Receipts__c:any;
  export default Date_Signed_Receipts__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Create_Datetime__c" {
  const Create_Datetime__c:any;
  export default Create_Datetime__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.RecName__c" {
  const RecName__c:string;
  export default RecName__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Payment_Commission__c" {
  const Payment_Commission__c:number;
  export default Payment_Commission__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Key_Formula__c" {
  const Key_Formula__c:string;
  export default Key_Formula__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Key__c" {
  const Key__c:string;
  export default Key__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.CC_Batch__c" {
  const CC_Batch__c:number;
  export default CC_Batch__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Receipt_Total_Checker__c" {
  const Receipt_Total_Checker__c:number;
  export default Receipt_Total_Checker__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Receipt_Query__c" {
  const Receipt_Query__c:boolean;
  export default Receipt_Query__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Query_Reason__c" {
  const Query_Reason__c:string;
  export default Query_Reason__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Welfare_Outstanding__c" {
  const Welfare_Outstanding__c:number;
  export default Welfare_Outstanding__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.IUA_Outstanding__c" {
  const IUA_Outstanding__c:number;
  export default IUA_Outstanding__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.UCF_Communal_Outstanding__c" {
  const UCF_Communal_Outstanding__c:number;
  export default UCF_Communal_Outstanding__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.UCF_Education_Outstanding__c" {
  const UCF_Education_Outstanding__c:number;
  export default UCF_Education_Outstanding__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Query_Status__c" {
  const Query_Status__c:string;
  export default Query_Status__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Bank_Code_Unpaid__c" {
  const Bank_Code_Unpaid__c:string;
  export default Bank_Code_Unpaid__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Query_Resolution__c" {
  const Query_Resolution__c:string;
  export default Query_Resolution__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Query_Notes__c" {
  const Query_Notes__c:string;
  export default Query_Notes__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Reference_Details__c" {
  const Reference_Details__c:string;
  export default Reference_Details__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.IUA_PriorValue__c" {
  const IUA_PriorValue__c:number;
  export default IUA_PriorValue__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Prior_Campaign_Value__c" {
  const Prior_Campaign_Value__c:string;
  export default Prior_Campaign_Value__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Receipt_Total__c" {
  const Receipt_Total__c:number;
  export default Receipt_Total__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Welfare_Portion__c" {
  const Welfare_Portion__c:number;
  export default Welfare_Portion__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.IUA_Portion__c" {
  const IUA_Portion__c:number;
  export default IUA_Portion__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.UCF_Ed_Portion__c" {
  const UCF_Ed_Portion__c:number;
  export default UCF_Ed_Portion__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.UCF_Comm_Portion__c" {
  const UCF_Comm_Portion__c:number;
  export default UCF_Comm_Portion__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Welfare_PriorValue__c" {
  const Welfare_PriorValue__c:number;
  export default Welfare_PriorValue__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.UCF_Ed_PriorValue__c" {
  const UCF_Ed_PriorValue__c:number;
  export default UCF_Ed_PriorValue__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.UCF_Comm_PriorValue__c" {
  const UCF_Comm_PriorValue__c:number;
  export default UCF_Comm_PriorValue__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Pledge_Prior_Value__c" {
  const Pledge_Prior_Value__c:number;
  export default Pledge_Prior_Value__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Modified_Date__c" {
  const Modified_Date__c:any;
  export default Modified_Date__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Created_Date__c" {
  const Created_Date__c:any;
  export default Created_Date__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Receipt_Adjustment__c" {
  const Receipt_Adjustment__c:boolean;
  export default Receipt_Adjustment__c;
}
declare module "@salesforce/schema/Receipt__ChangeEvent.Receipt_Type__c" {
  const Receipt_Type__c:string;
  export default Receipt_Type__c;
}
