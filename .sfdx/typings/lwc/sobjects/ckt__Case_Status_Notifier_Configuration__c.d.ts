declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__c.ckt__Include_case_contact__c" {
  const ckt__Include_case_contact__c:boolean;
  export default ckt__Include_case_contact__c;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__c.ckt__Include_case_owner__c" {
  const ckt__Include_case_owner__c:boolean;
  export default ckt__Include_case_owner__c;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__c.ckt__Message__c" {
  const ckt__Message__c:string;
  export default ckt__Message__c;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__c.ckt__Recipients__c" {
  const ckt__Recipients__c:string;
  export default ckt__Recipients__c;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__c.ckt__Status__c" {
  const ckt__Status__c:string;
  export default ckt__Status__c;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__c.ckt__Template_Folder_Name__c" {
  const ckt__Template_Folder_Name__c:string;
  export default ckt__Template_Folder_Name__c;
}
declare module "@salesforce/schema/ckt__Case_Status_Notifier_Configuration__c.ckt__Template_Name__c" {
  const ckt__Template_Name__c:string;
  export default ckt__Template_Name__c;
}
