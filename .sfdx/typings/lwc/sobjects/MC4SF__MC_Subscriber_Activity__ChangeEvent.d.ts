declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__ChangeEvent.MC4SF__MC_Subscriber__c" {
  const MC4SF__MC_Subscriber__c:any;
  export default MC4SF__MC_Subscriber__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__ChangeEvent.MC4SF__Action__c" {
  const MC4SF__Action__c:string;
  export default MC4SF__Action__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__ChangeEvent.MC4SF__Bounce_Type__c" {
  const MC4SF__Bounce_Type__c:string;
  export default MC4SF__Bounce_Type__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__ChangeEvent.MC4SF__MC_Campaign__c" {
  const MC4SF__MC_Campaign__c:any;
  export default MC4SF__MC_Campaign__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__ChangeEvent.MC4SF__MC_List__c" {
  const MC4SF__MC_List__c:any;
  export default MC4SF__MC_List__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__ChangeEvent.MC4SF__MailChimp_Campaign_ID__c" {
  const MC4SF__MailChimp_Campaign_ID__c:string;
  export default MC4SF__MailChimp_Campaign_ID__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__ChangeEvent.MC4SF__Text_URL__c" {
  const MC4SF__Text_URL__c:string;
  export default MC4SF__Text_URL__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__ChangeEvent.MC4SF__Timestamp__c" {
  const MC4SF__Timestamp__c:any;
  export default MC4SF__Timestamp__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__ChangeEvent.MC4SF__Type__c" {
  const MC4SF__Type__c:string;
  export default MC4SF__Type__c;
}
declare module "@salesforce/schema/MC4SF__MC_Subscriber_Activity__ChangeEvent.MC4SF__URL__c" {
  const MC4SF__URL__c:string;
  export default MC4SF__URL__c;
}
