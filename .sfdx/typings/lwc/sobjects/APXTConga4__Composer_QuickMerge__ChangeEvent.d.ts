declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__ChangeEvent.APXTConga4__Conga_Solution__c" {
  const APXTConga4__Conga_Solution__c:any;
  export default APXTConga4__Conga_Solution__c;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__ChangeEvent.APXTConga4__Description__c" {
  const APXTConga4__Description__c:string;
  export default APXTConga4__Description__c;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__ChangeEvent.APXTConga4__Launch_CM8__c" {
  const APXTConga4__Launch_CM8__c:string;
  export default APXTConga4__Launch_CM8__c;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__ChangeEvent.APXTConga4__Weblink_ID_Formula__c" {
  const APXTConga4__Weblink_ID_Formula__c:string;
  export default APXTConga4__Weblink_ID_Formula__c;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__ChangeEvent.APXTConga4__Weblink_ID__c" {
  const APXTConga4__Weblink_ID__c:string;
  export default APXTConga4__Weblink_ID__c;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__ChangeEvent.APXTConga4__Weblink_Name_Formula__c" {
  const APXTConga4__Weblink_Name_Formula__c:string;
  export default APXTConga4__Weblink_Name_Formula__c;
}
declare module "@salesforce/schema/APXTConga4__Composer_QuickMerge__ChangeEvent.APXTConga4__Weblink_Name__c" {
  const APXTConga4__Weblink_Name__c:string;
  export default APXTConga4__Weblink_Name__c;
}
