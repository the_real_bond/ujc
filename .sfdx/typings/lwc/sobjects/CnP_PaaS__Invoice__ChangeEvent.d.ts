declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__Addship__c" {
  const CnP_PaaS__Addship__c:boolean;
  export default CnP_PaaS__Addship__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__Allow_for_Editing__c" {
  const CnP_PaaS__Allow_for_Editing__c:boolean;
  export default CnP_PaaS__Allow_for_Editing__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__Billing_Address2__c" {
  const CnP_PaaS__Billing_Address2__c:string;
  export default CnP_PaaS__Billing_Address2__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__Billing_Address__c" {
  const CnP_PaaS__Billing_Address__c:string;
  export default CnP_PaaS__Billing_Address__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__C_P_Transaction__c" {
  const CnP_PaaS__C_P_Transaction__c:any;
  export default CnP_PaaS__C_P_Transaction__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__Campaign_Text__c" {
  const CnP_PaaS__Campaign_Text__c:string;
  export default CnP_PaaS__Campaign_Text__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__City__c" {
  const CnP_PaaS__City__c:string;
  export default CnP_PaaS__City__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__Convenience_Fee__c" {
  const CnP_PaaS__Convenience_Fee__c:number;
  export default CnP_PaaS__Convenience_Fee__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__Country__c" {
  const CnP_PaaS__Country__c:string;
  export default CnP_PaaS__Country__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__Currency_Code__c" {
  const CnP_PaaS__Currency_Code__c:string;
  export default CnP_PaaS__Currency_Code__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__Email__c" {
  const CnP_PaaS__Email__c:string;
  export default CnP_PaaS__Email__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__Final_Date__c" {
  const CnP_PaaS__Final_Date__c:any;
  export default CnP_PaaS__Final_Date__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__First_Name__c" {
  const CnP_PaaS__First_Name__c:string;
  export default CnP_PaaS__First_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__Guid__c" {
  const CnP_PaaS__Guid__c:string;
  export default CnP_PaaS__Guid__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__Invoice_Account_GUID__c" {
  const CnP_PaaS__Invoice_Account_GUID__c:string;
  export default CnP_PaaS__Invoice_Account_GUID__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__Invoice_Account_Id__c" {
  const CnP_PaaS__Invoice_Account_Id__c:string;
  export default CnP_PaaS__Invoice_Account_Id__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__Invoice_Check__c" {
  const CnP_PaaS__Invoice_Check__c:boolean;
  export default CnP_PaaS__Invoice_Check__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__Invoice_Contact__c" {
  const CnP_PaaS__Invoice_Contact__c:any;
  export default CnP_PaaS__Invoice_Contact__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__Invoice_Number__c" {
  const CnP_PaaS__Invoice_Number__c:string;
  export default CnP_PaaS__Invoice_Number__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__Invoice_Policy__c" {
  const CnP_PaaS__Invoice_Policy__c:any;
  export default CnP_PaaS__Invoice_Policy__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__Last_Name__c" {
  const CnP_PaaS__Last_Name__c:string;
  export default CnP_PaaS__Last_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__Organization_Information__c" {
  const CnP_PaaS__Organization_Information__c:string;
  export default CnP_PaaS__Organization_Information__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__Phone__c" {
  const CnP_PaaS__Phone__c:string;
  export default CnP_PaaS__Phone__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__Sameship__c" {
  const CnP_PaaS__Sameship__c:boolean;
  export default CnP_PaaS__Sameship__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__Send_Receipt__c" {
  const CnP_PaaS__Send_Receipt__c:boolean;
  export default CnP_PaaS__Send_Receipt__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__ShipCountry__c" {
  const CnP_PaaS__ShipCountry__c:string;
  export default CnP_PaaS__ShipCountry__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__ShipName__c" {
  const CnP_PaaS__ShipName__c:string;
  export default CnP_PaaS__ShipName__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__Ship_Address2__c" {
  const CnP_PaaS__Ship_Address2__c:string;
  export default CnP_PaaS__Ship_Address2__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__Ship_Address__c" {
  const CnP_PaaS__Ship_Address__c:string;
  export default CnP_PaaS__Ship_Address__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__Ship_Lastname__c" {
  const CnP_PaaS__Ship_Lastname__c:string;
  export default CnP_PaaS__Ship_Lastname__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__Shipcode__c" {
  const CnP_PaaS__Shipcode__c:string;
  export default CnP_PaaS__Shipcode__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__Shipinvoice__c" {
  const CnP_PaaS__Shipinvoice__c:boolean;
  export default CnP_PaaS__Shipinvoice__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__Shipphone__c" {
  const CnP_PaaS__Shipphone__c:string;
  export default CnP_PaaS__Shipphone__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__Shipping_Email__c" {
  const CnP_PaaS__Shipping_Email__c:string;
  export default CnP_PaaS__Shipping_Email__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__State__c" {
  const CnP_PaaS__State__c:string;
  export default CnP_PaaS__State__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__Status__c" {
  const CnP_PaaS__Status__c:string;
  export default CnP_PaaS__Status__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__TemplateName__c" {
  const CnP_PaaS__TemplateName__c:any;
  export default CnP_PaaS__TemplateName__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__Terms_Conditions__c" {
  const CnP_PaaS__Terms_Conditions__c:string;
  export default CnP_PaaS__Terms_Conditions__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__Thank_you__c" {
  const CnP_PaaS__Thank_you__c:string;
  export default CnP_PaaS__Thank_you__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__Tracker__c" {
  const CnP_PaaS__Tracker__c:string;
  export default CnP_PaaS__Tracker__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__WebTemplate__c" {
  const CnP_PaaS__WebTemplate__c:any;
  export default CnP_PaaS__WebTemplate__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__Zip_Code__c" {
  const CnP_PaaS__Zip_Code__c:string;
  export default CnP_PaaS__Zip_Code__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__invoicedate__c" {
  const CnP_PaaS__invoicedate__c:any;
  export default CnP_PaaS__invoicedate__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__invoicesent__c" {
  const CnP_PaaS__invoicesent__c:boolean;
  export default CnP_PaaS__invoicesent__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__sent_date__c" {
  const CnP_PaaS__sent_date__c:any;
  export default CnP_PaaS__sent_date__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__sent_status__c" {
  const CnP_PaaS__sent_status__c:string;
  export default CnP_PaaS__sent_status__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__shipState__c" {
  const CnP_PaaS__shipState__c:string;
  export default CnP_PaaS__shipState__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__shipcity__c" {
  const CnP_PaaS__shipcity__c:string;
  export default CnP_PaaS__shipcity__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__shipping__c" {
  const CnP_PaaS__shipping__c:number;
  export default CnP_PaaS__shipping__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__specific_date__c" {
  const CnP_PaaS__specific_date__c:any;
  export default CnP_PaaS__specific_date__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice__ChangeEvent.CnP_PaaS__td_Date__c" {
  const CnP_PaaS__td_Date__c:any;
  export default CnP_PaaS__td_Date__c;
}
