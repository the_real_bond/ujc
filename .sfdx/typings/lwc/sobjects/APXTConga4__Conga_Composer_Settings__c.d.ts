declare module "@salesforce/schema/APXTConga4__Conga_Composer_Settings__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/APXTConga4__Conga_Composer_Settings__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/APXTConga4__Conga_Composer_Settings__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/APXTConga4__Conga_Composer_Settings__c.SetupOwner" {
  const SetupOwner:any;
  export default SetupOwner;
}
declare module "@salesforce/schema/APXTConga4__Conga_Composer_Settings__c.SetupOwnerId" {
  const SetupOwnerId:any;
  export default SetupOwnerId;
}
declare module "@salesforce/schema/APXTConga4__Conga_Composer_Settings__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/APXTConga4__Conga_Composer_Settings__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/APXTConga4__Conga_Composer_Settings__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/APXTConga4__Conga_Composer_Settings__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/APXTConga4__Conga_Composer_Settings__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/APXTConga4__Conga_Composer_Settings__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/APXTConga4__Conga_Composer_Settings__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/APXTConga4__Conga_Composer_Settings__c.APXTConga4__Production_Org_Id__c" {
  const APXTConga4__Production_Org_Id__c:string;
  export default APXTConga4__Production_Org_Id__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Composer_Settings__c.APXTConga4__Comments__c" {
  const APXTConga4__Comments__c:string;
  export default APXTConga4__Comments__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Composer_Settings__c.APXTConga4__Disable_C7_Triggers__c" {
  const APXTConga4__Disable_C7_Triggers__c:boolean;
  export default APXTConga4__Disable_C7_Triggers__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Composer_Settings__c.APXTConga4__Server_Override__c" {
  const APXTConga4__Server_Override__c:string;
  export default APXTConga4__Server_Override__c;
}
