declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__ChangeEvent.CnP_PaaS__Contact__c" {
  const CnP_PaaS__Contact__c:any;
  export default CnP_PaaS__Contact__c;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__ChangeEvent.CnP_PaaS__Alias_Contact_Data__c" {
  const CnP_PaaS__Alias_Contact_Data__c:string;
  export default CnP_PaaS__Alias_Contact_Data__c;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__ChangeEvent.CnP_PaaS__Billing_City__c" {
  const CnP_PaaS__Billing_City__c:string;
  export default CnP_PaaS__Billing_City__c;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__ChangeEvent.CnP_PaaS__Billing_Country__c" {
  const CnP_PaaS__Billing_Country__c:string;
  export default CnP_PaaS__Billing_Country__c;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__ChangeEvent.CnP_PaaS__Billing_Phone__c" {
  const CnP_PaaS__Billing_Phone__c:string;
  export default CnP_PaaS__Billing_Phone__c;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__ChangeEvent.CnP_PaaS__Billing_Postal_Code__c" {
  const CnP_PaaS__Billing_Postal_Code__c:string;
  export default CnP_PaaS__Billing_Postal_Code__c;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__ChangeEvent.CnP_PaaS__Billing_State__c" {
  const CnP_PaaS__Billing_State__c:string;
  export default CnP_PaaS__Billing_State__c;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__ChangeEvent.CnP_PaaS__Billing_Street__c" {
  const CnP_PaaS__Billing_Street__c:string;
  export default CnP_PaaS__Billing_Street__c;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__ChangeEvent.CnP_PaaS__Contact_Mail_id__c" {
  const CnP_PaaS__Contact_Mail_id__c:string;
  export default CnP_PaaS__Contact_Mail_id__c;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__ChangeEvent.CnP_PaaS__First_Name__c" {
  const CnP_PaaS__First_Name__c:string;
  export default CnP_PaaS__First_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__ChangeEvent.CnP_PaaS__Last_Name__c" {
  const CnP_PaaS__Last_Name__c:string;
  export default CnP_PaaS__Last_Name__c;
}
