declare module "@salesforce/schema/MC4SF__MC_Campaign__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Abuse_Reports__c" {
  const MC4SF__Abuse_Reports__c:number;
  export default MC4SF__Abuse_Reports__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Analytics_Tag__c" {
  const MC4SF__Analytics_Tag__c:string;
  export default MC4SF__Analytics_Tag__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Analytics__c" {
  const MC4SF__Analytics__c:string;
  export default MC4SF__Analytics__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Archive_URL__c" {
  const MC4SF__Archive_URL__c:string;
  export default MC4SF__Archive_URL__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Authenticate__c" {
  const MC4SF__Authenticate__c:boolean;
  export default MC4SF__Authenticate__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Auto_FB_Post__c" {
  const MC4SF__Auto_FB_Post__c:string;
  export default MC4SF__Auto_FB_Post__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Auto_Footer__c" {
  const MC4SF__Auto_Footer__c:boolean;
  export default MC4SF__Auto_Footer__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Auto_Tweet__c" {
  const MC4SF__Auto_Tweet__c:boolean;
  export default MC4SF__Auto_Tweet__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Campaign_Share_Report__c" {
  const MC4SF__Campaign_Share_Report__c:string;
  export default MC4SF__Campaign_Share_Report__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Clicks__c" {
  const MC4SF__Clicks__c:number;
  export default MC4SF__Clicks__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Content_Type__c" {
  const MC4SF__Content_Type__c:string;
  export default MC4SF__Content_Type__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Create_Time__c" {
  const MC4SF__Create_Time__c:any;
  export default MC4SF__Create_Time__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Ecomm360__c" {
  const MC4SF__Ecomm360__c:boolean;
  export default MC4SF__Ecomm360__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Emails_Sent__c" {
  const MC4SF__Emails_Sent__c:number;
  export default MC4SF__Emails_Sent__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Facebook_Likes__c" {
  const MC4SF__Facebook_Likes__c:number;
  export default MC4SF__Facebook_Likes__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Forwards_Opens__c" {
  const MC4SF__Forwards_Opens__c:number;
  export default MC4SF__Forwards_Opens__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Forwards__c" {
  const MC4SF__Forwards__c:number;
  export default MC4SF__Forwards__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__From_Email__c" {
  const MC4SF__From_Email__c:string;
  export default MC4SF__From_Email__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__From_Name__c" {
  const MC4SF__From_Name__c:string;
  export default MC4SF__From_Name__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__HTML_Clicks_Tracked__c" {
  const MC4SF__HTML_Clicks_Tracked__c:boolean;
  export default MC4SF__HTML_Clicks_Tracked__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Hard_Bounces__c" {
  const MC4SF__Hard_Bounces__c:number;
  export default MC4SF__Hard_Bounces__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Inline_CSS__c" {
  const MC4SF__Inline_CSS__c:boolean;
  export default MC4SF__Inline_CSS__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Last_Click__c" {
  const MC4SF__Last_Click__c:any;
  export default MC4SF__Last_Click__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Last_Open__c" {
  const MC4SF__Last_Open__c:any;
  export default MC4SF__Last_Open__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Link_to_MC_Campaign__c" {
  const MC4SF__Link_to_MC_Campaign__c:string;
  export default MC4SF__Link_to_MC_Campaign__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__MC_List__r" {
  const MC4SF__MC_List__r:any;
  export default MC4SF__MC_List__r;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__MC_List__c" {
  const MC4SF__MC_List__c:any;
  export default MC4SF__MC_List__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__MailChimp_Folder_ID__c" {
  const MC4SF__MailChimp_Folder_ID__c:string;
  export default MC4SF__MailChimp_Folder_ID__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__MailChimp_ID__c" {
  const MC4SF__MailChimp_ID__c:string;
  export default MC4SF__MailChimp_ID__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__MailChimp_List_ID__c" {
  const MC4SF__MailChimp_List_ID__c:string;
  export default MC4SF__MailChimp_List_ID__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__MailChimp_Template_ID__c" {
  const MC4SF__MailChimp_Template_ID__c:string;
  export default MC4SF__MailChimp_Template_ID__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__MailChimp_Web_ID__c" {
  const MC4SF__MailChimp_Web_ID__c:string;
  export default MC4SF__MailChimp_Web_ID__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Opens_Tracked__c" {
  const MC4SF__Opens_Tracked__c:boolean;
  export default MC4SF__Opens_Tracked__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Opens__c" {
  const MC4SF__Opens__c:number;
  export default MC4SF__Opens__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Recipient_Likes__c" {
  const MC4SF__Recipient_Likes__c:number;
  export default MC4SF__Recipient_Likes__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Report_Password__c" {
  const MC4SF__Report_Password__c:string;
  export default MC4SF__Report_Password__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Report_Secure_URL__c" {
  const MC4SF__Report_Secure_URL__c:string;
  export default MC4SF__Report_Secure_URL__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Report_URL__c" {
  const MC4SF__Report_URL__c:string;
  export default MC4SF__Report_URL__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Segment_Opts__c" {
  const MC4SF__Segment_Opts__c:string;
  export default MC4SF__Segment_Opts__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Segmented_Text__c" {
  const MC4SF__Segmented_Text__c:string;
  export default MC4SF__Segmented_Text__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Send_Time__c" {
  const MC4SF__Send_Time__c:any;
  export default MC4SF__Send_Time__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Soft_Bounces__c" {
  const MC4SF__Soft_Bounces__c:number;
  export default MC4SF__Soft_Bounces__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Status__c" {
  const MC4SF__Status__c:string;
  export default MC4SF__Status__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Subject__c" {
  const MC4SF__Subject__c:string;
  export default MC4SF__Subject__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Syntax_Errors__c" {
  const MC4SF__Syntax_Errors__c:number;
  export default MC4SF__Syntax_Errors__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Text_Clicks_Tracked__c" {
  const MC4SF__Text_Clicks_Tracked__c:boolean;
  export default MC4SF__Text_Clicks_Tracked__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Timewarp_Schedule__c" {
  const MC4SF__Timewarp_Schedule__c:string;
  export default MC4SF__Timewarp_Schedule__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Timewarp__c" {
  const MC4SF__Timewarp__c:boolean;
  export default MC4SF__Timewarp__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__To_Name__c" {
  const MC4SF__To_Name__c:string;
  export default MC4SF__To_Name__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Type_Opts__c" {
  const MC4SF__Type_Opts__c:string;
  export default MC4SF__Type_Opts__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Type__c" {
  const MC4SF__Type__c:string;
  export default MC4SF__Type__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Unique_Clicks__c" {
  const MC4SF__Unique_Clicks__c:number;
  export default MC4SF__Unique_Clicks__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Unique_Likes__c" {
  const MC4SF__Unique_Likes__c:number;
  export default MC4SF__Unique_Likes__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Unique_Opens__c" {
  const MC4SF__Unique_Opens__c:number;
  export default MC4SF__Unique_Opens__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Unsubscribes__c" {
  const MC4SF__Unsubscribes__c:number;
  export default MC4SF__Unsubscribes__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Users_Who_Clicked__c" {
  const MC4SF__Users_Who_Clicked__c:number;
  export default MC4SF__Users_Who_Clicked__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Hourly_Stats_Emails_Sent__c" {
  const MC4SF__Hourly_Stats_Emails_Sent__c:number;
  export default MC4SF__Hourly_Stats_Emails_Sent__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Hourly_Stats_Unique_Opens__c" {
  const MC4SF__Hourly_Stats_Unique_Opens__c:number;
  export default MC4SF__Hourly_Stats_Unique_Opens__c;
}
declare module "@salesforce/schema/MC4SF__MC_Campaign__c.MC4SF__Hourly_Status_Recipients_Click__c" {
  const MC4SF__Hourly_Status_Recipients_Click__c:number;
  export default MC4SF__Hourly_Status_Recipients_Click__c;
}
