declare module "@salesforce/schema/dlrs__LookupParent__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/dlrs__LookupParent__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/dlrs__LookupParent__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/dlrs__LookupParent__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/dlrs__LookupParent__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/dlrs__LookupParent__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/dlrs__LookupParent__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/dlrs__LookupParent__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/dlrs__LookupParent__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/dlrs__LookupParent__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/dlrs__LookupParent__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/dlrs__LookupParent__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/dlrs__LookupParent__c.dlrs__Total__c" {
  const dlrs__Total__c:number;
  export default dlrs__Total__c;
}
declare module "@salesforce/schema/dlrs__LookupParent__c.dlrs__Colours__c" {
  const dlrs__Colours__c:string;
  export default dlrs__Colours__c;
}
declare module "@salesforce/schema/dlrs__LookupParent__c.dlrs__Descriptions2__c" {
  const dlrs__Descriptions2__c:string;
  export default dlrs__Descriptions2__c;
}
declare module "@salesforce/schema/dlrs__LookupParent__c.dlrs__Descriptions__c" {
  const dlrs__Descriptions__c:string;
  export default dlrs__Descriptions__c;
}
declare module "@salesforce/schema/dlrs__LookupParent__c.dlrs__Total2__c" {
  const dlrs__Total2__c:number;
  export default dlrs__Total2__c;
}
