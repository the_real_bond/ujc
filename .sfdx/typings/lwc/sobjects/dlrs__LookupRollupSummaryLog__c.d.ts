declare module "@salesforce/schema/dlrs__LookupRollupSummaryLog__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryLog__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryLog__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryLog__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryLog__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryLog__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryLog__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryLog__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryLog__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryLog__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryLog__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryLog__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryLog__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryLog__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryLog__c.dlrs__ErrorMessage__c" {
  const dlrs__ErrorMessage__c:string;
  export default dlrs__ErrorMessage__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryLog__c.dlrs__ParentId__c" {
  const dlrs__ParentId__c:string;
  export default dlrs__ParentId__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryLog__c.dlrs__ParentObject__c" {
  const dlrs__ParentObject__c:string;
  export default dlrs__ParentObject__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryLog__c.dlrs__ParentRecord__c" {
  const dlrs__ParentRecord__c:string;
  export default dlrs__ParentRecord__c;
}
