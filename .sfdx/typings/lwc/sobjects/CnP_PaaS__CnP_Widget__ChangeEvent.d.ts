declare module "@salesforce/schema/CnP_PaaS__CnP_Widget__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Widget__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Widget__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Widget__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Widget__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Widget__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Widget__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Widget__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Widget__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Widget__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Widget__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Widget__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Widget__ChangeEvent.CnP_PaaS__Widget_Code__c" {
  const CnP_PaaS__Widget_Code__c:string;
  export default CnP_PaaS__Widget_Code__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Widget__ChangeEvent.CnP_PaaS_EVT__C_P_Event__c" {
  const CnP_PaaS_EVT__C_P_Event__c:any;
  export default CnP_PaaS_EVT__C_P_Event__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Widget__ChangeEvent.CnP_PaaS_EVT__Unique_FieldName__c" {
  const CnP_PaaS_EVT__Unique_FieldName__c:string;
  export default CnP_PaaS_EVT__Unique_FieldName__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Widget__ChangeEvent.CnP_PaaS_EVT__Widget_type__c" {
  const CnP_PaaS_EVT__Widget_type__c:string;
  export default CnP_PaaS_EVT__Widget_type__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Widget__ChangeEvent.CnP_PaaS_EVT__description__c" {
  const CnP_PaaS_EVT__description__c:string;
  export default CnP_PaaS_EVT__description__c;
}
