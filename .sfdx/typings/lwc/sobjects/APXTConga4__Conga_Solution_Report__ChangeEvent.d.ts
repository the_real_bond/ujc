declare module "@salesforce/schema/APXTConga4__Conga_Solution_Report__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Report__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Report__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Report__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Report__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Report__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Report__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Report__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Report__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Report__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Report__ChangeEvent.APXTConga4__Conga_Solution__c" {
  const APXTConga4__Conga_Solution__c:any;
  export default APXTConga4__Conga_Solution__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Report__ChangeEvent.APXTConga4__Alias__c" {
  const APXTConga4__Alias__c:string;
  export default APXTConga4__Alias__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Report__ChangeEvent.APXTConga4__Comments__c" {
  const APXTConga4__Comments__c:string;
  export default APXTConga4__Comments__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Report__ChangeEvent.APXTConga4__Report_Id_Link__c" {
  const APXTConga4__Report_Id_Link__c:string;
  export default APXTConga4__Report_Id_Link__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Report__ChangeEvent.APXTConga4__Report_Id__c" {
  const APXTConga4__Report_Id__c:string;
  export default APXTConga4__Report_Id__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Report__ChangeEvent.APXTConga4__Report_Name__c" {
  const APXTConga4__Report_Name__c:string;
  export default APXTConga4__Report_Name__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Report__ChangeEvent.APXTConga4__pv0__c" {
  const APXTConga4__pv0__c:string;
  export default APXTConga4__pv0__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Report__ChangeEvent.APXTConga4__pv1__c" {
  const APXTConga4__pv1__c:string;
  export default APXTConga4__pv1__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Report__ChangeEvent.APXTConga4__pv2__c" {
  const APXTConga4__pv2__c:string;
  export default APXTConga4__pv2__c;
}
