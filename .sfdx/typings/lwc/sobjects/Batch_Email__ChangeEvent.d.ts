declare module "@salesforce/schema/Batch_Email__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Batch_Email__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/Batch_Email__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/Batch_Email__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Batch_Email__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Batch_Email__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Batch_Email__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Batch_Email__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Batch_Email__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Batch_Email__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Batch_Email__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Batch_Email__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Batch_Email__ChangeEvent.Body1_Welfare__c" {
  const Body1_Welfare__c:string;
  export default Body1_Welfare__c;
}
declare module "@salesforce/schema/Batch_Email__ChangeEvent.Body_1_UCFED__c" {
  const Body_1_UCFED__c:string;
  export default Body_1_UCFED__c;
}
declare module "@salesforce/schema/Batch_Email__ChangeEvent.Date__c" {
  const Date__c:string;
  export default Date__c;
}
declare module "@salesforce/schema/Batch_Email__ChangeEvent.Email_Body__c" {
  const Email_Body__c:string;
  export default Email_Body__c;
}
declare module "@salesforce/schema/Batch_Email__ChangeEvent.Email_Subject_UCFED__c" {
  const Email_Subject_UCFED__c:string;
  export default Email_Subject_UCFED__c;
}
declare module "@salesforce/schema/Batch_Email__ChangeEvent.Email_Subject__c" {
  const Email_Subject__c:string;
  export default Email_Subject__c;
}
