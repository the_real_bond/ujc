declare module "@salesforce/schema/dlrs__LookupRollupCalculateJob__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/dlrs__LookupRollupCalculateJob__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/dlrs__LookupRollupCalculateJob__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/dlrs__LookupRollupCalculateJob__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/dlrs__LookupRollupCalculateJob__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/dlrs__LookupRollupCalculateJob__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/dlrs__LookupRollupCalculateJob__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/dlrs__LookupRollupCalculateJob__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/dlrs__LookupRollupCalculateJob__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/dlrs__LookupRollupCalculateJob__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/dlrs__LookupRollupCalculateJob__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/dlrs__LookupRollupCalculateJob__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/dlrs__LookupRollupCalculateJob__ChangeEvent.dlrs__LookupRollupSummaryId__c" {
  const dlrs__LookupRollupSummaryId__c:string;
  export default dlrs__LookupRollupSummaryId__c;
}
