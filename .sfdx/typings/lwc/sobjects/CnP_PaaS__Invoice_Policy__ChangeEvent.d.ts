declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__Acknowledgment__c" {
  const CnP_PaaS__Acknowledgment__c:boolean;
  export default CnP_PaaS__Acknowledgment__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__Additional_Payment__c" {
  const CnP_PaaS__Additional_Payment__c:boolean;
  export default CnP_PaaS__Additional_Payment__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__American_Express__c" {
  const CnP_PaaS__American_Express__c:boolean;
  export default CnP_PaaS__American_Express__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__C_P_Replay_To_Address__c" {
  const CnP_PaaS__C_P_Replay_To_Address__c:string;
  export default CnP_PaaS__C_P_Replay_To_Address__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__ClickandPledge_SMTP__c" {
  const CnP_PaaS__ClickandPledge_SMTP__c:string;
  export default CnP_PaaS__ClickandPledge_SMTP__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__CnP_API_Settings__c" {
  const CnP_PaaS__CnP_API_Settings__c:any;
  export default CnP_PaaS__CnP_API_Settings__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__CnP_Alert_Subject__c" {
  const CnP_PaaS__CnP_Alert_Subject__c:string;
  export default CnP_PaaS__CnP_Alert_Subject__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__CnP_Mail_From_Name__c" {
  const CnP_PaaS__CnP_Mail_From_Name__c:string;
  export default CnP_PaaS__CnP_Mail_From_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__Custom_Payment_Options__c" {
  const CnP_PaaS__Custom_Payment_Options__c:string;
  export default CnP_PaaS__Custom_Payment_Options__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__Custom_Payment_Type__c" {
  const CnP_PaaS__Custom_Payment_Type__c:boolean;
  export default CnP_PaaS__Custom_Payment_Type__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__Decline__c" {
  const CnP_PaaS__Decline__c:string;
  export default CnP_PaaS__Decline__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__Declined_Invoice_Stage__c" {
  const CnP_PaaS__Declined_Invoice_Stage__c:string;
  export default CnP_PaaS__Declined_Invoice_Stage__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__Discover__c" {
  const CnP_PaaS__Discover__c:boolean;
  export default CnP_PaaS__Discover__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__DueDate__c" {
  const CnP_PaaS__DueDate__c:string;
  export default CnP_PaaS__DueDate__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__Email_Alerts__c" {
  const CnP_PaaS__Email_Alerts__c:string;
  export default CnP_PaaS__Email_Alerts__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__Email_Template__c" {
  const CnP_PaaS__Email_Template__c:any;
  export default CnP_PaaS__Email_Template__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__Internal_Alerts__c" {
  const CnP_PaaS__Internal_Alerts__c:boolean;
  export default CnP_PaaS__Internal_Alerts__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__Invoice_Subject__c" {
  const CnP_PaaS__Invoice_Subject__c:string;
  export default CnP_PaaS__Invoice_Subject__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__Issued__c" {
  const CnP_PaaS__Issued__c:string;
  export default CnP_PaaS__Issued__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__JCB__c" {
  const CnP_PaaS__JCB__c:boolean;
  export default CnP_PaaS__JCB__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__Mail_From_Address__c" {
  const CnP_PaaS__Mail_From_Address__c:string;
  export default CnP_PaaS__Mail_From_Address__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__Master_Card__c" {
  const CnP_PaaS__Master_Card__c:boolean;
  export default CnP_PaaS__Master_Card__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__Net__c" {
  const CnP_PaaS__Net__c:string;
  export default CnP_PaaS__Net__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__Paid_Invoice__c" {
  const CnP_PaaS__Paid_Invoice__c:string;
  export default CnP_PaaS__Paid_Invoice__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__Partial_Payments__c" {
  const CnP_PaaS__Partial_Payments__c:boolean;
  export default CnP_PaaS__Partial_Payments__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__Past_Due_Invoice__c" {
  const CnP_PaaS__Past_Due_Invoice__c:string;
  export default CnP_PaaS__Past_Due_Invoice__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__Policy_Status__c" {
  const CnP_PaaS__Policy_Status__c:string;
  export default CnP_PaaS__Policy_Status__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__Purchase_Order__c" {
  const CnP_PaaS__Purchase_Order__c:boolean;
  export default CnP_PaaS__Purchase_Order__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__SMTP_settings__c" {
  const CnP_PaaS__SMTP_settings__c:any;
  export default CnP_PaaS__SMTP_settings__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__Send_Email__c" {
  const CnP_PaaS__Send_Email__c:string;
  export default CnP_PaaS__Send_Email__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__Send_To__c" {
  const CnP_PaaS__Send_To__c:string;
  export default CnP_PaaS__Send_To__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__Terms_Conditions__c" {
  const CnP_PaaS__Terms_Conditions__c:string;
  export default CnP_PaaS__Terms_Conditions__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__Thank_you__c" {
  const CnP_PaaS__Thank_you__c:string;
  export default CnP_PaaS__Thank_you__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__Transcation_date__c" {
  const CnP_PaaS__Transcation_date__c:any;
  export default CnP_PaaS__Transcation_date__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__Visa__c" {
  const CnP_PaaS__Visa__c:boolean;
  export default CnP_PaaS__Visa__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__Web_Template__c" {
  const CnP_PaaS__Web_Template__c:any;
  export default CnP_PaaS__Web_Template__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Policy__ChangeEvent.CnP_PaaS__echeck__c" {
  const CnP_PaaS__echeck__c:boolean;
  export default CnP_PaaS__echeck__c;
}
