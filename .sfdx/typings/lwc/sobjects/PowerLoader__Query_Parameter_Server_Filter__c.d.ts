declare module "@salesforce/schema/PowerLoader__Query_Parameter_Server_Filter__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter_Server_Filter__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter_Server_Filter__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter_Server_Filter__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter_Server_Filter__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter_Server_Filter__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter_Server_Filter__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter_Server_Filter__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter_Server_Filter__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter_Server_Filter__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter_Server_Filter__c.PowerLoader__Query_Parameter__r" {
  const PowerLoader__Query_Parameter__r:any;
  export default PowerLoader__Query_Parameter__r;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter_Server_Filter__c.PowerLoader__Query_Parameter__c" {
  const PowerLoader__Query_Parameter__c:any;
  export default PowerLoader__Query_Parameter__c;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter_Server_Filter__c.PowerLoader__Criteria_Value__c" {
  const PowerLoader__Criteria_Value__c:string;
  export default PowerLoader__Criteria_Value__c;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter_Server_Filter__c.PowerLoader__Criteria__c" {
  const PowerLoader__Criteria__c:string;
  export default PowerLoader__Criteria__c;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter_Server_Filter__c.PowerLoader__Filter_Operation__c" {
  const PowerLoader__Filter_Operation__c:string;
  export default PowerLoader__Filter_Operation__c;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter_Server_Filter__c.PowerLoader__Filter_Type__c" {
  const PowerLoader__Filter_Type__c:string;
  export default PowerLoader__Filter_Type__c;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter_Server_Filter__c.PowerLoader__Filter_Value__c" {
  const PowerLoader__Filter_Value__c:string;
  export default PowerLoader__Filter_Value__c;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter_Server_Filter__c.PowerLoader__Negate_Criteria__c" {
  const PowerLoader__Negate_Criteria__c:boolean;
  export default PowerLoader__Negate_Criteria__c;
}
