declare module "@salesforce/schema/APXTConga4__Conga_Solution_Parameter__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Parameter__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Parameter__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Parameter__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Parameter__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Parameter__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Parameter__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Parameter__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Parameter__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Parameter__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Parameter__ChangeEvent.APXTConga4__Conga_Solution__c" {
  const APXTConga4__Conga_Solution__c:any;
  export default APXTConga4__Conga_Solution__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Parameter__ChangeEvent.APXTConga4__Comments__c" {
  const APXTConga4__Comments__c:string;
  export default APXTConga4__Comments__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Parameter__ChangeEvent.APXTConga4__Name__c" {
  const APXTConga4__Name__c:string;
  export default APXTConga4__Name__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Solution_Parameter__ChangeEvent.APXTConga4__Value__c" {
  const APXTConga4__Value__c:string;
  export default APXTConga4__Value__c;
}
