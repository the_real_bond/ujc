declare module "@salesforce/schema/CnP_PaaS__Sub_Recordtypes__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Recordtypes__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Recordtypes__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Recordtypes__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Recordtypes__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Recordtypes__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Recordtypes__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Recordtypes__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Recordtypes__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Recordtypes__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Recordtypes__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Recordtypes__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Recordtypes__c.CnP_PaaS__C_P_Record_Type__r" {
  const CnP_PaaS__C_P_Record_Type__r:any;
  export default CnP_PaaS__C_P_Record_Type__r;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Recordtypes__c.CnP_PaaS__C_P_Record_Type__c" {
  const CnP_PaaS__C_P_Record_Type__c:any;
  export default CnP_PaaS__C_P_Record_Type__c;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Recordtypes__c.CnP_PaaS__Campaign__r" {
  const CnP_PaaS__Campaign__r:any;
  export default CnP_PaaS__Campaign__r;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Recordtypes__c.CnP_PaaS__Campaign__c" {
  const CnP_PaaS__Campaign__c:any;
  export default CnP_PaaS__Campaign__c;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Recordtypes__c.CnP_PaaS__Class__r" {
  const CnP_PaaS__Class__r:any;
  export default CnP_PaaS__Class__r;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Recordtypes__c.CnP_PaaS__Class__c" {
  const CnP_PaaS__Class__c:any;
  export default CnP_PaaS__Class__c;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Recordtypes__c.CnP_PaaS__GL_Account__r" {
  const CnP_PaaS__GL_Account__r:any;
  export default CnP_PaaS__GL_Account__r;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Recordtypes__c.CnP_PaaS__GL_Account__c" {
  const CnP_PaaS__GL_Account__c:any;
  export default CnP_PaaS__GL_Account__c;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Recordtypes__c.CnP_PaaS__Ledger_Name__c" {
  const CnP_PaaS__Ledger_Name__c:string;
  export default CnP_PaaS__Ledger_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Recordtypes__c.CnP_PaaS__Ledger__c" {
  const CnP_PaaS__Ledger__c:string;
  export default CnP_PaaS__Ledger__c;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Recordtypes__c.CnP_PaaS__Opportunity_Allocation__c" {
  const CnP_PaaS__Opportunity_Allocation__c:boolean;
  export default CnP_PaaS__Opportunity_Allocation__c;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Recordtypes__c.CnP_PaaS__Opportunity_Product_Allocation__c" {
  const CnP_PaaS__Opportunity_Product_Allocation__c:boolean;
  export default CnP_PaaS__Opportunity_Product_Allocation__c;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Recordtypes__c.CnP_PaaS__Opportunity_SKU_Condition__c" {
  const CnP_PaaS__Opportunity_SKU_Condition__c:string;
  export default CnP_PaaS__Opportunity_SKU_Condition__c;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Recordtypes__c.CnP_PaaS__Percentage_amount__c" {
  const CnP_PaaS__Percentage_amount__c:string;
  export default CnP_PaaS__Percentage_amount__c;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Recordtypes__c.CnP_PaaS__SKU_value__c" {
  const CnP_PaaS__SKU_value__c:string;
  export default CnP_PaaS__SKU_value__c;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Recordtypes__c.CnP_PaaS__Sub_Class__r" {
  const CnP_PaaS__Sub_Class__r:any;
  export default CnP_PaaS__Sub_Class__r;
}
declare module "@salesforce/schema/CnP_PaaS__Sub_Recordtypes__c.CnP_PaaS__Sub_Class__c" {
  const CnP_PaaS__Sub_Class__c:any;
  export default CnP_PaaS__Sub_Class__c;
}
