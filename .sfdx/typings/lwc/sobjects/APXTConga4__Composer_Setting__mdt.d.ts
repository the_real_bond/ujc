declare module "@salesforce/schema/APXTConga4__Composer_Setting__mdt.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/APXTConga4__Composer_Setting__mdt.DeveloperName" {
  const DeveloperName:string;
  export default DeveloperName;
}
declare module "@salesforce/schema/APXTConga4__Composer_Setting__mdt.MasterLabel" {
  const MasterLabel:string;
  export default MasterLabel;
}
declare module "@salesforce/schema/APXTConga4__Composer_Setting__mdt.Language" {
  const Language:string;
  export default Language;
}
declare module "@salesforce/schema/APXTConga4__Composer_Setting__mdt.NamespacePrefix" {
  const NamespacePrefix:string;
  export default NamespacePrefix;
}
declare module "@salesforce/schema/APXTConga4__Composer_Setting__mdt.Label" {
  const Label:string;
  export default Label;
}
declare module "@salesforce/schema/APXTConga4__Composer_Setting__mdt.QualifiedApiName" {
  const QualifiedApiName:string;
  export default QualifiedApiName;
}
declare module "@salesforce/schema/APXTConga4__Composer_Setting__mdt.APXTConga4__Connected_App_Setting__r" {
  const APXTConga4__Connected_App_Setting__r:any;
  export default APXTConga4__Connected_App_Setting__r;
}
declare module "@salesforce/schema/APXTConga4__Composer_Setting__mdt.APXTConga4__Connected_App_Setting__c" {
  const APXTConga4__Connected_App_Setting__c:any;
  export default APXTConga4__Connected_App_Setting__c;
}
declare module "@salesforce/schema/APXTConga4__Composer_Setting__mdt.APXTConga4__Enable_Key_Generation__c" {
  const APXTConga4__Enable_Key_Generation__c:boolean;
  export default APXTConga4__Enable_Key_Generation__c;
}
