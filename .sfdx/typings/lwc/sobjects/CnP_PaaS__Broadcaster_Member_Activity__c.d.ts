declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__c.CnP_PaaS__Broadcaster_Campaign__r" {
  const CnP_PaaS__Broadcaster_Campaign__r:any;
  export default CnP_PaaS__Broadcaster_Campaign__r;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__c.CnP_PaaS__Broadcaster_Campaign__c" {
  const CnP_PaaS__Broadcaster_Campaign__c:any;
  export default CnP_PaaS__Broadcaster_Campaign__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__c.CnP_PaaS__Abuses__c" {
  const CnP_PaaS__Abuses__c:number;
  export default CnP_PaaS__Abuses__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__c.CnP_PaaS__Bounces__c" {
  const CnP_PaaS__Bounces__c:number;
  export default CnP_PaaS__Bounces__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__c.CnP_PaaS__Broadcaster_List__r" {
  const CnP_PaaS__Broadcaster_List__r:any;
  export default CnP_PaaS__Broadcaster_List__r;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__c.CnP_PaaS__Broadcaster_List__c" {
  const CnP_PaaS__Broadcaster_List__c:any;
  export default CnP_PaaS__Broadcaster_List__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__c.CnP_PaaS__Clicks__c" {
  const CnP_PaaS__Clicks__c:number;
  export default CnP_PaaS__Clicks__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__c.CnP_PaaS__Contact__r" {
  const CnP_PaaS__Contact__r:any;
  export default CnP_PaaS__Contact__r;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__c.CnP_PaaS__Contact__c" {
  const CnP_PaaS__Contact__c:any;
  export default CnP_PaaS__Contact__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__c.CnP_PaaS__Opens__c" {
  const CnP_PaaS__Opens__c:number;
  export default CnP_PaaS__Opens__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__c.CnP_PaaS__Queued__c" {
  const CnP_PaaS__Queued__c:number;
  export default CnP_PaaS__Queued__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__c.CnP_PaaS__Sents__c" {
  const CnP_PaaS__Sents__c:number;
  export default CnP_PaaS__Sents__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__c.CnP_PaaS__Unsubscribes__c" {
  const CnP_PaaS__Unsubscribes__c:number;
  export default CnP_PaaS__Unsubscribes__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Member_Activity__c.CnP_PaaS__ecomm__c" {
  const CnP_PaaS__ecomm__c:number;
  export default CnP_PaaS__ecomm__c;
}
