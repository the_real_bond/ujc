declare module "@salesforce/schema/ContactChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/ContactChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/ContactChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/ContactChangeEvent.Account" {
  const Account:any;
  export default Account;
}
declare module "@salesforce/schema/ContactChangeEvent.AccountId" {
  const AccountId:any;
  export default AccountId;
}
declare module "@salesforce/schema/ContactChangeEvent.IsPersonAccount" {
  const IsPersonAccount:boolean;
  export default IsPersonAccount;
}
declare module "@salesforce/schema/ContactChangeEvent.LastName" {
  const LastName:string;
  export default LastName;
}
declare module "@salesforce/schema/ContactChangeEvent.FirstName" {
  const FirstName:string;
  export default FirstName;
}
declare module "@salesforce/schema/ContactChangeEvent.Salutation" {
  const Salutation:string;
  export default Salutation;
}
declare module "@salesforce/schema/ContactChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/ContactChangeEvent.RecordType" {
  const RecordType:any;
  export default RecordType;
}
declare module "@salesforce/schema/ContactChangeEvent.RecordTypeId" {
  const RecordTypeId:any;
  export default RecordTypeId;
}
declare module "@salesforce/schema/ContactChangeEvent.OtherStreet" {
  const OtherStreet:string;
  export default OtherStreet;
}
declare module "@salesforce/schema/ContactChangeEvent.OtherCity" {
  const OtherCity:string;
  export default OtherCity;
}
declare module "@salesforce/schema/ContactChangeEvent.OtherState" {
  const OtherState:string;
  export default OtherState;
}
declare module "@salesforce/schema/ContactChangeEvent.OtherPostalCode" {
  const OtherPostalCode:string;
  export default OtherPostalCode;
}
declare module "@salesforce/schema/ContactChangeEvent.OtherCountry" {
  const OtherCountry:string;
  export default OtherCountry;
}
declare module "@salesforce/schema/ContactChangeEvent.OtherLatitude" {
  const OtherLatitude:number;
  export default OtherLatitude;
}
declare module "@salesforce/schema/ContactChangeEvent.OtherLongitude" {
  const OtherLongitude:number;
  export default OtherLongitude;
}
declare module "@salesforce/schema/ContactChangeEvent.OtherGeocodeAccuracy" {
  const OtherGeocodeAccuracy:string;
  export default OtherGeocodeAccuracy;
}
declare module "@salesforce/schema/ContactChangeEvent.OtherAddress" {
  const OtherAddress:any;
  export default OtherAddress;
}
declare module "@salesforce/schema/ContactChangeEvent.MailingStreet" {
  const MailingStreet:string;
  export default MailingStreet;
}
declare module "@salesforce/schema/ContactChangeEvent.MailingCity" {
  const MailingCity:string;
  export default MailingCity;
}
declare module "@salesforce/schema/ContactChangeEvent.MailingState" {
  const MailingState:string;
  export default MailingState;
}
declare module "@salesforce/schema/ContactChangeEvent.MailingPostalCode" {
  const MailingPostalCode:string;
  export default MailingPostalCode;
}
declare module "@salesforce/schema/ContactChangeEvent.MailingCountry" {
  const MailingCountry:string;
  export default MailingCountry;
}
declare module "@salesforce/schema/ContactChangeEvent.MailingLatitude" {
  const MailingLatitude:number;
  export default MailingLatitude;
}
declare module "@salesforce/schema/ContactChangeEvent.MailingLongitude" {
  const MailingLongitude:number;
  export default MailingLongitude;
}
declare module "@salesforce/schema/ContactChangeEvent.MailingGeocodeAccuracy" {
  const MailingGeocodeAccuracy:string;
  export default MailingGeocodeAccuracy;
}
declare module "@salesforce/schema/ContactChangeEvent.MailingAddress" {
  const MailingAddress:any;
  export default MailingAddress;
}
declare module "@salesforce/schema/ContactChangeEvent.Phone" {
  const Phone:string;
  export default Phone;
}
declare module "@salesforce/schema/ContactChangeEvent.Fax" {
  const Fax:string;
  export default Fax;
}
declare module "@salesforce/schema/ContactChangeEvent.MobilePhone" {
  const MobilePhone:string;
  export default MobilePhone;
}
declare module "@salesforce/schema/ContactChangeEvent.HomePhone" {
  const HomePhone:string;
  export default HomePhone;
}
declare module "@salesforce/schema/ContactChangeEvent.OtherPhone" {
  const OtherPhone:string;
  export default OtherPhone;
}
declare module "@salesforce/schema/ContactChangeEvent.AssistantPhone" {
  const AssistantPhone:string;
  export default AssistantPhone;
}
declare module "@salesforce/schema/ContactChangeEvent.ReportsTo" {
  const ReportsTo:any;
  export default ReportsTo;
}
declare module "@salesforce/schema/ContactChangeEvent.ReportsToId" {
  const ReportsToId:any;
  export default ReportsToId;
}
declare module "@salesforce/schema/ContactChangeEvent.Email" {
  const Email:string;
  export default Email;
}
declare module "@salesforce/schema/ContactChangeEvent.Title" {
  const Title:string;
  export default Title;
}
declare module "@salesforce/schema/ContactChangeEvent.Department" {
  const Department:string;
  export default Department;
}
declare module "@salesforce/schema/ContactChangeEvent.AssistantName" {
  const AssistantName:string;
  export default AssistantName;
}
declare module "@salesforce/schema/ContactChangeEvent.LeadSource" {
  const LeadSource:string;
  export default LeadSource;
}
declare module "@salesforce/schema/ContactChangeEvent.Birthdate" {
  const Birthdate:any;
  export default Birthdate;
}
declare module "@salesforce/schema/ContactChangeEvent.Description" {
  const Description:string;
  export default Description;
}
declare module "@salesforce/schema/ContactChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/ContactChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/ContactChangeEvent.HasOptedOutOfEmail" {
  const HasOptedOutOfEmail:boolean;
  export default HasOptedOutOfEmail;
}
declare module "@salesforce/schema/ContactChangeEvent.DoNotCall" {
  const DoNotCall:boolean;
  export default DoNotCall;
}
declare module "@salesforce/schema/ContactChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/ContactChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/ContactChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/ContactChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/ContactChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/ContactChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/ContactChangeEvent.LastCURequestDate" {
  const LastCURequestDate:any;
  export default LastCURequestDate;
}
declare module "@salesforce/schema/ContactChangeEvent.LastCUUpdateDate" {
  const LastCUUpdateDate:any;
  export default LastCUUpdateDate;
}
declare module "@salesforce/schema/ContactChangeEvent.EmailBouncedReason" {
  const EmailBouncedReason:string;
  export default EmailBouncedReason;
}
declare module "@salesforce/schema/ContactChangeEvent.EmailBouncedDate" {
  const EmailBouncedDate:any;
  export default EmailBouncedDate;
}
declare module "@salesforce/schema/ContactChangeEvent.Jigsaw" {
  const Jigsaw:string;
  export default Jigsaw;
}
declare module "@salesforce/schema/ContactChangeEvent.JigsawContactId" {
  const JigsawContactId:string;
  export default JigsawContactId;
}
declare module "@salesforce/schema/ContactChangeEvent.Individual" {
  const Individual:any;
  export default Individual;
}
declare module "@salesforce/schema/ContactChangeEvent.IndividualId" {
  const IndividualId:any;
  export default IndividualId;
}
declare module "@salesforce/schema/ContactChangeEvent.ckt__Opt_Out__c" {
  const ckt__Opt_Out__c:boolean;
  export default ckt__Opt_Out__c;
}
declare module "@salesforce/schema/ContactChangeEvent.tecnics__Next_Birth_Day__c" {
  const tecnics__Next_Birth_Day__c:any;
  export default tecnics__Next_Birth_Day__c;
}
declare module "@salesforce/schema/ContactChangeEvent.MC4SF__MC_Subscriber__c" {
  const MC4SF__MC_Subscriber__c:any;
  export default MC4SF__MC_Subscriber__c;
}
declare module "@salesforce/schema/ContactChangeEvent.CnP_PaaS__Alias_Contact_Data__c" {
  const CnP_PaaS__Alias_Contact_Data__c:string;
  export default CnP_PaaS__Alias_Contact_Data__c;
}
declare module "@salesforce/schema/ContactChangeEvent.CnP_PaaS__CnP_Connect_Alias_Index__c" {
  const CnP_PaaS__CnP_Connect_Alias_Index__c:string;
  export default CnP_PaaS__CnP_Connect_Alias_Index__c;
}
declare module "@salesforce/schema/ContactChangeEvent.CnP_PaaS__CnP_Global_Rank__c" {
  const CnP_PaaS__CnP_Global_Rank__c:number;
  export default CnP_PaaS__CnP_Global_Rank__c;
}
declare module "@salesforce/schema/ContactChangeEvent.CnP_PaaS__CnP_Total_Intrinsic__c" {
  const CnP_PaaS__CnP_Total_Intrinsic__c:number;
  export default CnP_PaaS__CnP_Total_Intrinsic__c;
}
declare module "@salesforce/schema/ContactChangeEvent.CnP_PaaS__CnP_Total_extrinsic__c" {
  const CnP_PaaS__CnP_Total_extrinsic__c:number;
  export default CnP_PaaS__CnP_Total_extrinsic__c;
}
declare module "@salesforce/schema/ContactChangeEvent.CnP_PaaS__Connect_Link__c" {
  const CnP_PaaS__Connect_Link__c:string;
  export default CnP_PaaS__Connect_Link__c;
}
