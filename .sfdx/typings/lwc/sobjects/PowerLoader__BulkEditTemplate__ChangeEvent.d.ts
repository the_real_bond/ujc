declare module "@salesforce/schema/PowerLoader__BulkEditTemplate__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplate__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplate__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplate__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplate__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplate__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplate__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplate__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplate__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplate__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplate__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplate__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplate__ChangeEvent.PowerLoader__Default__c" {
  const PowerLoader__Default__c:boolean;
  export default PowerLoader__Default__c;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplate__ChangeEvent.PowerLoader__JustMyRecords__c" {
  const PowerLoader__JustMyRecords__c:boolean;
  export default PowerLoader__JustMyRecords__c;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplate__ChangeEvent.PowerLoader__ObjectAPIName__c" {
  const PowerLoader__ObjectAPIName__c:string;
  export default PowerLoader__ObjectAPIName__c;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplate__ChangeEvent.PowerLoader__Private__c" {
  const PowerLoader__Private__c:boolean;
  export default PowerLoader__Private__c;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplate__ChangeEvent.PowerLoader__UID__c" {
  const PowerLoader__UID__c:string;
  export default PowerLoader__UID__c;
}
