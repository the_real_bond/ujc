declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__c.CnP_PaaS__Broadcaster_List__r" {
  const CnP_PaaS__Broadcaster_List__r:any;
  export default CnP_PaaS__Broadcaster_List__r;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__c.CnP_PaaS__Broadcaster_List__c" {
  const CnP_PaaS__Broadcaster_List__c:any;
  export default CnP_PaaS__Broadcaster_List__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__c.CnP_PaaS__Field_Id__c" {
  const CnP_PaaS__Field_Id__c:number;
  export default CnP_PaaS__Field_Id__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__c.CnP_PaaS__Req__c" {
  const CnP_PaaS__Req__c:boolean;
  export default CnP_PaaS__Req__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__c.CnP_PaaS__Tag__c" {
  const CnP_PaaS__Tag__c:string;
  export default CnP_PaaS__Tag__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__c.CnP_PaaS__helptext__c" {
  const CnP_PaaS__helptext__c:string;
  export default CnP_PaaS__helptext__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__c.CnP_PaaS__order__c" {
  const CnP_PaaS__order__c:string;
  export default CnP_PaaS__order__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__c.CnP_PaaS__show__c" {
  const CnP_PaaS__show__c:boolean;
  export default CnP_PaaS__show__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__c.CnP_PaaS__size__c" {
  const CnP_PaaS__size__c:string;
  export default CnP_PaaS__size__c;
}
