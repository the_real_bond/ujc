declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__ChangeEvent.CnP_PaaS__AuthorizeTransactions__c" {
  const CnP_PaaS__AuthorizeTransactions__c:number;
  export default CnP_PaaS__AuthorizeTransactions__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__ChangeEvent.CnP_PaaS__CnPAID__c" {
  const CnP_PaaS__CnPAID__c:string;
  export default CnP_PaaS__CnPAID__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__ChangeEvent.CnP_PaaS__DeclineTransactions__c" {
  const CnP_PaaS__DeclineTransactions__c:number;
  export default CnP_PaaS__DeclineTransactions__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__ChangeEvent.CnP_PaaS__Errors__c" {
  const CnP_PaaS__Errors__c:number;
  export default CnP_PaaS__Errors__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__ChangeEvent.CnP_PaaS__FileContent__c" {
  const CnP_PaaS__FileContent__c:string;
  export default CnP_PaaS__FileContent__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__ChangeEvent.CnP_PaaS__FileHeader__c" {
  const CnP_PaaS__FileHeader__c:string;
  export default CnP_PaaS__FileHeader__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__ChangeEvent.CnP_PaaS__FileName__c" {
  const CnP_PaaS__FileName__c:string;
  export default CnP_PaaS__FileName__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__ChangeEvent.CnP_PaaS__ProcessingDate__c" {
  const CnP_PaaS__ProcessingDate__c:any;
  export default CnP_PaaS__ProcessingDate__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__ChangeEvent.CnP_PaaS__Upload_Status__c" {
  const CnP_PaaS__Upload_Status__c:string;
  export default CnP_PaaS__Upload_Status__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__ChangeEvent.CnP_PaaS__Username__c" {
  const CnP_PaaS__Username__c:string;
  export default CnP_PaaS__Username__c;
}
