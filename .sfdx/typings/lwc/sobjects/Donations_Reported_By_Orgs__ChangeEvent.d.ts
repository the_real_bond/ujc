declare module "@salesforce/schema/Donations_Reported_By_Orgs__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Donations_Reported_By_Orgs__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/Donations_Reported_By_Orgs__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/Donations_Reported_By_Orgs__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Donations_Reported_By_Orgs__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Donations_Reported_By_Orgs__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Donations_Reported_By_Orgs__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Donations_Reported_By_Orgs__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Donations_Reported_By_Orgs__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Donations_Reported_By_Orgs__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Donations_Reported_By_Orgs__ChangeEvent.Year__c" {
  const Year__c:string;
  export default Year__c;
}
declare module "@salesforce/schema/Donations_Reported_By_Orgs__ChangeEvent.Organisation__c" {
  const Organisation__c:string;
  export default Organisation__c;
}
declare module "@salesforce/schema/Donations_Reported_By_Orgs__ChangeEvent.Amount__c" {
  const Amount__c:number;
  export default Amount__c;
}
declare module "@salesforce/schema/Donations_Reported_By_Orgs__ChangeEvent.Account__c" {
  const Account__c:any;
  export default Account__c;
}
