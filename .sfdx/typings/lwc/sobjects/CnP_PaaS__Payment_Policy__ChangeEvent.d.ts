declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__ChangeEvent.CnP_PaaS__Additional_Payment__c" {
  const CnP_PaaS__Additional_Payment__c:boolean;
  export default CnP_PaaS__Additional_Payment__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__ChangeEvent.CnP_PaaS__Campaign__c" {
  const CnP_PaaS__Campaign__c:any;
  export default CnP_PaaS__Campaign__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__ChangeEvent.CnP_PaaS__CnP_Reminder_Subject__c" {
  const CnP_PaaS__CnP_Reminder_Subject__c:string;
  export default CnP_PaaS__CnP_Reminder_Subject__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__ChangeEvent.CnP_PaaS__Early_Amount__c" {
  const CnP_PaaS__Early_Amount__c:number;
  export default CnP_PaaS__Early_Amount__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__ChangeEvent.CnP_PaaS__Early_Days__c" {
  const CnP_PaaS__Early_Days__c:string;
  export default CnP_PaaS__Early_Days__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__ChangeEvent.CnP_PaaS__Early_Discount__c" {
  const CnP_PaaS__Early_Discount__c:number;
  export default CnP_PaaS__Early_Discount__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__ChangeEvent.CnP_PaaS__Early_Payment__c" {
  const CnP_PaaS__Early_Payment__c:boolean;
  export default CnP_PaaS__Early_Payment__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__ChangeEvent.CnP_PaaS__Invoice_Due__c" {
  const CnP_PaaS__Invoice_Due__c:string;
  export default CnP_PaaS__Invoice_Due__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__ChangeEvent.CnP_PaaS__Invoice_Polici__c" {
  const CnP_PaaS__Invoice_Polici__c:any;
  export default CnP_PaaS__Invoice_Polici__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__ChangeEvent.CnP_PaaS__Late_Amount__c" {
  const CnP_PaaS__Late_Amount__c:number;
  export default CnP_PaaS__Late_Amount__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__ChangeEvent.CnP_PaaS__Late_Days__c" {
  const CnP_PaaS__Late_Days__c:string;
  export default CnP_PaaS__Late_Days__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__ChangeEvent.CnP_PaaS__Late_Fee__c" {
  const CnP_PaaS__Late_Fee__c:number;
  export default CnP_PaaS__Late_Fee__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__ChangeEvent.CnP_PaaS__Late_Payment__c" {
  const CnP_PaaS__Late_Payment__c:boolean;
  export default CnP_PaaS__Late_Payment__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__ChangeEvent.CnP_PaaS__Late_SKU__c" {
  const CnP_PaaS__Late_SKU__c:string;
  export default CnP_PaaS__Late_SKU__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__ChangeEvent.CnP_PaaS__Payment_Name__c" {
  const CnP_PaaS__Payment_Name__c:string;
  export default CnP_PaaS__Payment_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__ChangeEvent.CnP_PaaS__Remaindersent__c" {
  const CnP_PaaS__Remaindersent__c:boolean;
  export default CnP_PaaS__Remaindersent__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__ChangeEvent.CnP_PaaS__Reminder__c" {
  const CnP_PaaS__Reminder__c:boolean;
  export default CnP_PaaS__Reminder__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__ChangeEvent.CnP_PaaS__SKU__c" {
  const CnP_PaaS__SKU__c:string;
  export default CnP_PaaS__SKU__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__ChangeEvent.CnP_PaaS__Send__c" {
  const CnP_PaaS__Send__c:string;
  export default CnP_PaaS__Send__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__ChangeEvent.CnP_PaaS__Tax_Deductible__c" {
  const CnP_PaaS__Tax_Deductible__c:number;
  export default CnP_PaaS__Tax_Deductible__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__ChangeEvent.CnP_PaaS__Tax__c" {
  const CnP_PaaS__Tax__c:number;
  export default CnP_PaaS__Tax__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__ChangeEvent.CnP_PaaS__Template__c" {
  const CnP_PaaS__Template__c:string;
  export default CnP_PaaS__Template__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__ChangeEvent.CnP_PaaS__remainderdate__c" {
  const CnP_PaaS__remainderdate__c:any;
  export default CnP_PaaS__remainderdate__c;
}
