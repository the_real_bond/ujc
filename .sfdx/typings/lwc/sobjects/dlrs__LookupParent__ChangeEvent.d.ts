declare module "@salesforce/schema/dlrs__LookupParent__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/dlrs__LookupParent__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/dlrs__LookupParent__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/dlrs__LookupParent__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/dlrs__LookupParent__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/dlrs__LookupParent__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/dlrs__LookupParent__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/dlrs__LookupParent__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/dlrs__LookupParent__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/dlrs__LookupParent__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/dlrs__LookupParent__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/dlrs__LookupParent__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/dlrs__LookupParent__ChangeEvent.dlrs__Total__c" {
  const dlrs__Total__c:number;
  export default dlrs__Total__c;
}
declare module "@salesforce/schema/dlrs__LookupParent__ChangeEvent.dlrs__Colours__c" {
  const dlrs__Colours__c:string;
  export default dlrs__Colours__c;
}
declare module "@salesforce/schema/dlrs__LookupParent__ChangeEvent.dlrs__Descriptions2__c" {
  const dlrs__Descriptions2__c:string;
  export default dlrs__Descriptions2__c;
}
declare module "@salesforce/schema/dlrs__LookupParent__ChangeEvent.dlrs__Descriptions__c" {
  const dlrs__Descriptions__c:string;
  export default dlrs__Descriptions__c;
}
declare module "@salesforce/schema/dlrs__LookupParent__ChangeEvent.dlrs__Total2__c" {
  const dlrs__Total2__c:number;
  export default dlrs__Total2__c;
}
