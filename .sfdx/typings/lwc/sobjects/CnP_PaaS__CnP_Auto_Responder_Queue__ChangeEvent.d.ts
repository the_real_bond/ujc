declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Queue__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Queue__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Queue__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Queue__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Queue__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Queue__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Queue__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Queue__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Queue__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Queue__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Queue__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Queue__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Queue__ChangeEvent.CnP_PaaS__Application_Name__c" {
  const CnP_PaaS__Application_Name__c:string;
  export default CnP_PaaS__Application_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Queue__ChangeEvent.CnP_PaaS__CnP_Auto_Responder_Settings__c" {
  const CnP_PaaS__CnP_Auto_Responder_Settings__c:any;
  export default CnP_PaaS__CnP_Auto_Responder_Settings__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Queue__ChangeEvent.CnP_PaaS__CnP_Designer__c" {
  const CnP_PaaS__CnP_Designer__c:any;
  export default CnP_PaaS__CnP_Designer__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Queue__ChangeEvent.CnP_PaaS__CnP_Transaction__c" {
  const CnP_PaaS__CnP_Transaction__c:any;
  export default CnP_PaaS__CnP_Transaction__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Queue__ChangeEvent.CnP_PaaS__Contact__c" {
  const CnP_PaaS__Contact__c:any;
  export default CnP_PaaS__Contact__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Queue__ChangeEvent.CnP_PaaS__Last_Attempt__c" {
  const CnP_PaaS__Last_Attempt__c:any;
  export default CnP_PaaS__Last_Attempt__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Queue__ChangeEvent.CnP_PaaS__Sent_Count__c" {
  const CnP_PaaS__Sent_Count__c:string;
  export default CnP_PaaS__Sent_Count__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Auto_Responder_Queue__ChangeEvent.CnP_PaaS__Sent_Date__c" {
  const CnP_PaaS__Sent_Date__c:any;
  export default CnP_PaaS__Sent_Date__c;
}
