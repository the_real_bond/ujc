declare module "@salesforce/schema/CnP_PaaS__CnP_Widget__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Widget__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Widget__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Widget__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Widget__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Widget__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Widget__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Widget__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Widget__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Widget__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Widget__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Widget__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Widget__c.CnP_PaaS__Widget_Code__c" {
  const CnP_PaaS__Widget_Code__c:string;
  export default CnP_PaaS__Widget_Code__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Widget__c.CnP_PaaS_EVT__C_P_Event__r" {
  const CnP_PaaS_EVT__C_P_Event__r:any;
  export default CnP_PaaS_EVT__C_P_Event__r;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Widget__c.CnP_PaaS_EVT__C_P_Event__c" {
  const CnP_PaaS_EVT__C_P_Event__c:any;
  export default CnP_PaaS_EVT__C_P_Event__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Widget__c.CnP_PaaS_EVT__Unique_FieldName__c" {
  const CnP_PaaS_EVT__Unique_FieldName__c:string;
  export default CnP_PaaS_EVT__Unique_FieldName__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Widget__c.CnP_PaaS_EVT__Widget_type__c" {
  const CnP_PaaS_EVT__Widget_type__c:string;
  export default CnP_PaaS_EVT__Widget_type__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Widget__c.CnP_PaaS_EVT__description__c" {
  const CnP_PaaS_EVT__description__c:string;
  export default CnP_PaaS_EVT__description__c;
}
