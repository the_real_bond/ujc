declare module "@salesforce/schema/Candidate_Vote__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Candidate_Vote__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/Candidate_Vote__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/Candidate_Vote__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Candidate_Vote__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Candidate_Vote__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Candidate_Vote__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Candidate_Vote__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Candidate_Vote__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Candidate_Vote__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Candidate_Vote__ChangeEvent.Community_Campaign_Vote__c" {
  const Community_Campaign_Vote__c:any;
  export default Community_Campaign_Vote__c;
}
declare module "@salesforce/schema/Candidate_Vote__ChangeEvent.Community_Campaign_Candidate__c" {
  const Community_Campaign_Candidate__c:any;
  export default Community_Campaign_Candidate__c;
}
declare module "@salesforce/schema/Candidate_Vote__ChangeEvent.Candidate_Name__c" {
  const Candidate_Name__c:string;
  export default Candidate_Name__c;
}
