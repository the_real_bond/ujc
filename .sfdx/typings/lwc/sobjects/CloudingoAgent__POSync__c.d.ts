declare module "@salesforce/schema/CloudingoAgent__POSync__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CloudingoAgent__POSync__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CloudingoAgent__POSync__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CloudingoAgent__POSync__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CloudingoAgent__POSync__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CloudingoAgent__POSync__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CloudingoAgent__POSync__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CloudingoAgent__POSync__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CloudingoAgent__POSync__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CloudingoAgent__POSync__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CloudingoAgent__POSync__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CloudingoAgent__POSync__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CloudingoAgent__POSync__c.CloudingoAgent__BID__c" {
  const CloudingoAgent__BID__c:string;
  export default CloudingoAgent__BID__c;
}
declare module "@salesforce/schema/CloudingoAgent__POSync__c.CloudingoAgent__FR__c" {
  const CloudingoAgent__FR__c:boolean;
  export default CloudingoAgent__FR__c;
}
declare module "@salesforce/schema/CloudingoAgent__POSync__c.CloudingoAgent__LastSyncDate__c" {
  const CloudingoAgent__LastSyncDate__c:any;
  export default CloudingoAgent__LastSyncDate__c;
}
declare module "@salesforce/schema/CloudingoAgent__POSync__c.CloudingoAgent__PO__r" {
  const CloudingoAgent__PO__r:any;
  export default CloudingoAgent__PO__r;
}
declare module "@salesforce/schema/CloudingoAgent__POSync__c.CloudingoAgent__PO__c" {
  const CloudingoAgent__PO__c:any;
  export default CloudingoAgent__PO__c;
}
declare module "@salesforce/schema/CloudingoAgent__POSync__c.CloudingoAgent__RK__c" {
  const CloudingoAgent__RK__c:string;
  export default CloudingoAgent__RK__c;
}
declare module "@salesforce/schema/CloudingoAgent__POSync__c.CloudingoAgent__ST__c" {
  const CloudingoAgent__ST__c:string;
  export default CloudingoAgent__ST__c;
}
