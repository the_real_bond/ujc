declare module "@salesforce/schema/Community_Campaign__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Community_Campaign__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Community_Campaign__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Community_Campaign__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Community_Campaign__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Community_Campaign__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Community_Campaign__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Community_Campaign__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Community_Campaign__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Community_Campaign__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Community_Campaign__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Community_Campaign__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Community_Campaign__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/Community_Campaign__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Community_Campaign__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Community_Campaign__c.Current_Active_Campaign__c" {
  const Current_Active_Campaign__c:boolean;
  export default Current_Active_Campaign__c;
}
declare module "@salesforce/schema/Community_Campaign__c.End_Date__c" {
  const End_Date__c:any;
  export default End_Date__c;
}
declare module "@salesforce/schema/Community_Campaign__c.Min_Number_of_Votes__c" {
  const Min_Number_of_Votes__c:number;
  export default Min_Number_of_Votes__c;
}
declare module "@salesforce/schema/Community_Campaign__c.No_of_Votes_per_Candidate__c" {
  const No_of_Votes_per_Candidate__c:number;
  export default No_of_Votes_per_Candidate__c;
}
declare module "@salesforce/schema/Community_Campaign__c.Start_Date__c" {
  const Start_Date__c:any;
  export default Start_Date__c;
}
