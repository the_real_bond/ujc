declare module "@salesforce/schema/Tax_Certificate_Settings__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Tax_Certificate_Settings__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/Tax_Certificate_Settings__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/Tax_Certificate_Settings__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Tax_Certificate_Settings__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Tax_Certificate_Settings__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Tax_Certificate_Settings__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Tax_Certificate_Settings__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Tax_Certificate_Settings__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Tax_Certificate_Settings__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Tax_Certificate_Settings__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Tax_Certificate_Settings__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Tax_Certificate_Settings__ChangeEvent.Financial_Year_End_1__c" {
  const Financial_Year_End_1__c:string;
  export default Financial_Year_End_1__c;
}
declare module "@salesforce/schema/Tax_Certificate_Settings__ChangeEvent.Financial_Year_Start_1__c" {
  const Financial_Year_Start_1__c:string;
  export default Financial_Year_Start_1__c;
}
declare module "@salesforce/schema/Tax_Certificate_Settings__ChangeEvent.Year_End_Month_No_1__c" {
  const Year_End_Month_No_1__c:number;
  export default Year_End_Month_No_1__c;
}
declare module "@salesforce/schema/Tax_Certificate_Settings__ChangeEvent.Year_Start_Month_No_1__c" {
  const Year_Start_Month_No_1__c:number;
  export default Year_Start_Month_No_1__c;
}
