declare module "@salesforce/schema/CnP_PaaS_EVT__Attendee_custom_information__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Attendee_custom_information__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Attendee_custom_information__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Attendee_custom_information__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Attendee_custom_information__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Attendee_custom_information__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Attendee_custom_information__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Attendee_custom_information__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Attendee_custom_information__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Attendee_custom_information__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Attendee_custom_information__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Attendee_custom_information__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Attendee_custom_information__ChangeEvent.CnP_PaaS_EVT__Answer__c" {
  const CnP_PaaS_EVT__Answer__c:string;
  export default CnP_PaaS_EVT__Answer__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Attendee_custom_information__ChangeEvent.CnP_PaaS_EVT__C_P_Custom_Field__c" {
  const CnP_PaaS_EVT__C_P_Custom_Field__c:any;
  export default CnP_PaaS_EVT__C_P_Custom_Field__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Attendee_custom_information__ChangeEvent.CnP_PaaS_EVT__C_P_Event_Registrant__c" {
  const CnP_PaaS_EVT__C_P_Event_Registrant__c:any;
  export default CnP_PaaS_EVT__C_P_Event_Registrant__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Attendee_custom_information__ChangeEvent.CnP_PaaS_EVT__Contact__c" {
  const CnP_PaaS_EVT__Contact__c:any;
  export default CnP_PaaS_EVT__Contact__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Attendee_custom_information__ChangeEvent.CnP_PaaS_EVT__Event_name__c" {
  const CnP_PaaS_EVT__Event_name__c:any;
  export default CnP_PaaS_EVT__Event_name__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Attendee_custom_information__ChangeEvent.CnP_PaaS_EVT__Field_name__c" {
  const CnP_PaaS_EVT__Field_name__c:string;
  export default CnP_PaaS_EVT__Field_name__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Attendee_custom_information__ChangeEvent.CnP_PaaS_EVT__Field_type__c" {
  const CnP_PaaS_EVT__Field_type__c:string;
  export default CnP_PaaS_EVT__Field_type__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Attendee_custom_information__ChangeEvent.CnP_PaaS_EVT__Field_value__c" {
  const CnP_PaaS_EVT__Field_value__c:string;
  export default CnP_PaaS_EVT__Field_value__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Attendee_custom_information__ChangeEvent.CnP_PaaS_EVT__Registration_level__c" {
  const CnP_PaaS_EVT__Registration_level__c:any;
  export default CnP_PaaS_EVT__Registration_level__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Attendee_custom_information__ChangeEvent.CnP_PaaS_EVT__Section_name__c" {
  const CnP_PaaS_EVT__Section_name__c:string;
  export default CnP_PaaS_EVT__Section_name__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Attendee_custom_information__ChangeEvent.CnP_PaaS_EVT__attendee_session_Id__c" {
  const CnP_PaaS_EVT__attendee_session_Id__c:any;
  export default CnP_PaaS_EVT__attendee_session_Id__c;
}
