declare module "@salesforce/schema/CnP_PaaS__CnPRecurringTransaction__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurringTransaction__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurringTransaction__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurringTransaction__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurringTransaction__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurringTransaction__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurringTransaction__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurringTransaction__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurringTransaction__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurringTransaction__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurringTransaction__ChangeEvent.CnP_PaaS__Recurring_Transaction__c" {
  const CnP_PaaS__Recurring_Transaction__c:any;
  export default CnP_PaaS__Recurring_Transaction__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurringTransaction__ChangeEvent.CnP_PaaS__TransactionId__c" {
  const CnP_PaaS__TransactionId__c:any;
  export default CnP_PaaS__TransactionId__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurringTransaction__ChangeEvent.CnP_PaaS__InstallmentNumber__c" {
  const CnP_PaaS__InstallmentNumber__c:number;
  export default CnP_PaaS__InstallmentNumber__c;
}
