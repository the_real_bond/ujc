declare module "@salesforce/schema/CnP_PaaS__Invoice_Schedule__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Schedule__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Schedule__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Schedule__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Schedule__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Schedule__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Schedule__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Schedule__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Schedule__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Schedule__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Schedule__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Schedule__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Schedule__ChangeEvent.CnP_PaaS__Invoice_Payment_Policy__c" {
  const CnP_PaaS__Invoice_Payment_Policy__c:any;
  export default CnP_PaaS__Invoice_Payment_Policy__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Schedule__ChangeEvent.CnP_PaaS__Invoice_Policy__c" {
  const CnP_PaaS__Invoice_Policy__c:any;
  export default CnP_PaaS__Invoice_Policy__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Schedule__ChangeEvent.CnP_PaaS__Invoice__c" {
  const CnP_PaaS__Invoice__c:any;
  export default CnP_PaaS__Invoice__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Schedule__ChangeEvent.CnP_PaaS__Reminder_Date__c" {
  const CnP_PaaS__Reminder_Date__c:any;
  export default CnP_PaaS__Reminder_Date__c;
}
