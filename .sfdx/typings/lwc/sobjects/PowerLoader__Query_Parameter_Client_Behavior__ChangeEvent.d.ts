declare module "@salesforce/schema/PowerLoader__Query_Parameter_Client_Behavior__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter_Client_Behavior__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter_Client_Behavior__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter_Client_Behavior__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter_Client_Behavior__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter_Client_Behavior__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter_Client_Behavior__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter_Client_Behavior__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter_Client_Behavior__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter_Client_Behavior__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter_Client_Behavior__ChangeEvent.PowerLoader__Query_Parameter__c" {
  const PowerLoader__Query_Parameter__c:any;
  export default PowerLoader__Query_Parameter__c;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter_Client_Behavior__ChangeEvent.PowerLoader__Action_Text__c" {
  const PowerLoader__Action_Text__c:string;
  export default PowerLoader__Action_Text__c;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter_Client_Behavior__ChangeEvent.PowerLoader__Action__c" {
  const PowerLoader__Action__c:string;
  export default PowerLoader__Action__c;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter_Client_Behavior__ChangeEvent.PowerLoader__Apply_to_Tag__c" {
  const PowerLoader__Apply_to_Tag__c:string;
  export default PowerLoader__Apply_to_Tag__c;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter_Client_Behavior__ChangeEvent.PowerLoader__Criteria_Value__c" {
  const PowerLoader__Criteria_Value__c:string;
  export default PowerLoader__Criteria_Value__c;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter_Client_Behavior__ChangeEvent.PowerLoader__Criteria__c" {
  const PowerLoader__Criteria__c:string;
  export default PowerLoader__Criteria__c;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter_Client_Behavior__ChangeEvent.PowerLoader__Negate_Criteria__c" {
  const PowerLoader__Negate_Criteria__c:boolean;
  export default PowerLoader__Negate_Criteria__c;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter_Client_Behavior__ChangeEvent.PowerLoader__Set_Value_Type__c" {
  const PowerLoader__Set_Value_Type__c:string;
  export default PowerLoader__Set_Value_Type__c;
}
