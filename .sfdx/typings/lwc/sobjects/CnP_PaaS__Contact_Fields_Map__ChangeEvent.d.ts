declare module "@salesforce/schema/CnP_PaaS__Contact_Fields_Map__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Fields_Map__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Fields_Map__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Fields_Map__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Fields_Map__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Fields_Map__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Fields_Map__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Fields_Map__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Fields_Map__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Fields_Map__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Fields_Map__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Fields_Map__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Fields_Map__ChangeEvent.CnP_PaaS__Account_Field_Type__c" {
  const CnP_PaaS__Account_Field_Type__c:string;
  export default CnP_PaaS__Account_Field_Type__c;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Fields_Map__ChangeEvent.CnP_PaaS__Field_Map_Values__c" {
  const CnP_PaaS__Field_Map_Values__c:string;
  export default CnP_PaaS__Field_Map_Values__c;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Fields_Map__ChangeEvent.CnP_PaaS__Field_Type__c" {
  const CnP_PaaS__Field_Type__c:string;
  export default CnP_PaaS__Field_Type__c;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Fields_Map__ChangeEvent.CnP_PaaS__Process_Type__c" {
  const CnP_PaaS__Process_Type__c:string;
  export default CnP_PaaS__Process_Type__c;
}
