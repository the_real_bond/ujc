declare module "@salesforce/schema/APXTConga4__Conga_Email_Template__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Template__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Template__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Template__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Template__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Template__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Template__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Template__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Template__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Template__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Template__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Template__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Template__ChangeEvent.APXTConga4__Description__c" {
  const APXTConga4__Description__c:string;
  export default APXTConga4__Description__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Template__ChangeEvent.APXTConga4__HTMLBody__c" {
  const APXTConga4__HTMLBody__c:string;
  export default APXTConga4__HTMLBody__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Template__ChangeEvent.APXTConga4__Is_Body_Attachment__c" {
  const APXTConga4__Is_Body_Attachment__c:boolean;
  export default APXTConga4__Is_Body_Attachment__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Template__ChangeEvent.APXTConga4__Name__c" {
  const APXTConga4__Name__c:string;
  export default APXTConga4__Name__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Template__ChangeEvent.APXTConga4__Subject__c" {
  const APXTConga4__Subject__c:string;
  export default APXTConga4__Subject__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Template__ChangeEvent.APXTConga4__Template_Group__c" {
  const APXTConga4__Template_Group__c:string;
  export default APXTConga4__Template_Group__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Template__ChangeEvent.APXTConga4__TextBody__c" {
  const APXTConga4__TextBody__c:string;
  export default APXTConga4__TextBody__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Template__ChangeEvent.APXTConga4__Key__c" {
  const APXTConga4__Key__c:string;
  export default APXTConga4__Key__c;
}
