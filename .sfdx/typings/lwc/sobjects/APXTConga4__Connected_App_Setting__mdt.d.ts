declare module "@salesforce/schema/APXTConga4__Connected_App_Setting__mdt.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/APXTConga4__Connected_App_Setting__mdt.DeveloperName" {
  const DeveloperName:string;
  export default DeveloperName;
}
declare module "@salesforce/schema/APXTConga4__Connected_App_Setting__mdt.MasterLabel" {
  const MasterLabel:string;
  export default MasterLabel;
}
declare module "@salesforce/schema/APXTConga4__Connected_App_Setting__mdt.Language" {
  const Language:string;
  export default Language;
}
declare module "@salesforce/schema/APXTConga4__Connected_App_Setting__mdt.NamespacePrefix" {
  const NamespacePrefix:string;
  export default NamespacePrefix;
}
declare module "@salesforce/schema/APXTConga4__Connected_App_Setting__mdt.Label" {
  const Label:string;
  export default Label;
}
declare module "@salesforce/schema/APXTConga4__Connected_App_Setting__mdt.QualifiedApiName" {
  const QualifiedApiName:string;
  export default QualifiedApiName;
}
declare module "@salesforce/schema/APXTConga4__Connected_App_Setting__mdt.APXTConga4__Connected_App_Name_SF1__c" {
  const APXTConga4__Connected_App_Name_SF1__c:string;
  export default APXTConga4__Connected_App_Name_SF1__c;
}
declare module "@salesforce/schema/APXTConga4__Connected_App_Setting__mdt.APXTConga4__Connected_App_Name__c" {
  const APXTConga4__Connected_App_Name__c:string;
  export default APXTConga4__Connected_App_Name__c;
}
declare module "@salesforce/schema/APXTConga4__Connected_App_Setting__mdt.APXTConga4__Hostname__c" {
  const APXTConga4__Hostname__c:string;
  export default APXTConga4__Hostname__c;
}
