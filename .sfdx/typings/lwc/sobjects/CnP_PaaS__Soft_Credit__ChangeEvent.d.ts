declare module "@salesforce/schema/CnP_PaaS__Soft_Credit__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Soft_Credit__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__Soft_Credit__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__Soft_Credit__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__Soft_Credit__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__Soft_Credit__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Soft_Credit__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Soft_Credit__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Soft_Credit__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Soft_Credit__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Soft_Credit__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Soft_Credit__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Soft_Credit__ChangeEvent.CnP_PaaS__C_P_Transaction__c" {
  const CnP_PaaS__C_P_Transaction__c:any;
  export default CnP_PaaS__C_P_Transaction__c;
}
declare module "@salesforce/schema/CnP_PaaS__Soft_Credit__ChangeEvent.CnP_PaaS__Contact__c" {
  const CnP_PaaS__Contact__c:any;
  export default CnP_PaaS__Contact__c;
}
declare module "@salesforce/schema/CnP_PaaS__Soft_Credit__ChangeEvent.CnP_PaaS__Object_record__c" {
  const CnP_PaaS__Object_record__c:string;
  export default CnP_PaaS__Object_record__c;
}
declare module "@salesforce/schema/CnP_PaaS__Soft_Credit__ChangeEvent.CnP_PaaS__Opportunity__c" {
  const CnP_PaaS__Opportunity__c:any;
  export default CnP_PaaS__Opportunity__c;
}
declare module "@salesforce/schema/CnP_PaaS__Soft_Credit__ChangeEvent.CnP_PaaS__Reciprocal_Relationship__c" {
  const CnP_PaaS__Reciprocal_Relationship__c:string;
  export default CnP_PaaS__Reciprocal_Relationship__c;
}
declare module "@salesforce/schema/CnP_PaaS__Soft_Credit__ChangeEvent.CnP_PaaS__Related_Account__c" {
  const CnP_PaaS__Related_Account__c:any;
  export default CnP_PaaS__Related_Account__c;
}
declare module "@salesforce/schema/CnP_PaaS__Soft_Credit__ChangeEvent.CnP_PaaS__Related_Contact__c" {
  const CnP_PaaS__Related_Contact__c:any;
  export default CnP_PaaS__Related_Contact__c;
}
declare module "@salesforce/schema/CnP_PaaS__Soft_Credit__ChangeEvent.CnP_PaaS__Relationship__c" {
  const CnP_PaaS__Relationship__c:string;
  export default CnP_PaaS__Relationship__c;
}
declare module "@salesforce/schema/CnP_PaaS__Soft_Credit__ChangeEvent.CnP_PaaS__SKU_Filter__c" {
  const CnP_PaaS__SKU_Filter__c:string;
  export default CnP_PaaS__SKU_Filter__c;
}
declare module "@salesforce/schema/CnP_PaaS__Soft_Credit__ChangeEvent.CnP_PaaS__SKU__c" {
  const CnP_PaaS__SKU__c:string;
  export default CnP_PaaS__SKU__c;
}
declare module "@salesforce/schema/CnP_PaaS__Soft_Credit__ChangeEvent.CnP_PaaS__Status__c" {
  const CnP_PaaS__Status__c:string;
  export default CnP_PaaS__Status__c;
}
declare module "@salesforce/schema/CnP_PaaS__Soft_Credit__ChangeEvent.CnP_PaaS__Total_Soft_Credit__c" {
  const CnP_PaaS__Total_Soft_Credit__c:number;
  export default CnP_PaaS__Total_Soft_Credit__c;
}
