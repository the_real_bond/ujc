declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__ChangeEvent.CnP_PaaS__Discount__c" {
  const CnP_PaaS__Discount__c:number;
  export default CnP_PaaS__Discount__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__ChangeEvent.CnP_PaaS__Item_Campaign__c" {
  const CnP_PaaS__Item_Campaign__c:string;
  export default CnP_PaaS__Item_Campaign__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__ChangeEvent.CnP_PaaS__Item_Price__c" {
  const CnP_PaaS__Item_Price__c:number;
  export default CnP_PaaS__Item_Price__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__ChangeEvent.CnP_PaaS__Quantity__c" {
  const CnP_PaaS__Quantity__c:number;
  export default CnP_PaaS__Quantity__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__ChangeEvent.CnP_PaaS__SKU__c" {
  const CnP_PaaS__SKU__c:string;
  export default CnP_PaaS__SKU__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__ChangeEvent.CnP_PaaS__Tax__c" {
  const CnP_PaaS__Tax__c:number;
  export default CnP_PaaS__Tax__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__ChangeEvent.CnP_PaaS__Temp_Invoice__c" {
  const CnP_PaaS__Temp_Invoice__c:any;
  export default CnP_PaaS__Temp_Invoice__c;
}
declare module "@salesforce/schema/CnP_PaaS__Invoice_Items__ChangeEvent.CnP_PaaS__tax_deductible__c" {
  const CnP_PaaS__tax_deductible__c:number;
  export default CnP_PaaS__tax_deductible__c;
}
