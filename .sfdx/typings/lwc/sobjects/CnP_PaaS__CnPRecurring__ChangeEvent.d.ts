declare module "@salesforce/schema/CnP_PaaS__CnPRecurring__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurring__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurring__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurring__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurring__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurring__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurring__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurring__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurring__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurring__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurring__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurring__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurring__ChangeEvent.CnP_PaaS__Account__c" {
  const CnP_PaaS__Account__c:any;
  export default CnP_PaaS__Account__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurring__ChangeEvent.CnP_PaaS__Canceled__c" {
  const CnP_PaaS__Canceled__c:boolean;
  export default CnP_PaaS__Canceled__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurring__ChangeEvent.CnP_PaaS__Cancellation_Date__c" {
  const CnP_PaaS__Cancellation_Date__c:any;
  export default CnP_PaaS__Cancellation_Date__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurring__ChangeEvent.CnP_PaaS__Contact__c" {
  const CnP_PaaS__Contact__c:any;
  export default CnP_PaaS__Contact__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurring__ChangeEvent.CnP_PaaS__Date_Established__c" {
  const CnP_PaaS__Date_Established__c:any;
  export default CnP_PaaS__Date_Established__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurring__ChangeEvent.CnP_PaaS__FirstChargeDate__c" {
  const CnP_PaaS__FirstChargeDate__c:any;
  export default CnP_PaaS__FirstChargeDate__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurring__ChangeEvent.CnP_PaaS__Full_Name__c" {
  const CnP_PaaS__Full_Name__c:string;
  export default CnP_PaaS__Full_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurring__ChangeEvent.CnP_PaaS__Installment_Amount__c" {
  const CnP_PaaS__Installment_Amount__c:number;
  export default CnP_PaaS__Installment_Amount__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurring__ChangeEvent.CnP_PaaS__InstallmentsMade__c" {
  const CnP_PaaS__InstallmentsMade__c:number;
  export default CnP_PaaS__InstallmentsMade__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurring__ChangeEvent.CnP_PaaS__Installments__c" {
  const CnP_PaaS__Installments__c:number;
  export default CnP_PaaS__Installments__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurring__ChangeEvent.CnP_PaaS__NextInstallment_Date__c" {
  const CnP_PaaS__NextInstallment_Date__c:any;
  export default CnP_PaaS__NextInstallment_Date__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurring__ChangeEvent.CnP_PaaS__OrderNumber__c" {
  const CnP_PaaS__OrderNumber__c:string;
  export default CnP_PaaS__OrderNumber__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurring__ChangeEvent.CnP_PaaS__Periodicity__c" {
  const CnP_PaaS__Periodicity__c:string;
  export default CnP_PaaS__Periodicity__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurring__ChangeEvent.CnP_PaaS__RecurringMethod__c" {
  const CnP_PaaS__RecurringMethod__c:string;
  export default CnP_PaaS__RecurringMethod__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurring__ChangeEvent.CnP_PaaS__Recurring_Transaction_Id__c" {
  const CnP_PaaS__Recurring_Transaction_Id__c:string;
  export default CnP_PaaS__Recurring_Transaction_Id__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurring__ChangeEvent.CnP_PaaS__Total_Made__c" {
  const CnP_PaaS__Total_Made__c:number;
  export default CnP_PaaS__Total_Made__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurring__ChangeEvent.CnP_PaaS__Total__c" {
  const CnP_PaaS__Total__c:number;
  export default CnP_PaaS__Total__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurring__ChangeEvent.CnP_PaaS__Transaction_Result__c" {
  const CnP_PaaS__Transaction_Result__c:string;
  export default CnP_PaaS__Transaction_Result__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPRecurring__ChangeEvent.CnP_PaaS__Upcoming_Receipts__c" {
  const CnP_PaaS__Upcoming_Receipts__c:number;
  export default CnP_PaaS__Upcoming_Receipts__c;
}
