declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__ChangeEvent.CnP_PaaS__Donor_Personal_Donation_Amount_Rank_Max__c" {
  const CnP_PaaS__Donor_Personal_Donation_Amount_Rank_Max__c:number;
  export default CnP_PaaS__Donor_Personal_Donation_Amount_Rank_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__ChangeEvent.CnP_PaaS__Donor_Personal_Donation_Count_Rank_Max__c" {
  const CnP_PaaS__Donor_Personal_Donation_Count_Rank_Max__c:number;
  export default CnP_PaaS__Donor_Personal_Donation_Count_Rank_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__ChangeEvent.CnP_PaaS__Donor_Raised_Donation_Amount_Rank_Max__c" {
  const CnP_PaaS__Donor_Raised_Donation_Amount_Rank_Max__c:number;
  export default CnP_PaaS__Donor_Raised_Donation_Amount_Rank_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__ChangeEvent.CnP_PaaS__Donor_Raised_Donation_Count_Rank_Max__c" {
  const CnP_PaaS__Donor_Raised_Donation_Count_Rank_Max__c:number;
  export default CnP_PaaS__Donor_Raised_Donation_Count_Rank_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__ChangeEvent.CnP_PaaS__Donors_Rank_Factor_Max__c" {
  const CnP_PaaS__Donors_Rank_Factor_Max__c:number;
  export default CnP_PaaS__Donors_Rank_Factor_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__ChangeEvent.CnP_PaaS__Job_Status__c" {
  const CnP_PaaS__Job_Status__c:string;
  export default CnP_PaaS__Job_Status__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__ChangeEvent.CnP_PaaS__My_Donor_Personal_Donation_Amount_Max__c" {
  const CnP_PaaS__My_Donor_Personal_Donation_Amount_Max__c:number;
  export default CnP_PaaS__My_Donor_Personal_Donation_Amount_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__ChangeEvent.CnP_PaaS__My_Donor_Personal_Donation_Count_Max__c" {
  const CnP_PaaS__My_Donor_Personal_Donation_Count_Max__c:number;
  export default CnP_PaaS__My_Donor_Personal_Donation_Count_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__ChangeEvent.CnP_PaaS__My_Donor_Raised_Donation_Amount_Max__c" {
  const CnP_PaaS__My_Donor_Raised_Donation_Amount_Max__c:number;
  export default CnP_PaaS__My_Donor_Raised_Donation_Amount_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__ChangeEvent.CnP_PaaS__My_Donor_Raised_Donation_Count_Max__c" {
  const CnP_PaaS__My_Donor_Raised_Donation_Count_Max__c:number;
  export default CnP_PaaS__My_Donor_Raised_Donation_Count_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__ChangeEvent.CnP_PaaS__My_Rank_Factor_Max__c" {
  const CnP_PaaS__My_Rank_Factor_Max__c:number;
  export default CnP_PaaS__My_Rank_Factor_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__ChangeEvent.CnP_PaaS__Personal_Donation_Amount_Max__c" {
  const CnP_PaaS__Personal_Donation_Amount_Max__c:number;
  export default CnP_PaaS__Personal_Donation_Amount_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__ChangeEvent.CnP_PaaS__Personal_Donation_Amount_Rank_Max__c" {
  const CnP_PaaS__Personal_Donation_Amount_Rank_Max__c:number;
  export default CnP_PaaS__Personal_Donation_Amount_Rank_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__ChangeEvent.CnP_PaaS__Personal_Donation_Count_Max__c" {
  const CnP_PaaS__Personal_Donation_Count_Max__c:number;
  export default CnP_PaaS__Personal_Donation_Count_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__ChangeEvent.CnP_PaaS__Personal_Donation_Count_Rank_Max__c" {
  const CnP_PaaS__Personal_Donation_Count_Rank_Max__c:number;
  export default CnP_PaaS__Personal_Donation_Count_Rank_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__ChangeEvent.CnP_PaaS__Raised_Donation_Amount_Max__c" {
  const CnP_PaaS__Raised_Donation_Amount_Max__c:number;
  export default CnP_PaaS__Raised_Donation_Amount_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__ChangeEvent.CnP_PaaS__Raised_Donation_Amount_Rank_Max__c" {
  const CnP_PaaS__Raised_Donation_Amount_Rank_Max__c:number;
  export default CnP_PaaS__Raised_Donation_Amount_Rank_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__ChangeEvent.CnP_PaaS__Raised_Donation_Count_Max__c" {
  const CnP_PaaS__Raised_Donation_Count_Max__c:number;
  export default CnP_PaaS__Raised_Donation_Count_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__ChangeEvent.CnP_PaaS__Raised_Donation_Count_Rank_Max__c" {
  const CnP_PaaS__Raised_Donation_Count_Rank_Max__c:number;
  export default CnP_PaaS__Raised_Donation_Count_Rank_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__ChangeEvent.CnP_PaaS__Total_Intrinsic_Value_Max__c" {
  const CnP_PaaS__Total_Intrinsic_Value_Max__c:number;
  export default CnP_PaaS__Total_Intrinsic_Value_Max__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__ChangeEvent.CnP_PaaS__Total_Records__c" {
  const CnP_PaaS__Total_Records__c:number;
  export default CnP_PaaS__Total_Records__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__ChangeEvent.CnP_PaaS__Total__c" {
  const CnP_PaaS__Total__c:number;
  export default CnP_PaaS__Total__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics_Max__ChangeEvent.CnP_PaaS__Total_extrinsic_value_Max__c" {
  const CnP_PaaS__Total_extrinsic_value_Max__c:number;
  export default CnP_PaaS__Total_extrinsic_value_Max__c;
}
