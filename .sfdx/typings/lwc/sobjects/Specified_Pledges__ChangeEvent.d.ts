declare module "@salesforce/schema/Specified_Pledges__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Specified_Pledges__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/Specified_Pledges__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/Specified_Pledges__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Specified_Pledges__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Specified_Pledges__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Specified_Pledges__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Specified_Pledges__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Specified_Pledges__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Specified_Pledges__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Specified_Pledges__ChangeEvent.SP_Organisation__c" {
  const SP_Organisation__c:string;
  export default SP_Organisation__c;
}
declare module "@salesforce/schema/Specified_Pledges__ChangeEvent.SP_Amount__c" {
  const SP_Amount__c:number;
  export default SP_Amount__c;
}
declare module "@salesforce/schema/Specified_Pledges__ChangeEvent.SP_Donor__c" {
  const SP_Donor__c:string;
  export default SP_Donor__c;
}
declare module "@salesforce/schema/Specified_Pledges__ChangeEvent.ParentPledge__c" {
  const ParentPledge__c:any;
  export default ParentPledge__c;
}
declare module "@salesforce/schema/Specified_Pledges__ChangeEvent.SP_Date__c" {
  const SP_Date__c:any;
  export default SP_Date__c;
}
declare module "@salesforce/schema/Specified_Pledges__ChangeEvent.SP_Division__c" {
  const SP_Division__c:string;
  export default SP_Division__c;
}
declare module "@salesforce/schema/Specified_Pledges__ChangeEvent.SP_Camp_Year__c" {
  const SP_Camp_Year__c:string;
  export default SP_Camp_Year__c;
}
declare module "@salesforce/schema/Specified_Pledges__ChangeEvent.Date_Paid_Across__c" {
  const Date_Paid_Across__c:any;
  export default Date_Paid_Across__c;
}
declare module "@salesforce/schema/Specified_Pledges__ChangeEvent.SP_Direct__c" {
  const SP_Direct__c:boolean;
  export default SP_Direct__c;
}
declare module "@salesforce/schema/Specified_Pledges__ChangeEvent.Payment_Source__c" {
  const Payment_Source__c:string;
  export default Payment_Source__c;
}
declare module "@salesforce/schema/Specified_Pledges__ChangeEvent.Total_Division_Outstanding__c" {
  const Total_Division_Outstanding__c:number;
  export default Total_Division_Outstanding__c;
}
declare module "@salesforce/schema/Specified_Pledges__ChangeEvent.Total_Division_Pledge__c" {
  const Total_Division_Pledge__c:number;
  export default Total_Division_Pledge__c;
}
declare module "@salesforce/schema/Specified_Pledges__ChangeEvent.SP_Transfered__c" {
  const SP_Transfered__c:boolean;
  export default SP_Transfered__c;
}
declare module "@salesforce/schema/Specified_Pledges__ChangeEvent.SP_Campaign__c" {
  const SP_Campaign__c:string;
  export default SP_Campaign__c;
}
declare module "@salesforce/schema/Specified_Pledges__ChangeEvent.Donor_Email__c" {
  const Donor_Email__c:string;
  export default Donor_Email__c;
}
declare module "@salesforce/schema/Specified_Pledges__ChangeEvent.SP_Beneficiary__c" {
  const SP_Beneficiary__c:any;
  export default SP_Beneficiary__c;
}
declare module "@salesforce/schema/Specified_Pledges__ChangeEvent.SP_Beneficiary_Campaign__c" {
  const SP_Beneficiary_Campaign__c:string;
  export default SP_Beneficiary_Campaign__c;
}
declare module "@salesforce/schema/Specified_Pledges__ChangeEvent.SP_Pledge_Type__c" {
  const SP_Pledge_Type__c:string;
  export default SP_Pledge_Type__c;
}
