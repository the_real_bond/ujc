declare module "@salesforce/schema/CloudingoAgent__CldIn__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__c.CloudingoAgent__P1__c" {
  const CloudingoAgent__P1__c:string;
  export default CloudingoAgent__P1__c;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__c.CloudingoAgent__P2__c" {
  const CloudingoAgent__P2__c:string;
  export default CloudingoAgent__P2__c;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__c.CloudingoAgent__P3__c" {
  const CloudingoAgent__P3__c:string;
  export default CloudingoAgent__P3__c;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__c.CloudingoAgent__P4__c" {
  const CloudingoAgent__P4__c:string;
  export default CloudingoAgent__P4__c;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__c.CloudingoAgent__P5__c" {
  const CloudingoAgent__P5__c:string;
  export default CloudingoAgent__P5__c;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__c.CloudingoAgent__R1__c" {
  const CloudingoAgent__R1__c:string;
  export default CloudingoAgent__R1__c;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__c.CloudingoAgent__T1__c" {
  const CloudingoAgent__T1__c:string;
  export default CloudingoAgent__T1__c;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__c.CloudingoAgent__stat__c" {
  const CloudingoAgent__stat__c:number;
  export default CloudingoAgent__stat__c;
}
declare module "@salesforce/schema/CloudingoAgent__CldIn__c.CloudingoAgent__wsm__c" {
  const CloudingoAgent__wsm__c:number;
  export default CloudingoAgent__wsm__c;
}
