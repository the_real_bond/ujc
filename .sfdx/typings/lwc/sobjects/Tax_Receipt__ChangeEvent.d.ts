declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.AccountDate__c" {
  const AccountDate__c:string;
  export default AccountDate__c;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.AccountEmail__c" {
  const AccountEmail__c:string;
  export default AccountEmail__c;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.Account_Donor_Id__c" {
  const Account_Donor_Id__c:string;
  export default Account_Donor_Id__c;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.Account_Name__c" {
  const Account_Name__c:string;
  export default Account_Name__c;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.Account__c" {
  const Account__c:any;
  export default Account__c;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.Amount_in_Words__c" {
  const Amount_in_Words__c:string;
  export default Amount_in_Words__c;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.Attention__c" {
  const Attention__c:string;
  export default Attention__c;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.Certificate_Number__c" {
  const Certificate_Number__c:string;
  export default Certificate_Number__c;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.CloseDatemonthandYear__c" {
  const CloseDatemonthandYear__c:string;
  export default CloseDatemonthandYear__c;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.Contact__c" {
  const Contact__c:any;
  export default Contact__c;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.Date_closed__c" {
  const Date_closed__c:any;
  export default Date_closed__c;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.Email_Check__c" {
  const Email_Check__c:boolean;
  export default Email_Check__c;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.Financial_Year_End__c" {
  const Financial_Year_End__c:any;
  export default Financial_Year_End__c;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.Financial_Year_Start__c" {
  const Financial_Year_Start__c:any;
  export default Financial_Year_Start__c;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.IsClosed__c" {
  const IsClosed__c:boolean;
  export default IsClosed__c;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.ReceiptNumber__c" {
  const ReceiptNumber__c:string;
  export default ReceiptNumber__c;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.Receipt_Date__c" {
  const Receipt_Date__c:any;
  export default Receipt_Date__c;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.Tax_Certificate_Issued_to__c" {
  const Tax_Certificate_Issued_to__c:string;
  export default Tax_Certificate_Issued_to__c;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.TodayDate__c" {
  const TodayDate__c:string;
  export default TodayDate__c;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.Type__c" {
  const Type__c:string;
  export default Type__c;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.YearEnd__c" {
  const YearEnd__c:string;
  export default YearEnd__c;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.Year_Start_Words__c" {
  const Year_Start_Words__c:string;
  export default Year_Start_Words__c;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.Year_end_Words__c" {
  const Year_end_Words__c:string;
  export default Year_end_Words__c;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.YearendYear__c" {
  const YearendYear__c:string;
  export default YearendYear__c;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.Total_Receipt_Amount__c" {
  const Total_Receipt_Amount__c:number;
  export default Total_Receipt_Amount__c;
}
declare module "@salesforce/schema/Tax_Receipt__ChangeEvent.Donor_Status__c" {
  const Donor_Status__c:string;
  export default Donor_Status__c;
}
