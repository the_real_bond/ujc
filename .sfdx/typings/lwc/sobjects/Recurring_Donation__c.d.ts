declare module "@salesforce/schema/Recurring_Donation__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Recurring_Donation__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Recurring_Donation__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Recurring_Donation__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Recurring_Donation__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Recurring_Donation__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Recurring_Donation__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Recurring_Donation__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Recurring_Donation__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Recurring_Donation__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Recurring_Donation__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/Recurring_Donation__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Recurring_Donation__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Recurring_Donation__c.Donor__r" {
  const Donor__r:any;
  export default Donor__r;
}
declare module "@salesforce/schema/Recurring_Donation__c.Donor__c" {
  const Donor__c:any;
  export default Donor__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Start_Date__c" {
  const Start_Date__c:any;
  export default Start_Date__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Number_of_Installments__c" {
  const Number_of_Installments__c:number;
  export default Number_of_Installments__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Frequency__c" {
  const Frequency__c:string;
  export default Frequency__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.IUA_Installment__c" {
  const IUA_Installment__c:number;
  export default IUA_Installment__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.UCF_Education_Installment__c" {
  const UCF_Education_Installment__c:number;
  export default UCF_Education_Installment__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.UCF_Communal_Installment__c" {
  const UCF_Communal_Installment__c:number;
  export default UCF_Communal_Installment__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Welfare_Installment__c" {
  const Welfare_Installment__c:number;
  export default Welfare_Installment__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.IUA_Total__c" {
  const IUA_Total__c:number;
  export default IUA_Total__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.UCF_Education_Total__c" {
  const UCF_Education_Total__c:number;
  export default UCF_Education_Total__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.UCF_Communal_Total__c" {
  const UCF_Communal_Total__c:number;
  export default UCF_Communal_Total__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Welfare_Total__c" {
  const Welfare_Total__c:number;
  export default Welfare_Total__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Donation_Total__c" {
  const Donation_Total__c:number;
  export default Donation_Total__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Installment_Total__c" {
  const Installment_Total__c:number;
  export default Installment_Total__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Bank_Account__r" {
  const Bank_Account__r:any;
  export default Bank_Account__r;
}
declare module "@salesforce/schema/Recurring_Donation__c.Bank_Account__c" {
  const Bank_Account__c:any;
  export default Bank_Account__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Account_Name__c" {
  const Account_Name__c:string;
  export default Account_Name__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Bank_Name__c" {
  const Bank_Name__c:string;
  export default Bank_Name__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Branch_Code__c" {
  const Branch_Code__c:string;
  export default Branch_Code__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Account_Type__c" {
  const Account_Type__c:string;
  export default Account_Type__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Account_Number__c" {
  const Account_Number__c:string;
  export default Account_Number__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Method_of_Payment__c" {
  const Method_of_Payment__c:string;
  export default Method_of_Payment__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Type_of_Capture__c" {
  const Type_of_Capture__c:string;
  export default Type_of_Capture__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Household__r" {
  const Household__r:any;
  export default Household__r;
}
declare module "@salesforce/schema/Recurring_Donation__c.Household__c" {
  const Household__c:any;
  export default Household__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Campaign_Year__c" {
  const Campaign_Year__c:string;
  export default Campaign_Year__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Campaign__c" {
  const Campaign__c:string;
  export default Campaign__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Cancellation_Reason__c" {
  const Cancellation_Reason__c:string;
  export default Cancellation_Reason__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Communal_Reference_Number__c" {
  const Communal_Reference_Number__c:string;
  export default Communal_Reference_Number__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Installments__c" {
  const Installments__c:string;
  export default Installments__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Old_Pledge_Number__c" {
  const Old_Pledge_Number__c:string;
  export default Old_Pledge_Number__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Pledge_Total__c" {
  const Pledge_Total__c:number;
  export default Pledge_Total__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Recurring_Pledge_Auto_Number__c" {
  const Recurring_Pledge_Auto_Number__c:string;
  export default Recurring_Pledge_Auto_Number__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Pledge_Number__c" {
  const Pledge_Number__c:string;
  export default Pledge_Number__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Status__c" {
  const Status__c:string;
  export default Status__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Continuous__c" {
  const Continuous__c:string;
  export default Continuous__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Date_Signed__c" {
  const Date_Signed__c:any;
  export default Date_Signed__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Statement_Required__c" {
  const Statement_Required__c:string;
  export default Statement_Required__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Last_Receipt_Date__c" {
  const Last_Receipt_Date__c:any;
  export default Last_Receipt_Date__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Last_Rpt__c" {
  const Last_Rpt__c:string;
  export default Last_Rpt__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Year__c" {
  const Year__c:number;
  export default Year__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Create_Pledge_Transactions__c" {
  const Create_Pledge_Transactions__c:boolean;
  export default Create_Pledge_Transactions__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Canvas_Method__c" {
  const Canvas_Method__c:string;
  export default Canvas_Method__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Canvas_1__c" {
  const Canvas_1__c:string;
  export default Canvas_1__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Canvas_2__c" {
  const Canvas_2__c:string;
  export default Canvas_2__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Canvas_3__c" {
  const Canvas_3__c:string;
  export default Canvas_3__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Comments__c" {
  const Comments__c:string;
  export default Comments__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.SFDC_Test__c" {
  const SFDC_Test__c:number;
  export default SFDC_Test__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.SFDC_Test_2__c" {
  const SFDC_Test_2__c:number;
  export default SFDC_Test_2__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.IUA_Outstanding__c" {
  const IUA_Outstanding__c:number;
  export default IUA_Outstanding__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.UCF_Communal_Outstanding__c" {
  const UCF_Communal_Outstanding__c:number;
  export default UCF_Communal_Outstanding__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.UCF_Education_Outstanding__c" {
  const UCF_Education_Outstanding__c:number;
  export default UCF_Education_Outstanding__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Total_Actual_Amount__c" {
  const Total_Actual_Amount__c:number;
  export default Total_Actual_Amount__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Total_Outstanding__c" {
  const Total_Outstanding__c:number;
  export default Total_Outstanding__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Welfare_Outstanding__c" {
  const Welfare_Outstanding__c:number;
  export default Welfare_Outstanding__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Canvas_Info__c" {
  const Canvas_Info__c:string;
  export default Canvas_Info__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Campaign_Info__c" {
  const Campaign_Info__c:string;
  export default Campaign_Info__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.IUA_Specific_Program__c" {
  const IUA_Specific_Program__c:boolean;
  export default IUA_Specific_Program__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.UCFE_Specific_Program__c" {
  const UCFE_Specific_Program__c:boolean;
  export default UCFE_Specific_Program__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.UCFC_Specific_Program__c" {
  const UCFC_Specific_Program__c:boolean;
  export default UCFC_Specific_Program__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Welfare_Specific_Program__c" {
  const Welfare_Specific_Program__c:boolean;
  export default Welfare_Specific_Program__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Approved__c" {
  const Approved__c:boolean;
  export default Approved__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Date_Approved__c" {
  const Date_Approved__c:any;
  export default Date_Approved__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.IUA_Actual__c" {
  const IUA_Actual__c:number;
  export default IUA_Actual__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.UCF_Communal_Actual__c" {
  const UCF_Communal_Actual__c:number;
  export default UCF_Communal_Actual__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.UCF_Education_Actual__c" {
  const UCF_Education_Actual__c:number;
  export default UCF_Education_Actual__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Welfare_Actual__c" {
  const Welfare_Actual__c:number;
  export default Welfare_Actual__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Receipt_Count__c" {
  const Receipt_Count__c:number;
  export default Receipt_Count__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Total_Cancelled__c" {
  const Total_Cancelled__c:number;
  export default Total_Cancelled__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.PledgeAge__c" {
  const PledgeAge__c:number;
  export default PledgeAge__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Account_Type_Number__c" {
  const Account_Type_Number__c:string;
  export default Account_Type_Number__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Duplicate_Check__c" {
  const Duplicate_Check__c:boolean;
  export default Duplicate_Check__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Frequency_Code__c" {
  const Frequency_Code__c:string;
  export default Frequency_Code__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.YearofCampaign__c" {
  const YearofCampaign__c:number;
  export default YearofCampaign__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Donor_Id__c" {
  const Donor_Id__c:string;
  export default Donor_Id__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Total_Adjustments__c" {
  const Total_Adjustments__c:number;
  export default Total_Adjustments__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Next_Receipt_Date__c" {
  const Next_Receipt_Date__c:any;
  export default Next_Receipt_Date__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Next_Receipt_Month_Year__c" {
  const Next_Receipt_Month_Year__c:string;
  export default Next_Receipt_Month_Year__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Deactivate_Date__c" {
  const Deactivate_Date__c:any;
  export default Deactivate_Date__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Canvassers_All__c" {
  const Canvassers_All__c:string;
  export default Canvassers_All__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.PledgeEmail__c" {
  const PledgeEmail__c:string;
  export default PledgeEmail__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Do_Not_Send_Email__c" {
  const Do_Not_Send_Email__c:boolean;
  export default Do_Not_Send_Email__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Pledge_Category__c" {
  const Pledge_Category__c:string;
  export default Pledge_Category__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Donor_Did_Not_Split__c" {
  const Donor_Did_Not_Split__c:boolean;
  export default Donor_Did_Not_Split__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.PledgeID__c" {
  const PledgeID__c:string;
  export default PledgeID__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.UCF_Com_Specified__c" {
  const UCF_Com_Specified__c:string;
  export default UCF_Com_Specified__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.UCF_Ed_Specified__c" {
  const UCF_Ed_Specified__c:string;
  export default UCF_Ed_Specified__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Welfare_Specified__c" {
  const Welfare_Specified__c:string;
  export default Welfare_Specified__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.IUA_Fund_Specified__c" {
  const IUA_Fund_Specified__c:string;
  export default IUA_Fund_Specified__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Today_Date__c" {
  const Today_Date__c:string;
  export default Today_Date__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Do_Not_Ack__c" {
  const Do_Not_Ack__c:boolean;
  export default Do_Not_Ack__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Thank_you_Sent__c" {
  const Thank_you_Sent__c:boolean;
  export default Thank_you_Sent__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.UCF_Total__c" {
  const UCF_Total__c:number;
  export default UCF_Total__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Pledge_Total_Group__c" {
  const Pledge_Total_Group__c:string;
  export default Pledge_Total_Group__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Pledge_18_000_Group__c" {
  const Pledge_18_000_Group__c:string;
  export default Pledge_18_000_Group__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Campaign_Name__r" {
  const Campaign_Name__r:any;
  export default Campaign_Name__r;
}
declare module "@salesforce/schema/Recurring_Donation__c.Campaign_Name__c" {
  const Campaign_Name__c:any;
  export default Campaign_Name__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.DonorEmail__c" {
  const DonorEmail__c:string;
  export default DonorEmail__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Pledge_Type__c" {
  const Pledge_Type__c:string;
  export default Pledge_Type__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Specified_Amount__c" {
  const Specified_Amount__c:number;
  export default Specified_Amount__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.SP_Welfare_Total__c" {
  const SP_Welfare_Total__c:number;
  export default SP_Welfare_Total__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.SP_IUA_Total__c" {
  const SP_IUA_Total__c:number;
  export default SP_IUA_Total__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.SP_UCF_Ed__c" {
  const SP_UCF_Ed__c:number;
  export default SP_UCF_Ed__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.SP_UCF_Comm__c" {
  const SP_UCF_Comm__c:number;
  export default SP_UCF_Comm__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Donor_Cell__c" {
  const Donor_Cell__c:string;
  export default Donor_Cell__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Pledge_Month__c" {
  const Pledge_Month__c:string;
  export default Pledge_Month__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Refusal_Reason__c" {
  const Refusal_Reason__c:string;
  export default Refusal_Reason__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Canvasser__r" {
  const Canvasser__r:any;
  export default Canvasser__r;
}
declare module "@salesforce/schema/Recurring_Donation__c.Canvasser__c" {
  const Canvasser__c:any;
  export default Canvasser__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Sp_payover_formula__c" {
  const Sp_payover_formula__c:boolean;
  export default Sp_payover_formula__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Age_at_Donation__c" {
  const Age_at_Donation__c:number;
  export default Age_at_Donation__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.DO_Payment_Day__c" {
  const DO_Payment_Day__c:string;
  export default DO_Payment_Day__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Debut_Pledge__c" {
  const Debut_Pledge__c:boolean;
  export default Debut_Pledge__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.DateSignedWithAmount__c" {
  const DateSignedWithAmount__c:number;
  export default DateSignedWithAmount__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.LR_Date__c" {
  const LR_Date__c:any;
  export default LR_Date__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Latest_JC_Pledge__c" {
  const Latest_JC_Pledge__c:boolean;
  export default Latest_JC_Pledge__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Last_Canvas_Method__c" {
  const Last_Canvas_Method__c:string;
  export default Last_Canvas_Method__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Last_Canvasser__c" {
  const Last_Canvasser__c:string;
  export default Last_Canvasser__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Most_Recent_Pledge__c" {
  const Most_Recent_Pledge__c:boolean;
  export default Most_Recent_Pledge__c;
}
declare module "@salesforce/schema/Recurring_Donation__c.Dummy_Checkbox__c" {
  const Dummy_Checkbox__c:boolean;
  export default Dummy_Checkbox__c;
}
