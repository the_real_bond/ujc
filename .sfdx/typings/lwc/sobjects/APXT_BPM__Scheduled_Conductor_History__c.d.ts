declare module "@salesforce/schema/APXT_BPM__Scheduled_Conductor_History__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/APXT_BPM__Scheduled_Conductor_History__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/APXT_BPM__Scheduled_Conductor_History__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/APXT_BPM__Scheduled_Conductor_History__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/APXT_BPM__Scheduled_Conductor_History__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/APXT_BPM__Scheduled_Conductor_History__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/APXT_BPM__Scheduled_Conductor_History__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/APXT_BPM__Scheduled_Conductor_History__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/APXT_BPM__Scheduled_Conductor_History__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/APXT_BPM__Scheduled_Conductor_History__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/APXT_BPM__Scheduled_Conductor_History__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/APXT_BPM__Scheduled_Conductor_History__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/APXT_BPM__Scheduled_Conductor_History__c.APXT_BPM__Conga_Conductor__r" {
  const APXT_BPM__Conga_Conductor__r:any;
  export default APXT_BPM__Conga_Conductor__r;
}
declare module "@salesforce/schema/APXT_BPM__Scheduled_Conductor_History__c.APXT_BPM__Conga_Conductor__c" {
  const APXT_BPM__Conga_Conductor__c:any;
  export default APXT_BPM__Conga_Conductor__c;
}
declare module "@salesforce/schema/APXT_BPM__Scheduled_Conductor_History__c.APXT_BPM__Date__c" {
  const APXT_BPM__Date__c:any;
  export default APXT_BPM__Date__c;
}
declare module "@salesforce/schema/APXT_BPM__Scheduled_Conductor_History__c.APXT_BPM__Description__c" {
  const APXT_BPM__Description__c:string;
  export default APXT_BPM__Description__c;
}
declare module "@salesforce/schema/APXT_BPM__Scheduled_Conductor_History__c.APXT_BPM__Number_of_Failures__c" {
  const APXT_BPM__Number_of_Failures__c:number;
  export default APXT_BPM__Number_of_Failures__c;
}
declare module "@salesforce/schema/APXT_BPM__Scheduled_Conductor_History__c.APXT_BPM__Number_of_Service_Events__c" {
  const APXT_BPM__Number_of_Service_Events__c:number;
  export default APXT_BPM__Number_of_Service_Events__c;
}
declare module "@salesforce/schema/APXT_BPM__Scheduled_Conductor_History__c.APXT_BPM__Number_of_Successes__c" {
  const APXT_BPM__Number_of_Successes__c:number;
  export default APXT_BPM__Number_of_Successes__c;
}
declare module "@salesforce/schema/APXT_BPM__Scheduled_Conductor_History__c.APXT_BPM__Output_File_Link__c" {
  const APXT_BPM__Output_File_Link__c:string;
  export default APXT_BPM__Output_File_Link__c;
}
declare module "@salesforce/schema/APXT_BPM__Scheduled_Conductor_History__c.APXT_BPM__Ran_as__r" {
  const APXT_BPM__Ran_as__r:any;
  export default APXT_BPM__Ran_as__r;
}
declare module "@salesforce/schema/APXT_BPM__Scheduled_Conductor_History__c.APXT_BPM__Ran_as__c" {
  const APXT_BPM__Ran_as__c:any;
  export default APXT_BPM__Ran_as__c;
}
declare module "@salesforce/schema/APXT_BPM__Scheduled_Conductor_History__c.APXT_BPM__Total_Number_of_Records__c" {
  const APXT_BPM__Total_Number_of_Records__c:number;
  export default APXT_BPM__Total_Number_of_Records__c;
}
