declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__Avg_Click_Rate__c" {
  const MC4SF__Avg_Click_Rate__c:number;
  export default MC4SF__Avg_Click_Rate__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__Avg_Open_Rate__c" {
  const MC4SF__Avg_Open_Rate__c:number;
  export default MC4SF__Avg_Open_Rate__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__Avg_Sub_Rate__c" {
  const MC4SF__Avg_Sub_Rate__c:number;
  export default MC4SF__Avg_Sub_Rate__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__Avg_Unsub_Rate__c" {
  const MC4SF__Avg_Unsub_Rate__c:number;
  export default MC4SF__Avg_Unsub_Rate__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__Beamer_Address__c" {
  const MC4SF__Beamer_Address__c:string;
  export default MC4SF__Beamer_Address__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__Campaign_Count__c" {
  const MC4SF__Campaign_Count__c:number;
  export default MC4SF__Campaign_Count__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__Cleaned_Count_Since_Send__c" {
  const MC4SF__Cleaned_Count_Since_Send__c:number;
  export default MC4SF__Cleaned_Count_Since_Send__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__Cleaned_Count__c" {
  const MC4SF__Cleaned_Count__c:number;
  export default MC4SF__Cleaned_Count__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__Create_New_Leads_From_MailChimp__c" {
  const MC4SF__Create_New_Leads_From_MailChimp__c:boolean;
  export default MC4SF__Create_New_Leads_From_MailChimp__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__Date_Created__c" {
  const MC4SF__Date_Created__c:string;
  export default MC4SF__Date_Created__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__Default_From_Email__c" {
  const MC4SF__Default_From_Email__c:string;
  export default MC4SF__Default_From_Email__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__Default_From_Name__c" {
  const MC4SF__Default_From_Name__c:string;
  export default MC4SF__Default_From_Name__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__Default_Language__c" {
  const MC4SF__Default_Language__c:string;
  export default MC4SF__Default_Language__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__Default_Subject__c" {
  const MC4SF__Default_Subject__c:string;
  export default MC4SF__Default_Subject__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__Email_Type_Option__c" {
  const MC4SF__Email_Type_Option__c:boolean;
  export default MC4SF__Email_Type_Option__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__Group_Count__c" {
  const MC4SF__Group_Count__c:number;
  export default MC4SF__Group_Count__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__Grouping_Count__c" {
  const MC4SF__Grouping_Count__c:number;
  export default MC4SF__Grouping_Count__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__Last_Cleaned_Sync_Date__c" {
  const MC4SF__Last_Cleaned_Sync_Date__c:any;
  export default MC4SF__Last_Cleaned_Sync_Date__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__Last_Subscribed_Sync_Date__c" {
  const MC4SF__Last_Subscribed_Sync_Date__c:any;
  export default MC4SF__Last_Subscribed_Sync_Date__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__Last_Sync_Date__c" {
  const MC4SF__Last_Sync_Date__c:any;
  export default MC4SF__Last_Sync_Date__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__Last_Sync_Status__c" {
  const MC4SF__Last_Sync_Status__c:string;
  export default MC4SF__Last_Sync_Status__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__Last_Unsubscribed_Sync_Date__c" {
  const MC4SF__Last_Unsubscribed_Sync_Date__c:any;
  export default MC4SF__Last_Unsubscribed_Sync_Date__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__List_Rating__c" {
  const MC4SF__List_Rating__c:number;
  export default MC4SF__List_Rating__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__MailChimp_ID__c" {
  const MC4SF__MailChimp_ID__c:string;
  export default MC4SF__MailChimp_ID__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__MailChimp_Web_ID__c" {
  const MC4SF__MailChimp_Web_ID__c:string;
  export default MC4SF__MailChimp_Web_ID__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__Member_Count_Since_Send__c" {
  const MC4SF__Member_Count_Since_Send__c:number;
  export default MC4SF__Member_Count_Since_Send__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__Member_Count__c" {
  const MC4SF__Member_Count__c:number;
  export default MC4SF__Member_Count__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__Merge_Var_Count__c" {
  const MC4SF__Merge_Var_Count__c:number;
  export default MC4SF__Merge_Var_Count__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__Modules__c" {
  const MC4SF__Modules__c:string;
  export default MC4SF__Modules__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__Subscribe_URL_Long__c" {
  const MC4SF__Subscribe_URL_Long__c:string;
  export default MC4SF__Subscribe_URL_Long__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__Subscribe_URL_Short__c" {
  const MC4SF__Subscribe_URL_Short__c:string;
  export default MC4SF__Subscribe_URL_Short__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__Target_Sub_Rate__c" {
  const MC4SF__Target_Sub_Rate__c:number;
  export default MC4SF__Target_Sub_Rate__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__Unsubscribe_Count_Since_Send__c" {
  const MC4SF__Unsubscribe_Count_Since_Send__c:number;
  export default MC4SF__Unsubscribe_Count_Since_Send__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__Unsubscribe_Count__c" {
  const MC4SF__Unsubscribe_Count__c:number;
  export default MC4SF__Unsubscribe_Count__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__Use_Awesomebar__c" {
  const MC4SF__Use_Awesomebar__c:boolean;
  export default MC4SF__Use_Awesomebar__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__Visibility__c" {
  const MC4SF__Visibility__c:string;
  export default MC4SF__Visibility__c;
}
declare module "@salesforce/schema/MC4SF__MC_List__ChangeEvent.MC4SF__Unmapped_Fields__c" {
  const MC4SF__Unmapped_Fields__c:number;
  export default MC4SF__Unmapped_Fields__c;
}
