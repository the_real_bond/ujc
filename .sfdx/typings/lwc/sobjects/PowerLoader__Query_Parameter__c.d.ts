declare module "@salesforce/schema/PowerLoader__Query_Parameter__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter__c.PowerLoader__Query_Panel__r" {
  const PowerLoader__Query_Panel__r:any;
  export default PowerLoader__Query_Panel__r;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter__c.PowerLoader__Query_Panel__c" {
  const PowerLoader__Query_Panel__c:any;
  export default PowerLoader__Query_Panel__c;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter__c.PowerLoader__Base_Master_Field_API_Name__c" {
  const PowerLoader__Base_Master_Field_API_Name__c:string;
  export default PowerLoader__Base_Master_Field_API_Name__c;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter__c.PowerLoader__Base_Object_API_Name__c" {
  const PowerLoader__Base_Object_API_Name__c:string;
  export default PowerLoader__Base_Object_API_Name__c;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter__c.PowerLoader__Default_Value_Type__c" {
  const PowerLoader__Default_Value_Type__c:string;
  export default PowerLoader__Default_Value_Type__c;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter__c.PowerLoader__Default_Value__c" {
  const PowerLoader__Default_Value__c:string;
  export default PowerLoader__Default_Value__c;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter__c.PowerLoader__Display_Sequence__c" {
  const PowerLoader__Display_Sequence__c:number;
  export default PowerLoader__Display_Sequence__c;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter__c.PowerLoader__Display_Type__c" {
  const PowerLoader__Display_Type__c:string;
  export default PowerLoader__Display_Type__c;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter__c.PowerLoader__IsDisabled__c" {
  const PowerLoader__IsDisabled__c:boolean;
  export default PowerLoader__IsDisabled__c;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter__c.PowerLoader__IsHidden__c" {
  const PowerLoader__IsHidden__c:boolean;
  export default PowerLoader__IsHidden__c;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter__c.PowerLoader__IsRequired__c" {
  const PowerLoader__IsRequired__c:boolean;
  export default PowerLoader__IsRequired__c;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter__c.PowerLoader__Is_Label_HTML__c" {
  const PowerLoader__Is_Label_HTML__c:boolean;
  export default PowerLoader__Is_Label_HTML__c;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter__c.PowerLoader__Label__c" {
  const PowerLoader__Label__c:string;
  export default PowerLoader__Label__c;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter__c.PowerLoader__List_Label_Field_API_Name__c" {
  const PowerLoader__List_Label_Field_API_Name__c:string;
  export default PowerLoader__List_Label_Field_API_Name__c;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter__c.PowerLoader__List_Source_Type__c" {
  const PowerLoader__List_Source_Type__c:string;
  export default PowerLoader__List_Source_Type__c;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter__c.PowerLoader__List_Source__c" {
  const PowerLoader__List_Source__c:string;
  export default PowerLoader__List_Source__c;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter__c.PowerLoader__List_Value_Field_API_Name__c" {
  const PowerLoader__List_Value_Field_API_Name__c:string;
  export default PowerLoader__List_Value_Field_API_Name__c;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter__c.PowerLoader__Object_API_Name__c" {
  const PowerLoader__Object_API_Name__c:string;
  export default PowerLoader__Object_API_Name__c;
}
declare module "@salesforce/schema/PowerLoader__Query_Parameter__c.PowerLoader__Tag__c" {
  const PowerLoader__Tag__c:string;
  export default PowerLoader__Tag__c;
}
