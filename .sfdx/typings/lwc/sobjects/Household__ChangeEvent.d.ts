declare module "@salesforce/schema/Household__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Household__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/Household__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/Household__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Household__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Household__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Household__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Household__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Household__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Household__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Household__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Household__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Household__ChangeEvent.Communal_Reference_Number__c" {
  const Communal_Reference_Number__c:string;
  export default Communal_Reference_Number__c;
}
declare module "@salesforce/schema/Household__ChangeEvent.Old_Register_Area_R__c" {
  const Old_Register_Area_R__c:string;
  export default Old_Register_Area_R__c;
}
declare module "@salesforce/schema/Household__ChangeEvent.Household_Counter__c" {
  const Household_Counter__c:number;
  export default Household_Counter__c;
}
declare module "@salesforce/schema/Household__ChangeEvent.City_RP__c" {
  const City_RP__c:string;
  export default City_RP__c;
}
declare module "@salesforce/schema/Household__ChangeEvent.City_RS__c" {
  const City_RS__c:string;
  export default City_RS__c;
}
declare module "@salesforce/schema/Household__ChangeEvent.Country_RP__c" {
  const Country_RP__c:string;
  export default Country_RP__c;
}
declare module "@salesforce/schema/Household__ChangeEvent.Country_RS__c" {
  const Country_RS__c:string;
  export default Country_RS__c;
}
declare module "@salesforce/schema/Household__ChangeEvent.Email_Residential__c" {
  const Email_Residential__c:string;
  export default Email_Residential__c;
}
declare module "@salesforce/schema/Household__ChangeEvent.Postal_Code_RP__c" {
  const Postal_Code_RP__c:string;
  export default Postal_Code_RP__c;
}
declare module "@salesforce/schema/Household__ChangeEvent.Postal_Code_RS__c" {
  const Postal_Code_RS__c:string;
  export default Postal_Code_RS__c;
}
declare module "@salesforce/schema/Household__ChangeEvent.Province_RP__c" {
  const Province_RP__c:string;
  export default Province_RP__c;
}
declare module "@salesforce/schema/Household__ChangeEvent.Province_RS__c" {
  const Province_RS__c:string;
  export default Province_RS__c;
}
declare module "@salesforce/schema/Household__ChangeEvent.Residential_Address_RS__c" {
  const Residential_Address_RS__c:string;
  export default Residential_Address_RS__c;
}
declare module "@salesforce/schema/Household__ChangeEvent.Residential_Postal_Address_RP__c" {
  const Residential_Postal_Address_RP__c:string;
  export default Residential_Postal_Address_RP__c;
}
declare module "@salesforce/schema/Household__ChangeEvent.Residential_Telephone__c" {
  const Residential_Telephone__c:string;
  export default Residential_Telephone__c;
}
declare module "@salesforce/schema/Household__ChangeEvent.Suburb_RP__c" {
  const Suburb_RP__c:string;
  export default Suburb_RP__c;
}
declare module "@salesforce/schema/Household__ChangeEvent.Suburb_RS__c" {
  const Suburb_RS__c:string;
  export default Suburb_RS__c;
}
