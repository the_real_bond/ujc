declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__ChangeEvent.CnP_PaaS__Broadcaster_List__c" {
  const CnP_PaaS__Broadcaster_List__c:any;
  export default CnP_PaaS__Broadcaster_List__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__ChangeEvent.CnP_PaaS__Field_Id__c" {
  const CnP_PaaS__Field_Id__c:number;
  export default CnP_PaaS__Field_Id__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__ChangeEvent.CnP_PaaS__Req__c" {
  const CnP_PaaS__Req__c:boolean;
  export default CnP_PaaS__Req__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__ChangeEvent.CnP_PaaS__Tag__c" {
  const CnP_PaaS__Tag__c:string;
  export default CnP_PaaS__Tag__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__ChangeEvent.CnP_PaaS__helptext__c" {
  const CnP_PaaS__helptext__c:string;
  export default CnP_PaaS__helptext__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__ChangeEvent.CnP_PaaS__order__c" {
  const CnP_PaaS__order__c:string;
  export default CnP_PaaS__order__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__ChangeEvent.CnP_PaaS__show__c" {
  const CnP_PaaS__show__c:boolean;
  export default CnP_PaaS__show__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Merge_Fields__ChangeEvent.CnP_PaaS__size__c" {
  const CnP_PaaS__size__c:string;
  export default CnP_PaaS__size__c;
}
