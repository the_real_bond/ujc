declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats_Per_Hour__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats_Per_Hour__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats_Per_Hour__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats_Per_Hour__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats_Per_Hour__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats_Per_Hour__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats_Per_Hour__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats_Per_Hour__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats_Per_Hour__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats_Per_Hour__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats_Per_Hour__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats_Per_Hour__c.CnP_PaaS__Broadcaster_Campaign__r" {
  const CnP_PaaS__Broadcaster_Campaign__r:any;
  export default CnP_PaaS__Broadcaster_Campaign__r;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats_Per_Hour__c.CnP_PaaS__Broadcaster_Campaign__c" {
  const CnP_PaaS__Broadcaster_Campaign__c:any;
  export default CnP_PaaS__Broadcaster_Campaign__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats_Per_Hour__c.CnP_PaaS__Broadcaster_Stats__r" {
  const CnP_PaaS__Broadcaster_Stats__r:any;
  export default CnP_PaaS__Broadcaster_Stats__r;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats_Per_Hour__c.CnP_PaaS__Broadcaster_Stats__c" {
  const CnP_PaaS__Broadcaster_Stats__c:any;
  export default CnP_PaaS__Broadcaster_Stats__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats_Per_Hour__c.CnP_PaaS__Emails_Sent__c" {
  const CnP_PaaS__Emails_Sent__c:number;
  export default CnP_PaaS__Emails_Sent__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats_Per_Hour__c.CnP_PaaS__Recipients_Click__c" {
  const CnP_PaaS__Recipients_Click__c:number;
  export default CnP_PaaS__Recipients_Click__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats_Per_Hour__c.CnP_PaaS__Timestamp__c" {
  const CnP_PaaS__Timestamp__c:string;
  export default CnP_PaaS__Timestamp__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats_Per_Hour__c.CnP_PaaS__Unique_Opens__c" {
  const CnP_PaaS__Unique_Opens__c:number;
  export default CnP_PaaS__Unique_Opens__c;
}
