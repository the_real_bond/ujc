declare module "@salesforce/schema/Donation_Receipts__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Donation_Receipts__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Donation_Receipts__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Donation_Receipts__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Donation_Receipts__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Donation_Receipts__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Donation_Receipts__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Donation_Receipts__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Donation_Receipts__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Donation_Receipts__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Donation_Receipts__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/Donation_Receipts__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Donation_Receipts__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Donation_Receipts__c.Tax_Receipt__r" {
  const Tax_Receipt__r:any;
  export default Tax_Receipt__r;
}
declare module "@salesforce/schema/Donation_Receipts__c.Tax_Receipt__c" {
  const Tax_Receipt__c:any;
  export default Tax_Receipt__c;
}
declare module "@salesforce/schema/Donation_Receipts__c.Receipt__r" {
  const Receipt__r:any;
  export default Receipt__r;
}
declare module "@salesforce/schema/Donation_Receipts__c.Receipt__c" {
  const Receipt__c:any;
  export default Receipt__c;
}
declare module "@salesforce/schema/Donation_Receipts__c.Amount_in_Words__c" {
  const Amount_in_Words__c:string;
  export default Amount_in_Words__c;
}
declare module "@salesforce/schema/Donation_Receipts__c.Date__c" {
  const Date__c:any;
  export default Date__c;
}
declare module "@salesforce/schema/Donation_Receipts__c.Donation_Amount__c" {
  const Donation_Amount__c:number;
  export default Donation_Amount__c;
}
declare module "@salesforce/schema/Donation_Receipts__c.Donation_Type__c" {
  const Donation_Type__c:string;
  export default Donation_Type__c;
}
declare module "@salesforce/schema/Donation_Receipts__c.Type__c" {
  const Type__c:string;
  export default Type__c;
}
declare module "@salesforce/schema/Donation_Receipts__c.dateclosed__c" {
  const dateclosed__c:string;
  export default dateclosed__c;
}
declare module "@salesforce/schema/Donation_Receipts__c.Donor__c" {
  const Donor__c:string;
  export default Donor__c;
}
