declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats__ChangeEvent.CnP_PaaS__Broadcaster_Campaign__c" {
  const CnP_PaaS__Broadcaster_Campaign__c:any;
  export default CnP_PaaS__Broadcaster_Campaign__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats__ChangeEvent.CnP_PaaS__Abuse_Reports__c" {
  const CnP_PaaS__Abuse_Reports__c:number;
  export default CnP_PaaS__Abuse_Reports__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats__ChangeEvent.CnP_PaaS__Clicks__c" {
  const CnP_PaaS__Clicks__c:number;
  export default CnP_PaaS__Clicks__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats__ChangeEvent.CnP_PaaS__Emails_Sent__c" {
  const CnP_PaaS__Emails_Sent__c:number;
  export default CnP_PaaS__Emails_Sent__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats__ChangeEvent.CnP_PaaS__Facebook_Likes__c" {
  const CnP_PaaS__Facebook_Likes__c:number;
  export default CnP_PaaS__Facebook_Likes__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats__ChangeEvent.CnP_PaaS__Forwards_Opens__c" {
  const CnP_PaaS__Forwards_Opens__c:number;
  export default CnP_PaaS__Forwards_Opens__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats__ChangeEvent.CnP_PaaS__Forwards__c" {
  const CnP_PaaS__Forwards__c:number;
  export default CnP_PaaS__Forwards__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats__ChangeEvent.CnP_PaaS__Hard_Bounces__c" {
  const CnP_PaaS__Hard_Bounces__c:number;
  export default CnP_PaaS__Hard_Bounces__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats__ChangeEvent.CnP_PaaS__Last_Click__c" {
  const CnP_PaaS__Last_Click__c:string;
  export default CnP_PaaS__Last_Click__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats__ChangeEvent.CnP_PaaS__Last_Open__c" {
  const CnP_PaaS__Last_Open__c:string;
  export default CnP_PaaS__Last_Open__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats__ChangeEvent.CnP_PaaS__Opens__c" {
  const CnP_PaaS__Opens__c:number;
  export default CnP_PaaS__Opens__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats__ChangeEvent.CnP_PaaS__Recipient_Likes__c" {
  const CnP_PaaS__Recipient_Likes__c:number;
  export default CnP_PaaS__Recipient_Likes__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats__ChangeEvent.CnP_PaaS__Soft_Bounces__c" {
  const CnP_PaaS__Soft_Bounces__c:number;
  export default CnP_PaaS__Soft_Bounces__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats__ChangeEvent.CnP_PaaS__Syntax_Errors__c" {
  const CnP_PaaS__Syntax_Errors__c:number;
  export default CnP_PaaS__Syntax_Errors__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats__ChangeEvent.CnP_PaaS__Unique_Clicks__c" {
  const CnP_PaaS__Unique_Clicks__c:number;
  export default CnP_PaaS__Unique_Clicks__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats__ChangeEvent.CnP_PaaS__Unique_Likes__c" {
  const CnP_PaaS__Unique_Likes__c:number;
  export default CnP_PaaS__Unique_Likes__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats__ChangeEvent.CnP_PaaS__Unique_Opens__c" {
  const CnP_PaaS__Unique_Opens__c:number;
  export default CnP_PaaS__Unique_Opens__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats__ChangeEvent.CnP_PaaS__Unsubscribes__c" {
  const CnP_PaaS__Unsubscribes__c:number;
  export default CnP_PaaS__Unsubscribes__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats__ChangeEvent.CnP_PaaS__Users_Who_Cclicked__c" {
  const CnP_PaaS__Users_Who_Cclicked__c:number;
  export default CnP_PaaS__Users_Who_Cclicked__c;
}
