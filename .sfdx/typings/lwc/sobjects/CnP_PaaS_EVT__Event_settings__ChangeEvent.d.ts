declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Acknowledgement_Mandatory__c" {
  const CnP_PaaS_EVT__Acknowledgement_Mandatory__c:boolean;
  export default CnP_PaaS_EVT__Acknowledgement_Mandatory__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__American_Express__c" {
  const CnP_PaaS_EVT__American_Express__c:boolean;
  export default CnP_PaaS_EVT__American_Express__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Attendee_Ticket_Email_Body__c" {
  const CnP_PaaS_EVT__Attendee_Ticket_Email_Body__c:string;
  export default CnP_PaaS_EVT__Attendee_Ticket_Email_Body__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Box_Office_American_Express__c" {
  const CnP_PaaS_EVT__Box_Office_American_Express__c:boolean;
  export default CnP_PaaS_EVT__Box_Office_American_Express__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Box_Office_Credit_Card__c" {
  const CnP_PaaS_EVT__Box_Office_Credit_Card__c:boolean;
  export default CnP_PaaS_EVT__Box_Office_Credit_Card__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Box_Office_Custom_Payment_Check__c" {
  const CnP_PaaS_EVT__Box_Office_Custom_Payment_Check__c:boolean;
  export default CnP_PaaS_EVT__Box_Office_Custom_Payment_Check__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Box_Office_Custom_Payment_Name__c" {
  const CnP_PaaS_EVT__Box_Office_Custom_Payment_Name__c:string;
  export default CnP_PaaS_EVT__Box_Office_Custom_Payment_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Box_Office_Discover__c" {
  const CnP_PaaS_EVT__Box_Office_Discover__c:boolean;
  export default CnP_PaaS_EVT__Box_Office_Discover__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Box_Office_Free_Payment__c" {
  const CnP_PaaS_EVT__Box_Office_Free_Payment__c:string;
  export default CnP_PaaS_EVT__Box_Office_Free_Payment__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Box_Office_Invoice__c" {
  const CnP_PaaS_EVT__Box_Office_Invoice__c:boolean;
  export default CnP_PaaS_EVT__Box_Office_Invoice__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Box_Office_JCB__c" {
  const CnP_PaaS_EVT__Box_Office_JCB__c:boolean;
  export default CnP_PaaS_EVT__Box_Office_JCB__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Box_Office_Master_Card__c" {
  const CnP_PaaS_EVT__Box_Office_Master_Card__c:boolean;
  export default CnP_PaaS_EVT__Box_Office_Master_Card__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Box_Office_Purchase_Order__c" {
  const CnP_PaaS_EVT__Box_Office_Purchase_Order__c:boolean;
  export default CnP_PaaS_EVT__Box_Office_Purchase_Order__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Box_Office_Visa__c" {
  const CnP_PaaS_EVT__Box_Office_Visa__c:boolean;
  export default CnP_PaaS_EVT__Box_Office_Visa__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Box_Office_eCheck__c" {
  const CnP_PaaS_EVT__Box_Office_eCheck__c:boolean;
  export default CnP_PaaS_EVT__Box_Office_eCheck__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Credit_Card__c" {
  const CnP_PaaS_EVT__Credit_Card__c:boolean;
  export default CnP_PaaS_EVT__Credit_Card__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Custom_Payment_Check__c" {
  const CnP_PaaS_EVT__Custom_Payment_Check__c:boolean;
  export default CnP_PaaS_EVT__Custom_Payment_Check__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Custom_Payment_Name__c" {
  const CnP_PaaS_EVT__Custom_Payment_Name__c:string;
  export default CnP_PaaS_EVT__Custom_Payment_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Default_Account_Number__c" {
  const CnP_PaaS_EVT__Default_Account_Number__c:string;
  export default CnP_PaaS_EVT__Default_Account_Number__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Default_Public_Site_Url__c" {
  const CnP_PaaS_EVT__Default_Public_Site_Url__c:string;
  export default CnP_PaaS_EVT__Default_Public_Site_Url__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Discover__c" {
  const CnP_PaaS_EVT__Discover__c:boolean;
  export default CnP_PaaS_EVT__Discover__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Email_Fromname__c" {
  const CnP_PaaS_EVT__Email_Fromname__c:string;
  export default CnP_PaaS_EVT__Email_Fromname__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Email_ReplyTo__c" {
  const CnP_PaaS_EVT__Email_ReplyTo__c:string;
  export default CnP_PaaS_EVT__Email_ReplyTo__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Email_Subject__c" {
  const CnP_PaaS_EVT__Email_Subject__c:string;
  export default CnP_PaaS_EVT__Email_Subject__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Event_Ended__c" {
  const CnP_PaaS_EVT__Event_Ended__c:string;
  export default CnP_PaaS_EVT__Event_Ended__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Event_categories__c" {
  const CnP_PaaS_EVT__Event_categories__c:string;
  export default CnP_PaaS_EVT__Event_categories__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Free_Payment__c" {
  const CnP_PaaS_EVT__Free_Payment__c:string;
  export default CnP_PaaS_EVT__Free_Payment__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Internal_Notification__c" {
  const CnP_PaaS_EVT__Internal_Notification__c:string;
  export default CnP_PaaS_EVT__Internal_Notification__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Invoice__c" {
  const CnP_PaaS_EVT__Invoice__c:boolean;
  export default CnP_PaaS_EVT__Invoice__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__JCB__c" {
  const CnP_PaaS_EVT__JCB__c:boolean;
  export default CnP_PaaS_EVT__JCB__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Level_Sold_Out_Message__c" {
  const CnP_PaaS_EVT__Level_Sold_Out_Message__c:string;
  export default CnP_PaaS_EVT__Level_Sold_Out_Message__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Master_Card__c" {
  const CnP_PaaS_EVT__Master_Card__c:boolean;
  export default CnP_PaaS_EVT__Master_Card__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Organization_Information__c" {
  const CnP_PaaS_EVT__Organization_Information__c:string;
  export default CnP_PaaS_EVT__Organization_Information__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Purchase_Order__c" {
  const CnP_PaaS_EVT__Purchase_Order__c:boolean;
  export default CnP_PaaS_EVT__Purchase_Order__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Receipt_Terms_Condition__c" {
  const CnP_PaaS_EVT__Receipt_Terms_Condition__c:string;
  export default CnP_PaaS_EVT__Receipt_Terms_Condition__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Registered_Status_options__c" {
  const CnP_PaaS_EVT__Registered_Status_options__c:string;
  export default CnP_PaaS_EVT__Registered_Status_options__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Registrant_Ticket_Email_Body__c" {
  const CnP_PaaS_EVT__Registrant_Ticket_Email_Body__c:string;
  export default CnP_PaaS_EVT__Registrant_Ticket_Email_Body__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Registration_Declined_Message__c" {
  const CnP_PaaS_EVT__Registration_Declined_Message__c:string;
  export default CnP_PaaS_EVT__Registration_Declined_Message__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Send_Receipt__c" {
  const CnP_PaaS_EVT__Send_Receipt__c:boolean;
  export default CnP_PaaS_EVT__Send_Receipt__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Show_Terms_Conditions__c" {
  const CnP_PaaS_EVT__Show_Terms_Conditions__c:boolean;
  export default CnP_PaaS_EVT__Show_Terms_Conditions__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Site_Custom_Payment_Name__c" {
  const CnP_PaaS_EVT__Site_Custom_Payment_Name__c:string;
  export default CnP_PaaS_EVT__Site_Custom_Payment_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Sold_Out__c" {
  const CnP_PaaS_EVT__Sold_Out__c:string;
  export default CnP_PaaS_EVT__Sold_Out__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Terms_Conditions__c" {
  const CnP_PaaS_EVT__Terms_Conditions__c:string;
  export default CnP_PaaS_EVT__Terms_Conditions__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Thank_You_Message_Receipt__c" {
  const CnP_PaaS_EVT__Thank_You_Message_Receipt__c:string;
  export default CnP_PaaS_EVT__Thank_You_Message_Receipt__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Thank_You_Message__c" {
  const CnP_PaaS_EVT__Thank_You_Message__c:string;
  export default CnP_PaaS_EVT__Thank_You_Message__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__Visa__c" {
  const CnP_PaaS_EVT__Visa__c:boolean;
  export default CnP_PaaS_EVT__Visa__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_settings__ChangeEvent.CnP_PaaS_EVT__eCheck__c" {
  const CnP_PaaS_EVT__eCheck__c:boolean;
  export default CnP_PaaS_EVT__eCheck__c;
}
