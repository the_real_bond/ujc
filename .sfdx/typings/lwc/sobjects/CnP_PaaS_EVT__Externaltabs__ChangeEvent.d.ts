declare module "@salesforce/schema/CnP_PaaS_EVT__Externaltabs__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Externaltabs__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Externaltabs__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Externaltabs__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Externaltabs__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Externaltabs__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Externaltabs__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Externaltabs__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Externaltabs__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Externaltabs__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Externaltabs__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Externaltabs__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Externaltabs__ChangeEvent.CnP_PaaS_EVT__C_P_Event__c" {
  const CnP_PaaS_EVT__C_P_Event__c:any;
  export default CnP_PaaS_EVT__C_P_Event__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Externaltabs__ChangeEvent.CnP_PaaS_EVT__C_P_Widget__c" {
  const CnP_PaaS_EVT__C_P_Widget__c:any;
  export default CnP_PaaS_EVT__C_P_Widget__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Externaltabs__ChangeEvent.CnP_PaaS_EVT__Tab_order_number__c" {
  const CnP_PaaS_EVT__Tab_order_number__c:number;
  export default CnP_PaaS_EVT__Tab_order_number__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Externaltabs__ChangeEvent.CnP_PaaS_EVT__content_tab1__c" {
  const CnP_PaaS_EVT__content_tab1__c:string;
  export default CnP_PaaS_EVT__content_tab1__c;
}
