declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__Acknowledgement__c" {
  const CnP_PaaS__Acknowledgement__c:boolean;
  export default CnP_PaaS__Acknowledgement__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__American_Express__c" {
  const CnP_PaaS__American_Express__c:boolean;
  export default CnP_PaaS__American_Express__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__Application_Name__c" {
  const CnP_PaaS__Application_Name__c:string;
  export default CnP_PaaS__Application_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__Base_URL__c" {
  const CnP_PaaS__Base_URL__c:string;
  export default CnP_PaaS__Base_URL__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__Click_Pledge_Account_Id__c" {
  const CnP_PaaS__Click_Pledge_Account_Id__c:string;
  export default CnP_PaaS__Click_Pledge_Account_Id__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__Contact_Role__c" {
  const CnP_PaaS__Contact_Role__c:string;
  export default CnP_PaaS__Contact_Role__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__Discover__c" {
  const CnP_PaaS__Discover__c:boolean;
  export default CnP_PaaS__Discover__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__Form_CSS__c" {
  const CnP_PaaS__Form_CSS__c:string;
  export default CnP_PaaS__Form_CSS__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__Identification_Number__c" {
  const CnP_PaaS__Identification_Number__c:string;
  export default CnP_PaaS__Identification_Number__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__Invoice_PO__c" {
  const CnP_PaaS__Invoice_PO__c:boolean;
  export default CnP_PaaS__Invoice_PO__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__JCB__c" {
  const CnP_PaaS__JCB__c:boolean;
  export default CnP_PaaS__JCB__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__Master_Card__c" {
  const CnP_PaaS__Master_Card__c:boolean;
  export default CnP_PaaS__Master_Card__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__Organization_Information__c" {
  const CnP_PaaS__Organization_Information__c:string;
  export default CnP_PaaS__Organization_Information__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__Page_Name__c" {
  const CnP_PaaS__Page_Name__c:string;
  export default CnP_PaaS__Page_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__Payment_Declined__c" {
  const CnP_PaaS__Payment_Declined__c:string;
  export default CnP_PaaS__Payment_Declined__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__Purchase_Order__c" {
  const CnP_PaaS__Purchase_Order__c:boolean;
  export default CnP_PaaS__Purchase_Order__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__Receipt_Header__c" {
  const CnP_PaaS__Receipt_Header__c:string;
  export default CnP_PaaS__Receipt_Header__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__Show_terms__c" {
  const CnP_PaaS__Show_terms__c:boolean;
  export default CnP_PaaS__Show_terms__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__Terms_Conditions_Text__c" {
  const CnP_PaaS__Terms_Conditions_Text__c:string;
  export default CnP_PaaS__Terms_Conditions_Text__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__Terms_Conditions__c" {
  const CnP_PaaS__Terms_Conditions__c:string;
  export default CnP_PaaS__Terms_Conditions__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__Thank_you__c" {
  const CnP_PaaS__Thank_you__c:string;
  export default CnP_PaaS__Thank_you__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__Version_Number__c" {
  const CnP_PaaS__Version_Number__c:string;
  export default CnP_PaaS__Version_Number__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__Visa__c" {
  const CnP_PaaS__Visa__c:boolean;
  export default CnP_PaaS__Visa__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__cs_at_aid__c" {
  const CnP_PaaS__cs_at_aid__c:any;
  export default CnP_PaaS__cs_at_aid__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__cs_at_ca__c" {
  const CnP_PaaS__cs_at_ca__c:boolean;
  export default CnP_PaaS__cs_at_ca__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__cs_at_na__c" {
  const CnP_PaaS__cs_at_na__c:boolean;
  export default CnP_PaaS__cs_at_na__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__cs_cr_cc__c" {
  const CnP_PaaS__cs_cr_cc__c:boolean;
  export default CnP_PaaS__cs_cr_cc__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__cs_cr_uc__c" {
  const CnP_PaaS__cs_cr_uc__c:boolean;
  export default CnP_PaaS__cs_cr_uc__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__cs_os_at__c" {
  const CnP_PaaS__cs_os_at__c:string;
  export default CnP_PaaS__cs_os_at__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__cs_os_co__c" {
  const CnP_PaaS__cs_os_co__c:boolean;
  export default CnP_PaaS__cs_os_co__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__cs_os_cp__c" {
  const CnP_PaaS__cs_os_cp__c:boolean;
  export default CnP_PaaS__cs_os_cp__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__cs_os_cr__c" {
  const CnP_PaaS__cs_os_cr__c:boolean;
  export default CnP_PaaS__cs_os_cr__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__cs_os_dt__c" {
  const CnP_PaaS__cs_os_dt__c:string;
  export default CnP_PaaS__cs_os_dt__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__cs_os_fs__c" {
  const CnP_PaaS__cs_os_fs__c:string;
  export default CnP_PaaS__cs_os_fs__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__cs_os_ins__c" {
  const CnP_PaaS__cs_os_ins__c:string;
  export default CnP_PaaS__cs_os_ins__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__cs_os_pos__c" {
  const CnP_PaaS__cs_os_pos__c:string;
  export default CnP_PaaS__cs_os_pos__c;
}
declare module "@salesforce/schema/CnP_PaaS__Embeded_Virtual_Terminal__ChangeEvent.CnP_PaaS__e_Check__c" {
  const CnP_PaaS__e_Check__c:boolean;
  export default CnP_PaaS__e_Check__c;
}
