declare module "@salesforce/schema/CampaignUserSession__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CampaignUserSession__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CampaignUserSession__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CampaignUserSession__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CampaignUserSession__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CampaignUserSession__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CampaignUserSession__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CampaignUserSession__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CampaignUserSession__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CampaignUserSession__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CampaignUserSession__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CampaignUserSession__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CampaignUserSession__ChangeEvent.Community_Campaign__c" {
  const Community_Campaign__c:any;
  export default Community_Campaign__c;
}
declare module "@salesforce/schema/CampaignUserSession__ChangeEvent.Session_Id__c" {
  const Session_Id__c:string;
  export default Session_Id__c;
}
declare module "@salesforce/schema/CampaignUserSession__ChangeEvent.User_Token__c" {
  const User_Token__c:string;
  export default User_Token__c;
}
