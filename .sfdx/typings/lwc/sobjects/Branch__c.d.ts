declare module "@salesforce/schema/Branch__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Branch__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Branch__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Branch__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Branch__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Branch__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Branch__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Branch__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Branch__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Branch__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Branch__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Branch__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Branch__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Branch__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Branch__c.Branch_Name__c" {
  const Branch_Name__c:string;
  export default Branch_Name__c;
}
declare module "@salesforce/schema/Branch__c.Bank_Code__c" {
  const Bank_Code__c:string;
  export default Bank_Code__c;
}
