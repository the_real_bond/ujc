declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Acknowledgement_Mandatory__c" {
  const CnP_PaaS_EVT__Acknowledgement_Mandatory__c:boolean;
  export default CnP_PaaS_EVT__Acknowledgement_Mandatory__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Additional_Donation__c" {
  const CnP_PaaS_EVT__Additional_Donation__c:boolean;
  export default CnP_PaaS_EVT__Additional_Donation__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Additional_Fee__c" {
  const CnP_PaaS_EVT__Additional_Fee__c:string;
  export default CnP_PaaS_EVT__Additional_Fee__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Additonal_Donation_Campaign__c" {
  const CnP_PaaS_EVT__Additonal_Donation_Campaign__c:any;
  export default CnP_PaaS_EVT__Additonal_Donation_Campaign__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Address_1__c" {
  const CnP_PaaS_EVT__Address_1__c:string;
  export default CnP_PaaS_EVT__Address_1__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Address_2__c" {
  const CnP_PaaS_EVT__Address_2__c:string;
  export default CnP_PaaS_EVT__Address_2__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Agenda_Display_Name__c" {
  const CnP_PaaS_EVT__Agenda_Display_Name__c:string;
  export default CnP_PaaS_EVT__Agenda_Display_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__American_express__c" {
  const CnP_PaaS_EVT__American_express__c:boolean;
  export default CnP_PaaS_EVT__American_express__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Anonymous__c" {
  const CnP_PaaS_EVT__Anonymous__c:boolean;
  export default CnP_PaaS_EVT__Anonymous__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Attendee_NameBadge_Email_Body__c" {
  const CnP_PaaS_EVT__Attendee_NameBadge_Email_Body__c:string;
  export default CnP_PaaS_EVT__Attendee_NameBadge_Email_Body__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Available_Inventory__c" {
  const CnP_PaaS_EVT__Available_Inventory__c:number;
  export default CnP_PaaS_EVT__Available_Inventory__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__C_P_Account_Id__c" {
  const CnP_PaaS_EVT__C_P_Account_Id__c:string;
  export default CnP_PaaS_EVT__C_P_Account_Id__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Campaign__c" {
  const CnP_PaaS_EVT__Campaign__c:any;
  export default CnP_PaaS_EVT__Campaign__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Check__c" {
  const CnP_PaaS_EVT__Check__c:boolean;
  export default CnP_PaaS_EVT__Check__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__City__c" {
  const CnP_PaaS_EVT__City__c:string;
  export default CnP_PaaS_EVT__City__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Confirmation_message__c" {
  const CnP_PaaS_EVT__Confirmation_message__c:string;
  export default CnP_PaaS_EVT__Confirmation_message__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Country__c" {
  const CnP_PaaS_EVT__Country__c:string;
  export default CnP_PaaS_EVT__Country__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Credit_card__c" {
  const CnP_PaaS_EVT__Credit_card__c:boolean;
  export default CnP_PaaS_EVT__Credit_card__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Custom_Payment_Check__c" {
  const CnP_PaaS_EVT__Custom_Payment_Check__c:boolean;
  export default CnP_PaaS_EVT__Custom_Payment_Check__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Custom_Payment_Name__c" {
  const CnP_PaaS_EVT__Custom_Payment_Name__c:string;
  export default CnP_PaaS_EVT__Custom_Payment_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Declined__c" {
  const CnP_PaaS_EVT__Declined__c:string;
  export default CnP_PaaS_EVT__Declined__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Discount_Type__c" {
  const CnP_PaaS_EVT__Discount_Type__c:string;
  export default CnP_PaaS_EVT__Discount_Type__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Discover__c" {
  const CnP_PaaS_EVT__Discover__c:boolean;
  export default CnP_PaaS_EVT__Discover__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Display_Address__c" {
  const CnP_PaaS_EVT__Display_Address__c:string;
  export default CnP_PaaS_EVT__Display_Address__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Donation_tax_deductible__c" {
  const CnP_PaaS_EVT__Donation_tax_deductible__c:number;
  export default CnP_PaaS_EVT__Donation_tax_deductible__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__End_date_and_time__c" {
  const CnP_PaaS_EVT__End_date_and_time__c:any;
  export default CnP_PaaS_EVT__End_date_and_time__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Event_Ended__c" {
  const CnP_PaaS_EVT__Event_Ended__c:string;
  export default CnP_PaaS_EVT__Event_Ended__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Event_Site_2v__c" {
  const CnP_PaaS_EVT__Event_Site_2v__c:string;
  export default CnP_PaaS_EVT__Event_Site_2v__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Event_description__c" {
  const CnP_PaaS_EVT__Event_description__c:string;
  export default CnP_PaaS_EVT__Event_description__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Event_listing__c" {
  const CnP_PaaS_EVT__Event_listing__c:any;
  export default CnP_PaaS_EVT__Event_listing__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Event_title__c" {
  const CnP_PaaS_EVT__Event_title__c:string;
  export default CnP_PaaS_EVT__Event_title__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Hide_First_Page_of_Registration__c" {
  const CnP_PaaS_EVT__Hide_First_Page_of_Registration__c:boolean;
  export default CnP_PaaS_EVT__Hide_First_Page_of_Registration__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Hide_timer__c" {
  const CnP_PaaS_EVT__Hide_timer__c:boolean;
  export default CnP_PaaS_EVT__Hide_timer__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Internal_Notification__c" {
  const CnP_PaaS_EVT__Internal_Notification__c:string;
  export default CnP_PaaS_EVT__Internal_Notification__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Inventory_sold__c" {
  const CnP_PaaS_EVT__Inventory_sold__c:number;
  export default CnP_PaaS_EVT__Inventory_sold__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Invoice_P_O__c" {
  const CnP_PaaS_EVT__Invoice_P_O__c:boolean;
  export default CnP_PaaS_EVT__Invoice_P_O__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Label_for_additional_donation__c" {
  const CnP_PaaS_EVT__Label_for_additional_donation__c:string;
  export default CnP_PaaS_EVT__Label_for_additional_donation__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Maximum_capacity__c" {
  const CnP_PaaS_EVT__Maximum_capacity__c:string;
  export default CnP_PaaS_EVT__Maximum_capacity__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__NameBadge_Email_Body__c" {
  const CnP_PaaS_EVT__NameBadge_Email_Body__c:string;
  export default CnP_PaaS_EVT__NameBadge_Email_Body__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Name_Badge_Email_FromName__c" {
  const CnP_PaaS_EVT__Name_Badge_Email_FromName__c:string;
  export default CnP_PaaS_EVT__Name_Badge_Email_FromName__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Name_Badge_Email_ReplyTo__c" {
  const CnP_PaaS_EVT__Name_Badge_Email_ReplyTo__c:string;
  export default CnP_PaaS_EVT__Name_Badge_Email_ReplyTo__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Organization_Information__c" {
  const CnP_PaaS_EVT__Organization_Information__c:string;
  export default CnP_PaaS_EVT__Organization_Information__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Organization__c" {
  const CnP_PaaS_EVT__Organization__c:string;
  export default CnP_PaaS_EVT__Organization__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Pdf_Name__c" {
  const CnP_PaaS_EVT__Pdf_Name__c:string;
  export default CnP_PaaS_EVT__Pdf_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Price__c" {
  const CnP_PaaS_EVT__Price__c:string;
  export default CnP_PaaS_EVT__Price__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Public_site__c" {
  const CnP_PaaS_EVT__Public_site__c:string;
  export default CnP_PaaS_EVT__Public_site__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Publish_date__c" {
  const CnP_PaaS_EVT__Publish_date__c:any;
  export default CnP_PaaS_EVT__Publish_date__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Purchase_order__c" {
  const CnP_PaaS_EVT__Purchase_order__c:boolean;
  export default CnP_PaaS_EVT__Purchase_order__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Quantity__c" {
  const CnP_PaaS_EVT__Quantity__c:string;
  export default CnP_PaaS_EVT__Quantity__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Receipt_Header__c" {
  const CnP_PaaS_EVT__Receipt_Header__c:string;
  export default CnP_PaaS_EVT__Receipt_Header__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Receipt_Terms_Condition__c" {
  const CnP_PaaS_EVT__Receipt_Terms_Condition__c:string;
  export default CnP_PaaS_EVT__Receipt_Terms_Condition__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Registration_Timeout__c" {
  const CnP_PaaS_EVT__Registration_Timeout__c:string;
  export default CnP_PaaS_EVT__Registration_Timeout__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Registration_Type__c" {
  const CnP_PaaS_EVT__Registration_Type__c:string;
  export default CnP_PaaS_EVT__Registration_Type__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Registration_conformation_message__c" {
  const CnP_PaaS_EVT__Registration_conformation_message__c:string;
  export default CnP_PaaS_EVT__Registration_conformation_message__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Registrations_Included__c" {
  const CnP_PaaS_EVT__Registrations_Included__c:string;
  export default CnP_PaaS_EVT__Registrations_Included__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Report_Color__c" {
  const CnP_PaaS_EVT__Report_Color__c:string;
  export default CnP_PaaS_EVT__Report_Color__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__SKU_for_additional_donation__c" {
  const CnP_PaaS_EVT__SKU_for_additional_donation__c:string;
  export default CnP_PaaS_EVT__SKU_for_additional_donation__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Select_Template__c" {
  const CnP_PaaS_EVT__Select_Template__c:any;
  export default CnP_PaaS_EVT__Select_Template__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Send_Agenda__c" {
  const CnP_PaaS_EVT__Send_Agenda__c:boolean;
  export default CnP_PaaS_EVT__Send_Agenda__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Send_Receipt__c" {
  const CnP_PaaS_EVT__Send_Receipt__c:boolean;
  export default CnP_PaaS_EVT__Send_Receipt__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Show_Terms_Conditions__c" {
  const CnP_PaaS_EVT__Show_Terms_Conditions__c:boolean;
  export default CnP_PaaS_EVT__Show_Terms_Conditions__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Site_Url__c" {
  const CnP_PaaS_EVT__Site_Url__c:string;
  export default CnP_PaaS_EVT__Site_Url__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Sold_Out__c" {
  const CnP_PaaS_EVT__Sold_Out__c:string;
  export default CnP_PaaS_EVT__Sold_Out__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Start_date_and_time__c" {
  const CnP_PaaS_EVT__Start_date_and_time__c:any;
  export default CnP_PaaS_EVT__Start_date_and_time__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__State__c" {
  const CnP_PaaS_EVT__State__c:string;
  export default CnP_PaaS_EVT__State__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Terms_Conditions__c" {
  const CnP_PaaS_EVT__Terms_Conditions__c:string;
  export default CnP_PaaS_EVT__Terms_Conditions__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Thank_You_Message_Receipt__c" {
  const CnP_PaaS_EVT__Thank_You_Message_Receipt__c:string;
  export default CnP_PaaS_EVT__Thank_You_Message_Receipt__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Thank_You__c" {
  const CnP_PaaS_EVT__Thank_You__c:string;
  export default CnP_PaaS_EVT__Thank_You__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Ticket_Count_Number__c" {
  const CnP_PaaS_EVT__Ticket_Count_Number__c:number;
  export default CnP_PaaS_EVT__Ticket_Count_Number__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Ticket_Email_Body__c" {
  const CnP_PaaS_EVT__Ticket_Email_Body__c:string;
  export default CnP_PaaS_EVT__Ticket_Email_Body__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Ticket_Email_Subject__c" {
  const CnP_PaaS_EVT__Ticket_Email_Subject__c:string;
  export default CnP_PaaS_EVT__Ticket_Email_Subject__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Ticket_Increment_Number__c" {
  const CnP_PaaS_EVT__Ticket_Increment_Number__c:number;
  export default CnP_PaaS_EVT__Ticket_Increment_Number__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Ticket_Start_Number__c" {
  const CnP_PaaS_EVT__Ticket_Start_Number__c:number;
  export default CnP_PaaS_EVT__Ticket_Start_Number__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Ticket_email_ReplyTo__c" {
  const CnP_PaaS_EVT__Ticket_email_ReplyTo__c:string;
  export default CnP_PaaS_EVT__Ticket_email_ReplyTo__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Ticket_email_fromname__c" {
  const CnP_PaaS_EVT__Ticket_email_fromname__c:string;
  export default CnP_PaaS_EVT__Ticket_email_fromname__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Tickets_widget__c" {
  const CnP_PaaS_EVT__Tickets_widget__c:string;
  export default CnP_PaaS_EVT__Tickets_widget__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Total_Inventory__c" {
  const CnP_PaaS_EVT__Total_Inventory__c:number;
  export default CnP_PaaS_EVT__Total_Inventory__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Total__c" {
  const CnP_PaaS_EVT__Total__c:string;
  export default CnP_PaaS_EVT__Total__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Venue_Name__c" {
  const CnP_PaaS_EVT__Venue_Name__c:string;
  export default CnP_PaaS_EVT__Venue_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Visa__c" {
  const CnP_PaaS_EVT__Visa__c:boolean;
  export default CnP_PaaS_EVT__Visa__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__Zip__c" {
  const CnP_PaaS_EVT__Zip__c:string;
  export default CnP_PaaS_EVT__Zip__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__category__c" {
  const CnP_PaaS_EVT__category__c:any;
  export default CnP_PaaS_EVT__category__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__eTicket_Email_Body__c" {
  const CnP_PaaS_EVT__eTicket_Email_Body__c:string;
  export default CnP_PaaS_EVT__eTicket_Email_Body__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__e_check__c" {
  const CnP_PaaS_EVT__e_check__c:boolean;
  export default CnP_PaaS_EVT__e_check__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__event_coordinator__c" {
  const CnP_PaaS_EVT__event_coordinator__c:any;
  export default CnP_PaaS_EVT__event_coordinator__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__iFrame_Code_2__c" {
  const CnP_PaaS_EVT__iFrame_Code_2__c:string;
  export default CnP_PaaS_EVT__iFrame_Code_2__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__iFrame_Code__c" {
  const CnP_PaaS_EVT__iFrame_Code__c:string;
  export default CnP_PaaS_EVT__iFrame_Code__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__master_card__c" {
  const CnP_PaaS_EVT__master_card__c:boolean;
  export default CnP_PaaS_EVT__master_card__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event__ChangeEvent.CnP_PaaS_EVT__name_Badge_Email_Subject__c" {
  const CnP_PaaS_EVT__name_Badge_Email_Subject__c:string;
  export default CnP_PaaS_EVT__name_Badge_Email_Subject__c;
}
