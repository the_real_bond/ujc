declare module "@salesforce/schema/APXTConga4__Conga_Collection_Solution__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/APXTConga4__Conga_Collection_Solution__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/APXTConga4__Conga_Collection_Solution__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/APXTConga4__Conga_Collection_Solution__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/APXTConga4__Conga_Collection_Solution__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/APXTConga4__Conga_Collection_Solution__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/APXTConga4__Conga_Collection_Solution__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/APXTConga4__Conga_Collection_Solution__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/APXTConga4__Conga_Collection_Solution__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/APXTConga4__Conga_Collection_Solution__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/APXTConga4__Conga_Collection_Solution__c.APXTConga4__Conga_Collection__r" {
  const APXTConga4__Conga_Collection__r:any;
  export default APXTConga4__Conga_Collection__r;
}
declare module "@salesforce/schema/APXTConga4__Conga_Collection_Solution__c.APXTConga4__Conga_Collection__c" {
  const APXTConga4__Conga_Collection__c:any;
  export default APXTConga4__Conga_Collection__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Collection_Solution__c.APXTConga4__Conga_Solution__r" {
  const APXTConga4__Conga_Solution__r:any;
  export default APXTConga4__Conga_Solution__r;
}
declare module "@salesforce/schema/APXTConga4__Conga_Collection_Solution__c.APXTConga4__Conga_Solution__c" {
  const APXTConga4__Conga_Solution__c:any;
  export default APXTConga4__Conga_Solution__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Collection_Solution__c.APXTConga4__Description__c" {
  const APXTConga4__Description__c:string;
  export default APXTConga4__Description__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Collection_Solution__c.APXTConga4__Sort_Order__c" {
  const APXTConga4__Sort_Order__c:number;
  export default APXTConga4__Sort_Order__c;
}
