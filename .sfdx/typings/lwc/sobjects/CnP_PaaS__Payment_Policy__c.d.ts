declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.CnP_PaaS__Additional_Payment__c" {
  const CnP_PaaS__Additional_Payment__c:boolean;
  export default CnP_PaaS__Additional_Payment__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.CnP_PaaS__Campaign__r" {
  const CnP_PaaS__Campaign__r:any;
  export default CnP_PaaS__Campaign__r;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.CnP_PaaS__Campaign__c" {
  const CnP_PaaS__Campaign__c:any;
  export default CnP_PaaS__Campaign__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.CnP_PaaS__CnP_Reminder_Subject__c" {
  const CnP_PaaS__CnP_Reminder_Subject__c:string;
  export default CnP_PaaS__CnP_Reminder_Subject__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.CnP_PaaS__Early_Amount__c" {
  const CnP_PaaS__Early_Amount__c:number;
  export default CnP_PaaS__Early_Amount__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.CnP_PaaS__Early_Days__c" {
  const CnP_PaaS__Early_Days__c:string;
  export default CnP_PaaS__Early_Days__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.CnP_PaaS__Early_Discount__c" {
  const CnP_PaaS__Early_Discount__c:number;
  export default CnP_PaaS__Early_Discount__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.CnP_PaaS__Early_Payment__c" {
  const CnP_PaaS__Early_Payment__c:boolean;
  export default CnP_PaaS__Early_Payment__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.CnP_PaaS__Invoice_Due__c" {
  const CnP_PaaS__Invoice_Due__c:string;
  export default CnP_PaaS__Invoice_Due__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.CnP_PaaS__Invoice_Polici__r" {
  const CnP_PaaS__Invoice_Polici__r:any;
  export default CnP_PaaS__Invoice_Polici__r;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.CnP_PaaS__Invoice_Polici__c" {
  const CnP_PaaS__Invoice_Polici__c:any;
  export default CnP_PaaS__Invoice_Polici__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.CnP_PaaS__Late_Amount__c" {
  const CnP_PaaS__Late_Amount__c:number;
  export default CnP_PaaS__Late_Amount__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.CnP_PaaS__Late_Days__c" {
  const CnP_PaaS__Late_Days__c:string;
  export default CnP_PaaS__Late_Days__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.CnP_PaaS__Late_Fee__c" {
  const CnP_PaaS__Late_Fee__c:number;
  export default CnP_PaaS__Late_Fee__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.CnP_PaaS__Late_Payment__c" {
  const CnP_PaaS__Late_Payment__c:boolean;
  export default CnP_PaaS__Late_Payment__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.CnP_PaaS__Late_SKU__c" {
  const CnP_PaaS__Late_SKU__c:string;
  export default CnP_PaaS__Late_SKU__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.CnP_PaaS__Payment_Name__c" {
  const CnP_PaaS__Payment_Name__c:string;
  export default CnP_PaaS__Payment_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.CnP_PaaS__Remaindersent__c" {
  const CnP_PaaS__Remaindersent__c:boolean;
  export default CnP_PaaS__Remaindersent__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.CnP_PaaS__Reminder__c" {
  const CnP_PaaS__Reminder__c:boolean;
  export default CnP_PaaS__Reminder__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.CnP_PaaS__SKU__c" {
  const CnP_PaaS__SKU__c:string;
  export default CnP_PaaS__SKU__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.CnP_PaaS__Send__c" {
  const CnP_PaaS__Send__c:string;
  export default CnP_PaaS__Send__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.CnP_PaaS__Tax_Deductible__c" {
  const CnP_PaaS__Tax_Deductible__c:number;
  export default CnP_PaaS__Tax_Deductible__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.CnP_PaaS__Tax__c" {
  const CnP_PaaS__Tax__c:number;
  export default CnP_PaaS__Tax__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.CnP_PaaS__Template__c" {
  const CnP_PaaS__Template__c:string;
  export default CnP_PaaS__Template__c;
}
declare module "@salesforce/schema/CnP_PaaS__Payment_Policy__c.CnP_PaaS__remainderdate__c" {
  const CnP_PaaS__remainderdate__c:any;
  export default CnP_PaaS__remainderdate__c;
}
