declare module "@salesforce/schema/Specified_Pledges__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Specified_Pledges__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Specified_Pledges__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Specified_Pledges__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Specified_Pledges__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Specified_Pledges__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Specified_Pledges__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Specified_Pledges__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Specified_Pledges__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Specified_Pledges__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Specified_Pledges__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/Specified_Pledges__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Specified_Pledges__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Specified_Pledges__c.SP_Organisation__c" {
  const SP_Organisation__c:string;
  export default SP_Organisation__c;
}
declare module "@salesforce/schema/Specified_Pledges__c.SP_Amount__c" {
  const SP_Amount__c:number;
  export default SP_Amount__c;
}
declare module "@salesforce/schema/Specified_Pledges__c.SP_Donor__c" {
  const SP_Donor__c:string;
  export default SP_Donor__c;
}
declare module "@salesforce/schema/Specified_Pledges__c.ParentPledge__r" {
  const ParentPledge__r:any;
  export default ParentPledge__r;
}
declare module "@salesforce/schema/Specified_Pledges__c.ParentPledge__c" {
  const ParentPledge__c:any;
  export default ParentPledge__c;
}
declare module "@salesforce/schema/Specified_Pledges__c.SP_Date__c" {
  const SP_Date__c:any;
  export default SP_Date__c;
}
declare module "@salesforce/schema/Specified_Pledges__c.SP_Division__c" {
  const SP_Division__c:string;
  export default SP_Division__c;
}
declare module "@salesforce/schema/Specified_Pledges__c.SP_Camp_Year__c" {
  const SP_Camp_Year__c:string;
  export default SP_Camp_Year__c;
}
declare module "@salesforce/schema/Specified_Pledges__c.Date_Paid_Across__c" {
  const Date_Paid_Across__c:any;
  export default Date_Paid_Across__c;
}
declare module "@salesforce/schema/Specified_Pledges__c.SP_Direct__c" {
  const SP_Direct__c:boolean;
  export default SP_Direct__c;
}
declare module "@salesforce/schema/Specified_Pledges__c.Payment_Source__c" {
  const Payment_Source__c:string;
  export default Payment_Source__c;
}
declare module "@salesforce/schema/Specified_Pledges__c.Total_Division_Outstanding__c" {
  const Total_Division_Outstanding__c:number;
  export default Total_Division_Outstanding__c;
}
declare module "@salesforce/schema/Specified_Pledges__c.Total_Division_Pledge__c" {
  const Total_Division_Pledge__c:number;
  export default Total_Division_Pledge__c;
}
declare module "@salesforce/schema/Specified_Pledges__c.SP_Transfered__c" {
  const SP_Transfered__c:boolean;
  export default SP_Transfered__c;
}
declare module "@salesforce/schema/Specified_Pledges__c.SP_Campaign__c" {
  const SP_Campaign__c:string;
  export default SP_Campaign__c;
}
declare module "@salesforce/schema/Specified_Pledges__c.Donor_Email__c" {
  const Donor_Email__c:string;
  export default Donor_Email__c;
}
declare module "@salesforce/schema/Specified_Pledges__c.SP_Beneficiary__r" {
  const SP_Beneficiary__r:any;
  export default SP_Beneficiary__r;
}
declare module "@salesforce/schema/Specified_Pledges__c.SP_Beneficiary__c" {
  const SP_Beneficiary__c:any;
  export default SP_Beneficiary__c;
}
declare module "@salesforce/schema/Specified_Pledges__c.SP_Beneficiary_Campaign__c" {
  const SP_Beneficiary_Campaign__c:string;
  export default SP_Beneficiary_Campaign__c;
}
declare module "@salesforce/schema/Specified_Pledges__c.SP_Pledge_Type__c" {
  const SP_Pledge_Type__c:string;
  export default SP_Pledge_Type__c;
}
