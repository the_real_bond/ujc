declare module "@salesforce/schema/CnP_PaaS__C_P_Designer_Content_Data__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Designer_Content_Data__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Designer_Content_Data__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Designer_Content_Data__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Designer_Content_Data__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Designer_Content_Data__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Designer_Content_Data__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Designer_Content_Data__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Designer_Content_Data__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Designer_Content_Data__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Designer_Content_Data__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Designer_Content_Data__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Designer_Content_Data__c.CnP_PaaS__C_P_Mail_Content_Data__c" {
  const CnP_PaaS__C_P_Mail_Content_Data__c:string;
  export default CnP_PaaS__C_P_Mail_Content_Data__c;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Designer_Content_Data__c.CnP_PaaS__CnP_Designer__r" {
  const CnP_PaaS__CnP_Designer__r:any;
  export default CnP_PaaS__CnP_Designer__r;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Designer_Content_Data__c.CnP_PaaS__CnP_Designer__c" {
  const CnP_PaaS__CnP_Designer__c:any;
  export default CnP_PaaS__CnP_Designer__c;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Designer_Content_Data__c.CnP_PaaS__CnP_Mail_Content_Data__c" {
  const CnP_PaaS__CnP_Mail_Content_Data__c:string;
  export default CnP_PaaS__CnP_Mail_Content_Data__c;
}
declare module "@salesforce/schema/CnP_PaaS__C_P_Designer_Content_Data__c.CnP_PaaS__Order_info__c" {
  const CnP_PaaS__Order_info__c:number;
  export default CnP_PaaS__Order_info__c;
}
