declare module "@salesforce/schema/MC4SF__MC_Interest_Group__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Group__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Group__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Group__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Group__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Group__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Group__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Group__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Group__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Group__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Group__ChangeEvent.MC4SF__MC_Interest_Grouping__c" {
  const MC4SF__MC_Interest_Grouping__c:any;
  export default MC4SF__MC_Interest_Grouping__c;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Group__ChangeEvent.MC4SF__Bit__c" {
  const MC4SF__Bit__c:string;
  export default MC4SF__Bit__c;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Group__ChangeEvent.MC4SF__Deleted_In_MailChimp__c" {
  const MC4SF__Deleted_In_MailChimp__c:boolean;
  export default MC4SF__Deleted_In_MailChimp__c;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Group__ChangeEvent.MC4SF__Display_Order__c" {
  const MC4SF__Display_Order__c:string;
  export default MC4SF__Display_Order__c;
}
declare module "@salesforce/schema/MC4SF__MC_Interest_Group__ChangeEvent.MC4SF__Subscribers__c" {
  const MC4SF__Subscribers__c:number;
  export default MC4SF__Subscribers__c;
}
