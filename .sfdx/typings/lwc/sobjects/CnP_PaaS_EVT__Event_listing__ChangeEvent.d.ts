declare module "@salesforce/schema/CnP_PaaS_EVT__Event_listing__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_listing__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_listing__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_listing__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_listing__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_listing__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_listing__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_listing__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_listing__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_listing__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_listing__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_listing__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_listing__ChangeEvent.CnP_PaaS_EVT__C_P_Designer__c" {
  const CnP_PaaS_EVT__C_P_Designer__c:any;
  export default CnP_PaaS_EVT__C_P_Designer__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_listing__ChangeEvent.CnP_PaaS_EVT__Font_family__c" {
  const CnP_PaaS_EVT__Font_family__c:string;
  export default CnP_PaaS_EVT__Font_family__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_listing__ChangeEvent.CnP_PaaS_EVT__Font_size__c" {
  const CnP_PaaS_EVT__Font_size__c:string;
  export default CnP_PaaS_EVT__Font_size__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_listing__ChangeEvent.CnP_PaaS_EVT__Footer_Text__c" {
  const CnP_PaaS_EVT__Footer_Text__c:string;
  export default CnP_PaaS_EVT__Footer_Text__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_listing__ChangeEvent.CnP_PaaS_EVT__Footer_background__c" {
  const CnP_PaaS_EVT__Footer_background__c:string;
  export default CnP_PaaS_EVT__Footer_background__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_listing__ChangeEvent.CnP_PaaS_EVT__Footer_information__c" {
  const CnP_PaaS_EVT__Footer_information__c:string;
  export default CnP_PaaS_EVT__Footer_information__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_listing__ChangeEvent.CnP_PaaS_EVT__Iframe_2v__c" {
  const CnP_PaaS_EVT__Iframe_2v__c:string;
  export default CnP_PaaS_EVT__Iframe_2v__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_listing__ChangeEvent.CnP_PaaS_EVT__Logo_width__c" {
  const CnP_PaaS_EVT__Logo_width__c:string;
  export default CnP_PaaS_EVT__Logo_width__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_listing__ChangeEvent.CnP_PaaS_EVT__Page_background__c" {
  const CnP_PaaS_EVT__Page_background__c:string;
  export default CnP_PaaS_EVT__Page_background__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_listing__ChangeEvent.CnP_PaaS_EVT__Page_header__c" {
  const CnP_PaaS_EVT__Page_header__c:string;
  export default CnP_PaaS_EVT__Page_header__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_listing__ChangeEvent.CnP_PaaS_EVT__Public_Site_Url__c" {
  const CnP_PaaS_EVT__Public_Site_Url__c:string;
  export default CnP_PaaS_EVT__Public_Site_Url__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_listing__ChangeEvent.CnP_PaaS_EVT__Public_site__c" {
  const CnP_PaaS_EVT__Public_site__c:string;
  export default CnP_PaaS_EVT__Public_site__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_listing__ChangeEvent.CnP_PaaS_EVT__Section_Headers_background__c" {
  const CnP_PaaS_EVT__Section_Headers_background__c:string;
  export default CnP_PaaS_EVT__Section_Headers_background__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_listing__ChangeEvent.CnP_PaaS_EVT__Section_description__c" {
  const CnP_PaaS_EVT__Section_description__c:string;
  export default CnP_PaaS_EVT__Section_description__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_listing__ChangeEvent.CnP_PaaS_EVT__Section_header_title__c" {
  const CnP_PaaS_EVT__Section_header_title__c:string;
  export default CnP_PaaS_EVT__Section_header_title__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_listing__ChangeEvent.CnP_PaaS_EVT__Section_headers_text__c" {
  const CnP_PaaS_EVT__Section_headers_text__c:string;
  export default CnP_PaaS_EVT__Section_headers_text__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_listing__ChangeEvent.CnP_PaaS_EVT__Section_titleheader_background__c" {
  const CnP_PaaS_EVT__Section_titleheader_background__c:string;
  export default CnP_PaaS_EVT__Section_titleheader_background__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_listing__ChangeEvent.CnP_PaaS_EVT__Title_and_information__c" {
  const CnP_PaaS_EVT__Title_and_information__c:string;
  export default CnP_PaaS_EVT__Title_and_information__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_listing__ChangeEvent.CnP_PaaS_EVT__Upload_background_image__c" {
  const CnP_PaaS_EVT__Upload_background_image__c:string;
  export default CnP_PaaS_EVT__Upload_background_image__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_listing__ChangeEvent.CnP_PaaS_EVT__Upload_banner__c" {
  const CnP_PaaS_EVT__Upload_banner__c:string;
  export default CnP_PaaS_EVT__Upload_banner__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_listing__ChangeEvent.CnP_PaaS_EVT__iFrame_Code__c" {
  const CnP_PaaS_EVT__iFrame_Code__c:string;
  export default CnP_PaaS_EVT__iFrame_Code__c;
}
