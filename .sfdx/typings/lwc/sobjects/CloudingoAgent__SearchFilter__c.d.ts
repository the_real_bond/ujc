declare module "@salesforce/schema/CloudingoAgent__SearchFilter__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CloudingoAgent__SearchFilter__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CloudingoAgent__SearchFilter__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CloudingoAgent__SearchFilter__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CloudingoAgent__SearchFilter__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CloudingoAgent__SearchFilter__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CloudingoAgent__SearchFilter__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CloudingoAgent__SearchFilter__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CloudingoAgent__SearchFilter__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CloudingoAgent__SearchFilter__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CloudingoAgent__SearchFilter__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CloudingoAgent__SearchFilter__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CloudingoAgent__SearchFilter__c.CloudingoAgent__Content__c" {
  const CloudingoAgent__Content__c:string;
  export default CloudingoAgent__Content__c;
}
declare module "@salesforce/schema/CloudingoAgent__SearchFilter__c.CloudingoAgent__EXID__c" {
  const CloudingoAgent__EXID__c:string;
  export default CloudingoAgent__EXID__c;
}
declare module "@salesforce/schema/CloudingoAgent__SearchFilter__c.CloudingoAgent__Version__c" {
  const CloudingoAgent__Version__c:string;
  export default CloudingoAgent__Version__c;
}
