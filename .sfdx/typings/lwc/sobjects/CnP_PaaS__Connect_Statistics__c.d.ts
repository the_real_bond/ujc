declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CnP_PaaS__Contact__r" {
  const CnP_PaaS__Contact__r:any;
  export default CnP_PaaS__Contact__r;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CnP_PaaS__Contact__c" {
  const CnP_PaaS__Contact__c:any;
  export default CnP_PaaS__Contact__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CnP_PaaS__Donor_PD_Amount_Rank_Factor__c" {
  const CnP_PaaS__Donor_PD_Amount_Rank_Factor__c:number;
  export default CnP_PaaS__Donor_PD_Amount_Rank_Factor__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CnP_PaaS__Donor_PD_Count_Rank_Factor__c" {
  const CnP_PaaS__Donor_PD_Count_Rank_Factor__c:number;
  export default CnP_PaaS__Donor_PD_Count_Rank_Factor__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CnP_PaaS__Donor_RD_Amount_Rank_Factor__c" {
  const CnP_PaaS__Donor_RD_Amount_Rank_Factor__c:number;
  export default CnP_PaaS__Donor_RD_Amount_Rank_Factor__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CnP_PaaS__Donor_RD_Count_Rank_Factor__c" {
  const CnP_PaaS__Donor_RD_Count_Rank_Factor__c:number;
  export default CnP_PaaS__Donor_RD_Count_Rank_Factor__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CnP_PaaS__Donor_Rank_Factor__c" {
  const CnP_PaaS__Donor_Rank_Factor__c:number;
  export default CnP_PaaS__Donor_Rank_Factor__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CnP_PaaS__Global_Rank_Factor__c" {
  const CnP_PaaS__Global_Rank_Factor__c:number;
  export default CnP_PaaS__Global_Rank_Factor__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CnP_PaaS__Global_Rank__c" {
  const CnP_PaaS__Global_Rank__c:number;
  export default CnP_PaaS__Global_Rank__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CnP_PaaS__My_Rank_Factor__c" {
  const CnP_PaaS__My_Rank_Factor__c:number;
  export default CnP_PaaS__My_Rank_Factor__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CnP_PaaS__PD_Amount_Rank_Factor__c" {
  const CnP_PaaS__PD_Amount_Rank_Factor__c:number;
  export default CnP_PaaS__PD_Amount_Rank_Factor__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CnP_PaaS__PD_Count_Rank_Factor__c" {
  const CnP_PaaS__PD_Count_Rank_Factor__c:number;
  export default CnP_PaaS__PD_Count_Rank_Factor__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CnP_PaaS__Personal_Donations_Amount_Donors__c" {
  const CnP_PaaS__Personal_Donations_Amount_Donors__c:number;
  export default CnP_PaaS__Personal_Donations_Amount_Donors__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CnP_PaaS__Personal_Donations_Amount_Rank_Donors__c" {
  const CnP_PaaS__Personal_Donations_Amount_Rank_Donors__c:number;
  export default CnP_PaaS__Personal_Donations_Amount_Rank_Donors__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CnP_PaaS__Personal_Donations_Amount_Rank__c" {
  const CnP_PaaS__Personal_Donations_Amount_Rank__c:number;
  export default CnP_PaaS__Personal_Donations_Amount_Rank__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CnP_PaaS__Personal_Donations_Amount__c" {
  const CnP_PaaS__Personal_Donations_Amount__c:number;
  export default CnP_PaaS__Personal_Donations_Amount__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CnP_PaaS__Personal_Donations_Count_Donors__c" {
  const CnP_PaaS__Personal_Donations_Count_Donors__c:number;
  export default CnP_PaaS__Personal_Donations_Count_Donors__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CnP_PaaS__Personal_Donations_Count_Rank_Donors__c" {
  const CnP_PaaS__Personal_Donations_Count_Rank_Donors__c:number;
  export default CnP_PaaS__Personal_Donations_Count_Rank_Donors__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CnP_PaaS__Personal_Donations_Count_Rank__c" {
  const CnP_PaaS__Personal_Donations_Count_Rank__c:number;
  export default CnP_PaaS__Personal_Donations_Count_Rank__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CnP_PaaS__Personal_Donations_Count__c" {
  const CnP_PaaS__Personal_Donations_Count__c:number;
  export default CnP_PaaS__Personal_Donations_Count__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CnP_PaaS__RD_Amount_Rank_Factor__c" {
  const CnP_PaaS__RD_Amount_Rank_Factor__c:number;
  export default CnP_PaaS__RD_Amount_Rank_Factor__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CnP_PaaS__RD_Count_Rank_Factor__c" {
  const CnP_PaaS__RD_Count_Rank_Factor__c:number;
  export default CnP_PaaS__RD_Count_Rank_Factor__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CnP_PaaS__Raised_Donations_Amount_Donors__c" {
  const CnP_PaaS__Raised_Donations_Amount_Donors__c:number;
  export default CnP_PaaS__Raised_Donations_Amount_Donors__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CnP_PaaS__Raised_Donations_Amount_Rank_Donors__c" {
  const CnP_PaaS__Raised_Donations_Amount_Rank_Donors__c:number;
  export default CnP_PaaS__Raised_Donations_Amount_Rank_Donors__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CnP_PaaS__Raised_Donations_Amount_Rank__c" {
  const CnP_PaaS__Raised_Donations_Amount_Rank__c:number;
  export default CnP_PaaS__Raised_Donations_Amount_Rank__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CnP_PaaS__Raised_Donations_Amount__c" {
  const CnP_PaaS__Raised_Donations_Amount__c:number;
  export default CnP_PaaS__Raised_Donations_Amount__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CnP_PaaS__Raised_Donations_Count_Donors__c" {
  const CnP_PaaS__Raised_Donations_Count_Donors__c:number;
  export default CnP_PaaS__Raised_Donations_Count_Donors__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CnP_PaaS__Raised_Donations_Count_Rank_Donors__c" {
  const CnP_PaaS__Raised_Donations_Count_Rank_Donors__c:number;
  export default CnP_PaaS__Raised_Donations_Count_Rank_Donors__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CnP_PaaS__Raised_Donations_Count_Rank__c" {
  const CnP_PaaS__Raised_Donations_Count_Rank__c:number;
  export default CnP_PaaS__Raised_Donations_Count_Rank__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CnP_PaaS__Raised_Donations_Count__c" {
  const CnP_PaaS__Raised_Donations_Count__c:number;
  export default CnP_PaaS__Raised_Donations_Count__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CnP_PaaS__Total_Intrinsic_Value__c" {
  const CnP_PaaS__Total_Intrinsic_Value__c:number;
  export default CnP_PaaS__Total_Intrinsic_Value__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CnP_PaaS__Total_My_Donors_Rank__c" {
  const CnP_PaaS__Total_My_Donors_Rank__c:number;
  export default CnP_PaaS__Total_My_Donors_Rank__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CnP_PaaS__Total_extrinsic_value__c" {
  const CnP_PaaS__Total_extrinsic_value__c:number;
  export default CnP_PaaS__Total_extrinsic_value__c;
}
declare module "@salesforce/schema/CnP_PaaS__Connect_Statistics__c.CnP_PaaS__Total_rank__c" {
  const CnP_PaaS__Total_rank__c:number;
  export default CnP_PaaS__Total_rank__c;
}
