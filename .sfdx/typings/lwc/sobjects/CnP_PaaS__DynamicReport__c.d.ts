declare module "@salesforce/schema/CnP_PaaS__DynamicReport__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__DynamicReport__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__DynamicReport__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__DynamicReport__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__DynamicReport__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__DynamicReport__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__DynamicReport__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__DynamicReport__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__DynamicReport__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__DynamicReport__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__DynamicReport__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__DynamicReport__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__DynamicReport__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/CnP_PaaS__DynamicReport__c.CnP_PaaS__PageName__c" {
  const CnP_PaaS__PageName__c:string;
  export default CnP_PaaS__PageName__c;
}
declare module "@salesforce/schema/CnP_PaaS__DynamicReport__c.CnP_PaaS__Report_Name__c" {
  const CnP_PaaS__Report_Name__c:string;
  export default CnP_PaaS__Report_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__DynamicReport__c.CnP_PaaS__application_name__c" {
  const CnP_PaaS__application_name__c:string;
  export default CnP_PaaS__application_name__c;
}
declare module "@salesforce/schema/CnP_PaaS__DynamicReport__c.CnP_PaaS__query__c" {
  const CnP_PaaS__query__c:string;
  export default CnP_PaaS__query__c;
}
