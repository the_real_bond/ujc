declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__ChangeEvent.CnP_PaaS__Amount__c" {
  const CnP_PaaS__Amount__c:number;
  export default CnP_PaaS__Amount__c;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__ChangeEvent.CnP_PaaS__C_P_Data__c" {
  const CnP_PaaS__C_P_Data__c:any;
  export default CnP_PaaS__C_P_Data__c;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__ChangeEvent.CnP_PaaS__Email__c" {
  const CnP_PaaS__Email__c:string;
  export default CnP_PaaS__Email__c;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__ChangeEvent.CnP_PaaS__First_Name__c" {
  const CnP_PaaS__First_Name__c:string;
  export default CnP_PaaS__First_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__ChangeEvent.CnP_PaaS__Installment__c" {
  const CnP_PaaS__Installment__c:number;
  export default CnP_PaaS__Installment__c;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__ChangeEvent.CnP_PaaS__Last_Name__c" {
  const CnP_PaaS__Last_Name__c:string;
  export default CnP_PaaS__Last_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__ChangeEvent.CnP_PaaS__Map_Contact__c" {
  const CnP_PaaS__Map_Contact__c:any;
  export default CnP_PaaS__Map_Contact__c;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__ChangeEvent.CnP_PaaS__Master_Transaction_Number__c" {
  const CnP_PaaS__Master_Transaction_Number__c:string;
  export default CnP_PaaS__Master_Transaction_Number__c;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__ChangeEvent.CnP_PaaS__OrderNumber__c" {
  const CnP_PaaS__OrderNumber__c:string;
  export default CnP_PaaS__OrderNumber__c;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__ChangeEvent.CnP_PaaS__Recurring__c" {
  const CnP_PaaS__Recurring__c:string;
  export default CnP_PaaS__Recurring__c;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__ChangeEvent.CnP_PaaS__Xmldata__c" {
  const CnP_PaaS__Xmldata__c:string;
  export default CnP_PaaS__Xmldata__c;
}
declare module "@salesforce/schema/CnP_PaaS__Temporary_Contact__ChangeEvent.CnP_PaaS_EVT__Event_ID__c" {
  const CnP_PaaS_EVT__Event_ID__c:any;
  export default CnP_PaaS_EVT__Event_ID__c;
}
