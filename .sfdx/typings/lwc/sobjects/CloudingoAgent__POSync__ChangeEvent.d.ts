declare module "@salesforce/schema/CloudingoAgent__POSync__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CloudingoAgent__POSync__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CloudingoAgent__POSync__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CloudingoAgent__POSync__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CloudingoAgent__POSync__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CloudingoAgent__POSync__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CloudingoAgent__POSync__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CloudingoAgent__POSync__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CloudingoAgent__POSync__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CloudingoAgent__POSync__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CloudingoAgent__POSync__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CloudingoAgent__POSync__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CloudingoAgent__POSync__ChangeEvent.CloudingoAgent__BID__c" {
  const CloudingoAgent__BID__c:string;
  export default CloudingoAgent__BID__c;
}
declare module "@salesforce/schema/CloudingoAgent__POSync__ChangeEvent.CloudingoAgent__FR__c" {
  const CloudingoAgent__FR__c:boolean;
  export default CloudingoAgent__FR__c;
}
declare module "@salesforce/schema/CloudingoAgent__POSync__ChangeEvent.CloudingoAgent__LastSyncDate__c" {
  const CloudingoAgent__LastSyncDate__c:any;
  export default CloudingoAgent__LastSyncDate__c;
}
declare module "@salesforce/schema/CloudingoAgent__POSync__ChangeEvent.CloudingoAgent__PO__c" {
  const CloudingoAgent__PO__c:any;
  export default CloudingoAgent__PO__c;
}
declare module "@salesforce/schema/CloudingoAgent__POSync__ChangeEvent.CloudingoAgent__RK__c" {
  const CloudingoAgent__RK__c:string;
  export default CloudingoAgent__RK__c;
}
declare module "@salesforce/schema/CloudingoAgent__POSync__ChangeEvent.CloudingoAgent__ST__c" {
  const CloudingoAgent__ST__c:string;
  export default CloudingoAgent__ST__c;
}
