declare module "@salesforce/schema/Salesforce_Check_List__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Salesforce_Check_List__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/Salesforce_Check_List__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/Salesforce_Check_List__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Salesforce_Check_List__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Salesforce_Check_List__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Salesforce_Check_List__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Salesforce_Check_List__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Salesforce_Check_List__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Salesforce_Check_List__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Salesforce_Check_List__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Salesforce_Check_List__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Salesforce_Check_List__ChangeEvent.Issue_Group__c" {
  const Issue_Group__c:string;
  export default Issue_Group__c;
}
declare module "@salesforce/schema/Salesforce_Check_List__ChangeEvent.Status__c" {
  const Status__c:string;
  export default Status__c;
}
declare module "@salesforce/schema/Salesforce_Check_List__ChangeEvent.Assigned_Consultant__c" {
  const Assigned_Consultant__c:string;
  export default Assigned_Consultant__c;
}
declare module "@salesforce/schema/Salesforce_Check_List__ChangeEvent.Minimum_Estimate_Hours__c" {
  const Minimum_Estimate_Hours__c:number;
  export default Minimum_Estimate_Hours__c;
}
declare module "@salesforce/schema/Salesforce_Check_List__ChangeEvent.Maximum_Estimate_Hours__c" {
  const Maximum_Estimate_Hours__c:number;
  export default Maximum_Estimate_Hours__c;
}
declare module "@salesforce/schema/Salesforce_Check_List__ChangeEvent.Actual_Hours__c" {
  const Actual_Hours__c:number;
  export default Actual_Hours__c;
}
declare module "@salesforce/schema/Salesforce_Check_List__ChangeEvent.Project_Description__c" {
  const Project_Description__c:string;
  export default Project_Description__c;
}
declare module "@salesforce/schema/Salesforce_Check_List__ChangeEvent.Project_Detail__c" {
  const Project_Detail__c:string;
  export default Project_Detail__c;
}
declare module "@salesforce/schema/Salesforce_Check_List__ChangeEvent.Priority__c" {
  const Priority__c:string;
  export default Priority__c;
}
