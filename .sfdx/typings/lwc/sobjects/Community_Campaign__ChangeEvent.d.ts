declare module "@salesforce/schema/Community_Campaign__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Community_Campaign__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/Community_Campaign__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/Community_Campaign__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Community_Campaign__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Community_Campaign__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Community_Campaign__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Community_Campaign__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Community_Campaign__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Community_Campaign__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Community_Campaign__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Community_Campaign__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Community_Campaign__ChangeEvent.Current_Active_Campaign__c" {
  const Current_Active_Campaign__c:boolean;
  export default Current_Active_Campaign__c;
}
declare module "@salesforce/schema/Community_Campaign__ChangeEvent.End_Date__c" {
  const End_Date__c:any;
  export default End_Date__c;
}
declare module "@salesforce/schema/Community_Campaign__ChangeEvent.Min_Number_of_Votes__c" {
  const Min_Number_of_Votes__c:number;
  export default Min_Number_of_Votes__c;
}
declare module "@salesforce/schema/Community_Campaign__ChangeEvent.No_of_Votes_per_Candidate__c" {
  const No_of_Votes_per_Candidate__c:number;
  export default No_of_Votes_per_Candidate__c;
}
declare module "@salesforce/schema/Community_Campaign__ChangeEvent.Start_Date__c" {
  const Start_Date__c:any;
  export default Start_Date__c;
}
