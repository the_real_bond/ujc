declare module "@salesforce/schema/CnP_PaaS__Ledger__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Ledger__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__Ledger__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__Ledger__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__Ledger__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__Ledger__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Ledger__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Ledger__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Ledger__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Ledger__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Ledger__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Ledger__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Ledger__ChangeEvent.CnP_PaaS__Account_Name__c" {
  const CnP_PaaS__Account_Name__c:string;
  export default CnP_PaaS__Account_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__Ledger__ChangeEvent.CnP_PaaS__Account_Number__c" {
  const CnP_PaaS__Account_Number__c:string;
  export default CnP_PaaS__Account_Number__c;
}
declare module "@salesforce/schema/CnP_PaaS__Ledger__ChangeEvent.CnP_PaaS__Account_Type__c" {
  const CnP_PaaS__Account_Type__c:string;
  export default CnP_PaaS__Account_Type__c;
}
declare module "@salesforce/schema/CnP_PaaS__Ledger__ChangeEvent.CnP_PaaS__Description__c" {
  const CnP_PaaS__Description__c:string;
  export default CnP_PaaS__Description__c;
}
declare module "@salesforce/schema/CnP_PaaS__Ledger__ChangeEvent.CnP_PaaS__Notes__c" {
  const CnP_PaaS__Notes__c:string;
  export default CnP_PaaS__Notes__c;
}
declare module "@salesforce/schema/CnP_PaaS__Ledger__ChangeEvent.CnP_PaaS__Sub_Account__c" {
  const CnP_PaaS__Sub_Account__c:string;
  export default CnP_PaaS__Sub_Account__c;
}
