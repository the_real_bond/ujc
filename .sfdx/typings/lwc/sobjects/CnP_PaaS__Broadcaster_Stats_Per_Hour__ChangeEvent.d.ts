declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats_Per_Hour__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats_Per_Hour__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats_Per_Hour__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats_Per_Hour__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats_Per_Hour__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats_Per_Hour__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats_Per_Hour__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats_Per_Hour__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats_Per_Hour__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats_Per_Hour__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats_Per_Hour__ChangeEvent.CnP_PaaS__Broadcaster_Campaign__c" {
  const CnP_PaaS__Broadcaster_Campaign__c:any;
  export default CnP_PaaS__Broadcaster_Campaign__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats_Per_Hour__ChangeEvent.CnP_PaaS__Broadcaster_Stats__c" {
  const CnP_PaaS__Broadcaster_Stats__c:any;
  export default CnP_PaaS__Broadcaster_Stats__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats_Per_Hour__ChangeEvent.CnP_PaaS__Emails_Sent__c" {
  const CnP_PaaS__Emails_Sent__c:number;
  export default CnP_PaaS__Emails_Sent__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats_Per_Hour__ChangeEvent.CnP_PaaS__Recipients_Click__c" {
  const CnP_PaaS__Recipients_Click__c:number;
  export default CnP_PaaS__Recipients_Click__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats_Per_Hour__ChangeEvent.CnP_PaaS__Timestamp__c" {
  const CnP_PaaS__Timestamp__c:string;
  export default CnP_PaaS__Timestamp__c;
}
declare module "@salesforce/schema/CnP_PaaS__Broadcaster_Stats_Per_Hour__ChangeEvent.CnP_PaaS__Unique_Opens__c" {
  const CnP_PaaS__Unique_Opens__c:number;
  export default CnP_PaaS__Unique_Opens__c;
}
