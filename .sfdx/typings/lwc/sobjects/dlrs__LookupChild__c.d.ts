declare module "@salesforce/schema/dlrs__LookupChild__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/dlrs__LookupChild__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/dlrs__LookupChild__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/dlrs__LookupChild__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/dlrs__LookupChild__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/dlrs__LookupChild__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/dlrs__LookupChild__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/dlrs__LookupChild__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/dlrs__LookupChild__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/dlrs__LookupChild__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/dlrs__LookupChild__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/dlrs__LookupChild__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/dlrs__LookupChild__c.dlrs__Amount__c" {
  const dlrs__Amount__c:number;
  export default dlrs__Amount__c;
}
declare module "@salesforce/schema/dlrs__LookupChild__c.dlrs__LookupParent__r" {
  const dlrs__LookupParent__r:any;
  export default dlrs__LookupParent__r;
}
declare module "@salesforce/schema/dlrs__LookupChild__c.dlrs__LookupParent__c" {
  const dlrs__LookupParent__c:any;
  export default dlrs__LookupParent__c;
}
declare module "@salesforce/schema/dlrs__LookupChild__c.dlrs__Color__c" {
  const dlrs__Color__c:string;
  export default dlrs__Color__c;
}
declare module "@salesforce/schema/dlrs__LookupChild__c.dlrs__Description2__c" {
  const dlrs__Description2__c:string;
  export default dlrs__Description2__c;
}
declare module "@salesforce/schema/dlrs__LookupChild__c.dlrs__Description__c" {
  const dlrs__Description__c:string;
  export default dlrs__Description__c;
}
declare module "@salesforce/schema/dlrs__LookupChild__c.dlrs__LookupParent2__r" {
  const dlrs__LookupParent2__r:any;
  export default dlrs__LookupParent2__r;
}
declare module "@salesforce/schema/dlrs__LookupChild__c.dlrs__LookupParent2__c" {
  const dlrs__LookupParent2__c:any;
  export default dlrs__LookupParent2__c;
}
