declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.RecordType" {
  const RecordType:any;
  export default RecordType;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.RecordTypeId" {
  const RecordTypeId:any;
  export default RecordTypeId;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__Account_Type__c" {
  const CnP_PaaS__Account_Type__c:string;
  export default CnP_PaaS__Account_Type__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__Account__r" {
  const CnP_PaaS__Account__r:any;
  export default CnP_PaaS__Account__r;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__Account__c" {
  const CnP_PaaS__Account__c:any;
  export default CnP_PaaS__Account__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__Amount__c" {
  const CnP_PaaS__Amount__c:number;
  export default CnP_PaaS__Amount__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__Application_Name__c" {
  const CnP_PaaS__Application_Name__c:string;
  export default CnP_PaaS__Application_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__AutorizationCode__c" {
  const CnP_PaaS__AutorizationCode__c:string;
  export default CnP_PaaS__AutorizationCode__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__C_P_Currency__r" {
  const CnP_PaaS__C_P_Currency__r:any;
  export default CnP_PaaS__C_P_Currency__r;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__C_P_Currency__c" {
  const CnP_PaaS__C_P_Currency__c:any;
  export default CnP_PaaS__C_P_Currency__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__C_P_Recurring__r" {
  const CnP_PaaS__C_P_Recurring__r:any;
  export default CnP_PaaS__C_P_Recurring__r;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__C_P_Recurring__c" {
  const CnP_PaaS__C_P_Recurring__c:any;
  export default CnP_PaaS__C_P_Recurring__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__Campaign__c" {
  const CnP_PaaS__Campaign__c:string;
  export default CnP_PaaS__Campaign__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__CardExpiration__c" {
  const CnP_PaaS__CardExpiration__c:string;
  export default CnP_PaaS__CardExpiration__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__Charge_Amount__c" {
  const CnP_PaaS__Charge_Amount__c:number;
  export default CnP_PaaS__Charge_Amount__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__Charge_Date__c" {
  const CnP_PaaS__Charge_Date__c:any;
  export default CnP_PaaS__Charge_Date__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__CheckOutPageId__c" {
  const CnP_PaaS__CheckOutPageId__c:string;
  export default CnP_PaaS__CheckOutPageId__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__CheckOutPage__c" {
  const CnP_PaaS__CheckOutPage__c:string;
  export default CnP_PaaS__CheckOutPage__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__CheckType__c" {
  const CnP_PaaS__CheckType__c:string;
  export default CnP_PaaS__CheckType__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__Check_Number__c" {
  const CnP_PaaS__Check_Number__c:string;
  export default CnP_PaaS__Check_Number__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__Check_Type__c" {
  const CnP_PaaS__Check_Type__c:string;
  export default CnP_PaaS__Check_Type__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__CnPAccountID__c" {
  const CnP_PaaS__CnPAccountID__c:number;
  export default CnP_PaaS__CnPAccountID__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__CnP_Account_ID__c" {
  const CnP_PaaS__CnP_Account_ID__c:string;
  export default CnP_PaaS__CnP_Account_ID__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__CnP_Fundraiser_Contact__r" {
  const CnP_PaaS__CnP_Fundraiser_Contact__r:any;
  export default CnP_PaaS__CnP_Fundraiser_Contact__r;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__CnP_Fundraiser_Contact__c" {
  const CnP_PaaS__CnP_Fundraiser_Contact__c:any;
  export default CnP_PaaS__CnP_Fundraiser_Contact__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__Contact__r" {
  const CnP_PaaS__Contact__r:any;
  export default CnP_PaaS__Contact__r;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__Contact__c" {
  const CnP_PaaS__Contact__c:any;
  export default CnP_PaaS__Contact__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__CouponCode__c" {
  const CnP_PaaS__CouponCode__c:string;
  export default CnP_PaaS__CouponCode__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__CreditCard4x4__c" {
  const CnP_PaaS__CreditCard4x4__c:string;
  export default CnP_PaaS__CreditCard4x4__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__Credit_Card_Name__c" {
  const CnP_PaaS__Credit_Card_Name__c:string;
  export default CnP_PaaS__Credit_Card_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__CurrencyCode__c" {
  const CnP_PaaS__CurrencyCode__c:number;
  export default CnP_PaaS__CurrencyCode__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__Currency__c" {
  const CnP_PaaS__Currency__c:string;
  export default CnP_PaaS__Currency__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__CustomQuestions__c" {
  const CnP_PaaS__CustomQuestions__c:string;
  export default CnP_PaaS__CustomQuestions__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__Custom_Payment_Type_Name__c" {
  const CnP_PaaS__Custom_Payment_Type_Name__c:string;
  export default CnP_PaaS__Custom_Payment_Type_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__Deductible_Charge__c" {
  const CnP_PaaS__Deductible_Charge__c:number;
  export default CnP_PaaS__Deductible_Charge__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__Deductible_Due__c" {
  const CnP_PaaS__Deductible_Due__c:number;
  export default CnP_PaaS__Deductible_Due__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__Discount_Charge__c" {
  const CnP_PaaS__Discount_Charge__c:number;
  export default CnP_PaaS__Discount_Charge__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__Discount_Due__c" {
  const CnP_PaaS__Discount_Due__c:number;
  export default CnP_PaaS__Discount_Due__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__Donation_Name__c" {
  const CnP_PaaS__Donation_Name__c:string;
  export default CnP_PaaS__Donation_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__Email__c" {
  const CnP_PaaS__Email__c:string;
  export default CnP_PaaS__Email__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__GatewayTransactionNumber__c" {
  const CnP_PaaS__GatewayTransactionNumber__c:string;
  export default CnP_PaaS__GatewayTransactionNumber__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__IdNumber__c" {
  const CnP_PaaS__IdNumber__c:string;
  export default CnP_PaaS__IdNumber__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__IdState__c" {
  const CnP_PaaS__IdState__c:string;
  export default CnP_PaaS__IdState__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__IdType__c" {
  const CnP_PaaS__IdType__c:string;
  export default CnP_PaaS__IdType__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__Invoice_Check_Number__c" {
  const CnP_PaaS__Invoice_Check_Number__c:string;
  export default CnP_PaaS__Invoice_Check_Number__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__Name_On_card__c" {
  const CnP_PaaS__Name_On_card__c:string;
  export default CnP_PaaS__Name_On_card__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__Organization_Name__c" {
  const CnP_PaaS__Organization_Name__c:string;
  export default CnP_PaaS__Organization_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__PaymentType__c" {
  const CnP_PaaS__PaymentType__c:string;
  export default CnP_PaaS__PaymentType__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__Payment_For__c" {
  const CnP_PaaS__Payment_For__c:string;
  export default CnP_PaaS__Payment_For__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__Purchase_Order_Number__c" {
  const CnP_PaaS__Purchase_Order_Number__c:string;
  export default CnP_PaaS__Purchase_Order_Number__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__Quantity__c" {
  const CnP_PaaS__Quantity__c:number;
  export default CnP_PaaS__Quantity__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__Record_Type__c" {
  const CnP_PaaS__Record_Type__c:string;
  export default CnP_PaaS__Record_Type__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__Routing_Number__c" {
  const CnP_PaaS__Routing_Number__c:string;
  export default CnP_PaaS__Routing_Number__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__ShippingMethod__c" {
  const CnP_PaaS__ShippingMethod__c:string;
  export default CnP_PaaS__ShippingMethod__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__Shipping_Cost_Charge__c" {
  const CnP_PaaS__Shipping_Cost_Charge__c:number;
  export default CnP_PaaS__Shipping_Cost_Charge__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__Shipping_Cost_Due__c" {
  const CnP_PaaS__Shipping_Cost_Due__c:number;
  export default CnP_PaaS__Shipping_Cost_Due__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__SurCharge__c" {
  const CnP_PaaS__SurCharge__c:number;
  export default CnP_PaaS__SurCharge__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__TaxAmount_Charge__c" {
  const CnP_PaaS__TaxAmount_Charge__c:number;
  export default CnP_PaaS__TaxAmount_Charge__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__TaxAmount_Due__c" {
  const CnP_PaaS__TaxAmount_Due__c:number;
  export default CnP_PaaS__TaxAmount_Due__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__TotalCharged__c" {
  const CnP_PaaS__TotalCharged__c:number;
  export default CnP_PaaS__TotalCharged__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__TotalDue__c" {
  const CnP_PaaS__TotalDue__c:number;
  export default CnP_PaaS__TotalDue__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__Tracker__c" {
  const CnP_PaaS__Tracker__c:string;
  export default CnP_PaaS__Tracker__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__TransactionDate__c" {
  const CnP_PaaS__TransactionDate__c:any;
  export default CnP_PaaS__TransactionDate__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__TransactionDiscountCharge__c" {
  const CnP_PaaS__TransactionDiscountCharge__c:number;
  export default CnP_PaaS__TransactionDiscountCharge__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__TransactionDiscountDue__c" {
  const CnP_PaaS__TransactionDiscountDue__c:number;
  export default CnP_PaaS__TransactionDiscountDue__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__TransactionTimeZone__c" {
  const CnP_PaaS__TransactionTimeZone__c:any;
  export default CnP_PaaS__TransactionTimeZone__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__Transaction_Result__c" {
  const CnP_PaaS__Transaction_Result__c:string;
  export default CnP_PaaS__Transaction_Result__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__Transaction_Type__c" {
  const CnP_PaaS__Transaction_Type__c:string;
  export default CnP_PaaS__Transaction_Type__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__UrlReferrer__c" {
  const CnP_PaaS__UrlReferrer__c:string;
  export default CnP_PaaS__UrlReferrer__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__VaultGUID__c" {
  const CnP_PaaS__VaultGUID__c:string;
  export default CnP_PaaS__VaultGUID__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__bSource__c" {
  const CnP_PaaS__bSource__c:number;
  export default CnP_PaaS__bSource__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__remarks__c" {
  const CnP_PaaS__remarks__c:string;
  export default CnP_PaaS__remarks__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__sf_Campaign__r" {
  const CnP_PaaS__sf_Campaign__r:any;
  export default CnP_PaaS__sf_Campaign__r;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Transaction__c.CnP_PaaS__sf_Campaign__c" {
  const CnP_PaaS__sf_Campaign__c:any;
  export default CnP_PaaS__sf_Campaign__c;
}
