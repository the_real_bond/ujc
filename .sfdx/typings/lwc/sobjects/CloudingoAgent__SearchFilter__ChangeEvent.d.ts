declare module "@salesforce/schema/CloudingoAgent__SearchFilter__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CloudingoAgent__SearchFilter__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CloudingoAgent__SearchFilter__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CloudingoAgent__SearchFilter__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CloudingoAgent__SearchFilter__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CloudingoAgent__SearchFilter__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CloudingoAgent__SearchFilter__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CloudingoAgent__SearchFilter__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CloudingoAgent__SearchFilter__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CloudingoAgent__SearchFilter__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CloudingoAgent__SearchFilter__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CloudingoAgent__SearchFilter__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CloudingoAgent__SearchFilter__ChangeEvent.CloudingoAgent__Content__c" {
  const CloudingoAgent__Content__c:string;
  export default CloudingoAgent__Content__c;
}
declare module "@salesforce/schema/CloudingoAgent__SearchFilter__ChangeEvent.CloudingoAgent__EXID__c" {
  const CloudingoAgent__EXID__c:string;
  export default CloudingoAgent__EXID__c;
}
declare module "@salesforce/schema/CloudingoAgent__SearchFilter__ChangeEvent.CloudingoAgent__Version__c" {
  const CloudingoAgent__Version__c:string;
  export default CloudingoAgent__Version__c;
}
