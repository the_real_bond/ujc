declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Acknowledgement_mandatory__c" {
  const CnP_PaaS_EVT__Acknowledgement_mandatory__c:boolean;
  export default CnP_PaaS_EVT__Acknowledgement_mandatory__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Action_button_background__c" {
  const CnP_PaaS_EVT__Action_button_background__c:string;
  export default CnP_PaaS_EVT__Action_button_background__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Action_button_text__c" {
  const CnP_PaaS_EVT__Action_button_text__c:string;
  export default CnP_PaaS_EVT__Action_button_text__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Button_Label__c" {
  const CnP_PaaS_EVT__Button_Label__c:string;
  export default CnP_PaaS_EVT__Button_Label__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Button_font_family__c" {
  const CnP_PaaS_EVT__Button_font_family__c:string;
  export default CnP_PaaS_EVT__Button_font_family__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Button_font_size__c" {
  const CnP_PaaS_EVT__Button_font_size__c:string;
  export default CnP_PaaS_EVT__Button_font_size__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Custom_Page_Html__c" {
  const CnP_PaaS_EVT__Custom_Page_Html__c:string;
  export default CnP_PaaS_EVT__Custom_Page_Html__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Display_Banner__c" {
  const CnP_PaaS_EVT__Display_Banner__c:boolean;
  export default CnP_PaaS_EVT__Display_Banner__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Display_Description__c" {
  const CnP_PaaS_EVT__Display_Description__c:boolean;
  export default CnP_PaaS_EVT__Display_Description__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Display_Footer__c" {
  const CnP_PaaS_EVT__Display_Footer__c:boolean;
  export default CnP_PaaS_EVT__Display_Footer__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Display_Title_Information__c" {
  const CnP_PaaS_EVT__Display_Title_Information__c:boolean;
  export default CnP_PaaS_EVT__Display_Title_Information__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Display_Title__c" {
  const CnP_PaaS_EVT__Display_Title__c:boolean;
  export default CnP_PaaS_EVT__Display_Title__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__End_date__c" {
  const CnP_PaaS_EVT__End_date__c:any;
  export default CnP_PaaS_EVT__End_date__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Engine_CSS__c" {
  const CnP_PaaS_EVT__Engine_CSS__c:string;
  export default CnP_PaaS_EVT__Engine_CSS__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Event_name__c" {
  const CnP_PaaS_EVT__Event_name__c:any;
  export default CnP_PaaS_EVT__Event_name__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Font_family__c" {
  const CnP_PaaS_EVT__Font_family__c:string;
  export default CnP_PaaS_EVT__Font_family__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Font_size__c" {
  const CnP_PaaS_EVT__Font_size__c:string;
  export default CnP_PaaS_EVT__Font_size__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Footer_Border_Color__c" {
  const CnP_PaaS_EVT__Footer_Border_Color__c:string;
  export default CnP_PaaS_EVT__Footer_Border_Color__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Footer_Text__c" {
  const CnP_PaaS_EVT__Footer_Text__c:string;
  export default CnP_PaaS_EVT__Footer_Text__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Footer_background__c" {
  const CnP_PaaS_EVT__Footer_background__c:string;
  export default CnP_PaaS_EVT__Footer_background__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Footer_information__c" {
  const CnP_PaaS_EVT__Footer_information__c:string;
  export default CnP_PaaS_EVT__Footer_information__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Logo_width__c" {
  const CnP_PaaS_EVT__Logo_width__c:string;
  export default CnP_PaaS_EVT__Logo_width__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Main_Section_Background_Color__c" {
  const CnP_PaaS_EVT__Main_Section_Background_Color__c:string;
  export default CnP_PaaS_EVT__Main_Section_Background_Color__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Main_Section_Border_Color__c" {
  const CnP_PaaS_EVT__Main_Section_Border_Color__c:string;
  export default CnP_PaaS_EVT__Main_Section_Border_Color__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Page_Section_Border_Color__c" {
  const CnP_PaaS_EVT__Page_Section_Border_Color__c:string;
  export default CnP_PaaS_EVT__Page_Section_Border_Color__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Page__c" {
  const CnP_PaaS_EVT__Page__c:string;
  export default CnP_PaaS_EVT__Page__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Page_background__c" {
  const CnP_PaaS_EVT__Page_background__c:string;
  export default CnP_PaaS_EVT__Page_background__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Page_header__c" {
  const CnP_PaaS_EVT__Page_header__c:string;
  export default CnP_PaaS_EVT__Page_header__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Page_name__c" {
  const CnP_PaaS_EVT__Page_name__c:string;
  export default CnP_PaaS_EVT__Page_name__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Section_Description__c" {
  const CnP_PaaS_EVT__Section_Description__c:string;
  export default CnP_PaaS_EVT__Section_Description__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Section_Headers_background__c" {
  const CnP_PaaS_EVT__Section_Headers_background__c:string;
  export default CnP_PaaS_EVT__Section_Headers_background__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Section_Title__c" {
  const CnP_PaaS_EVT__Section_Title__c:string;
  export default CnP_PaaS_EVT__Section_Title__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Section_font_family__c" {
  const CnP_PaaS_EVT__Section_font_family__c:string;
  export default CnP_PaaS_EVT__Section_font_family__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Section_font_size__c" {
  const CnP_PaaS_EVT__Section_font_size__c:string;
  export default CnP_PaaS_EVT__Section_font_size__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Section_header_title__c" {
  const CnP_PaaS_EVT__Section_header_title__c:string;
  export default CnP_PaaS_EVT__Section_header_title__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Section_headers_text__c" {
  const CnP_PaaS_EVT__Section_headers_text__c:string;
  export default CnP_PaaS_EVT__Section_headers_text__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Section_titleheader_background__c" {
  const CnP_PaaS_EVT__Section_titleheader_background__c:string;
  export default CnP_PaaS_EVT__Section_titleheader_background__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Show_Page_Image__c" {
  const CnP_PaaS_EVT__Show_Page_Image__c:boolean;
  export default CnP_PaaS_EVT__Show_Page_Image__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Show_ajenda__c" {
  const CnP_PaaS_EVT__Show_ajenda__c:boolean;
  export default CnP_PaaS_EVT__Show_ajenda__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Show_terms_conditions__c" {
  const CnP_PaaS_EVT__Show_terms_conditions__c:boolean;
  export default CnP_PaaS_EVT__Show_terms_conditions__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Start_date__c" {
  const CnP_PaaS_EVT__Start_date__c:any;
  export default CnP_PaaS_EVT__Start_date__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Term_Background_color__c" {
  const CnP_PaaS_EVT__Term_Background_color__c:string;
  export default CnP_PaaS_EVT__Term_Background_color__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Terms_conditions__c" {
  const CnP_PaaS_EVT__Terms_conditions__c:string;
  export default CnP_PaaS_EVT__Terms_conditions__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Theme_Selection__c" {
  const CnP_PaaS_EVT__Theme_Selection__c:string;
  export default CnP_PaaS_EVT__Theme_Selection__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Title_Section_Border_Color__c" {
  const CnP_PaaS_EVT__Title_Section_Border_Color__c:string;
  export default CnP_PaaS_EVT__Title_Section_Border_Color__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Title_and_information__c" {
  const CnP_PaaS_EVT__Title_and_information__c:string;
  export default CnP_PaaS_EVT__Title_and_information__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Upload_background_image__c" {
  const CnP_PaaS_EVT__Upload_background_image__c:string;
  export default CnP_PaaS_EVT__Upload_background_image__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Upload_banner__c" {
  const CnP_PaaS_EVT__Upload_banner__c:string;
  export default CnP_PaaS_EVT__Upload_banner__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Layout__ChangeEvent.CnP_PaaS_EVT__Upload_logo__c" {
  const CnP_PaaS_EVT__Upload_logo__c:string;
  export default CnP_PaaS_EVT__Upload_logo__c;
}
