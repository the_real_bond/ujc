declare module "@salesforce/schema/PowerLoader__QueryPanelTemplate__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplate__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplate__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplate__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplate__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplate__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplate__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplate__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplate__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplate__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplate__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplate__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplate__ChangeEvent.PowerLoader__Private__c" {
  const PowerLoader__Private__c:boolean;
  export default PowerLoader__Private__c;
}
declare module "@salesforce/schema/PowerLoader__QueryPanelTemplate__ChangeEvent.PowerLoader__Query_Panel_Name__c" {
  const PowerLoader__Query_Panel_Name__c:string;
  export default PowerLoader__Query_Panel_Name__c;
}
