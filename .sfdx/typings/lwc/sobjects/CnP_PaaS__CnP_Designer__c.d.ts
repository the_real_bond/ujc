declare module "@salesforce/schema/CnP_PaaS__CnP_Designer__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Designer__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Designer__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Designer__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Designer__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Designer__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Designer__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Designer__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Designer__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Designer__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Designer__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Designer__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Designer__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Designer__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Designer__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Designer__c.CnP_PaaS__Design_Selection__c" {
  const CnP_PaaS__Design_Selection__c:string;
  export default CnP_PaaS__Design_Selection__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Designer__c.CnP_PaaS__Library__c" {
  const CnP_PaaS__Library__c:string;
  export default CnP_PaaS__Library__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Designer__c.CnP_PaaS__Pdf__c" {
  const CnP_PaaS__Pdf__c:string;
  export default CnP_PaaS__Pdf__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Designer__c.CnP_PaaS__Pdf_include__c" {
  const CnP_PaaS__Pdf_include__c:boolean;
  export default CnP_PaaS__Pdf_include__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Designer__c.CnP_PaaS__SalesForce_Public_Site_URL__c" {
  const CnP_PaaS__SalesForce_Public_Site_URL__c:string;
  export default CnP_PaaS__SalesForce_Public_Site_URL__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Designer__c.CnP_PaaS__Select_Layout__c" {
  const CnP_PaaS__Select_Layout__c:string;
  export default CnP_PaaS__Select_Layout__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Designer__c.CnP_PaaS__Select_Template_Cat__c" {
  const CnP_PaaS__Select_Template_Cat__c:string;
  export default CnP_PaaS__Select_Template_Cat__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Designer__c.CnP_PaaS__Tags__c" {
  const CnP_PaaS__Tags__c:string;
  export default CnP_PaaS__Tags__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Designer__c.CnP_PaaS__Template_Layout_Name__c" {
  const CnP_PaaS__Template_Layout_Name__c:string;
  export default CnP_PaaS__Template_Layout_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Designer__c.CnP_PaaS_EVT__Event_Custom_Css__c" {
  const CnP_PaaS_EVT__Event_Custom_Css__c:string;
  export default CnP_PaaS_EVT__Event_Custom_Css__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Designer__c.CnP_PaaS_EVT__Event_Designer_Category__c" {
  const CnP_PaaS_EVT__Event_Designer_Category__c:string;
  export default CnP_PaaS_EVT__Event_Designer_Category__c;
}
