declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__c.CnP_PaaS__AuthorizeTransactions__c" {
  const CnP_PaaS__AuthorizeTransactions__c:number;
  export default CnP_PaaS__AuthorizeTransactions__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__c.CnP_PaaS__CnPAID__c" {
  const CnP_PaaS__CnPAID__c:string;
  export default CnP_PaaS__CnPAID__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__c.CnP_PaaS__DeclineTransactions__c" {
  const CnP_PaaS__DeclineTransactions__c:number;
  export default CnP_PaaS__DeclineTransactions__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__c.CnP_PaaS__Errors__c" {
  const CnP_PaaS__Errors__c:number;
  export default CnP_PaaS__Errors__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__c.CnP_PaaS__FileContent__c" {
  const CnP_PaaS__FileContent__c:string;
  export default CnP_PaaS__FileContent__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__c.CnP_PaaS__FileHeader__c" {
  const CnP_PaaS__FileHeader__c:string;
  export default CnP_PaaS__FileHeader__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__c.CnP_PaaS__FileName__c" {
  const CnP_PaaS__FileName__c:string;
  export default CnP_PaaS__FileName__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__c.CnP_PaaS__ProcessingDate__c" {
  const CnP_PaaS__ProcessingDate__c:any;
  export default CnP_PaaS__ProcessingDate__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__c.CnP_PaaS__Upload_Status__c" {
  const CnP_PaaS__Upload_Status__c:string;
  export default CnP_PaaS__Upload_Status__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnPBatchUpload__c.CnP_PaaS__Username__c" {
  const CnP_PaaS__Username__c:string;
  export default CnP_PaaS__Username__c;
}
