declare module "@salesforce/schema/PowerLoader__Page_Layout_Cache__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/PowerLoader__Page_Layout_Cache__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/PowerLoader__Page_Layout_Cache__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/PowerLoader__Page_Layout_Cache__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/PowerLoader__Page_Layout_Cache__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/PowerLoader__Page_Layout_Cache__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/PowerLoader__Page_Layout_Cache__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/PowerLoader__Page_Layout_Cache__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/PowerLoader__Page_Layout_Cache__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/PowerLoader__Page_Layout_Cache__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/PowerLoader__Page_Layout_Cache__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/PowerLoader__Page_Layout_Cache__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/PowerLoader__Page_Layout_Cache__ChangeEvent.PowerLoader__Object_API_Name__c" {
  const PowerLoader__Object_API_Name__c:string;
  export default PowerLoader__Object_API_Name__c;
}
declare module "@salesforce/schema/PowerLoader__Page_Layout_Cache__ChangeEvent.PowerLoader__Record_Type_ID__c" {
  const PowerLoader__Record_Type_ID__c:string;
  export default PowerLoader__Record_Type_ID__c;
}
