declare module "@salesforce/schema/dlrs__LookupRollupSummaryScheduleItems__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryScheduleItems__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryScheduleItems__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryScheduleItems__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryScheduleItems__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryScheduleItems__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryScheduleItems__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryScheduleItems__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryScheduleItems__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryScheduleItems__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryScheduleItems__ChangeEvent.dlrs__LookupRollupSummary__c" {
  const dlrs__LookupRollupSummary__c:any;
  export default dlrs__LookupRollupSummary__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryScheduleItems__ChangeEvent.dlrs__ParentId__c" {
  const dlrs__ParentId__c:string;
  export default dlrs__ParentId__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryScheduleItems__ChangeEvent.dlrs__ParentRecord__c" {
  const dlrs__ParentRecord__c:string;
  export default dlrs__ParentRecord__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryScheduleItems__ChangeEvent.dlrs__QualifiedParentID__c" {
  const dlrs__QualifiedParentID__c:string;
  export default dlrs__QualifiedParentID__c;
}
declare module "@salesforce/schema/dlrs__LookupRollupSummaryScheduleItems__ChangeEvent.dlrs__LookupRollupSummary2__c" {
  const dlrs__LookupRollupSummary2__c:string;
  export default dlrs__LookupRollupSummary2__c;
}
