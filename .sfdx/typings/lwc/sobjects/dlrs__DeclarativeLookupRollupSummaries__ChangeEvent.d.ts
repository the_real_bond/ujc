declare module "@salesforce/schema/dlrs__DeclarativeLookupRollupSummaries__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/dlrs__DeclarativeLookupRollupSummaries__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/dlrs__DeclarativeLookupRollupSummaries__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/dlrs__DeclarativeLookupRollupSummaries__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/dlrs__DeclarativeLookupRollupSummaries__ChangeEvent.SetupOwner" {
  const SetupOwner:any;
  export default SetupOwner;
}
declare module "@salesforce/schema/dlrs__DeclarativeLookupRollupSummaries__ChangeEvent.SetupOwnerId" {
  const SetupOwnerId:any;
  export default SetupOwnerId;
}
declare module "@salesforce/schema/dlrs__DeclarativeLookupRollupSummaries__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/dlrs__DeclarativeLookupRollupSummaries__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/dlrs__DeclarativeLookupRollupSummaries__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/dlrs__DeclarativeLookupRollupSummaries__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/dlrs__DeclarativeLookupRollupSummaries__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/dlrs__DeclarativeLookupRollupSummaries__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/dlrs__DeclarativeLookupRollupSummaries__ChangeEvent.dlrs__CalculateJobScopeSize__c" {
  const dlrs__CalculateJobScopeSize__c:number;
  export default dlrs__CalculateJobScopeSize__c;
}
declare module "@salesforce/schema/dlrs__DeclarativeLookupRollupSummaries__ChangeEvent.dlrs__ScheduledJobScopeSize__c" {
  const dlrs__ScheduledJobScopeSize__c:number;
  export default dlrs__ScheduledJobScopeSize__c;
}
declare module "@salesforce/schema/dlrs__DeclarativeLookupRollupSummaries__ChangeEvent.dlrs__HideManageLookupRollupSummariesInfo__c" {
  const dlrs__HideManageLookupRollupSummariesInfo__c:boolean;
  export default dlrs__HideManageLookupRollupSummariesInfo__c;
}
