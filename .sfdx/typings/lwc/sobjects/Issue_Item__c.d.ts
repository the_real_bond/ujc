declare module "@salesforce/schema/Issue_Item__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Issue_Item__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Issue_Item__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Issue_Item__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Issue_Item__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Issue_Item__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Issue_Item__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Issue_Item__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Issue_Item__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Issue_Item__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Issue_Item__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/Issue_Item__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Issue_Item__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Issue_Item__c.Issue__c" {
  const Issue__c:string;
  export default Issue__c;
}
declare module "@salesforce/schema/Issue_Item__c.Status__c" {
  const Status__c:string;
  export default Status__c;
}
declare module "@salesforce/schema/Issue_Item__c.Salesforce_Check_List__r" {
  const Salesforce_Check_List__r:any;
  export default Salesforce_Check_List__r;
}
declare module "@salesforce/schema/Issue_Item__c.Salesforce_Check_List__c" {
  const Salesforce_Check_List__c:any;
  export default Salesforce_Check_List__c;
}
declare module "@salesforce/schema/Issue_Item__c.Description__c" {
  const Description__c:string;
  export default Description__c;
}
declare module "@salesforce/schema/Issue_Item__c.Project_Detail__c" {
  const Project_Detail__c:string;
  export default Project_Detail__c;
}
