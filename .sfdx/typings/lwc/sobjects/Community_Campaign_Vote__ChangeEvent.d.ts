declare module "@salesforce/schema/Community_Campaign_Vote__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Community_Campaign_Vote__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/Community_Campaign_Vote__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/Community_Campaign_Vote__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Community_Campaign_Vote__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Community_Campaign_Vote__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Community_Campaign_Vote__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Community_Campaign_Vote__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Community_Campaign_Vote__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Community_Campaign_Vote__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Community_Campaign_Vote__ChangeEvent.Community_Campaign__c" {
  const Community_Campaign__c:any;
  export default Community_Campaign__c;
}
declare module "@salesforce/schema/Community_Campaign_Vote__ChangeEvent.Communal_Reference_Number__c" {
  const Communal_Reference_Number__c:string;
  export default Communal_Reference_Number__c;
}
declare module "@salesforce/schema/Community_Campaign_Vote__ChangeEvent.Community_Register__c" {
  const Community_Register__c:any;
  export default Community_Register__c;
}
declare module "@salesforce/schema/Community_Campaign_Vote__ChangeEvent.Email__c" {
  const Email__c:string;
  export default Email__c;
}
declare module "@salesforce/schema/Community_Campaign_Vote__ChangeEvent.First_Name__c" {
  const First_Name__c:string;
  export default First_Name__c;
}
declare module "@salesforce/schema/Community_Campaign_Vote__ChangeEvent.Last_Name__c" {
  const Last_Name__c:string;
  export default Last_Name__c;
}
declare module "@salesforce/schema/Community_Campaign_Vote__ChangeEvent.Send_Email__c" {
  const Send_Email__c:boolean;
  export default Send_Email__c;
}
declare module "@salesforce/schema/Community_Campaign_Vote__ChangeEvent.CellPhone__c" {
  const CellPhone__c:string;
  export default CellPhone__c;
}
declare module "@salesforce/schema/Community_Campaign_Vote__ChangeEvent.Mail_Merge_Addres__c" {
  const Mail_Merge_Addres__c:string;
  export default Mail_Merge_Addres__c;
}
declare module "@salesforce/schema/Community_Campaign_Vote__ChangeEvent.Postal_Code__c" {
  const Postal_Code__c:string;
  export default Postal_Code__c;
}
declare module "@salesforce/schema/Community_Campaign_Vote__ChangeEvent.Prefered_Name__c" {
  const Prefered_Name__c:string;
  export default Prefered_Name__c;
}
declare module "@salesforce/schema/Community_Campaign_Vote__ChangeEvent.Residential_Address__c" {
  const Residential_Address__c:string;
  export default Residential_Address__c;
}
declare module "@salesforce/schema/Community_Campaign_Vote__ChangeEvent.Suburb__c" {
  const Suburb__c:string;
  export default Suburb__c;
}
declare module "@salesforce/schema/Community_Campaign_Vote__ChangeEvent.No_of_Candidate_Votes__c" {
  const No_of_Candidate_Votes__c:number;
  export default No_of_Candidate_Votes__c;
}
