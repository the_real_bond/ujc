declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__c.CnP_PaaS_EVT__American_Express__c" {
  const CnP_PaaS_EVT__American_Express__c:boolean;
  export default CnP_PaaS_EVT__American_Express__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__c.CnP_PaaS_EVT__C_P_API_Settings__r" {
  const CnP_PaaS_EVT__C_P_API_Settings__r:any;
  export default CnP_PaaS_EVT__C_P_API_Settings__r;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__c.CnP_PaaS_EVT__C_P_API_Settings__c" {
  const CnP_PaaS_EVT__C_P_API_Settings__c:any;
  export default CnP_PaaS_EVT__C_P_API_Settings__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__c.CnP_PaaS_EVT__C_P_Event__r" {
  const CnP_PaaS_EVT__C_P_Event__r:any;
  export default CnP_PaaS_EVT__C_P_Event__r;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__c.CnP_PaaS_EVT__C_P_Event__c" {
  const CnP_PaaS_EVT__C_P_Event__c:any;
  export default CnP_PaaS_EVT__C_P_Event__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__c.CnP_PaaS_EVT__Credit_Card__c" {
  const CnP_PaaS_EVT__Credit_Card__c:boolean;
  export default CnP_PaaS_EVT__Credit_Card__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__c.CnP_PaaS_EVT__Currency__c" {
  const CnP_PaaS_EVT__Currency__c:string;
  export default CnP_PaaS_EVT__Currency__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__c.CnP_PaaS_EVT__Currency_label__c" {
  const CnP_PaaS_EVT__Currency_label__c:string;
  export default CnP_PaaS_EVT__Currency_label__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__c.CnP_PaaS_EVT__Custom_Payment_Check__c" {
  const CnP_PaaS_EVT__Custom_Payment_Check__c:boolean;
  export default CnP_PaaS_EVT__Custom_Payment_Check__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__c.CnP_PaaS_EVT__Custom_Payment_Name__c" {
  const CnP_PaaS_EVT__Custom_Payment_Name__c:string;
  export default CnP_PaaS_EVT__Custom_Payment_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__c.CnP_PaaS_EVT__Discover__c" {
  const CnP_PaaS_EVT__Discover__c:boolean;
  export default CnP_PaaS_EVT__Discover__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__c.CnP_PaaS_EVT__Free_Payment__c" {
  const CnP_PaaS_EVT__Free_Payment__c:string;
  export default CnP_PaaS_EVT__Free_Payment__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__c.CnP_PaaS_EVT__Invoice__c" {
  const CnP_PaaS_EVT__Invoice__c:boolean;
  export default CnP_PaaS_EVT__Invoice__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__c.CnP_PaaS_EVT__JCB__c" {
  const CnP_PaaS_EVT__JCB__c:boolean;
  export default CnP_PaaS_EVT__JCB__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__c.CnP_PaaS_EVT__Master_Card__c" {
  const CnP_PaaS_EVT__Master_Card__c:boolean;
  export default CnP_PaaS_EVT__Master_Card__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__c.CnP_PaaS_EVT__Purchase_Order__c" {
  const CnP_PaaS_EVT__Purchase_Order__c:boolean;
  export default CnP_PaaS_EVT__Purchase_Order__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__c.CnP_PaaS_EVT__Visa__c" {
  const CnP_PaaS_EVT__Visa__c:boolean;
  export default CnP_PaaS_EVT__Visa__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__c.CnP_PaaS_EVT__eCheck__c" {
  const CnP_PaaS_EVT__eCheck__c:boolean;
  export default CnP_PaaS_EVT__eCheck__c;
}
