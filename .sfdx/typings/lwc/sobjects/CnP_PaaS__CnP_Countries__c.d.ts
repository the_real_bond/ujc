declare module "@salesforce/schema/CnP_PaaS__CnP_Countries__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Countries__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Countries__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Countries__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Countries__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Countries__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Countries__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Countries__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Countries__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Countries__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Countries__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Countries__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Countries__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Countries__c.CnP_PaaS__CnP_Country_Name__c" {
  const CnP_PaaS__CnP_Country_Name__c:string;
  export default CnP_PaaS__CnP_Country_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Countries__c.CnP_PaaS__Country_Abbrev__c" {
  const CnP_PaaS__Country_Abbrev__c:string;
  export default CnP_PaaS__Country_Abbrev__c;
}
declare module "@salesforce/schema/CnP_PaaS__CnP_Countries__c.CnP_PaaS__Country_Code__c" {
  const CnP_PaaS__Country_Code__c:string;
  export default CnP_PaaS__Country_Code__c;
}
