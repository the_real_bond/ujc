declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__c.CnP_PaaS__Contact__r" {
  const CnP_PaaS__Contact__r:any;
  export default CnP_PaaS__Contact__r;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__c.CnP_PaaS__Contact__c" {
  const CnP_PaaS__Contact__c:any;
  export default CnP_PaaS__Contact__c;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__c.CnP_PaaS__Alias_Contact_Data__c" {
  const CnP_PaaS__Alias_Contact_Data__c:string;
  export default CnP_PaaS__Alias_Contact_Data__c;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__c.CnP_PaaS__Billing_City__c" {
  const CnP_PaaS__Billing_City__c:string;
  export default CnP_PaaS__Billing_City__c;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__c.CnP_PaaS__Billing_Country__c" {
  const CnP_PaaS__Billing_Country__c:string;
  export default CnP_PaaS__Billing_Country__c;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__c.CnP_PaaS__Billing_Phone__c" {
  const CnP_PaaS__Billing_Phone__c:string;
  export default CnP_PaaS__Billing_Phone__c;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__c.CnP_PaaS__Billing_Postal_Code__c" {
  const CnP_PaaS__Billing_Postal_Code__c:string;
  export default CnP_PaaS__Billing_Postal_Code__c;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__c.CnP_PaaS__Billing_State__c" {
  const CnP_PaaS__Billing_State__c:string;
  export default CnP_PaaS__Billing_State__c;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__c.CnP_PaaS__Billing_Street__c" {
  const CnP_PaaS__Billing_Street__c:string;
  export default CnP_PaaS__Billing_Street__c;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__c.CnP_PaaS__Contact_Mail_id__c" {
  const CnP_PaaS__Contact_Mail_id__c:string;
  export default CnP_PaaS__Contact_Mail_id__c;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__c.CnP_PaaS__First_Name__c" {
  const CnP_PaaS__First_Name__c:string;
  export default CnP_PaaS__First_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS__Contact_Mail_ids__c.CnP_PaaS__Last_Name__c" {
  const CnP_PaaS__Last_Name__c:string;
  export default CnP_PaaS__Last_Name__c;
}
