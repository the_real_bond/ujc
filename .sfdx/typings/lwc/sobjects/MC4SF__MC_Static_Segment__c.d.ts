declare module "@salesforce/schema/MC4SF__MC_Static_Segment__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/MC4SF__MC_Static_Segment__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/MC4SF__MC_Static_Segment__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/MC4SF__MC_Static_Segment__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/MC4SF__MC_Static_Segment__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/MC4SF__MC_Static_Segment__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/MC4SF__MC_Static_Segment__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/MC4SF__MC_Static_Segment__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/MC4SF__MC_Static_Segment__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/MC4SF__MC_Static_Segment__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/MC4SF__MC_Static_Segment__c.MC4SF__MC_List__r" {
  const MC4SF__MC_List__r:any;
  export default MC4SF__MC_List__r;
}
declare module "@salesforce/schema/MC4SF__MC_Static_Segment__c.MC4SF__MC_List__c" {
  const MC4SF__MC_List__c:any;
  export default MC4SF__MC_List__c;
}
declare module "@salesforce/schema/MC4SF__MC_Static_Segment__c.MC4SF__Created_Date__c" {
  const MC4SF__Created_Date__c:string;
  export default MC4SF__Created_Date__c;
}
declare module "@salesforce/schema/MC4SF__MC_Static_Segment__c.MC4SF__Last_Reset__c" {
  const MC4SF__Last_Reset__c:string;
  export default MC4SF__Last_Reset__c;
}
declare module "@salesforce/schema/MC4SF__MC_Static_Segment__c.MC4SF__Last_Update__c" {
  const MC4SF__Last_Update__c:string;
  export default MC4SF__Last_Update__c;
}
declare module "@salesforce/schema/MC4SF__MC_Static_Segment__c.MC4SF__MailChimp_ID__c" {
  const MC4SF__MailChimp_ID__c:number;
  export default MC4SF__MailChimp_ID__c;
}
declare module "@salesforce/schema/MC4SF__MC_Static_Segment__c.MC4SF__Member_Count__c" {
  const MC4SF__Member_Count__c:number;
  export default MC4SF__Member_Count__c;
}
