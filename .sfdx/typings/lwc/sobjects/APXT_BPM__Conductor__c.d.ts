declare module "@salesforce/schema/APXT_BPM__Conductor__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__c.LastActivityDate" {
  const LastActivityDate:any;
  export default LastActivityDate;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__c.APXT_BPM__Content_Workspace_Id__c" {
  const APXT_BPM__Content_Workspace_Id__c:string;
  export default APXT_BPM__Content_Workspace_Id__c;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__c.APXT_BPM__Description__c" {
  const APXT_BPM__Description__c:string;
  export default APXT_BPM__Description__c;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__c.APXT_BPM__Has_Query_Id__c" {
  const APXT_BPM__Has_Query_Id__c:number;
  export default APXT_BPM__Has_Query_Id__c;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__c.APXT_BPM__Has_Record_Id__c" {
  const APXT_BPM__Has_Record_Id__c:number;
  export default APXT_BPM__Has_Record_Id__c;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__c.APXT_BPM__Has_Report_Id__c" {
  const APXT_BPM__Has_Report_Id__c:number;
  export default APXT_BPM__Has_Report_Id__c;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__c.APXT_BPM__Next_Run_Date_Display__c" {
  const APXT_BPM__Next_Run_Date_Display__c:any;
  export default APXT_BPM__Next_Run_Date_Display__c;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__c.APXT_BPM__Next_Run_Date__c" {
  const APXT_BPM__Next_Run_Date__c:any;
  export default APXT_BPM__Next_Run_Date__c;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__c.APXT_BPM__Query_Id__c" {
  const APXT_BPM__Query_Id__c:string;
  export default APXT_BPM__Query_Id__c;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__c.APXT_BPM__Record_Id__c" {
  const APXT_BPM__Record_Id__c:string;
  export default APXT_BPM__Record_Id__c;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__c.APXT_BPM__Report_Id__c" {
  const APXT_BPM__Report_Id__c:string;
  export default APXT_BPM__Report_Id__c;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__c.APXT_BPM__Schedule_Description_Display__c" {
  const APXT_BPM__Schedule_Description_Display__c:string;
  export default APXT_BPM__Schedule_Description_Display__c;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__c.APXT_BPM__Schedule_Description__c" {
  const APXT_BPM__Schedule_Description__c:string;
  export default APXT_BPM__Schedule_Description__c;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__c.APXT_BPM__Title__c" {
  const APXT_BPM__Title__c:string;
  export default APXT_BPM__Title__c;
}
declare module "@salesforce/schema/APXT_BPM__Conductor__c.APXT_BPM__URL_Field_Name__c" {
  const APXT_BPM__URL_Field_Name__c:string;
  export default APXT_BPM__URL_Field_Name__c;
}
