declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__ChangeEvent.CnP_PaaS_EVT__American_Express__c" {
  const CnP_PaaS_EVT__American_Express__c:boolean;
  export default CnP_PaaS_EVT__American_Express__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__ChangeEvent.CnP_PaaS_EVT__C_P_API_Settings__c" {
  const CnP_PaaS_EVT__C_P_API_Settings__c:any;
  export default CnP_PaaS_EVT__C_P_API_Settings__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__ChangeEvent.CnP_PaaS_EVT__C_P_Event__c" {
  const CnP_PaaS_EVT__C_P_Event__c:any;
  export default CnP_PaaS_EVT__C_P_Event__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__ChangeEvent.CnP_PaaS_EVT__Credit_Card__c" {
  const CnP_PaaS_EVT__Credit_Card__c:boolean;
  export default CnP_PaaS_EVT__Credit_Card__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__ChangeEvent.CnP_PaaS_EVT__Currency__c" {
  const CnP_PaaS_EVT__Currency__c:string;
  export default CnP_PaaS_EVT__Currency__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__ChangeEvent.CnP_PaaS_EVT__Currency_label__c" {
  const CnP_PaaS_EVT__Currency_label__c:string;
  export default CnP_PaaS_EVT__Currency_label__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__ChangeEvent.CnP_PaaS_EVT__Custom_Payment_Check__c" {
  const CnP_PaaS_EVT__Custom_Payment_Check__c:boolean;
  export default CnP_PaaS_EVT__Custom_Payment_Check__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__ChangeEvent.CnP_PaaS_EVT__Custom_Payment_Name__c" {
  const CnP_PaaS_EVT__Custom_Payment_Name__c:string;
  export default CnP_PaaS_EVT__Custom_Payment_Name__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__ChangeEvent.CnP_PaaS_EVT__Discover__c" {
  const CnP_PaaS_EVT__Discover__c:boolean;
  export default CnP_PaaS_EVT__Discover__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__ChangeEvent.CnP_PaaS_EVT__Free_Payment__c" {
  const CnP_PaaS_EVT__Free_Payment__c:string;
  export default CnP_PaaS_EVT__Free_Payment__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__ChangeEvent.CnP_PaaS_EVT__Invoice__c" {
  const CnP_PaaS_EVT__Invoice__c:boolean;
  export default CnP_PaaS_EVT__Invoice__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__ChangeEvent.CnP_PaaS_EVT__JCB__c" {
  const CnP_PaaS_EVT__JCB__c:boolean;
  export default CnP_PaaS_EVT__JCB__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__ChangeEvent.CnP_PaaS_EVT__Master_Card__c" {
  const CnP_PaaS_EVT__Master_Card__c:boolean;
  export default CnP_PaaS_EVT__Master_Card__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__ChangeEvent.CnP_PaaS_EVT__Purchase_Order__c" {
  const CnP_PaaS_EVT__Purchase_Order__c:boolean;
  export default CnP_PaaS_EVT__Purchase_Order__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__ChangeEvent.CnP_PaaS_EVT__Visa__c" {
  const CnP_PaaS_EVT__Visa__c:boolean;
  export default CnP_PaaS_EVT__Visa__c;
}
declare module "@salesforce/schema/CnP_PaaS_EVT__Event_Account_Info__ChangeEvent.CnP_PaaS_EVT__eCheck__c" {
  const CnP_PaaS_EVT__eCheck__c:boolean;
  export default CnP_PaaS_EVT__eCheck__c;
}
