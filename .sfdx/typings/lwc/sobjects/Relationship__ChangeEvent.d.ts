declare module "@salesforce/schema/Relationship__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Relationship__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/Relationship__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/Relationship__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Relationship__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Relationship__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Relationship__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Relationship__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Relationship__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Relationship__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Relationship__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Relationship__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Relationship__ChangeEvent.Description__c" {
  const Description__c:string;
  export default Description__c;
}
declare module "@salesforce/schema/Relationship__ChangeEvent.Individual_Communal_Reference_Number__c" {
  const Individual_Communal_Reference_Number__c:string;
  export default Individual_Communal_Reference_Number__c;
}
declare module "@salesforce/schema/Relationship__ChangeEvent.Related_Communal_Reference_Number__c" {
  const Related_Communal_Reference_Number__c:string;
  export default Related_Communal_Reference_Number__c;
}
declare module "@salesforce/schema/Relationship__ChangeEvent.Status__c" {
  const Status__c:string;
  export default Status__c;
}
declare module "@salesforce/schema/Relationship__ChangeEvent.Type__c" {
  const Type__c:string;
  export default Type__c;
}
declare module "@salesforce/schema/Relationship__ChangeEvent.Individual__c" {
  const Individual__c:any;
  export default Individual__c;
}
declare module "@salesforce/schema/Relationship__ChangeEvent.Related_Individual__c" {
  const Related_Individual__c:any;
  export default Related_Individual__c;
}
declare module "@salesforce/schema/Relationship__ChangeEvent.Reciprocal_Relationship__c" {
  const Reciprocal_Relationship__c:any;
  export default Reciprocal_Relationship__c;
}
declare module "@salesforce/schema/Relationship__ChangeEvent.Individual_Gender__c" {
  const Individual_Gender__c:string;
  export default Individual_Gender__c;
}
declare module "@salesforce/schema/Relationship__ChangeEvent.Related_Individual_Gender__c" {
  const Related_Individual_Gender__c:string;
  export default Related_Individual_Gender__c;
}
declare module "@salesforce/schema/Relationship__ChangeEvent.DB_Status__c" {
  const DB_Status__c:string;
  export default DB_Status__c;
}
declare module "@salesforce/schema/Relationship__ChangeEvent.DoD__c" {
  const DoD__c:any;
  export default DoD__c;
}
