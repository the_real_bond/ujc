declare module "@salesforce/schema/PowerLoader__BulkEditTemplate__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplate__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplate__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplate__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplate__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplate__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplate__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplate__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplate__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplate__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplate__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplate__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplate__c.PowerLoader__Default__c" {
  const PowerLoader__Default__c:boolean;
  export default PowerLoader__Default__c;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplate__c.PowerLoader__JustMyRecords__c" {
  const PowerLoader__JustMyRecords__c:boolean;
  export default PowerLoader__JustMyRecords__c;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplate__c.PowerLoader__ObjectAPIName__c" {
  const PowerLoader__ObjectAPIName__c:string;
  export default PowerLoader__ObjectAPIName__c;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplate__c.PowerLoader__Private__c" {
  const PowerLoader__Private__c:boolean;
  export default PowerLoader__Private__c;
}
declare module "@salesforce/schema/PowerLoader__BulkEditTemplate__c.PowerLoader__UID__c" {
  const PowerLoader__UID__c:string;
  export default PowerLoader__UID__c;
}
