declare module "@salesforce/schema/APXTConga4__Conga_Email_Staging__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Staging__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Staging__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Staging__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Staging__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Staging__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Staging__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Staging__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Staging__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Staging__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Staging__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Staging__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Staging__ChangeEvent.APXTConga4__HTMLBody__c" {
  const APXTConga4__HTMLBody__c:string;
  export default APXTConga4__HTMLBody__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Staging__ChangeEvent.APXTConga4__Subject__c" {
  const APXTConga4__Subject__c:string;
  export default APXTConga4__Subject__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Staging__ChangeEvent.APXTConga4__TextBody__c" {
  const APXTConga4__TextBody__c:string;
  export default APXTConga4__TextBody__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Staging__ChangeEvent.APXTConga4__WhatId__c" {
  const APXTConga4__WhatId__c:string;
  export default APXTConga4__WhatId__c;
}
declare module "@salesforce/schema/APXTConga4__Conga_Email_Staging__ChangeEvent.APXTConga4__WhoId__c" {
  const APXTConga4__WhoId__c:string;
  export default APXTConga4__WhoId__c;
}
